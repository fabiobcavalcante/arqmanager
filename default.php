﻿<!DOCTYPE html>

<html lang="pt-BR">

<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="format-detection" content="telephone=no">

    <meta name="robots" content="noindex">

    <title>LUANA PALHETA ARQUITETURA</title>

  <link rel="shortcut icon" href="dist/img/favicon/favicon.ico">
  <link rel="shortcut icon" href="dist/img/favicon/favicon-16x16.ico" sizes="16x16">
  <link rel="shortcut icon" href="dist/img/favicon/favicon-32x32.ico" sizes="32x32">

  <link rel="android" href="dist/img/favicon/android-chrome-192x192.png" sizes="192x192">
  <link rel="android" href="dist/img/favicon/android-chrome-512x512.png" sizes="512x512">

    <link rel="apple-touch-icon" href="dist/img/favicon/apple-touch-icon.png">

    <!-- <link rel="icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-32.png" sizes="32x32">
    <link rel="icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-57.png" sizes="57x57">
    <link rel="icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-76.png" sizes="76x76">
    <link rel="icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-96.png" sizes="96x96">
    <link rel="icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-128.png" sizes="128x128">
    <link rel="shortcut icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-192.png" sizes="192x192">
    <link rel="apple-touch-icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-152.png" sizes="152x152">
    <link rel="apple-touch-icon" href="https://latam-files.hostgator.com/system/temporary-page/images/favicons/favicon-180.png" sizes="180x180"> -->

    <link href="https://latam-files.hostgator.com/system/temporary-page/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://latam-files.hostgator.com/system/temporary-page/css/fonts.css" rel="stylesheet">


  <meta charset="utf-8">
  <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate, Post-Check=0, Pre-Check=0">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="admin/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load.
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">-->
  <link rel="stylesheet" href="admin/dist/css/skins/skin-LUANAPALHETA.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style type="text/css">
        body {background-color: #B1AFAE;}
        a, a:visited {
            color:#000000 !important;
        }
        a:active,a:hover{
            color:#E5E5E1 !important;
        }
        body {
            background-image: url('admin/dist/img/grade.svg') ;
            background-repeat: repeat-y;
            background-position: right top;
        }
        @media (min-width: 320px) and (max-width: 1024px) {
            body {
                background-image: none;
            }
        }
    </style>

</head>

<body>

<section class="container space">
    <div style="margin: 40px 0 auto;">

        <div class="row">

            <div class="col-xs-12">

                <div class="logo">

                    <img  src="admin/dist/img/logo/logo_branca.svg" alt="Logo">

                </div>

            </div>

        </div>


        <div class="row">

            <div class="col-md-12">

                <h3>Contatos</h3>
                <hr>
                <div class="col-md-4 col-md-12">
                    <a href="https://wa.me/559181469630"><i class="fa fa-whatsapp" aria-hidden="true"></i> 91 98146-9630</a>
                    <div></div>
                    <a href="https://www.instagram.com/luanapalheta.arquitetura"><i class="fa fa-instagram" aria-hidden="true"></i> @luanapalheta.arquitetura</a>
                </div>
                <div class="col-md-4 col-md-12">
                    <a href="mailto:contato@luanapalheta.com.br"><i class="fa fa-envelope-o" aria-hidden="true"></i> contato@luanapalheta.com.br</a>
                    <div></div>
                    <a href="mailto:projetos@luanapalheta.com.br"><i class="fa fa-envelope-o" aria-hidden="true"></i> projetos@luanapalheta.com.br</a>
                </div>

                <div class="col-md-4 col-md-12">
                    <a href="https://www.google.com.br/maps/place/Condom%C3%ADnio+Mirai+Offices/@-1.439657,-48.4933435,17z/data=!3m1!4b1!4m5!3m4!1s0x92a48ebdd0828535:0x8ed231de8c0f1ea9!8m2!3d-1.4396624!4d-48.4911548" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i> Rua Municipalidade, 985 - Ed. Mirai Offices
                    sala 617 - 66050-350 - Belém Pará.</a>
                </div>
            </div>
        </div>
    </div>
</section>

</body>

</html>
