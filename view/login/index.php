<?php
$oFachada = new FachadaPrincipalBD();
?>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ARQ MANAGER - Login</title>
    <?php include_once('includes/head.php')?>
    <style type="text/css">
        .login-page{
            background-color:#d2d6de;
        }
    </style>
</head>
<body class="hold-transition login-page" onload="return foco();">
<div class="login-box">
    <div class="login-logo">
        <a href="#"><img class="image-responsive" src="dist/img/logo_arq_azul.png" style="width: 300px"></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php include_once('includes/mensagem.php')?>
        <p class="login-box-msg">Entre com seus dados para acessar o sistema</p>

        <form name="frmLogin" action="?action=Login.processaFormulario" method="post">
            <input type="hidden" name="sOP" value="Logon">
            <div class="form-group has-feedback">
                <input type="text" name="fLogin" id="login" class="form-control" placeholder="Login" value="<?php echo isset($_POST['fLogin']) ? $_POST['fLogin'] : ""; ?>" required>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="fSenha" class="form-control" placeholder="Senha" value="<?php echo isset($_POST['fSenha']) ? $_POST['fSenha'] : ""; ?>" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <?php if (isset($_REQUEST['voEscritorio']) && $_REQUEST['voEscritorio']) { ?>
                <div class="form-group has-feedback" id="divEscritorio">
                    <select class="form-control" name="fIdEscritorio" id="IdEscritorio" required>
                        <option value="">Selecione o Escritório</option>
                        <?php foreach ($_REQUEST['voEscritorio'] as $key => $oEscritorio) { ?>
                            <option value="<?php echo $oEscritorio->getIdEscritorio(); ?>"><?php echo $oEscritorio->getNomeFantasia(); ?></option>
                        <?php } ?>
                    </select>
                </div>
            <?php } ?>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-success btn-block btn-flat">Entrar</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php include_once('includes/javascript.php')?>
<script>
    function foco(){
        document.frmLogin.login.focus();
    }


</script>
</body>
</html>
