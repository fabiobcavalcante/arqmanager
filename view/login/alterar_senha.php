<?php
$sOP = $_REQUEST['sOP'];
$oColaborador = $_REQUEST['oColaborador'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Usuário - <?php echo $sOP ?></title>
    <?php include_once('includes/head.php')?>
    <style type="text/css">
        #progress{
            background: transparent;
            transition: border 0.2s;
            color: #fff;
        }
        #progress div{
            background: transparent;
            transition: background 0.2s;
        }
    </style>
</head>

<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->

        <div class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
                <span class="logo-lg">
                  <img color="white" src="https://arqmanager.com.br/wp-content/uploads/2021/06/logo_arq_azul_min.svg">
                </span>
              </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img color="white" src="https://arqmanager.com.br/wp-content/uploads/2021/06/logo_arq_azul.png" class="imagemLogo"></span>
        </div>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

        </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Acesso</h1>
            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <form method="post" class="form-horizontal" name="formColaborador" action="?action=Colaborador.processaFormulario">
                            <div class="box-header">
                                <h3 class="box-title">Colaborador - Alterar Senha</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <input type="hidden" name="sOP" value="AlterarSenha" />
                                <input type="hidden" name="fCodColaborador" value="<?php echo $oColaborador->getCodColaborador(); ?>">
                                <div  class="box-body" id="form-group">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="Senha">Senha Atual:</label>
                                            <div class="input-group col-md-11">
                                                <input class="form-control" type='password' placeholder='Senha Atual' name='fSenhaAtual'  required   value=''/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="Senha">Nova Senha:</label>
                                            <div class="input-group col-md-11">
                                                <input class="form-control" type='password' id='SenhaNova' placeholder='Senha Nova' name='fSenhaNova'  required   value=''  />
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="Senha">Repete nova Senha:</label>
                                            <div class="input-group col-md-11">
                                                <input class="form-control" type='password' id='SenhaNova2' placeholder='Senha Nova' name='fSenhaNova2'  required   value=''/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-1" id="progress"><div></div></div>

                                    </div>
                                    <input type='hidden' name='fAtivo' value='<?php echo ($oColaborador) ? $oColaborador->getAtivo() : "1"?>'/>
                                    <div class="form-group col-md-12">
                                        <div class="col-sm-offset-5 col-sm-2">
                                            <button type="submit" class="btn btn-lg btn-success" >Alterar Senha </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script type="text/javascript" language="javascript">
    jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
    function progress(percent, $element, velocity) {
        percent = percent >= 100 ? 100 : percent;
        var progressBarWidth = percent * $element.width() / 100;
        $element.find('div').stop().animate({ width: progressBarWidth }, velocity, "linear").html(percent + "% ");
    }

    $('input').on('input', function(){
        var value = $(this).val();
        var progressValue = $('#progress div');
        var color, percent = 0;
        if(value.length <= 5){
            color = "red";
            percent = 25;
        }else if(value.length <= 10){
            color = "orange";
            percent = 50;
        }else{
            color = "#3c948b";
            percent = 100;
        }
        progress(percent, $('#progress'), 300);
        progressValue.css("background", color);
        $('#progress').css("border", "1px solid " + color);
    })
</script>
</body>
</html>
