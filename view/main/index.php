<?php
$oFachada = new FachadaPrincipalBD();
$voResultado = false;
$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
$oPrincipal = $oFachada->recuperarUmRelatorioPrincipal($nIdEscritorio);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>ARQMANAGER</title>
    <?php include_once('./includes/head.php');
    $f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
    ?>
</head>
<body class="hold-transition skin-green-light sidebar-mini">
<div class="wrapper">
    <?php include_once('./includes/header.php')?>
    <?php include_once('./includes/menu.php')?>

    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?>
                    <!--<small>it all starts here</small>-->
                </h1>
                <ol class="breadcrumb">
                    <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                </ol>
                <?php include_once('./includes/mensagem.php')?>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Seja bem vindo!</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <!-- small box -->
                                <div class="small-box bg-primary">
                                    <div class="inner">
                                        <h3><?php echo $oPrincipal->propostas_sem_projeto;?></h3>
                                        <p>Propostas não confirmadas</p>
                                    </div>
                                    <div class="icon"> <i class="fa fa-flag-o" aria-hidden="true"></i> </div>
                                    <a href="?action=Proposta.preparaLista&nStatus=1" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                            </div>
							<div class="col-lg-3 col-xs-12">
                                <!-- small box -->
                                <div class="small-box bg-aqua">
                                    <div class="inner">
                                        <h3>
                                            <?php echo $oPrincipal->projetos;?>
                                        </h3>
                                        <p>Projetos em andamento</p>
                                    </div>
                                    <div class="icon"> <i class="ion ion-bag"></i> </div>
                                    <a href="?action=Projeto.preparaLista&sStatus=ANDAMENTO&sOP=Visualizar" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                            </div>
							<div class="col-lg-3 col-xs-12">
                                <!-- small box -->
                                <div class="small-box bg-info" style="background-color:DeepSkyBlue">
                                    <div class="inner">
                                        <h3>
                                            <?php echo $oPrincipal->projetos_concluidos;?>
                                        </h3>
                                        <p>Projetos concluídos</p>
                                    </div>
                                    <div class="icon"> <i class="fa fa-graduation-cap"></i> </div>
                                    <a href="?action=Projeto.preparaLista&sStatus=CONCLUIDO&sOP=Visualizar" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                            </div>
							<div class="col-lg-3 col-xs-12">
                                <!-- small box -->
                                <div class="small-box bg-yellow">
                                    <div class="inner">
                                        <h3><?php echo $oPrincipal->aniversariantes;?></h3>
                                        <p>Aniversariantes do mês</p>
                                    </div>
                                    <div class="icon"> <i class="fa fa-birthday-cake" aria-hidden="true"></i> </div>
                                    <a href="?action=Cliente.listaAniversariantes&sOP=Visualizar&nMesSelecionado=<?php echo date('m')?>" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                            </div>

                            <!-- ./col -->

                            <?php //if($_SESSION['oUsuarioLP']->getCodGrupoUsuario() == 1 || $_SESSION['oUsuarioLP']->getCodGrupoUsuario() == 2){?>
                                <!-- <div class="col-lg-3 col-xs-12">

                                    <div class="small-box bg-red">
                                        <div class="inner">
                                            <h3><?php // echo $f->format($oPrincipal->despesas);?></h3>
                                            <p>Despesas efetivadas dos últimos 30 dias</p>
                                        </div>
                                        <div class="icon"> <i class="ion ion-pie-graph"></i> </div>
                                        <a href="?action=ContaMovimentacao.preparaLista&sOP=DespesasEfetivadas" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                                </div> -->

                                <!-- ./col -->
                                <!-- <div class="col-lg-3 col-xs-12">
                                    <!-- small box -->
                                  <!--  <div class="small-box bg-green">
                                        <div class="inner">
                                            <h3><?php // echo $f->format($oPrincipal->entradas);?></h3>
                                            <p>Entradas confirmadas dos últimos 30 dias</p>
                                        </div>
                                        <div class="icon"> <i class="fa fa-usd" aria-hidden="true"></i> </div>
                                        <a href="?action=ContaMovimentacao.preparaLista&sOP=EntradasEfetivadas" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                                </div> -->
                                <!-- ./col -->
                            <?php //} ?>





                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- /.box-body -->
        <div class="box-footer" style="text-align: center"></div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
</div>
<!-- /.content-wrapper -->
<?php include_once('./includes/footer.php')?>

<?php include_once('./includes/javascript.php')?>
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>

</body>
</html>
