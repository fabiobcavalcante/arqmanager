<?php
 $sOP = $_REQUEST['sOP'];
 $oEscritorio = $_REQUEST['oEscritorio'];
 $voEmailTipo = $_REQUEST['voEmailTipo'];
 // $vSelecionado = $_REQUEST['aEmailEscritorio'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Escritório - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Configurações - <?php echo $sOP?></li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <form method="post" class="form-horizontal" name="formEscritorio" action="?action=Escritorio.processaFormulario">
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Configurações - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->

 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fIdEscritorio" value="<?=(is_object($oEscritorio)) ? $oEscritorio->getIdEscritorio() : ""?>" />
 				<div  class="box-body" id="form-group">
 								<input type='hidden' name='fIdEscritorio' value='<?php echo ($oEscritorio) ? $oEscritorio->getIdEscritorio() : ""?>'/>

        <div class="form-group col-md-4">
					<label for="NomeFantasia">Fantasia:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='NomeFantasia' placeholder='Fantasia' name='fNomeFantasia'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getNomeFantasia() : ""?>'/>
				</div>
			</div>

      <div class="form-group col-md-4">
					<label for="RazaoSocial">Razão Social:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='RazaoSocial' placeholder='Razao_social' name='fRazaoSocial'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getRazaoSocial() : ""?>'/>
				</div>
			</div>

      <div class="form-group col-md-4">
					<label for="Documento">Documento:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Documento' placeholder='Documento' name='fDocumento'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getDocumento() : ""?>'/>
				</div>
			</div>

      <div class="form-group col-md-4">
					<label for="EnderecoReduzido">Endereço reduzido:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='EnderecoReduzido' placeholder='Endereco_reduzido' name='fEnderecoReduzido'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getEnderecoReduzido() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-4">
					<label for="EnderecoCompleto">Endereço Completo:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='EnderecoCompleto' placeholder='Endereco_completo' name='fEnderecoCompleto'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getEnderecoCompleto() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-2">
					<label for="cidadeuf">Cidade Uf:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='CidadeUf' placeholder='Cidade / UF' name='fCidadeUf'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getCidadeUf() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-2">
					<label for="Fone">Fone:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Fone' placeholder='Fone' name='fFone'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getFone() : ""?>'/>
				</div>
			</div>

      <div class="form-group col-md-4">
					<label for="Site">Site:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Site' placeholder='Site' name='fSite'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getSite() : ""?>'/>
				</div>
			</div>

      <div class="form-group col-md-4">
					<label for="Email">Email:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Email' placeholder='Email' name='fEmail'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getEmail() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-4">
					<label for="Email">Email Administrador:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='EmailAdministrador' placeholder='EmailAdministrador' name='fEmailAdministrador'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getEmailAdministrador() : ""?>'/>
				</div>
			</div>

      <div class="form-group col-md-4">
					<label for="Instagram">Instagram:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Instagram' placeholder='Instagram' name='fInstagram'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getInstagram() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-4">
					<label for="logomarca">Logomarca:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Logo' placeholder='Logo' name='fLogomarca'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getLogomarca() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-4">
					<label for="logomarcamini">Logomarca Mini:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Logo' placeholder='Logo' name='fLogomarcaMini'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getLogomarcaMini() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-4">
					<label for="Assinatura">Assinatura:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Assinatura' placeholder='Assinatura' name='fAssinatura'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getAssinatura() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-4">
					<label for="Assinatura">Hora Técnica:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='HoraTecnica' placeholder='Hora' name='fHoraTecnica'  required   value='<?php echo ($oEscritorio) ? $oEscritorio->getHoraTecnica() : ""?>'/>
				</div>
			</div>
      <div class="form-group col-md-2">
					<label for="EnvioEmail">Envio de Email:</label>
					<div class="input-group col-md-11">
          <select name='fEnvioEmail' id="EnvioEmail"  class="form-control"  required >
          <option>Selecione</option>
          <option <?php echo (is_object($oEscritorio) && $oEscritorio->getEnvioEmail()=='S') ? " selected ": " "?> value='S'>Sim</option>
          <option <?php echo (is_object($oEscritorio) && $oEscritorio->getEnvioEmail()=='N') ? " selected ": " "?> value='N'>Não</option>
        </select>

				</div>
			</div>
      <div class="form-group col-md-2">
					<label for="DiasUteis">Dias Úteis:</label>
					<div class="input-group col-md-11">
          <select name='fDiasUteis' id="DiasUteis"  class="form-control"  required >
          <option>Selecione</option>
          <option <?php echo (is_object($oEscritorio) && $oEscritorio->getDiasUteis()=='S') ? " selected ": " "?> value='S'>Sim</option>
          <option <?php echo (is_object($oEscritorio) && $oEscritorio->getDiasUteis()=='N') ? " selected ": " "?> value='N'>Não</option>
        </select>

				</div>
			</div>
      <div class="form-group col-md-6">
          <label for="Assinatura">Cabeçalho Email:</label>
          <div class="input-group col-md-11">
          <textarea rows="8" class="form-control" placeholder='Cabeçalho' name='fCabecalhoEmail'  required><?php echo ($oEscritorio) ? $oEscritorio->getCabecalhoEmail() : ""?></textarea>
        </div>
      </div>
      <div class="form-group col-md-6">
          <label for="Assinatura">Rodapé Email:</label>
          <div class="input-group col-md-11">
          <textarea rows="8" class="form-control" placeholder='Rodapé' name='fRodapeEmail'  required><?php echo ($oEscritorio) ? $oEscritorio->getRodapeEmail() : ""?></textarea>
        </div>
      </div>
 			</div>
 				<!-- /.box-body -->

           </div>
           <!-- /.box -->
           <?php if($voEmailTipo){?>
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title">Email's a serem enviados</h3>
             </div>
             <div class="box-body">
               <div class='row'></div>
               <?php if(is_array($voEmailTipo)){?>
                 <table id="lista" class="table table-bordered table-striped">
                   <thead>
                   <tr>

                     <th>Título</th>
                 <th>Habilitar</th>
                 <th>Ação</th>


                   </tr>
                   </thead>
                   <tbody>
                           <?php foreach($voEmailTipo as $oEmailTipo){ ?>
                   <tr>

                     <td><?php echo $oEmailTipo->getTitulo()?></td>
           					<td><input type="checkbox" value="<?php echo $oEmailTipo->getCodEmailTipo()?>" <?php echo ($oEmailTipo->getAtivo()==1)? " checked ": "" ?> name="fCodEmailTipo[]"/></td>
           					<td> <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Ajustar Template</button><a href="?action=EmailTipoEscritorio.prepaFormulario&sOP=Alterar&nCodEmailTipo=<?php echo $oEmailTipo->getCodEmailTipo()?>">Ajustar Template</a></td>

                 </tr>
                 <?php }?>
                 </tbody>
                   <tfoot>
                         <tr>

                           <th>Título</th>
                       <th>Habilitar</th>
                       <th>Ação</th>

                         </tr>
                         </tfoot>
                       </table>
               <?php }//if(count($voCliente)){?>


        <div class="form-group col-md-12">
            <div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Cliente.preparaLista">Voltar</a></div>
            <button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
        </div>
         </div>
             <!-- /.box-body -->
             </div>
               <!-- /.box -->
             <?php }//if($voProjeto){?>



         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
     </form>
   </div>
    <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>

 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <!-- Large modal -->


 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <p>fsdsdfsfsdf</p>             <p>fsdsdfsfsdf</p>             <p>fsdsdfsfsdf</p>             <p>fsdsdfsfsdf</p>             <p>fsdsdfsfsdf</p>
             sdfsdfsd\
         </div>
     </div>
 </div>


 </body>
 </html>
