﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oEscritorio = $_REQUEST['oEscritorio'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>LUANAPALHETA - escritorio - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1>LUANAPALHETA - Principal</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Escritorio.preparaLista">Gerenciar escritorios</a>
 			<li class="active">escritorio - <?php echo $sOP?></li>
 		</ol>	  
 
     </section>
 		
     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">escritorio - Detalhe</h3>
         </div>
         <div class="box-body">
           	
 <div class="col-md-4">
		
 <label for="NomeFantasia" class="control-label">Nome_fantasia:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getNomeFantasia() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="RazaoSocial" class="control-label">Razao_social:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getRazaoSocial() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="Documento" class="control-label">Documento:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getDocumento() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="EnderecoReduzido" class="control-label">Endereco_reduzido:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getEnderecoReduzido() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="Fone" class="control-label">Fone:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getFone() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="EnderecoCompleto" class="control-label">Endereco_completo:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getEnderecoCompleto() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="Site" class="control-label">Site:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getSite() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="Email" class="control-label">Email:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getEmail() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="Instagram" class="control-label">Instagram:</label>
		<p><?php echo ($oEscritorio) ? $oEscritorio->getInstagram() : ""?></p>
	</div>

 		 
 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Escritorio.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>		
 </div>
 <!-- ./wrapper -->
 </body>
 </html>