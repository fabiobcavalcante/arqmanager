<?php
 $voEscritorio = $_REQUEST['voEscritorio'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>LUANAPALHETA - Lista de escritorio </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1>LUANAPALHETA - Principal</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar escritorios</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">escritorio</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formEscritorio" id="formEscritorio" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesEscritorio" onChange="JavaScript: submeteForm('Escritorio')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=Escritorio.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo escritorio</option>
   						<option value="?action=Escritorio.preparaFormulario&sOP=Alterar" lang="1">Alterar escritorio selecionado</option>
   						<option value="?action=Escritorio.preparaFormulario&sOP=Detalhar" lang="1">Detalhar escritorio selecionado</option>
   						<option value="?action=Escritorio.processaFormulario&sOP=Excluir" lang="2">Excluir escritorio(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voEscritorio)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('Escritorio')"><i class="icon fa fa-check"></a></th>
   					<th>IdEscritorio</th>
					<th>NomeFantasia</th>
					<th>RazaoSocial</th>
					<th>Documento</th>
					<th>EnderecoReduzido</th>
					<th>Fone</th>
					<th>EnderecoCompleto</th>
					<th>Site</th>
					<th>Email</th>
					<th>Instagram</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voEscritorio as $oEscritorio){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('Escritorio')" type="checkbox" value="<?php echo $oEscritorio->getIdEscritorio()?>" name="fIdEscritorio[]"/></td>
  					<td><?php echo $oEscritorio->getIdEscritorio()?></td>
					<td><?php echo $oEscritorio->getNomeFantasia()?></td>
					<td><?php echo $oEscritorio->getRazaoSocial()?></td>
					<td><?php echo $oEscritorio->getDocumento()?></td>
					<td><?php echo $oEscritorio->getEnderecoReduzido()?></td>
					<td><?php echo $oEscritorio->getFone()?></td>
					<td><?php echo $oEscritorio->getEnderecoCompleto()?></td>
					<td><?php echo $oEscritorio->getSite()?></td>
					<td><?php echo $oEscritorio->getEmail()?></td>
					<td><?php echo $oEscritorio->getInstagram()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>IdEscritorio</th>
					<th>NomeFantasia</th>
					<th>RazaoSocial</th>
					<th>Documento</th>
					<th>EnderecoReduzido</th>
					<th>Fone</th>
					<th>EnderecoCompleto</th>
					<th>Site</th>
					<th>Email</th>
					<th>Instagram</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voEscritorio)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
