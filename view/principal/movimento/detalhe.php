﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oMovimento = $_REQUEST['oMovimento'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lançamento - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Lnaçamento</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Movimento.preparaLista">Gerenciar Lançamentos</a>
 			<li class="active">Lançamento - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Lançamento - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="CodMovimento" class="control-label">Cod:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getCodMovimento() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="CodDespesaReceitaTipo" class="control-label">Tipo de Lançamento:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getCodDespesaReceitaTipo() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DataEntrada" class="control-label">Data:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getDataEntrada() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Descricao" class="control-label">Desrição:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getDescricao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DataPrevisao" class="control-label">Data Previsão:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getDataPrevisao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DataEfetivacao" class="control-label">Data Efetivação:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getDataEfetivacao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Observacao" class="control-label">Observação:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getObservacao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="CodProjetoPagamento" class="control-label">Projeto:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getCodProjetoPagamento() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="InseridoPor" class="control-label">Inserido Por:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getInseridoPor() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oMovimento) ? $oMovimento->getAlteradoPor() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Movimento.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
