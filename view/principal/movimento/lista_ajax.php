<?php
$voMovimentoPendente = $_REQUEST['voMovimentoPendente'];
$nTotal = 0;
if($voMovimentoPendente){
?>
<table id="lista" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Ações</th>
        <th>Tipo</th>
        <th>Descrição</th>
        <th>Previsão</th>
        <th>Valor</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($voMovimentoPendente as $oMovimentoPendente){
        $nTotal += $oMovimentoPendente->valor;
        ?>
        <tr>
            <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-codigo="<?php echo $oMovimentoPendente->codigo;?>" data-tipo="<?php echo $oMovimentoPendente->tipo?>" data-acao="Alterar" data-target="#modalMovimento"> <i class="fa fa-edit"></i></button>
                <a data-toggle="modal" data-target="#confirm-delete" data-link="?action=Movimento.processaFormulario&sOP=Excluir&nCodMovimento=<?php echo $oMovimentoPendente->codigo; ?>"><button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Excluir"> <i class="fa fa-trash"></i></button></a>

            </td>
            <td><?php echo $oMovimentoPendente->tipo?></td>
            <td><?php echo $oMovimentoPendente->descricao?></td>
            <td><?php echo $oMovimentoPendente->data?></td>
            <td>R$<?php echo $oMovimentoPendente->valor?></td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th>Total</th>
        <th>R$<?php echo number_format($nTotal,2,",",".");?></th>
    </tr>
    </tfoot>
</table>
<?php }else{ ?>
  <div title="Mensagem" class="btn bg-success">
      <p><i class="fa fa-check"></i> Não existem movimentos pendentes de confirmação</p>
  </div>
<?php }//else{ ?>
