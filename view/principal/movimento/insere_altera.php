<?php
$oFachada = new FachadaPrincipalBD();
$sOP = $_REQUEST['sOP'];
$oMovimento = $_REQUEST['oMovimento'] ?? null;
$sTitulo = $_REQUEST['sTitulo'];
$voProjeto = $_REQUEST['voProjeto'];
$voContaBancaria = $_REQUEST['voContaBancaria'];
$voPlanoContas = $_REQUEST['voPlanoContas'];
$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
?>
<form method="post" class="form-horizontal" name="formMovimento" id="formMovimento" action="?action=Movimento.processaFormulario" enctype="multipart/form-data">
    <input type="hidden" name="fCodMovimento" value="<?php echo ($oMovimento) ? $oMovimento->getCodMovimento():"" ?>">
    <input type="hidden" name="sOP" value="<?php echo $sOP?>"><!-- aqui-->
    <input type="hidden" name="nAno" value="<?php echo $_REQUEST['nAno'] ?? null; ?>">
    <input type="hidden" name="nMes" value="<?php echo $_REQUEST['nMes'] ?? null; ?>">
    <input type="hidden" name="nContaBancaria" value="<?php echo $_REQUEST['nContaBancaria'] ?? null; ?>">
    <input type="hidden" name="fCodDespesaReceitaTipo" value="<?php echo $_REQUEST['nTipo']; ?>"><!-- aqui-->
    <div class="modal-header" style="color:#FFF; background-color: #696969;; border-top-left-radius: 2px; border-top-right-radius: 2px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php  echo $sTitulo?></h4><!-- aqui-->
    </div>

    <div class="modal-body">

        <div class="modal-body">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="Apropriacao">Apropriação:</label>
                    <select name='fCodPlanoContas' id="PlanoContas" class="form-control select2" style="width:95%" required >
                        <option value=''>Selecione</option>
                        <?php
                        if($_REQUEST['nTipo'] == 1){
                            if($voPlanoContas){?>
                                <?php foreach($voPlanoContas as $oPlanoContas){
                                    $sSelected = ($oMovimento && $oMovimento->getCodPlanoContas() == $oPlanoContas->getCodPlanoContas()) ? "selected" : "";
                                    ?>
                                    <option value='<?php echo $oPlanoContas->getCodPlanoContas()?>' <?php echo $sSelected; ?>><?php echo $oPlanoContas->getDescricao()?></option>
                                <?php }
                            }
                        }else{
                            if($voPlanoContas){
                                foreach($voPlanoContas as $oPlanoContas){ ?>
                                    <optgroup label="<?php echo  $oPlanoContas->getDescricao()?>">
                                        <?php $voPlanoContasDetalhe = $oFachada->recuperarTodosPlanoContasPorPai($nIdEscritorio,$oPlanoContas->getCodPlanoContas(),1);
                                        if($voPlanoContasDetalhe){
                                            foreach($voPlanoContasDetalhe as $oPlanoContasDetalhe){
                                                $sSelected = ($oMovimento && $oMovimento->getCodPlanoContas() == $oPlanoContasDetalhe->getCodPlanoContas()) ? "selected" : "";
                                                ?>
                                                <option value='<?php echo $oPlanoContasDetalhe->getCodPlanoContas()?>' <?php echo $sSelected; ?>><?php echo $oPlanoContasDetalhe->getDescricao()?></option>
                                            <?php }
                                        }
                                        ?>
                                    </optgroup>

                                <?php }
                            }
                        } ?>

                    </select>
                </div>
                <div class="col-md-3">
                    <label for="Competencia">Competência:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <input class="form-control mesano" type='text' maxlength="7" id='Competencia' placeholder='99/9999' name='fCompetencia'  required   value='<?php echo ($oMovimento) ? $oMovimento->getCompetenciaFormatado():"" ?>'/>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="DataEntrada">Data:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <input class="form-control date" type='text' id='DataEntrada' placeholder='Data' name='fDataEntrada'  required   value='<?php echo ($oMovimento) ? $oMovimento->getDataEntradaFormatado():"" ?>'/>
                    </div>
                </div>

                <div class="col-md-6">
                    <label for="Descricao">Descrição:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'  required   value='<?php echo ($oMovimento) ? $oMovimento->getDescricao():"" ?>'/>
                    </div>
                </div>


                <div class="col-md-6">
                    <label for="Valor">Valor:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <input class="form-control" type='text' id='Valor' placeholder='Valor' name='fValor'  onkeyup='FormataMoney(this);' required  value='<?php echo ($oMovimento) ? $oMovimento->getValorFormatado():"" ?>'/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="Projeto">Projeto:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <select name='fCodProjeto' id="Projeto" class="form-control select2" style="width:95%">
                            <option value=''>Selecione</option>
                            <?php if($voProjeto){?>
                                <?php foreach($voProjeto as $oProjeto){
                                    if($oMovimento && $oMovimento->getCodProjeto()){
                                        if($oMovimento->getCodProjeto() == $oProjeto->cod_projeto)
                                            $sSelect=" selected ";
                                        else {
                                            $sSelect="";
                                        }
                                    }else{
                                        $sSelect="";
                                    }
                                    ?>
                                    <option <?php echo $sSelect?> value='<?php echo $oProjeto->cod_projeto?>'><?php echo "{$oProjeto->nome} - {$oProjeto->identificacao}"?></option>
                                <?php																	}
                            }  ?>

                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <label for="Observacao">Observação:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <input class="form-control" type='text' id='Observacao' placeholder='Observação' name='fObservacao'  value='<?php echo ($oMovimento) ? $oMovimento->getObservacao():"" ?>'/>
                    </div>
                </div>

            </div>

            <div class="form-group row">

                <div class="col-md-3">
                    <label for="Efetivacao"> Efetivado:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <input type="checkbox" id="Efetivacao" name='fEfetivacao' onclick="efetivacao(this.checked)">
                    </div>
                </div>

                <div class="col-md-5">
                    <label for="CodConta">Conta:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <select id="CodConta" name='fCodContaBancaria' class="form-control select2" style="width:95%" disabled>
                            <option value=''>Selecione</option>
                            <?php $sSelected = "";
                            if($voContaBancaria){
                                foreach($voContaBancaria as $oContaBancaria){
                                    ?>
                                    <option value='<?php echo $oContaBancaria->getCodContaBancaria()?>'><?php echo $oContaBancaria->getBanco(); ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <label for="Comprovante"> Comprovante:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <input class="form-control" type='file' id='Comprovante' name='fComprovante' required disabled>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="submit" class="btn btn-lg btn-success"><?php echo $sOP?></button>
        </div>

</form>


<script>
    //Função para confirmar pagamento
    $(function(){
        $('#formMovimento').submit(function(){
            var dados = new FormData(this);

            $.ajax({
                type: "POST",
                datatype: "json",
                url: '?action=Movimento.processaFormulario',
                data: dados,
                processData: false,
                cache: false,
                contentType: false,
                xhr: function(){
                    var xhr = new window.XMLHttpRequest();
                    // Handle progress
                    //Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total * 100;
                            //Do something with upload progress
                            $("#bodyMovimento").html('<img src="dist/img/load.gif" style="width: 150px" alt="">');
                            //$("#btnSalvar").attr("disabled",true);
                        }
                    }, false);
                    //Download progress
                    xhr.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total * 100;
                            //Do something with download progress
                        }
                    }, false);

                    return xhr;
                },
                error: function(oXMLRequest,sErrorType){
                    console.log(dados);
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status+' , '+sErrorType);
                },
                success: function(dados){
                    console.log(dados);
                    dados = jQuery.parseJSON(dados);
                    let sDivResposta = (dados.nTipo === "RECEITA") ? "idRespostaReceita" : "idRespostaDespesa";
                    let sDivBody = (dados.nTipo === "RECEITA") ? "divBodyReceita" : "divBodyDespesa";

                    $("#"+sDivResposta).html("<div class='alert alert-" + dados.sClass + "'><p>" + dados.sMsg + "</p></div>");
                    if (dados.bResultado === 1) {
                        $('#modalMovimento').modal("toggle");
                        $('#'+sDivBody).load("?action=Movimento.preparaListaAjax2&sOP=Visualizar&sTipo="+dados.nTipo);
                    }else{
                        $('#bodyMovimento').load("?action=Movimento.preparaFormulario&sOP=Alterar&sTipo="+dados.nTipo+"&nCodMovimento=" + dados.nCodMovimento);
                    }
                }
            });
            return false;
        });
    });

    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
        $('.date').mask('00/00/0000');
        $('.mesano').mask('00/0000');
    });

</script>