<?php
$sOP = $_REQUEST['sOP'];
$oMovimento = $_REQUEST['oMovimento'];
$voDespesaReceitaTipo = $_REQUEST['voDespesaReceitaTipo'];
?>

<form method="post" class="form-horizontal" name="formMovimento" action="?action=Movimento.processaFormulario" enctype="multipart/form-data">
    <input type="hidden" name="sOP" value="<?php echo $sOP; ?>" />
    <input type="hidden" name="fCodMovimento" value="<?=(is_object($oMovimento)) ? $oMovimento->getCodMovimento() : ""?>" />
    <div  class="box-body" id="form-group">
        <div class="form-group col-md-3 col-sm-6">
            <label for="CodDespesaReceitaTipo">Tipo de Lançamento:</label>
            <div class="input-group col-md-11 col-sm-12">
                <select id="CodDespesaReceitaTipo" name='fCodDespesaReceitaTipo' id="CodDespesaReceitaTipo" class="form-control select2"  required  >
                    <option value=''>Selecione</option>
                    <?php $sSelected = "";
                    if($voDespesaReceitaTipo){
                        foreach($voDespesaReceitaTipo as $oDespesaReceitaTipo){
                            if($oMovimento){
                                $sSelected = ($oMovimento->getCodDespesaReceitaTipo() == $oDespesaReceitaTipo->getCodDespesaReceitaTipo()) ? "selected" : "";
                            }
                            ?>
                            <option  <?php echo $sSelected?> value='<?php echo $oDespesaReceitaTipo->getCodDespesaReceitaTipo()?>'><?php echo $oDespesaReceitaTipo->getAbreviacao() . " - " . $oDespesaReceitaTipo->getDescricao()?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group col-md-3 col-sm-6">
            <label for="Descricao">Desrição:</label>
            <div class="input-group col-md-11 col-sm-12">
                <input class="form-control" type='text' id='Descricao' placeholder='Desrição' name='fDescricao'  required   value='<?php echo ($oMovimento) ? $oMovimento->getDescricao() : ""?>'/>
            </div>
        </div>
        <div class="form-group col-md-2 col-sm-4">
            <label for="DataEntrada">Data:</label>
            <div class="input-group col-md-11 col-sm-12">
                <input class="form-control" type='text' id='DataEntrada' placeholder='Data' name='fDataEntrada'  required   value='<?php echo ($oMovimento) ? $oMovimento->getDataEntradaFormatado() : ""?>'/>
            </div>
        </div>
        <div class="form-group col-md-2 col-sm-4">
            <label for="Valor">Valor:</label>
            <div class="input-group col-md-11 col-sm-12">
                <input class="form-control" type='text' id='Valor' placeholder='Valor' name='fValor'  onkeyup='FormataMoney(this);' required  value='<?php echo ($oMovimento) ? $oMovimento->getValorFormatado() : ""?>'/>
            </div>
        </div>

        <div class="form-group col-md-4 col-sm-8">
            <label for="Observacao">Observação:</label>
            <div class="input-group col-md-11 col-sm-12">
                <input class="form-control" type='text' id='Observacao' placeholder='Observação' name='fObservacao'  value='<?php echo ($oMovimento) ? $oMovimento->getObservacao() : ""?>'/>
            </div>
        </div>

        <div class="form-group  col-md-2 col-sm-6">
            <label for="Efetivacao"> Efetivado:</label>
            <div class="input-group col-md-11 col-sm-12">
                <!-- <input type="checkbox" id="Efetivacao" name='fEfetivacao' class="flat-red"> -->
                <input type="checkbox" id="Efetivacao" name='fEfetivacao'>
            </div>
        </div>

        <div class="form-group col-md-3">
            <label for="CodConta">Conta:</label>
            <div class="input-group col-md-11 col-sm-12">
                <select id="CodConta" disabled name='fCodContaBancaria' class="form-control select2">
                    <option value=''>Selecione</option>
                    <?php $sSelected = "";
                    if($voContaBancaria){
                        foreach($voContaBancaria as $oContaBancaria){
                            // if($oMovimento){
                            //   $sSelected = ($oMovimento->getCodDespesaReceitaTipo() == $oContaBancaria->getCodContaBancaria()) ? "selected" : "";
                            // }
                            ?>
                            <option  <?php //echo $sSelected?> value='<?php echo $oContaBancaria->getCodContaBancaria()?>'><?php echo $oContaBancaria->getCodColaborador() . " - " . $oContaBancaria->getValorFormatado()?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group col-md-4">
            <label for="Comprovante"> Comprovante:</label>
            <div class="input-group col-md-11 col-sm-12">
                <input class="form-control" type='file' id='Comprovante' name='fComprovante' disabled>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-sm-offset-5 col-sm-2">
                <button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</form>
