<?php
$sOP = $_REQUEST['sOP'];
$voMovimento = $_REQUEST['voMovimento'];
$voDespesaReceitaTipo = $_REQUEST['voDespesaReceitaTipo'];
$voContaBancaria = $_REQUEST['voContaBancaria'];
$voFormaPagamento = $_REQUEST['voFormaPagamento'];
$f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);

$nReceber = $_REQUEST['nReceber'];
$nRecebido = $_REQUEST['nRecebido'];
$nPagar = $_REQUEST['nPagar'];
$nPago = $_REQUEST['nPago'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Lançamentos</title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Movimento.preparaLista">Gerenciar Lançamentos</a>
                <li class="active">Lançamento - Gerenciar</li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title btn btn-block btn-primary btn-sm-1">Lançamento - PAGAMENTOS</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="divForm" style="text-align: center"></div>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lançamentos</h3>
                        </div>

                        <div class="box-body">

                            <form action="?action=Movimento.preparaLista" method="post">
                                <div class="form group row">
                                    <div class="col-md-3">
                                        <label for="EntradaSaida" class="control-label">Tipo:</label>
                                        <select class="form-control" name="fEntradaSaida" id="EntradaSaida">
                                            <option value="">Selecione</option>
                                            <option <?php echo ($_REQUEST['fEntradaSaida'] == 'ENTRADA') ? " selected " : ""?> value="ENTRADA">Entrada</option>
                                            <option <?php echo ($_REQUEST['fEntradaSaida'] == 'SAIDA') ? " selected " : ""?> value="SAIDA">Saída</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="Pagamento" class="control-label">Pagamento:</label>
                                        <select class="form-control" name="fPagamento" id="Pagamento">
                                            <option value="">Selecione</option>
                                            <option <?php echo ($_REQUEST['fPagamento'] == 'Efetivado') ? " selected " : ""?> value="Efetivado">Efetivado</option>
                                            <option <?php echo ($_REQUEST['fPagamento'] == 'Previsto') ? " selected " : ""?> value="Previsto">Previsto</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="DataInicio">Período:</label>
                                        <input type="text" name="periodo" class="form-control col-6" />

                                        <div class="input-group input-daterange">
                                            <input type="text" class="form-control datepicker" name="fDataInicio" id="DataInicio" data-provide="datepicker" data-dayNames=" ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado']"  data-date-format="dd/mm/yyyy" autocomplete="off">
                                            <div class="input-group-addon">até</div>
                                            <!--suppress HtmlFormInputWithoutLabel -->
                                            <input type="text" class="form-control datepicker" name="fDataFim" id="DataFim" data-provide="datepicker" data-date-format="dd/mm/yyyy" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-md-1">
                                        <label class="control-label">&nbsp;</label><br>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-filter fa-lg"></i> Filtrar</button>
                                    </div>
                                </div>

                            </form>
                            <br><hr>

                            <div class="row">
                                <div class="col-lg-3 col-xs-12">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3><?php echo number_format($nReceber,2,',','.');?></h3>
                                            <p>A Receber</p>
                                        </div>
                                        <div class="icon"><i class="fa fa-usd" aria-hidden="true"></i> </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xs-12">
                                    <!-- small box -->
                                    <div class="small-box bg-green">
                                        <div class="inner">
                                            <h3><?php echo number_format($nRecebido,2,',','.');?></h3>
                                            <p>Recebido</p>
                                        </div>
                                        <div class="icon"><i class="fa fa-usd" aria-hidden="true"></i> </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xs-12">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                            <h3><?php echo number_format($nPagar,2,',','.');?></h3>
                                            <p>A Pagar</p>
                                        </div>
                                        <div class="icon"><i class="fa fa-usd" aria-hidden="true"></i> </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-xs-12">
                                    <!-- small box -->
                                    <div class="small-box bg-red">
                                        <div class="inner">
                                            <h3><?php echo number_format($nPago,2,',','.');?></h3>
                                            <p>Pago</p>
                                        </div>
                                        <div class="icon"><i class="fa fa-usd" aria-hidden="true"></i> </div>
                                    </div>
                                </div>
                            </div>


                            <div class="table-responsive">
                                <?php if(is_array($voMovimento)){ ?>

                                    <div title="Mensagem" class="alert alert-primary alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <p><i class="icon fa fa-search"></i> <?php echo ($_REQUEST['sPesquisa']) ? $_REQUEST['sPesquisa'] : "Todos"; ?></p>
                                    </div>
                                    <?php
                                    $nSaldoPeriodo =0;
                                    ?>

                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Tipo</th>
<!--                                            <th>Descrição</th>-->
                                            <th>Data</th>
                                            <th>Detalhe</th>
                                            <th>Efetivação</th>
                                            <th>Valor</th>
                                            <th>Observação</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($voMovimento as $oMovimento){
                                            if($oMovimento->tipo == 'SAIDA'){
                                                $nSaldoPeriodo = $nSaldoPeriodo - $oMovimento->valor;
                                                $link = "<a class='btn btn-primary pull-center' href='?action=Movimento.preparaFormulario&sOP=Alterar&nIdMovimento=".$oMovimento->codigo."'><i class='fa fa-edit'></i> Alterar/Confirmar Lançamento </a>";
                                            }else{
                                                $nSaldoPeriodo = $nSaldoPeriodo + $oMovimento->valor;
                                                $link = "<button id='botaoConfirma".$oMovimento->codigo."' type='button' class='btn btn-success btn-sm' data-toggle='modal' data-codigo-confirmacao='".$oMovimento->codigo."' data-target='#modalConfirmacaoPagamento'> <i class='fa fa-check'></i> Confirmar Entrada</button>";
                                            }

                                            if(!$oMovimento->comprovante){
                                                $sComprovante = "<button id='botaoConfirma".$oMovimento->codigo."' type='button' class='btn btn-danger btn-sm' data-toggle='modal' data-codigo-confirmacao='".$oMovimento->codigo."' data-target='#modalConfirmacaoPagamento'> <i class='fa fa-upload'></i></button>";
                                            } else {
                                                $sComprovante = "<a class='btn btn-primary btn-sm' target='_blank' href='http://luanapalheta.com.br/admin/{$oMovimento->comprovante}'><i class='fa fa-folder-open' aria-hidden='true'></i></a>";
                                            } ?>
                                            <tr>

                                                <td><?php echo ($oMovimento->tipo == 'SAIDA') ? "<span class='btn btn-danger pull-center'><i class='fa fa-minus'></i></span>" : "<span class='btn btn-success pull-center'><i class='fa fa-plus'></i></span>"?></td>

<!--                                                <td>--><?php //echo $oMovimento->descricao?><!--</td>-->
                                                <td><?php echo $oMovimento->data?></td>
                                                <td><?php echo $oMovimento->detalhe?></td>
                                                <td><?php echo (!$oMovimento->data_efetivacao) ? $link : $oMovimento->data_efetivacao;?></td>
                                                <td><?php echo $f->format($oMovimento->valor)?></td>
                                                <td><?php echo $oMovimento->observacao?></td>
                                                <th style="white-space: nowrap">
                                                    <?php echo $sComprovante;?>
                                                    <button type='button' class='btn btn-success btn-sm' data-toggle='modal'> <i class='fa fa-edit'></i></button>
                                                    <button type='button' class='btn btn-danger btn-sm' data-toggle='modal'> <i class='fa fa-trash'></i></button>
                                                </th>

                                            </tr>
                                        <?php }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th><?php echo ($nSaldoPeriodo < 0) ? "<span class='btn btn-danger pull-center'><i class='fa fa-minus'></i></span>" : "<span class='btn btn-success pull-center'><i class='fa fa-plus'></i></span>" ?></th>
                                            <th>Descrição</th>
                                            <th>Data</th>
                                            <th>Detalhe</th>
                                            <th>Efetivação</th>
                                            <th><?php echo $f->format($nSaldoPeriodo);?></th>
                                            <th>Observação</th>
                                            <th></th>

                                        </tr>
                                        </tfoot>
                                    </table>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

    </div>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="modalConfirmacaoPagamento" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="?action=ProjetoPagamento.processaFormulario">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Conta Destino</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="">

                        <input type="hidden" name="sOP" value="ConfirmarPagamento">
                        <input type="hidden" id="CodProjetoPagamento" name="nCodProjetoPagamento" value="">
                        <input type="hidden" name="sLink" value="Movimento">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="ContaBancaria" class="control-label">Conta Destino</label>
                                <select class="form-control select2" name="fCodContaBancaria" id="ContaBancaria" style="width:100%" onchange="if(this.value !== '') $('#btnSalvar').prop('disabled',false);else $('#btnSalvar').prop('disabled',true);" >
                                    <option value="">Selecione</option>
                                    <?php foreach($voContaBancaria as $oContaBancaria){?>
                                        <option value="<?php echo $oContaBancaria->getCodContaBancaria()?>"><?php echo $oContaBancaria->getCodColaborador() . " - " . $oContaBancaria->getBanco() . " - " . $oContaBancaria->getAgencia() . " - " .$oContaBancaria->getConta() . " - " .$oContaBancaria->getValorFormatado()?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="submit" id="btnSalvar" class="btn btn-success" disabled>Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once('includes/footer.php')?>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>


<!--suppress HtmlUnknownTarget -->
<script>

    window.onload = function () {
        $("#divForm").html('<img src="dist/img/load.gif" alt="" style="width: 180px">').load("?action=Movimento.preparaFormulario&sOP=Cadastrar");
        $("body").addClass("sidebar-collapse");
    }

    $('#confirm-delete').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let sLink = button.data('link');
        let modal = $(this);
        modal.find('#ok').attr('href', sLink);
    });

    $('#Efetivacao').click(function() {
        $('#CodConta').attr('disabled',! this.checked);
        $('#Comprovante').attr('disabled',! this.checked);
    });

    $('#modalConfirmacaoPagamento').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let codigo = button.data('codigo-confirmacao');
        let modal = $(this);
        modal.find('#CodProjetoPagamento').val(codigo);

    });

    $(document).ready(function() {

        let table = $('.table').DataTable({
            dom: 'Bfrtip',
            search: {
                "smart": true
            },
            columnDefs: [
                {type: 'date-euro', targets: [1,3]}
            ],
            buttons: [
                {
                    extend: 'pdfHtml5',
                    className: 'btn btn-primary',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    title: 'Entregas'
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-primary',
                    title: 'Entregas'
                },
                {
                    extend: 'csv',
                    className: 'btn btn-primary',
                    title: "lista_entregas",
                    charset: 'utf-8',
                    bom: true
                },
                {
                    extend: 'print',
                    className: 'btn btn-primary',
                    text: 'Imprimir',
                },
                {
                    extend: 'colvis',
                    className: 'btn btn-primary',
                    text: 'Selecionar colunas'
                }
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json",
                "decimal": ",",
                "thousands": "."
            },
            "paging": false
        });
        table
            .order( [ 1, 'desc' ] )
            .draw();
    });

  $(document).ready(function() {

    $('input[name="periodo"]').daterangepicker({
      "locale": {
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "applyLabel": "Aplicar",
        "cancelLabel": "Cancelar",
        "daysOfWeek": [
          "Dom",
          "Seg",
          "Ter",
          "Qua",
          "Qui",
          "Sex",
          "Sab"
        ],
        "monthNames": [
          "Janeiro",
          "Fevereiro",
          "Março",
          "Abril",
          "Maio",
          "Junho",
          "Julho",
          "Agosto",
          "Setembro",
          "Outubro",
          "Novembro",
          "Dezembro"
        ],
        "firstDay": 1
      }
    });
  });

</script>

</body>
</html>
