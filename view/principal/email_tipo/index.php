<?php
 $voEmailTipo = $_REQUEST['voEmailTipo'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Tipos de Email </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Tipos de Email</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Tipo de Email</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formEmailTipo" id="formEmailTipo" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesEmailTipo" onChange="JavaScript: submeteForm('EmailTipo')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=EmailTipo.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Tipo de Email</option>
   						<option value="?action=EmailTipo.preparaFormulario&sOP=Alterar" lang="1">Alterar Tipo de Email selecionado</option>
   						<option value="?action=EmailTipo.preparaFormulario&sOP=Detalhar" lang="1">Gerenciar Tipo de Email selecionado</option>
   						<option value="?action=EmailTipoEtapa.preparaLista" lang="1">Gerenciar Entregas do Tipo de Email selecionado</option>
   						<option value="?action=EmailTipo.processaFormulario&sOP=Excluir" lang="2">Excluir Tipo de Email(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voEmailTipo)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('EmailTipo')"><i class="icon fa fa-check"></a></th>
   					<th width="9%">Id</th>
					  <th width="80%">Descrição</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voEmailTipo as $oEmailTipo){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('EmailTipo')" type="checkbox" value="<?php echo $oEmailTipo->getCodEmailTipo()?>" name="fIdEmailTipo[]"/></td>
  					<td><?php echo $oEmailTipo->getCodEmailTipo()?></td>
					<td><?php echo $oEmailTipo->getTitulo()?></td>


  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Id</th>
					<th>Título</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voEmailTipo)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
