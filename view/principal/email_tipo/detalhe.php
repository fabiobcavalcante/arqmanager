﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oServico = $_REQUEST['oServico'];
$voServicoDetalhe = $_REQUEST['voServicoDetalhe'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Serviço - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Servico.preparaLista">Gerenciar Serviços</a>
 			<li class="active">Serviço - <?php echo $sOP?></li>
 		</ol>
    <?php include_once('includes/mensagem.php')?>
     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Serviço - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="DescServico" class="control-label">Descrição:</label>
		<p><?php echo ($oServico) ? $oServico->getDescServico() : ""?></p>
	</div>
 <div class="col-md-4">

 <label for="Percentual" class="control-label">Empresa:</label>
		<p><?php echo ($oServico) ? $oServico->getPercentualEmpresaFormatado() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="IncluidoPor" class="control-label">Inserido por:</label>
		<p><?php echo ($oServico) ? $oServico->getInseridoPor() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oServico) ? $oServico->getAlteradoPor() : ""?></p>
	</div>

             <div class="box-body table-responsive">
 			<form method="post" action="" name="formServicoDetalhe" id="formServicoDetalhe" class="formulario">
                <input type="hidden" name="fCodServico" value="<?php echo ($oServico) ? $oServico->getCodServico() : ""?>">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesServicoDetalhe" onChange="JavaScript: submeteForm('ServicoDetalhe')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=ServicoDetalhe.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Detalhe do Serviço</option>
   						<option value="?action=ServicoDetalhe.preparaFormulario&sOP=Alterar" lang="1">Alterar Detalhe do Serviço selecionado</option>
<!--   						<option value="?action=ServicoDetalhe.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Detalhe do Serviço selecionado</option>-->
   						<option value="?action=ServicoDetalhe.processaFormulario&sOP=Excluir" lang="2">Excluir Detalhe do Serviço(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voServicoDetalhe)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('ServicoDetalhe')"><i class="icon fa fa-check"></a></th>
   					<th>Id</th>
					<th>Descrição</th>
					<th>Percentual</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voServicoDetalhe as $oServicoDetalhe){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('ServicoDetalhe')" type="checkbox" value="<?=$oServicoDetalhe->getCodServicoDetalhe()?>" name="fIdServicoDetalhe[]"/></td>
  					<td><?php echo $oServicoDetalhe->getCodServicoDetalhe()?></td>
					<td><?php echo $oServicoDetalhe->getDescServicoDetalhe()?></td>
					<td><?php echo $oServicoDetalhe->getPercentualFormatado()?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Id</th>
					<th>Descrição</th>
					<th>Percentual</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voServicoDetalhe)){?>
  			</form>
             </div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Servico.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
