<?php
 $voProjetoPagamento = $_REQUEST['voProjetoPagamento'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Pagamento do Projeto </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Pagamento do Projetos</li>
 		</ol>	  

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Pagamento do Projeto</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formProjetoPagamento" id="formProjetoPagamento" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesProjetoPagamento" onChange="JavaScript: submeteForm('ProjetoPagamento')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=ProjetoPagamento.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Pagamento do Projeto</option>
   						<option value="?action=ProjetoPagamento.preparaFormulario&sOP=Alterar" lang="1">Alterar Pagamento do Projeto selecionado</option>
   						<option value="?action=ProjetoPagamento.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Pagamento do Projeto selecionado</option>
   						<option value="?action=ProjetoPagamento.processaFormulario&sOP=Excluir" lang="2">Excluir Pagamento do Projeto(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voProjetoPagamento)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('ProjetoPagamento')"><i class="icon fa fa-check"></a></th>
   					<th>Código</th>
					<th>Projeto</th>
					<th>Forma</th>
					<th>Valor</th>
					<th>Previsão</th>
					<th>Efetivação</th>
					<th>Descrição</th>
					<th>Incluído por</th>
					<th>Alterado por</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voProjetoPagamento as $oProjetoPagamento){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('ProjetoPagamento')" type="checkbox" value="<?=$oProjetoPagamento->getCodProjetoPagamento()?>" name="fIdProjetoPagamento[]"/></td>
  					<td><?php echo $oProjetoPagamento->getCodProjetoPagamento()?></td>
					<td><?php echo $oProjetoPagamento->getCodProjeto()?></td>
					<td><?php echo $oProjetoPagamento->getCodPagamentoForma()?></td>
					<td><?php echo $oProjetoPagamento->getValorFormatado()?></td>
					<td><?php echo $oProjetoPagamento->getDataPrevisaoFormatado()?></td>
					<td><?php echo $oProjetoPagamento->getDataEfetivacaoFormatado()?></td>
					<td><?php echo $oProjetoPagamento->getDescPagamento()?></td>
					<td><?php echo $oProjetoPagamento->getIncluidoPor()?></td>
					<td><?php echo $oProjetoPagamento->getAlteradoPor()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Código</th>
					<th>Projeto</th>
					<th>Forma</th>
					<th>Valor</th>
					<th>Previsão</th>
					<th>Efetivação</th>
					<th>Descrição</th>
					<th>Incluído por</th>
					<th>Alterado por</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voProjetoPagamento)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
