﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oProjetoPagamento = $_REQUEST['oProjetoPagamento'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Pagamento do Projeto - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=ProjetoPagamento.preparaLista">Gerenciar Pagamento do Projetos</a>
 			<li class="active">Pagamento do Projeto - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Pagamento do Projeto - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="CodProjetoPagamento" class="control-label">Código:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getCodProjetoPagamento() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="CodProjeto" class="control-label">Projeto:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getCodProjeto() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="CodPagamentoForma" class="control-label">Forma:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getCodPagamentoForma() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Valor" class="control-label">Valor:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getValor() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DataPrevisao" class="control-label">Previsão:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getDataPrevisao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DataEfetivacao" class="control-label">Efetivação:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getDataEfetivacao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DescPagamento" class="control-label">Descrição:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getDescPagamento() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="IncluidoPor" class="control-label">Incluído por:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getIncluidoPor() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getAlteradoPor() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=ProjetoPagamento.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
