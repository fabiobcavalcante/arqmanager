<?php
$oProjetoPagamento = $_REQUEST['oProjetoPagamento'];
$voContaBancaria = $_REQUEST['voContaBancaria'];
$voFormaPagamento = $_REQUEST['voFormaPagamento'];

$f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
?>
<form method="post" action="?action=ProjetoPagamento.processaFormulario" id="formPagamento" enctype="multipart/form-data">
    <div class="modal-header">
        <h3 class="modal-title">Confirmação de Pagamento</h3>
    </div>

    <div class="modal-body" id="divPagamento">

        <input type="hidden" name="sOP" value="ConfirmarPagamento">
        <input type="hidden" name="sDestino" value="Financeiro">
        <input type="hidden" name="nCodProjetoPagamento" value="<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>">
        <input type="hidden" name="fValor" value="<?php echo $oProjetoPagamento->getValorFormatado();?>">
        <input type="hidden" name="fDataPrevisao" value="<?php echo $oProjetoPagamento->getDataPrevisaoFormatado();?>">
        <input type="hidden" name="fDescPagamento" value="<?php echo $oProjetoPagamento->getDescPagamento();?>">

        <div class="form-group row">
            <div class="col-md-12">
                <label for="ContaBancaria" class="control-label">Conta Destino</label>
                <select class="form-control select2" name="fCodContaBancaria" id="ContaBancaria" style="width:100%" onchange="if(this.value !== '') $('#btnSalvar').prop('disabled',false);else $('#btnSalvar').prop('disabled',true);" >
                    <option value="">Selecione</option>
                    <?php foreach($voContaBancaria as $oContaBancaria){?>
                        <option value="<?php echo $oContaBancaria->getCodContaBancaria()?>"><?php echo $oContaBancaria->getCodColaborador() . " - " . $oContaBancaria->getBanco() . " - " . $oContaBancaria->getAgencia() . " - " .$oContaBancaria->getConta() . " - " .$f->format($oContaBancaria->getAtivo())?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label for="CodFormaPagamento" class="control-label">Forma de Pagamento</label>
                <select class="form-control" name="fCodFormaPagamento" id="CodFormaPagamento" style="width:100%" onchange="if(this.value !== '') $('#btnSalvar').prop('disabled',false);else $('#btnSalvar').prop('disabled',true);" >
                    <option value="">Selecione</option>
                    <?php foreach($voFormaPagamento as $oFormaPagamento){?>
                        <option value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label for="DataConfirmacao" class="control-label">Data Confirmação</label>
                <input class="form-control date" type='text' id='DataConfirmacao' placeholder='Data Confirmação' name='fDataConfirmacao' required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label for="ContaBancaria" class="control-label">Comprovante</label>
                <input class="form-control" type='file' id='Comprovante' name='fComprovante'>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        <button type="submit" id="btnSalvar" class="btn btn-success" disabled>Salvar</button>
    </div>
</form>


<script>
    //Função para confirmar pagamento
    $(function(){
        $('#formPagamento').submit(function(){
            var dados = new FormData(this);

            $.ajax({
                type: "POST",
                datatype: "json",
                url: '?action=ProjetoPagamento.processaFormulario',
                data: dados,
                processData: false,
                cache: false,
                contentType: false,
                xhr: function(){
                    var xhr = new window.XMLHttpRequest();
                    // Handle progress
                    //Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total * 100;
                            //Do something with upload progress
                            $("#divPagamento").html('<img src="dist/img/load.gif" style="width: 150px" alt="">');
                            $("#btnSalvar").attr("disabled",true);
                        }
                    }, false);
                    //Download progress
                    xhr.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total * 100;
                            //Do something with download progress
                        }
                    }, false);

                    return xhr;
                },
                error: function(oXMLRequest,sErrorType){
                    console.log(form.attr('action'));
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status+' , '+sErrorType);
                },
                success: function(dados){
                    // console.log(dados);
                    dados = jQuery.parseJSON(dados);
                    $('#modalConfirmacaoPagamento').modal("toggle");
                    $('#divBodyReceita').load("?action=Projeto.preparaListaAjax2&sOP=Cadastrar");
                    $("#idRespostaReceita").html("<div class='" + dados.sClass + "'><p>" + dados.sMsg + "</p></div>");
                }
            });
            return false;
        });
    });


		$(function () {
				$('.date').mask('00/00/0000');
		});




</script>
