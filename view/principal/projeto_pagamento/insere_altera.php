<?php
$sOP = $_REQUEST['sOP'];
$oProjetoPagamento = $_REQUEST['oProjetoPagamento'];

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Pagamento do Projeto - <?php echo $sOP ?></title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=ProjetoPagamento.preparaLista">Gerenciar Pagamento do Projetos</a>
                <li class="active">Pagamento do Projeto - <?php echo $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Pagamento do Projeto - <?php echo $sOP ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <form method="post" class="form-horizontal" name="formProjetoPagamento" action="?action=ProjetoPagamento.processaFormulario">
                            <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
                            <input type="hidden" name="fCodProjetoPagamento" value="<?=(is_object($oProjetoPagamento)) ? $oProjetoPagamento->getCodProjetoPagamento() : ""?>" />
                            <div  class="box-body" id="form-group">

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="CodProjetoPagamento">Código:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='CodProjetoPagamento' placeholder='Código' name='fCodProjetoPagamento'  required  onKeyPress="TodosNumero(event);" value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getCodProjetoPagamento() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="CodProjeto">Projeto:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='CodProjeto' placeholder='Projeto' name='fCodProjeto'  required  onKeyPress="TodosNumero(event);" value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getCodProjeto() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="CodPagamentoForma">Forma:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='CodPagamentoForma' placeholder='Forma' name='fCodPagamentoForma'  required  onKeyPress="TodosNumero(event);" value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getCodPagamentoForma() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="Valor">Valor:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='Valor' placeholder='Valor' name='fValor'  required   value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getValorFormatado() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="DataPrevisao">Previsão:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='DataPrevisao' placeholder='Previsão' name='fDataPrevisao'  required   value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getDataPrevisaoFormatado() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="DataEfetivacao">Efetivação:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='DataEfetivacao' placeholder='Efetivação' name='fDataEfetivacao'  required   value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getDataEfetivacaoFormatado() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="DescPagamento">Descrição:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='DescPagamento' placeholder='Descrição' name='fDescPagamento'  required   value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getDescPagamento() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="IncluidoPor">Incluído por:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='IncluidoPor' placeholder='Incluído por' name='fIncluidoPor'  required   value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getIncluidoPor() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="AlteradoPor">Alterado por:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='AlteradoPor' placeholder='Alterado por' name='fAlteradoPor'  required   value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getAlteradoPor() : ""?>'/>
                                    </div>
                                </div>
                                <input type='hidden' name='fAtivo' value='<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getAtivo() : "1"?>'/>


                                <div class="form-group col-md-12 col-sm-12">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script type="text/javascript" language="javascript">

    jQuery(function($){
        $("#Valor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
        $("#DataPrevisao").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
        $("#DataEfetivacao").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
    });
</script>
</body>
</html>
