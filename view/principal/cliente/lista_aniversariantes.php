﻿<?php
 $sOP = $_REQUEST['sOP'];
 $voMeses = $_REQUEST['voMeses'];
 $voAniversariantes = $_REQUEST['voAniversariantes'];
 $nMesSelecionado = $_REQUEST['nMesSelecionado'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Aniversariantes do mês</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Listar Aniversariantes do Mês</li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">

         </div>
         <div class="box-body">
           <ul class="nav nav-tabs">
             <?php foreach($voMeses as $oMes){?>
               <li <?php echo ($oMes->mes == $nMesSelecionado ? " class='active'" : "")?>><a href="?action=Cliente.listaAniversariantes&sOP=Visualizar&nMesSelecionado=<?php echo $oMes->mes?>"><?php echo $oMes->mes_descricao?></a></li>
          <?php } ?>
          </ul>
          <div class="col-md-7" style="padding:5px">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-birthday-cake" aria-hidden="true"></i> Lista de Aniversariantes</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <?php if($voAniversariantes){?>
                <tr>
                  <th>DIA</th>
                  <th>PESSOA</th>
                  <th>TIPO</th>
                  <th>STATUS</th>
                </tr>
                <?php foreach($voAniversariantes as $oAniversariante){?>
                  <tr>
                    <td><?php echo $oAniversariante->getAlteradoPor()?></td>
                    <td><?php echo $oAniversariante->getNome()?></td>
                    <td><?php echo $oAniversariante->getAtivo()?></td>
                    <td><?php echo ($oAniversariante->getObservacao()) ? '<i class="fa fa-envelope" aria-hidden="true"></i> '. $oAniversariante->getObservacao() :"-"?></td>
                  </tr>

                <?php } ?>
                <?php }else{ ?>
                <tr>
                  <td colspan="4">Não há aniversariantes no mês informado</td>
                </tr>
              <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
          <div class="form-group col-md-12">
              <div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?">Voltar</a></div>
          </div>
        </div>

 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
