<?php
$sOP = $_REQUEST['sOP'];
$oCliente = $_REQUEST['oCliente'];
$voIndicacao = $_REQUEST['voIndicacao'];

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title> Cliente - <?php echo $sOP ?></title>
    <?php include_once('includes/head.php')?>

</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Cliente.preparaLista">Gerenciar Clientes</a>
                <li class="active">Cliente - <?php echo $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
            <div id="idResposta"></div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Cliente - <?php echo $sOP ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <form method="post" class="form-horizontal" name="formCliente" id="formCliente" action="?action=Cliente.processaFormulario" onsubmit="return validaForm()">
                            <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
                            <input type="hidden" name="fCodCliente" value="<?php echo (is_object($oCliente)) ? $oCliente->getCodCliente() : ""?>" />
                            <input type="hidden" name="fHeader" value="?action=Cliente.preparaLista">
                            <div  class="box-body" id="form-group">

                                <div class="form-group col-md-2 col-sm-12">
                                    <label for="CodTipoPessoa">Tipo Pessoa:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <select name='fCodTipoPessoa' id="CodTipoPessoa"  class="form-control CodTipoPessoa"  required >
                                            <option>Selecione</option>
                                            <option <?php echo (is_object($oCliente) && $oCliente->getCodTipoPessoa()=='1') ? " selected ": " "?> value='1'>Física</option>
                                            <option <?php echo (is_object($oCliente) && $oCliente->getCodTipoPessoa()=='2') ? " selected ": " "?> value='2'>Jurídica</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-2 col-sm-12">
                                    <label for="Identificacao">CPF/CNPJ:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='Identificacao' name='fIdentificacao' value='<?php echo ($oCliente) ? $oCliente->getIdentificacao() : ""?>' onblur="validaDocumento()" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Nome">Nome/Fantasia:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control maiuscula" type='text' id='Nome' placeholder='Nome/Fantasia' name='fNome' value='<?php echo ($oCliente) ? $oCliente->getNome() : ""?>' required>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Email">Email:</label>
                                    <div class="input-group col-md-12 col-sm-12">
                                        <input class="form-control minuscula" type='text' id='Email' placeholder='Email' name='fEmail' value='<?php echo ($oCliente) ? $oCliente->getEmail() : ""?>' onblur="validaEmail()" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-12">
                                    <label for="Cep">CEP:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='cep' placeholder='CEP' name='fCep'  required   value='<?php echo ($oCliente) ? $oCliente->getCep() : ""?>'/>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-12">
                                    <label for="Uf">Estado:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='uf' placeholder='Estado' name='fUf'  required   value='<?php echo ($oCliente) ? $oCliente->getUf() : ""?>'/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Cidade">Cidade:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='cidade' placeholder='Cidade' name='fCidade'  required   value='<?php echo ($oCliente) ? $oCliente->getCidade() : ""?>'/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Bairro">Bairro:</label>
                                    <div class="input-group col-md-12 col-sm-12">
                                        <input class="form-control" type='text' id='bairro' placeholder='Bairro' name='fBairro'  required   value='<?php echo ($oCliente) ? $oCliente->getBairro() : ""?>'/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Logradouro">Logradouro:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='rua' placeholder='Logradouro' name='fLogradouro'  required   value='<?php echo ($oCliente) ? $oCliente->getLogradouro() : ""?>'/>
                                    </div>
                                </div>
                                <div class="form-group col-md-1 col-sm-12">
                                    <label for="Numero">Nº:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='Numero' placeholder='Número' name='fNumero'  required   value='<?php echo ($oCliente) ? $oCliente->getNumero() : ""?>'/>
                                    </div>
                                </div>
                                <div class="form-group col-md-3 col-sm-12">
                                    <label for="Complemento">Complemento:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='Complemento' placeholder='Complemento' name='fComplemento' value='<?php echo ($oCliente) ? $oCliente->getComplemento() : ""?>'/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Telefones">Fones:</label>
                                    <div class="input-group col-md-12 col-sm-12">
                                        <input class="form-control" type='text' id='Telefones' placeholder='Fones' name='fTelefones'  required   value='<?php echo ($oCliente) ? $oCliente->getTelefones() : ""?>'/>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-12">
                                    <label for="DataNascimento">Data Nascimento:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control date" type='text' id='DataNascimento' placeholder='Data Nascimento' name='fDataNascimento'  required   value='<?php echo ($oCliente) ? $oCliente->getDataNascimentoFormatado() : ""?>' <?php echo(is_object($oCliente) && ($oCliente->getCodTipoPessoa() !='1')) ? " disabled ": " "?>/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="RazaoSocial">Razão Social:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='RazaoSocial' placeholder='Razão Social' name='fRazaoSocial'  required   value='<?php echo ($oCliente) ? $oCliente->getRazaoSocial() : ""?>' <?php echo(is_object($oCliente) && ($oCliente->getCodTipoPessoa() =='1')) ? " disabled ": " "?>/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="InscricaoEstadual">Insc. Estadual:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='InscricaoEstadual' placeholder='Insc. Estadual' name='fInscricaoEstadual'  value='<?php echo ($oCliente) ? $oCliente->getInscricaoEstadual() : ""?>' <?php echo(is_object($oCliente) && ($oCliente->getCodTipoPessoa() =='1')) ? " disabled ": " "?>/>
                                    </div>
                                </div>
                                <div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="InscricaoMunicipal">Insc Munic.:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='InscricaoMunicipal' placeholder='Insc Munic.' name='fInscricaoMunicipal'  value='<?php echo ($oCliente) ? $oCliente->getInscricaoMunicipal() : ""?>' <?php echo(is_object($oCliente) && ($oCliente->getCodTipoPessoa() =='1')) ? " disabled ": " "?>  />
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Indicacao">Indicação:</label>
                                    <select name='fIndicadoPor' id="divIndicado"  class="form-control select2"  required  >
                                        <option value=''>Selecione</option>
                                        <?php $sSelected = "";
                                        if($voIndicacao){
                                            foreach($voIndicacao as $oIndicacao){
                                                if($oCliente){

                                                    $sSelected = ($oCliente->getIndicadoPor() == $oIndicacao->getCodIndicacao()) ? "selected" : "";
                                                }
                                                ?>
                                                <option  <?php echo $sSelected?> value='<?php echo $oIndicacao->getCodIndicacao()?>'><?php echo $oIndicacao->getDescricao()?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-1 col-sm-12">
                                    <div class="">
                                        <span class="input-group-btn">
                                        <div style="margin: 25px;"></div>
                                        <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modalIndicacao" data-cliente="<?php echo ($oCliente ? $oCliente->getCodCliente() : "")?>"><i class="fa fa-plus"></i> Novo</button>
                                        </span>
                                    </div>
                                </div>
                                    <?php if($sOP == 'Cadastrar'){?>
                                <div class="col-sm-3" style="padding-top: 15px">
                                    <div class="form-group col-md-12 col-sm-12 radio" >

                                            <div class="checkbox">
                                                <label class="">
                                                    <input name="sEnviarEmail" type="checkbox" value="S">
                                                    <strong>Cliente novo?</strong>
                                                </label>
                                            </div>
                                        </div>

                                </div>
                                    <?php } ?>
                                <div class="form-group col-md-9 col-sm-9">
                                    <label for="Observacao">Observação:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='Observacao' placeholder='Observação' name='fObservacao' value='<?php echo ($oCliente) ? $oCliente->getObservacao() : ""?>'/>
                                    </div>
                                 </div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <button type="submit" id="botao" class="btn btn-lg btn-success"><?php echo $sOP?></button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>



                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Indicacao -->
    <div class="modal fade" id="modalIndicacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <header class="modal-header">
                    <h2 class="modal-title" id="divTitulo">Indicação</h2>
                </header>
                <div class="modal-body" id="modalBody">
                    <input type="hidden" value="<?php ($oCliente) ? $oCliente->getCodCliente() : ""?>" name="fCodCliente" id="CodCliente">
                    <div class="form-group row">

                        <div class="col-md-12">
                            <label class="control-label" for="Indicacao">Indicação</label>
                            <input type="text" class="form-control" name="fDescricao" id="Descricao" required>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
                <footer class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="gravaIndicacao()">Salvar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </footer>
            </div>
        </div>
    </div>

    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>

<script type="text/javascript">
    function gravaIndicacao(){
        let nCodCliente = $("#CodCliente").val();
        let sDescricao = $("#Descricao").val();
        let sUrl = '?action=Indicacao.processaFormulario&sOP=Cadastrar&nOrigem=1&fDescricao=' + sDescricao;

        if(nCodCliente){
            let sUrl = '?action=Indicacao.processaFormulario&sOP=Alterar&nOrigem=1&fCodCliente=' + nCodCliente + "&fDescricao=" + sDescricao;
        }

        $.ajax({

            dataType: "json",
            type: "GET",
            url: sUrl,
            beforeSend: function(){
                $("#modalBody").html('<img src="dist/img/load.gif" style="width: 150px" alt="">');
            },
            error: function(oXMLRequest,sErrorType){
                console.log(sUrl);
                console.log(oXMLRequest.responseText);
                console.log(oXMLRequest.status + ', ' + sErrorType);
            },
            success: function(dados){
                    // console.log(dados, dados.bErro);
                    $('#modalIndicacao').find("button[data-dismiss = 'modal']").click();
                    // console.log($('#modalIndicacao').find("button [data-dismiss = 'modal']"))
                    // $('#modalIndicacao').modal("hide");

                    $("#idResposta").html("<div class='"+dados.bErro+"'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><p><i class='"+dados.sIcone+"'></i>"+dados.sMsg+"</p></div>");
                 
            }
        });


    }

    $('#modalIndicacao',0,0).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget,0,0);
        let nCodCliente = button.data('cliente');
        $("#CodCliente").val(nCodCliente);
    });

    $(document).ready(function() {
        $("#cep").mask("99.999-999",{ 'placeholder': '99.999-999' });

        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep.
            $("#rua").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#uf").val("");
        }

        //Quando o campo cep perde o foco.
        $("#cep").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#rua").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#uf").val("...");


                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#rua").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#uf").val(dados.uf);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        });
    });

    jQuery(document).ready(function(){jQuery(".select2").data("placeholder","Selecione").select2(); });

    <?php if($sOP == 'Alterar'){ ?>
    $('.CodTipoPessoa option:not(:selected)').attr('disabled', true); // Desabilita todas as opções que não são a selecionada. $('.CodTipoPessoa').attr('readonly', true);
    <?php } ?>

    $('#CodTipoPessoa').change(function(){
        if($(this).val() === '1'){
            $("#RazaoSocial").prop('disabled', true).prop('required', false);
            $("#InscricaoEstadual").prop('disabled', true).prop('required', false);
            $("#InscricaoMunicipal").prop('disabled', true).prop('required', false);
            $("#DataNascimento").prop('disabled', false).prop('required', true);
            $('#Identificacao').mask('000.000.000-00', {reverse: true});
            $("label[for = Identificacao]").text("CPF");
        }else{
            $("#RazaoSocial").prop('disabled', false);
            $("#RazaoSocial").prop('required', true);
            $("#InscricaoEstadual").prop('disabled', false);
            $("#InscricaoEstadual").prop('required', false);
            $("#InscricaoMunicipal").prop('disabled', false);
            $("#InscricaoMunicipal").prop('required', false);
            $('#Identificacao').mask('00.000.000/0000-00', {reverse: true});
            $("label[for = Identificacao]").text("CNPJ");
        }
    });

    function validaForm(){
        return validaEmail() && validaDocumento();
    }

    function validaDocumento(){
        let bRetorno;
        let sIdentificacao = $('#Identificacao').val();
        let nCodTipoPessoa = $('#CodTipoPessoa').val();

        if(nCodTipoPessoa === '1')
            bRetorno = valida_cpf(sIdentificacao);
        else
            bRetorno = validar_CNPJ(sIdentificacao);

        if (!bRetorno) {
            chamarModal('Identificacao', "CPF/CNPJ inválido");
            $('#botao').attr('disabled', true);
            return false;
        } else {
            $("#Identificacao").css('background-color','');
            $('#botao').attr('disabled', false);
            return true;
        }
    }

    function validaEmail(){
        let sEmail = $('#Email').val();
        let bRetorno = validacaoEmail(sEmail);

        if (!bRetorno) {
            chamarModal('Email', "Email inválido");
            $('#botao').attr('disabled', true);
            return false;
        } else {
            $("#Email").css('background-color','');
            $('#botao').attr('disabled', false);
            return true;
        }
    }

</script>
</body>
</html>
