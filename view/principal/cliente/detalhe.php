﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oCliente = $_REQUEST['oCliente'];
 $voProjeto = $_REQUEST['voProjeto'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Cliente - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Cliente.preparaLista">Gerenciar Clientes</a>
 			<li class="active">Cliente - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Cliente - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-3">

 <label for="Nome" class="control-label">Nome:</label>
		<p><?php echo ($oCliente) ? $oCliente->getNome() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="Tipo" class="control-label">Tipo:</label>
		<p><?php echo ($oCliente) ? $oCliente->getCodTipoPessoaFormatada() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="Aniversario" class="control-label">Data Nascimento:</label>
		<p><?php echo ($oCliente) ? $oCliente->getDataNascimentoFormatado() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="CodIndicacao" class="control-label">Indicado por:</label>
		<p><?php echo ($oCliente) ? $oCliente->getIndicacao()->getDescricao() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="Endereco" class="control-label">Endereço:</label>
		<p><?php echo ($oCliente) ? $oCliente->getLogradouro() . ", Nº " .$oCliente->getNumero()  : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="Complemento" class="control-label">Complemento:</label>
		<p><?php echo ($oCliente) ? $oCliente->getComplemento() : " - "?></p>
	</div>

 <div class="col-md-2">

 <label for="Bairro" class="control-label">Bairro:</label>
		<p><?php echo ($oCliente) ? $oCliente->getBairro() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="Cidade" class="control-label">Cidade:</label>
		<p><?php echo ($oCliente) ? $oCliente->getCidade() : ""?></p>
	</div>

 <div class="col-md-1">

 <label for="Uf" class="control-label">UF:</label>
		<p><?php echo ($oCliente) ? $oCliente->getUf() : ""?></p>
	</div>

 <div class="col-md-1">

 <label for="Cep" class="control-label">CEP:</label>
		<p><?php echo ($oCliente) ? $oCliente->getCep() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="Telefone" class="control-label">Telefones:</label>
		<p><?php echo ($oCliente) ? $oCliente->getTelefones() : ""?></p>
	</div>


 <div class="col-md-2">

 <label for="Cpf" class="control-label">CPF/CNPJ:</label>
		<p><?php echo ($oCliente) ? $oCliente->getIdentificacao() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="Email" class="control-label">Email:</label>
		<p><?php echo ($oCliente) ? $oCliente->getEmail() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="InseridoPor" class="control-label">Inserido por:</label>
		<p><?php echo ($oCliente) ? $oCliente->getIncluidoPor() : ""?></p>
	</div>

 <div class="col-md-2">
 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oCliente) ? $oCliente->getAlteradoPor() : " - "?></p>
	</div>

 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
           <?php if($voProjeto){?>
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title">Projetos de <?php echo ($oCliente) ? $oCliente->getNome() : ""?></h3>
             </div>
             <div class="box-body">

               <div class='row'></div>
               <?php if(is_array($voProjeto)){?>
                 <table id="lista" class="table table-bordered table-striped">
                   <thead>
                   <tr>

                     <th>Serviço</th>
                 <th>Descrição</th>
                 <th>Início</th>
                 <th>Previsão Fim</th>
                 <th>Fim</th>


                   </tr>
                   </thead>
                   <tbody>
                           <?php foreach($voProjeto as $oProjeto){ ?>
                   <tr>

                     <td><?php echo $oProjeto->getCodServico()?></td>
           					<td><a href="?action=Projeto.preparaFormulario&sOP=Detalhar&nIdProjeto=<?php echo $oProjeto->getCodProjeto()?>"><?php echo $oProjeto->getDescricao()?></a></td>
           					<td><?php echo $oProjeto->getDataInicioFormatado()?></td>
           					<td><?php echo $oProjeto->getDataPrevisaoFimFormatado()?></td>
           					<td><?php echo $oProjeto->getDataPrevisaoFimFormatado2()?> </td>

                 </tr>
                 <?php }?>
                 </tbody>
                   <tfoot>
                         <tr>

                           <th>Serviço</th>
                       <th>Descrição</th>
                       <th>Início</th>
                       <th>Previsão Fim</th>
                       <th>Fim</th>
                         </tr>
                         </tfoot>
                       </table>
               <?php }//if(count($voCliente)){?>


        <div class="form-group col-md-12">
            <div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Cliente.preparaLista">Voltar</a></div>
        </div>
         </div>
             <!-- /.box-body -->
             </div>
               <!-- /.box -->
             <?php }//if($voProjeto){?>
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
