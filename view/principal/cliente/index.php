<?php
$voCliente = $_REQUEST['voCliente'];
$oPrincipal = $_REQUEST['oPrincipal'];

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Lista de Clientes </title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li class="active">Gerenciar Clientes</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-teal"><i class="fa fa-trophy"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Ranking Origem</span>
                            <a href="?action=Indicacao.preparaLista&sOP=Ranking">
                                <span class="info-box-number"><?php echo $oPrincipal->ranking_n_indicacao?></span>
                            </a>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-orange"><i class="fa fa-calendar"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Aniversariantes do Mês</span>
                            <span class="info-box-number">
               <a href="?action=Cliente.listaAniversariantes&sOP=Visualizar">
             <?php echo $oPrincipal->aniversariantes?></span>
                            </a>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-trophy"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Ranking de Projetos</span>
                            <span class="info-box-number">
              <?php $vRanking = explode(" - ", $oPrincipal->top_projetos);
              echo $vRanking[0]?>
            </span>
                            <small><?php echo $vRanking[1]?></small>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Cliente</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <form method="post" action="" name="formCliente" id="formCliente" class="formulario">

                                    <div class='form-group col-md-4'>
                                        <a href="?action=Cliente.preparaFormulario&sOP=Cadastrar" class="btn btn-success"><i class="fa fa-file-o" data-toggle="tooltip" data-placement="top" title="Novo Cliente"></i> Novo Cliente</a>
                                    </div>
                                <div class='row'></div>
                                <?php if(is_array($voCliente)){?>
                                    <table id="lista" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">Ações</th>
                                            <th class="col-md-3">Nome</th>
                                            <th class="col-md-1">Identificação</th>
                                            <th class="col-md-1">Aniversário</th>
                                            <th class="col-md-2">Telefones</th>
                                            <th class="col-md-2">Indicação</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($voCliente as $oCliente){ ?>
                                            <tr>
                                                <td style="white-space: nowrap;vertical-align: middle">

                                                    <!-- Single button -->
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Ações<span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="?action=Cliente.preparaFormulario&sOP=Detalhar&nIdCliente=<?php echo $oCliente->getCodCliente()?>"><i class="fa fa-search"></i> Detalhar Cliente</a></li>
                                                            <li><a href="?action=Cliente.preparaFormulario&sOP=Alterar&nIdCliente=<?php echo $oCliente->getCodCliente()?>"><i class="fa fa-edit"></i> Alterar Cliente</a></li>
                                                            <li><a href="" data-toggle="modal" data-target="#confirm-delete" onclick="modalConfirmDelete(<?php echo $oCliente->getCodCliente(); ?>)"><i class="fa fa-close"></i> Excluir Cliente</a></li>
                                                        </ul>
                                                    </div>

                                                </td>
                                                <td><?php echo $oCliente->getNome()?></td>
                                                <td align="center"><?php echo $oCliente->getIdentificacao()?></td>
                                                <td align="center"><?php echo $oCliente->getDataAniversario()?></td>
                                                <td><?php echo $oCliente->getTelefones()?></td>
                                                <td><?php echo $oCliente->getIndicadoPor()?></td>
                                            </tr>
                                        <?php }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Identificação</th>
                                            <th>Aniversário</th>
                                            <th>Telefones</th>
                                            <th>Indicação</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                <?php }//if(count($voCliente)){?>
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
</body>
</html>
