﻿<?php

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Sobre o Sistema</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Sobre o Sisitema</li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">

         </div>
         <div class="box-body">
          <div class="col-md-7" style="padding:5px">
          <div>
            <div class="box-header">
              <h3 class="box-title">Sobre o Sistema</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

							<div class="form-group col-md-8" style="text-align:justify;">Este sistema foi desenvolvido para auxiliar escritórios de arquitetura e interior e arquitetos para ter maior controle do escritório, tarefas, gastos, orçamentos, clientes e projetos dando maior gerenciamento de atividades.
							Idealizado inicialmente para auxiliar o escritório Luana Palheta Arquitetura, porém com as facilidades na gestão do mesmo foi gerando curiosidade e interesse de outros escritórios e fomos ampliando o leque de Funcionalidades.
						 </div>

            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
          <div class="form-group col-md-12">
              <div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?">Voltar</a></div>
          </div>
        </div>

 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
