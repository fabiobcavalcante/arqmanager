<?php
 $voContaMovimentacao = $_REQUEST['voContaMovimentacao'];
 $sDescricao = $_REQUEST['sDescricao'];
 $f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Conta Movimentação </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - CONSULTAS / RELATÓRIOS</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active"><?php echo $sDescricao;?></li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title"><?php echo $sDescricao;?></h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">

 			<div class='row'></div>
 			<?php if(is_array($voContaMovimentacao)){ $nTotal=0;
			?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th>Data</th>
					<th>Conta</th>
					<th>Descrição</th>
					<th>Valor</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voContaMovimentacao as $oContaMovimentacao){
					if($oContaMovimentacao->getTipo() == 'S'){
						$nTotal = $nTotal + $oContaMovimentacao->getValorDespesa();
					}else{
						$nTotal = $nTotal + $oContaMovimentacao->getValorReceita();
					}
				   ?>
   				<tr>
					<td><?php echo $oContaMovimentacao->getDataMovimentacaoFormatado()?></td>
					<td><?php echo $oContaMovimentacao->getAtivo()?></td>
					<td><?php echo $oContaMovimentacao->getDescricao()?></td>
					<td><?php echo ($oContaMovimentacao->getTipo() == 'S') ? "<i class='fa fa-minus-circle' aria-hidden='true'></i> R$" . $oContaMovimentacao->getValorDespesaFormatado() : "<i class='fa fa-plus-circle' aria-hidden='true'></i> R$" . $oContaMovimentacao->getValorReceitaFormatado() ?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
					<th></th>
					<th></th>

					<th>TOTAL</th>
					<th><?php echo $f->format($nTotal)?></th>
                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voContaMovimentacao)){?>

             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
