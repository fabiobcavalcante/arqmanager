﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oContaMovimentacao = $_REQUEST['oContaMovimentacao'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Conta Bancária - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=ContaMovimentacao.preparaLista">Gerenciar Conta Bancárias</a>
 			<li class="active">Conta Bancária - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Conta Bancária - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="CodColaborador" class="control-label">Cod_colaborador:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getCodColaborador() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Agencia" class="control-label">Agencia:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getAgencia() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="AgenciaDv" class="control-label">- Digito Verificador Agencia:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getAgenciaDv() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Conta" class="control-label">- Conta:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getConta() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="ContaDv" class="control-label">- Digito Verificador Conta:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getContaDv() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Tipo" class="control-label">- Tipo:
0 - Conta Corrente
1 - Conta Salario
2 - Conta Poupanca
3 - Outra Contas:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getTipo() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Banco" class="control-label">- Banco:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getBanco() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="IncluidoPor" class="control-label">- Inclusao:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getIncluidoPor() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="AlteradoPor" class="control-label">- Alteracao:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getAlteradoPor() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DataEncerramento" class="control-label">Data Encerramento:</label>
		<p><?php echo ($oContaMovimentacao) ? $oContaMovimentacao->getDataEncerramento() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=ContaMovimentacao.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
