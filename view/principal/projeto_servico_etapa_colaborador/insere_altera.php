<?php
$voColaborador = $_REQUEST['voColaborador'];
$vColaboradorEtapa = $_REQUEST['vColaboradorEtapa'];
?>
<form action="?action=ProjetoServicoEtapaColaborador.processaFormulario" method="post" id="formColaborador">
    <header class="modal-header">
        <h2 class="modal-title" id="divTitulo">Envolvidos</h2>
    </header>
    <div class="modal-body" id="envBody">
        <input type="hidden" value="Alterar" name="sOP">
        <input type="hidden" name="fCodEtapa" id="CodEtapa" value="<?php echo $_GET['CodEtapa'] ?>">
        <input type="hidden" value="<?php echo $_GET['CodProjeto'] ?>" name="fCodProjeto" id="CodProjeto">
        <input type="hidden" value="" name="fPercentual" id="Percentual">

        <div class="form-group row">
            <div class="col-md-12">
                <label class="control-label" for="Pessoas">Colaboradores</label>
                <?php if($voColaborador){?>
                    <select class="form-control select2" name="fColaboradores[]" id="Colaboradores" multiple="multiple" data-placeholder="Colaboradores" style="width: 100%;">
                        <?php foreach($voColaborador as $oColaborador){
                            $sSelected = "";
                            if (in_array($oColaborador->getCodColaborador(),$vColaboradorEtapa))
                                $sSelected = "selected";
                            ?>
                            <option value="<?php echo $oColaborador->getCodColaborador()?>" <?php echo $sSelected; ?>><?php echo $oColaborador->getLogin()?></option>
                        <?php } ?>
                    </select>
                <?php } ?>
            </div>
        </div>

    </div>
    <!-- /.box-body -->
    <footer class="modal-footer">
        <button type="submit" class="btn btn-success">Salvar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </footer>
</form>


<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    });
</script>
