<?php
 $sOP = $_REQUEST['sOP'];
 $oDespesaReceitaTipo = $_REQUEST['oDespesaReceitaTipo'];

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Tipo de Despesa X Receita - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=DespesaReceitaTipo.preparaLista">Gerenciar Tipo de Despesa X Receitas</a>
 			<li class="active">Tipo de Despesa X Receita - <?php echo $sOP?></li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Tipo de Despesa X Receita - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formDespesaReceitaTipo" action="?action=DespesaReceitaTipo.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodDespesaReceitaTipo" value="<?=(is_object($oDespesaReceitaTipo)) ? $oDespesaReceitaTipo->getCodDespesaReceitaTipo() : ""?>" />
 				<div  class="box-body" id="form-group">
 								<input type='hidden' name='fCodDespesaReceitaTipo' value='<?php echo ($oDespesaReceitaTipo) ? $oDespesaReceitaTipo->getCodDespesaReceitaTipo() : ""?>'/>

 <div class="form-group col-md-4 col-sm-6">
					<label for="Abreviacao">Abreviação:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Abreviacao' placeholder='Abreviação' name='fAbreviacao'  required   value='<?php echo ($oDespesaReceitaTipo) ? $oDespesaReceitaTipo->getAbreviacao() : ""?>'/>
				</div>
			</div>
 <div class="form-group col-md-4 col-sm-6">
					<label for="Abreviacao">Entrada/Saída:</label>
					<div class="input-group col-md-11 col-sm-12">
                        <select name="fEntradaSaida" class="form-control select2">
                            <option value="">Selecione</option>
                            <option <?php echo (($oDespesaReceitaTipo) && $oDespesaReceitaTipo->getEntradaSaida() =='Entrada') ? " selected " : "" ?> value="Entrada">Entrada</option>
                            <option <?php echo (($oDespesaReceitaTipo) && $oDespesaReceitaTipo->getEntradaSaida() =='Saida') ? " selected " : "" ?> value="Saida">Saída</option>
                        </select>
					<?php echo ($oDespesaReceitaTipo) ? $oDespesaReceitaTipo->getEntradaSaida() : ""?>
				</div>
			</div>
 <div class="form-group col-md-4 col-sm-12">
					<label for="Descricao">Descrição:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'  required   value='<?php echo ($oDespesaReceitaTipo) ? $oDespesaReceitaTipo->getDescricao() : ""?>'/>
				</div>
			</div>



 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(function($){
				 });
 </script>
 </body>
 </html>
