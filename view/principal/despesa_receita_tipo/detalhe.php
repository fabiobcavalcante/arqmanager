﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oDespesaReceitaTipo = $_REQUEST['oDespesaReceitaTipo'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Tipo de Despesa X Receita - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=DespesaReceitaTipo.preparaLista">Gerenciar Tipo de Despesa X Receitas</a>
 			<li class="active">Tipo de Despesa X Receita - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Tipo de Despesa X Receita - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="Abreviacao" class="control-label">Abreviação:</label>
		<p><?php echo ($oDespesaReceitaTipo) ? $oDespesaReceitaTipo->getAbreviacao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Descricao" class="control-label">Descrição:</label>
		<p><?php echo ($oDespesaReceitaTipo) ? $oDespesaReceitaTipo->getDescricao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="IncluidoPor" class="control-label">Incluído por:</label>
		<p><?php echo ($oDespesaReceitaTipo) ? $oDespesaReceitaTipo->getIncluidoPor() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oDespesaReceitaTipo) ? $oDespesaReceitaTipo->getAlteradoPor() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=DespesaReceitaTipo.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
