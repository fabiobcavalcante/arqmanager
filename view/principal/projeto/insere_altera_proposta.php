<?php
$sOP = $_REQUEST['sOP'];
$oProjeto = $_REQUEST['oProjeto'];
$voServico = $_REQUEST['voServico'];
$voClienteColaborador = $_REQUEST['voClienteColaborador'];
$voCliente = $_REQUEST['voCliente'];
$voFormaPagamento  = $_REQUEST['voFormaPagamento'];
$voProjetoPagamento = $_REQUEST['voProjetoPagamento'];

$voPermissao = array(2,3);

$voContaBancaria = $_REQUEST['voContaBancaria'];
 $f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
 $vPermissao = array(1,2,3);
?>
<!doctype html>
<html lang="pt-br">

<section class="content">
    <div class="row">
        <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Projeto - <?php echo $sOP ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <form method="post" class="form-horizontal" name="formProjeto" action="?action=Projeto.processaFormulario">
                            <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
                            <input type="hidden" name="fCodProjeto" value="<?=(is_object($oProjeto)) ? $oProjeto->getCodProjeto() : ""?>" />
                            <input type='hidden' name='fDataFim'  value='<?php echo ($oProjeto) ? $oProjeto->getDataFimFormatado() : ""?>'/>
                            <div  class="box-body" id="form-group">
                                <div class="form-group col-md-6">
                                    <h3>Dados Gerais</h3>
                                    <hr>
                                    <div class="form-group col-md-6  col-sm-12">
                                        <label for="Cliente">Cliente:</label>
                                        <div class="input-group col-md-11 col-sm-12">

                                            <select name='fCodCliente'  class="form-control select2" onchange="modalCliente(this.value)" required>
                                                <option value=''>Selecione</option>
                                                <option value='add'>Novo Cliente</option>
                                                <?php $sSelected = "";
                                                if($voCliente){
                                                    foreach($voCliente as $oCliente){
                                                        if($oProposta){
                                                            $sSelected = ($oProposta->getCodCliente() == $oCliente->getCodCliente()) ? "selected" : "";
                                                        }elseif($_REQUEST['nIdCliente']){
                                                          $sSelected = ($_REQUEST['nIdCliente'] == $oCliente->getCodCliente()) ? "selected" : "";
                                                        }
                                                        ?>
                                                        <option  <?php echo $sSelected?> value='<?php echo $oCliente->getCodCliente()?>'><?php echo $oCliente->getNome()?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6 col-sm-12">
                                        <label for="Descricao">Descrição:</label>
                                        <div class="input-group col-md-11 col-sm-12">
                                            <input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'  required   value='<?php echo ($oProposta) ? $oProposta->getDescricao() : ""?>'/>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-3 col-sm-6">
                                        <label for="DataInicio">Início:</label>
                                        <div class="input-group col-md-11 col-sm-12">
                                            <input class="form-control date" type='text' id='DataInicio' placeholder='Início' name='fDataInicio'  required   value='<?php echo date('d/m/Y')?>'/>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-6 col-sm-8">
                                        <label for="Servico">Serviço:</label>
                                        <div class="input-group col-md-12">

                                            <select name='fCodServico' id="CodServico"  class="form-control select2"  required onChange="verificaServico()">
                                                <option value=''>Selecione</option>
                                                <?php $sSelected = "";
                                                if($voServico){
                                                    foreach($voServico as $oServico){
                                                        if($oProposta){
                                                            $sSelected = ($oProposta->getCodServico() == $oServico->getCodServico()) ? "selected" : "";
                                                        }
                                                        ?>
                                                        <option  <?php echo $sSelected?> value='<?php echo $oServico->getCodServico()?>'><?php echo $oServico->getDescServico()?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-11  col-sm-12">
                                        <!--                    <div id="divEnvolvidos" ></div>-->
                                        <label for="Observacao">Observação</label>
                                        <textarea name="fObservacao" id="Observacao" class="form-control col-md-12"><?php echo ($oProjeto)?$oProjeto->getObservacao() : ""?></textarea>

                                    </div>
                                    <div class="form-group col-md-11 col-sm-12">
                                      <?php if(($sOP == 'Alterar') && (($oProjeto->getCodServico() == 3) ||($oProjeto->getCodServico() == 4))){
                                        $voColaborador = $oFachada->recuperarTodosColaborador();
                                        $oServicoDetalhe =  $oFachada->recuperarTodosServicoDetalhePorServico($oProjeto->getCodServico())[0];
                                        $vooProjetoColaborador = $oFachada->recuperarTodosProjetoColaboradorPorProjeto($oProjeto->getCodProjeto());
                                        if($vooProjetoColaborador){
                                          foreach($vooProjetoColaborador as $oProjetoColaborador){
                                            $aProjetoColaborador[] = $oProjetoColaborador->getCodServicoDetalhe() . "||".$oProjetoColaborador->getCodColaborador();
                                          }
                                        }
                                       ?>
                                         <label for="Colaboradores" class="control-label">Colaborador(es):</label>
                                       <select name="fCodColaborador[]" class="form-control select2" multiple >
                                           <option value="">Selecione</option>
                                           <?php foreach($voColaborador as $oColaborador) {
                                               $sSelected = (in_array($oServicoDetalhe->getCodServicoDetalhe() . "||" .$oColaborador->getCodColaborador(),$aProjetoColaborador)) ? " selected " : "";
                                           ?>
                                               <option <?php echo $sSelected;?> value="<?php echo $oServicoDetalhe->getCodServicoDetalhe() . "||" .$oColaborador->getCodColaborador()?>"><?php echo $oColaborador->getNome()?></option>
                                       <?php } ?>
                                       </select>

                                    <?php   }else{ ?>
                                        <div id="divColaborador" ></div>
                                      <?php } ?>

                                    </div>

                                </div>
                                <?php if(in_array($_SESSION['oUsuarioLP']->getCodGrupoUsuario(),$vPermissao)){?>
                                <div class="form-group col-md-6 col-sm-12">
                                    <h3>Dados Pagamento</h3>
                                    <hr>

                                    <?php if($voProjetoPagamento){
                                        $i=0;
                                        foreach($voProjetoPagamento as $oProjetoPagamento){
                                            $sDisabled = ($oProjetoPagamento->getDataEfetivacao()) ? "disabled" : "";
                                            ?>
                                            <fieldset name="<?php echo $i++; ?>" <?php echo $sDisabled;?> >
                                                <div class="form-group row">
                                                    <div class="col-md-3 col-sm-6">
                                                        <input type="hidden" name='fCodProjetoPagamento[]' value="<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getCodProjetoPagamento(): ""?>">
                                                        <label for="FormaPagamento" class="control-label">Forma Pagamento</label>
                                                        <select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" required <?php echo $sDisabled?> onchange="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-forma-pagamento')">
                                                            <option value="">Selecione</option>
                                                            <?php foreach($voFormaPagamento as $oFormaPagamento){
                                                                $sSelected = ($oProjetoPagamento->getCodFormaPagamento() == $oFormaPagamento->getCodFormaPagamento()) ? " selected='selected'" : "";
                                                                ?>
                                                                <option <?php echo $sSelected ?> value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 col-sm-4">
                                                        <label for="ddi" class="control-label">Valor</label>
                                                        <input type='text' class='form-control' placeholder='Valor' name='fValor[]' onkeyup="FormataMoney(this);repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-valor')" class='form-control' id='ValorParcela' required value="<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getValorFormatado() : ""?>">
                                                    </div>
                                                    <div class="col-md-2 col-sm-4">
                                                        <label for="Vencimento" class="control-label">Vencimento</label>
                                                        <input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" maxlenght="10"  required value="<?php echo ($oProjetoPagamento)? $oProjetoPagamento->getDataPrevisaoFormatado(): ""?>" onkeyup="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-previsao')">
                                                    </div>
                                                    <div class="col-md-3 col-sm-6">
                                                        <label for="Parcela" class="control-label">Parcela</label>
                                                        <input name='fDescPagamento[]' type='text'  class='form-control col-xs-2' value='<?php echo ($oProjetoPagamento)? $oProjetoPagamento->getDescPagamento():""?>' maxlength="11" onkeyup="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-descricao-pagamento')">
                                                    </div>

                                                    <div class="col-md-2 col-sm-4">
                                                        <?php   if($oProjetoPagamento->getDataEfetivacao()){
                                                            echo "<label class='control-label'>Pago em</label>";
                                                            echo "<div class='form-control'>".$oProjetoPagamento->getDataEfetivacaoFormatado()."</div>";

                                                        }else{
                                                            ?>
                                                            <label class='control-label'>Opções</label>
                                                            <div>
                                                                <!-- <a href="?action=ProjetoPagamento.processaFormulario&sOP=ConfirmarPagamento&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>"><button type="button" class="btn btn-success btn-sm" data-widget="confirm" data-toggle="tooltip" title="Confirmar Recebimento"> <i class="fa fa-usd"></i></button></a> -->
                                                                <button id="botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-codigo-confirmacao="<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" data-valor="<?php echo $oProjetoPagamento->getValorFormatado();?>" data-forma-pagamento="<?php echo $oProjetoPagamento->getCodFormaPagamento(); ?>" data-previsao="<?php echo $oProjetoPagamento->getDataPrevisaoFormatado();?>" data-descricao-pagamento="<?php echo $oProjetoPagamento->getDescPagamento();?>" data-target="#modalConfirmacaoPagamento"> <i class="fa fa-usd"></i></button>

                                                                <a data-toggle="modal" data-target="#confirm-delete" data-link="?action=ProjetoPagamento.processaFormulario&sOP=Excluir&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento(); ?>"><button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Excluir Parcela"> <i class="fa fa-trash"></i></button></a>

                                                            </div>

                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </fieldset>

                                        <?php       } //foreach
                                    } // if

									if((!$oProjeto) || ($oProjeto->getDataFim() =="")){
									?>

                                    <div class="form-group row">
                                        <hr>
                                        <div class="col-md-3 col-sm-6">
                                            <input type='hidden' name='fCodProjetoPagamento[]' value="">
                                            <label for="FormaPagamento" class="control-label">Forma Pagamento</label>
                                            <select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" >
                                                <option value="">Selecione</option>
                                                <?php foreach($voFormaPagamento as $oFormaPagamento){?>
                                                    <option value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-sm-4">
                                            <label for="ddi" class="control-label">Valor</label>
                                            <input type='text' class='form-control' placeholder='Valor' name='fValor[]' onkeyup='FormataMoney(this);' class='form-control' id='ValorParcela' >
                                        </div>
                                        <div class="col-md-2 col-sm-4">
                                            <label for="Vencimento" class="control-label">Vencimento</label>
                                            <input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" maxlenght="10"  value=""/>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <label for="Parcela" class="control-label">Parcela</label>
                                            <input name='fDescPagamento[]' type='text'  class='form-control col-xs-2' id='Descricao"+qtdeCampos+"'value='' placeholder="99/99" maxlength="11">
                                        </div>
                                        <div class="col-md-1 col-sm-6">
                                            <label for="Parcela" class="control-label">Confirma</label>
                                            <input name='fConfirmaPamento' type='checkbox' id='idConfirma' value="1">
                                        </div>
                                        <div class="col-md-1 col-sm-4">
                                            <br>
                                            <button type="button" class="btn btn-success btn-sm addButton" onClick="addCampos()" data-widget="adparcela" data-toggle="tooltip" title="Adiciona Parcela"> <i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div id="campoPai"></div>
                                    </div>
                                <?php } ?>
                                 </div>

								 <?php } // permissão ?>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
                                    </div>
                                </div>
                            </div>


                            <!-- /.box-body -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Modal -->
<div class="modal fade" id="modalConfirmacaoPagamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form method="post" action="?action=ProjetoPagamento.processaFormulario">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Conta Destino</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="">

          <input type="hidden" name="sOP" value="ConfirmarPagamento">
          <input type="hidden" id="CodProjetoPagamento" name="nCodProjetoPagamento" value="">
          <input type="hidden" id="CodFormaPagamento" name="fCodFormaPagamento" value="">
          <input type="hidden" id="Valor" name="fValor" value="">
          <input type="hidden" id="DataPrevisao" name="fDataPrevisao" value="">
          <input type="hidden" id="DescPagamento" name="fDescPagamento" value="">

          <div class="form-group row">
            <div class="col-md-12">
              <label for="ContaBancaria" class="control-label">Conta Destino</label>
                <select class="form-control select2" name="fCodContaBancaria" id="ContaBancaria" style="width:100%" onchange="if(this.value!='') $('#btnSalvar').prop('disabled',false);else $('#btnSalvar').prop('disabled',true);" >
                    <option value="">Selecione</option>
                    <?php foreach($voContaBancaria as $oContaBancaria){?>
                        <option value="<?php echo $oContaBancaria->getCodContaBancaria()?>"><?php echo $oContaBancaria->getCodColaborador() . " - " . $oContaBancaria->getBanco() . " - " . $oContaBancaria->getAgencia() . " - " .$oContaBancaria->getConta() . " - " .$f->format($oContaBancaria->getAtivo())?></option>
                    <?php } ?>
                </select>
            </div>
          </div>

      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        <button type="submit" id="btnSalvar" class="btn btn-success" disabled>Salvar</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Cliente -->
<div class="modal fade" id="modalCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document"  style="width:90%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cadastrar Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="post" name="formCliente" id="formCliente" action="?action=Cliente.processaFormulario">

      <div class="modal-body">
        <input type="hidden" name="sOP" value="Cadastrar">
        <input type="hidden" name="fCodCliente" value="">
        <input type="hidden" name="fHeader" value="?action=Projeto.preparaFormulario&sOP=Cadastrar">

        <div class="form-group col-md-2 col-sm-12 row">
          <label for="DbTipoPessoa">Tipo Pessoa:</label>
            <div class="input-group col-md-11 col-sm-12">
              <select name='fCodTipoPessoa' id="CodTipoPessoa"  class="form-control CodTipoPessoa"  required >
                <option value=''>Selecione</option>
                <option value='1'>Física</option>
                <option value='2'>Jurídica</option>
            </select>
          </div>
        </div>

        <div class="form-group col-md-2 col-sm-12 row">
          <label for="Identificacao">CPF/CNPJ:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='Identificacao' name='fIdentificacao' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="Nome">Nome/Fantasia:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='Nome' placeholder='Nome/Fantasia' name='fNome' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="Email">Email:</label>
          <div class="input-group col-md-12 col-sm-12">
            <input class="form-control" type='text' id='email' placeholder='Email' name='fEmail' required>
          </div>
        </div>

        <div class="form-group col-md-2 col-sm-12 row">
          <label for="Cep">CEP:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='cep' placeholder='CEP' name='fCep' required>
          </div>
        </div>

        <div class="form-group col-md-2 col-sm-12 row">
          <label for="Uf">Estado:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='uf' placeholder='Estado' name='fUf' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="Cidade">Cidade:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='cidade' placeholder='Cidade' name='fCidade' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="Bairro">Bairro:</label>
          <div class="input-group col-md-12 col-sm-12">
            <input class="form-control" type='text' id='bairro' placeholder='Bairro' name='fBairro' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="Logradouro">Logradouro:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='rua' placeholder='Logradouro' name='fLogradouro' required>
          </div>
        </div>

        <div class="form-group col-md-1 col-sm-12 row">
          <label for="Numero">Número:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='Numero' placeholder='Número' name='fNumero' required>
          </div>
        </div>

        <div class="form-group col-md-3 col-sm-12 row">
          <label for="Complemento">Complemento:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='Complemento' placeholder='Complemento' name='fComplemento' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="Telefones">Fones:</label>
          <div class="input-group col-md-12 col-sm-12">
            <input class="form-control" type='text' id='Telefones' placeholder='Fones' name='fTelefones' required>
          </div>
        </div>

        <div class="form-group col-md-2 col-sm-12 row">
          <label for="DataNascimento">Data Nascimento:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='DataNascimento' placeholder='Data Nascimento' name='fDataNascimento' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="RazaoSocial">Razão Social:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='RazaoSocial' placeholder='Razão Social' name='fRazaoSocial' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="InscricaoEstadual">Insc. Estadual:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='InscricaoEstadual' placeholder='Insc. Estadual' name='fInscricaoEstadual' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="InscricaoMunicipal">Insc Munic.:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='InscricaoMunicipal' placeholder='Insc Munic.' name='fInscricaoMunicipal' required>
          </div>
        </div>

        <div class="form-group col-md-4 col-sm-12 row">
          <label for="AcessoUsuario">Indicação:</label>
          <div class="input-group col-md-11 col-sm-12">
            <select name='fIndicadoPor'  class="form-control select2"  required  >
              <option value=''>Selecione</option>
              <?php foreach($voClienteColaborador as $oClienteColaborador){ ?>
                <option value='<?php echo $oClienteColaborador->getIncluidoPor()."||".$oClienteColaborador->getIdentificacao()?>'><?php echo $oClienteColaborador->getNome()?></option>
              <?php } ?>
            </select>
          </div>
        </div>

        <div class="form-group col-md-12 col-sm-12">
          <label for="Observacao">Observação:</label>
          <div class="input-group col-md-11 col-sm-12">
            <input class="form-control" type='text' id='Observacao' placeholder='Observação' name='fObservacao'>
          </div>
        </div>

      </div>

      <div class="modal-footer">
          <div class="form-group col-md-12 col-sm-12">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary">Cadastrar</button>
          </div>
      </div>

      </form>
    </div>
  </div>
</div>
    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script type="text/javascript" language="javascript">

function verificaServico(){
  nValor = document.getElementById('CodServico').value;

  if(nValor == 3 || nValor == 4){
    recuperaConteudoDinamico('index.php','action=Projeto.preparaListaColaboraresAjax&sOP=Visualizar&fCodServico='+nValor+'&fCodProjeto=<?php echo ($oProjeto) ? $oProjeto->getCodProjeto() :"";?>','divColaborador');
  }else {
    document.getElementById('divColaborador').innerHTML = "";
  }
}
    //jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
    jQuery(function($){
        $("#DataInicio").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
        $("#DataPrevisaoFim").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
        $("#DataFim").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
        $(".date").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
    });




    function repeteValor(textoDigitado,div,attr){
      $(div).attr(attr,textoDigitado);
    }



    $('#modalConfirmacaoPagamento').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var codigo = button.data('codigo-confirmacao');
        var formaPagamento = button.data('forma-pagamento');
        var dataPrevisao = button.data('previsao');
        var valor = button.data('valor');
        var descPagamento = button.data('descricao-pagamento');
        var modal = $(this);

        var a = $('#botaoConfirma').data('myval'); //getter
        var teste = $('#botaoConfirma').attr("data-myval","20"); //setter

      //  document.getElementById('formPgto').action='venda/pagamento/' + pgtoid + '/edit';
        modal.find('#CodProjetoPagamento').val(codigo);
        modal.find('#CodFormaPagamento').val(formaPagamento);
        modal.find('#DataPrevisao').val(dataPrevisao);
        modal.find('#Valor').val(valor);
        modal.find('#DescPagamento').val(descPagamento);

    });


    var qtdeCampos = 0;
    //te
    function addCampos(){
        var objPai = document.getElementById("campoPai");
        //Criando o elemento DIV;
        var objFilho = document.createElement("div");
        //Definindo atributos ao objFilho:
        objFilho.setAttribute("id","filho"+qtdeCampos);

        //Inserindo o elemento no pai:
        objPai.appendChild(objFilho);
        //Escrevendo algo no filho recém-criado:

//             document.getElementById("filho"+qtdeCampos).innerHTML = "<div class='form-group'><div class='col-xs-3'><input type='text' placeholder='Valor' name='fValor[]'  onkeyup='FormataMoney(this);' class='form-control' id='ValorParcela"+qtdeCampos+"'  required ></div><div class='col-xs-2'><input class='form-control' type='text' id='Vencimento' maxlenght='10' placeholder='Vencimento' name='fVencimento[]' required   value=''/><input name='fGrupoUsuario[]' type='text'  class='form-control col-xs-2' id='Descricao"+qtdeCampos+"'value=''></div><button  type='button' class='btn btn-danger removeButton' onclick='removerCampo("+qtdeCampos+")' value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button></div>";
        document.getElementById("filho"+qtdeCampos).innerHTML =
            '<div class="form-group row">' +
            '<input type="hidden" name="fCodProjeto[]" value="<?php echo($oProjeto)? $oProjeto->getCodProjeto() : ""?>">' +
            '<input type="hidden" name="fCodProjetoPagamento[]" value="">' +
            '<div class="col-md-3">\n' +
            '<label for="tipo" class="control-label">Forma Pagamento</label>\n' +
            '<select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" required>\n' +
            '<option value="">Selecione</option>\n' +
            '<?php foreach($voFormaPagamento as $oFormaPagamento){?>\n' +
            '<option value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>\n' +
            '<?php } ?>\n' +
            '</select>\n' +
            '</div>\n' +
            '<div class="col-md-2 col-sm-4">\n' +
            '<label for="Valor" class="control-label">Valor</label>\n' +
            '<input type="text" class="form-control" placeholder="Valor" name="fValor[]"  onkeyup="FormataMoney(this);" class="form-control" id="ValorParcela"  required >\n' +
            '</div>\n' +
            '<div class="col-md-2 col-sm-4">\n' +
            '<label for="Vencimento" class="control-label">Vencimento</label>\n' +
            '<input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" maxlenght="10"   required value=""/>\n' +
            '</div>\n' +
            '<div class="col-md-3 col-sm-6">\n' +
            '<label for="Parcela" class="control-label">Parcela</label>\n' +
            '<input type="text" class="form-control" name="fDescPagamento[]" id="parcela" placeholder="99/99" maxlength="11">\n' +
            '</div>\n' +
            '<div class="col-md-1">\n' +
            '<br>\n' +
            '<button type="button" class="btn btn-danger btn-sm removeButton" onclick="removerCampo(' + qtdeCampos +')" ><i class="fa fa-trash"></i></button>\n' +
            '</div>\n' +
            '</div>';


        var x = document.createElement("SCRIPT");
        qtdeCampos++;
    }

    function removerCampo(id){
        var objPai = document.getElementById("campoPai");
        var objFilho = document.getElementById("filho"+id);
        //Removendo o DIV com id específico do n-pai:
        var removido = objPai.removeChild(objFilho);
    }

    $('#confirm-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var sLink = button.data('link');
        var modal = $(this);
        modal.find('#ok').attr('href', sLink);
    });

    $('#CodTipoPessoa').change(function(){
      if($(this).val() == '1'){
          $("#RazaoSocial").prop('disabled', true);
          $("#RazaoSocial").prop('required', false);
          $("#InscricaoEstadual").prop('disabled', true);
          $("#InscricaoEstadual").prop('required', false);
          $("#InscricaoMunicipal").prop('disabled', true);
          $("#InscricaoMunicipal").prop('required', false);
          $("#DataNascimento").prop('disabled', false);
          $("#DataNascimento").prop('required', true);
          $("#Rg").prop('disabled', false);
          $("#Rg").prop('required', true);
          $('#DataNascimento').inputmask('99/99/9999', { 'placeholder': '99/99/9999' })
          $('#Identificacao').inputmask('999.999.999-99', { 'placeholder': '999.999.999-99' })
          $("label[for = Identificacao]").text("CPF");
      }else{
         $("#RazaoSocial").prop('disabled', false);
          $("#RazaoSocial").prop('required', true);
          $("#InscricaoEstadual").prop('disabled', false);
          $("#InscricaoEstadual").prop('required', true);
           $("#InscricaoMunicipal").prop('disabled', false);
          $("#InscricaoMunicipal").prop('required', true);
          $("#Rg").prop('disabled', true);
          $("#Rg").prop('required', false);
          $("#DataNascimento").prop('disabled', true);
          $("#DataNascimento").prop('required', false);
          $('#Identificacao').inputmask('99.999.999/9999-99', { 'placeholder': '99.999.999/9999-99' })
          $("label[for = Identificacao]").text("CNPJ");
           //
    	//		validar(this.value, 'cnpj', 'validar_CNPJ','CNPJ inválido');

      }

     });
      $("#Identificacao").blur(function() {
          var valor = $('#CodTipoPessoa').val();
          var nIdentificacao = $('#Identificacao').val();

    	  if(valor ==1){
              validar(nIdentificacao, 'Identificacao', 'valida_cpf','CPF inválido');
          }else{
              validar(nIdentificacao, 'Identificacao', 'validar_CNPJ','CNPJ inválido');
          }
      });

       $("#email").blur(function() {
          var sEmail = $('#email').val();
          validar(sEmail, 'email', 'validacaoEmail','Email inválido');
      });

    function modalCliente(sAcao){

      if (sAcao === 'add') {
        $('#modalCliente').modal('show');
      }


    }


    $(document).ready(function() {

    $('#alerta').on('hidden.bs.modal', function(event) {
        $(this).removeClass( 'fv-modal-stack' );
        $('body').data( 'fv_open_modals', $('body').data( 'fv_open_modals' ) - 1 );
    });

    $('#alerta').on('shown.bs.modal', function (event) {
        // keep track of the number of open modals
        if ( typeof( $('body').data( 'fv_open_modals' ) ) == 'undefined' ) {
            $('body').data( 'fv_open_modals', 0 );
        }

        // if the z-index of this modal has been set, ignore.
        if ($(this).hasClass('fv-modal-stack')) {
            return;
        }

        $(this).addClass('fv-modal-stack');
        $('body').data('fv_open_modals', $('body').data('fv_open_modals' ) + 1 );
        $(this).css('z-index', 1040 + (10 * $('body').data('fv_open_modals' )));
        $('#modalCliente').not('.fv-modal-stack').css('z-index', 1039 + (10 * $('body').data('fv_open_modals')));
        $('#modalCliente').not('fv-modal-stack').addClass('fv-modal-stack');

    });
});

$(document).ready(function() {
    $("#cep").inputmask("99.999-999",{ 'placeholder': '99.999-999' });

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#uf").val("");

    }

    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#rua").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                $("#uf").val("...");


                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#rua").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#uf").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
});


function adicionarDiasData(){
    var dataFormulario = document.getElementById('DataInicio').value;

    var dias = document.getElementById(nDias).value;

    var data        = new Date(dataFormulario);
    var dataVenc    = new Date(data.getTime() + (dias * 24 * 60 * 60 * 1000));
      alert(data);
    novaData = dataVenc.getDate() + "/" + (dataVenc.getMonth() + 1) + "/" + dataVenc.getFullYear();
    //alert(dataFormulario);
    return false;
  //  document.getElementById("DataPrevisaoFim").innerHTML = novaData;


}

</script>
</body>
</html>
