<?php
 $sOP = $_REQUEST['sOP'];
 $oProjeto = $_REQUEST['oProjeto'];
 $voEtapas = $_REQUEST['voEtapas'];
$oCliente = $oProjeto->getCliente();
 $oProposta = $oProjeto->getProposta();
 $voPagamento = $_REQUEST['voProjetoPagamento'];

$nTotal = 0;
$nParcelas = count($voPagamento);
foreach ($voPagamento as $oPagamento) {
    $nTotal += $oPagamento->getValor();
}

$voPrazos = $_REQUEST['voPrazos'];
$voProjetoServicoMicroServico = $_REQUEST['voProjetoServicoMicroServico'];


$sDescricaoMicroServico = "";

if($voProjetoServicoMicroServico){
   foreach($voProjetoServicoMicroServico as $oMicroServico){
		 if($oMicroServico->getCodProjeto() != 'RRT - CAU'){
			 $sDescricaoMicroServico .= $oMicroServico->getCodProjeto();
			 if($oMicroServico->getQuantidade() >0){
           $sDescricaoMicroServico .= " QTDE. " . $oMicroServico->getQuantidade() . (($oMicroServico->getDescricaoQuantidade()) ? " (".$oMicroServico->getDescricaoQuantidade()."); " : "");
       }elseif($oMicroServico->getDescricaoQuantidade()){
           $sDescricaoMicroServico.= " (".$oMicroServico->getDescricaoQuantidade()."); ";
       }else{
           $sDescricaoMicroServico .= "; ";
       }

		 }
   } //foreach($voProjetoServicoMicroServico as $oMicroServico){
$sDescricaoMicroServico = substr($sDescricaoMicroServico,0,-2);
}


 ?>
 <!doctype html>
 <html>
 <head>
 <title>Projeto - Gráfico</title>
 <?php include_once('includes/head.php')?>
     <style>
     .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #02560ba8;
    border-color: #02560ba8;
    padding: 1px 10px;
    color: #fff;
}
     </style>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Projeto.preparaLista">Gerenciar Projetos</a>
 			<li class="active">Projeto - <?php echo $sOP?></li>
 		</ol>
    <?php include_once('includes/mensagem.php')?>
     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Projeto - Detalhe</h3>
         </div>
         <div class="box-body">
<div class="col-md-6">
 <div class="col-md-4">

 <label for="CodCliente" class="control-label">Cliente:</label>
		<p>
				<?php echo ($oCliente) ? $oCliente->getNome() : ""?>
		</p>
	</div>
   <div class="col-md-2">

   <label for="Identificação" class="control-label">Identificação:</label>
  		<p><?php echo ($oCliente) ? $oCliente->getIdentificacao() : ""?></p>
  	</div>
   <div class="col-md-6">

      <label for="Identificação" class="control-label">Endereço:</label>
     		<p><?php echo ($oCliente) ? $oCliente->getLogradouro() .", nº". $oCliente->getNumero() . " ".  $oCliente->getComplemento(). ". Bairro:" . $oCliente->getBairro() . " na cidade de ". $oCliente->getCidade() . "/" . $oCliente->getUf()  : ""?></p>
     	</div>
 <div class="col-md-4">

 <label for="CodServico" class="control-label">Serviço:</label>
		<p><?php echo ($oProjeto) ? $oProjeto->getServico()->getDescServico() : ""?></p>
	</div>



 <div class="col-md-4">

 <label for="Descricao" class="control-label">Descrição:</label>
		<p><?php echo ($oProjeto) ? $oProjeto->getDescricao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DataInicio" class="control-label">Início:</label>
		<p><?php echo ($oProjeto) ? $oProjeto->getDataInicioFormatado() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DataPrevisaoFim" class="control-label">Data Previsão:</label>
		<p><?php echo ($oProjeto) ? $oProjeto->getDataPrevisaoFimFormatado() : ""?></p>
	</div>

</div>
<div class="col-md-6">


      <?php if($voPrazos){
          $nTotalDias=0;
          $date = new DateTime($oProjeto->getDataInicio());
        ?>
      <h3>Entregas</h3>
     <hr>
      <div class="row">
       <div class="col-md-6"><label for="Etapa" class="control-label">Etapa:</label></div>
       <div class="col-md-2"><label for="Prazo" class="control-label">Prazo:</label></div>

       <div class="col-md-2"><label for="Ação" class="control-label">Ação:</label></div>

     </div>
      <?php foreach($voPrazos as $oPrazo){
          // $nTotalDias += $oPrazo->getPrazo();
          // $dDataLimite = $date->add(new DateInterval("P{$nTotalDias}D"));
        ?>
      <div class="row" style="padding-top:3px">
       <div class="col-md-6">
           <?php echo ($oPrazo)? $oPrazo->getServicoEtapa()->getDescricao():""?>
 	       </div>
         <div class="col-md-2"><?php echo ($oPrazo && $oPrazo->getPrazo()>0)? $oPrazo->getDataPrevistaFormatado() :"-";?></div>

         <div class="col-md-2">
           <?php if(!$oPrazo->getDataConclusao()){
                  //   if(in_array(210, $_SESSION['voPermissao']))
                  //     echo "<a href='?action=ProjetoServicoEtapa.processaFormulario&sOP=Concluir&nCodProposta={$oPrazo->getCodProposta()}&nCodEtapa={$oPrazo->getCodEtapa()}'><button type='button' class='btn btn-xs btn-success'><span class='btn-label'><i class='glyphicon glyphicon-ok'></i></span> Concluir</button></a>";
                  //   else {
                  //     echo " - ";
                  //   }
                  // }else{
									echo "-";
								}else{
                    if($oPrazo->getDataConclusao() <= $oPrazo->getDataPrevista()){
                      echo '<i class="fa fa-thumbs-o-up btn-xs" style="color:green;" aria-hidden="true"></i> ';
                    }else{
                        echo '<i class="fa fa-thumbs-o-down btn-xs" style="color:red;" aria-hidden="true"></i> ';
                    }
									}

                  ?>
           </div>
           </div>
          <?php } ?>

          <?php }?>

  		</div>

      <div class="col-xs-12">
	 <h3>GRÁFICO - <?php echo "{$oCliente->getNome()} - {$oProposta->getIdentificacao()}"?></h3>
  <hr>
	<div class="row">
	 <div id="chart_div"></div>
</div>


</div>
<?php //} ?>
         <div class="row">.</div>
 			<div class="form-group col-md-12" align='center'>
 				<a class="btn btn-lg btn-primary" href="?action=Projeto.preparaLista">Voltar</a>
 			</div>
         </form>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php if($voEtapas){?>
 <script type="text/javascript">
     // google.charts.load('current', {'packages':['gantt']});
     google.charts.load('current', {'packages': ['gantt'],'language':'pt_br'});

     google.charts.setOnLoadCallback(drawChart);

     function drawChart() {

       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Task ID');
        data.addColumn('string', 'Task Name');
        data.addColumn('string', 'Resource');
        data.addColumn('date',   'Start Date');
        data.addColumn('date', 'End Date');
        data.addColumn('number', 'Duration');
        data.addColumn('number', 'Percent Complete');
        data.addColumn('string', 'Dependencies');

       data.addRows([
         <?php foreach($voEtapas as $oEtapa){
           $vDataPrevisaoInicio  = explode('-',$oEtapa->data_previsao_fim);
           $vDataPrevisaoFim     = explode('-',$oEtapa->data_previsao_fim);

        ?>
                ['<?php echo $oEtapa->cod_etapa?>','<?php echo $oEtapa->eta_descricao?>','Projeto', new Date(<?php echo $vDataPrevisaoInicio[0]?>, <?php echo $vDataPrevisaoInicio[1]?>, <?php echo $vDataPrevisaoInicio[2]?>), new Date(<?php echo $vDataPrevisaoFim[0]?>, <?php echo $vDataPrevisaoFim[1]?>, <?php echo $vDataPrevisaoFim[2]?>), null,  100, null],
    <?php      }

         ?>


          ['Hockey', 'Hockey Season', 'sports', new Date(2021, 5, 21),  new Date(2021, 5, 21), null, 89, null]
       ]);

       var options = {
         height: 400,
         gantt: {
           trackHeight: 30
         }
       };

       var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

       chart.draw(data, options);
     }
   </script>
 <?php }?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
