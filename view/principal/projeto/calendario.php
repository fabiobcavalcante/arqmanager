<?php
$sOP = $_REQUEST['sOP'];
$oProjeto = $_REQUEST['oProjeto'];
$nPrazoDiasUteis = $_REQUEST['nPrazo'];
$oCliente = $_REQUEST['oCliente'];
$oProposta = $_REQUEST['oProposta'];
$voEtapa = $_REQUEST['voEtapa'];
$voPropostaMicroServico = $_REQUEST['voPropostaMicroServico'];
$oArquivamento = $_REQUEST['oArquivamento'];

$sDescricaoMicroServico = "";
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Projeto - Calendário</title>
    <?php include_once('includes/head.php')?>
    <link rel="stylesheet" href="vendor/bootstrap4/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/all.min.css" />
</head>
<body class="sidebar-collapse sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>

    <?php include_once('includes/menu.php')?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Projeto.preparaLista">Gerenciar Projetos</a>
                <li class="active">Projeto - <?php echo $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h4>CALENDÁRIO  - <?php echo "{$oCliente->getNome()} - {$oProposta->getIdentificacao()}"?></h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <?php if($voPropostaMicroServico){  ?>
                                <h3>Micro Etapas</h3>
                                <hr>
                                <form method="post" class="form-horizontal" name="formAtualizaPrazo" id="formAtualizaPrazo" action="?action=Projeto.processaFormulario">
                                    <input name="sOP" type="hidden" value="AtualizarPrazos">
                                    <input name="fCodProposta" type="hidden" value="<?php echo $oProjeto->getCodProposta()?>">
                                    <input name="fCodProjeto" type="hidden" value="<?php echo $oProjeto->getCodProjeto()?>">
                                    <input name="fPrazo" type="hidden" value="<?php echo $nPrazoDiasUteis?>">
                                    <input name="fDataInicioProjeto" id="fDataInicioProjeto" type="hidden" value="<?php echo $oProjeto->getDataInicioFormatado()?>">
                                    <input name="fIdArquivamento" type="hidden" value="<?php echo ($oArquivamento) ? $oArquivamento->getIdArquivamento() : ""; ?>">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Detalhe</th>
                                            <th>Dias</th>
                                            <th>Prevista Início</th>
                                            <th>Prevista Fim</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php $nTotalDiasUteis =0;
                                        foreach($voPropostaMicroServico as $oMicroServico){
                                            $nTotalDiasUteis += $oMicroServico->getDias();
                                            ?>
                                            <tr>
                                                <td>
                                                    <input name="fCodMicroservico[]" type="hidden" value="<?php echo $oMicroServico->getCodMicroservico()?>">
                                                    <?php
                                                    echo $oMicroServico->getServicoMicroservico()->getDescricao();
                                                    if($oMicroServico->getQuantidade() >0){
                                                        echo " QTDE. " . $oMicroServico->getQuantidade() . (($oMicroServico->getDescricaoQuantidade()) ? " (".$oMicroServico->getDescricaoQuantidade().")" : "");
                                                    }elseif($oMicroServico->getDescricaoQuantidade()){
                                                        echo " (".$oMicroServico->getDescricaoQuantidade().")";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $oDetalheMicroServico = $oMicroServico->getServicoMicroservico();
                                                    if($oDetalheMicroServico->getPrazo() == 0){
                                                        $sDias = "readonly";
                                                        $nPrazo=1;
                                                    }else{
                                                        $sDias = "";
                                                        $nPrazo=0;
                                                    }

                                                    ?>
                                                    <input name="fDias[]" id="Dias<?php echo $oMicroServico->getCodMicroservico()?>" type="number" value='<?php echo ($nPrazo == 1) ? 0 : $oMicroServico->getDias();?>' min='1' max='99' onkeyup="calculaData()" <?php echo ($oMicroServico->getDataEfetivacaoFim() || $sDias) ? "readonly" : ""?>/></td>
                                                <td id="PrevisaoInicio<?php echo $oMicroServico->getCodMicroservico()?>"><?php echo ($oMicroServico->getDataPrevisaoInicio()) ? $oMicroServico->getDataPrevisaoInicioFormatado() : "-";?></td>
                                                <td id="PrevisaoFim<?php echo $oMicroServico->getCodMicroservico()?>"><?php echo ($oMicroServico->getDataPrevisaoFim()) ? $oMicroServico->getDataPrevisaoFimFormatado() : "-";?>
                                                </td>
                                            </tr>

                                        <?php } ?>

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th></th>
                                            <th id="divMicroservicos"></th>
                                            <th colspan="2">
                                                <?php if($_SESSION['oEscritorio']->getDiasUteis() =='S'){?>
                                                <div id="divPrazoEtapas"><?php echo $nPrazoDiasUteis?></div> dias úteis</th>
                                            <?php }else{
                                                $dDataMaxima = $_REQUEST['nPrazo'];
                                                $data1 = new DateTime($oProjeto->getDataInicio());
                                                $data2 = new DateTime($oProjeto->getDataInicio());
                                                $sDias = "P{$dDataMaxima}D";

                                                $data2->add(new DateInterval($sDias));
                                                $intervalo = $data1->diff($data2);
                                                echo substr($intervalo->format('%R%a dias corridos'),1);
                                            } ?>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <div style="text-align: center">
                                        <button type="submit" id="btnAtualizarPrazos" class="btn btn-sm btn-primary">Atualizar prazos</button>
                                        <div id="divMensagemPrazo"></div>
                                    </div>
                                    <hr>
                                </form>
                            <?php } ?>

                        </div>

                        <div class="col-lg-6">
                            <!-- THE CALENDAR -->
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
        </section>
    </div>
</div>
<!-- /.content-wrapper -->
<?php include_once('includes/footer.php')?>
<?php include_once('includes/javascript.php')?>
<!-- row.with.details-->
<!-- Page specific script -->

<script>

    function calculaData(){
        // verificar Data
        let dDataInicio = $("#fDataInicioProjeto").val();
        let nSoma = 0;
        let nVez = 0;
        let totalDiasUteis=0;
        var prazoEtapas = parseInt($("input[type=hidden][name=fPrazo]").val());
        let elements = document.getElementById("formAtualizaPrazo").elements;

        for (let i = 0; i < elements.length; i++) {
            if(elements[i].name === "fCodMicroservico[]") {

                let nCodMicroservico = elements[i].value;
                let nDias = $('#Dias' + nCodMicroservico).val();
                if (parseInt(nDias) === 0)
                    nDias = 1;
                let sDivInicio = '#PrevisaoInicio' + nCodMicroservico;
                let sDivFim = '#PrevisaoFim' + nCodMicroservico;
                if (nDias !== '') {
                    nSoma = nSoma + parseInt(nDias);
                    totalDiasUteis = nSoma + totalDiasUteis;
                    let nInicio = nSoma - parseInt(nDias);
                    if(nVez !== 0){
                        nInicio = nInicio+1;
                    }
                    nVez=1;

                    let actionInicio = "?action=Relatorio.calculaPrazo&sOP=Visualizar&sTipo=<?php echo $_SESSION['oEscritorio']->getDiasUteis()?>&nDias="+nInicio+"&dData="+dDataInicio;
                    let actionFim = "?action=Relatorio.calculaPrazo&sOP=Visualizar&sTipo=<?php echo $_SESSION['oEscritorio']->getDiasUteis()?>&nDias="+nSoma+"&dData="+dDataInicio;

                    $(sDivInicio).load(actionInicio);
                    $(sDivFim).load(actionFim);
                } else{
                    $(sDivInicio).text("");
                    $(sDivFim).text("");
                }
            }
        }

        $('#divMicroservicos').html(nSoma);

        if(nSoma > prazoEtapas){
            $('#btnAtualizarPrazos').prop('disabled',true);
            $('#divMensagemPrazo').html("<br><span class='alert alert-warning'><i class='fa fa-exclamation-triangle'></i> O Prazo da proposta deve ser menor ou igual a soma dos dias de micro serviços!</span>");
        }else{
            $('#btnAtualizarPrazos').prop('disabled',false);
            $('#divMensagemPrazo').html("");
        }

    }

    <?php if($voEtapa){?>
    $(function () {

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date()
        var d    = date.getDate(),
            m    = date.getMonth(),
            y    = date.getFullYear()
        $('#calendar').fullCalendar({
            header    : {
                left  : 'prev,next today',
                center: 'title'
            },
            buttonText: {
                today: 'Hoje',
                month: 'Mês',
                week : 'Semana',
                day  : 'Dia'
            },
            //Random default events
            events    : [
                <?php foreach($voEtapa as $oEtapa){ ?>
                {
                    id: '<?php echo $oEtapa->cod_microservico; ?>',
                    title: '<?php echo $oEtapa->descricao; ?>',
                    start: '<?php echo $oEtapa->data_previsao_inicio_microservico; ?>',
                    end: '<?php echo $oEtapa->data_previsao_fim_microservico; ?>T23:59:59',
                    backgroundColor: '<?php echo $oEtapa->cor; ?>',
                    borderColor: '<?php echo $oEtapa->cor; ?>'
                },
                <?php } ?>
            ],
            editable  : false,
            droppable : false
        })


    });
    <?php } ?>

</script>

<!-- ./wrapper -->
</body>
</html>
