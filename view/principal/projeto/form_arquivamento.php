<?php
$oProjeto = $_REQUEST['oProjeto'];
$voProjetoArquivamento = $_REQUEST['voProjetoArquivamento'];
$sArquivar = ($_REQUEST['fCodStatus']==6) ? "Arquivar" : "Desarquivar";
?>
<form action="?action=Projeto.processaFormulario&sOP=AlterarStatus" method="post" id="formArquivamento">
    <input type="hidden" name="fCodStatus" value="<?php echo ($_REQUEST['fCodStatus']=='6')?'6':'7'?>">
    <input type="hidden" name="fCodProjeto" value="<?php echo $oProjeto->cod_projeto?>">
    <header class="modal-header">
        <h2 class="modal-title"><?php echo $sArquivar?> Projeto</h2>
        <h4><?php echo $oProjeto->identificacao ."[".$oProjeto->nome."]"?></h4>
    </header>
    <div class="modal-body" id="modalBody">
        <input type="hidden" value="<?php echo $oProjeto->cod_projeto ?>" name="fCodProjeto" id="CodProjeto">
        <div class="form-group row">
            <div class="col-md-3">
                <label class="control-label" for="DataConclusao">Data</label>
                <input type="text" class="form-control date" name="fDataAcao" id="DataAcao"  required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label class="control-label" for="Motivo">Motivo</label>
                <textarea rows="4" class="form-control" name="fMotivoAcao" id="MotivoAcao" required></textarea>
            </div>
        </div>
        <?php if(is_object($voProjetoArquivamento)){?>
        <div class="row">
            <table class="table-responsive">
                <thead>
                    <tr>
                        <th>Quem</th>
                        <th>Quando</th>
                        <th>Status</th>
                        <th>Motivo</th>
                    </tr>

                </thead>
                <tbody>
                <?php foreach($voProjetoArquivamento as $oProjetoArquivamento){?>
                    <tr>
                        <td><?php echo $oProjetoArquivamento->getCodColaborador()?></td>
                        <td><?php echo $oProjetoArquivamento->getDataAcaoFormatado()?></td>
                        <td><?php echo $oProjetoArquivamento->getCodStatus()?></td>
                        <td><?php echo $oProjetoArquivamento->getMotivoAcao()?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php }?>
    </div>
    <!-- /.box-body -->
    <footer class="modal-footer">
        <button type="submit" class="btn btn-success">Enviar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </footer>
</form>
<script>
    $(function () {
        //Máscaras]
        $('.date').mask('00/00/0000');
    });
</script>