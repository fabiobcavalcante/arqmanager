<?php
$sOP = $_REQUEST['sOP'];
$oProjeto = $_REQUEST['oProjeto'];
$voServico = $_REQUEST['voServico'];
$voCliente = $_REQUEST['voCliente'];
$voFormaPagamento  = $_REQUEST['voFormaPagamento'];
$voProjetoPagamento = $_REQUEST['voProjetoPagamento'];

?>
<!doctype html>
<form method="post" class="form-horizontal" name="formProjeto" action="?action=ProjetoPagamento.processaFormulario">
<input type="hidden" name="sOP" value="Alterar" />
<input type="hidden" name="fCodProjeto" value="<?php echo (is_object($oProjeto)) ? $oProjeto->getCodProjeto() : ""?>" />
<div  class="box-body" id="form-group">

                                <div class="form-group col-md-12">
                                    <h3>Dados Pagamento</h3>
                                    <hr>

                                    <?php if($voProjetoPagamento){
                                        $i=0;
                                        foreach($voProjetoPagamento as $oProjetoPagamento){
                                            $sDisabled = ($oProjetoPagamento->getDataEfetivacao()) ? "disabled" : "";
                                            ?>
                                            <fieldset name="fsLinha<?php echo $i++; ?>" <?php echo $sDisabled;?> >
                                                <div class="form-group row">
                                                    <div class="col-md-3 col-sm-6">
                                                        <input type="hidden" name='fCodProjetoPagamento[]' value="<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getCodProjetoPagamento(): ""?>">
                                                        <label for="FormaPagamento" class="control-label">Forma Pagamento</label>
                                                        <select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" required <?php echo $sDisabled?>>
                                                            <option value="">Selecione</option>
                                                            <?php foreach($voFormaPagamento as $oFormaPagamento){
                                                                $sSelected = ($oProjetoPagamento->getCodFormaPagamento() == $oFormaPagamento->getCodFormaPagamento()) ? " selected='selected'" : "";
                                                                ?>
                                                                <option <?php echo $sSelected ?> value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 col-sm-4">
                                                        <label for="ddi" class="control-label">Valor</label>
                                                        <input type='text' class='form-control' placeholder='Valor' name='fValor[]'  onkeyup='FormataMoney(this);' class='form-control' id='ValorParcela'  required value="<?php echo ($oProjetoPagamento) ? $oProjetoPagamento->getValorFormatado() : ""?>"/>
                                                    </div>
                                                    <div class="col-md-2 col-sm-4">
                                                        <label for="Vencimento" class="control-label">Vencimento</label>
                                                        <input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" maxlenght="10" required value="<?php echo ($oProjetoPagamento)? $oProjetoPagamento->getDataPrevisaoFormatado(): ""?>"/>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6">
                                                        <label for="Parcela" class="control-label">Parcela</label>
                                                        <input name='fDescPagamento[]' type='text'  class='form-control col-xs-2' value='<?php echo ($oProjetoPagamento)? $oProjetoPagamento->getDescPagamento():""?>' maxlength="11">
                                                    </div>

                                                    <div class="col-md-2 col-sm-4">
                                                        <?php   if($oProjetoPagamento->getDataEfetivacao()){
                                                            echo "<label class='control-label'>Data Pagamento</label>";
                                                            echo "<div class='form-control'>".$oProjetoPagamento->getDataEfetivacaoFormatado()."</div>";

                                                        }else{
                                                            ?>
                                                            <label class='control-label'>Opções</label>
                                                            <div >
                                                                <a href="?action=ProjetoPagamento.processaFormulario&sOP=ConfirmarPagamento&nPagina=1&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento()?>"><button type="button" class="btn btn-success btn-sm" data-widget="confirm" data-toggle="tooltip" title="Confirmar Recebimento"> <i class="fa fa-usd"></i></button></a>
                                                                
                                                                <a data-toggle="modal" data-target="#confirm-delete" data-link="?action=ProjetoPagamento.processaFormulario&sOP=Excluir&nPagina=1&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento(); ?>"><button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Excluir Parcela"> <i class="fa fa-trash"></i></button></a>
                                                                
                                                            </div>

                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </fieldset>

                                        <?php       } //foreach
                                    } // if ?>

                                    <div class="form-group row">
                                        <hr>
                                        <div class="col-md-3 col-sm-6">
                                            <input type='hidden' name='fCodProjetoPagamento[]' value="">
                                            <label for="FormaPagamento" class="control-label">Forma Pagamento</label>
                                            <select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" >
                                                <option value="">Selecione</option>
                                                <?php foreach($voFormaPagamento as $oFormaPagamento){?>
                                                    <option value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-sm-4">
                                            <label for="ddi" class="control-label">Valor</label>
                                            <input type='text' class='form-control' placeholder='Valor' name='fValor[]'  onkeyup='FormataMoney(this);' class='form-control' id='ValorParcela' >
                                        </div>
                                        <div class="col-md-2 col-sm-4">
                                            <label for="Vencimento" class="control-label">Vencimento</label>
                                            <input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" maxlenght="10"  value=""/>
                                        </div>
                                        <div class="col-md-3 col-sm-4">
                                            <label for="Parcela" class="control-label">Parcela</label>
                                            <input name='fDescPagamento[]' type='text'  class='form-control col-xs-2' id='Descricao"+qtdeCampos+"'value='' placeholder="99/99" maxlength="11">
                                        </div>

                                        <div class="col-md-2 col-sm-3">
                                            <br>
                                            <button type="button" class="btn btn-success btn-sm addButton" onClick="addCampos()" data-widget="adparcela" data-toggle="tooltip" title="Adiciona Parcela"> <i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>




                                    <div class="form-group col-md-12">
                                        <div id="campoPai"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <button type="submit" class="btn btn-lg btn-success" >Alterar</button>
                                    </div>
                                </div>
                            </div>


                            <!-- /.box-body -->
                        </form>
                   