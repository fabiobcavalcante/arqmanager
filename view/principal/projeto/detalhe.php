<?php
$sOP = $_REQUEST['sOP'];
$oProjeto = $_REQUEST['oProjeto'];
$oCliente = $oProjeto->getCliente();
$oProposta = $oProjeto->getProposta();

$voPagamento = $_REQUEST['voProjetoPagamento'];

$nTotal = 0;
if (is_array($voPagamento)){
    $nParcelas = count($voPagamento);
    foreach ($voPagamento as $oPagamento) {
        $nTotal += $oPagamento->getValor();
    }
}

$voPrazos = $_REQUEST['voPrazos'];
$voPropostaMicroServico = $_REQUEST['voPropostaMicroServico'];

$sDescricaoMicroServico = "";

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Projeto - Detalhe</title>
    <?php include_once('includes/head.php')?>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #02560ba8;
            border-color: #02560ba8;
            padding: 1px 10px;
            color: #fff;
        }
    </style>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Projeto.preparaLista">Gerenciar Projetos</a>
                <li class="active">Projeto - <?php echo $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Projeto - Detalhe</h3>
                </div>
                <div class="box-body">

                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label for="CodCliente" class="control-label">Cliente:</label>
                            <p><?php echo ($oCliente) ? "<a href='?action=Cliente.preparaFormulario&sOP=Detalhar&nIdCliente={$oCliente->getCodCliente()}'>{$oCliente->getNome()}</a		>" : ""?>	</p>
                        </div>
                        <div class="col-md-2">
                            <label for="Identificação" class="control-label">Identificação:</label>
                            <p><?php echo ($oCliente) ? $oCliente->getIdentificacao() : ""?></p>
                        </div>
                        <div class="col-md-6">
                            <label for="Identificação" class="control-label">Endereço:</label>
                            <p><?php echo ($oCliente) ? $oCliente->getLogradouro() .", nº". $oCliente->getNumero() . " ".  $oCliente->getComplemento(). ". Bairro:" . $oCliente->getBairro() . " na cidade de ". $oCliente->getCidade() . "/" . $oCliente->getUf()  : ""?></p>
                        </div>
                        <div class="col-md-4">
                            <label for="CodServico" class="control-label">Serviço:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getServico()->getDescServico() : ""?></p>
                        </div>
                        <div class="col-md-4">
                            <label for="Descricao" class="control-label">Descrição:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getDescricao() : ""?></p>
                        </div>

                        <div class="col-md-2">
                            <label for="DataInicio" class="control-label">Início:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getDataInicioFormatado() : ""?></p>
                        </div>
                        <div class="col-md-2">
                            <label for="DataPrevisaoFim" class="control-label">Data Previsão:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getDataPrevisaoFimFormatado() : ""?></p>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <?php if($voPrazos){
                            $nTotalDias=0;
                            $date = new DateTime($oProjeto->getDataInicio());
                            ?>
                            <h3>Entregas</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-8"><label for="Etapa" class="control-label">Etapa:</label></div>
                                <div class="col-md-4"><label for="Prazo" class="control-label">Prazo:</label></div>
                            </div>
                            <?php foreach($voPrazos as $oPrazo){ ?>
                                <div class="row" style="padding-top:3px">
                                    <div class="col-md-8">
                                        <?php echo ($oPrazo)? $oPrazo->getServicoEtapa()->getDescricao():""?>
                                    </div>
                                    <div class="col-md-4"><?php echo ($oPrazo && $oPrazo->getPrazo()>0)? $oPrazo->getDataPrevistaFormatado() :"-";?>
                                        <?php     if($oPrazo->getDataConclusao() <= $oPrazo->getDataPrevista()){
                                            echo '<i class="fa fa-thumbs-o-up btn-xs" style="color:green;" aria-hidden="true"></i> ';
                                        }else{
                                            echo '<i class="fa fa-thumbs-o-down btn-xs" style="color:red;" aria-hidden="true"></i> ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>

                        <?php }?>

                    </div>
                    <div class="col-md-12">
                        <?php if($voPropostaMicroServico){  ?>
                            <h3>Micro serviços</h3>
                            <hr>

                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Detalhe</th>
                                    <th>Dias</th>
                                    <th>Prevista Início</th>
                                    <th>Prevista Fim</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($voPropostaMicroServico as $oMicroServico){?>

                                    <tr>
                                        <td><?php
                                            echo $oMicroServico->getServicoMicroservico()->getDescricao();
                                            if($oMicroServico->getQuantidade() >0){
                                                echo " QTDE. " . $oMicroServico->getQuantidade() . (($oMicroServico->getDescricaoQuantidade()) ? " (".$oMicroServico->getDescricaoQuantidade().")" : "");
                                            }elseif($oMicroServico->getDescricaoQuantidade()){
                                                echo " (".$oMicroServico->getDescricaoQuantidade().")";
                                            }
                                            ?>;</td>
                                        <td><?php echo ($oMicroServico->getDias() >0) ? $oMicroServico->getDias() : "-";?></td>
                                        <td><?php echo ($oMicroServico->getDataPrevisaoInicio()) ? $oMicroServico->getDataPrevisaoInicioFormatado() : "-";?></td>
                                        <td><?php echo ($oMicroServico->getDataPrevisaoFim()) ? $oMicroServico->getDataPrevisaoFimFormatado() : "-";?></td>

                                    </tr>

                                <?php } //foreach($voProjetoServicoMicroServico as $oMicroServico){ ?>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Detalhe</th>
                                    <th>Dias</th>
                                    <th>Prevista Início</th>
                                    <th>Prevista Fim</th>

                                </tr>
                                </tfoot>
                            </table>
                        <?php }?>

                    </div>
                    <?php //if(($voPagamento) && ($_SESSION['oUsuarioLP']->getCodGrupoUsuario()==1 || $_SESSION['oUsuarioLP']->getCodGrupoUsuario()==2)) {?>

                    <?php //} ?>
                    <div class="row"><br><br><br></div>
                    <div class="form-group col-md-12" align='center'>
                        <a class="btn btn-lg btn-primary" href="?action=Projeto.preparaLista">Voltar</a>
                    </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>
</div>
<?php include_once('includes/javascript.php')?>

</div>
<!-- ./wrapper -->
</body>
</html>
