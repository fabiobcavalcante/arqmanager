<?php
$voProjetoPagamento = $_REQUEST['voProjetoPagamento'];
$nTotal = 0;
if($voProjetoPagamento){
?>
<table id="lista" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Ações</th>
        <th>Descrição</th>
        <th>Previsão</th>
        <th>Valor</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($voProjetoPagamento as $oProjetoPagamento){
        $nTotal += $oProjetoPagamento->getValor();
        ?>
        <tr>
            <td>
                <a href="?action=ProjetoPagamento.preparaFormularioAjax&sOP=Cadastrar&nCodProjeto=<?php echo $oProjetoPagamento->getCodProjeto()?>"><span class="btn btn-sm bg-blue" title="Adicionar Pagamento ao Projeto"> <i class="fa fa-plus"></i></span></a>

                <a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=3&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>"><button id="GerarRecibo<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" type="button" class="btn btn-warning btn-sm"  data-toggle="tooltip" title="Gerar Recibo"><i class="fa fa-file-o"></i></button></a>

                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-codigo="<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" data-target="#modalConfirmacaoPagamento"> <i class="fa fa-usd"></i></button>

                <a data-toggle="modal" data-target="#confirm-delete" data-link="?action=ProjetoPagamento.processaFormulario&sOP=Excluir&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento(); ?>"><button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Excluir Parcela"> <i class="fa fa-trash"></i></button></a>

            </td>
            <td><?php echo $oProjetoPagamento->getDescPagamento()?></td>
            <td><?php echo $oProjetoPagamento->getDataPrevisaoFormatado()?></td>
            <td><?php echo $oProjetoPagamento->getValorFormatado()?></td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr>
        <th></th>
        <th></th>
        <th>Total</th>
        <th><?php echo number_format($nTotal,2,",",".");?></th>
    </tr>
    </tfoot>
</table>
<?php }else{ ?>
 <div title="Mensagem" class="btn bg-success"><p><i class="fa fa-check"></i> Não existem pagamentos de projeto pendentes de confirmação</p></div>
<?php } ?>
