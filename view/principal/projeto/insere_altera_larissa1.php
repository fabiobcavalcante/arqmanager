<?php
$sOP = $_REQUEST['sOP'];
$oProjeto = $_REQUEST['oProjeto'];
$oProposta = $_REQUEST['oProposta'] ?? null;
$voServico = $_REQUEST['voServico'];

$voCliente = $_REQUEST['voCliente'];
$voStatus = $_REQUEST['voStatus'];

$voProjetoPagamento = (isset($_REQUEST['voProjetoPagamento'])) ? $_REQUEST['voProjetoPagamento'] : null;

 $sOP2 = $_GET['sOP2'] ? $_GET['sOP2']  : null;

 $voIndicacao = $_REQUEST['voIndicacao'] ?? null;
 
 $voFormaPagamento  = $_REQUEST['voFormaPagamento'];
 $voPermissao = array(2,3);
 $voContaBancaria = $_REQUEST['voContaBancaria'];
 
 $f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
 $vPermissao = array(1,2,3);
 
 $bPagamento = 0;
 $sDisabled = '';
 $nCodProposta = ($oProposta) ? $oProposta->getCodProposta() : "";
 
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Projeto</title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light" <?php echo ($oProposta) ? "onload='etapaServico({$oProposta->getCodServico()},{$oProposta->getCodProposta()})'" : ''?>>
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Projeto.preparaLista">Gerenciar Projetos</a>
                <li class="active">Projeto - <?php echo ($sOP2) ?: $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->

        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Projeto - <?php echo ($oProposta) ? "<strong>{$oProposta->getNumeroPropostaFormatado()}</strong>" : "" ?> <?php echo ($sOP2) ? $sOP2 : $sOP ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <form method="post" class="form-horizontal" name="formProposta" id="formPropostaProjeto" action="?action=Projeto.processaFormulario">
                            <input type="hidden" name="sOP" value="<?php echo ($sOP2) ? $sOP2 : $sOP; ?>" >
                            <div  class="box-body" id="form-group">
                                <input type='hidden' name='fCodProposta' id="idProposta" value='<?php echo ($sOP2) ? false : (($oProposta) ? $oProposta->getCodProposta() : "")?>'>
                                <input type='hidden' name='fNumeroProposta' value='<?php echo ($oProposta) ? $oProposta->getNumeroProposta() : ""?>'>
                                <input type='hidden' name='fAnoProposta' value='<?php echo ($oProposta) ? $oProposta->getAnoProposta() : ""?>'>
                                <input type='hidden' name='fCodProjeto' id="idProjeto" value='<?php echo ($sOP2) ? false : (($oProjeto) ? $oProjeto->getCodProjeto() : "")?>'>
                                <input type='hidden' name='fNumeroProjeto' value='<?php echo ($oProposta) ? $oProjeto->getNumeroProjeto() : ""?>'>
                                <input type='hidden' name='fAnoProjeto' value='<?php echo ($oProjeto) ? $oProjeto->getAnoProjeto() : ""?>'>
                                <?php if($sOP == "Cadastrar" || $sOP2 == "Cadastrar"){?>
                                    <input type='hidden' name='fCodStatus' value='1'>
                                <?php } ?>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="Data">Data:</label>
                                    <div class="input-group col-md-11">
                                        <input class="form-control date" type='text' id='Data' name='fDataProposta' required value='<?php echo ($oProposta) ? $oProposta->getDataPropostaFormatado() :  date('d/m/Y')?>'>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Servico">Serviço:</label>
                                    <div class="input-group col-md-11  col-sm-12">
                                        <?php if ($sOP == 'Cadastrar'){ ?>
                                            <select name='fCodServico' id="Servico" class="form-control select2"  required  onchange="etapaServico(this.value)">
                                                <option value=''>Selecione</option>
                                                <?php $sSelected = "";
                                                if($voServico){
                                                    foreach($voServico as $oServico){
                                                        if($oProposta){
                                                            $sSelected = ($oProposta->getCodServico() == $oServico->getCodServico()) ? "selected" : "";
                                                        }
                                                        ?>
                                                        <option  <?php echo $sSelected?> value='<?php echo $oServico->getCodServico()?>'><?php echo $oServico->getDescServico()?></option>
                                                        <?php
                                                    }
                                                } ?>
                                            </select>
                                        <?php } else{
                                            echo $oProposta->getServico()->getDescServico(); ?>
                                            <input type="hidden" name='fCodServico' id="Servico" required value='<?php echo $oProposta->getCodServico() ?>'>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Cliente">Cliente:</label>
                                    <div class="input-group col-md-11">
                                        <select name='fCodCliente' id="Cliente" class="form-control select2" >
                                            <option value=''>Selecione</option>
                                            <?php $sSelected = "";
                                            if($voCliente){
                                                foreach($voCliente as $oCliente){
                                                    if($oProposta){
                                                        $sSelected = ($oProposta->getCodCliente() == $oCliente->getCodCliente()) ? "selected" : "";
                                                    }
                                                    ?>
                                                    <option  <?php echo $sSelected?> value='<?php echo $oCliente->getCodCliente()?>'><?php echo $oCliente->getNome()?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="Descricao">Descrição:</label>
                                    <div class="input-group col-md-11">
                                        <textarea name="fDescricao" id="Descricao" class="form-control" placeholder="Descrição" style="width: 100%; height: 150px; font-size: 15px; line-height: 14px; border: 1px solid #dddddd; padding: 10px;"><?php echo ($oProposta) ? $oProposta->getDescricao() : ""?></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="Identificacao">Identificação:</label>
                                    <div class="input-group col-md-11">
                                        <input class="form-control maiuscula" type='text' id='Identificacao' placeholder='Identificação' name='fIdentificacao' required value='<?php echo ($oProposta) ? $oProposta->getIdentificacao() : ""?>'>
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="ValorProposta">Valor:</label>
                                    <div class="input-group col-md-11">
                                        <input class="form-control money" type='text' id='ValorProposta' placeholder='Valor' name='fValorProposta' onkeyup="FormataMoney(this);" required   value='<?php echo ($oProposta) ? $oProposta->getValorPropostaFormatado() : ""?>'>
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="EntregaParcial">Entrega Parcial:</label>
                                    <div class="input-group col-md-11">
                                        <select class="form-control" name="fEntregaParcial" id="EntregaParcial" required onchange="entregaFracionada(this.value)">
                                            <option value="">Selecione</option>
                                            <option value="Não" <?php echo ($oProposta && $oProposta->getEntregaParcial() == 'Não') ? "selected" : ""?>>Não</option>
                                            <option value="Sim" <?php echo ($oProposta && $oProposta->getEntregaParcial() == 'Sim') ? "selected" : ""?>>Sim</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                        <label for="Metragem">Metragem</label>
                                        <input name="fMetragem" id="Metragem" class="form-control" type='text' placeholder='M²' onkeyup="FormataMoney(this);" value="<?php echo ($oProjeto)? $oProjeto->getMetragemFormatado() : ""?>">
                                    </div>
                                <div class="form-group col-md-6">
                                    <label for="Descricao">Prazo:</label>
                                    <div class="input-group col-md-11">
                                        <textarea name="fPrazoProposta" id="Prazo" class="textarea" placeholder="Prazo" style="width: 100%; height: 150px; font-size: 15px; line-height: 14px; border: 1px solid #dddddd; "><?php echo ($oProposta) ? $oProposta->getPrazo() : "";?></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="Descricao">Observação:</label>
                                    <div class="input-group col-md-11">
                                        <textarea name="fObservacao" id="Observacao" class="textarea" placeholder="Observação" style="width: 100%; height: 150px; font-size: 15px; line-height: 14px; border: 1px solid #dddddd; "><?php echo ($oProjeto) ? $oProjeto->getObservacao() : "";?></textarea>
                                    </div>
                                </div>
                                <div id="divFracionamento" class="form-group col-md-12"></div>
 <!-- parte nova pagamentos -->
  
 <?php if(in_array($_SESSION['oGrupoUsuario']->getCodGrupoUsuario(),$vPermissao)){ ?>
                                    <div class="form-group col-md-12 col-sm-12">
                                        <h3>Dados Pagamento</h3>
                                        <hr>

                                        <?php
                                        if($voProjetoPagamento){
                                            $i=0;
                                            foreach($voProjetoPagamento as $oProjetoPagamento){
                                                $bPagamento = ($bPagamento == 1) ? 1 : ($oProjetoPagamento->getDataEfetivacao()) ? 1 : 0;
                                                $sDisabled = ($oProjetoPagamento->getDataEfetivacao()) ? "disabled" : ""; ?>
                                                <fieldset name="<?php echo $i++; ?>">
                                                    <div class="form-group row">

                                                        <div class="col-md-3 col-sm-6">
                                                            <input type="hidden" name='fCodProjetoPagamento[]' value="<?php echo $oProjetoPagamento->getCodProjetoPagamento() ?>" <?php echo $sDisabled;?>>
                                                            <label for="FormaPagamento" class="control-label">Forma Pagamento</label>
                                                            <select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" required <?php echo $sDisabled?> onchange="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-forma-pagamento')" <?php echo $sDisabled;?>>
                                                                <option value="">Selecione</option>
                                                                <?php foreach($voFormaPagamento as $oFormaPagamento){
                                                                    $sSelected = ($oProjetoPagamento->getCodFormaPagamento() == $oFormaPagamento->getCodFormaPagamento()) ? " selected='selected'" : ""; ?>
                                                                    <option <?php echo $sSelected ?> value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-2 col-sm-12">
                                                            <label for="ValorParcela" class="control-label">Valor</label>
                                                            <input type='text' placeholder='Valor' name='fValor[]' onkeyup='FormataMoney(this);' class='form-control' id='ValorParcela' value="<?php echo $oProjetoPagamento->getValorFormatado() ?>" <?php echo $sDisabled;?>>
                                                        </div>

                                                        <div class="col-md-2 col-sm-4">
                                                            <label for="Vencimento" class="control-label">Vencimento</label>
                                                            <input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" max="10"  required value="<?php echo ($oProjetoPagamento)? $oProjetoPagamento->getDataPrevisaoFormatado(): ""?>" onkeyup="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-previsao')" <?php echo $sDisabled;?>>
                                                        </div>

                                                        <div class="col-md-3 col-sm-6">
                                                            <label for="DescPagamento" class="control-label">Parcela</label>
                                                            <input name='fDescPagamento[]' id="DescPagamento" type='text' class='form-control col-xs-2' value='<?php echo $oProjetoPagamento->getDescPagamento() ?>' maxlength="11" onkeyup="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-descricao-pagamento')" <?php echo $sDisabled;?>>
                                                        </div>
                                                        <div class="col-md-2 col-sm-4">
                                                            <?php if($oProjetoPagamento->getDataEfetivacao()){ ?>
                                                                <label class='control-label'>Pago em</label><br>
                                                                <a class="btn btn-primary btn-sm" href="/<?php echo $oProjetoPagamento->getComprovante() ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Pago em <?php echo $oProjetoPagamento->getDataEfetivacaoFormatado() ?>"><i class="fa fa-check"></i></a>
                                                            <?php }else{ ?>
                                                                <label class='control-label'>Opções</label><br>
                                                                    <button id="botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-codigo-confirmacao="<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" data-valor="<?php echo $oProjetoPagamento->getValorFormatado();?>" data-forma-pagamento="<?php echo $oProjetoPagamento->getCodFormaPagamento(); ?>" data-previsao="<?php echo $oProjetoPagamento->getDataPrevisaoFormatado();?>" data-descricao-pagamento="<?php echo $oProjetoPagamento->getDescPagamento();?>" data-target="#modalConfirmacaoPagamento" title="Confirmar Pagamento"> <i class="fa fa-usd"></i></button>
                                                                    <a data-toggle="modal" data-target="#confirm-delete" data-link="?action=ProjetoPagamento.processaFormulario&sOP=Excluir&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento(); ?>"><button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Excluir Parcela"> <i class="fa fa-trash"></i></button></a>
                                                            <?php } ?>
                                                            <a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=3&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>"><button id="GerarRecibo<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" type="button" class="btn btn-warning btn-sm"  data-toggle="tooltip" title="Gerar Recibo"><i class="fa fa-file-o"></i></button></a>
                                                        </div>

                                                    </div>
                                                </fieldset>
                                            <?php } ?>
                                        <?php } ?>

                                        <div class="form-group">
                                            <div class="col-md-3 col-sm-6">
                                                <input type='hidden' name='fCodProjetoPagamento[]' value="">
                                                <label for="FormaPagamento" class="control-label">Forma Pagamento</label>
                                                <select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" >
                                                    <option value="">Selecione</option>
                                                    <?php foreach($voFormaPagamento as $oFormaPagamento){?>
                                                        <option value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2 col-sm-12">
                                                <label for="ddi" class="control-label">Valor</label>
                                                <input type='text' placeholder='Valor' name='fValor[]' onkeyup='FormataMoney(this);' class='form-control' id='ValorParcela' >
                                            </div>
                                            <div class="col-md-2 col-sm-12">
                                                <label for="Vencimento" class="control-label">Vencimento</label>
                                                <input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" max="10"  value=""/>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <label for='Descricao"+qtdeCampos+"' class="control-label">Parcela</label>
                                                <input name='fDescPagamento[]' type='text'  class='form-control col-xs-2' id='Descricao"+qtdeCampos+"' value='' placeholder="99/99" maxlength="11">
                                            </div>
                                            <div class="col-md-1 col-sm-4">
                                                <br>
                                                <button type="button" class="btn btn-success btn-sm addButton" onClick="addCampos()" data-widget="adparcela" data-toggle="tooltip" title="Adiciona Parcela"> <i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <div id="campoPai"></div>
                                    </div>
                                <?php } ?>

<!-- fim parte nova pagamentos -->
                                <div id="divMicroServicos" class="form-group col-md-12"></div>
                                <div id="divServicoEtapa" class="form-group col-md-12"></div>

                                <div class="form-group col-md-12">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <button type="submit" class="btn btn-lg btn-success"><?php echo ($sOP2) ?: $sOP; ?></button>

                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            
<!-- Modal -->
<div class="modal fade" id="modalConfirmacaoPagamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="?action=ProjetoPagamento.processaFormulario" enctype="multipart/form-data">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Confirmação de Pagamento</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <input type="hidden" name="sOP" value="ConfirmarPagamento">
                    <input type="hidden" id="CodProjetoPagamento" name="nCodProjetoPagamento" value="">
                    <input type="hidden" id="CodFormaPagamento" name="fCodFormaPagamento" value="">
                    <input type="hidden" id="Valor" name="fValor" value="">
                    <input type="hidden" id="DataPrevisao" name="fDataPrevisao" value="">
                    <input type="hidden" id="DescPagamento" name="fDescPagamento" value="">
                    <input type="hidden" id="Origem" name="fOrigem" value="projeto">

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="ContaBancaria" class="control-label">Conta Destino</label>
                            <select class="form-control select2" name="fCodContaBancaria" id="ContaBancaria" style="width:100%" onchange="if(this.value !== '') $('#btnSalvar').prop('disabled',false);else $('#btnSalvar').prop('disabled',true);" >
                                <option value="">Selecione</option>
                                <?php foreach($voContaBancaria as $oContaBancaria){?>
                                    <option value="<?php echo $oContaBancaria->getCodContaBancaria()?>"><?php echo $oContaBancaria->getCodColaborador() . " - " . $oContaBancaria->getBanco() . " - " . $oContaBancaria->getAgencia() . " - " .$oContaBancaria->getConta();?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="DataConfirmacao" class="control-label">Data Confirmação</label>
                            <input class="form-control date" type='text' id='DataConfirmacao' placeholder='Data Confirmação' name='fDataConfirmacao' required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="ContaBancaria" class="control-label">Comprovante</label>
                            <input class="form-control" type='file' id='Comprovante' name='fComprovante'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="submit" id="btnSalvar" class="btn btn-success" disabled>Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>

    <div class="modal fade" id="modalEntrega">
        <div class="modal-dialog">
            <div class="modal-content" id="bodyEntrega"></div>
        </div>
    </div>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script type="text/javascript">
    $(document).ready(function() {
        entregaFracionada($("#EntregaParcial").val());
    });


    function toggle(source) {
        checkboxes = document.getElementsByName('foo');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    function entregaFracionada(nEntrega){
        if (nEntrega === "Sim"){
            $("#FormaPagamento").hide();
            recuperaConteudo('index.php','?action=PropostaFracionada.preparaLista&sOP=Fracionamento&nIdProposta=<?php echo ($oProposta) ? $oProposta->getCodProposta() : ""?>','divFracionamento');
            console.log(recuperaConteudo);
        } else{
            $("#FormaPagamento").show();
            $("#divFracionamento").html("");
        }

    }

    $('#confirm-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var sLink = button.data('link');
        var modal = $(this);
        modal.find('#ok').attr('href', sLink);
    });

    function etapaServico(nCodServico,nCodProposta=null){
        if(!nCodProposta){
            recuperaConteudo('index.php','?action=ServicoMicroServico.preparaLista&sOP=Servico&bPagamento=0&nIdServico=' + nCodServico,'divMicroServicos');
            recuperaConteudo('index.php','?action=ServicoEtapa.preparaLista&sOP=Servico&bPagamento=0&nIdServico=' + nCodServico,'divServicoEtapa');
        }else{
            recuperaConteudo('index.php','?action=ServicoMicroServico.preparaLista&bPagamento=0&sOP=Servico&sOP2=Alterar&nCodProposta='+nCodProposta+'&nIdServico=' + nCodServico,'divMicroServicos');
            recuperaConteudo('index.php','?action=ServicoEtapa.preparaLista&bPagamento=0&sOP=Servico&sOP2=Alterar&nCodProposta='+nCodProposta+'&nIdServico=' + nCodServico,'divServicoEtapa');
        }

    }

    <?php if(isset($_REQUEST['oProjeto']) && isset($_REQUEST['sOP2']) && $_REQUEST['sOP2'] != 'Cadastrar'){?>
    $(function(){
        $("input").attr("readonly","readonly");
        $("select").attr("disabled","disabled");
        $("button").attr("disabled","disabled");

    });
    <?php } ?>

    $(function () {
        $('.wysihtml5').wysihtml5({
            toolbar: {
                "font-styles": false,
                "blockquote": false
            }
        });
    })

    $(function() {
        var textareas = $('textarea.clean');
        $.each(textareas, function(key, value) {
            $(this).val($(this).val().replace(/[ ]+/g, ' ').replace(/^[ ]+/m, ''));
            $('a[title="CTRL+S"]').remove(); // Remove o button Small
            $('a[title="CTRL+U"]').remove(); // Remove o button Underline
            $('a[title="Insert image"]').remove(); // Remove o button Insert Image
            $('a[title="Insert link"]').remove(); // Remove o button Insert Link
        })
    });

    function abreModalEntrega(){
        let nCodServico = $("#Servico").val();
        recuperaConteudoDinamico("","action=ServicoMicroServico.preparaFormulario&nCodServico="+nCodServico+"&sOP=Cadastrar", "bodyEntrega");
        $("#modalEntrega").modal('show');
    }

    $("#modalEntrega").on('shown.bs.modal', function(){
        $('.my-colorpicker1').colorpicker()
    });

    $('#modalConfirmacaoPagamento').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let codigo = button.data('codigo-confirmacao');
        let formaPagamento = button.data('forma-pagamento');
        let dataPrevisao = button.data('previsao');
        let valor = button.data('valor');
        let descPagamento = button.data('descricao-pagamento');
        let modal = $(this);

        modal.find('#CodProjetoPagamento').val(codigo);
        modal.find('#CodFormaPagamento').val(formaPagamento);
        modal.find('#DataPrevisao').val(dataPrevisao);
        modal.find('#Valor').val(valor);
        modal.find('#DescPagamento').val(descPagamento);

    });

    function processaFormulario(sForm) {

        let form = $("#" + sForm);

        $.ajax({
            type: "POST",
            datatype: "json",
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function () {
                Swal.fire({
                    title: 'Enviando...',
                    didOpen: () => {
                        Swal.showLoading()
                    }
                })
            },
            error: function (oXMLRequest, sErrorType) {
                console.log(form.attr('action'));
                console.log(oXMLRequest.responseText);
                console.log(oXMLRequest.status + ' , ' + sErrorType);
            },
            success: function (dados) {
                console.log(dados);
                dados = jQuery.parseJSON(dados);

                if (dados.sClass === "success") {

                    recuperaConteudo('index.php', '?action=ServicoMicroServico.preparaLista&sOP=Servico&bPagamento=0&nIdServico=' + dados.nCodServico, 'divMicroServicos');

                    $('#modalEntrega').modal('hide');
                }

                Swal.fire({
                    icon: dados.sClass,
                    title: dados.sMsg,
                    timer: 2000
                });
            }
        });
    }


    var qtdeCampos = 0;
    //te  
    function addCampos(){
        var objPai = document.getElementById("campoPai");
        //Criando o elemento DIV;
        var objFilho = document.createElement("div");
        //Definindo atributos ao objFilho:
        objFilho.setAttribute("id","filho"+qtdeCampos);

        //Inserindo o elemento no pai:
        objPai.appendChild(objFilho);
        //Escrevendo algo no filho recém-criado:

        document.getElementById("filho"+qtdeCampos).innerHTML =
            '<div class="form-group">' +
            '<input type="hidden" name="fCodProjeto[]" value="<?php echo($oProjeto)? $oProjeto->getCodProjeto() : ""?>">' +
            '<input type="hidden" name="fCodProjetoPagamento[]" value="">' +
            '<div class="col-md-2 col-sm-12">\n' +
            '<label for="tipo" class="control-label">Forma Pagamento</label>\n' +
            '<select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" required>\n' +
            '<option value="">Selecione</option>\n' +
            '<?php foreach($voFormaPagamento as $oFormaPagamento){?>\n' +
            '<option value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>\n' +
            '<?php } ?>\n' +
            '</select>\n' +
            '</div>\n' +
            '<div class="col-md-2 col-sm-12">\n' +
            '<label for="Valor" class="control-label">Valor</label>\n' +
            '<input type="text" class="form-control" placeholder="Valor" name="fValor[]" onkeyup="FormataMoney(this);" class="form-control" id="ValorParcela"  required >\n' +
            '</div>\n' +
            '<div class="col-md-2 col-sm-12">\n' +
            '<label for="Vencimento" class="control-label">Vencimento</label>\n' +
            '<input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" maxlenght="10" required value=""/>\n' +
            '</div>\n' +
            '<div class="col-md-2 col-sm-6">\n' +
            '<label for="Parcela" class="control-label">Parcela</label>\n' +
            '<input type="text" class="form-control" name="fDescPagamento[]" id="parcela" placeholder="99/99" maxlength="11">\n' +
            '</div>\n' +
            '<div class="col-md-1 col-sm-12">\n' +
            '<br>\n' +
            '<button type="button" class="btn btn-danger btn-sm removeButton" onclick="removerCampo(' + qtdeCampos +')" ><i class="fa fa-trash"></i></button>\n' +
            '</div>\n' +
            '</div>';

        qtdeCampos++;
        $('.select2').select2();
        $('.date').mask('00/00/0000');
    }

    function removerCampo(id){
        var objPai = document.getElementById("campoPai");
        var objFilho = document.getElementById("filho"+id);
        //Removendo o DIV com id específico do n-pai:
        objPai.removeChild(objFilho);
    }


</script>
</body>
</html>
