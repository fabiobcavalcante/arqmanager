<?php
$voProjeto = $_REQUEST['voProjeto'];
$oDocumentoContrato = $_REQUEST['oDocumentoContrato'];
$oDocumentoTE = $_REQUEST['oDocumentoTE'];
$oDocumentoRRT = $_REQUEST['oDocumentoRRT'];
$sStatus = $_REQUEST['sStatus'] ?? false;
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Lista de Projetos </title>
    <?php include_once('includes/head.php')?>
    <style>
        .linkAtivo{border:2px solid red;}
    </style>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li class="active">Gerenciar Projetos</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Projeto - <?php echo ($sStatus) ?: "TODOS"?></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <form method="post" action="" name="formProjeto" id="formProjeto" class="formulario">
                                <div class='form-group col-md-4'>
                                    <a href="?action=Projeto.preparaFormulario&sOP=Cadastrar" class="btn btn-success"><i class="fa fa-file-o" data-toggle="tooltip" data-placement="top" title="Novo Projeto"></i> NOVO PROJETO</a>
                                </div>

                                <div style="text-align:right" class='form-group col-md-8'>
                                       <a class="active" href="?action=Projeto.preparaLista&sStatus=TODOS"><span class="label label-warning <?php echo (!($sStatus) || ($sStatus =='TODOS')) ? "linkAtivo": ""?>">Todos</span></a>
                                        <a href="?action=Projeto.preparaLista&sStatus=ANDAMENTO"><span class="label label-info <?php echo ($sStatus =='ANDAMENTO') ? "linkAtivo": ""?>"> Andamento</span></a>
                                        <a href="?action=Projeto.preparaLista&sStatus=CONCLUIDO"><span class="label label-success <?php echo ($sStatus =='CONCLUIDO') ? "linkAtivo": ""?>">Concluído</span></a>
                                    </div>
                                <div class='row'></div>
                                <?php if(is_array($voProjeto)){?>
                                    <table id="listaProjeto" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width:10%;text-align:center;">Ações</th>
                                            <th style="width:10%">Número</th>
                                            <th style="width:14%">Cliente</th>
                                            <th style="width:10%">Serviço</th>
                                            <th style="width:30%">Descrição</th>
                                            <th style="width:10%">Início</th>
                                            <th style="width:10%">Data previsão</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($voProjeto as $oProjeto){ ?>
                                            <tr>
                                                <td style="white-space: nowrap;text-align:center;vertical-align: middle">

                                                    <!-- Single button -->
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Ações<span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="?action=Projeto.preparaFormulario&sOP=Detalhar&nCodProjeto=<?php echo $oProjeto->cod_projeto?>"><i class="fa fa-search"></i> Detalhar Projeto</a></li>
                                                            <li><a href="?action=ProjetoDocumento.preparaLista&nCodProjeto=<?php echo $oProjeto->cod_projeto?>"><i class="fa fa-file"></i> Documentos do Projeto</a></li>

                                                            <?php if(!$oProjeto->data_fim){ ?>
                                                                <li><a href="?action=Projeto.preparaFormulario&sOP=Alterar&nCodProjeto=<?php echo $oProjeto->cod_projeto?>"><i class="fa fa-edit"></i> Alterar Projeto</a></li>
                                                                <?php if($oDocumentoContrato){?>
                                                                    <li><a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=2&nCodProjeto=<?php echo $oProjeto->cod_projeto?>"><i class="fa fa-gavel"></i> Contrato</a></li>
                                                                <?php } ?>
                                                            <?php } ?>

                                                            <li><a target="_blank" href="?action=Projeto.preparaFormulario&sOP=Calendario&nCodProjeto=<?php echo $oProjeto->cod_projeto?>"><i class="fa fa-calendar"></i> Calendario</a></li>
                                                            <?php if($oDocumentoRRT){?>
                                                                <li><a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=4&nCodProjeto=<?php echo $oProjeto->cod_projeto?>"><i class="fa fa-file"></i> Gerar RRT</a></li>
                                                            <?php } ?>
                                                            <?php if($oDocumentoTE){?>
                                                                <li><a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=5&sOP=TermoEntrega&nCodProjeto=<?php echo $oProjeto->cod_projeto?>"><i class="fa fa-file"></i> Termo de Entrega</a></li>
                                                            <?php } ?>
                                                            <?php if(!$oProjeto->data_fim){ ?>
                                                                <li><a href="" data-toggle="modal" data-target="#confirm-delete" onclick="modalConfirmDelete(<?php echo $oProjeto->cod_projeto; ?>)"><i class="fa fa-close"></i> Excluir Projeto</a></li>
                                                            <?php } ?>

                                                        </ul>
                                                    </div>

                                                </td>
                                                <td><?php echo $oProjeto->ano_projeto ."/".str_pad($oProjeto->numero_projeto , 4 , '0' , STR_PAD_LEFT);?></td>
                                                <td><?php echo $oProjeto->nome?></td>
                                                <td><?php echo $oProjeto->desc_servico?></td>
                                                <td><?php echo $oProjeto->identificacao?></td>
                                                <td>
                                                    <?php
                                                    $oData = new DateTime($oProjeto->data_inicio);
                                                    echo $oData->format("d/m/Y");
                                                    ?></td>
                                                <td><?php 	$oData = new DateTime($oProjeto->data_previsao_fim);
                                                    echo $oData->format("d/m/Y");?></td>

                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th style="text-align:center;">Ações</th>
                                            <th>Número</th>
                                            <th>Cliente</th>
                                            <th>Serviço</th>
                                            <th>Descrição</th>
                                            <th>Início</th>
                                            <th>Data previsão</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                <?php } ?>
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script>
    function modalConfirmDelete(nCodProjeto) {
        $('#confirm-delete', 0, 0).on('show.bs.modal', function() {
            let modal = $(this, 0, 0);
            modal.find('#ok').attr('href',"?action=Projeto.processaFormulario&sOP=Excluir&fIdProjeto=" + nCodProjeto);
        });
    }


    $('#listaProjeto').DataTable({
        dom: 'Bfrtip',
        columnDefs: [
            {
                targets: [5,6],
                type: 'date-euro',
            }
        ],
        buttons: ['excel', 'pdf', 'print'],
        'responsive': true,
        'paging'      : true,
        'lengthChange': false,
        'info'        : true,
        'autoWidth'   : true,
        "pagingType"  : "full",
        "language": {
            "decimal": ",",
            "thousands": ".",
            "lengthMenu": "Mostrar _MENU_ registros por p&aacute;gina",
            "zeroRecords": "Nada encontrado",
            "info": "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro Encontrado",
            "infoFiltered": "(filtrados de _MAX_ registros)",
            "search":         "Pesquisar:",
            "paginate": {
                "first":      "Primeiro",
                "last":       "&Uacute;ltimo",
                "next":       "Pr&oacute;ximo",
                "previous":   "Anterior"
            }
        }
    });
</script>
</body>
</html>
