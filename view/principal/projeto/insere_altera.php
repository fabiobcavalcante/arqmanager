<?php
$sOP = $_REQUEST['sOP'];
$oProjeto = $_REQUEST['oProjeto'];
$oProposta = $_REQUEST['oProposta'] ?? null;

$voClienteColaborador = $_REQUEST['voClienteColaborador'];
$voServico = $_REQUEST['voServico'];
$voIndicacao = $_REQUEST['voIndicacao'] ?? null;
$voProjetoPagamento = (isset($_REQUEST['voProjetoPagamento'])) ? $_REQUEST['voProjetoPagamento'] : null;

$voCliente = $_REQUEST['voCliente'];
$voFormaPagamento  = $_REQUEST['voFormaPagamento'];
$voPermissao = array(2,3);
$voContaBancaria = $_REQUEST['voContaBancaria'];

if($sOP == 'Cadastrar')
    $voProposta = $_REQUEST['voProposta'];

$f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
$vPermissao = array(1,2,3);

$bPagamento = 0;
$sDisabled = '';
$nCodProposta = ($oProposta) ? $oProposta->getCodProposta() : "";

$voStatus = $_REQUEST['voStatus'];

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Projeto - <?php echo $sOP; ?></title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Projeto.preparaLista">Gerenciar Projetos</a>
                <li class="active">Projeto - <?php echo $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <form method="post" class="form-horizontal" name="formPropostaProjeto" id="formPropostaProjeto" action="?action=Projeto.processaFormulario" enctype="multipart/form-data">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Projeto - <?php echo $sOP ?></h3>
                            </div>
                            <!-- /.box-header -->

                            <input type="hidden" name="sOP" value="<?php echo $sOP?>">
                            <input type="hidden" name="fCodProjeto" value="<?php echo ($oProjeto) ? $oProjeto->getCodProjeto() : ""?>">
                            <input type="hidden" name="fNumeroProjeto" value="<?php echo ($oProjeto) ? $oProjeto->getNumeroProjeto() : ""?>">
                            <input type="hidden" name="fAnoProjeto" value="<?php echo ($oProjeto) ? $oProjeto->getAnoProjeto() : ""?>">
                            <input type="hidden" name="fAnoProjeto" value="<?php echo ($oProjeto) ? $oProjeto->getAnoProjeto() : ""?>">
                            <input type="hidden" name="fCodProposta" value="<?php echo ($oProjeto) ? $oProjeto->getCodProposta() : ((isset($oProposta)) ? $oProposta->getCodProposta() : "")?>">
                            <input type="hidden" name="fCodStatus" value="<?php echo ($oProjeto) ? $oProjeto->getCodStatus() :  "7";?>">
                            <div class="box-body" id="form-group">

                                <!-- Dados da Proposta -->
                                <?php if($oProposta && $sOP=='Cadastrar'){ ?>
                                    <div class="form-group col-md-12">
                                        <h4>Dados da Proposta [<?php echo ($oProposta->getCliente()) ? $oProposta->getCliente()->getNome() : $oProposta->getNome()?>]</h4>
                                        <hr>
                                        <div class="col-md-12  col-sm-12">
                                            <div class="col-md-1 col-sm-12">
                                                <label for="CodServico" class="control-label">Serviço:</label>
                                                <p><?php echo $oProposta->getServico()->getDescServico() ?></p>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <label for="Descricao" class="control-label">Descrição:</label>
                                                <p><?php echo $oProposta->getDescricao() ?></p>
                                            </div>
                                            <div class="col-md-2 col-sm-12">
                                                <label for="Valor" class="control-label">Valor:</label>
                                                <p><?php echo "R$ {$oProposta->getValorPropostaFormatado()}"; ?></p>
                                            </div>
                                            <div class="col-md-2 col-sm-12">
                                                <label for="A VISTA" class="control-label">Valor à Vista:</label>
                                                <p><?php echo "R$ {$oProposta->getValorAvistaFormatado()}"; ?></p>
                                            </div>
                                            <div class="col-md-3 col-sm-12">
                                                <label for="ValorParcelaAprazo" class="control-label">Valor Parcela:</label>
                                                <p><?php echo $oProposta->getValorParcelaAprazo() ?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!-- fim Dados da Proposta -->
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <h3>Dados Gerais</h3>
                                        <hr>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <label for="CodCliente">Cliente:</label>
                                        <select name='fCodCliente' id="CodCliente" class="form-control select2" onchange="modalCliente(this.value)" required>
                                            <option value=''>Selecione</option>
                                            <option value='add'>Novo Cliente</option>
                                            <?php $sSelected = "";
                                            if($voCliente){
                                                foreach($voCliente as $oCliente){
                                                    if($oProjeto){
                                                        $sSelected = ($oProjeto->getCodCliente() == $oCliente->getCodCliente() || ($oProposta && ($oProposta->getCodCliente() == $oCliente->getCodCliente()))) ? "selected" : "";
                                                    }elseif($_REQUEST['nIdCliente']){
                                                        $sSelected = ($_REQUEST['nIdCliente'] == $oCliente->getCodCliente()) ? "selected" : "";
                                                    } ?>
                                                    <option <?php echo $sSelected?> value='<?php echo $oCliente->getCodCliente()?>'><?php echo $oCliente->getNome()?></option>
                                                    <?php
                                                }
                                            } ?>
                                        </select>
                                    </div>

                                    <?php if(($sOP == 'Alterar') || isset($oProposta) ) { ?>
                                        <input type="hidden" name='fCodServico' id="Servico" value="<?php echo ($oProposta) ? $oProposta->getCodServico() : (($oProjeto)? $oProjeto->getCodServico() : "") ?>"><br>
                                    <?php }else{ ?>
                                        <div class="col-md-2 col-sm-12">
                                            <label for="Servico">Serviço:</label>
                                            <select name='fCodServico' id="Servico"  class="form-control" required >
                                                <option value=''>Selecione</option>
                                                <?php $sSelected = "";
                                                if($voServico){
                                                    foreach($voServico as $oServico){
                                                        if($oProjeto)
                                                            $sSelected = ($oProjeto->getCodServico() == $oServico->getCodServico()) ? "selected" : "";
                                                        else if($oProposta)
                                                            $sSelected = ($oProposta->getCodServico() == $oServico->getCodServico()) ? "selected" : ""; ?>
                                                        <option <?php echo $sSelected?> value='<?php echo $oServico->getCodServico()?>'><?php echo $oServico->getDescServico()?></option>
                                                        <?php
                                                    }
                                                } ?>
                                            </select>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-2 col-sm-12">
                                        <label for="Data">Início:</label>
                                        <div class="input-group col-md-11 col-sm-12">
                                            <input class="form-control date" type='text' id='Data' placeholder='Início' name='fDataInicio' value='<?php echo ($oProjeto) ? $oProjeto->getDataInicioFormatado() : (($sOP == 'Cadastrar') ? date('d/m/Y') : "") ?>' required>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <label for="Metragem">Metragem</label>
                                        <input name="fMetragem" id="Metragem" class="form-control" type='text' placeholder='M²' onkeyup="FormataMoney(this);" value="<?php echo ($oProjeto)? $oProjeto->getMetragemFormatado() : ""?>">
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="Descricao">Descrição:</label>
                                        <textarea name="fDescricao" id="Descricao" class="form-control col-md-12" rows="4"><?php echo ($sOP == "Alterar") ? $oProjeto->getDescricao() :  (($oProposta) ? $oProposta->getDescricao() : "")?></textarea>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="Observacao">Observação</label>
                                        <textarea name="fObservacao" id="Observacao" class="form-control col-md-12" rows="4"><?php echo ($oProjeto)?$oProjeto->getObservacao() : ""?></textarea>
                                    </div>
                                </div>

                                <?php if(in_array($_SESSION['oGrupoUsuario']->getCodGrupoUsuario(),$vPermissao)){ ?>
                                    <div class="form-group col-md-12 col-sm-12">
                                        <h3>Dados Pagamento</h3>
                                        <hr>

                                        <?php
                                        if($voProjetoPagamento){
                                            $i=0;
                                            foreach($voProjetoPagamento as $oProjetoPagamento){
                                                $bPagamento = ($bPagamento == 1) ? 1 : ($oProjetoPagamento->getDataEfetivacao()) ? 1 : 0;
                                                $sDisabled = ($oProjetoPagamento->getDataEfetivacao()) ? "disabled" : ""; ?>
                                                <fieldset name="<?php echo $i++; ?>">
                                                    <div class="form-group row">

                                                        <div class="col-md-3 col-sm-6">
                                                            <input type="hidden" name='fCodProjetoPagamento[]' value="<?php echo $oProjetoPagamento->getCodProjetoPagamento() ?>" <?php echo $sDisabled;?>>
                                                            <label for="FormaPagamento" class="control-label">Forma Pagamento</label>
                                                            <select class="form-control select2" name="fCodFormaPagamento[]" id="FormaPagamento" required <?php echo $sDisabled?> onchange="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-forma-pagamento')" <?php echo $sDisabled;?>>
                                                                <option value="">Selecione</option>
                                                                <?php foreach($voFormaPagamento as $oFormaPagamento){
                                                                    $sSelected = ($oProjetoPagamento->getCodFormaPagamento() == $oFormaPagamento->getCodFormaPagamento()) ? " selected='selected'" : ""; ?>
                                                                    <option <?php echo $sSelected ?> value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-2 col-sm-12">
                                                            <label for="ValorParcela" class="control-label">Valor</label>
                                                            <input type='text' placeholder='Valor' name='fValor[]' onkeyup='FormataMoney(this);' class='form-control' id='ValorParcela' value="<?php echo $oProjetoPagamento->getValorFormatado() ?>" <?php echo $sDisabled;?>>
                                                        </div>

                                                        <div class="col-md-2 col-sm-4">
                                                            <label for="Vencimento" class="control-label">Vencimento</label>
                                                            <input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" max="10"  required value="<?php echo ($oProjetoPagamento)? $oProjetoPagamento->getDataPrevisaoFormatado(): ""?>" onkeyup="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-previsao')" <?php echo $sDisabled;?>>
                                                        </div>

                                                        <div class="col-md-3 col-sm-6">
                                                            <label for="DescPagamento" class="control-label">Parcela</label>
                                                            <input name='fDescPagamento[]' id="DescPagamento" type='text' class='form-control col-xs-2' value='<?php echo $oProjetoPagamento->getDescPagamento() ?>' maxlength="11" onkeyup="repeteValor(this.value,'#botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>','data-descricao-pagamento')" <?php echo $sDisabled;?>>
                                                        </div>
                                                        <div class="col-md-2 col-sm-4">
                                                            <?php if($oProjetoPagamento->getDataEfetivacao()){ ?>
                                                                <label class='control-label'>Pago em</label><br>
                                                                <a class="btn btn-primary btn-sm" href="/<?php echo $oProjetoPagamento->getComprovante() ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Pago em <?php echo $oProjetoPagamento->getDataEfetivacaoFormatado() ?>"><i class="fa fa-check"></i></a>
                                                            <?php }else{ ?>
                                                                <label class='control-label'>Opções</label><br>
                                                                    <button id="botaoConfirma<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-codigo-confirmacao="<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" data-valor="<?php echo $oProjetoPagamento->getValorFormatado();?>" data-forma-pagamento="<?php echo $oProjetoPagamento->getCodFormaPagamento(); ?>" data-previsao="<?php echo $oProjetoPagamento->getDataPrevisaoFormatado();?>" data-descricao-pagamento="<?php echo $oProjetoPagamento->getDescPagamento();?>" data-target="#modalConfirmacaoPagamento" title="Confirmar Pagamento"> <i class="fa fa-usd"></i></button>
                                                                    <a data-toggle="modal" data-target="#confirm-delete" data-link="?action=ProjetoPagamento.processaFormulario&sOP=Excluir&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento(); ?>"><button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Excluir Parcela"> <i class="fa fa-trash"></i></button></a>
                                                            <?php } ?>
                                                            <a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=3&nCodProjetoPagamento=<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>"><button id="GerarRecibo<?php echo $oProjetoPagamento->getCodProjetoPagamento();?>" type="button" class="btn btn-warning btn-sm"  data-toggle="tooltip" title="Gerar Recibo"><i class="fa fa-file-o"></i></button></a>
                                                        </div>

                                                    </div>
                                                </fieldset>
                                            <?php } ?>
                                        <?php } ?>

                                        <div id="divMestre" class="form-group">
                                            <div class="col-md-3 col-sm-6">
                                                <input type='hidden' name='fCodProjetoPagamento[]' value="">
                                                <label for="FormaPagamento" class="control-label">Forma Pagamento</label>
                                                <select class="form-control" name="fCodFormaPagamento[]" id="FormaPagamento" >
                                                    <option value="">Selecione</option>
                                                    <?php foreach($voFormaPagamento as $oFormaPagamento){?>
                                                        <option value="<?php echo $oFormaPagamento->getCodFormaPagamento()?>"><?php echo $oFormaPagamento->getDescricao()?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2 col-sm-12">
                                                <label for="ddi" class="control-label">Valor</label>
                                                <input type='text' placeholder='Valor' name='fValor[]' onkeyup='FormataMoney(this);' class='form-control' id='ValorParcela' >
                                            </div>
                                            <div class="col-md-2 col-sm-12">
                                                <label for="Vencimento" class="control-label">Vencimento</label>
                                                <input type="text" class="form-control date" placeholder="Vencimento" name="fDataPrevisao[]" id="Vencimento" max="10"  value=""/>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <label for='Descricao"+qtdeCampos+"' class="control-label">Parcela</label>
                                                <input name='fDescPagamento[]' type='text'  class='form-control col-xs-2' id='Descricao"+qtdeCampos+"' value='' placeholder="99/99" maxlength="11">
                                            </div>
                                            <div class="col-md-1 col-sm-4">
                                                <br>
                                                <button id="btn" type="button" class="btn btn-success btn-sm addButton" onClick="addCampos()" title="Adiciona Parcela"> <i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <div id="campoPai"></div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div id="divMicroservico"></div>
                            <div id="divEtapa"></div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <div class="form-group col-md-12">
                                <div class="col-sm-offset-5 col-sm-2">
                                    <button type="submit" class="btn btn-lg btn-success"><?php echo $sOP; ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.row -->
</div>
<!-- /.content-wrapper -->

<?php if($sOP == 'Cadastrar' && isset($voProposta)){?>
    <!-- Modal Proposta -->
    <div class="modal fade" id="modalProposta" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="?action=Projeto.preparaFormulario">
                    <div class="modal-header">
                        <h3 class="modal-title">Vincular Proposta</h3>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" name="sOP" value="Cadastrar">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="Proposta" class="control-label">Proposta</label>
                                <select class="form-control select2" name="fCodProposta" id="Proposta" style="width:100%" onchange="if(this.value !== '') $('#btnSalvar').prop('disabled',false);else $('#btnSalvar').prop('disabled',true);" >
                                    <option value="">Selecione</option>
                                    <?php foreach($voProposta as $oPropostaList){?>
                                        <option value="<?php echo $oPropostaList->getCodProposta()?>"><?php echo "{$oPropostaList->getNome()} - {$oPropostaList->getDescricao()}"?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btnSalvar" class="btn btn-success" disabled>Selecionar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="modalConfirmacaoPagamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="?action=ProjetoPagamento.processaFormulario" enctype="multipart/form-data">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Confirmação de Pagamento</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <input type="hidden" name="sOP" value="ConfirmarPagamento">
                    <input type="hidden" id="CodProjetoPagamento" name="nCodProjetoPagamento" value="">
                    <input type="hidden" id="CodFormaPagamento" name="fCodFormaPagamento" value="">
                    <input type="hidden" id="Valor" name="fValor" value="">
                    <input type="hidden" id="DataPrevisao" name="fDataPrevisao" value="">
                    <input type="hidden" id="DescPagamento" name="fDescPagamento" value="">
                    <input type="hidden" id="Origem" name="fOrigem" value="projeto">

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="ContaBancaria" class="control-label">Conta Destino</label>
                            <select class="form-control select2" name="fCodContaBancaria" id="ContaBancaria" style="width:100%" onchange="if(this.value !== '') $('#btnSalvar').prop('disabled',false);else $('#btnSalvar').prop('disabled',true);" >
                                <option value="">Selecione</option>
                                <?php foreach($voContaBancaria as $oContaBancaria){?>
                                    <option value="<?php echo $oContaBancaria->getCodContaBancaria()?>"><?php echo $oContaBancaria->getCodColaborador() . " - " . $oContaBancaria->getBanco() . " - " . $oContaBancaria->getAgencia() . " - " .$oContaBancaria->getConta();?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="DataConfirmacao" class="control-label">Data Confirmação</label>
                            <input class="form-control date" type='text' id='DataConfirmacao' placeholder='Data Confirmação' name='fDataConfirmacao' required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="ContaBancaria" class="control-label">Comprovante</label>
                            <input class="form-control" type='file' id='Comprovante' name='fComprovante'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="submit" id="btnSalvar" class="btn btn-success" disabled>Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Cliente -->
<div class="modal fade" id="modalCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document"  style="width:90%">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Cadastrar Cliente</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="post" name="formCliente" id="formCliente" action="?action=Cliente.processaFormulario">

                <div class="modal-body">
                    <input type="hidden" name="sOP" value="Cadastrar">
                    <input type="hidden" name="fCodCliente" value="">
                    <input type="hidden" name="fHeader" value="?action=Projeto.preparaFormulario&sOP=Cadastrar&fCodProposta=<?php echo ($oProposta) ? $oProposta->getCodProposta():"" ?>">

                    <div class="form-group col-md-2 col-sm-12 row">
                        <label for="CodTipoPessoa">Tipo Pessoa:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <select name='fCodTipoPessoa' id="CodTipoPessoa"  class="form-control CodTipoPessoa"  required >
                                <option value=''>Selecione</option>
                                <option value='1'>Física</option>
                                <option value='2'>Jurídica</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="Nome">Nome/Fantasia:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='Nome' placeholder='Nome/Fantasia' name='fNome' required>
                        </div>
                    </div>
                    <div class="form-group col-md-2 col-sm-12 row">
                        <label for="Identificacao">CPF/CNPJ:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='Identificacao' name='fIdentificacao' required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="email">Email:</label>
                        <div class="input-group col-md-12 col-sm-12">
                            <input class="form-control" type='text' id='email' placeholder='Email' name='fEmail' required>
                        </div>
                    </div>

                    <div class="form-group col-md-2 col-sm-12 row">
                        <label for="cep">CEP:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control cep" type='text' id='cep' placeholder='CEP' name='fCep' required>
                        </div>
                    </div>

                    <div class="form-group col-md-2 col-sm-12 row">
                        <label for="uf">Estado:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='uf' placeholder='Estado' name='fUf' required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="cidade">Cidade:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='cidade' placeholder='Cidade' name='fCidade' required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="bairro">Bairro:</label>
                        <div class="input-group col-md-12 col-sm-12">
                            <input class="form-control" type='text' id='bairro' placeholder='Bairro' name='fBairro' required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="rua">Logradouro:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='rua' placeholder='Logradouro' name='fLogradouro' required>
                        </div>
                    </div>

                    <div class="form-group col-md-1 col-sm-12 row">
                        <label for="Numero">Nº:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='Numero' placeholder='Número' name='fNumero' required>
                        </div>
                    </div>

                    <div class="form-group col-md-3 col-sm-12 row">
                        <label for="Complemento">Complemento:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='Complemento' placeholder='Complemento' name='fComplemento'>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="Telefones">Fones:</label>
                        <div class="input-group col-md-12 col-sm-12">
                            <input class="form-control" type='text' id='Telefones' placeholder='Fones' name='fTelefones' required>
                        </div>
                    </div>

                    <div class="form-group col-md-2 col-sm-12 row">
                        <label for="DataNascimento">Data Nascimento:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control date" type='text' id='DataNascimento' placeholder='Data Nascimento' name='fDataNascimento' required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="RazaoSocial">Razão Social:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='RazaoSocial' placeholder='Razão Social' name='fRazaoSocial' required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="InscricaoEstadual">Insc. Estadual:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='InscricaoEstadual' placeholder='Insc. Estadual' name='fInscricaoEstadual' required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 row">
                        <label for="InscricaoMunicipal">Insc Munic.:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='InscricaoMunicipal' placeholder='Insc Munic.' name='fInscricaoMunicipal' required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12">
                         <label for="IndicadoPor">Indicação:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <select name='fIndicadoPor' id="IndicadoPor" class="form-control select2" style="width:100%" required>

                                <option value=''>Selecione</option>
                                <?php $sSelected = "";
                                if($voIndicacao){
                                    foreach($voIndicacao as $oIndicacao){
                                        if($oCliente){

                                            $sSelected = ($oCliente->getIndicadoPor() == $oIndicacao->getCodIndicacao()) ? "selected" : "";
                                        }
                                        ?>
                                        <option  <?php echo $sSelected?> value='<?php echo $oIndicacao->getCodIndicacao()?>'><?php echo $oIndicacao->getDescricao()?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-1 col-sm-12">
                                    <div class="">
                                        <span class="input-group-btn">
                                        <div style="margin: 25px;"></div>
                                        <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modalIndicacao" data-cliente="<?php echo ($oCliente ? $oCliente->getCodCliente() : "")?>"><i class="fa fa-plus"></i> Novo</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-3" style="padding-top: 15px">
                                    <div class="form-group col-md-12 col-sm-12 radio" >

                                            <div class="checkbox">
                                                <label class="">
                                                    <input name="sEnviarEmail" type="checkbox" value="S">
                                                    <strong>Cliente novo?</strong>
                                                </label>
                                            </div>
                                        </div>

                                </div> 

                    <div class="form-group col-md-12 col-sm-12">
                        <label for="Observacao">Observação:</label>
                        <div class="input-group col-md-11 col-sm-12">
                            <input class="form-control" type='text' id='Observacao' placeholder='Observação' name='fObservacao'>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="form-group col-md-12 col-sm-12">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="botao" class="btn btn-primary">Cadastrar</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEntrega">
    <div class="modal-dialog">
        <div class="modal-content" id="bodyEntrega"></div>
    </div>
</div>

<!-- Modal Indicação -->
   <!-- Modal Indicacao -->
   <div class="modal fade" id="modalIndicacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <header class="modal-header">
                    <h2 class="modal-title" id="divTitulo">Indicação</h2>
                </header>
                <div class="modal-body" id="modalBody">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="control-label" for="Indicacao">Indicação</label>
                            <input type="text" class="form-control" name="fDescricao" id="Descricao" required>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
                <footer class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="gravaIndicacao()">Salvar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </footer>
            </div>
        </div>
    </div>


<!-- fim modal indicação -->
<?php include_once('includes/footer.php')?>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script>

    //Abrir modal com listagem de Propostas
    <?php if($sOP == 'Cadastrar' && !isset($_REQUEST['nCodProposta'])){ ?>
    $(document).ready(function(){
        $('#modalProposta').modal('show');
    });
    <?php } ?>

    $('#Efetivacao').click(function() {
        $('#CodConta').attr('disabled',! this.checked);
        $('#Comprovante').attr('disabled',! this.checked);
    });

    <?php if($oProposta || $oProjeto){ ?>
    $(document).ready(function(){
        recuperaServicoEtapa();
        <?php if ($sDisabled) {?>
        $('#DataInicio').attr('readonly',true);
        <?php } ?>
    });
    <?php } ?>

    function recuperaServicoEtapa(){

        <?php if ($oProposta){ ?>
        <?php if ($sOP == 'Cadastrar'){ ?>

        recuperaConteudoDinamico('',"action=ServicoMicroServico.preparaLista&sOP=Servico&sOP2=Alterar&nIdServico=<?php echo $oProposta->getCodServico() ?>&nCodProposta=<?php echo $oProposta->getCodProposta() ?>&bPagamento=<?php echo $bPagamento; ?>",'divMicroservico');

        recuperaConteudoDinamico('',"action=ServicoEtapa.preparaLista&sOP=Servico&sOP2=Alterar&nIdServico=<?php echo $oProposta->getCodServico() ?>&nCodProposta=<?php echo $oProposta->getCodProposta() ?>&bPagamento=<?php echo $bPagamento; ?>",'divEtapa');

        <?php }else{ ?>

        recuperaConteudoDinamico("","action=ServicoMicroServico.preparaLista&sOP=Servico&sOP2=Alterar&nIdServico=<?php echo $oProjeto->getProposta()->getCodServico() ?>&nCodProposta=<?php echo $oProjeto->getCodProposta() ?>&bPagamento=<?php echo $bPagamento; ?>","divMicroservico");

        recuperaConteudoDinamico("","action=ServicoEtapa.preparaLista&sOP=Servico&sOP2=Alterar&nIdServico=<?php echo $oProjeto->getProposta()->getCodServico() ?>&nCodProposta=<?php echo $oProjeto->getCodProposta() ?>&bPagamento=<?php echo $bPagamento; ?>","divEtapa");

        <?php } ?>
        <?php } ?>



    }

    function repeteValor(textoDigitado,div,attr){
        $(div).attr(attr,textoDigitado);
    }

    $('#modalConfirmacaoPagamento').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let codigo = button.data('codigo-confirmacao');
        let formaPagamento = button.data('forma-pagamento');
        let dataPrevisao = button.data('previsao');
        let valor = button.data('valor');
        let descPagamento = button.data('descricao-pagamento');
        let modal = $(this);

        modal.find('#CodProjetoPagamento').val(codigo);
        modal.find('#CodFormaPagamento').val(formaPagamento);
        modal.find('#DataPrevisao').val(dataPrevisao);
        modal.find('#Valor').val(valor);
        modal.find('#DescPagamento').val(descPagamento);

    });

    var qtdeCampos = 0;
    //te
    function addCampos(){
        var objPai = document.getElementById("campoPai");
        //Criando o elemento DIV;
        var objFilho = document.createElement("div");
        //Definindo atributos ao objFilho:
        objFilho.setAttribute("id","filho"+qtdeCampos);

        //Inserindo o elemento no pai:
        objPai.appendChild(objFilho);
        //Escrevendo algo no filho recém-criado:
        let linhaClone = $('#divMestre').clone();
        // ;

            // linhaClone.appendTo("#filho"+qtdeCampos);
        let botaoRemover = document.createElement('a');
        botaoRemover.classList.add("btn","btn-danger","btn-sm","removeButton");
        botaoRemover.innerHTML = '<i class="fa fa-trash"></i>';
        botaoRemover.setAttribute("onclick","removerCampo("+qtdeCampos+")");
        linhaClone.find('.addButton')[0].parentNode.replaceChild(botaoRemover, linhaClone.find('.addButton')[0])

        linhaClone.appendTo("#filho"+qtdeCampos);
        // <button type="button" class="btn btn-danger btn-sm removeButton" onclick="removerCampo(' +  +')" ></button>
        qtdeCampos++;
         
         $('.date').mask('00/00/0000');
    }

    function removerCampo(id){
        var objPai = document.getElementById("campoPai");
        var objFilho = document.getElementById("filho"+id);
        //Removendo o DIV com id específico do n-pai:
        objPai.removeChild(objFilho);
    }

    $('#confirm-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var sLink = button.data('link');
        var modal = $(this);
        modal.find('#ok').attr('href', sLink);
    });

    $('#CodTipoPessoa').change(function(){
        if($(this).val() === '1'){
            $("#RazaoSocial").prop('disabled', true).prop('required', false);
            $("#InscricaoEstadual").prop('disabled', true).prop('required', false);
            $("#InscricaoMunicipal").prop('disabled', true).prop('required', false);
            $("#DataNascimento").prop('disabled', false).prop('required', true);
            $('#Identificacao').mask('000.000.000-00', {reverse: true});
            $("label[for = Identificacao]").text("CPF");
        }else{
            $("#RazaoSocial").prop('disabled', false);
            $("#RazaoSocial").prop('required', true);
            $("#InscricaoEstadual").prop('disabled', false);
            $("#InscricaoEstadual").prop('required', true);
            $("#InscricaoMunicipal").prop('disabled', false);
            $("#InscricaoMunicipal").prop('required', true);
            $('#Identificacao').mask('00.000.000/0000-00', {reverse: true});
            $("label[for = Identificacao]").text("CNPJ");
        }
    });

    $("#Identificacao").blur(function() {
        var valor = $('#CodTipoPessoa').val();
        var nIdentificacao = $('#Identificacao').val();
        if(valor === '1'){
            validar(nIdentificacao, 'Identificacao', 'valida_cpf','CPF inválido');
        }else{
            validar(nIdentificacao, 'Identificacao', 'validar_CNPJ','CNPJ inválido');
        }
    });

    $("#email").blur(function() {
        var sEmail = $('#email').val();
        validar(sEmail, 'email', 'validacaoEmail','Email inválido');
    });

    function modalCliente(sAcao){
        if (sAcao === 'add') {
            $('#modalCliente').modal('show');
        }
    }

    $(document).ready(function() {

        $('#alerta').on('hidden.bs.modal', function(event) {
            $(this).removeClass( 'fv-modal-stack' );
            $('body').data( 'fv_open_modals', $('body').data( 'fv_open_modals' ) - 1 );
        });

        $('#alerta').on('shown.bs.modal', function (event) {
            // keep track of the number of open modals
            if ( typeof( $('body').data( 'fv_open_modals' ) ) == 'undefined' ) {
                $('body').data( 'fv_open_modals', 0 );
            }

            // if the z-index of this modal has been set, ignore.
            if ($(this).hasClass('fv-modal-stack')) {
                return;
            }

            $(this).addClass('fv-modal-stack');
            $('body').data('fv_open_modals', $('body').data('fv_open_modals' ) + 1 );
            $(this).css('z-index', 1040 + (10 * $('body').data('fv_open_modals' )));
            $('#modalCliente').not('.fv-modal-stack').css('z-index', 1039 + (10 * $('body').data('fv_open_modals')));
            $('#modalCliente').not('fv-modal-stack').addClass('fv-modal-stack');

        });
    });

    $(document).ready(function() {
        function limpa_formulario_cep() {
            // Limpa valores do formulário de cep.
            $("#rua").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#uf").val("");
        }

        //Quando o campo cep perde o foco.
        $("#cep").blur(function() {
            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');
            //Verifica se campo cep possui valor informado.
            if (cep != "") {
                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;
                //Valida o formato do CEP.
                if(validacep.test(cep)) {
                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#rua").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#uf").val("...");
                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#rua").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#uf").val(dados.uf);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulario_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulario_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulario_cep();
            }
        });
    });

    function calculaData(nDias,nDiv){
        let sDiv = '#previsao'+nDiv;
        // verificar Data
        let dDataInicio = document.getElementById('DataProposta').value;
        let data = moment(dDataInicio, "DD/MM/YYYY");
        let dDataBanco = data.format("YYYY-MM-DD");
        // chamar via ajax a procedure Prazo;
        let action = "?action=Relatorio.calculaPrazo&sOP=Visualizar&nDias="+nDias+"&dData="+dDataBanco;
        $(sDiv).load(action);
        // retornar resultado formatado na div correta
    }

    function abreModalEntrega(){
        let nCodServico = $("#Servico").val();
        recuperaConteudoDinamico("","action=ServicoMicroServico.preparaFormulario&nCodServico="+nCodServico+"&sOP=Cadastrar", "bodyEntrega");
        $("#modalEntrega").modal('show');
    }

    $("#modalEntrega").on('shown.bs.modal', function(){
        $('.my-colorpicker1').colorpicker()
    });

    function processaFormulario(sForm) {

        let form = $("#" + sForm);

        $.ajax({
            type: "POST",
            datatype: "json",
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function () {
                Swal.fire({
                    title: 'Enviando...',
                    didOpen: () => {
                        Swal.showLoading()
                    }
                })
            },
            error: function (oXMLRequest, sErrorType) {
                console.log(form.attr('action'));
                console.log(oXMLRequest.responseText);
                console.log(oXMLRequest.status + ' , ' + sErrorType);
            },
            success: function (dados) {
                dados = jQuery.parseJSON(dados);

                if (dados.sClass === "success") {

                    recuperaConteudo('index.php', '?action=ServicoMicroServico.preparaLista&sOP=Servico&sOP2=Alterar&bPagamento=0&nCodProposta=<?php echo $nCodProposta ?>&nIdServico=' + dados.nCodServico, 'divMicroservico');

                    $('#modalEntrega').modal('hide');
                }

                Swal.fire({
                    icon: dados.sClass,
                    title: dados.sMsg,
                    timer: 2000
                });
            }
        });
    }


   function gravaIndicacao(){
        let nCodCliente = $("#CodCliente").val();
        let sDescricao = $("#Descricao").val();
        let sUrl = '?action=Indicacao.processaFormulario&sOP=Cadastrar&nOrigem=1&fDescricao=' + sDescricao;

        if(nCodCliente){
            let sUrl = '?action=Indicacao.processaFormulario&sOP=Alterar&nOrigem=1&fCodCliente=' + nCodCliente + "&fDescricao=" + sDescricao;
        }

        $.ajax({

            dataType: "json",
            type: "GET",
            url: sUrl,
            beforeSend: function() {
                $("#modalBody").html('<img src="dist/img/load.gif" style="width: 150px" alt="">');
            },
            error: function(oXMLRequest,sErrorType){
                console.log(sUrl);
                console.log(oXMLRequest.responseText);
                console.log(oXMLRequest.status + ', ' + sErrorType);
            },
            success: function(dados){

                $('#modalIndicacao').modal("toggle");
                $("#divIndicado").load("?action=Indicacao.preparaListaAjax&nCodIndicacao=");
                
                $("#idResposta").html("<div class='"+dados.bErro+"'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><p><i class='"+dados.sIcone+"'></i>"+dados.sMsg+"</p></div>");

            }
        });


    }

    $('#modalIndicacao',0,0).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget,0,0);
        let nCodCliente = button.data('cliente');
        $("#CodCliente").val(nCodCliente);
    });

</script>
</body>
</html>
