<?php
header ('Content-type: text/html; charset=UTF-8');
 $voColaborador = $_REQUEST['voColaborador'];
 $oServicoDetalhe = $_REQUEST['oServicoDetalhe'];
 $voProjetoColaborador = $_REQUEST['voProjetoColaborador'];
?>
  <label for="Colaboradores" class="control-label">Colaborador(es):</label>
<select name="fCodColaborador[]" class="form-control select2" multiple >
    <option value="">Selecione</option>
    <?php foreach($voColaborador as $oColaborador) {
        $sSelected = (in_array($oServicoDetalhe->getCodServicoDetalhe() . "||" .$oColaborador->getCodColaborador(),$voProjetoColaborador)) ? " selected " : "";
    ?>
        <option <?php echo $sSelected;?> value="<?php echo $oServicoDetalhe->getCodServicoDetalhe() . "||" .$oColaborador->getCodColaborador()?>"><?php echo $oColaborador->getNome()?></option>
<?php } ?>
</select>
