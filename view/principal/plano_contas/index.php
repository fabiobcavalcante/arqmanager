<?php
 $voPlanoContas = $_REQUEST['voPlanoContas'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Plano de Contas </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Plano de Contas</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Plano de Contas</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formPlanoContas" id="formPlanoContas" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesPlanoContas" onChange="JavaScript: submeteForm('PlanoContas')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=PlanoContas.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Plano de Contas</option>
   						<option value="?action=PlanoContas.preparaFormulario&sOP=Alterar" lang="1">Alterar Plano de Contas selecionado</option>
   						<option value="?action=PlanoContas.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Plano de Contas selecionado</option>
   						<option value="?action=PlanoContas.processaFormulario&sOP=Excluir" lang="2">Excluir Plano de Contas(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voPlanoContas)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('PlanoContas')"><i class="icon fa fa-check"></a></th>
   					<th>Id</th>
					<th>Código</th>
					<th>Descrição</th>
					<th>Visível</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voPlanoContas as $oPlanoContas){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('PlanoContas')" type="checkbox" value="<?=$oPlanoContas->getCodPlanoContas()?>" name="fIdPlanoContas[]"/></td>
  					<td><?php echo $oPlanoContas->getCodPlanoContas()?></td>
					<td><?php echo $oPlanoContas->getCodigo()?></td>
					<td><?php echo $oPlanoContas->getDescricao()?></td>
					<td><?php echo $oPlanoContas->getVisivelFormatado()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Id</th>
									 <th>Código</th>
					 				<th>Descrição</th>
					 				<th>Visível</th>
                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voPlanoContas)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
