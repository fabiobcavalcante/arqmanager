<?php
$sOP = $_REQUEST['sOP'];
$oPlanoContas = $_REQUEST['oPlanoContas'];
$voPai = $_REQUEST['voPai'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Plano de Contas - <?php echo $sOP ?></title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=PlanoContas.preparaLista">Gerenciar Plano de Contas</a>
                <li class="active">Plano de Contas - <?php echo $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Plano de Contas - <?php echo $sOP ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <form method="post" class="form-horizontal" name="formPlanoContas" action="?action=PlanoContas.processaFormulario">
                            <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
                            <input type="hidden" name="fCodPlanoContas" value="<?php echo (is_object($oPlanoContas)) ? $oPlanoContas->getCodPlanoContas() : ""?>" />
                            <div  class="box-body" id="form-group">

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="Codigo">Código:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='Codigo' placeholder='Codigo' name='fCodigo'  required   value='<?php echo ($oPlanoContas) ? $oPlanoContas->getCodigo() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="Descricao">Descrição:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <input class="form-control" type='text' id='Descricao' placeholder='Descricao' name='fDescricao'  required   value='<?php echo ($oPlanoContas) ? $oPlanoContas->getDescricao() : ""?>'/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="Visivel">Pai:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <?php if($voPai){?>
                                            <select name="fIdPai"  class="form-control" id='IdPai' required>
                                                <option value="">Selecione</option>
                                                <option <?php echo ($oPlanoContas && $oPlanoContas->getIdPai()==0) ? " selected "  : ""?>value="0">Nível Zero</option>
                                                <?php foreach($voPai as $oPai){?>
                                                    <option <?php echo ($oPlanoContas && $oPlanoContas->getIdPai()==$oPai->getCodPlanoContas()) ? " selected "  : ""?> value="<?php echo $oPai->getCodPlanoContas()?>"><?php echo $oPai->getDescricao()?></option>
                                                <?php } ?>
                                            </select>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="Visivel">Visível:</label>
                                    <div class="input-group col-md-11 col-sm-12">
                                        <select name="fVisivel"  class="form-control" id='Visivel' required>
                                            <option value="">Selecione</option>
                                            <option <?php echo ($oPlanoContas && $oPlanoContas->getVisivel()==1) ? " selected "  : ""?> value="1">Sim</option>
                                            <option <?php echo ($oPlanoContas && $oPlanoContas->getVisivel()==0) ? " selected "  : ""?> value="0">Não</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script type="text/javascript" language="javascript">

    jQuery(function($){
    });
</script>
</body>
</html>
