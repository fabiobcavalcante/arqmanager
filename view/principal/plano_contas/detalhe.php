﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oPlanoContas = $_REQUEST['oPlanoContas'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Plano de Contas - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Plano de Contas</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=PlanoContas.preparaLista">Gerenciar Plano de Contas</a>
 			<li class="active">Plano de Contas - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Plano de Contas - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="Codigo" class="control-label">Código:</label>
		<p><?php echo ($oPlanoContas) ? $oPlanoContas->getCodigo() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Descricao" class="control-label">Descrição:</label>
		<p><?php echo ($oPlanoContas) ? $oPlanoContas->getDescricao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Visivel" class="control-label">Visível:</label>
		<p><?php echo ($oPlanoContas) ? $oPlanoContas->getVisivelFormatado() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="UsuInc" class="control-label">Incluído por:</label>
		<p><?php echo ($oPlanoContas) ? $oPlanoContas->getUsuInc() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="UsuAlt" class="control-label">Alterado por:</label>
		<p><?php echo ($oPlanoContas) ? $oPlanoContas->getUsuAlt() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=PlanoContas.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
