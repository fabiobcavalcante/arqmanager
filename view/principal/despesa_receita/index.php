<?php
 $voDespesaReceita = $_REQUEST['voDespesaReceita'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Despesa X Receita </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?>/h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Despesa X Receitas</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Despesa X Receita</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formDespesaReceita" id="formDespesaReceita" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesDespesaReceita" onChange="JavaScript: submeteForm('DespesaReceita')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=DespesaReceita.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Despesa X Receita</option>
   						<option value="?action=DespesaReceita.preparaFormulario&sOP=Alterar" lang="1">Alterar Despesa X Receita selecionado</option>
   						<option value="?action=DespesaReceita.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Despesa X Receita selecionado</option>
   						<option value="?action=DespesaReceita.processaFormulario&sOP=Excluir" lang="2">Excluir Despesa X Receita(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voDespesaReceita)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('DespesaReceita')"><i class="icon fa fa-check"></a></th>
   					<th>Cod</th>
					<th>Tipo</th>
					<th>Descrição</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voDespesaReceita as $oDespesaReceita){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('DespesaReceita')" type="checkbox" value="<?=$oDespesaReceita->getCodDespesaReceita()?>" name="fIdDespesaReceita[]"/></td>
  					<td><?php echo $oDespesaReceita->getCodDespesaReceita()?></td>
					<td><?php echo $oDespesaReceita->getCodDespesaReceitaTipo()?></td>
					<td><?php echo $oDespesaReceita->getDescricao()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Cod</th>
					<th>Tipo</th>
					<th>Descrição</th>
                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voDespesaReceita)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
