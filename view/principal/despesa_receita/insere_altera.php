<?php
 $sOP = $_REQUEST['sOP'];
 $oDespesaReceita = $_REQUEST['oDespesaReceita'];

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Despesa X Receita - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=DespesaReceita.preparaLista">Gerenciar Despesa X Receitas</a>
 			<li class="active">Despesa X Receita - <?php echo $sOP?></li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Despesa X Receita - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formDespesaReceita" action="?action=DespesaReceita.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodDespesaReceita" value="<?=(is_object($oDespesaReceita)) ? $oDespesaReceita->getCodDespesaReceita() : ""?>" />
 				<div  class="box-body" id="form-group">
 								<input type='hidden' name='fCodDespesaReceita' value='<?php echo ($oDespesaReceita) ? $oDespesaReceita->getCodDespesaReceita() : ""?>'/>

 <div class="form-group col-md-4 col-sm-6">
					<label for="CodDespesaReceitaTipo">Tipo:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='CodDespesaReceitaTipo' placeholder='Tipo' name='fCodDespesaReceitaTipo'  required  onKeyPress="TodosNumero(event);" value='<?php echo ($oDespesaReceita) ? $oDespesaReceita->getCodDespesaReceitaTipo() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4 col-sm-6">
					<label for="Descricao">Descrição:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'  required   value='<?php echo ($oDespesaReceita) ? $oDespesaReceita->getDescricao() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4 col-sm-6">
					<label for="IncluidoPor">Incluído por:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='IncluidoPor' placeholder='Incluído por' name='fIncluidoPor'  required   value='<?php echo ($oDespesaReceita) ? $oDespesaReceita->getIncluidoPor() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4 col-sm-6">
					<label for="AlteradoPor">Alterado por:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='AlteradoPor' placeholder='Alterado por' name='fAlteradoPor'  required   value='<?php echo ($oDespesaReceita) ? $oDespesaReceita->getAlteradoPor() : ""?>'/>
				</div>
			</div>
					<input type='hidden' name='fAtivo' value='<?php echo ($oDespesaReceita) ? $oDespesaReceita->getAtivo() : "1"?>'/>


 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(function($){
				 });
 </script>
 </body>
 </html>
