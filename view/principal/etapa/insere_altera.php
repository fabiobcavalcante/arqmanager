<?php
$sOP = $_REQUEST['sOP'];
$oEtapa = $_REQUEST['oEtapa'];
?>

<div class="box">
    <div class="modal-header">
        <h3 class="modal-title">Etapa - <?php echo $sOP ?></h3>
    </div>
    <!-- /.box-header -->
    <form method="post" class="form-horizontal" name="formServico" action="?action=Etapa.processaFormulario">
        <input type="hidden" name="sOP" value="<?php echo $sOP?>" >
        <input type="hidden" name="fCodEtapa" value="<?=($oEtapa) ? $oEtapa->getCodEtapa() : ""?>" >
        <div class="modal-body">

            <div class="form-group row">

                <div class="col-md-12">
                    <label for="Descricao">Descrição:</label>
                    <input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao' value='<?php echo ($oEtapa) ? $oEtapa->getDescricao() : ""?>' required>
                </div>
            </div>
            <div class="form-group row">

                <div class="col-md-6">
                    <label for="Ordem">Ordem:</label>
                    <input class="form-control" type='number' name="fOrdem" id='Ordem'  value='<?php echo ($oEtapa) ? $oEtapa->getOrdem() : ""?>' required>
                </div>

                <div class="col-md-6">
                    <label for="Exibir">Exibir</label>
                    <select class="form-control" name="fExibir" id="Exibir" required>
                        <option value="1" <?php echo ($oEtapa && $oEtapa->getExibir() == 1) ? "selected" : ""; ?>>Sim</option>
                        <option value="0" <?php echo ($oEtapa && $oEtapa->getExibir() == 0) ? "selected" : ""; ?>>Não</option>
                    </select>
                </div>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="modal-footer">
            <div class="form-group col-md-12">
                <div class="col-sm-offset-5 col-sm-2">
                    <button type="submit" class="btn btn-lg btn-success"><?php echo $sOP?></button>
                </div>
            </div>
        </div>
    </form>
</div>