<?php
 $voIndicacao = $_REQUEST['voIndicacao'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Indicações </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Indicações</h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li class="active">Gerenciar Indicações</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Indicações</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formIndicacao" id="formIndicacao" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesIndicacao" onChange="JavaScript: submeteForm('Indicacao')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						   						<option value="?action=Indicacao.preparaFormulario&sOP=Alterar" lang="1">Alterar Indicação selecionada</option>
   						<option value="?action=Indicacao.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Indicação selecionada</option>
          <?php if($_SESSION['oUsuarioLP'] ==1 || $_SESSION['oUsuarioLP'] ==2){?>
   						<option value="?action=Indicacao.processaFormulario&sOP=Excluir" lang="2">Excluir Grupo(s) selecionado(s)</option>
		  <?php }?>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voIndicacao)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('Indicacao')"><i class="icon fa fa-check"></a></th>
   					<th class='Titulo'>Id</th>
					<th class='Titulo'>Descrição</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voIndicacao as $oIndicacao){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('Indicacao')" type="checkbox" value="<?php echo $oIndicacao->getCodIndicacao()?>" name="fIdIndicacao[]"/></td>
  					<td><?php echo $oIndicacao->getCodIndicacao()?></td>
					<td><?php echo  $oIndicacao->getDescricao()?></td>
					<td>  <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Ações<span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="?action=Indicacao.preparaFormulario&sOP=Alterar&nCodIndicacao=<?php echo $oIndicacao->getCodIndicacao()?>"><i class="fa fa-pencil-square-o"></i> Alterar</a></li>
                </ul>
            </div></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th></th>
                   <th class='Titulo'></th>
					<th class='Titulo'></th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voIndicacao)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
