<?php
 $voIndicacao = $_REQUEST['voIndicacao'];

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Ranking de Indicações </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Indicações</h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li class="active">Ranking Indicações</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title"><Ranking de Indicações</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formIndicacao" id="formIndicacao" class="formulario">
 			<div class='row'></div>
 			<?php if(is_array($voIndicacao)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th class='Titulo'>Indicador</th>
   					<th class='Titulo'>Indicações</th>
						<?php 	if(in_array(203,$_SESSION['voPermissao'])){ ?>
					  <th class='Titulo'>Recebido</th>
					  <th class='Titulo'>A Receber</th>
					<?php }//	if(in_array(203,$_SESSION[voPermissao])){ ?>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voIndicacao as $oIndicacao){ ?>
   				<tr>
  					<td><?php echo $oIndicacao->descricao?></td>
  					<td><?php echo $oIndicacao->indicacoes?></td>
						<?php 	if(in_array(203,$_SESSION['voPermissao'])){ ?>
					  <td><?php echo ($oIndicacao->total_recebido > 0) ? 'R$'.$oIndicacao->total_recebido : 'R$0,00';?></td>
					  <td><?php echo ($oIndicacao->total_a_receber > 0) ? 'R$'.$oIndicacao->total_a_receber : 'R$0,00';?></td>
				<?php }//	if(in_array(203,$_SESSION[voPermissao])){ ?>
  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th class='Titulo'></th>
                   <th class='Titulo'></th>
									 	<?php 	if(in_array(203,$_SESSION['voPermissao'])){ ?>
									 <th class='Titulo'></th>
									 <th class='Titulo'></th>
								 <?php }//	if(in_array(203,$_SESSION[voPermissao])){ ?>
                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voIndicacao)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
