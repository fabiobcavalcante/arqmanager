<div class="box box-primary">
  <div class="box-header">
    <h3 class="box-title">Serviço Etapa</h3>
  </div>
  <div class="box-body">
<?php
 $voServicoEtapa = $_REQUEST['voServicoEtapa'];

 if(is_array($voServicoEtapa)){ ?>
   <table class="table table-bordered table-striped">
      <thead>
         <tr>
            <th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('Proposta')"><i class="icon fa fa-check"></a></th>
	         <th>Etapa</th>
           <th>Prazo</th>
	         <th>Descrição</th>
	      </tr>
	   </thead>
	   <tbody>
      <?php
      foreach($voServicoEtapa as $oServicoEtapa){
         $nCodEtapa = $oServicoEtapa->getCodEtapa();
         $sDescricao = (is_array($aProjetoServicoMicroServico)) ?  $aProjetoServicoEtapa['descricao'] : "";
         $nPrazo = (is_array($aProjetoServicoMicroServico) && ( (in_array($oServicoEtapa->getCodEtapa(), $aProjetoServicoEtapa['descricao'])))) ? "checked='checked'" : "";
         ?>
         <tr>
            <td style="white-space:nowrap"><input type="checkbox" value="<?php echo $nCodEtapa; ?>" class="foo2"  name="fIdServicoEtapa[]" checked></td>
            <td style="white-space:nowrap"><?php echo $oServicoEtapa->getDescricao()?></td>
            <td><input type="number" class="form-control" name="fPrazo[]" value="" placeholder="Prazo" onkeypress="TodosNumero(event)"></td>
            <td><input type="text" class="form-control" name="fDescricaoEtapa[]" value="<?php echo $sDescricao?>" placeholder="Descrição"></td>
         </tr>
      <?php } ?>
      </tbody>
   </table>
<?php } ?>
</div>
</div>
