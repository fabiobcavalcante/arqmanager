﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oServicoEtapa = $_REQUEST['oServicoEtapa'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Serviço x Etapa - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=ServicoEtapa.preparaLista">Gerenciar Serviço x Etapas</a>
 			<li class="active">Serviço x Etapa - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Serviço x Etapa - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="CodServico" class="control-label">Serviço:</label>
		<p><?php echo ($oServicoEtapa) ? $oServicoEtapa->getCodServico() : ""?></p>
	</div>
 <div class="col-md-2">

 <label for="Ordem" class="control-label">Ordem:</label>
		<p><?php echo ($oServicoEtapa) ? $oServicoEtapa->getOrdem() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Descricao" class="control-label">Descrição:</label>
		<p><?php echo ($oServicoEtapa) ? $oServicoEtapa->getDescricao() : ""?></p>
	</div>
 <div class="col-md-4">

 <label for="ContratoDescricao" class="control-label">Contrato Descrição:</label>
		<p><?php echo ($oServicoEtapa) ? $oServicoEtapa->getDescricaoContrato() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=ServicoEtapa.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
