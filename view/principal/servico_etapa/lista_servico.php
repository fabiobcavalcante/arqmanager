<?php
$voServicoEtapa = $_REQUEST['voServicoEtapa'];
$voSelecaoEtapa = $_REQUEST['vSelecaoEtapa'];
$bPagamento = $_GET['bPagamento'];
$sDisabled = ($bPagamento == 1) ? "disabled" : "";

if(is_array($voServicoEtapa)){ ?>
    <!--suppress HtmlFormInputWithoutLabel -->
    <div class="table-responsive">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <?php if (!$bPagamento){ ?>
                            <th style="width: 1%"><a href="javascript: marcarTodosCheckBoxFormulario('Proposta')"><i class="icon fa fa-check"></a></th>
                        <?php } ?>
                        <th>Etapa</th>
                        <th>Prazo</th>
                        <th>Previsão</th>
                        <th>Descrição</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($voServicoEtapa as $oServicoEtapa){
                        $sChecado = $nPrazo = $sDescricao = "";
                        $nCodEtapa = (int)$oServicoEtapa->getCodServicoEtapa();

                        if($_REQUEST['sOP2']=='Alterar'){
                            if(array_key_exists($nCodEtapa,$voSelecaoEtapa)){
                                $sChecado = "checked";
                                $sDescricao = $voSelecaoEtapa[$nCodEtapa][1];
                                $nPrazo = $voSelecaoEtapa[$nCodEtapa][2];
                                $sDataPrevista = $voSelecaoEtapa[$nCodEtapa][3];
                            }else
                                $sChecado = $sDescricao = $nPrazo = "";

                        }else{
                            $sChechado = "checked";
                            $sDataPrevista = "";
                        }
                        ?>
                        <tr>
                            <?php if (!$bPagamento){ ?>
                                <td style="white-space:nowrap">
                                    <input <?php echo $sChecado; ?> type="checkbox" value="<?php echo $oServicoEtapa->getCodServicoEtapa();?>" class="foo2" name="fIdServicoEtapa[]" id="ChEtapa<?php echo $oServicoEtapa->getCodServicoEtapa();?>">
                                </td>
                            <?php } ?>
                            <td style="white-space:nowrap">
                                <?php echo $oServicoEtapa->getDescricao();?>
                            </td>
                            <td>
                                <div class="col-lg-12">
                                    <input type="number" class="form-control" name="fPrazo[]" id="Prazo<?php echo $oServicoEtapa->getCodServicoEtapa();?>" value="<?php echo $nPrazo?>" placeholder="Prazo" onkeyup="calculaData()" <?php echo $sDisabled; ?>>
                                </div>
                            </td>
                            <td>
                                <div id="Previsao<?php echo $oServicoEtapa->getCodServicoEtapa();?>"><?php echo $sDataPrevista;?></div>
                                <input type="hidden" name="fDataPrevisao[]" id="DataPrevisao<?php echo $oServicoEtapa->getCodServicoEtapa();?>" value="">
                            </td>
                            <td>
                                <input type="text" class="form-control col-lg-12 col-md-12" name="fDescricaoEtapa[]" value="<?php echo $sDescricao?>" placeholder="Descrição" <?php echo $sDisabled; ?>>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- col-lg-12 -->
        </div>
        <!-- .row	 -->
    </div>
    <!-- .table-responsive -->
<?php } ?>

<script>
    function efetivacao(Campo){
        if (Campo) {
            $('#CodConta').attr('disabled',false).attr('required', true);
            $('#Comprovante').attr('disabled',false).attr('required', true);
        } else{
            $('#CodConta').attr('disabled',true).attr('required', false);
            $('#Comprovante').attr('disabled',true).attr('required', false);
        }
    }

    function calculaData(){
        // verificar Data
        let dDataInicio = $("#Data").val();
        let nSoma = 0;

        let elements = document.getElementById("formPropostaProjeto").elements;

        for (let i = 0; i < elements.length; i++) {
            if(elements[i].name === "fIdServicoEtapa[]") {
                let nCodEtapa = elements[i].value;
                let nDias = $('#Prazo' + nCodEtapa).val();
                let sDiv = '#Previsao' + nCodEtapa;
                let sDivCampo = '#DataPrevisao' + nCodEtapa;

                if (nDias !== '') {
                    nSoma = nSoma + parseInt(nDias);
                    // chamar via ajax a procedure Prazo;
                    let action = "?action=Relatorio.calculaPrazo&sOP=Visualizar&sTipo=<?php echo $_SESSION['oEscritorio']->getDiasUteis()?>&nDias="+nSoma+"&dData="+dDataInicio;

                    $.ajax({
                        dataType: "html",
                        type: "GET",
                        url: action,
                        error: function(oXMLRequest,sErrorType){
                            console.log(oXMLRequest.responseText);
                            console.log(oXMLRequest.status+' , '+sErrorType);
                        },
                        success: function(data){
                            $(sDiv).html(data);
                            $(sDivCampo).val(data);
                        }
                    });
                } else{
                    $(sDiv).text("");
                }
            }
        }
    }
</script>
