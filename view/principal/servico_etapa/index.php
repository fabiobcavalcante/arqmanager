<?php
$voServicoEtapa = $_REQUEST['voServicoEtapa'];
$oServico = $_REQUEST['oServico'];
?>
<div class="box">
    <div class="modal-header">
        <h3 class="modal-title">Entregas do Serviço (<?php echo $oServico->getDescServico()?>)</h3>
    </div>
    <!-- /.box-header -->
    <div class="modal-body">
        <div class='form-group col-md-4'>
            <button type="button" class="btn btn-success" onclick="abreModal('ServicoEtapa','Cadastrar','<?php echo $oServico->getCodServico()?>')"><i class="fa fa-plus"></i> Cadastrar Entrega</button>
        </div>

        <div class='row'></div>
        <?php if($voServicoEtapa){?>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th style="width: 1%"></th>
                    <th>Id</th>
                    <th>Descrição</th>
                    <th>Ordem</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($voServicoEtapa as $oServicoEtapa){ ?>
                    <tr>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ações<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a onclick="abreModal('ServicoEtapa','Alterar','<?php echo $oServicoEtapa->getCodServico()?>','<?php echo $oServicoEtapa->getCodServicoEtapa()?>')"><i class="fa fa-edit"></i> Alterar Entrega</a></li>

                                    <li><a onclick="abreModal('MicroServico','','<?php echo $oServicoEtapa->getCodServico()?>','<?php echo $oServicoEtapa->getCodEtapa()?>','<?php echo $oServicoEtapa->getCodServicoEtapa()?>')"><i class="fa fa-list"></i> Gerenciar Microserviços</a></li>

                                    <li><a onclick="abreModal('ServicoEtapa','Excluir','<?php echo $oServicoEtapa->getCodServicoEtapa()?>')"><i class="fa fa-times"></i> Excluir Entrega</a></li>
                                </ul>
                            </div>
                        </td>
                        <td><?php echo $oServicoEtapa->getCodServicoEtapa()?></td>
                        <td><?php echo $oServicoEtapa->getDescricao()?></td>
                        <td><?php echo $oServicoEtapa->getOrdem()?></td>
                    </tr>
                <?php }?>
                </tbody>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Id</th>
                    <th>Descrição</th>
                    <th>Ordem</th>
                </tr>
                </tfoot>
            </table>
        <?php } ?>
    </div>
    <!-- /.box-body -->
</div>