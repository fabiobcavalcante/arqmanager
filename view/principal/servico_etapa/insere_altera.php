<?php
$sOP = $_REQUEST['sOP'];
$oServicoEtapa = $_REQUEST['oServicoEtapa'];
$voEtapa = $_REQUEST['voEtapa'];
if($sOP == 'Alterar')
    $oServico = $oServicoEtapa->getServico();
else {
    $oServico = $_REQUEST['oServico'];
}
?>

<div class="box">

    <form method="post" class="form-horizontal" id="formServicoEtapa" action="?action=ServicoEtapa.processaFormulario" onsubmit="event.preventDefault();processaFormulario('formServicoEtapa')">

        <div class="modal-header">
            <h3 class="modal-title">Etapa de Entrega - <?php echo $sOP ?></h3>
        </div>
        <!-- /.box-header -->

        <div class="modal-body">
            <input type="hidden" name="sOP" value="<?php echo $sOP?>">
            <input type='hidden' name='fCodServicoEtapa' value='<?php echo ($oServicoEtapa) ? $oServicoEtapa->getCodServicoEtapa() : ""?>'>
            <input type='hidden' name='fCodServico' value='<?php echo ($oServicoEtapa) ? $oServicoEtapa->getCodServico() : $oServico->getCodServico()?>'>

            <div class="form-group">
                <div class="col-md-12">
                    <label for="Servico">Serviço:</label>
                    <div class="input-group col-md-11"><?php echo $oServico->getDescServico()?></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-2">
                    <label for="Ordem">Ordem:</label>
                    <input class="form-control" type='text' id='Ordem' placeholder='Ordem' name='fOrdem'  required   value='<?php echo ($oServicoEtapa) ? $oServicoEtapa->getOrdem () : ""?>'>
                </div>

                <div class="col-md-3">
                    <label for="CodEtapa">Etapa</label>
                    <select class="form-control" name="fCodEtapa" id="CodEtapa">
                        <option value="">Selecione</option>
                        <?php foreach ($voEtapa as $oEtapa){
                            $sSelected = ($oServicoEtapa && $oEtapa->getCodEtapa() == $oServicoEtapa->getCodEtapa()) ? "selected" : "";
                            ?>
                            <option value="<?php echo $oEtapa->getCodEtapa() ?>" <?php echo $sSelected ?>><?php echo $oEtapa->getDescricao(); ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-md-7">
                    <label for="Descricao">Descrição:</label>
                    <input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'  required   value='<?php echo ($oServicoEtapa) ? $oServicoEtapa->getDescricao() : ""?>'>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label for="ContratoDescricao">Contrato Descrição:</label>
                    <textarea cols="4" class="form-control" id='ContratoDescricao' placeholder='Descrição de Contrato' name='fDescricaoContrato' ><?php echo ($oServicoEtapa) ? $oServicoEtapa->getDescricaoContrato() : ""?></textarea>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-sm-offset-5 col-sm-2">
                <button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
            </div>
        </div>
    </form>
</div>
