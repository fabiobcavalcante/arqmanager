<?php
$oProducao = $_REQUEST['oColaboradorProducao'];
$voColaboradorPagamento = $_REQUEST['voColaboradorPagamento'];

?>
<header class="modal-header">
    <h2 class="modal-title" id="divTitulo">Detalhar <?php echo $voColaboradorPagamento->eta_descricao[0]?></h2>
</header>
<div class="modal-body" id="envBody">
    <input type="hidden" value="<?php echo $oProducao->cod_producao ?>" name="fCodProducao" id="CodProducao">

    <div class="form-group row">
        <div class="col-md-12">
            <label for="Projeto">Projeto:</label>
            <?php echo "{$oProducao->nome} - {$oProducao->identificacao}" ?>
        </div>
        <div class="col-md-3">
            <label for="Valor">Valor:</label>
            R$<?php echo number_format($oProducao->valor_projeto , 2, ',', '.'); ?>
        </div>
        <div class="col-md-3">
            <label for="Recebido">Recebido: </label>
            R$<?php echo number_format($oProducao->valor_recebido , 2, ',', '.'); ?>
        </div>
        <div class="col-md-3">
            <label for="Receber">Receber: </label>
            R$<?php echo number_format($oProducao->valor_receber , 2, ',', '.'); ?>
        </div>
        <div class="col-md-3">
            <label for="Colaborador">Colaborador: </label>
            <?php
            if($oProducao->percentual){
                echo "{$oProducao->percentual}%";
            }else{
                echo "R$ ".number_format($oProducao->valor_total_colaborador , 2, ',', '.');
            }?>
        </div>


    </div>

    <div class="form-group row">
        <?php	if(is_array($voColaboradorPagamento)){
            ?>
            <table id="lista" class="table table-bordered table-striped">
                <thead>
                <tr>

                    <th>Data</th>
                    <th>Valor</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php

                foreach($voColaboradorPagamento as $oColaboradorPagamento){
                    $nCodColaboradorPagamento = (int)$oColaboradorPagamento->getCodColaboradorPagamento();
                    // $datetime = new DateTime($oPropostaMicroServico->data_previsao_fim_microservico);
                    // $sDataPrevisaoFim = $datetime->format('d/m/Y');
                    ?>
                    <tr>
                        <td><?php echo $oColaboradorPagamento->getDataPagamentoFormatado(); ?></td>
                        <td><?php echo $oColaboradorPagamento->getValorFormatado();?></td>
                        <td>
                            <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modalAlterar" data-CodColaboradorPagamento="<?php echo $nCodColaboradorPagamento; ?>" data-codproducao="" data-datapagamento="<?php echo $oColaboradorPagamento->getDataPagamento(); ?>" data-valor="<?php echo $oColaboradorPagamento->getValor(); ?>"><i class="fa fa-exclamation-circle"></i> Alterar</button>
                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modalExcluir" data-CodColaboradorPagamento="<?php echo $nCodColaboradorPagamento; ?>"><i class="fa fa-check"></i> Excluir</button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>

                    <th>Data</th>
                    <th>Valor</th>
                    <th>Ações</th>
                </tr>
                </tfoot>
            </table>
        <?php }else{ ?>
            <div class="form-group row">
                <h4 class="box-title btn btn-block btn-warning btn-sm-1">NENHUM PAGAMENTO REGISTRADO PARA O COLABORADOR NESTE PROJETO</h4>
            </div>
        <?php } ?>

    </div>

</div>
<!-- /.box-body -->

<footer class="modal-footer">
    <!-- <button type="submit" class="btn btn-success">Salvar</button> -->
    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
</footer>

<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    });
</script>
