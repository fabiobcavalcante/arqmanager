<?php
 $oColaborador = $_REQUEST['oColaborador'];
 $voColaboradorProducao = $_REQUEST['voColaboradorProducao'];
 $voProjetoVinculo = $_REQUEST['voProjetoVinculo'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Produção do Colaborador</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Principal</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Produção</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Produção de <?php echo ($oColaborador) ? $oColaborador->getNome():""?></h3>
             </div>
             <!-- /.box-header -->
						 <div class='row'>
							 <a class="btn btn-app bg-green" title="Vincular Projeto" data-toggle="modal" data-target="#modalVinculo"><i class="fa fa-send-o"></i> VINCULAR PROJETO</a>
						 </div>
             <div class="box-body table-responsive">
 			<div class='row'></div>



			<?php if(is_array($voColaboradorProducao)){
						$nSaldo = 0;
						$nLinha=1;

				?>
				 <div class="box-body table-responsive">
					<table id="lista" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th colspan="5"><div align="center">Projeto</div></th>
							<th colspan="4"><div align="center">Colaborador</div></th>
							<th rowspan="2">Ação</th>
						</tr>
						<tr>
							<th>Cliente</th>
							<th>Identificação</th>
							<th>Valor</th>
							<th>Pago</th>
							<th>Pagar</th>
							<th>%</th>
							<th>Valor</th>
							<th>Pago</th>
							<th>Saldo</th>

					</tr>
						</thead>
						<tbody>
										 <?php foreach($voColaboradorProducao as $oColaboradorProducao){
											// 	if ($oContaMovimentacao->getTipo() == 'E'){
											// 	$nSaldo = $nSaldo + $oContaMovimentacao->getValorReceita();
											// }else {
											// 	$nSaldo = $nSaldo - $oContaMovimentacao->getValorDespesa();
											// }
							//	if ($nLinha ==1) {
								 ?>

								 <tr>

									 <td width="25%"><?php echo $oColaboradorProducao->nome?></td>
									 <td width="15%"><?php echo $oColaboradorProducao->identificacao?></td>
									 <td width="8%"><?php echo $oColaboradorProducao->valor_projeto?></td>
									 <td width="7%"><?php echo $oColaboradorProducao->valor_recebido?></td>
									 <td width="8%"><?php echo $oColaboradorProducao->valor_receber?></td>
									 <td width="7%"><?php echo ($oColaboradorProducao->percentual) ? $oColaboradorProducao->percentual ."%" : "-" ?></td>
									 <td width="8%"><?php echo number_format($oColaboradorProducao->valor_total_colaborador , 2, ',', '.') ; ?></td>
									 <td width="7%"><?php echo number_format($oColaboradorProducao->valor_pago_colaborador , 2, ',', '.'); ?></td>
									 <td width="8%"><?php echo number_format(($oColaboradorProducao->valor_total_colaborador - $oColaboradorProducao->valor_pago_colaborador) , 2, ',', '.')?></td>
									 <td width="7%">
										 <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#modalDetalhar" data-producao="<?php echo $oColaboradorProducao->cod_producao?>"><i style="font-size:18px;" class="fa fa-plus-circle" aria-hidden="true"></i>  Detalhar</button>
									 </td>
								 </tr>
							 <?php } ?>

						</tbody>
							<tfoot>
									 <tr>

										 <th></th>
			 							<th></th>
			 							<th></th>
			 							<th></th>
			 							<th></th>
			 							<th></th>
			 							<th></th>
			 							<th></th>
			 							<th></th>
									 </tr>
									 </tfoot>
								 </table>
										</div>
									<!-- box-body table-responsive -->
								<?php } ?>


							</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>


	 <!-- Modal VINCULO-->
	 <!--
	 <div class='modal fade' id='modalVinculo' tabindex="-1" role='dialog'>
			 <div class='modal-lg modal-dialog'>
					 <div class='modal-content'>
							 <form method="post" class="form-horizontal" name="formColaboradorProducao" action="?action=ColaboradorProducao.processaFormulario">
									 <input type="hidden" name="sOP" value="Cadastrar">
									 <input type="hidden" name="fCodColaborador" value="<?php echo $oColaborador->getCodColaborador(); ?>">

									 <div class="modal-header" style="color:#FFF; background-color: #696969;; border-top-left-radius: 2px; border-top-right-radius: 2px;">
											 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											 <h4 class="modal-title">Vincular Projeto x Colaborador</h4>
									 </div>

									 <div class="modal-body">

											 <div class="form-group row">

													 <div class="col-md-12">
															 <label for="CodProjeto">Projeto:</label>
															 <select id="CodProjeto" name='fCodProjeto' id="CodProjeto" class="form-control select2" style="width:95%" required >
																	 <option value=''>Selecione</option>
																	 <?php $sSelected = "";
																	 foreach($voProjetoVinculo as $oProjetoVinculo){ ?>
																		 <option value='<?php echo $oProjetoVinculo->cod_projeto?>'><?php echo "{$oProjetoVinculo->nome} - {$oProjetoVinculo->identificacao}"?></option>
														 <?php } ?>
															 </select>
													 </div>

													 <div class="col-md-6">
															 <label for="Descricao">Percentual:</label>
															 <div class="input-group col-md-11 col-sm-12">
																	 <input class="form-control" maxlength="2" type='number' id='Percentual' placeholder='%' onblur="valorPercentual()" 	name='fPercentual' min='0' max='50' value=''/>
															 </div>
													 </div>

													 <div class="col-md-6">
															 <label for="Valor">Valor:</label>
															 <div class="input-group col-md-11 col-sm-12">
																	 <input class="form-control money" type='text' id='Valor' placeholder='Valor' name='fValor' onblur="valorPercentual()" value=''/>
															 </div>
													 </div>
											 </div>
									 </div>
									 <div class="modal-footer">
											 <button type="submit" class="btn btn-lg btn-success">Cadastrar</button>
									 </div>

							 </form>
					 </div>
			 </div>
	 </div> -->


	 <!-- FIM Modal VINCULO-->
	 <!-- Modal Detalhar -->
 	<div class="modal fade" id="modalDetalhar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 			<div class="modal-dialog modal-lg" role="document">
 					<div id="modalProducao" class="modal-content">

 					</div>
 			</div>
 	</div>

   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script>
 function valorPercentual() {
		 let percentual = $('#Percentual').val();
		 let valor = $('#Valor').val();

		 if(valor!==""){
			 $('#Valor').attr('disabled',false);
			 $('#Percentual').attr('disabled',true);
		 }else if(percentual!==""){
		 		 $('#Valor').attr('disabled',true);
				 $('#Percentual').attr('disabled',false);
		 }else{
			 $('#Valor').attr('disabled',false);
			 $('#Percentual').attr('disabled',false);
		 }
 }

  $('#modalDetalhar',0,0).on('show.bs.modal', function (event) {
 	 	let button = $(event.relatedTarget,0,0);
	 	 let nCodProducao = button.data('producao');
	 	 let sLink = "?action=ColaboradorPagamento.preparaLista&sOP=Producao&fCodProducao=" + nCodProducao;
	 	 $("#modalProducao").html('<img src="dist/img/load.gif" style="width: 150px" alt="">').load(sLink);

  });

 </script>
 </body>
 </html>
