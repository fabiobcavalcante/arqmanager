<?php
$voPropostaMicroServico = $_REQUEST['voPropostaMicroServico'];
$oEtapa = $_REQUEST['oEtapa'];
$oProjeto = $_REQUEST['oVProjeto'];
?>
<header class="modal-header">
    <h3 class="modal-title" id="divTitulo"><?php echo "$oProjeto->nome [$oProjeto->identificacao] - {$oEtapa->getDescricao()}"?></h3>
</header>
<div class="modal-body" id="envBody">

    <div class="form-group row">
        <?php if($_REQUEST['bCalendario']){ ?>
            <?php if(is_array($voPropostaMicroServico)){ ?>
                <table id="lista" class="table table-bordered table-striped">
                    <thead>
                    <tr>

                        <th>Descrição</th>
                        <th>Prazo em dias</th>
                        <th>Início</th>
                        <th>Fim</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach($voPropostaMicroServico as $oPropostaMicroServico){
                        $nCodMicroservico = (int)$oPropostaMicroServico->cod_microservico;

                        if ($oPropostaMicroServico->data_previsao_inicio_microservico || $oPropostaMicroServico->data_previsao_fim_microservico){
                            $datetime = new DateTime($oPropostaMicroServico->data_previsao_inicio_microservico);
                            $sData = $datetime->format('d/m/Y');
                            $datetime = new DateTime($oPropostaMicroServico->data_previsao_inicio_microservico);
                            $sDataPrevisaoInicio = $datetime->format('d/m/Y');
                            $datetime = new DateTime($oPropostaMicroServico->data_previsao_fim_microservico);
                            $sDataPrevisaoFim = $datetime->format('d/m/Y');
                        } else{
                            $sDataPrevisaoInicio = "";
                            $sDataPrevisaoFim = "";
                        }
                        ?>
                        <tr>
                            <td><?php echo $oPropostaMicroServico->descricao; ?></td>
                            <?php if(!$oPropostaMicroServico->data_efetivacao_fim_microservico){?>
                                <td><?php echo $oPropostaMicroServico->dias;?></td>
                                <td><?php echo $sDataPrevisaoInicio ?></td>
                                <td><?php echo $sDataPrevisaoFim ?></td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modalJustificar" data-microservico="<?php echo $oPropostaMicroServico->cod_microservico; ?>" data-proposta="<?php echo $oPropostaMicroServico->cod_proposta; ?>"><i class="fa fa-exclamation-circle"></i> Justificar</button>

                                    <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#modalConcluir"   data-microservico="<?php echo $oPropostaMicroServico->cod_microservico; ?>" data-proposta="<?php echo $oPropostaMicroServico->cod_proposta; ?>" data-obs="<?php echo $oPropostaMicroServico->justificativa_microservico; ?>" data-dataprevista="<?php echo $sDataPrevisaoFim; ?>"><i class="fa fa-check"></i> Concluir</button>
                                </td>
                            <?php }else{ ?>
                                <td><?php echo $oPropostaMicroServico->dias;?></td>
                                <td><?php echo $sDataPrevisaoInicio ?></td>
                                <td><?php echo $sDataPrevisaoFim ?></td>
                                <td>
                                    <?php
                                    $datetime = new DateTime($oPropostaMicroServico->data_efetivacao_fim_microservico);
                                    $sDataEfetivacaoFim = $datetime->format('d/m/Y');
                                    ?>
                                    <i class="fa fa-check"></i> Concluido em <?php echo $sDataEfetivacaoFim; ?>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        <?php }else{ ?>
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <p>Calendário do projeto não definido pela área responsável!</p>
                </div>
                <?php if ($_REQUEST['bPermissaoCalendario']){ ?>
                    <a href="?action=Projeto.preparaFormulario&sOP=Calendario&nCodProjeto=<?php echo $oProjeto->cod_projeto ?>" class="btn btn-primary"><i class="fa fa-calendar"></i> Definir</a>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

</div>
<!-- /.box-body -->
<footer class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
</footer>

<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    });
</script>
