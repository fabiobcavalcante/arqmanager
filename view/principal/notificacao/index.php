<?php
$voNotificacao = $_REQUEST['voNotificacao'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Notificações Enviadas</title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li class="active">Notificações Enviadas</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Notificações Enviadas</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <?php if($voNotificacao){?>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Cliente</th>
                                        <th>Descrição</th>
                                        <th>Data</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($voNotificacao as $oNotificacao){ ?>
                                        <tr>
                                            <td><?php echo $oNotificacao->nome; ?></td>
                                            <td><?php echo $oNotificacao->descricao; ?></td>
                                            <td><?php echo maskInvData($oNotificacao->data_hora,1); ?></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Id</th>
                                        <th>Título</th>

                                    </tr>
                                    </tfoot>
                                </table>
                            <?php } ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
</body>
</html>
