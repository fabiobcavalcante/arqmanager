<?php
$oPropostaMicroServico = $_REQUEST['oPropostaMicroServico'];
$oMicroServico = $oPropostaMicroServico->getServicoMicroservico();
$oProjeto = $_REQUEST['oProjeto'];
?>
<form action="?action=PropostaMicroServico.processaFormulario&sOP=Concluir" method="post" id="formConcluir">
    <header class="modal-header">
        <h2 class="modal-title">Concluir Serviço</h2>
    </header>
    <div class="modal-body" id="modalBody">
        <input type="hidden" value="<?php echo $oPropostaMicroServico->getCodProposta()?>" name="fCodProposta" id="CodProposta">
        <input type="hidden" value="<?php echo $oPropostaMicroServico->getCodMicroservico()?>" name="fCodMicroservico" id="Microservico1">
        <div class="form-group row">
            <div class="col-md-6">
                <label class="control-label" for="DataPrevista">Data Prevista: </label>
                <input type="text" value="<?php echo $oPropostaMicroServico->getDataPrevisaoFimFormatado()?>" class="form-control date" id="DataPrevista" readonly>
            </div>

            <div class="col-md-6">
                <label class="control-label" for="DataConclusao">Data de Conclusão</label>
                <input type="text" class="form-control date" name="fDataConclusao" id="DataConclusao" onblur="FormataStringData(this.value)" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label class="control-label" for="Observacao">Observação</label>
                <select name='fObservacao' id="Observacao" class="form-control select2" style="width:95%" required>
                    <option value=''>Selecione</option>
                    <option <?php echo ($oPropostaMicroServico->getObservacao() && ($oPropostaMicroServico->getObservacao() == 'Atraso por alta demanda')) ? " selected " : ""?> value='Atraso por alta demanda'>Atraso por alta demanda</option>
                    <option <?php echo ($oPropostaMicroServico->getObservacao() && ($oPropostaMicroServico->getObservacao() == 'Dificuldade para reunir com o cliente')) ? " selected ": ""?> value='Dificuldade para reunir com o cliente'>Dificuldade para reunir com o cliente</option>
                    <option <?php echo ($oPropostaMicroServico->getObservacao() && ($oPropostaMicroServico->getObservacao() == 'Outro')) ? " selected ": ""?> value='Outro'>Outro</option>
                </select>
            </div>
            <?php if($oMicroServico->getOrdem() ==0){?>
                <div class="col-md-6">
                    <label class="control-label" for="Metragem">Metragem(M²)</label>
                    <input type="text" class="form-control" onkeyup="FormataMoney(this);" value="<?php echo ($oProjeto) ? $oProjeto->getMetragemFormatado() : ""?>" name="fMetragem" id="Metragem" required>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- /.box-body -->
    <footer class="modal-footer">
        <button type="submit" class="btn btn-success">Enviar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </footer>
</form>
<script>
    $(function () {
        //Máscaras]
        $('.date').mask('00/00/0000');
    });
</script>
