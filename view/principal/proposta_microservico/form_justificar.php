<?php
$oPropostaMicroServico = $_REQUEST['oPropostaMicroServico'];
$oProjeto = $_REQUEST['oProjeto'];
?>
<form action="?action=PropostaMicroServico.processaFormulario&sOP=Justificar" method="post" id="formJustificar">
    <header class="modal-header">
        <h2 class="modal-title" id="divTitulo">Justificar</h2>
    </header>
    <div class="modal-body" id="jusBody">
        <input type="hidden" name="fCodProposta" id="CodProposta" value="<?php echo $oProjeto->getCodProposta() ?>">
        <input type="hidden" name="fCodProjeto" id="CodProjeto" value="<?php echo $oProjeto->getCodProjeto() ?>">

        <input type="hidden" name="fCodMicroservico" id="CodMicroServico" value="<?php echo $oPropostaMicroServico->getCodMicroServico() ?>">
        <input type="hidden" name="fCodEtapa" id="CodEtapa" value="<?php echo $oPropostaMicroServico->getServicoMicroservico()->getCodEtapa() ?>">


        <div class="form-group row">
            <div class="col-md-12">
                <label class="control-label" for="Observacao">Observação</label>
                <select name='fObservacao' id="Observacao" class="form-control" required>
                    <option value=''>Selecione</option>
                    <option value='Atraso por alta demanda'>Atraso por alta demanda</option>
                    <option value='Dificuldade para reunir com o cliente'>Dificuldade para reunir com o cliente</option>
                    <option value='Outro'>Outro</option>
                </select>
            </div>
        </div>

    </div>
    <!-- /.box-body -->
    <footer class="modal-footer">
        <button type="submit" class="btn btn-success">Enviar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </footer>
</form>

<script>
    $(document).ready(function() {
        $("#Observacao").val('<?php echo $oPropostaMicroServico->getObservacao() ?>');
    });
</script>