<?php
$voPropostaFracionada = $_REQUEST['voPropostaFracionada'];
$sum=0;
?>
<div class="table-responsive">
    <table id="lista" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th></th>
            <th style="white-space: nowrap"><label for="NumeroEntrega">Nº Entrega</label></th>
            <th><label for="Descricao">Descrição</label></th>
            <th><label for="Percentual">Percentual</label></th>
            <th style="white-space: nowrap"><label for="DataPrevista">Data Prevista</label></th>
            <th><label for="Valor">Valor</label></th>
            <th><label for="Parcela">Parcelas</label></th>
            <th style="white-space: nowrap"><label for="IndiceCorrecao">% Correção</label></th>
            <th></th>
        </tr>
        </thead>
        <tbody id="campoPaiFracionada">

        <?php if ($voPropostaFracionada){ ?>
            <?php foreach($voPropostaFracionada as $oPropostaFracionada){ ?>
                <input type="hidden" name="fCodFracionamento[]" value="<?php echo $oPropostaFracionada->getCodFracionamento() ?>">
                <tr id="filhoFracionada<?php echo $sum; ?>">
                    <td style="width: 5%"><button type="button" class="btn btn-danger btn-sm removeButton" onclick="removerCampo(<?=$sum; ?>)"><i class="fa fa-minus"></i> </button> </td>
                    <td style="width: 5%"><input type="text" class="form-control" name="fNumeroEntrega[]" id="NumeroEntrega" value="<?php echo $oPropostaFracionada->getNumeroEntrega() ?>"></td>
                    <td style="width: 5%"><input type="text" class="form-control" name="fDescricaoEntrega[]" id="Descricao" value="<?php echo $oPropostaFracionada->getDescricao() ?>"></td>
                    <td style="width: 5%"><input type="text" class="form-control" name="fPercentual[]" id="Percentual" value="<?php echo $oPropostaFracionada->getPercentualFormatado() ?>"></td>
                    <td style="width: 5%"><input type="text" class="form-control date" name="fDataPrevista[]" id="DataPrevista" value="<?php echo $oPropostaFracionada->getDataPrevistaFormatado() ?>"></td>
                    <td style="width: 5%"><input type="text" class="form-control money" name="fValor[]" id="Valor" value="<?php echo $oPropostaFracionada->getValorFormatado() ?>"></td>
                    <td style="width: 5%"><input type="text" class="form-control" name="fParcelas[]" id="Parcela" value="<?php echo $oPropostaFracionada->getParcelas() ?>"></td>
                    <td style="width: 5%"><input type="text" class="form-control" name="fIndiceCorrecao[]" id="IndiceCorrecao" value="<?php echo $oPropostaFracionada->getIndiceCorrecao() ?>"></td>
                </tr>
                <?php $sum+=1; } ?>
        <?php } ?>
        <input type="hidden" name="fCodFracionamento[]">
        <tr>
            <td style="width: 5%"><button type="button" class="btn btn-success btn-sm" onclick="addCamposFracionada();"><i class="fa fa-plus"></i> </button> </td>
            <td style="width: 5%"><input type="text" class="form-control" name="fNumeroEntrega[]" id="NumeroEntrega"></td>
            <td style="width: 30%"><input type="text" class="form-control" name="fDescricaoEntrega[]" id="Descricao"></td>
            <td style="width: 5%"><input type="text" class="form-control" name="fPercentual[]" id="Percentual"></td>
            <td style="width: 15%"><input type="text" class="form-control date" name="fDataPrevista[]" id="DataPrevista"></td>
            <td style="width: 15%"><input type="text" class="form-control money" name="fValor[]" id="Valor"></td>
            <td style="width: 5%"><input type="text" class="form-control" name="fParcelas[]" id="Parcela"></td>
            <td style="width: 5%"><input type="text" class="form-control" name="fIndiceCorrecao[]" id="IndiceCorrecao"></td>
        </tr>
        </tbody>
    </table>

</div>

<script>
    $(function () {
        $('.date').mask('00/00/0000');
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
    });

    var qtdeCampos = <?php echo $sum;?>;

    function addCamposFracionada(){
        var objPai = document.getElementById("campoPaiFracionada");
        //Criando o elemento DIV;
        var objFilho = document.createElement("tr");
        //Definindo atributos ao objFilho:
        objFilho.setAttribute("id","filhoFracionada"+qtdeCampos);

        //Inserindo o elemento no pai:
        objPai.appendChild(objFilho);
        //Escrevendo algo no filho recém-criado:

        document.getElementById("filhoFracionada"+qtdeCampos).innerHTML =
            '<input type="hidden" name="fCodFracionamento[]">' +
            "<td><button type='button' class='btn btn-danger btn-sm removeButton' onclick='removerCampoFracionada(" + qtdeCampos + ");'><i class='fa fa-minus'></i> </button> </td>" +
            '<td><input type="text" class="form-control" name="fNumeroEntrega[]" id="NumeroEntrega"></td>' +
            '<td><input type="text" class="form-control" name="fDescricaoEntrega[]" id="Descricao"></td>' +
            '<td><input type="text" class="form-control" name="fPercentual[]" id="Percentual"></td>' +
            '<td><input type="text" class="form-control date" name="fDataPrevista[]" id="DataPrevista"></td>' +
            '<td><input type="text" class="form-control money" name="fValor[]" id="Valor"></td>' +
            '<td><input type="text" class="form-control" name="fParcelas[]" id="Parcela"></td>' +
            '<td><input type="text" class="form-control" name="fIndiceCorrecao[]" id="IndiceCorrecao"></td>';

        qtdeCampos++;
    }

    function removerCampoFracionada(id){
        var objPai = document.getElementById("campoPaiFracionada");
        var objFilho = document.getElementById("filhoFracionada"+id);
        //Removendo o DIV com id específico do n-pai:
        var removido = objPai.removeChild(objFilho);
    }
</script>