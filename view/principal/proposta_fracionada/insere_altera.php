<?php
 $sOP = $_REQUEST['sOP'];
 $oPropostaFracionada = $_REQUEST['oPropostaFracionada'];
 
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Entregas - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->
 
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <header>
 	<?php include_once("controle/includes/topo.php")?>
 	<?php include_once("controle/includes/menu.php")?> 
 </header>
 <div class="container-fluid">
    
   	<div class="content">
 	<ol class="breadcrumb"> <strong>Voc&ecirc; est&aacute; aqui:</strong>
 		<li><a href="index.php">Home</a></li>
 		<li><a href="?action=PropostaFracionada.preparaLista">Gerenciar Entregass</a> &gt; <strong><?php echo $sOP?> Entregas</strong></li>
 	</ol>
      
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Entregas</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formPropostaFracionada" action="?action=PropostaFracionada.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodFracionamento" value="<?=(is_object($oPropostaFracionada)) ? $oPropostaFracionada->getCodFracionamento() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Entregas">
 		
 							<input type='hidden' name='fCodFracionamento' value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getCodFracionamento() : ""?>'/>
				
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodProposta">Proposta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodProposta' placeholder='Proposta' name='fCodProposta'  required  onKeyPress="TodosNumero(event);" value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getCodProposta() : ""?>'/>
				</div>
				</div>
				<br>
				
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="NumeroEntrega">Entrega:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='NumeroEntrega' placeholder='Entrega' name='fNumeroEntrega'  required  onKeyPress="TodosNumero(event);" value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getNumeroEntrega() : ""?>'/>
				</div>
				</div>
				<br>
				
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Percentual">Percentual:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Percentual' placeholder='Percentual' name='fPercentual'  required   value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getPercentualFormatado() : ""?>'/>
				</div>
				</div>
				<br>
				
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DataPrevista">Data:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DataPrevista' placeholder='Data' name='fDataPrevista'  required   value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getDataPrevistaFormatado() : ""?>'/>
				</div>
				</div>
				<br>
				
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DataEfetivacao">Efetivação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DataEfetivacao' placeholder='Efetivação' name='fDataEfetivacao'  required   value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getDataEfetivacaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>
				
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Valor">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Valor' placeholder='Valor' name='fValor'  required   value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getValorFormatado() : ""?>'/>
				</div>
				</div>
				<br>
				
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="IndiceCorrecao">Índice Atualização:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='IndiceCorrecao' placeholder='Índice Atualização' name='fIndiceCorrecao'  required   value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getIndiceCorrecaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>
				
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ValorFinal">Valor Final:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ValorFinal' placeholder='Valor Final' name='fValorFinal'  required   value='<?= ($oPropostaFracionada) ? $oPropostaFracionada->getValorFinalFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 		
         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-success" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
  		
 		<script type="text/javascript" language="javascript">
  			
			 jQuery(function($){ 
			$("#Percentual").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#DataPrevista").mask("99/99/9999");
			$("#DataEfetivacao").mask("99/99/9999");
			$("#Valor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#IndiceCorrecao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#ValorFinal").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>  
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("controle/includes/mensagem.php")?>
 <!-- end .container --></div>
 	<footer>
 		<?php require_once("controle/includes/rodape.php")?>
 	</footer>
 </body>
 <!-- InstanceEnd --></html>