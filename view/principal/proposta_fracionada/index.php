<?php
 $voPropostaFracionada = $_REQUEST['voPropostaFracionada'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista de Entregass</title>
 <!-- InstanceEndEditable -->
 	<link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
 	<link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
 	<link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
 	<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 	<link href="css/style.css" rel="stylesheet" type="text/css">
 	<link rel="icon" href="/gdg2/docs/gerador/img/favicon_gerador.png" />
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 	<header>
 		<?php include_once("controle/includes/topo.php")?>
 		<?php include_once("controle/includes/menu.php")?>  
 	</header>
 <div class="container-fluid">
   	<div class="content">
 	<section>
     <ol class="breadcrumb"> <strong>Voc&ecirc; est&aacute; aqui:</strong>
 		<li><a href="index.php">Home</a></li>
 		<li><a href="?action=PropostaFracionada.preparaLista">Gerenciar Entregass</a></li>
 	</ol>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Entregass</h3>
       <!-- InstanceEndEditable -->
     
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formPropostaFracionada" id="formPropostaFracionada" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><div class="col-md-4"><select class="form-control Acoes" name="acoes" id="acoesPropostaFracionada" onChange="JavaScript: submeteForm('PropostaFracionada')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=PropostaFracionada.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Entregas</option>
   						<option value="?action=PropostaFracionada.preparaFormulario&sOP=Alterar" lang="1">Alterar Entregas selecionado</option>
   						<option value="?action=PropostaFracionada.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Entregas selecionado</option>
   						<option value="?action=PropostaFracionada.processaFormulario&sOP=Excluir" lang="2">Excluir Entregas(s) selecionado(s)</option>
   					</select></div></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voPropostaFracionada)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('PropostaFracionada')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Id</th>
					<th class='Titulo'>Proposta</th>
					<th class='Titulo'>Entrega</th>
					<th class='Titulo'>Percentual</th>
					<th class='Titulo'>Data</th>
					<th class='Titulo'>Efetiva��o</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>Índice atualiza��o</th>
					<th class='Titulo'>Valor final</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voPropostaFracionada as $oPropostaFracionada){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('PropostaFracionada')" type="checkbox" value="<?=$oPropostaFracionada->getCodFracionamento()?>" name="fIdPropostaFracionada[]"/></td>
  					<td><?= $oPropostaFracionada->getCodFracionamento()?></td>
					<td><?= $oPropostaFracionada->getCodProposta()?></td>
					<td><?= $oPropostaFracionada->getNumeroEntrega()?></td>
					<td><?= $oPropostaFracionada->getPercentualFormatado()?></td>
					<td><?= $oPropostaFracionada->getDataPrevistaFormatado()?></td>
					<td><?= $oPropostaFracionada->getDataEfetivacaoFormatado()?></td>
					<td><?= $oPropostaFracionada->getValorFormatado()?></td>
					<td><?= $oPropostaFracionada->getIndiceCorrecaoFormatado()?></td>
					<td><?= $oPropostaFracionada->getValorFinalFormatado()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voPropostaFracionada)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{ 
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );	
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('PropostaFracionada');
  	 </script>
       
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("controle/includes/mensagem.php")?>
 <!-- end .container --></div>
 	<footer>
 		<?php require_once("controle/includes/rodape.php")?>
 	</footer>
 </body>
 <!-- InstanceEnd --></html>