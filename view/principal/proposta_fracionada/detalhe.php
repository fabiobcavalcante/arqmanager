﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oPropostaFracionada = $_REQUEST['oPropostaFracionada'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Entregas - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->
 
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 	 <?php include_once("controle/includes/topo.php")?>
      <?php include_once("controle/includes/menu.php")?>  
 <div class="container-fluid">
 
   <div class="content">
       <ol class="breadcrumb"> <strong>Voc&ecirc; est&aacute; aqui:</strong>
 		<li><a href="index.php">Home</a></li>
 		<li><a href="?action=PropostaFracionada.preparaLista">Gerenciar Entregass</a> &gt; <strong><?php echo $sOP?> Entregas</strong></li>
 	</ol>
 	
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Entregas</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">
 						
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Proposta:&nbsp;</strong><?= ($oPropostaFracionada) ? $oPropostaFracionada->getCodProposta() : ""?></div>
				</div>
				<br>
				
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Entrega:&nbsp;</strong><?= ($oPropostaFracionada) ? $oPropostaFracionada->getNumeroEntrega() : ""?></div>
				</div>
				<br>
				
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Percentual:&nbsp;</strong><?= ($oPropostaFracionada) ? $oPropostaFracionada->getPercentual() : ""?></div>
				</div>
				<br>
				
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data:&nbsp;</strong><?= ($oPropostaFracionada) ? $oPropostaFracionada->getDataPrevista() : ""?></div>
				</div>
				<br>
				
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Efetivação:&nbsp;</strong><?= ($oPropostaFracionada) ? $oPropostaFracionada->getDataEfetivacao() : ""?></div>
				</div>
				<br>
				
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor:&nbsp;</strong><?= ($oPropostaFracionada) ? $oPropostaFracionada->getValor() : ""?></div>
				</div>
				<br>
				
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Índice Atualização:&nbsp;</strong><?= ($oPropostaFracionada) ? $oPropostaFracionada->getIndiceCorrecao() : ""?></div>
				</div>
				<br>
				
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor Final:&nbsp;</strong><?= ($oPropostaFracionada) ? $oPropostaFracionada->getValorFinal() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2" style="margin-bottom: 20px; "><a class="btn btn-success" href="?action=PropostaFracionada.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("controle/includes/mensagem.php")?>
 <!-- end .container --></div>
 	<footer>
 		<?php require_once("controle/includes/rodape.php")?>
 	</footer>
 </body>
 <!-- InstanceEnd --></html>