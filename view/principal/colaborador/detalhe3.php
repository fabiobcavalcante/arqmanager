<?php
$ts = gmdate("D, d M Y H:i:s") . " GMT";
header("Expires: $ts");
header("Last-Modified: $ts");
header("Pragma: no-cache");
header("Cache-Control: no-cache, must-revalidate");
//header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');

$sOP = $_REQUEST['sOP'];
$oColaborador = $_REQUEST['oColaborador'];
$voProjeto = $_REQUEST['voProjetoColaborador'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Colaborador - Detalhe</title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>

    <style>
        .label {
            cursor: pointer;
        }
        .progress {
            display: none;
            margin-bottom: 1rem;
        }
        .alert {
            display: none;
        }
        .img-container img {
            max-width: 100%;
        }

    </style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="bower_components/cropper/css/cropper.css">
    <?php include_once('includes/menu.php')?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Colaborador.preparaLista">Gerenciar Colaboradores</a>
                <li class="active">Colaborador - <?php echo $sOP?></li>
            </ol>

        </section>
        <?php include_once('includes/mensagem.php')?>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Colaborador - Detalhe</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="box-header with-border">
                            <h4 class="box-title">Colaborador</h4>
                        </div>
                        <div class="col-md-6">
                            <label for="Nome" class="control-label">Nome:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getNome() : ""?></p>
                        </div>
                        <div class="col-md-6">
                            <label for="DataAniversario" class="control-label">Data Nascimento:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getDataNascimentoFormatado() : ""?></p>
                        </div>

                        <div class="col-md-6">
                            <label for="Email" class="control-label">Email:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getEmail() : ""?></p>
                        </div>
                        <div class="col-md-6">
                            <label for="Cpf" class="control-label">CPF:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getCpf() : ""?></p>
                        </div>
                        <div class="col-md-3">
                            <label for="Banco" class="control-label">Banco:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getBanco() : ""?></p>
                        </div>

                        <div class="col-md-3">
                            <label for="Agencia" class="control-label">Agência:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getAgencia() : ""?></p>
                        </div>

                        <div class="col-md-3">

                            <label for="TipoConta" class="control-label">Tipo:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getTipoConta() : ""?></p>
                        </div>

                        <div class="col-md-3">

                            <label for="Conta" class="control-label">Conta:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getConta() : ""?></p>
                        </div>

                        <div class="col-md-6">

                            <label for="InseridoPor" class="control-label">Inserido por:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getInseridoPor() : ""?></p>
                        </div>

                        <div class="col-md-6">

                            <label for="AlteradoPor" class="control-label">Alterado por:</label>
                            <p><?php echo ($oColaborador) ? $oColaborador->getAlteradoPor() : ""?></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box-header with-border">
                            <h4 class="box-title">Foto</h4>
                        </div>

                    </div>
                    <div class="box-body no-padding" align="center">
                        <form enctype="multipart/form-data">

                            <input type="hidden" name="fCodColaborador" id="CodColaborador" value="<?php echo ($oColaborador) ? $oColaborador->getCodColaborador() : ''?>">

                            <input type="hidden" name="fLink" id="Link" value='?action=Colaborador.processaFormulario&sOP=AlterarFoto&fCodColaborador=<?php echo ($oColaborador) ? $oColaborador->getCodColaborador() : ""?>'>

                            <label class="label" data-toggle="tooltip" title="" data-original-title="Troque sua foto">
                                <img class="rounded" id="avatar" src="<?php echo getUserPhoto(); ?>" alt="avatar">
                                <input type="file" class="sr-only" id="input" name="image" accept="image/*">
                            </label>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                            </div>

                            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modalLabel">Recortar foto</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="img-container">
                                                <img id="image" src="dist/img/no_avatar.jpg">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="row" id="actions">
                                                <div class="col-md-9 docs-buttons">

                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate Left">
                          <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.rotate(-90)">
                            <span class="fa fa-undo-alt"></span>
                          </span>
                                                        </button>
                                                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate Right">
                          <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.rotate(90)">
                            <span class="fa fa-redo-alt"></span>
                          </span>
                                                        </button>
                                                    </div>

                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            <button type="button" class="btn btn-primary" id="crop">Cortar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>


                    </div>
                    <!-- /.box-footer -->
                    <div >

                        <?php if(is_array($voProjeto)){?>
                            <div class="box-header with-border">
                                <h3 class="box-title"><strong>Projetos Envolvidos</strong></h3>
                            </div>
                            <table id="lista" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Projeto</th>
                                    <th>Serviço</th>
                                    <th>Cliente</th>
                                    <th>Percentual</th>
                                    <th>Recebido</th>
                                    <th>a receber</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($voProjeto as $oProjeto){ ?>
                                    <tr>
                                        <td><?php echo $oProjeto->getDescricao()?></td>
                                        <td><?php echo $oProjeto->getCodServico()?></td>
                                        <td><?php echo $oProjeto->getCodCliente()?></td>
                                        <td><?php echo $oProjeto->getAtivo()?></td>
                                        <td><?php echo $oProjeto->getIncluidoPor()?></td>
                                        <td><?php echo $oProjeto->getAlteradoPor()?></td>
                                        <td><?php echo $oProjeto->getObservacao()?></td>
                                    </tr>
                                <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Projeto</th>
                                    <th>Serviço</th>
                                    <th>Cliente</th>
                                    <th>Percentual</th>
                                    <th>Recebido</th>
                                    <th>a receber</th>
                                    <th>Status</th>
                                </tr>
                                </tfoot>
                            </table>
                        <?php }//if(count($voColaboradorCarreira)){?>

                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Colaborador.preparaLista">Voltar</a></div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <?php include_once('includes/footer.php')?>

    <?php include_once('includes/javascript.php')?>

    <script src="bower_components/cropper/js/cropper.min.js"></script>
    <script src="bower_components/cropper/js/cropper.js"></script>

    <script>
        window.onload = function () {
            loadFoto();
        };

        function loadFoto(){

            jQuery.ajax({
                url:"?action=Colaborador.colaboradorFoto&CodColaborador=<?php echo $oColaborador->getCodColaborador(); ?>",
                cache:false,
                xhrFields:{
                    responseType: 'blob'
                },
                success: function(data){
                    var img = document.getElementById('avatar');
                    var url = window.URL || window.webkitURL;
                    img.src = url.createObjectURL(data);
                }
            });

        }
    </script>

</div>
<!-- ./wrapper -->
</body>
</html>
