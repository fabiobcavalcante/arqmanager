﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oColaborador = $_REQUEST['oColaborador'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Colaborador - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Colaborador.preparaLista">Gerenciar Colaboradores</a>
 			<li class="active">Colaborador - <?php echo $sOP?></li>
 		</ol>

     </section>
<?php include_once('includes/mensagem.php')?>
     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Colaborador - Detalhe</h3>
         </div>
         <div class="box-body">
          <div class="col-md-6">
            <div class="box-header with-border">
              <h4 class="box-title">Colaborador</h4>
            </div>
 <div class="col-md-4">

 <label for="Nome" class="control-label">Nome:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getNome() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="DataAniversario" class="control-label">Data Nascimento:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getDataNascimentoFormatado() : ""?></p>
	</div>

  <div class="col-md-2">

 <label for="Cpf" class="control-label">CPF:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getCpf() : ""?></p>
	</div>
 <div class="col-md-3">

 <label for="Email" class="control-label">Email:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getEmail() : ""?></p>
	</div>
 <div class="col-md-4">

 <label for="Banco" class="control-label">Banco:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getBanco() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="Agencia" class="control-label">Agência:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getAgencia() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="TipoConta" class="control-label">Tipo de Conta:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getTipoConta() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="Conta" class="control-label">Conta:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getConta() : ""?></p>
	</div>

 <div class="col-md-7">

 <label for="InseridoPor" class="control-label">Inserido por:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getInseridoPor() : ""?></p>
	</div>

 <div class="col-md-5">

 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getAlteradoPor() : ""?></p>
	</div>
</div>
<div class="col-md-6">
  <div class="box-header with-border">
    <h4 class="box-title">Foto</h4>
  </div>
<div class="form-group col-sm-12">
    <div class="box-body no-padding" align="center">
        <img class="img-responsive" width="450" src="<?php echo ($oColaborador && $oColaborador->getFoto()) ? "view" . DIRECTORY_SEPARATOR . "principal" . DIRECTORY_SEPARATOR."colaborador" . DIRECTORY_SEPARATOR."foto".DIRECTORY_SEPARATOR.$oColaborador->getFoto() : "dist/img/no_avatar.jpg"?>" alt="User Image">
    </div>
                  <!-- /.box-footer -->

</div>
  <div class="form-group col-md-12">
    <div class="col-sm-offset-5 col-sm-2"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
              <i class="fa fa-picture-o" aria-hidden="true"></i> Alterar Foto </button></div>
  </div>
</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Colaborador.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->


      <div class="modal fade" id="modal-default">
             <div class="modal-dialog">
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                   <h4 class="modal-title">Escolher Foto para Perfil</h4>
                 </div>
                 <div class="modal-body">
                   <form action="?action=Colaborador.processaFormulario" enctype="multipart/form-data" name="frmUpload" method="post">
                     <input type="hidden" name="sOP" value="AlterarFoto">
                     <input type="hidden" name="fCodColaborador" value="<?php echo ($oColaborador) ? $oColaborador->getCodColaborador() : ""?>">
                     <input type="file" name="fFoto">
                 </div>
                 <div class="modal-footer">
                   <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Fechar</button>
                   <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
                 </div>
                 </form>
               </div>
               <!-- /.modal-content -->
             </div>
             <!-- /.modal-dialog -->
           </div>
           <!-- /.modal -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>

 </div>
 <!-- ./wrapper -->
 </body>
 </html>
