<?php
$voColaborador = $_REQUEST['voColaborador'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Lista de Colaboradores </title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li class="active">Gerenciar Colaboradores</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Colaboradores</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <a href="?action=Colaborador.preparaFormulario&sOP=Cadastrar" class="btn btn-success"><i class="fa fa-plus"></i> Cadastrar Colaborador</a>

                            <?php if($voColaborador){?>
                                <table id="lista" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nome</th>
                                        <th>Data Nascimento</th>
                                        <th>CPF</th>
                                        <th>Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($voColaborador as $oColaborador){ ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Ações<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="?action=Colaborador.preparaFormulario&sOP=Alterar&nCodColaborador=<?php echo $oColaborador->getCodColaborador()?>"><i class="fa fa-pencil-square-o"></i> Alterar Colaborador</a></li>
                                                        <li><a href="?action=ColaboradorProducao.preparaLista&sOP=Visualizar&nCodColaborador=<?php echo $oColaborador->getCodColaborador()?>"><i class="fa fa-file-text-o"></i> Produção do Colaborador</a></li>
                                                        <li><a href="?action=Colaborador.preparaFormulario&sOP=Detalhar&nCodColaborador=<?php echo $oColaborador->getCodColaborador()?>"><i class="fa fa-search"></i> Detalhar</a></li>
                                                        <li><a href="?action=Colaborador.processaFormulario&sOP=LimparSenha&nCodColaborador=<?php echo $oColaborador->getCodColaborador()?>"><i class="fa fa-lock"></i> Resetar Senha</a></li>
                                                        <li><a href="?action=ColaboradorEscritorioGrupo.processaFormulario&sOP=Excluir&nCodColaborador=<?php echo $oColaborador->getCodColaborador()?>"><i class="fa fa-trash-o"></i> Excluir Colaborador</a></li>

                                                    </ul>
                                                </div>
                                            </td>
                                            
                                            <td><?php echo $oColaborador->getNome()?></td>
                                            <td><?php echo $oColaborador->getDataNascimentoFormatado()?></td>
                                            <td><?php echo $oColaborador->getCpf()?></td>
                                            <td><?php echo $oColaborador->getEmail()?></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            <?php } ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
</body>
</html>
