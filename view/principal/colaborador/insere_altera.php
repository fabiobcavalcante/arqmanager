<?php
$sOP = $_REQUEST['sOP'];
$oColaborador = $_REQUEST['oColaborador'];
$voAcessoGrupoUsuario = $_REQUEST['voAcessoGrupoUsuario'];
$oGrupoColaborador = $_REQUEST['oGrupoColaborador'] ?? null;
$nTipoColaboradorDisabled = ($oColaborador && $oColaborador->getTipo() == '2') ? " disabled " : "";
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Colaborador - <?php echo $sOP ?></title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Colaborador.preparaLista">Gerenciar Colaboradores</a>
                <li class="active">Colaborador - <?php echo $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>

        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <form method="post" class="form-horizontal" name="formColaborador" action="?action=Colaborador.processaFormulario">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Colaborador / Usuário - <?php echo $sOP ?></h3>
                            </div>
                            <!-- /.box-header -->

                            <input type="hidden" name="sOP" id="sOP" value="<?php echo $sOP; ?>">
                            <input type="hidden" name="fCodColaborador" id="CodColaborador" value="<?=($oColaborador) ? $oColaborador->getCodColaborador() : ""?>">
                            <div class="box-body" id="form-group">
                                <div id="sMsg2" title="Mensagem" class="alert alert-success alert-dismissible" style="display:none">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-ban"></i></p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group col-md-12"><h4>Dados do Colaborador</h4></div>
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label for="Cpf">CPF:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <input class="form-control" type='text' id='Cpf' placeholder='CPF' name='fCpf'  required   value='<?php echo ($oColaborador) ? $oColaborador->getCpf() : ""?>'/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-8 col-sm-12">
                                            <label for="Nome">Nome:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <input class="form-control" type='text' id='Nome' placeholder='Nome' name='fNome'  required   value='<?php echo ($oColaborador) ? $oColaborador->getNome() : ""?>'/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 col-sm-12">
                                            <label for="Email">Email:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <input class="form-control" type='text' id='Email' placeholder='Email' name='fEmail'  required   value='<?php echo ($oColaborador) ? $oColaborador->getEmail() : ""?>'/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label for="DataNascimento">Nascimento:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <input class="form-control date" type='text' id='DataNascimento' placeholder='Data Nascimento' name='fDataNascimento'  required   value='<?php echo ($oColaborador) ? $oColaborador->getDataNascimentoFormatado() : ""?>'/>
                                            </div>
                                        </div>


                                        <div class="form-group col-md-4 col-sm-12">
                                            <label for="TipoColaborador">Tipo:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <select name='fTipo' class="form-control" id="TipoColaborador" required>
                                                    <option value=''>Selecione</option>
                                                    <option  <?php echo ($oColaborador && $oColaborador->getTipo() == '0') ? "selected" : "";?> value='0'>Geral</option>
                                                    <option  <?php echo ($oColaborador && $oColaborador->getTipo() == '1') ? "selected" : "";?> value='1'>Administração</option>
                                                    <option  <?php echo ($oColaborador && $oColaborador->getTipo() == '2') ? "selected" : "";?> value='2'>Terceirizado</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group col-md-12"><h4>Dados Bancários</h4></div>
                                        <div class="form-group col-md-6 col-sm-12">
                                            <label for="TipoConta">Tipo de Conta:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <select name='fTipoConta' id='TipoConta' class="form-control" >
                                                    <option value=''>Selecione</option>
                                                    <option  <?php echo ($oColaborador && $oColaborador->getTipoConta() == 'Corrente') ? "selected" : "";?> value='Corrente'>Corrente</option>
                                                    <option  <?php echo ($oColaborador && $oColaborador->getTipoConta() == 'Poupanca') ? "selected" : "";?> value='Poupanca'>Poupança</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-12">
                                            <label for="Banco">Banco:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <input class="form-control" type='text' id='Banco' placeholder='Banco' name='fBanco' value='<?php echo ($oColaborador) ? $oColaborador->getBanco() : ""?>'/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <label for="Agencia">Agência:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <input class="form-control" type='text' id='Agencia' placeholder='Agência' name='fAgencia' value='<?php echo ($oColaborador) ? $oColaborador->getAgencia() : ""?>'/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label for="Conta">Conta:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <input class="form-control" type='text' id='Conta' placeholder='Conta' name='fConta' value='<?php echo ($oColaborador) ? $oColaborador->getConta() : ""?>'/>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label for="PixTipo">Tipo Chave Pix:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <select name='fPixTipo' id="PixTipo"  class="form-control">
                                                    <option value=''>Selecione</option>
                                                    <option <?php echo ($oColaborador && $oColaborador->getPixTipo() == 'Celular') ? "selected" : "";?> value='Celular'>Celular</option>
                                                    <option <?php echo ($oColaborador && $oColaborador->getPixTipo() == 'Email') ? "selected" : "";?> value='Email'>Email</option>
                                                    <option <?php echo ($oColaborador && $oColaborador->getPixTipo() == 'CPF/CNPJ') ? "selected" : "";?> value='CPF/CNPJ'>CPF/CNPJ</option>
                                                    <option <?php echo ($oColaborador && $oColaborador->getPixTipo() == 'Chave aleatória') ? "selected" : "";?> value='Chave aleatória'>Chave aleatória</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-7 col-sm-12">
                                            <label for="PixChave">Chave Pix:</label>
                                            <div class="input-group col-md-11 col-sm-12">
                                                <input class="form-control" type='text' id='PixChave' placeholder='PixChave' name='fPixChave' value='<?php echo ($oColaborador) ? $oColaborador->getPixChave() : ""?>'/>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group"><h4>Dados do Usuário</h4></div>
                                            <div class="form-group col-md-4 col-md-12">
                                                <label for="CodGrupoUsuario">Grupo Usuário:</label>
                                                <div class="input-group col-md-11 col-sm-12">
                                                    <select name='fCodGrupoUsuario' id="CodGrupoUsuario" class="form-control" required <?php echo $nTipoColaboradorDisabled?>>
                                                        <option value=''>Selecione</option>
                                                        <?php $sSelected = "";
                                                        if($voAcessoGrupoUsuario){
                                                            foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){
                                                                if($oGrupoColaborador){
                                                                    $sSelected = ($oGrupoColaborador->getCodGrupoUsuario() == $oAcessoGrupoUsuario->getCodGrupoUsuario()) ? "selected" : "";
                                                                }
                                                                ?>
                                                                <option  <?php echo $sSelected?> value='<?php echo $oAcessoGrupoUsuario->getCodGrupoUsuario()?>'><?php echo $oAcessoGrupoUsuario->getDescricao()?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4 col-md-12">
                                                <label for="Login">Login:</label>
                                                <div class="input-group col-md-11 col-sm-12">
                                                    <input class="form-control" type='text' id='Login' placeholder='Login' name='fLogin'  required <?php echo $nTipoColaboradorDisabled?>  value='<?php echo ($oColaborador) ? $oColaborador->getLogin() : ""?>'/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="col-sm-offset-5 col-sm-2 col-md-6">
                                            <button type="submit" class="btn btn-lg btn-success" id="btn"><?php echo $sOP?></button>
                                        </div>
                                    </div>
                                </div>
                                <!-- class="row"> -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.row -->
</div>
<!-- /.content-wrapper -->
<?php include_once('includes/footer.php')?>

<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script type="text/javascript">
    jQuery(function($){
        $('#Cpf').mask('000.000.000-00', {reverse: true});
    });
    $("#Cpf").blur(function() {
        var nIdentificacao = $('#Cpf').val();
        validar(nIdentificacao, 'Cpf', 'valida_cpf','CPF inválido');
        recuperaConteudoDinamicoJson(nIdentificacao);
    });
    $("#Email").blur(function() {
        var sEmail = $('#Email').val();
        validar(sEmail, 'Email', 'validacaoEmail','Email inválido');
    });


    $('#TipoColaborador').change(function(){
        if($(this).val() == '2'){
            $("#Login").prop('disabled', true).prop('required', false);
            $("#CodGrupoUsuario").prop('disabled', true).prop('required', false);
        }else{
            $("#Login").prop('disabled', false).prop('required', true);
            $("#CodGrupoUsuario").prop('disabled', false).prop('required', true);

        }
    });


    function recuperaConteudoDinamicoJson(sCpf){
        $.ajax({
            dataType: "JSON",
            type: "GET",
            beforeSend: function(){
                $("#sMsg2").html("Carregando Informação <i class='fa fa-spin fa-refresh'></i>");
            },
            url: '?action=Colaborador.recuperarUmColaboradorCpf&sCPF='+sCpf,
            error: function(oXMLRequest,sErrorType){
                console.log(oXMLRequest.responseText);
                console.log(oXMLRequest.status+' , '+sErrorType);
            },
            success: function(data){
                $("#CodColaborador").val(data.CodColaborador);
                $("#Nome").val(data.Nome);
                $("#Email").val(data.Email);
                $("#DataNascimento").val(data.DataNascimento);
                $("#TipoColaborador").val(data.TipoColaborador);
                $("#Login").val(data.Login);
                $("#TipoConta").val(data.TipoConta);
                $("#Banco").val(data.Banco);
                $("#Agencia").val(data.Agencia);
                $("#Conta").val(data.Conta);
                $("#PixTipo").val(data.PixTipo);
                $("#PixChave").val(data.PixChave);
                $("#sOP").val(data.sOP);
                $("#btn").text(data.sOP);

                $("#sMsg2").show().text(data.sMsg2);
            }
        });
    }
</script>
</body>
</html>
