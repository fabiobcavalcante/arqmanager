<?php
/**
 *
 * HTML5 Image uploader with Jcrop
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2012, Script Tutorials
 * http://www.script-tutorials.com/
 */

function uploadImageFile() { // Note: GD library is required for this function

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $iWidth = $iHeight = 200; // desired image result dimensions
        $iJpgQuality = 90;

        if ($_FILES) {

            // if no errors and size less than 250kb
            if (! $_FILES['fFoto']['error'] && $_FILES['fFoto']['size'] < 2500 * 1024) {
                if (is_uploaded_file($_FILES['fFoto']['tmp_name'])) {

                    // new unique filename

                      $dirUploads = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."luanapalheta".DIRECTORY_SEPARATOR."view".DIRECTORY_SEPARATOR."principal".DIRECTORY_SEPARATOR."colaborador" . DIRECTORY_SEPARATOR . "foto" . DIRECTORY_SEPARATOR;
                      $sTempFileName = $dirUploads . $_REQUEST['fCodColaborador'];

                    // move uploaded file into cache folder
                    move_uploaded_file($_FILES['fFoto']['tmp_name'], $sTempFileName);

                    // change file permission to 644
                    @chmod($sTempFileName, 0644);

                    if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                        $aSize = getimagesize($sTempFileName); // try to obtain image info
                        if (!$aSize) {
                            @unlink($sTempFileName);
                            return;
                        }

                        // check for image type
                        switch($aSize['mime']) {
                            case 'image/jpg':
                            case 'image/jpeg':
                                $sExt = '.jpg';

                                // create a new image from file
                                $vImg = @imagecreatefromjpeg($sTempFileName);
                                break;
                            /*case IMAGETYPE_GIF:
                                $sExt = '.gif';

                                // create a new image from file
                                $vImg = @imagecreatefromgif($sTempFileName);
                                break;*/
                            case 'image/png':
                                $sExt = '.png';

                                // create a new image from file
                                $vImg = @imagecreatefrompng($sTempFileName);
                                break;
                            default:
                                @unlink($sTempFileName);
                                return;
                        }

                        // create a new true color image
                        $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );

                        // copy and resize part of an image with resampling
                        imagecopyresampled($vDstImg, $vImg, 0,0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

// var_dump($vDstImg);
// var_dump($vImg);
// var_dump((int)$_POST['x2']);
// var_dump((int)$_POST['y2']);
// var_dump((int)$_POST['x1']);
// var_dump((int)$_POST['y1']);
// var_dump($iWidth);
// var_dump($iHeight);
// var_dump($_POST['w']);
// var_dump((int)$_POST['h']);

                        // define a result image filename
                        $sResultFileName = $sTempFileName . $sExt;

                        // output image to file
                        imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
                        @unlink($sTempFileName);

                        return $sResultFileName;
                    }
                }
            }
        }
    }
}

$sImage = uploadImageFile();
echo '<img src="'.$sImage.'" />';
