﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oColaborador = $_REQUEST['oColaborador'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Colaborador - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>

     <style type="text/css">
     .step2, .error {
         display: none;
     }
     .error {
         font-size: 14px;
         font-weight: bold;
         color: red;
     }
     .info {
         font-size: 14px;
     }
     label {
         margin: 0 5px;
     }

     .jcrop-holder {
         display: inline-block;
     }



     /* jquery.Jcrop.min.css v0.9.10 (build:20120429) */
     .jcrop-holder{direction:ltr;text-align:left;}
     .jcrop-vline,.jcrop-hline{background:#FFF url(Jcrop.gif) top left repeat;font-size:0;position:absolute;}
     .jcrop-vline{height:100%;width:1px!important;}
     .jcrop-hline{height:1px!important;width:100%;}
     .jcrop-vline.right{right:0;}
     .jcrop-hline.bottom{bottom:0;}
     .jcrop-handle{background-color:#333;border:1px #eee solid;font-size:1px;}
     .jcrop-tracker{-webkit-tap-highlight-color:transparent;-webkit-touch-callout:none;-webkit-user-select:none;height:100%;width:100%;}
     .jcrop-handle.ord-n{left:50%;margin-left:-4px;margin-top:-4px;top:0;}
     .jcrop-handle.ord-s{bottom:0;left:50%;margin-bottom:-4px;margin-left:-4px;}
     .jcrop-handle.ord-e{margin-right:-4px;margin-top:-4px;right:0;top:50%;}
     .jcrop-handle.ord-w{left:0;margin-left:-4px;margin-top:-4px;top:50%;}
     .jcrop-handle.ord-nw{left:0;margin-left:-4px;margin-top:-4px;top:0;}
     .jcrop-handle.ord-ne{margin-right:-4px;margin-top:-4px;right:0;top:0;}
     .jcrop-handle.ord-se{bottom:0;margin-bottom:-4px;margin-right:-4px;right:0;}
     .jcrop-handle.ord-sw{bottom:0;left:0;margin-bottom:-4px;margin-left:-4px;}
     .jcrop-dragbar.ord-n,.jcrop-dragbar.ord-s{height:7px;width:100%;}
     .jcrop-dragbar.ord-e,.jcrop-dragbar.ord-w{height:100%;width:7px;}
     .jcrop-dragbar.ord-n{margin-top:-4px;}
     .jcrop-dragbar.ord-s{bottom:0;margin-bottom:-4px;}
     .jcrop-dragbar.ord-e{margin-right:-4px;right:0;}
     .jcrop-dragbar.ord-w{margin-left:-4px;}
     .jcrop-light .jcrop-vline,.jcrop-light .jcrop-hline{background:#FFF;filter:Alpha(opacity=70)!important;opacity:.70!important;}
     .jcrop-light .jcrop-handle{-moz-border-radius:3px;-webkit-border-radius:3px;background-color:#000;border-color:#FFF;border-radius:3px;}
     .jcrop-dark .jcrop-vline,.jcrop-dark .jcrop-hline{background:#000;filter:Alpha(opacity=70)!important;opacity:.7!important;}
     .jcrop-dark .jcrop-handle{-moz-border-radius:3px;-webkit-border-radius:3px;background-color:#FFF;border-color:#000;border-radius:3px;}
     .jcrop-holder img,img.jcrop-preview{max-width:none;}
     </style>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?>/h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Colaborador.preparaLista">Gerenciar Colaboradores</a>
 			<li class="active">Colaborador - <?php echo $sOP?></li>
 		</ol>

     </section>
<?php include_once('includes/mensagem.php')?>
     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Colaborador - Detalhe</h3>
         </div>
         <div class="box-body">
          <div class="col-md-6">
            <div class="box-header with-border">
              <h4 class="box-title">Colaborador</h4>
            </div>
 <div class="col-md-4">

 <label for="Nome" class="control-label">Nome:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getNome() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="DataAniversario" class="control-label">Data Nascimento:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getDataNascimentoFormatado() : ""?></p>
	</div>

  <div class="col-md-2">

 <label for="Cpf" class="control-label">CPF:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getCpf() : ""?></p>
	</div>
 <div class="col-md-3">

 <label for="Email" class="control-label">Email:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getEmail() : ""?></p>
	</div>
 <div class="col-md-4">

 <label for="Banco" class="control-label">Banco:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getBanco() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="Agencia" class="control-label">Agência:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getAgencia() : ""?></p>
	</div>

 <div class="col-md-2">

 <label for="TipoConta" class="control-label">Tipo de Conta:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getTipoConta() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="Conta" class="control-label">Conta:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getConta() : ""?></p>
	</div>

 <div class="col-md-7">

 <label for="InseridoPor" class="control-label">Inserido por:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getInseridoPor() : ""?></p>
	</div>

 <div class="col-md-5">

 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oColaborador) ? $oColaborador->getAlteradoPor() : ""?></p>
	</div>
</div>
<div class="col-md-6">
  <div class="box-header with-border">
    <h4 class="box-title">Foto</h4>
  </div>

  <div class="form-group col-md-12">
<img class="img-responsive" id="preview" src="<?php echo ($oColaborador && $oColaborador->getFoto()) ? "view" . DIRECTORY_SEPARATOR . "principal" . DIRECTORY_SEPARATOR."colaborador" . DIRECTORY_SEPARATOR."foto".DIRECTORY_SEPARATOR.$oColaborador->getFoto() : "dist/img/no_avatar.jpg"?>" alt="User Image">
    <form id="upload_form" enctype="multipart/form-data" method="post" action="?action=Colaborador.processaFormulario" onsubmit="return checkForm()">
        <input type="hidden" name="sOP" value="AlterarFoto">
        <input type="hidden" name="fCodColaborador" value="<?php echo ($oColaborador) ? $oColaborador->getCodColaborador() : ""?>">
                          <!-- hidden crop params -->
                          <input type="hidden" id="x1" name="x1" />
                          <input type="hidden" id="y1" name="y1" />
                          <input type="hidden" id="x2" name="x2" />
                          <input type="hidden" id="y2" name="y2" />
                          <div><input type="file" name="fFoto" id="image_file" onchange="fileSelectHandler()" /></div>
                          <div class="error"></div>
                          <div class="step2">

                              <div class="info">
                                <input type="hidden" id="filedim" name="filedim" />
                                 <input type="hidden" id="w" name="w" />
                                <input type="hidden" id="h" name="h" />
                              </div>
                          </div>

                        <button type="submit" class="btn btn-success"><i class="fa fa-picture-o" aria-hidden="true"></i> Salvar Miniatura</button>

                      </form>
                    </div>
                    </div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Colaborador.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>


 <script src="bower_components/jcrop/jquery.color.js" type="text/javascript"></script>
    <script src="bower_components/jcrop/jquery.Jcrop.js" type="text/javascript"></script>
    <script src="bower_components/jcrop/script.js" type="text/javascript"></script>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/vader/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="bower_components/jcrop/jcrop.css" type="text/css" />

 </div>
 <!-- ./wrapper -->
 </body>
 </html>
