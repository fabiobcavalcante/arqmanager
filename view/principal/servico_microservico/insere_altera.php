<?php
$sOP = $_REQUEST['sOP'];
$oServicoMicroservico = $_REQUEST['oServicoMicroServico'] ?? null;
$oServicoEtapa = $_REQUEST['oServicoEtapa'] ?? null;
?>
<div class="box">
    <div class="modal-header">
        <h3 class="modal-title">Serviço x Microserviço - <?php echo $sOP ?></h3>
    </div>
    <form method="post" id="formServicoMicroservico" action="?action=ServicoMicroServico.processaFormulario" onsubmit="event.preventDefault();processaFormulario('formServicoMicroservico')">

        <div class="modal-body">
            <input type="hidden" name="sOP" value="<?php echo $sOP?>">
            <input type="hidden" name="fCodMicroservico" value="<?php echo ($oServicoMicroservico) ? $oServicoMicroservico->getCodMicroservico() : ""?>">
            <?php if ($oServicoEtapa){ ?>
                <input type="hidden" name='fCodServicoEtapa' value=<?php echo $oServicoEtapa->getCodServicoEtapa(); ?>>
                <input type="hidden" name="fCodServico" value="<?php echo $oServicoEtapa->getCodServico(); ?>">
                <input type="hidden" name="fCodEtapa" value="<?php echo $oServicoEtapa->getCodEtapa(); ?>">

                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="Servico">Etapa:</label>
                        <div class="input-group col-md-11">
                            <h4><?php echo $oServicoEtapa->getDescricao()?></h4>
                        </div>
                    </div>
                </div>
            <?php }else{ ?>
                <input type="hidden" name="fCodServico" value="<?php echo $_GET['nCodServico']; ?>">

                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="CodServicoEtapa">Etapa:</label>
                        <select class="form-control" name="fCodServicoEtapa" id="CodServicoEtapa" required>
                            <option value="">Selecione</option>
                            <?php foreach ($_REQUEST['voServicoEtapa'] as $oServicoEtapa){ ?>
                            <option value="<?php echo $oServicoEtapa->getCodServicoEtapa();?>"><?php echo $oServicoEtapa->getDescricao();?></option>
                            <?php } ?>
                        </select>
                        <div class="input-group col-md-11">
                            <h4><?php echo $oServicoEtapa->getDescricao()?></h4>
                        </div>
                    </div>
                </div>
            <?php } ?>





            <div class="form-group row">
                <div class="col-md-6">
                    <label for="Descricao">Descrição:</label>
                    <div class="input-group col-md-11">
                        <input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao' value='<?php echo ($oServicoMicroservico) ? $oServicoMicroservico->getDescricao() : ""?>' required>
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <label for="Cor">Cor:</label>
                    <div class="input-group col-md-11">
                        <input type="text" class="form-control my-colorpicker1 colorpicker-element" name="fCor" value="<?php echo ($oServicoMicroservico) ? $oServicoMicroservico->getCor() : ""?>" autocomplete="off" required>
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <label for="Ordem">Ordem:</label>
                    <div class="input-group col-md-11">
                        <input class="form-control" type='number' min="0" max="99" id='Ordem' placeholder='Ordem' name='fOrdem' required   value='<?php echo ($oServicoMicroservico) ? $oServicoMicroservico->getOrdem() : ""?>'/>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    <label for="Prazo">Prazo:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <select name='fPrazo' id="Prazo" class="form-control" required >
                            <option>Selecione</option>
                            <option <?php echo ($oServicoMicroservico && $oServicoMicroservico->getPrazo()=='1') ? " selected ": " "?> value='1'>Sim</option>
                            <option <?php echo ($oServicoMicroservico && $oServicoMicroservico->getPrazo()=='0') ? " selected ": " "?> value='0'>Não</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <label for="Detalhe">Detalhe:</label>
                    <div class="input-group col-md-11 col-sm-12">
                        <select name='fDetalhe' id="Detalhe" class="form-control" required >
                            <option value="">Selecione</option>
                            <option <?php echo ($oServicoMicroservico && $oServicoMicroservico->getDetalhe()=='1') ? " selected ": " "?> value="1">Sim</option>
                            <option <?php echo ($oServicoMicroservico && $oServicoMicroservico->getDetalhe()=='0') ? " selected ": " "?> value="0">Não</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <div class="col-sm-offset-5 col-sm-2">
                <button type="submit" class="btn btn-lg btn-success"><?php echo $sOP?></button>
            </div>
        </div>

    </form>
</div>
