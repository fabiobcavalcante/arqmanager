<?php
$voServicoMicroServico = $_REQUEST['voServicoMicroServico'];
$voSelecao = $_REQUEST['vSelecao'];
$bPagamento = $_GET['bPagamento'];

$sDisabled = ($bPagamento == 1) ? "disabled" : "";

if(is_array($voServicoMicroServico)){
    $aDescricao = array("Imagem 3D","Detalhamento");
    ?>
		<div class="row">
		<div class="col-md-12">
		<div class="table-responsive">
		<table id="lista" class="table table-bordered table-striped">
        <thead>
        <tr>
            <?php if (!$bPagamento){ ?>
                <th width="1%"style="white-space: nowrap"><a href="javascript: marcarTodosCheckBoxFormulario('Proposta')"><i class="icon fa fa-check"></a></th>
            <?php } ?>
            <th width="50%">Descrição</th>
            <th  width="39%"></th>
            <th width="10%"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($voServicoMicroServico as $oServicoMicroServico){
            $sType = (in_array($oServicoMicroServico->getDescricao(),$aDescricao)) ? "text" : "hidden";
            $sChecado = "";
            $nQuantidade = "";
            $sDescricao = "";
            $nCodMicroservico = (int)$oServicoMicroServico->getCodMicroservico();

            if($_REQUEST['sOP2']=='Alterar'){
                if($voSelecao && (array_key_exists($oServicoMicroServico->getCodMicroservico(),$voSelecao))){
                    $sChecado = "checked";
                    $nQuantidade = $voSelecao[$nCodMicroservico][1];
                    $sDescricao = $voSelecao[$nCodMicroservico][2];
                }else{
                    $sChecado = "";
                    $nQuantidade = "";
                    $sDescricao = "";
                }

            }else{
                $sChecado = "checked";
            } ?>
            <tr>
                <?php if (!$bPagamento){ ?>
                    <td style="white-space:nowrap"><input <?php echo $sChecado;?> type="checkbox" value="<?php echo $nCodMicroservico; ?>" class="foo1" name="fIdServicoMicroServico[]"/ ></td>
                <?php } ?>
                <td style="white-space:nowrap"><?php echo $oServicoMicroServico->getDescricao(); ?></td>
                <td><input type="<?php echo $sType; ?>" class="form-control" name="fDescricaoQuantidade[]" id="" value="<?php echo $sDescricao?>" placeholder="Descrição" <?php echo $sDisabled; ?>></td>
                <td><input type="<?php echo $sType; ?>" class="form-control" name="fQuantidade[]" id="" value="<?php echo $nQuantidade?>" placeholder="Quantidade" onkeypress="TodosNumero(event)" <?php echo $sDisabled; ?>></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
	</div>
</div>
</div>
	<!-- .table-responsive -->
<?php } ?>
