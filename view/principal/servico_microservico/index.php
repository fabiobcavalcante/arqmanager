<?php
$voMicroservico = $_REQUEST['voMicroServico'];
$oServico = $_REQUEST['oServico'] ;
?>
<div class="box">
    <div class="modal-header">
        <h3 class="modal-title">Microserviços</h3>
    </div>
    <div class="modal-body">

        <button type="button" class="btn btn-success" onclick="abreModal('MicroServico','Cadastrar','<?php echo $oServico->getCodServico()?>','<?php echo $_GET['nCodServicoEtapa']; ?>')"><i class="fa fa-plus"></i> Cadastrar Microserviço</button>

        <?php if($voMicroservico){ ?>
            <table id="lista" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th style="width: 1%"></th>
                    <th>Id</th>
                    <th>Descrição</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($voMicroservico as $oMicroservico){
                    $nCodServico = $oMicroservico->cod_servico;
                    $nCodServicoEtapa = $oMicroservico->cod_servico_etapa;
                    $nCodMicroservico = $oMicroservico->cod_microservico;
                    ?>
                    <tr>
                        <td style="white-space: nowrap">
                            <button type="button" class="btn btn-success btn-xs" onclick="abreModal('MicroServico','Alterar','<?=$nCodServico;?>','<?=$nCodServicoEtapa;?>','<?=$nCodMicroservico;?>')"><i class="fa fa-edit"></i> </button>

                            <button type="button" class="btn btn-danger btn-xs disabled"><i class="fa fa-trash"></i> </button>
                        </td>
                        <td><?= $nCodMicroservico; ?></td>
                        <td><?= $oMicroservico->desc_microservico; ?></td>
                    </tr>
                <?php }?>
                </tbody>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Id</th>
                    <th>Descrição</th>
                </tr>
                </tfoot>
            </table>
        <?php } ?>
    </div>
</div>