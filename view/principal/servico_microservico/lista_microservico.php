<?php
$voServicoMicroServico = $_REQUEST['voServicoMicroServico'];
$voSelecao = $_REQUEST['vSelecao'];

$bPagamento = $_GET['bPagamento'];
$sDisabled = ($bPagamento == 1) ? "disabled" : "";

if(is_array($voServicoMicroServico)){
    // $aDescricao = array("Imagem 3D","Apresentação de imagens","Apresentação de imagens em 3D");
    $aDias = array("RRT - CAU","Arrumação dos ambientes com sugestão de objetos com dia programado");
    ?>
    <table id="tabela2" class="table table-bordered table-striped">
        <thead>
        <tr>
            <?php if (!$bPagamento){ ?>
                <th style="white-space: nowrap"><a href="javascript: marcarTodosCheckBoxFormulario('Projeto')"><i class="icon fa fa-check"></a></th>
            <?php } ?>
            <?php if($_REQUEST['sOP2']=='Alterar'){ ?>
                <th class="col-sm-1">Dias <?php ($_SESSION['oEscritorio']->getDiasUteis() == 'S') ? " úteis":"" ?></th>
            <?php } ?>
            <th>Descrição</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach($voServicoMicroServico as $oServicoMicroServico){

            $sType = ($oServicoMicroServico->getDetalhe() ==1) ? "text" : "hidden";
            $sDias = (in_array($oServicoMicroServico->getDescricao(),$aDias)) ? "readonly" : "";

            $sChecado = "";
            $nQuantidade = "";
            $sDescricao = "";
            $nDias = "";
            $nCodMicroservico = (int)$oServicoMicroServico->getCodMicroservico();

            if($_REQUEST['sOP2']=='Alterar'){

                if(array_key_exists($oServicoMicroServico->getCodMicroservico(),$voSelecao)){
                    $sChecado = "checked";
                    $nQuantidade = $voSelecao[$nCodMicroservico][1];
                    $sDescricao = $voSelecao[$nCodMicroservico][2];
                    $nDias = $voSelecao[$nCodMicroservico][3];
                    $dDataPrevisaoInicio = $voSelecao[$nCodMicroservico][4];
                    $dDataEfetivacaoInicio = $voSelecao[$nCodMicroservico][5];
                    $dDataPrevisaoFim = $voSelecao[$nCodMicroservico][6];
                    $dDataEfetivacaoFim = $voSelecao[$nCodMicroservico][7];
                }else{
                    $sChecado = "";
                    $nQuantidade = "";
                    $sDescricao = "";
                    $nDias = "";
                    $dDataPrevisaoInicio = "";
                    $dDataEfetivacaoInicio = "";
                    $dDataPrevisaoFim = "";
                    $dDataEfetivacaoFim = "";
                }

            } ?>
            <tr>
                <?php if (!$bPagamento){ ?>
                    <td style="white-space:nowrap"><input <?php echo $sChecado;?> type="checkbox" value="<?php echo $nCodMicroservico; ?>" class="foo1" name="fIdServicoMicroServico[]"></td>
                <?php } ?>
                <?php if($_REQUEST['sOP2']=='Alterar'){ ?>
                    <td><?php echo $nDias?></td>
                <?php } ?>
                <td style="white-space:nowrap"><?php echo $oServicoMicroServico->getDescricao(); ?></td>
                <td><input type="<?php echo $sType; ?>" class="form-control" name="fQuantidade<?php echo $nCodMicroservico; ?>" id="" value="<?php echo $nQuantidade?>" placeholder="Quantidade" onkeypress="TodosNumero(event)" <?php echo $sDisabled; ?>></td>
                <td><input type="<?php echo $sType; ?>" class="form-control" name="fDescricaoQuantidade<?php echo $nCodMicroservico; ?>" id="" value="<?php echo $sDescricao?>" placeholder="Descrição" <?php echo $sDisabled; ?>></td>
            </tr>
        <?php } ?>

        </tbody>
    </table>
<?php } ?>
<button type="button" class="btn btn-success" onclick="abreModalEntrega()"><i class="fa fa-plus"></i> Adicionar Item/serviço </button></td>
<script>
    $(document).ready(function() {
        $('#tabela2').DataTable( {
             'responsive': true,
            'paging'      : false,
            "info":     false,
            'order': false,
            "language": {
                "search":"Pesquisar:",
            }
        });

    } );
</script>