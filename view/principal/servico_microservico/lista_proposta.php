<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Micro Serviço</h3>
    </div>
    <div class="box-body">
        <?php
        $voServicoMicroServico = $_REQUEST['voServicoMicroServico'];
        if(is_array($voServicoMicroServico)){ ?>
            <table id="lista2" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('Proposta')"><i class="icon fa fa-check"></a></th>
                    <th>Descrição</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($voServicoMicroServico as $oServicoMicroServico){
                    $sType = ($oServicoMicroServico->getDescricao() == "Imagem 3D") ? "text" : "hidden";
                    $nCodMicroservico = $oServicoMicroServico->getCodMicroservico();
                    ?>
                    <tr>
                        <td style="white-space:nowrap"><input checked='checked' type="checkbox" value="<?php echo $nCodMicroservico; ?>" class="foo1" name="fIdServicoMicroServico[]"/ ></td>
                        <td style="white-space:nowrap"><?php echo $oServicoMicroServico->getDescricao()?></td>
                        <td><input type="<?php echo $sType; ?>" class="form-control" name="fDescricaoQuantidade[]" id="" value="" placeholder="Descrição"></td>
                        <td><input type="<?php echo $sType; ?>" class="form-control" name="fQuantidade[]" id="" value="" placeholder="Quantidade" onkeypress="TodosNumero(event)"></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>
