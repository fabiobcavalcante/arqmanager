<?php
 $voProjetoServicoMicroservico = $_REQUEST['voProjetoServicoMicroservico'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Projeto X Serviço X Microserviço </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Projeto X Serviço X Microserviços</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Projeto X Serviço X Microserviço</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formProjetoServicoMicroservico" id="formProjetoServicoMicroservico" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesProjetoServicoMicroservico" onChange="JavaScript: submeteForm('ProjetoServicoMicroservico')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=ProjetoServicoMicroservico.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Projeto X Serviço X Microserviço</option>
   						<option value="?action=ProjetoServicoMicroservico.preparaFormulario&sOP=Alterar" lang="1">Alterar Projeto X Serviço X Microserviço selecionado</option>
   						<option value="?action=ProjetoServicoMicroservico.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Projeto X Serviço X Microserviço selecionado</option>
   						<option value="?action=ProjetoServicoMicroservico.processaFormulario&sOP=Excluir" lang="2">Excluir Projeto X Serviço X Microserviço(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voProjetoServicoMicroservico)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('ProjetoServicoMicroservico')"><i class="icon fa fa-check"></a></th>
   					<th>Id</th>
					<th>Proposta</th>
					<th>Projeto</th>
					<th>Descrição</th>
					<th>Quantidade</th>
					<th>Qtde descricao</th>
					<th>Inserido por</th>
					<th>Alterado por</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voProjetoServicoMicroservico as $oProjetoServicoMicroservico){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('ProjetoServicoMicroservico')" type="checkbox" value="<?=$oProjetoServicoMicroservico->getCodEtapa()?>||<?=$oProjetoServicoMicroservico->getCodProposta()?>" name="fIdProjetoServicoMicroservico[]"/></td>
  					<td><?php echo $oProjetoServicoMicroservico->getCodEtapa()?></td>
					<td><?php echo $oProjetoServicoMicroservico->getCodProposta()?></td>
					<td><?php echo $oProjetoServicoMicroservico->getCodProjeto()?></td>
					<td><?php echo $oProjetoServicoMicroservico->getDescricao()?></td>
					<td><?php echo $oProjetoServicoMicroservico->getQuantidade()?></td>
					<td><?php echo $oProjetoServicoMicroservico->getDescricaoQuantidade()?></td>
					<td><?php echo $oProjetoServicoMicroservico->getInseridoPor()?></td>
					<td><?php echo $oProjetoServicoMicroservico->getAlteradoPor()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Id</th>
					<th>Proposta</th>
					<th>Projeto</th>
					<th>Descrição</th>
					<th>Quantidade</th>
					<th>Qtde descricao</th>
					<th>Inserido por</th>
					<th>Alterado por</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voProjetoServicoMicroservico)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
