<?php
 $sOP = $_REQUEST['sOP'];
 $oProjetoServicoMicroservico = $_REQUEST['oProjetoServicoMicroservico'];
 $voProjeto = $_REQUEST['voProjeto'];

$voServicoEtapa = $_REQUEST['voServicoEtapa'];

$voProposta = $_REQUEST['voProposta'];


 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Projeto X Serviço X Microserviço - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=ProjetoServicoMicroservico.preparaLista">Gerenciar Projeto X Serviço X Microserviços</a>
 			<li class="active">Projeto X Serviço X Microserviço - <?php echo $sOP?></li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Projeto X Serviço X Microserviço - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formProjetoServicoMicroservico" action="?action=ProjetoServicoMicroservico.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodEtapa" value="<?=(is_object($oProjetoServicoMicroservico)) ? $oProjetoServicoMicroservico->getCodEtapa() : ""?>" />
			<input type="hidden" name="fCodProposta" value="<?=(is_object($oProjetoServicoMicroservico)) ? $oProjetoServicoMicroservico->getCodProposta() : ""?>" />
 				<div  class="box-body" id="form-group">

 <div class="form-group col-md-4">
					<label for="ServicoEtapa">Id:</label>
					<div class="input-group col-md-11">
					<select name='fCodEtapa'  class="form-control select2"  required  >
						<option value=''>Selecione</option>
						<?php $sSelected = "";
						   if($voServicoEtapa){
							   foreach($voServicoEtapa as $oServicoEtapa){
								   if($oProjetoServicoMicroservico){
									   $sSelected = ($oProjetoServicoMicroservico->getCodEtapa() == $oServicoEtapa->getCodEtapa()) ? "selected" : "";
								   }
						?>
								   <option  <?php echo $sSelected?> value='<?php echo $oServicoEtapa->getCodEtapa()?>'><?php echo $oServicoEtapa->getCodEtapa()?></option>
						<?php
							   }
						   }
						?>
					</select>
			</div>
				</div>

 <div class="form-group col-md-4">
					<label for="Proposta">Proposta:</label>
					<div class="input-group col-md-11">
					<select name='fCodProposta'  class="form-control select2"  required  >
						<option value=''>Selecione</option>
						<?php $sSelected = "";
						   if($voProposta){
							   foreach($voProposta as $oProposta){
								   if($oProjetoServicoMicroservico){
									   $sSelected = ($oProjetoServicoMicroservico->getCodProposta() == $oProposta->getCodProposta()) ? "selected" : "";
								   }
						?>
								   <option  <?php echo $sSelected?> value='<?php echo $oProposta->getCodProposta()?>'><?php echo $oProposta->getCodProposta()?></option>
						<?php
							   }
						   }
						?>
					</select>
			</div>
				</div>

 <div class="form-group col-md-4">
					<label for="Projeto">Projeto:</label>
					<div class="input-group col-md-11">
					<select name='fCodProjeto'  class="form-control select2"  required  >
						<option value=''>Selecione</option>
						<?php $sSelected = "";
						   if($voProjeto){
							   foreach($voProjeto as $oProjeto){
								   if($oProjetoServicoMicroservico){
									   $sSelected = ($oProjetoServicoMicroservico->getCodProjeto() == $oProjeto->getCodProjeto()) ? "selected" : "";
								   }
						?>
								   <option  <?php echo $sSelected?> value='<?php echo $oProjeto->getCodProjeto()?>'><?php echo $oProjeto->getCodProjeto()?></option>
						<?php
							   }
						   }
						?>
					</select>
			</div>
				</div>

 <div class="form-group col-md-4">
					<label for="Descricao">Descrição:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'  required   value='<?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getDescricao() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="Quantidade">quantidade:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Quantidade' placeholder='quantidade' name='fQuantidade'  required  onKeyPress="TodosNumero(event);" value='<?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getQuantidade() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="DescricaoQuantidade">qtde descricao:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='DescricaoQuantidade' placeholder='qtde descricao' name='fDescricaoQuantidade'  required   value='<?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getDescricaoQuantidade() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="InseridoPor">Inserido por:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='InseridoPor' placeholder='Inserido por' name='fInseridoPor'  required   value='<?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getInseridoPor() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="AlteradoPor">Alterado por:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='AlteradoPor' placeholder='Alterado por' name='fAlteradoPor'  required   value='<?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getAlteradoPor() : ""?>'/>
				</div>
			</div>


 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 </script>
 </body>
 </html>
