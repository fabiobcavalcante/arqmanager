﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oProjetoServicoMicroservico = $_REQUEST['oProjetoServicoMicroservico'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Projeto X Serviço X Microserviço - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=ProjetoServicoMicroservico.preparaLista">Gerenciar Projeto X Serviço X Microserviços</a>
 			<li class="active">Projeto X Serviço X Microserviço - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Projeto X Serviço X Microserviço - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="CodEtapa" class="control-label">Id:</label>
		<p><?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getCodEtapa() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="CodProposta" class="control-label">Proposta:</label>
		<p><?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getCodProposta() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="CodProjeto" class="control-label">Projeto:</label>
		<p><?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getCodProjeto() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Descricao" class="control-label">Descrição:</label>
		<p><?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getDescricao() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Quantidade" class="control-label">quantidade:</label>
		<p><?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getQuantidade() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="DescricaoQuantidade" class="control-label">qtde descricao:</label>
		<p><?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getDescricaoQuantidade() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="InseridoPor" class="control-label">Inserido por:</label>
		<p><?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getInseridoPor() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oProjetoServicoMicroservico) ? $oProjetoServicoMicroservico->getAlteradoPor() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=ProjetoServicoMicroservico.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
