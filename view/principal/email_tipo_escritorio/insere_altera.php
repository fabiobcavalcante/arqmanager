<?php
 $sOP = $_REQUEST['sOP'];
 $oServico = $_REQUEST['oEmailTipo'];

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Tipo de Email - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=EmailTipo.preparaLista">Gerenciar Tipo de Emails</a>
 			<li class="active">Tipo de Email - <?php echo $sOP?></li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Tipo de Email - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formEmailTipo" action="?action=EmailTipo.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodEmailTipo" value="<?=(is_object($oEmailTipo)) ? $oEmailTipo->getCodEmailTipo() : ""?>" />
 				<div  class="box-body" id="form-group">
 <div class="form-group col-md-4 col-sm-5">
					<label for="Título">Tipo:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Titulo' placeholder='Título' name='fTitulo'  required   value='<?php echo ($oEmailTipo) ? $oEmailTipo->getTitulo() : ""?>'/>
				</div>
			</div>
 <div class="form-group col-md-4 col-sm-5">
					<label for="DescDocumento">Template:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='DescDocumento' placeholder='DescDocumento' name='fDescDocumento'  required   value='<?php echo ($oServico) ? $oServico->getDescDocumento() : ""?>'/>
				</div>
			</div>

 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(function($){
				 });
 </script>
 </body>
 </html>
