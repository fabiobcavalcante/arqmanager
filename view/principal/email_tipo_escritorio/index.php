<?php
 $voEmailTipo = $_REQUEST['voEmailTipoEscritorio'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Tipos de Email </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Tipos de Email</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Tipo de Email</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formEmailTipoEscritorio" id="formEmailTipoEscritorio" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesEmailTipoEscritorio" onChange="JavaScript: submeteForm('EmailTipoEscritorio')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=EmailTipoEscritorio.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Tipo de Email</option>
   						<option value="?action=EmailTipoEscritorio.preparaFormulario&sOP=Alterar" lang="1">Alterar Tipo de Email selecionado</option>
   						<option value="?action=EmailTipoEscritorio.preparaFormulario&sOP=Detalhar" lang="1">Gerenciar Tipo de Email selecionado</option>
   						<option value="?action=EmailTipoEscritorioEtapa.preparaLista" lang="1">Gerenciar Entregas do Tipo de Email selecionado</option>
   						<option value="?action=EmailTipoEscritorio.processaFormulario&sOP=Excluir" lang="2">Excluir Tipo de Email(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voEmailTipoEscritorio)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('EmailTipoEscritorio')"><i class="icon fa fa-check"></a></th>
   					<th width="9%">Id</th>
					  <th width="80%">Escritório</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voEmailTipoEscritorio as $oEmailTipoEscritorio){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('EmailTipoEscritorio')" type="checkbox" value="<?php echo $oEmailTipoEscritorio->getCodEmailTipo()?>||<?php echo $oEmailTipoEscritorio->getIdEscritorio()?>" name="fIdEmailTipoEscritorio[]"/></td>
  					<td><?php echo $oEmailTipo->getCodEmailTipo()?></td>
					<td><?php echo $oEmailTipo->getIdEscritorio()?></td>


  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Id</th>
					<th>Escritório</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voEmailTipo)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
