<?php
 $voStatus = $_REQUEST['voStatus'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Status </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> </h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li class="active">Gerenciar Status</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Status</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formStatus" id="formStatus" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesStatus" onChange="JavaScript: submeteForm('Status')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=Status.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Status</option>
   						<option value="?action=Status.preparaFormulario&sOP=Alterar" lang="1">Alterar Status selecionado</option>
   						<option value="?action=Status.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Status selecionado</option>
   						<option value="?action=Status.processaFormulario&sOP=Excluir" lang="2">Excluir Status selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voStatus)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('Status')"><i class="icon fa fa-check"></a></th>
   					<th class='Titulo'>Id</th>
					<th class='Titulo'>Descrição</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voStatus as $oStatus){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('Status')" type="checkbox" value="<?php echo $oStatus->getCodStatus()?>" name="fIdStatus[]"/></td>
  					<td><?php echo $oStatus->getCodStatus()?></td>
					<td><?php echo  $oStatus->getDescricao()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th></th>
                   <th class='Titulo'></th>
					<th class='Titulo'></th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voStatus)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
