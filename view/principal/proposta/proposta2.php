<?php
ob_start();
set_time_limit(60);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Orçamento</title>
    <meta charset="utf-8">

</head>
<body>


<table style="width:100%">
    <tr>
        <td> <img src="dist/img/logo.svg" width="220">
        </td>
        <td align="center"><h3>SERVIÇO: <?php echo strtoupper($oProposta->getServico()->getDescServico())?></h3></td>
    </tr>
</table>
<div style="width:630px">
    <p>
        <strong>CLIENTE: <?php echo $oProposta->getNome()?>.</strong>
    </p>
	<p>
        <strong>TÍTULO: <?php //echo $oProposta->getTitulo()?>.</strong>
    </p>

    <p>
        DESCRIÇÃO: Conforme solicitado, apresentamos proposta orçamentária para a realização de projeto de <?php echo ucwords($oProposta->getServico()->getDescServico());?> para <?php echo $oProposta->getDescricao()?>.
    </p>
    <p>
        Esta proposta contempla:
    </p>
    <ul>
        <?php foreach($voPropostaMicroServico as $oMicroServico){?>
            <li>
                <em>
                    <?php
                    echo $oMicroServico->getServicoMicroservico()->getDescricao();
                    if($oMicroServico->getQuantidade() >0){
                        echo " QTDE. " . $oMicroServico->getQuantidade() . (($oMicroServico->getDescricaoQuantidade()) ? " (".$oMicroServico->getDescricaoQuantidade().")" : "");
                    }elseif($oMicroServico->getDescricaoQuantidade()){
                        echo " (".$oMicroServico->getDescricaoQuantidade().")";
                    }
                    ?>;
                </em>
            </li>
        <?php } //foreach($voProjetoServicoMicroServico as $oMicroServico){ ?>
    </ul>

    <p>
        <em></em>
    </p>
    <p>
        <strong>VALOR DA PROPOSTA:</strong>
    </p>

    <p>
        <strong>R$ <?php echo $oProposta->getValorPropostaFormatado()?> </strong>(<?php echo strtoupper($oProposta->getValorPropostaFormatadoExtenso())?>)
    </p>
     <p>
        <strong>FORMA DE PAGAMENTO</strong>:
    </p>
    <p>
		À VISTA: R$ <?php echo $oProposta->getValorAvistaFormatado()?></strong>
<BR>
         <strong> À PRAZO: R$ <?php echo $oProposta->getValorPropostaFormatado()?> </strong>
                    (<?php echo $oProposta->getValorParcelaAprazo()?>)

    </p>
    <p>
        <strong></strong>
    </p>
    <p>
        <strong>PRAZOS DE ENTREGA:</strong>
    </p>

    <?php
    $nEtapa=1;
    $sEtapaAnt = "";

    foreach($voProjetoServicoEtapa as $oEtapa){
        $sDescricao = $nEtapa .". ";
        if($oEtapa->getPrazo() > 0){
            $sDescricao .= $oEtapa->getServicoEtapa()->getDescricao()."               ".$oEtapa->getPrazo(). " dias";
        }
        //$sDescricao .= ($nEtapa ==1) ? "data de assinatura do contrato" : (($oEtapa->getPrazo() > 0)? " aprovação do(a) {$sEtapaAnt}":" - a combinar ");
        ?>
        <?php echo $sDescricao ?>;<br>
        <?php $nEtapa++;
        $sEtapaAnt = $oEtapa->getServicoEtapa()->getDescricao();
    } ?>

    <p align="justify">
        <strong>OBS1:</strong>
        <?php if($oProposta->getVisitasIncluidas() > 0){?>
        Está incluso no presente orçamento <?php echo $oProposta->getVisitasIncluidas()?> visitas técnicas ao local com agendamento prévio. Para mais visitas poderá ser cobrada hora técnica no valor de R$ 250,00 (DUZENTOS E CINQUENTA REAIS).
      <?php } else{ ?>
        Visitas técnicas serão custeadas e acordadas com o cliente.
      <?php } ?>
    </p>

    <p>
        <strong></strong>
    </p>
</div>
</body>
</html>
<?php
//  die();
$sPapel = "A4-P";
$sNomeArquivo = preg_replace("[^a-zA-Z0-9_]", "", strtr($oProposta->getNome(). "_".date('Y-m-d'), "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
$sNomeArquivo = preg_replace('<\W+>', "-", $sNomeArquivo);
$sNomeArquivo = "1.proposta_{$sNomeArquivo}.pdf";
include_once("includes/gera_pdf.php");
?>
