<?php
 $sOP = $_REQUEST['sOP'];
 $oProposta = $_REQUEST['oProposta'];
 $oProjeto = $_REQUEST['oProjeto'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Orçamento - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Proposta.preparaLista">Gerenciar Orçamentos</a>
 			<li class="active">Orçamento - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Orçamento - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">
 <label for="CodCliente" class="control-label">Cliente:</label>
		<p><?php echo ($oProposta && $oProposta->getCliente()) ? $oProposta->getCliente()->getNome() : "CLIENTE NOVO"?></p>
	</div>
 <div class="col-md-4">

 <label for="Data" class="control-label">Data:</label>
		<p><?php echo ($oProposta) ? $oProposta->getDataPropostaFormatado() : ""?></p>
	</div>
   <div class="col-md-4">
  <label for="CodServico" class="control-label">Serviço:</label>
 		<p><?php echo ($oProposta) ? $oProposta->getServico()->getDescServico() : ""?></p>
 	</div>
 <div class="col-md-4">
 <label for="Nome" class="control-label">Nome:</label>
		<p><?php echo ($oProposta) ? $oProposta->getNome() : ""?></p>
	</div>
 <div class="col-md-4">

 <label for="Descricao" class="control-label">Descrição:</label>
		<p><?php echo ($oProposta) ? $oProposta->getDescricao() : ""?></p>
</div>
<div class="col-md-4">
<label for="Status" class="control-label">Status:</label>
		<p><?php echo ($oProposta) ? $oProposta->getStatus()->getDescricao() : ""?></p>
	</div>
 <div class="col-md-4">

 <label for="ValorProposta" class="control-label">Valor:</label>
		<p><?php echo ($oProposta) ? "R\$".$oProposta->getValorPropostaFormatado() : ""?></p>
	</div>
 <div class="col-md-4">
 <label for="ValorAvista" class="control-label">Valor à Vista:</label>
		<p><?php echo ($oProposta) ? "R\$".$oProposta->getValorAvistaFormatado() : ""?></p>
	</div>
 <div class="col-md-4">
 <label for="Parcelamento" class="control-label">Parcelamento:</label>
		<p><?php echo ($oProposta) ? $oProposta->getValorParcelaAprazo() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="InseridoPor" class="control-label">Inserido por:</label>
		<p>
      <?php $vInseridoPor = explode("||", $oProposta->getInseridoPor());
            echo ($oProposta) ? "{$vInseridoPor[0]} ; <strong>Em: </strong>{$vInseridoPor[1]}" : ""?></p>
	</div>
<?php  if (($oProposta) && ($oProposta->getAlteradoPor())) {?>
 <div class="col-md-4">

 <label for="AlteradoPor" class="control-label">Alterado Por:</label>
		<p><?php echo ($oProposta) ? $oProposta->getAlteradoPor() : ""?></p>
	</div>
<?php } ?>
   <?php if ($oProjeto){?>
    <div class="col-md-4">
     <label class="control-label"></label>
   		<p><a class="btn-sm btn-primary" role="button" href="?action=Projeto.preparaFormulario&sOP=Detalhar&nIdProjeto=<?php echo $oProjeto->getCodProjeto()?>"> <i class="fa fa-gavel"> </i> Visualizar Contrato</a></p>
   	</div>
   <?php } ?>

 			<div class="form-group col-md-12">
            <p>&nbsp;</p>
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Proposta.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
