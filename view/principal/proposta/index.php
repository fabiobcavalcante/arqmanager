<?php
$voProposta = $_REQUEST['voProposta'];
$voStatus = $_REQUEST['voStatus'];
$oPrincipal = $_REQUEST['oPrincipal'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Lista de Orçamentos </title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li class="active">Gerenciar Orçamentos</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-android-document"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Total</span>
                            <span class="info-box-number"><?php echo $oPrincipal->total_propostas?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-calendar"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Últimos 30 dias</span>
                            <span class="info-box-number"><?php echo $oPrincipal->proposta_aberto_30d?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="ion ion-podium"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Percentual Fechado</span>
                            <span class="info-box-number"><?php echo str_replace(".",",",$oPrincipal->percentual_fechado)?><small>%</small></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-gray"><i class="ion-social-buffer"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">
                                <?php foreach($voStatus as $oStatus){?>
                                    <div class="row">
                                        <span style="padding-left: 20px" class="label label-<?php echo $oStatus->cor?>"><?php echo $oStatus->quantidade ." - ". $oStatus->descricao?></span>
                                    </div>
                                <?php } ?>
                            </span>
                            <span class="info-box-number"></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Orçamentos</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <form method="post" action="" name="formProposta" id="formProposta" class="formulario">
                                <div class="row">
                                    <div class='form-group col-md-4'>
                                        <select class="form-control Acoes" name="acoes" id="acoesProposta" onChange="JavaScript: submeteForm('Proposta')">
                                            <option value="" selected>Ações...</option>
                                            <option value="?action=Proposta.preparaFormulario&sOP=Cadastrar" lang="0">Novo Orçamento</option>
                                            <option value="?action=Proposta.preparaFormulario&sOP=Alterar" lang="1">Alterar Orçamento selecionado</option>
                                            <option value="?action=Proposta.preparaFormulario&sOP=Alterar&sOP2=Cadastrar" lang="1">Novo Orçamento a partir do selecionado</option>
                                            <option value="?action=Proposta.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Orçamento selecionado</option>
                                            <option value="?action=Proposta.processaFormulario&sOP=Excluir" lang="2">Excluir Orçamento(s) selecionado(s)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class='row'></div>

                                <?php if(is_array($voProposta)){?>
                                    <table id="listaProposta" class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-1"><a href="javascript: marcarTodosCheckBoxFormulario('Proposta')"><i class="icon fa fa-check"></a></th>
                                            <th class="col-1">Número</th>
                                            <th class="col-1">Data</th>
                                            <th class="col-1">Status</th>
                                            <th class="col-2">Serviço</th>
                                            <th class="col-3">Cliente</th>
                                            <th class="col-4">Identificação</th>
                                            <th class="col-1">Ação</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($voProposta as $oProposta){ ?>
                                            <tr>
                                                <td><input onClick="JavaScript: atualizaAcoes('Proposta')" type="checkbox" value="<?php echo $oProposta->getCodProposta()?>" name="fIdProposta[]"/></td>
                                                <td><?php echo $oProposta->getNumeroPropostaFormatado()?></td>
                                                <td><?php echo $oProposta->getDataPropostaFormatado()?></td>
                                                <td><span class="label label-<?php echo $oProposta->getVisitasIncluidas()?>"><?php echo $oProposta->getCodStatus()?></span></td>
                                                <td><?php echo $oProposta->getCodServico()?></td>
                                                <td><?php echo $oProposta->getNome()?> </td>
                                                <td><?php echo ($oProposta->getIdentificacao()) ? $oProposta->getIdentificacao(): $oProposta->getDescricao()?></td>
                                                <td>
                                                    <a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=1&nCodProposta=<?php echo $oProposta->getCodProposta()?>" class="btn btn-primary btn-xs"> <i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="Imprimir"></i></a>
                                                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#confirm-delete" data-id="<?php echo $oProposta->getCodProposta()?>"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Cancelar"></i></button>
                                                </td>
                                            </tr>
                                        <?php }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Número</th>
                                            <th>Data</th>
                                            <th>Status</th>
                                            <th>Serviço</th>
                                            <th>Cliente</th>
                                            <th>Identificação</th>
                                            <th>Ação</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                <?php } ?>
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>

</div>
<!-- /.box-body -->

<!-- Modal BOLETO-->
<div class='modal fade' id='PropostaImpressao' role='dialog'>
    <div class='modal-lg modal-dialog'>
        <div class='modal-content'>
            <div class="modal-header"
                 style="color:#FFF; background-color: #696969;; border-top-left-radius: 2px; border-top-right-radius: 2px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">PROPOSTA COMERCIAL</h4>
            </div>
            <div class="modal-body" id="bodyPropostaImpressao"></div>
        </div>
    </div>
</div>

<!-- Modal BOLETO-->
<div class='modal fade' id='PropostaImpressao' role='dialog'>
    <div class='modal-lg modal-dialog'>
        <div class='modal-content'>
            <div class="modal-header"
                 style="color:#FFF; background-color: #696969;; border-top-left-radius: 2px; border-top-right-radius: 2px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">PROPOSTA COMERCIAL</h4>
            </div>
            <div class="modal-body" id="bodyPropostaImpressao"></div>
        </div>
    </div>
</div>

<!-- /.content-wrapper -->
<?php include_once('includes/footer.php')?>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script>

    $('#confirm-delete',0,0).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget,0,0);
        let modal = $(this,0,0);
        let nCodProposta = button.data("id");
        modal.find("#ok").attr("href","?action=Proposta.processaFormulario&sOP=AlterarStatus&nStatus=4&fCodProposta="+nCodProposta);
    });

    $('#listaProposta').DataTable({
        dom: 'Bfrtip',
        "columns": [
            null,
            null,
            { "type": "date-euro" },
            null,
            null,
            null,
            null,
            null
        ],
        buttons: ['excel', 'pdf', 'print'],
        'responsive': true,
        'paging'      : true,
        'lengthChange': false,
        'info'        : true,
        'autoWidth'   : true,
        "pagingType"  : "full",
        "language": {
            "decimal": ",",
            "thousands": ".",
            "lengthMenu": "Mostrar _MENU_ registros por p&aacute;gina",
            "zeroRecords": "Nada encontrado",
            "info": "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro Encontrado",
            "infoFiltered": "(filtrados de _MAX_ registros)",
            "search":         "Pesquisar:",
            "paginate": {
                "first":      "Primeiro",
                "last":       "&Uacute;ltimo",
                "next":       "Pr&oacute;ximo",
                "previous":   "Anterior"
            }
        }
    });

</script>
</body>
</html>
