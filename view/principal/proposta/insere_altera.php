<?php
$sOP = $_REQUEST['sOP'];
$oProposta = $_REQUEST['oProposta'];
$voServico = $_REQUEST['voServico'];

$voCliente = $_REQUEST['voCliente'];
$voStatus = $_REQUEST['voStatus'];

 $sOP2 = $_GET['sOP2'] ? $_GET['sOP2']  : null;
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Orçamento</title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light" <?php echo ($oProposta) ? "onload='etapaServico({$oProposta->getCodServico()},{$oProposta->getCodProposta()})'" : ''?>>
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Proposta.preparaLista">Gerenciar Orçamentos</a>
                <li class="active">Orçamento - <?php echo ($sOP2) ?: $sOP?></li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->

        <section class="content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Proposta - <?php echo ($oProposta) ? "<strong>{$oProposta->getNumeroPropostaFormatado()}</strong>" : "" ?> <?php echo ($sOP2) ? $sOP2 : $sOP ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <form method="post" class="form-horizontal" name="formProposta" id="formPropostaProjeto" action="?action=Proposta.processaFormulario">
                            <input type="hidden" name="sOP" value="<?php echo ($sOP2) ? $sOP2 : $sOP; ?>" >
                            <div  class="box-body" id="form-group">
                                <input type='hidden' name='fCodProposta' id="idProposta" value='<?php echo ($sOP2) ? false : (($oProposta) ? $oProposta->getCodProposta() : "")?>'>
                                <input type='hidden' name='fNumeroProposta' value='<?php echo ($oProposta) ? $oProposta->getNumeroProposta() : ""?>'>
                                <input type='hidden' name='fAnoProposta' value='<?php echo ($oProposta) ? $oProposta->getAnoProposta() : ""?>'>
                                <?php if($sOP == "Cadastrar" || $sOP2 == "Cadastrar"){?>
                                    <input type='hidden' name='fCodStatus' value='1'>
                                <?php } ?>

                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="Data">Data:</label>
                                    <div class="input-group col-md-11">
                                        <input class="form-control date" type='text' id='Data' name='fDataProposta' required value='<?php echo ($oProposta) ? $oProposta->getDataPropostaFormatado() :  date('d/m/Y')?>'>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 col-sm-12">
                                    <label for="Servico">Serviço:</label>
                                    <div class="input-group col-md-11  col-sm-12">
                                        <?php if ($sOP == 'Cadastrar'){ ?>
                                            <select name='fCodServico' id="Servico" class="form-control select2"  required  onchange="etapaServico(this.value)">
                                                <option value=''>Selecione</option>
                                                <?php $sSelected = "";
                                                if($voServico){
                                                    foreach($voServico as $oServico){
                                                        if($oProposta){
                                                            $sSelected = ($oProposta->getCodServico() == $oServico->getCodServico()) ? "selected" : "";
                                                        }
                                                        ?>
                                                        <option  <?php echo $sSelected?> value='<?php echo $oServico->getCodServico()?>'><?php echo $oServico->getDescServico()?></option>
                                                        <?php
                                                    }
                                                } ?>
                                            </select>
                                        <?php } else{
                                            echo $oProposta->getServico()->getDescServico(); ?>
                                            <input type="hidden" name='fCodServico' id="Servico" required value='<?php echo $oProposta->getCodServico() ?>'>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Cliente">Cliente:</label>
                                    <div class="input-group col-md-11">
                                        <select name='fCodCliente' id="Cliente" class="form-control select2" >
                                            <option value=''>Selecione</option>
                                            <?php $sSelected = "";
                                            if($voCliente){
                                                foreach($voCliente as $oCliente){
                                                    if($oProposta){
                                                        $sSelected = ($oProposta->getCodCliente() == $oCliente->getCodCliente()) ? "selected" : "";
                                                    }
                                                    ?>
                                                    <option  <?php echo $sSelected?> value='<?php echo $oCliente->getCodCliente()?>'><?php echo $oCliente->getNome()?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="Nome">Nome:</label>
                                    <div class="input-group col-md-11">
                                        <input class="form-control maiuscula" type='text' id='Nome' placeholder='Nome' name='fNome' required value='<?php echo ($oProposta) ? $oProposta->getNome() : ""?>'>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="Identificacao">Identificação:</label>
                                    <div class="input-group col-md-11">
                                        <input class="form-control maiuscula" type='text' id='Identificacao' placeholder='Identificação' name='fIdentificacao' required value='<?php echo ($oProposta) ? $oProposta->getIdentificacao() : ""?>'>
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="ValorProposta">Valor:</label>
                                    <div class="input-group col-md-11">
                                        <input class="form-control money" type='text' id='ValorProposta' placeholder='Valor' name='fValorProposta' onkeyup="FormataMoney(this);" required   value='<?php echo ($oProposta) ? $oProposta->getValorPropostaFormatado() : ""?>'>
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="EntregaParcial">Entrega Parcial:</label>
                                    <div class="input-group col-md-11">
                                        <select class="form-control" name="fEntregaParcial" id="EntregaParcial" required onchange="entregaFracionada(this.value)">
                                            <option value="">Selecione</option>
                                            <option value="Não" <?php echo ($oProposta && $oProposta->getEntregaParcial() == 'Não') ? "selected" : ""?>>Não</option>
                                            <option value="Sim" <?php echo ($oProposta && $oProposta->getEntregaParcial() == 'Sim') ? "selected" : ""?>>Sim</option>
                                        </select>
                                    </div>
                                </div>

                                <?php if($_SESSION['oEscritorio']->getHoraTecnica()){?>
                                    <div class="form-group col-md-1">
                                        <label for="Visitas">Visitas:</label>
                                        <div class="input-group col-md-11">
                                            <input class="form-control" type='number' max="5" size="1" id='Visitas' placeholder='Visitas' name='fVisitasIncluidas' required value='<?php echo ($oProposta) ? $oProposta->getVisitasIncluidas() : ""?>'>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <input type='hidden' name='fVisitasIncluidas'  value='<?php echo ($oProposta) ? $oProposta->getVisitasIncluidas() : "0"?>'>
                                <?php } ?>


                                <?php if($_SESSION['oEscritorio']->getIdEscritorio() == 1){
                                    $classtextarea = "form-control";
                                    ?>
                                    <div class="form-group col-md-3">
                                        <label for="ValorAvista">Valor à Vista:</label>
                                        <div class="input-group col-md-11">
                                            <input class="form-control money" type='text' id='ValorAvista' placeholder='Valor à Vista' name='fValorAvista'  required  onkeyup="FormataMoney(this);"  value='<?php echo ($oProposta) ? $oProposta->getValorAvistaFormatado() : ""?>'>
                                        </div>
                                    </div>
                                <?php } else{
                                        $classtextarea = "textarea clean wysihtml5";
                                    ?>
                                    <input type='hidden' name='fValorAvista' required value='0'>

                                <?php } ?>

                                <div class="form-group col-md-4">
                                    <label for="Descricao">Descrição:</label>
                                    <div class="input-group col-md-11">
                                        <textarea name="fDescricao" id="Descricao" class="form-control" placeholder="Descrição" style="width: 100%; height: 150px; font-size: 15px; line-height: 14px; border: 1px solid #dddddd; padding: 10px;"><?php echo ($oProposta) ? $oProposta->getDescricao() : ""?></textarea>
                                    </div>
                                </div>

                                <div class="form-group col-md-4" id="FormaPagamento">
                                    <label for="ValorParcelaAprazo">Forma de Pagamento:</label>
                                    <div class="input-group col-md-11">
                                        <textarea name="fValorParcelaAprazo" id="ValorParcelaAprazo" class="<?php echo $classtextarea?>" placeholder="Forma de Pagamento" style="width: 100%; height: 150px; font-size: 15px; line-height: 14px; border: 1px solid #dddddd; padding: 10px;"><?php echo ($oProposta) ? $oProposta->getValorParcelaAprazo() : "";?></textarea>
                                    </div>
                                </div>
                                <?php if($_SESSION['oEscritorio']->getIdEscritorio() == 2){?>
                                <div class="form-group col-md-4">
                                    <label for="Prazo">Prazo:</label>
                                    <div class="input-group col-md-11">

                                        <textarea name="fPrazoProposta" id="Prazo" class="textarea clean wysihtml5" placeholder="Prazo" style="width: 100%; height: 150px; font-size: 15px; line-height: 14px; border: 1px solid #dddddd; padding: 10px;"><?php echo ($oProposta) ? $oProposta->getPrazo() : "";?></textarea>
                                    </div>
                                </div>
                                <?php } //if($_SESSION['oEscritorio']->getIdEscritorio() == 2){?>
                                <div id="divFracionamento" class="form-group col-md-12"></div>
                                <div id="divMicroServicos" class="form-group col-md-12"></div>
                                <div id="divServicoEtapa" class="form-group col-md-12"></div>

                                <div class="form-group col-md-12">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <button type="submit" class="btn btn-lg btn-success"><?php echo ($sOP2) ?: $sOP; ?></button>

                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>

    <div class="modal fade" id="modalEntrega">
        <div class="modal-dialog">
            <div class="modal-content" id="bodyEntrega"></div>
        </div>
    </div>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
<script type="text/javascript">
    $(document).ready(function() {
        entregaFracionada($("#EntregaParcial").val());
    });


    function toggle(source) {
        checkboxes = document.getElementsByName('foo');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    function entregaFracionada(nEntrega){
        if (nEntrega === "Sim"){
            $("#FormaPagamento").hide();
            recuperaConteudo('index.php','?action=PropostaFracionada.preparaLista&sOP=Fracionamento&nIdProposta=<?php echo ($oProposta) ? $oProposta->getCodProposta() : ""?>','divFracionamento');
        } else{
            $("#FormaPagamento").show();
            $("#divFracionamento").html("");
        }

    }

    function etapaServico(nCodServico,nCodProposta=null){
        if(!nCodProposta){
            recuperaConteudo('index.php','?action=ServicoMicroServico.preparaLista&sOP=Servico&bPagamento=0&nIdServico=' + nCodServico,'divMicroServicos');
            recuperaConteudo('index.php','?action=ServicoEtapa.preparaLista&sOP=Servico&bPagamento=0&nIdServico=' + nCodServico,'divServicoEtapa');
        }else{
            recuperaConteudo('index.php','?action=ServicoMicroServico.preparaLista&bPagamento=0&sOP=Servico&sOP2=Alterar&nCodProposta='+nCodProposta+'&nIdServico=' + nCodServico,'divMicroServicos');
            recuperaConteudo('index.php','?action=ServicoEtapa.preparaLista&bPagamento=0&sOP=Servico&sOP2=Alterar&nCodProposta='+nCodProposta+'&nIdServico=' + nCodServico,'divServicoEtapa');
        }

    }

    <?php if(isset($_REQUEST['oProjeto']) && isset($_REQUEST['sOP2']) && $_REQUEST['sOP2'] != 'Cadastrar'){?>
    $(function(){
        $("input").attr("readonly","readonly");
        $("select").attr("disabled","disabled");
        $("button").attr("disabled","disabled");

    });
    <?php } ?>

    $(function () {
        $('.wysihtml5').wysihtml5({
            toolbar: {
                "font-styles": false,
                "blockquote": false
            }
        });
    })

    $(function() {
        var textareas = $('textarea.clean');
        $.each(textareas, function(key, value) {
            $(this).val($(this).val().replace(/[ ]+/g, ' ').replace(/^[ ]+/m, ''));
            $('a[title="CTRL+S"]').remove(); // Remove o button Small
            $('a[title="CTRL+U"]').remove(); // Remove o button Underline
            $('a[title="Insert image"]').remove(); // Remove o button Insert Image
            $('a[title="Insert link"]').remove(); // Remove o button Insert Link
        })
    });

    function abreModalEntrega(){
        let nCodServico = $("#Servico").val();
        recuperaConteudoDinamico("","action=ServicoMicroServico.preparaFormulario&nCodServico="+nCodServico+"&sOP=Cadastrar", "bodyEntrega");
        $("#modalEntrega").modal('show');
    }

    $("#modalEntrega").on('shown.bs.modal', function(){
        $('.my-colorpicker1').colorpicker()
    });

    function processaFormulario(sForm) {

        let form = $("#" + sForm);

        $.ajax({
            type: "POST",
            datatype: "json",
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function () {
                Swal.fire({
                    title: 'Enviando...',
                    didOpen: () => {
                        Swal.showLoading()
                    }
                })
            },
            error: function (oXMLRequest, sErrorType) {
                console.log(form.attr('action'));
                console.log(oXMLRequest.responseText);
                console.log(oXMLRequest.status + ' , ' + sErrorType);
            },
            success: function (dados) {
                console.log(dados);
                dados = jQuery.parseJSON(dados);

                if (dados.sClass === "success") {

                    recuperaConteudo('index.php', '?action=ServicoMicroServico.preparaLista&sOP=Servico&bPagamento=0&nIdServico=' + dados.nCodServico, 'divMicroServicos');

                    $('#modalEntrega').modal('hide');
                }

                Swal.fire({
                    icon: dados.sClass,
                    title: dados.sMsg,
                    timer: 2000
                });
            }
        });
    }

</script>
</body>
</html>
