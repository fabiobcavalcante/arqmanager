<?php
$sOP = $_REQUEST['sOP'];
$voProjetoArquivamento = $_REQUEST['voProjetoArquivamento'];

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Projetos Arquivados </title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?> - Projetos Arquivados</h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
                <li class="active">Arquivados</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">


            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title btn btn-block btn-warning btn-sm-1">Projetos Arquivados</h4>
                        </div>
                        <!-- /.box-header -->
                        <ul class="nav nav-pills nav-fill">
                          <li class="nav-item">
                            <a class="nav-link" href="?action=Relatorio.preparaLista&sOP=Entrega">Ativo</a>
                          </li>
                          <li class="nav-item">

                            <a class="nav-link active" href="?action=Relatorio.preparaLista&sOP=Arquivados">Arquivados</a>
                          </li>
                        </ul>
                        <div class="box-body table-responsive">

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width:25%">CLIENTE / PROJETO</th>
                                    <th style="width:10%">DATA</th>
                                    <th style="width:15%">COLABORADOR</th>
                                    <th style="width:50%">MOTIVO</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($voProjetoArquivamento){ ?>
                                    <?php foreach($voProjetoArquivamento as $oProjetoArquivamento){ ?>
                                        <tr>
                                            <td><button data-toggle="tooltip" data-placement="right" title="Desarquivar" onclick="abreModal('Desarquivar',<?php echo $oProjetoArquivamento->cod_projeto; ?>,7)"><i style="font-size:18px;" class="fa fa-archive" aria-hidden="true"></i> </button> <?php echo $oProjetoArquivamento->identificacao_projeto?></td>
                                            <td><?php echo $oProjetoArquivamento->data_acao_formatada?></td>
                                            <td><?php echo $oProjetoArquivamento->cod_colaborador?></td>
                                            <td><?php echo $oProjetoArquivamento->motivo_acao?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Detalhar -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document" id="tamanhoModal">
            <div class="modal-content" id="bodyModal"></div>
        </div>
    </div>


    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>

<script>

    function abreModal(sFuncao,nCodProjeto,nCodServicoEtapa,nCodMicroServico){
        let sLink = "";
        switch (sFuncao){
            case "Desarquivar":
                sLink = "action=Projeto.preparaFormulario&sOP=AlterarStatus&nCodProjeto=" + nCodProjeto + "&CodStatus=" + nCodServicoEtapa;
                recuperaConteudoDinamico("",sLink,'bodyModal');
                $('#tamanhoModal').removeClass('modal-lg');
                break;

        }
        $('#modal').modal('show');

    }



    //Função para arquivar
    jQuery(document).submit(function(e){
        let form = jQuery(e.target);
        if(form.is("#formArquivamento")){ // check if this is the form that you want (delete this check to apply this to all forms)
            e.preventDefault();
            $.ajax({
                type: "POST",
                datatype: "json",
                url: form.attr('action'),
                data: form.serialize(), // serializes the form's elements.
                beforeSend: function(){
                    Swal.fire({
                        title: 'Enviando...',
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                error: function(oXMLRequest,sErrorType){
                    console.log(sUrl);
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status + ', ' + sErrorType);
                },
                success: function(dados){
                    dados = jQuery.parseJSON(dados);
                    if (dados.sClass === 'success')
                        $('#modal').modal('hide');
                    Swal.fire({
                        icon: dados.sClass,
                        title: dados.sMsg,
                        timer: 2000
                    });
                    document.location.reload(true);
                }
            });
        }
    });

    function FormataStringData(data) {
        let dia  = data.split("/")[0];
        let mes  = data.split("/")[1];
        let ano  = data.split("/")[2];
        let dData = ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);

        let dDataConclusao = new Date(dData);
        let dDataPrevista = $("#DataPrevista").val();

        dia  = dDataPrevista.split("/")[0];
        mes  = dDataPrevista.split("/")[1];
        ano  = dDataPrevista.split("/")[2];
        dData = ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);

        dDataPrevista = new Date(dData);

        if (dDataPrevista < dDataConclusao)
            $("#Observacao").attr("required",true);
        else
            $("#Observacao").attr("required",false);
    }


    $(document).ready(function() {

        let table = $('.table').DataTable({
            dom: 'Bfrtip',
            search: {
                "smart": true
            },
            columnDefs: [
                {type: 'date-euro', targets: [3]}
            ],
            buttons: [
                {
                    extend: 'pdfHtml5',
                    className: 'btn btn-primary',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    title: 'Entregas'
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-primary',
                    title: 'Entregas'
                },
                {
                    extend: 'csv',
                    className: 'btn btn-primary',
                    title: "lista_entregas",
                    charset: 'utf-8',
                    bom: true
                },
                {
                    extend: 'print',
                    className: 'btn btn-primary',
                    text: 'Imprimir',
                },
                {
                    extend: 'colvis',
                    className: 'btn btn-primary',
                    text: 'Selecionar colunas'
                }
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json",
                "decimal": ",",
                "thousands": "."
            },
            "paging": false
        });
        table
            .order( [ 3, 'asc' ] )
            .draw();
    });
</script>

</body>
</html>
