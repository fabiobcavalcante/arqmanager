<?php
 $sOP = $_REQUEST['sOP'];
 $voColaborador = $_REQUEST['voColaborador'];
 $voAno = $_REQUEST['voAnos'];
 $oColaboradorSelecionado = $_REQUEST['oColaboradorSelecionado'];
 $nAnoSelecionado = $_REQUEST['nAnoSelecionado'];

 $voResultado = $_REQUEST['voResultado'];
 $oTotais = $_REQUEST['oTotais'];
 $f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);

 ?>
<!doctype html>
<html lang="pt-br">
<head>
<title>Relatório por Colaborador</title>
<?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
<?php include_once('includes/header.php')?>
<?php include_once('includes/menu.php')?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
    <ol class="breadcrumb">
      <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
      <li class="active">Relatório por Colaborador</li>
    </ol>
    <?php include_once('includes/mensagem.php')?>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div  class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Relatórios - por Colaborador</h3>
        </div>
        <div class="box-body">
          <form action="?action=Relatorio.preparaFinanceiroColaborador&sOP=Visualizar" method="post">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="col-sm-1 control-label" >Colaborador:</label>
                <div class="col-sm-3 col-xs-3">
                  <?php if($voColaborador){ ?>
                  <select class="form-control select2" name="fCodColaborador">
                    <option value="">Selecione</option>
                    <?php foreach($voColaborador as $oColaborador){
                        if($oColaboradorSelecionado)
                              $sSelected = ($oColaboradorSelecionado->getCodColaborador() == $oColaborador->getCodColaborador()) ? " selected='selected' " : "";
                            ?>
                    <option <?php echo $sSelected ?> value="<?php echo $oColaborador->getCodColaborador();?>"><?php echo $oColaborador->getNome();?></option>
                    <?php } ?>
                  </select>
                  <?php }  ?>
                </div>
                <label class="col-sm-1 control-label" >Ano:</label>
                <div class="col-sm-1 col-xs-1">
                  <?php if($voAno){ ?>
                  <select class="form-control select2" name="fAno">
                    <option value="">Selecione</option>
                    <?php foreach($voAno as $oAno){
                              if($nAnoSelecionado)
                                $sSelected = ($nAnoSelecionado == $oAno->getDataFim()) ? " selected='selected' " : "";
                            ?>
                    <option <?php echo $sSelected ?> value="<?php echo $oAno->getDataFim();?>"><?php echo $oAno->getDataFim();?></option>
                    <?php } ?>
                  </select>
                  <?php }  ?>
                </div>
                <div class="col-xs-1">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-filter fa-lg"></i> Filtrar</button>
                </div>
                <!-- /.form group -->
              </div>
            </div>
          </form>
        </div>
        <div class="box-footer">
          <?php if(is_array($voResultado)){?>
          <div class="box  border">
            <div class="box-header">
              <h4>PRODUÇÃO (<?php echo $oColaboradorSelecionado->getNome();?>)</h4>
            </div>
            <div class="box-body">
            <!--
              <div class="row">
                <div class="col-lg-3 col-xs-6">
                  <!-- small box
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3>
                        <?php  //echo $oTotais->getAtivo();?>
                      </h3>
                      <p>Nº Entradas</p>
                    </div>
                    <div class="icon"> <i class="ion ion-bag"></i> </div>
                    <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                </div>

                <!-- ./col
                <div class="col-lg-3 col-xs-6">
                  <!-- small box
                  <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3><?php //echo $f->format($oTotais->getIncluidoPor());?></h3>
                      <p>Total Previsto</p>
                    </div>
                    <div class="icon"> <i class="fa fa-calendar" aria-hidden="true"></i> </div>
                    <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                </div>
                <!-- ./col
                <div class="col-lg-3 col-xs-6">
                  <!-- small box
                  <div class="small-box bg-green">
                    <div class="inner">
                      <h3><?php //echo $f->format($oTotais->getValor());?></h3>
                      <p>Total Mês Confirmados</p>
                    </div>
                    <div class="icon"> <i class="fa fa-usd" aria-hidden="true"></i> </div>
                    <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                </div>

                <!-- ./col
                <div class="col-lg-3 col-xs-6">
                  <!-- small box
                  <div class="small-box bg-red">
                    <div class="inner">
                      <h3><?php //echo str_replace('.',',',$oTotais->getAlteradoPor());?><sup style="font-size: 20px">%</sup></h3>
                      <p>Recebido</p>
                    </div>
                    <div class="icon"> <i class="ion ion-pie-graph"></i> </div>
                    <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a> </div>
                </div>
              </div>
            </div>
          </div>
          -->
          <div class="box-body grid_resultado">
          <div class="row">
          <div style="padding:5px">
            <div class="col-sm-2 grid-topo" align="center"><strong>OPÇÕES</strong></div>
            <div class="col-sm-2 grid-topo"><strong>MÊS</strong></div>
            <div class="col-sm-2 grid-topo"><strong>PROJETOS</strong></div>
            <div class="col-sm-2 grid-topo"><strong>VALOR BASE</strong></div>
            <div class="col-sm-2 grid-topo"><strong>COMISSÕES</strong></div>
            <div class="col-sm-2 grid-topo"><strong>REMUNERAÇÃO</strong></div>
          </div>
          <?php foreach($voResultado as $oResultado){
                     $nTotalValorBase = $nTotalValorBase + $oResultado->valor_base;
                     $nTotalValorComissao = $nTotalValorComissao + $oResultado->comissoes;
                     $nTotalValorPagar = $nTotalValorPagar + $oResultado->total_receber;

            ?>
          <div class="row">
            <div class="table-bordered col-sm-2" align="center"><button class='btn btn-success btn-sm pull-center' <?php echo ($oResultado->comissao == 0)?" disabled " : ""?> onClick="MostraEsconde2('divDetalhe<?php echo $oResultado->cod_colaborador?>')"> <i class='fa fa-plus'></i> </button> <button class='btn btn-primary btn-sm pull-center' onclick='abreModal(<?php echo $oResultado->cod_colaborador;?>,<?php echo $oResultado->mes?>,<?php echo $oResultado->ano?>)'><i class='fa fa-file'></i></button></div>
            <div class="table-bordered col-sm-2" style="padding:5px"><?php echo $oResultado->mes?></div>
            <div class="table-bordered col-sm-2" style="padding:5px"><?php echo $oResultado->numero_projetos?></div>
            <div class="table-bordered col-sm-2" style="padding:5px"><?php echo $f->format($oResultado->salario)?></div>
            <div class="table-bordered col-sm-2" style="padding:5px"><?php echo $f->format($oResultado->comissao)?></div>
            <div class="table-bordered col-sm-2" style="padding:5px"  ><?php echo $f->format($oResultado->remuneracao);?></div>
          </div>
          <div class="row">
            <?php
                  $voProjeto = $oFachada->recuperarTodosProjetoColaboradorPorMesAnoColaboradorEfetivado($oResultado->mes,$oResultado->ano, $oResultado->cod_colaborador);
                  if($voProjeto){
            ?>
                  <div id="divDetalhe<?php echo $oResultado->cod_colaborador?>" style="display:none">
                    <?php  foreach($voProjeto as $oProjeto){
                              $vServico = explode("||",$oProjeto->getCodServicoDetalhe());
                              $voServicoSubDetalhe = $oFachada->recuperarTodosServicoDetalhePorServico($vServico[0]);
                     ?>
                      <div class="col-sm-1" align="right"><button class='btn btn-success btn-sm' onClick="MostraEsconde2('divSubDetalhe<?php echo $vServico[3] . "_" .$vServico[0]. "_" . $oResultado->cod_colaborador ;?>')"><i class='fa fa-plus'></i> </button></div>
                      <div class="table-bordered col-sm-3" style="padding:5px; background-color:#c4c4bb;"><strong>CLIENTE:</strong> <?php echo $vServico[2];?></div>
                      <div class="table-bordered col-sm-2" style="padding:5px"><strong>SERVIÇO:</strong> <?php echo $vServico[1];?></div>
                      <div class="table-bordered col-sm-2" style="padding:5px"><?php echo $f->format($oProjeto->getCodProjeto())?></div>
                      <div class="table-bordered col-sm-2" style="padding:5px"><?php echo $oProjeto->getPercentual()?></div>
                      <div class="table-bordered col-sm-2" style="padding:5px"><?php echo $f->format($oProjeto->getCodProjetoColaborador())?></div>
                <div id="divSubDetalhe<?php echo $vServico[3] . "_" .$vServico[0] . "_" . $oResultado->cod_colaborador ;?>" style="display:none">
                <?php
                  foreach($voServicoSubDetalhe as $oServicoDetalhe){
                      $oPercentual = $oFachada->recuperarUmProjetoColaboradorPorColaboradorProjetoServicoDetalhe($oResultado->cod_colaborador, $vServico[3],$oServicoDetalhe->getCodServicoDetalhe());
                  ?>
              <div class="row grid_resultado2">
                <div class="col-sm-4">&nbsp</div>
                <div class="table-bordered col-sm-4"  style="background-color:#e7e7e4;" ><?php echo $oServicoDetalhe->getDescServicoDetalhe()?></div>
                <div class="table-bordered col-sm-2" ><?php echo ($oPercentual) ? $oPercentual->getPercentual() : "-"?></div>
                <div class="table-bordered col-sm-2" ><?php echo ($oPercentual) ? round((($oPercentual->getPercentual()/100) * $oProjeto->getCodProjeto()),2) : "-"?></div>
              </div>
            <?php }//foreach($voServicoDetalhe as $oServicoDetalhe){ ?>
            </div>
          <?php  } //  foreach($voProjeto as $oProjeto){ ?>
            </div>
          <?php } //if($voProjeto){ ?>
          </div>
          <?php    } //foreach($voResultado as $oResultado){
              ?>

            <?php }//if(is_array($voResultado)){?>
            </div>
            <!-- /.row-->
          </div>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" width="100%">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <label class="modal-title">Relatório Financeiro</label>
      </div>
      <div class="modal-body">
        <div id="divPrint"></div>
      </div>

      <div class="modal-footer">
        <a id="imprimir" href="" target="_blank" class="btn btn-info btn"><span class="fa fa-print"></span> IMPRIMIR</a>
        <button type="button" class="btn btn-danger"  data-dismiss="modal"><span class="fa fa-power-off"></span> FECHAR</button>


      </div>
    </div>
  </div>
</div>
<?php include_once('includes/javascript.php')?>
//Ajax
<script type="text/javascript">
function abreModal(nColaborador,mes,ano){
  var sLink = '?action=Relatorio.preparaFinanceiroMensalPrint&nCodColaborador='+nColaborador+'&nMes='+mes+'&nAno='+ano;
  $.ajax({
    type: 'GET',
    url: sLink,
    success: function(data){
      $('#divPrint').html(data);
      $('#imprimir').attr("href", sLink+"&sImpressao=sim"); // Set herf value

      $('#myModal').modal('show');
    }
  });
}


</script>
</body>
</html>
