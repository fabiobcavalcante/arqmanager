﻿<?php
$sOP = $_REQUEST['sOP'];
$voMeses = $_REQUEST['voMeses'];
$voResultado = $_REQUEST['voResultado'];
$oPrincipal = $_REQUEST['oPrincipal'];
$nAno = ($_REQUEST['nAno']) ? $_REQUEST['nAno'] : date('Y');
$nMesSelecionado = $_REQUEST['nMesSelecionado'];
$sStatus = $_REQUEST['sStatus'];

$voFormaPagamento  = $_REQUEST['voFormaPagamento'];
$voPermissao = array(2,3);
$voContaBancaria = $_REQUEST['voContaBancaria'];
if($sOP == 'Cadastrar')
    $voProposta = $_REQUEST['voProposta'];
$f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
$vPermissao = array(1,2,3);

$bPagamento = 0;

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>FINANCEIRO</title>
    <?php include_once('includes/head.php')?>

</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li class="active">Relatório Financeiro <?php echo $sStatus?></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">

                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <label for="ANO">ANO:</label>
                            <div class="input-group col-md-11 col-sm-12">
                                <input class="form-control" id="fAno" name="fAno" value="<?php echo $nAno?>" onblur="atualizaAno(this.value);">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <label for="Situação">SITUAÇÃO:</label>
                            <div class="input-group col-md-11 col-sm-12">
                                <select class="form-control select2" name="fSituacao" onchange="atualizaSituacao(this.value);">
                                    <option value="1" <?php echo ($_GET['nEfetivado'] == 1)? " selected " : ""?>>Previstos</option>
                                    <option value="2" <?php echo ($_GET['nEfetivado'] == 2)? " selected " : ""?>>Confirmados</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-12">
                            <button type="button" class="btn btn-app bg-light-blue" title="Gráfico"  data-toggle="modal" onclick="carregaGrafico()" data-target="#carregaPainel" ><i class="fa fa-line-chart"></i> Painel</button>
                        </div>
                        <div class="col-md-1 col-sm-12">
                            <button type="button" class="btn btn-app bg-blue-gradient" title="A Receber" ><i class="fa fa-plus"></i> <?php echo $oPrincipal->total_previsao?>  </button>
                        </div>
                        <div class="col-md-1 col-sm-12">
                            <button type="button" class="btn btn-app bg-green" title="Confirmado" ><i class="fa fa-check"></i>1.477.200,00</button>
                        </div>

                    </div>


                    <p>&nbsp;</p>
                    <ul class="nav nav-tabs">
                        <?php foreach($voMeses as $oMes){ ?>
                            <?php if ($oMes->nMes == $nMesSelecionado)
                                $sMes = $oMes->sMes;
                            ?>
                            <li <?php echo ($oMes->mes == $nMesSelecionado ? " class='active'" : "")?>><a href="?action=Relatorio.preparaFinanceiroMensal&sOP=Visualizar&nEfetivado=<?php echo $_REQUEST['nEfetivado']?>&nAno=<?php echo $nAno?>&nMesSelecionado=<?php echo $oMes->mes?>"><?php echo $oMes->mes_descricao?></a></li>
                        <?php } ?>
                    </ul>

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-money" aria-hidden="true"></i> PAGAMENTOS <?php echo $sStatus?></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover" id="tableFinanceiro" style="width: 100%">
                                <?php if($voResultado){ ?>
                                    <thead>
                                    <tr>
                                        <th>Previsão</th>
                                        <th>Confirmação</th>
                                        <th>Cliente</th>
                                        <th>Projeto</th>
                                        <th>Descrição</th>
                                        <th>Valor</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $nTotal=0; foreach($voResultado as $oResultado){
                                        if($_GET['nEfetivado'] == 1){
                                            switch($oResultado->data_previsao){
                                                case $oResultado->data_previsao < date('Y-m-d'):
                                                    $sClass='class="danger"';
                                                    break;
                                                case $oResultado->data_previsao > date('Y-m-d'):
                                                    $sClass='class="success"';
                                                    break;
                                                case $oResultado->data_previsao = date('Y-m-d'):
                                                    $sClass='class="warning"';
                                                    break;

                                            }
                                        }else{
                                            $sClass="";
                                        }
                                        ?>
                                        <tr <?php echo $sClass;?>>
                                            <td><?php echo $oResultado->data_previsao_formatada?></td>
                                            <td><?php echo $oResultado->data_efetivacao_formatada?></td>
                                            <td><?php echo $oResultado->nome?></td>
                                            <td><?php echo $oResultado->titulo?></td>
                                            <td><?php echo $oResultado->desc_pagamento?></td>
                                            <td><?php echo $oResultado->valor_formatado?></td>
                                            <td>
                                                <?php if($_GET['nEfetivado'] == 1){?>

                                                    <a href="?action=Email.processaFormulario&sOP=Visualizar&sIdTipo=3D&fCodProjetoPagamento=<?php echo $oResultado->cod_projeto_pagamento?>" class="btn btn-primary btn-sm">
                                                        <i class='fa fa-envelope-o'></i>
                                                    </a>

                                                    <button id="botaoConfirma<?php echo $oResultado->cod_projeto_pagamento;?>" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-codigo-confirmacao="<?php echo $oResultado->cod_projeto_pagamento;?>" data-valor="<?php echo $oResultado->valor_formatado;?>" data-forma-pagamento="<?php echo $oResultado->cod_forma_pagamento; ?>" data-previsao="<?php echo $oResultado->data_previsao_formatada;?>" data-descricao-pagamento="<?php echo $oResultado->desc_pagamento;?>" data-target="#modalConfirmacaoPagamento"> <i class="fa fa-usd"></i></button>
                                                <?php }else{?>
                                                    <a class="btn btn-primary btn-sm" href="http://luanapalheta.com.br/admin/<?php echo $oResultado->comprovante ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Pago em <?php echo $oResultado->data_efetivacao_formatada ?>"><i class="fa fa-check"></i></a>

                                                <?php } ?>
                                            </td>
                                        </tr>

                                        <?php $nTotal += $oResultado->valor; } ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="4">TOTAL MÊS</th>
                                        <th><strong><?php echo number_format($nTotal,2,",",".");?><strong></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="3">Não há pagamentos <?php echo $sStatus?> no período informado</td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <div class="form-group col-md-12">
                        <div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?">Voltar</a></div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>

    <!-- Modal -->
    <div class="modal fade" id="modalConfirmacaoPagamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="?action=ProjetoPagamento.processaFormulario" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLongTitle">Confirmação de Pagamento</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="">

                            <input type="hidden" name="sOP" value="ConfirmarPagamento">
                            <input type="hidden" id="CodProjetoPagamento" name="nCodProjetoPagamento" value="">
                            <input type="hidden" id="CodFormaPagamento" name="fCodFormaPagamento" value="">
                            <input type="hidden" id="Valor" name="fValor" value="">
                            <input type="hidden" id="DataPrevisao" name="fDataPrevisao" value="">
                            <input type="hidden" id="DescPagamento" name="fDescPagamento" value="">

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="ContaBancaria" class="control-label">Conta Destino</label>
                                    <select class="form-control select2" name="fCodContaBancaria" id="ContaBancaria" style="width:100%" onchange="if(this.value !== '') $('#btnSalvar').prop('disabled',false);else $('#btnSalvar').prop('disabled',true);" >
                                        <option value="">Selecione</option>
                                        <?php foreach($voContaBancaria as $oContaBancaria){?>
                                            <option value="<?php echo $oContaBancaria->getCodContaBancaria()?>"><?php echo $oContaBancaria->getCodColaborador() . " - " . $oContaBancaria->getBanco() . " - " . $oContaBancaria->getAgencia() . " - " .$oContaBancaria->getConta() . " - " .$f->format($oContaBancaria->getAtivo())?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="DataConfirmacao" class="control-label">Data Confirmação</label>
                                    <input class="form-control date" type='text' id='DataConfirmacao' placeholder='Data Confirmação' name='fDataConfirmacao' required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="ContaBancaria" class="control-label">Comprovante</label>
                                    <input class="form-control" type='file' id='Comprovante' name='fComprovante'>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                        <button type="submit" id="btnSalvar" class="btn btn-success" disabled>Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="carregaPainel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Gráfico Ano</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divGrafico"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php include_once('includes/javascript.php')?>

<?php include_once('includes/mensagem.php')?>

<script>
    function atualizaAno(nAno){
        var url_atual = window.location.href;
        var url = new URL(url_atual);
        var EFETIVADO = url.searchParams.get("nEfetivado");
        var MES = url.searchParams.get("nMesSelecionado");
        var urlNova = "?action=Relatorio.preparaFinanceiroMensal&sOP=Visualizar&nEfetivado="+EFETIVADO+"&nAno="+nAno+"&nMesSelecionado="+MES;
        window.location.href =urlNova;

    }

    function atualizaSituacao(nSituacao){
        var url_atual = window.location.href;
        var url = new URL(url_atual);
        var EFETIVADO = url.searchParams.get("nEfetivado");
        var MES = url.searchParams.get("nMesSelecionado");
        var ANO = url.searchParams.get("nAno");
        var urlNova = "?action=Relatorio.preparaFinanceiroMensal&sOP=Visualizar&nEfetivado="+nSituacao+"&nAno="+ANO+"&nMesSelecionado="+MES;
        window.location.href =urlNova;

    }

    $('#modalConfirmacaoPagamento').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let codigo = button.data('codigo-confirmacao');
        let formaPagamento = button.data('forma-pagamento');
        let dataPrevisao = button.data('previsao');
        let valor = button.data('valor');
        let descPagamento = button.data('descricao-pagamento');
        let modal = $(this);

        modal.find('#CodProjetoPagamento').val(codigo);
        modal.find('#CodFormaPagamento').val(formaPagamento);
        modal.find('#DataPrevisao').val(dataPrevisao);
        modal.find('#Valor').val(valor);
        modal.find('#DescPagamento').val(descPagamento);

    });


    nAnoAtual = '<?php echo date('Y');?>';
    sTipoPadrao = 'Previsto';
    function carregaGrafico(){
        let nAno = $("#AnoGrafico").val();
        let sTipo = $("#TipoGrafico").val();


        if(nAno == "" || typeof nAno == typeof undefined) {
            nAno = nAnoAtual;
        }

        if(sTipo == "" || typeof sTipo == typeof undefined) {
            sTipo = sTipoPadrao;
        }

        $.post("?action=Relatorio.preparaPainelFinanceiro&Ano=" + nAno + '&Tipo=' + sTipo)

            .done(function(data){
                console.log(data);
                $("#divGrafico").html(data);
            })

        $.post("?action=Relatorio.recuperarTotalQuadros&nAno=" + nAno + '&sTipo=' + sTipo)
            .done(function(data){
                data = JSON.parse(data);

                $("#despesa_mensal").text(data.media_despesa);
                $("#texto_despesa_mensal").text(data.texto_despesa_mensal);
                $("#bg_quadro1").css("background-color",data.cor1);

                $("#receita_mensal").text(data.media_receita);
                $("#texto_receita_mensal").text(data.texto_receita_mensal);
                $("#bg_quadro2").css("background-color",data.cor2);
            })

    }



    $(document).ready(function() {

        let table = $('#tableFinanceiro').DataTable({
            dom: 'Bfrtip',
            search: {
                "smart": true
            },
            columnDefs: [
                {type: 'date-euro', targets: [0]}
            ],
            buttons: [
                {
                    extend: 'pdfHtml5',
                    className: 'btn btn-primary',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    title: 'Pagamentos <?php echo "{$sStatus} - {$sMes}"?>'
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-primary',
                    title: 'Pagamentos <?php echo "{$sStatus} - {$sMes}"?>'
                },
                {
                    extend: 'csv',
                    className: 'btn btn-primary',
                    title: "pagamentos_<?php echo "{$sStatus}_{$sMes}"?>",
                    charset: 'utf-8',
                    bom: true
                },
                {
                    extend: 'print',
                    className: 'btn btn-primary',
                    text: 'Imprimir',
                },
                {
                    extend: 'colvis',
                    className: 'btn btn-primary',
                    text: 'Selecionar colunas'
                }
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json",
                "decimal": ",",
                "thousands": "."
            },
            "paging": false
        });
        table
            .order( [ 0, 'desc' ] )
            .draw();
    });
</script>
<!-- ./wrapper -->
</body>
</html>
