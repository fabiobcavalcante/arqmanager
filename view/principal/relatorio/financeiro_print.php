<?php
$voResultado = $_REQUEST['voResultado'];
$nMes = str_pad($_REQUEST['Mes'], 2, "0", STR_PAD_LEFT);
$nAno = $_REQUEST['Ano'];
$oColaborador = $_REQUEST['oColaborador'];
 $f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
 $p = new NumberFormatter( 'pt_br', NumberFormatter::PERCENT );
 //$p->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, 0);
 // $p->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, 2);
if($_REQUEST['sLoad'] == 'sim'){
  echo "<script> window.onload = function() { window.print(); } </script>";
}

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=500, initial-scale=1">
<title>Relatório - Impressão</title>
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load.
<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">-->
<link rel="stylesheet" href="dist/css/skins/skin-UNIT.css">
<style>
page[size="A4"] {
  width: 21cm;
  height: 29.7cm;
}
page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  height: 21cm;
}
@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
  }

  th,
  td {
    border: 1px solid #ddd;
    text-align: left;
  }
  .header {
    padding-top: 10px;
    margin: 0 15px;
    text-align: center;
    border-bottom: 2px solid #ddd;
  }
  .tabela{
    margin: 0 15px;

  }
  table {
    border-collapse: collapse;
    font-size: 80%;
  }
  table th {
    background-color: #4caf50;
    color: white;
    text-align: center;
  }
  th,
  tr:nth-child(odd) {background: #CCC},
  tr:nth-child(even) {background: #FFF},
  td {
    border: 1px solid #ddd;
    text-align: left;
  }
}
.header {
  padding-top: 10px;
  margin: 0 15px;
  text-align: center;
  border-bottom: 2px solid #ddd;
}
.tabela{
  margin: 0 15px;

}
table {
  border-collapse: collapse;
  font-size: 80%;
}
table th {
  background-color: #4caf50;
  color: white;
  text-align: center;
}
th,
tr:nth-child(odd) {background: #CCC},
tr:nth-child(even) {background: #FFF},
td {
  border: 1px solid #ddd;
  text-align: left;
}
</style>
</head>
<body>
  <div id="divPrint">
  <page size="A4">

    <div class="header"><img src="dist/img/logo2.png" width="101" height="114"  alt=""/>
        <br>
      <h5>PRODUÇÃO - <?php echo $nMes . "/".$nAno?><br><?php echo $oColaborador->getNome()?></h5>
    </div>
    <?php if($voResultado){?>
    <div class="tabela">
    <table class="table table-bordered table-striped table-condensed" width="100%">
      <thead>
        <tr>
          <th width="33%">CLIENTE/PROJETO</th>
          <th width="28%">SERVIÇO</th>
          <th width="19%">COMISSÕES</th>
          <th width="20%">PAGAR</th>
        </tr>
      </thead>
      <tbody>


      	<?php foreach($voResultado as $oResultado){
          	$voServico = $oFachada->recuperarTodosServicoDetalhePorServico($oResultado->servico_detalhe);
          ?>
        <tr>
          <td rowspan="<?php echo count($voServico)+1?>" style="background-color:#fff">
            <?php echo $oResultado->nome?>
            <br>[<?php echo strtoupper($oResultado->servico)?>]
            <br><span class="badge badge-success"><?php echo $f->format($oResultado->valor_recebido)?></span>
          </td>
        </tr>
        <?php $nCor=0;
              foreach($voServico as $oServico){
                $nTotalServico=0;
              $oProjetoColaborador = $oFachada->recuperarUmProjetoColaboradorPorColaboradorProjetoServicoDetalhe($oColaborador->getCodColaborador(),$oResultado->cod_projeto,$oServico->getCodServicoDetalhe());
              if($nCor==0){
                $nCor=1;
                $sCor="#FFF";
              }else{
                $nCor=0;
                $sCor="#ccc";
              }
              if($oProjetoColaborador){
                $nValorPagar =  ($oProjetoColaborador->getPercentual() * $oResultado->pagar)/100;
                $nTotalPagar = $nTotalPagar + $nValorPagar;
              }else{
                $nValorPagar = 0;
              }
  ?>
        <tr>
          <td><?php echo $oServico->getDescServicoDetalhe()?></td>
          <td><div align="center"><?php echo ($oProjetoColaborador) ? $p->format($oProjetoColaborador->getPercentual()/100) : "-"?></div></td>
          <td><?php echo $f->format($nValorPagar);
                  $nTotalServico += $nValorPagar;
               ?></td>
        </tr>
          <?php } ?>
       <?php } ?>

      </tbody>
      <tfoot>
        <tr>
          <th width="33%"></th>
          <th width="28%"></th>
          <th width="19%"></th>
          <th width="20%"><?php echo $f->format($nTotalPagar);?></th>
        </tr>
      </tfoot>
    </table>
  </div>
<?php } else{ ?>
  <div class="row">
    <div class="col-xs-12">
         <div class="box">
           <div class="box-header">
               <h5 class="box-title btn btn-block btn-danger btn-sm-1">Não há lançamentos para o COLABORADOR no período <?php echo $nMes . "/" . $nAno?></h5>
           </div>
        <!-- /.box-header -->
                <div class="box-body">
                 </div>
    <!-- /.box-body -->

      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <?php }?>

  </page>
  </div>
</body>
</html>
