<?php
$oPrincipalMes = $_REQUEST['oPrincipalMes'];
$voContaMovimentacao = $_REQUEST['voContaMovimentacao'];
$nSaldo = $oPrincipalMes->saldo_inicial + $oPrincipalMes->recebido - $oPrincipalMes->pago;
?>
<div class="row">

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-android-document"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">SALDO INICIAL</span>
                <span class="info-box-number">$<?php echo number_format($oPrincipalMes->saldo_inicial,2, ',', '.' )?></span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-podium"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">A RECEBER/RECEBIDO</span>
                <span class="info-box-number">$<?php echo number_format($oPrincipalMes->previsto_receber,2, ',', '.' )?></span>
                <span class="info-box-number">$<?php echo number_format($oPrincipalMes->recebido,2, ',', '.' )?></span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-android-document"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">PAGO</span>
                <span class="info-box-number">$<?php echo number_format($oPrincipalMes->pago,2, ',', '.' )?></span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-android-document"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">SALDO</span>
                <span class="info-box-number">$<?php echo number_format($nSaldo,2, ',', '.' ); ?></span>
            </div>
        </div>
    </div>

</div>

<?php if($voContaMovimentacao){ ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">EXTRATO</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="data" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Dia</th>
                            <th>Receita / Despesa</th>
                            <th>Descrição</th>
                            <th>Valor</th>
                            <th>Comprovante</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $nTotal = $oPrincipalMes->saldo_inicial;
                        foreach($voContaMovimentacao as $oContaMovimentacao){
                            if($oContaMovimentacao->tipo == 'E')
                                $mOperacao = "+";
                            else
                                $mOperacao = "-";

                            eval("\$nTotal = $nTotal $mOperacao $oContaMovimentacao->valor;");

                            ?>
                            <tr>
                                <td><?php echo str_pad($oContaMovimentacao->dia_efetivacao, 2, "0", STR_PAD_LEFT) ?></td>
                                <td><?php echo $oContaMovimentacao->descricao?></td>
                                <td><?php echo ($oContaMovimentacao->tipo == 'S') ? "<span class='btn btn-sm bg-red'><i class='fa fa-minus'></i></span>" : "<span class='btn btn-sm bg-green'><i class='fa fa-plus'></i></span>"?></td>
                                <td><?php echo ($oContaMovimentacao->tipo == 'S') ? "<span class='text-red'>R$".number_format($oContaMovimentacao->valor,2, ',', '.' )."</span>" : "<span class='text-green'>R$".number_format($oContaMovimentacao->valor,2, ',', '.' )."</span>"?></td>
                                <td><a target="_blank" href="/<?php echo $oContaMovimentacao->comprovante?>"><span class='btn btn-sm btn-primary'><i class='fa fa-file'></i></span></a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Saldo</th>
                            <th>R$<?php echo number_format($nTotal,2, ',', '.' )?></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
<?php }else{ ?>
    <div class="alert alert-info">
        Sem movimentação para a conta no período selecionado.
    </div>
<?php } ?>
