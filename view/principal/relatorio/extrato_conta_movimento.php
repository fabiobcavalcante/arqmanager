<?php
 $sOP = $_REQUEST['sOP'];

 $voContaBancaria = $_REQUEST['voContaBancaria'];
 $voContaMovimentacao = $_REQUEST['voContaMovimentacao'];
 $oContaSelecionada = $_REQUEST['oContaSelecionada'];
$f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lançamento - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
  </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?> - CONSULTAS / RELATÓRIOS</h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Relatórios -Extrato de Conta</li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h4 class="box-title btn btn-block btn-warning btn-sm-1">EXTRATO DE CONTA</h4>
                </div>
             <!-- /.box-header -->
                  <div class="box-body">
                  <form method="post" class="form-horizontal" name="formRelatorioExtratoContaMovimento" action="?action=Relatorio.preparaExtratoBancario">
                    <input type="hidden" name="sOP" value="Visualizar" />
                   	<div  class="box-body" id="form-group">

                        <div class="form-group col-md-3">
                    					<label for="Conta">Conta:</label>
                    					<div class="input-group col-md-11 col-sm-12">
                    					<select id="CodConta" name='fCodContaBancaria' class="form-control select2">
                    						<option value=''>Selecione</option>
                    						<?php $sSelected = "";
                    						   if($voContaBancaria){
                    							   foreach($voContaBancaria as $oContaBancaria){
                    								    if($oContaSelecionada){
                    									    $sSelected = ($oContaSelecionada->getCodContaBancaria() == $oContaBancaria->getCodContaBancaria()) ? "selected" : "";
                    								    }
                    						?>
                    								   <option  <?php echo $sSelected?> value='<?php echo $oContaBancaria->getCodContaBancaria()?>'><?php echo $oContaBancaria->getCodColaborador()?></option>
                    						<?php
                    							   }
                    						   }
                    						?>
                    					</select>
                    			</div>
                    				</div>
                              <div class="col-md-2">
                                <!-- Date and time range -->
                                  <div class="form-group">
                                    <label>Período:</label>
                                    <div class="input-group">
                                      <input type="text" class="btn btn-default pull-left" id="daterange-btn" name="fPeriodo" style="width:100%;" >
                                    </div>

                                  </div>
                                <!-- /.form group -->
                                </div>
                                <div class="col-md-1">
                                    <label class="control-label">&nbsp;</label><br>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-filter fa-lg"></i> Filtrar</button>
                                </div>
                   				</div>
                   				<!-- /.box-body -->
                   </form>

 				</div>
 				<!-- /.box-body -->

           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>

             <!-- /.box-header -->

    <?php if(is_array($voContaMovimentacao)){
          $nSaldo = 0;
          $nLinha=1;

      ?>
       <div class="box-body table-responsive">
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
					<th>Data</th>
          <th>Desrição</th>
	         <th>Tipo</th>
					<th>Valor</th>

        	</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voContaMovimentacao as $oContaMovimentacao){
                      if ($oContaMovimentacao->getTipo() == 'E'){
                      $nSaldo = $nSaldo + $oContaMovimentacao->getValorReceita();
                    }else {
                      $nSaldo = $nSaldo - $oContaMovimentacao->getValorDespesa();
                    }
              if ($nLinha ==1) {
               ?>

          <tr>
            <td colspan="2"><span style="font-weight:bold; float:right">SALDO INICIAL:</span></td>
            <td><?php echo ($oContaMovimentacao->getValorInicial() < 0) ? "<span class='btn btn-danger pull-center'><i class='fa fa-minus'></i></span>" : "<span class='btn btn-success pull-center'><i class='fa fa-plus'></i></span>"?></td>
           	<td><span style="font-weight:bold"><?php echo $f->format($oContaMovimentacao->getValorInicial())?></span></td>
          </tr>
        <?php } ?>
          <tr>
            <td><?php echo $oContaMovimentacao->getDataMovimentacaoFormatado()?></td>
  					<td><?php echo $oContaMovimentacao->getDescricao()?></td>
  					<td><?php echo ($oContaMovimentacao->getTipo() == 'S') ? "<span class='btn btn-danger pull-center'><i class='fa fa-minus'></i></span>" : "<span class='btn btn-success pull-center'><i class='fa fa-plus'></i></span>"?></td>
					  <td><?php echo $f->format( ($oContaMovimentacao->getValorReceita() == null) ? $oContaMovimentacao->getValorDespesa() : $oContaMovimentacao->getValorReceita() )?></td>
  				</tr>
  				<?php $nLinha++;
          }?>
  				</tbody>
  			    <tfoot>
                 <tr>

                   <th></th>
                   <th><span style="float:right">SALDO NO PERÍODO:</span></th>
         	         <th><?php echo ($nSaldo > 0) ? "<span class='btn btn-success pull-center'><i class='fa fa-plus'></i></span>" : "<span class='btn btn-danger pull-center'><i class='fa fa-minus'></i></span>" ?></th>
         					<th><?php echo $f->format($nSaldo)?></th>
                 </tr>
                 </tfoot>
               </table>
                  </div>
  			<?php }else{//if(count($voMovimento)){?>
          <div class="row">
            <div class="col-xs-12">
                 <div class="box">
                   <div class="box-header">
                       <h5 class="box-title btn btn-block btn-danger btn-sm-1">Não há lançamentos para a Conta/Período selecionado</h5>
                   </div>
                <!-- /.box-header -->
                        <div class="box-body">
    				             </div>
    				<!-- /.box-body -->

              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>


        <?php } ?>

           </div></div>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->


 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
 <script type="text/javascript" language="javascript">

 function filtrar(){

 }

jQuery(function($){

    $("#DataInicio").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
    $("#DataPrevisaoFim").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
    $("#DataFim").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
    $(".date").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
    });

     //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()
    //Date range as a button
     $('#daterange-btn').daterangepicker(
      {

    "locale": {
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "applyLabel": "Aplicar",
        "cancelLabel": "Cancelar",
        "fromLabel": "De",
        "toLabel": "Para",
        "customRangeLabel": "Custom",
        "weekLabel": "Sem",
        "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sab"],
        "monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
        "firstDay": 1,

    },


        ranges   : {
          'Hoje'       : [moment(), moment()],
          'Ontem'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Últimos 7 Dias' : [moment().subtract(6, 'days'), moment()],
          'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
          'Mês Atual'  : [moment().startOf('month'), moment().endOf('month')],
          'Último Mês'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()

      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'))
      }
    )

</script>

 </body>
 </html>
