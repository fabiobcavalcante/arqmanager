<?php
$sOP = $_REQUEST['sOP'];
$voContaBancaria = $_REQUEST['voContaBancaria'];
$voFormaPagamento = $_REQUEST['voFormaPagamento'];
$voContaMovimentacao = $_REQUEST['voContaMovimentacao'] ?? null;
$oContaSelecionada = $_REQUEST['oContaSelecionada'] ?? null;
$oPrincipal = $_REQUEST['oPrincipal'];
$voPlanoContasGrupo = $_REQUEST['voPlanoContas'];
$voReceita = $_REQUEST['voReceita'];
$voProjeto = $_REQUEST['voProjeto'];

$f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);
$voAno = $_REQUEST['voAno'];
$voMes = $_REQUEST['voMes'];
$oPrincipalMes = $_REQUEST['oPrincipalMes'] ?? null;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Financeiro</title>
    <?php include_once('includes/head.php')?>

    <!-- DataTables -->
    <!--suppress HtmlUnknownTarget -->
    <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <style>
        .small-box{
            overflow: hidden;
        }
        h4{
            font-size: 30px !important;
            font-weight: bold;
        }
    </style>

</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light sidebar-collapse">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?> -
                Financeiro
                <small>Extrato</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li class="active">Financeiro</li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Main row -->
            <div class="row">
                <!-- Ações -->
                <div class="col-md-4">
                    <!-- Application buttons -->
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">AÇÕES</h3>
                        </div>
                        <div class="box-body">
                            <a class="btn btn-app bg-green" title="Cadastrar Receita" data-acao="Cadastrar" data-toggle="modal" data-target="#modalReceita"><i class="fa fa-plus"></i> RECEITA</a>
                            <a class="btn btn-app bg-red" title="Cadastrar Despesa" data-toggle="modal" data-target="#modalDespesa"><i class="fa fa-minus"></i> DESPESA</a>
                            <button type="button" class="btn btn-app bg-blue" title="Painel Financeiro" onclick="carregaGrafico()"><i class="fa fa-line-chart"></i> Painel</button>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>

                <!-- Total Receber -->
                <div class="col-md-2">
                    <!-- Application buttons -->
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">TOTAL A RECEBER</h3>
                        </div>
                        <div class="box-body">
                            <h1 class="btn-lg btn-app bg-green"><i class="fa fa-plus"></i> <?php echo $oPrincipal->total_previsao?></h1>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>

                <!-- Formulário Filtro -->
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">FILTRO</h3>
                        </div>

                        <div class="box-body">
                            <form method="post" class="form-inline" action="?action=Relatorio.preparaExtratoConta" id="formExtrato">

                                <div class="row">

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label" for="nAno">Ano:</label>
                                            <select class="form-control" name="nAno" id="nAno" required>
                                                <option value="">Selecione</option>
                                                <?php foreach($voAno as $oAno) { ?>
                                                    <option value="<?php echo $oAno->ano; ?>" <?php echo (isset($_REQUEST['nAno']) && $_REQUEST['nAno'] == $oAno->ano)  ? "selected" : ""; ?>><?php echo $oAno->ano; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-2">
                                        <label class="control-label" for="nMes">Mês</label>
                                        <select class="form-control" name="nMes" id="nMes" required>
                                            <option value="">Selecione</option>
                                            <?php foreach($voMes as $oMes) { ?>
                                                <option <?php echo (isset($_REQUEST['nMes']) && $_REQUEST['nMes'] == $oMes->mes)  ? "selected" : ""; ?> value="<?php echo $oMes->mes; ?>"><?php echo $oMes->mes_descricao; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label" for="nContaBancaria">Conta</label>
                                        <select class="form-control" name="nContaBancaria" id="nContaBancaria" required>
                                            <option value="">Selecione</option>
                                            <?php foreach($voContaBancaria as $oContaBancaria) {
                                                $sSelected = (($oContaSelecionada) && ($oContaSelecionada->getCodContaBancaria() == $oContaBancaria->getCodContaBancaria()))  ? "selected" : "";
                                                ?>
                                                <option <?php echo $sSelected; ?> value="<?php echo $oContaBancaria->getCodContaBancaria() ?>"><?php echo "{$oContaBancaria->getBanco()} - {$oContaBancaria->getAgencia()} - {$oContaBancaria->getConta()}"?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary">FILTRAR</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <!-- Extrato conta chamada via Ajax -->
            <div id="divExtrato"></div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal RECEITA-->
    <div class='modal fade' id='modalReceita' tabindex="-1" role='dialog'>
        <div class='modal-lg modal-dialog'>
            <div class='modal-content'>
                <div class="modal-header" style="color:#FFF; background-color: #696969;; border-top-left-radius: 2px; border-top-right-radius: 2px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cadastrar Receita</h4>
                </div>
                <div class="modal-body">
                    <a class="btn btn-app bg-gray" title="Novo Projeto" target="_blank" href="?action=Projeto.preparaFormulario&sOP=Cadastrar"><i class="fa fa-star" aria-hidden="true"></i> Novo Projeto</a>
                    <a class="btn btn-app bg-gray" title="Parcela/Confirmação" onclick="lista('divBodyReceita','?action=Projeto.preparaListaAjax2&sOP=Cadastrar')"><i class="fa fa-check"></i>Confirmação/Projeto</a>
                    <a class="btn btn-app bg-gray" title="Novo Recurso" data-toggle="modal" data-tipo="RECEITA" data-acao="Cadastrar" data-target="#modalMovimento"><i class="fa fa-star" aria-hidden="true"></i>Novo Recurso</a>
                    <a class="btn btn-app bg-gray" title="Confirmação" onclick="lista('divBodyReceita','?action=Movimento.preparaListaAjax2&sOP=Visualizar&sTipo=RECEITA')"><i class="fa fa-check"></i>Confirmação</a>
                    <div id="idRespostaReceita"></div>
                    <div id="divBodyReceita"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal DESPESA-->
    <div class='modal fade' id='modalDespesa' tabindex="-1" role='dialog'>
        <div class='modal-lg modal-dialog'>
            <div class='modal-content'>

                <div class="modal-header" style="color:#FFF; background-color: #696969;; border-top-left-radius: 2px; border-top-right-radius: 2px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cadastrar Despesa</h4>
                </div>
                <div class="modal-body">
                    <a class="btn btn-app bg-gray" title="Cadastrar Despesa" data-toggle="modal" data-tipo="DESPESA" data-acao="Cadastrar" data-target="#modalMovimento"><i class="fa fa-plus" aria-hidden="true"></i> Cadastrar Despesa</a>
                    <a class="btn btn-app bg-gray" title="Confirmação" onclick="lista('divBodyDespesa','?action=Movimento.preparaListaAjax2&sOP=Visualizar&sTipo=DESPESA')"><i class="fa fa-check"></i>Confirmação</a>
                    <div id="idRespostaDespesa"></div>
                    <div id="divBodyDespesa"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Movimento-->
    <div class="modal fade" id="modalMovimento" tabindex="-1">
        <div class="modal-lg modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="bodyMovimento"></div>
        </div>
    </div>

    <!-- Modal CONFIRMAPAGAMENTO-->
    <div class="modal fade" id="modalConfirmacaoPagamento" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="bodyPagamento"></div>
        </div>
    </div>

    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper, JSUnresolvedVariable -->
<?php include_once('includes/javascript.php')?>

<!--suppress HtmlUnknownTarget, JSUnresolvedVariable -->

<script>
    nAnoAtual = '<?php echo date('Y');?>';
    function carregaGrafico(sAcao){
        let nAno = $("#AnoGrafico").val();
        let sTipo = $("#TipoGrafico").val();

        if(nAno == "" || typeof nAno == typeof undefined) {
            nAno = nAnoAtual;
        }

        if(sTipo == "" || typeof sTipo == typeof undefined) {
            sTipo = 'Entrada';
        }
        $.post("?action=Relatorio.preparaPainelFinanceiro&Ano=" + nAno + '&Tipo=' + sTipo)

        .done(function(data){
            // console.log(data);
            $("#divExtrato").html(data);
        })

        $.post("?action=Relatorio.recuperarTotalQuadros&nAno=" + nAno + '&sTipo=' + sTipo)
        .done(function(data){
            data = JSON.parse(data);

            $("#despesa_mensal").text(data.media_despesa);
            $("#texto_despesa_mensal").text(data.texto_despesa_mensal);
            $("#bg_quadro1").css("background-color",data.cor1);

            $("#receita_mensal").text(data.media_receita);
            $("#texto_receita_mensal").text(data.texto_receita_mensal);
            $("#bg_quadro2").css("background-color",data.cor2);
        })

    }

    function efetivacao(Campo){
        if (Campo) {
            $('#CodConta').attr('disabled',false).attr('required', true);
            $('#Comprovante').attr('disabled',false).attr('required', true);
        } else{
            $('#CodConta').attr('disabled',true).attr('required', false);
            $('#Comprovante').attr('disabled',true).attr('required', false);
        }
    }

    function lista(div,action){
        sDiv = '#'+div;
        $(sDiv).load(action);
    }

    $('#modalIndicacao',0,0).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget,0,0);
        let nCodCliente = button.data('cliente');
        $("#CodCliente").val(nCodCliente);
    });

    $('#modalConfirmacaoPagamento').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let codigo = button.data('codigo');
        let sLink = "?action=ProjetoPagamento.preparaFormulario&sOP=ConfirmarPagamento&nCodProjetoPagamento=" + codigo;
        let modal = $(this);
        modal.find("#bodyPagamento").load(sLink);
    });
    $('#modalMovimento').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let codigo = button.data('codigo');
        let acao = button.data('acao');
        let tipo = button.data('tipo');
        let sLink = "?action=Movimento.preparaFormulario&sOP="+acao+"&sTipo="+tipo+"&nCodMovimento=" + codigo;
        let modal = $(this);
        modal.find("#bodyMovimento").load(sLink);
    });

    $('#confirm-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var sLink = button.data('link');
        var modal = $(this);
        modal.find('#ok').attr('href', sLink);
    });

    //Filtro extrato
    jQuery(document).submit(function(e){
        var form = jQuery(e.target);
        if(form.is("#formExtrato")){ // check if this is the form that you want (delete this check to apply this to all forms)
            e.preventDefault();
            $.ajax({
                type: "POST",
                datatype: "json",
                url: form.attr('action'),
                data: form.serialize(), // serializes the form's elements.
                beforeSend: function(){
                    $('#divExtrato').html('<img src="/res/admin/img/load.gif" alt="">');
                },
                error: function(oXMLRequest,sErrorType){
                    console.log(form.attr('action'));
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status+' , '+sErrorType);
                },
                success: function(dados){
                    $('#divExtrato').html(dados);
                }
            });
        }
    });

    //Filtro extrato
    jQuery(document).submit(function(e){
        var form = jQuery(e.target);
        if(form.is("#formExtrato")){ // check if this is the form that you want (delete this check to apply this to all forms)
            e.preventDefault();
            $.ajax({
                type: "POST",
                datatype: "json",
                url: form.attr('action'),
                data: form.serialize(), // serializes the form's elements.
                beforeSend: function(){
                    $('#divExtrato').html('<img src="/res/admin/img/load.gif" alt="">');
                },
                error: function(oXMLRequest,sErrorType){
                    console.log(form.attr('action'));
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status+' , '+sErrorType);
                },
                success: function(dados){
                    $('#divExtrato').html(dados);
                }
            });
        }
    });

</script>
</body>
</html>
