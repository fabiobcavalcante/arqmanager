<?php
$sOP = $_REQUEST['sOP'];
$voProjetoArquivamento = $_REQUEST['voProjetoArquivamento'];

?>
 <table id="tabelaArquivados" class="table table-bordered">
<thead>
    <tr>
        <th style="width:25%">CLIENTE / PROJETO</th>
        <th style="width:10%">DATA</th>
        <th style="width:15%">COLABORADOR</th>
        <th style="width:50%">MOTIVO</th>
    </tr>
    </thead>
    <tbody>
    <?php if($voProjetoArquivamento){ ?>
        <?php foreach($voProjetoArquivamento as $oProjetoArquivamento){ ?>
            <tr>
                <td><button data-toggle="tooltip" data-placement="right" title="Desarquivar" onclick="abreModal('Desarquivar',<?php echo $oProjetoArquivamento->cod_projeto; ?>,7)"><i style="font-size:18px;" class="fa fa-archive" aria-hidden="true"></i> </button> <?php echo $oProjetoArquivamento->identificacao_projeto?></td>
                <td><?php echo $oProjetoArquivamento->data_acao_formatada?></td>
                <td><?php echo $oProjetoArquivamento->cod_colaborador?></td>
                <td><?php echo $oProjetoArquivamento->motivo_acao?></td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
<script>
    $(document).ready(function() {

        let table = $('#tabelaArquivados').DataTable({
            dom: 'Bfrtip',
            search: {
                "smart": true
            },
            columnDefs: [
                {type: 'date-euro', targets: [3]}
            ],
            buttons: [
                {
                    extend: 'pdfHtml5',
                    className: 'btn btn-primary',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    title: 'Entregas'
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-primary',
                    title: 'Entregas'
                },
                {
                    extend: 'csv',
                    className: 'btn btn-primary',
                    title: "lista_entregas",
                    charset: 'utf-8',
                    bom: true
                },
                {
                    extend: 'print',
                    className: 'btn btn-primary',
                    text: 'Imprimir',
                },
                {
                    extend: 'colvis',
                    className: 'btn btn-primary',
                    text: 'Selecionar colunas'
                }
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json",
                "decimal": ",",
                "thousands": "."
            },
            "paging": false
        });
        table
            .order( [ 3, 'asc' ] )
            .draw();
    });