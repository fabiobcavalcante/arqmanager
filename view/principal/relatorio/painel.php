<?php
$sFinanceiro = $_REQUEST['sFinanceiro'];
$vCor = $_REQUEST['vCor'];
$vAno = $_REQUEST['vAno'];
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label class="control-label" for="AnoGrafico">Ano:</label>
                        <select class="form-control" name="AnoGrafico" id="AnoGrafico" required>
                            <option value="">Selecione</option>
                            <?php foreach($vAno as $nAno) { ?>
                                <option value="<?php echo $nAno->ano; ?>" <?php echo ($_REQUEST['Ano']==$nAno->ano) ? 'selected' : '';?>><?php echo $nAno->ano; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label class="control-label" for="TipoGrafico">Tipo:</label>
                        <select class="form-control" name="TipoGrafico" id="TipoGrafico" required>
                            <option value="">Selecione</option>
                            <option value="Entrada" <?php echo ($_REQUEST['Tipo']=='Entrada') ? 'selected' : '';?>>Entrada/Saída</option>
                            <option value="Previsto" <?php echo ($_REQUEST['Tipo']=='Previsto') ? 'selected' : '';?>>Previsto/Realizado</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <br>
                        <button type="submit" class="btn btn-primary" onclick="carregaGrafico('Atualizar')">FILTRAR</button>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-8" id="barchart_material" style="width: 900px; height: 500px;"></div>
                    <div class="col-md-4 col-xs-12">
                        <div class="row">
                            <!-- small box -->
                            <div class="col-md-5 small-box" id="bg_quadro1">
                                <div class="inner">
                                    <h4 id="receita_mensal"></h4>
                                    <p id="texto_receita_mensal"></p>
                                </div>
                                <div class="icon"> <i class="fa fa-line-chart" aria-hidden="true"></i> </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-5 small-box"  id="bg_quadro2">
                                <div class="inner">
                                    <h4 id="despesa_mensal"></h4>
                                    <p id="texto_despesa_mensal"></p>
                                </div>
                                <div class="icon"> <i class="fa fa-bar-chart" aria-hidden="true"></i> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>

<script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        let data = google.visualization.arrayToDataTable([
            <?php echo $sFinanceiro; ?>
        ]);
        console.log(data);
        let options = {
            chart: {
                title: <?php echo $_REQUEST['sTitulo']; ?>,
                subtitle: '',
            },
            colors: <?php echo $vCor; ?>,
            bars: 'vertical', // Required for Material Bar Charts.
        };

        let chart = new google.charts.Bar(document.getElementById('barchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
</script>