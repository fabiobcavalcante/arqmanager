
<?php

$oTotais = $_REQUEST['oTotais'];
$voDetalhe = $_REQUEST['voDetalhe'];
$oColaborador =  $_REQUEST['oColaborador'];

$f = new NumberFormatter("pt_br", NumberFormatter::CURRENCY);

?>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"><?php echo $oTotais->nome?></h3>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-sm-3">PROJETOS: <?php echo "XX"?></div>
        <div class="col-sm-3">VALOR BASE: <?php echo $f->format($oTotais->valor_base)?></div>
        <div class="col-sm-3">TOTAL COMISSÃO: <?php echo $f->format($oTotais->comissoes)?></div>
        <div class="col-sm-3">TOTAL GERAL: <?php echo $f->format($oTotais->total_receber) ?></div>
      </div>
      <?php if($voDetalhe){?>

        <table>
          <thead>
              <tr>
                <th>CLIENTE</th>
                <th>SERVIÇO</th>
                <th>RECEBIDO</th>
                <th>PERCENTUAL</th>
                <th>COMISSÃO</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($voDetalhe as $oProjeto){
                  $vServico = explode("||",$oProjeto->getCodServicoDetalhe());
                ?>

            <tr onclick="mostraDetalhe($vServico[2],$vServico[0],$oProjeto->getCodColaborador());">
              <td><?php echo $vServico[2];?></td>
              <td><?php echo $vServico[1];?></td>
              <td><?php echo $f->format($oProjeto->getCodProjeto())?></td>
              <td><?php echo $oProjeto->getPercentual()?></td>
              <td><?php echo $f->format($oProjeto->getCodProjetoColaborador())?></td>
             </tr>
            <?php }?>
            </tbody>
            </table>

        <?php }//if($voDetalhe){?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">FECHAR</button>

      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
