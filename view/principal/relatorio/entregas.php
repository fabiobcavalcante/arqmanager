<?php
$sOP = $_REQUEST['sOP'];
$voServicoEtapa = $_REQUEST['voServicoEtapa'];
$voEtapa = $_REQUEST['voEtapa'];
$voProjeto = $_REQUEST['voProjeto'];
$voColaborador = $_REQUEST['voColaborador'];

$vEntrega = $_REQUEST['vEntrega'];

switch ($sOP) {
    case 'Entrega':
        if($_REQUEST['sEntregas'] == 'Arquivados'){
            $sTitulo = "Projetos Arquivados";
            $sConclusao = "Ações";
            $voProjetoArquivados = $_REQUEST['voProjetoArquivados'];
        }else{
            $sTitulo = "Próximas Entregas";
            $sConclusao = "Ações";
        }

        break;

    case 'Concluido':
        $sTitulo = "Entregas realizadas";
        $sConclusao = "Conclusão";

        break;
}

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Entregas </title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?> - Entregas</h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
                <li class="active">Etapas</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">


            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title btn btn-block btn-warning btn-sm-1"><?php echo $sTitulo?></h4>
                        </div>
                        <!-- /.box-header -->

                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">Ativos</a></li>
                                <li><a onclick="recuperaConteudoDinamico('','action=Relatorio.preparaLista&sOP=Arquivados','divArquivados')" href="#tab_2"  data-toggle="tab">Arquivados</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="box-body table-responsive">

                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th style="width:25%">CLIENTE / PROJETO</th>
                                                <?php if($voEtapa){ ?>
                                                    <?php foreach($voEtapa as $oEtapa){ ?>
                                                        <th style="width:25%"><?php echo $oEtapa->getDescricao() ?></th>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if($vEntrega){ ?>
                                                <?php foreach($vEntrega as $nCodProjeto=>$Entrega){ ?>
                                                    <tr>
                                                        <td><button data-toggle="tooltip" data-placement="right" title="Arquivar" onclick="abreModal('Arquivar',<?php echo $nCodProjeto; ?>,6)"><i style="font-size:18px;" class="fa fa-archive" aria-hidden="true"></i> </button> <?php echo $Entrega['Identificacao'] ?></td>
                                                        <?php if ($Entrega['Calendario'] == 1){ ?>
                                                            <td colspan="3" style="text-align: center"><a href="?action=Projeto.preparaFormulario&sOP=Calendario&nCodProjeto=<?php echo $nCodProjeto ?>" class="btn btn-xs btn-primary"><i class="fa fa-calendar"></i> Definir calendário</a></td>

                                                        <?php }else{ ?>
                                                            <?php foreach($Entrega['Etapas'] as $nCodEtapa=>$vEtapa){ ?>

                                                                <td style="background-color:<?php echo $vEtapa['Cor'];?>">
                                                                    <?php echo ($vEtapa['DataConclusao']) ? maskInvData($vEtapa['DataConclusao']) : (($vEtapa['DataPrevista']) ?: "<small class='badge badge-secondary'>N/C</small>"); ?>

                                                                    <?php if (!$vEtapa['DataConclusao'] && $vEtapa['Acao']==1){ ?>
                                                                        <button type="button" class="btn btn-xs btn-info" onclick="abreModal('Detalhar',<?php echo $nCodProjeto; ?>,<?php echo $vEtapa['ServicoEtapa']; ?>)"><i style="font-size:18px;" class="fa fa-plus-circle" aria-hidden="true"></i> Detalhar</button>
                                                                        <button type="button" class="btn btn-xs btn-primary" onclick="abreModal('Envolvidos',<?php echo $nCodProjeto; ?>,<?php echo $vEtapa['ServicoEtapa']; ?>)"><i class="fa fa-users"></i>  Envolvidos</button>
                                                                    <?php } ?>

                                                                </td>
                                                            <?php } ?>
                                                        <?php } ?>

                                                    </tr>

                                                <?php } ?>
                                            <?php } ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!--                                 /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    <div id="divArquivados"></div>
                                </div>
                                <!-- /.tab-pane -->
                                <!-- /.tab-content -->
                                <!-- nav-tabs-custom -->
                            </div>
                            <!-- /.col -->

                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Detalhar -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document" id="tamanhoModal">
            <div class="modal-content" id="bodyModal"></div>
        </div>
    </div>

    <!-- Modal Concluir -->
    <div class="modal fade" id="modalConcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="concluir"></div>
        </div>
    </div>

    <!-- Modal Justificar -->
    <div class="modal fade" id="modalJustificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="bodyJustificar"></div>
        </div>
    </div>

    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>

<script>

    function abreModal(sFuncao,nCodProjeto,nCodServicoEtapa,nCodMicroServico){
        let sLink = "";
        switch (sFuncao){
            case "Detalhar":
                sLink = "action=PropostaMicroServico.preparaLista&sOP=Etapa&nCodProjeto=" + nCodProjeto + "&nCodServicoEtapa=" + nCodServicoEtapa;
                recuperaConteudoDinamico("", sLink, "bodyModal")
                $('#tamanhoModal').addClass('modal-lg');
                break;
            case "Envolvidos":
                sLink = "action=ProjetoServicoEtapaColaborador.preparaFormulario&sOP=Alterar&CodProjeto=" + nCodProjeto + "&CodEtapa=" + nCodServicoEtapa;
                recuperaConteudoDinamico("",sLink,'bodyModal');
                $('#tamanhoModal').addClass('modal-lg');
                break;
            case "Arquivar":
                sLink = "action=Projeto.preparaFormulario&sOP=AlterarStatus&nCodProjeto=" + nCodProjeto + "&CodStatus=" + nCodServicoEtapa;
                recuperaConteudoDinamico("",sLink,'bodyModal');
                $('#tamanhoModal').removeClass('modal-lg');
                break;
            case "Desarquivar":
                sLink = "action=Projeto.preparaFormulario&sOP=AlterarStatus&nCodProjeto=" + nCodProjeto + "&CodStatus=" + nCodServicoEtapa;
                recuperaConteudoDinamico("",sLink,'bodyModal');
                $('#tamanhoModal').removeClass('modal-lg');
                break;

        }
        $('#modal').modal('show');

    }


    //Função para vincular colaboradores
    jQuery(document).submit(function(e){
        let form = jQuery(e.target);
        if(form.is("#formColaborador")){ // check if this is the form that you want (delete this check to apply this to all forms)
            e.preventDefault();
            $.ajax({
                type: "POST",
                datatype: "json",
                url: form.attr('action'),
                data: form.serialize(), // serializes the form's elements.
                error: function(oXMLRequest,sErrorType){
                    console.log(form.attr('action'));
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status+' , '+sErrorType);
                },
                success: function(dados){
                    dados = jQuery.parseJSON(dados);
                    $('#envBody').html("<div class='alert alert-" + dados.sClass + "'><p>" + dados.sMsg + "</p></div>")
                }
            });
        }
    });

    $('#modalConcluir',0,0).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget,0,0);
        let nCodProposta = button.data('proposta');
        let nCodMicroServico = button.data('microservico');
        let sLink = "?action=PropostaMicroServico.preparaFormulario&sOP=Concluir&nCodProposta=" + nCodProposta + "&nCodMicroServico=" + nCodMicroServico;

        $("#concluir").html('<img src="dist/img/load.gif" style="width: 150px" alt="">').load(sLink);
    });

    //Função para concluir etapas
    jQuery(document).submit(function(e){
        let form = jQuery(e.target);
        if(form.is("#formConcluir")){ // check if this is the form that you want (delete this check to apply this to all forms)
            e.preventDefault();
            $.ajax({
                type: "POST",
                datatype: "json",
                url: form.attr('action'),
                data: form.serialize(), // serializes the form's elements.
                beforeSend: function(){
                    $("#modalBody").html('<img src="dist/img/load.gif" style="width: 150px" alt="">');
                },
                error: function(oXMLRequest,sErrorType){
                    console.log(sUrl);
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status + ', ' + sErrorType);
                },
                success: function(dados){
                    dados = jQuery.parseJSON(dados);
                    $('#modalBody').html("<div class='alert alert-" + dados.sClass + "'><p>" + dados.sMsg + "</p></div>");

                    setTimeout(function () { $('#modalConcluir').modal('hide'); }, 2000);
                    let sLink = "action=PropostaMicroServico.preparaLista&sOP=Etapa&nCodProjeto=" + dados.nCodProjeto + "&nCodServicoEtapa=" + dados.nCodEtapa;
                    recuperaConteudoDinamico("",sLink,'bodyModal');
                }
            });
        }
    });

    $('#modalJustificar',0,0).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget,0,0);
        let nCodProposta = button.data('proposta');
        let nCodMicroServico = button.data("microservico");

        let sLink = "action=PropostaMicroServico.preparaFormulario&sOP=Justificar&nCodProposta=" + nCodProposta + "&nCodMicroServico=" + nCodMicroServico;
        recuperaConteudoDinamico("",sLink,'bodyJustificar');
    });

    //Função para concluir etapas
    jQuery(document).submit(function(e){
        let form = jQuery(e.target);
        if(form.is("#formJustificar")){ // check if this is the form that you want (delete this check to apply this to all forms)
            e.preventDefault();
            $.ajax({
                type: "POST",
                datatype: "json",
                url: form.attr('action'),
                data: form.serialize(), // serializes the form's elements.
                beforeSend: function(){
                    Swal.fire({
                        title: 'Enviando...',
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                error: function(oXMLRequest,sErrorType){
                    console.log(sUrl);
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status + ', ' + sErrorType);
                },
                success: function(dados){
                    dados = jQuery.parseJSON(dados);
                    if (dados.sClass === 'success')
                        $('#modalJustificar').modal('hide');
                    Swal.fire({
                        icon: dados.sClass,
                        title: dados.sMsg,
                        timer: 2000
                    });
                }
            });
        }
    });


    //Função para arquivar
    jQuery(document).submit(function(e){
        let form = jQuery(e.target);
        if(form.is("#formArquivamento")){ // check if this is the form that you want (delete this check to apply this to all forms)
            e.preventDefault();
            $.ajax({
                type: "POST",
                datatype: "json",
                url: form.attr('action'),
                data: form.serialize(), // serializes the form's elements.
                beforeSend: function(){
                    Swal.fire({
                        title: 'Enviando...',
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                error: function(oXMLRequest,sErrorType){
                    console.log(sUrl);
                    console.log(oXMLRequest.responseText);
                    console.log(oXMLRequest.status + ', ' + sErrorType);
                },
                success: function(dados){
                    dados = jQuery.parseJSON(dados);
                    if (dados.sClass === 'success')
                        $('#modal').modal('hide');
                    Swal.fire({
                        icon: dados.sClass,
                        title: dados.sMsg,
                        timer: 2000
                    });
                    document.location.reload(true);
                }
            });
        }
    });

    function FormataStringData(data) {
        let dia  = data.split("/")[0];
        let mes  = data.split("/")[1];
        let ano  = data.split("/")[2];
        let dData = ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);

        let dDataConclusao = new Date(dData);
        let dDataPrevista = $("#DataPrevista").val();

        dia  = dDataPrevista.split("/")[0];
        mes  = dDataPrevista.split("/")[1];
        ano  = dDataPrevista.split("/")[2];
        dData = ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);

        dDataPrevista = new Date(dData);

        if (dDataPrevista < dDataConclusao)
            $("#Observacao").attr("required",true);
        else
            $("#Observacao").attr("required",false);
    }


    $(document).ready(function() {

        let table = $('.table').DataTable({
            dom: 'Bfrtip',
            search: {
                "smart": true
            },
            columnDefs: [
                {type: 'date-euro', targets: [3]}
            ],
            buttons: [
                {
                    extend: 'pdfHtml5',
                    className: 'btn btn-primary',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    title: 'Entregas'
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-primary',
                    title: 'Entregas'
                },
                {
                    extend: 'csv',
                    className: 'btn btn-primary',
                    title: "lista_entregas",
                    charset: 'utf-8',
                    bom: true
                },
                {
                    extend: 'print',
                    className: 'btn btn-primary',
                    text: 'Imprimir',
                },
                {
                    extend: 'colvis',
                    className: 'btn btn-primary',
                    text: 'Selecionar colunas'
                }
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json",
                "decimal": ",",
                "thousands": "."
            },
            "paging": false
        });
        table
            .order( [ 3, 'asc' ] )
            .draw();
    });
</script>

</body>
</html>
