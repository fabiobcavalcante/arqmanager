<?php
$sOP = $_REQUEST['sOP'];
$voProjeto = $_REQUEST['voProjeto'];

?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Entregas Realizadas</title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?> - Entregas</h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
                <li class="active">Etapas</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title btn btn-block btn-success btn-sm-1">Projetos Concluídos</h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
													<?php if(is_array($voProjeto)){?>
															<table id="lista" class="table table-bordered table-striped">
																	<thead>
																	<tr>
																			<th width="1%">Id</th>
																			<th width="18%">Cliente</th>
																			<th width="10%">Serviço</th>
																			<th width="35%">Descrição</th>
																			<th width="10%">Início</th>
																			<th width="10%">Data previsão</th>
																			<th width="15%">Ações</th>
																	</tr>
																	</thead>
																	<tbody>
																	<?php foreach($voProjeto as $oProjeto){ ?>
																			<tr>

																					<td><?php echo $oProjeto->cod_projeto?></td>
																					<td><?php echo $oProjeto->nome?></td>
																					<td><?php echo $oProjeto->desc_servico?></td>
																					<td><?php echo $oProjeto->identificacao?></td>
																					<td>
																						<?php
																						$oData = new DateTime($oProjeto->data_inicio);
																						echo $oData->format("d/m/Y");
																						?></td>
																					<td><?php 	$oData = new DateTime($oProjeto->data_previsao_fim);
																						echo $oData->format("d/m/Y");?></td>
																					<td style="white-space: nowrap;vertical-align: middle">
																							<a href="?action=Projeto.preparaFormulario&sOP=Detalhar&nCodProjeto=<?php echo $oProjeto->cod_projeto?>" class="btn btn-primary btn-xs"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Detalhar"></i></a>
																							<a target="_blank" href="?action=Projeto.preparaFormulario&sOP=Calendario&nCodProjeto=<?php echo $oProjeto->cod_projeto?>" class="btn btn-success btn-xs"><i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Calendario"></i></a>
																							<a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=4&nCodProjeto=<?php echo $oProjeto->cod_projeto?>" class="btn btn-primary btn-xs"><strong>RRT</strong></a>
																							<a target="_blank" href="?action=Documento.preparaDocumento&nCodDocumento=5&sOP=TermoEntrega&nCodProjeto=<?php echo $oProjeto->cod_projeto?>" class="btn btn-warning btn-xs"><strong>TE</strong></a>
																					</td>
																			</tr>
																	<?php } ?>
																	</tbody>
																	<tfoot>
																	<tr>

																			<th>Id</th>
																			<th>Cliente</th>
																			<th>Serviço</th>
																			<th>Descrição</th>
																			<th>Início</th>
																			<th>Data previsão</th>
																	</tr>
																	</tfoot>
															</table>
													<?php } ?>
                            <div class='row'></div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include_once('includes/footer.php')?>
</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>


</body>
</html>
