<?php
$voProjetoDocumento = $_REQUEST['voProjetoDocumento'];
$oProjeto = $_REQUEST['oProjeto'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Projeto - Documentos</title>
    <?php include_once('includes/head.php')?>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia();?> </h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li><a href="?action=Projeto.preparaLista">Gerenciar Projetos</a>
                <li class="active">Documentos</li>
            </ol>
            <?php include_once('includes/mensagem.php')?>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Documentos</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal"><i class="fa fa-plus"></i> Novo Documento</button>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label for="CodCliente" class="control-label">Cliente:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getCliente()->getNome() : ""?></p>
                        </div>
                        <div class="col-md-2">
                            <label for="Identificação" class="control-label">Identificação:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getCliente()->getIdentificacao() : ""?></p>
                        </div>
                        <div class="col-md-6">
                            <label for="Identificação" class="control-label">Endereço:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getCliente()->getLogradouro() .", nº". $oProjeto->getCliente()->getNumero() . " ".  $oProjeto->getCliente()->getComplemento(). ". Bairro:" . $oProjeto->getCliente()->getBairro() . " na cidade de ". $oProjeto->getCliente()->getCidade() . "/" . $oProjeto->getCliente()->getUf()  : ""?></p>
                        </div>
                        <div class="col-md-4">
                            <label for="CodServico" class="control-label">Serviço:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getServico()->getDescServico() : ""?></p>
                        </div>
                        <div class="col-md-4">
                            <label for="Descricao" class="control-label">Descrição:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getDescricao() : ""?></p>
                        </div>

                        <div class="col-md-2">
                            <label for="DataInicio" class="control-label">Início:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getDataInicioFormatado() : ""?></p>
                        </div>
                        <div class="col-md-2">
                            <label for="DataPrevisaoFim" class="control-label">Data Previsão:</label>
                            <p><?php echo ($oProjeto) ? $oProjeto->getDataPrevisaoFimFormatado() : ""?></p>
                        </div>

                    </div>
                    <?php if ($voProjetoDocumento){ ?>
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Descrição</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($voProjetoDocumento as $oProjetoDocumento){ ?>
                                    <tr>
                                        <td><?php echo $oProjetoDocumento->getCodProjetoDocumento() ?></td>
                                        <td><?php echo $oProjetoDocumento->getDescricao() ?></td>
                                        <td>
                                            <a href="<?php echo $oProjetoDocumento->getUrl() ?>" target="_blank" class="btn btn-success">Abrir</a>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete" data-id="<?php echo $oProjetoDocumento->getCodProjetoDocumento()?>">Excluir</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php }else{ ?>
                        <br><br>
                        <div class="col-md-12">
                            <div class="alert alert-warning">
                                <p>Nenhum documento enviado.</p>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="form-group col-md-12" style="text-align: center">
                        <a class="btn btn-lg btn-primary" href="?action=Projeto.preparaLista">Voltar</a>
                    </div>
                </div>

            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Documento -->
    <div class="modal fade" id="modal" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <header class="box-header">
                    <h2 class="box-title">Cadastrar Documento</h2>
                </header>
                <form id="formUpload" method="post" enctype="multipart/form-data" action="?action=ProjetoDocumento.processaFormulario">
                    <input type="hidden" name="fCodProjetoDocumento" id="CodProjetoDocumento" value="">
                    <input type="hidden" name="fCodProjeto" id="CodProjeto" value="<?php echo $_REQUEST['nCodProjeto'] ?>">
                    <input type="hidden" name="sOP" id="sOP" value="Cadastrar">

                    <div class="box-body">

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="Descricao">Descrição</label>
                                <input name="fDescricao" id="Descricao" class="form-control" type='text' placeholder="Nome do Documento" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="Documento">Arquivo</label>
                                <input type="file" name="fDocumento" id="Documento">
                            </div>
                        </div>

                    </div>

                    <!-- /.box-body -->
                    <footer class="box-footer">
                        <input type="submit" class="btn btn-primary" name="btnEnviar" id="btnEnviar" value="Salvar">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </footer>

                </form>

            </div>

        </div>
    </div>

    <?php include_once('includes/footer.php')?>
</div>
<?php include_once('includes/javascript.php')?>
<!-- ./wrapper -->
<script>
    $('#confirm-delete',0,0).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget,0,0);
        let modal = $(this,0,0);
        let nCodDocumento = button.data("id");
        modal.find("#ok").attr("href","?action=ProjetoDocumento.processaFormulario&sOP=Excluir&nCodProjetoDocumento=" + nCodDocumento + "&nCodProjeto=<?php echo $_GET['nCodProjeto'] ?>");
    });
</script>
</body>
</html>
