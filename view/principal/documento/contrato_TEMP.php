<?php
ob_start();
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Contrato</title>
</head>
<body>
  <p style="text-align: center;"><strong>CONTRATO DE ELABORAÇÃO DE PROJETO #SERVICO#</strong></p>
  <p style="padding-left: 350px; text-align: justify;">CONTRATO DE PRESTAÇÃO DE SERVIÇO PARA ELABORAÇÃO DE PROJETO DE #SERVICO#, QUE ENTRE SI CELEBRAM <strong>#CLIENTE#</strong> E <strong>#RAZAO_SOCIAL#</strong> NA FORMA ABAIXO:</p>
  <p style="text-align: justify;">&nbsp;</p>
  <p style="text-align: justify;"><strong>#CLIENTE#</strong>, inscrito(a) sob o documento de nº<strong>#IDENTIFICACAO#</strong> com sede no endereço, #ENDERECO_CLIENTE# doravante denominado parte CONTRATANTE, e <strong>#RAZAO_SOCIAL#</strong>, sob identificação #ESCRITORIO_DOCUMENTO#, cujo escritório localiza-se à #ESCRITORIO_ENDERECO# nesta cidade, doravante denominada parte CONTRATADA, firmam o presente contrato, que se regerá pelas cláusulas seguintes e pelas condições descritas no presente.</p>
  <p style="text-align: justify;"><strong>I - DO OBJETO DO CONTRATO</strong></p>
  <p style="text-align: justify;"><strong> CLÁUSULA PRIMEIRA:</strong> É objeto do presente contrato a realização do desenvolvimento completo do projeto de #SERVICO#: <strong>" #PROPOSTA_DESCRICAO#"</strong> composto de dados concepcionais apresentados em escala adequada à perfeita compreensão dos elementos nele contidos.</p>
  <p style="text-align: justify;"><strong>II- DA REALIZAÇÃO DO SERVIÇO</strong></p>
  <p style="text-align: justify;"><strong>CLÁUSULA SEGUNDA:</strong> O serviço a ser realizado será composto pelas seguintes etapas:</p>
  #ETAPAS#
    <p style="text-align: justify;"><strong>PARÁGRAFO PRIMEIRO:</strong>
    #VISITAS#
    
  </p>
  <p style="text-align: justify;"><strong>PARÁGRAFO SEGUNDO:</strong> A execução do projeto fica única e exclusivamente a cargo do CONTRATANTE, que poderá, no entanto, contratar o serviço de gerenciamento de obra, o qual será necessariamente precedido de um orçamento de execução do projeto. O gerenciamento de obra compõe-se de: a) administração dos custos, onde se incluem as cotações de preços em fornecedores de mão de obra terceirizada, a compra dos materiais e etc; b) coordenação da execução de todos os serviços que serão executados na obra, exceto aqueles prestados por empresas especializadas como modulados, refrigeração, som, automação, etc. e a indicação da mão de obra (pedreiro, encanador, eletricista, gesseiro, pintor, etc.).</p>
  <p style="text-align: justify;"><strong>PARÁGRAFO TERCEIRO:</strong> Os desenhos serão apresentados em escalas adequadas à perfeita compreensão dos elementos contidos.</p>
  <p style="text-align: justify;"><strong>PARÁGRAFO QUARTO:</strong> A CONTRATADA entregará, no prazo estipulado no presente contrato, o Registro de Responsabilidade Técnica (RRT) emitido pelo Conselho de Arquitetura e Urbanismo (CAU) e o projeto de interior em uma cópia impressa e outra digitalizada a partir de QRCODE, que ficará disponível para download por um período de 12 meses com todos os arquivos referentes ao projeto para a CONTRATANTE, ficando os demais custos adicionais de impressão pagos por conta deste.</p>
  <p style="text-align: justify;"><strong>III- DA RESPONSABILIDADE DO CONTRATANTE:</strong></p>
  <p style="text-align: justify;"><strong>CLÁUSULA TERCEIRA:</strong> A CONTRATANTE somente poderá utilizar o projeto para o fim e local indicados nos documentos e desenhos apresentados, ficando vedada a sua reprodução ou execução no seu todo ou parte para outra finalidade ou local sem a prévia autorização de seu autor sob as penas da lei. (Lei nº 9.610/1998).</p>
  <p style="text-align: justify;"><strong>CLÁUSULA QUARTA:</strong> A CONTRATANTE deverá seguir todas as especificações técnicas descritas no projeto de interior, ficando a CONTRATADA isenta de responsabilidade em caso da não observância das normas técnicas emitidas pelos Conselhos de Classe e legislações vigentes.</p>
  <p style="text-align: justify;"><strong>CLÁUSULA QUINTA:</strong> Após a aprovação do projeto pela CONTRATANTE as modificações de qualquer natureza ou dimensões neste serão cobradas separadamente.</p>
  <p style="text-align: justify;"><strong>CLÁUSULA SEXTA:</strong> O projeto de cargas elétricas, o projeto de dimensionamento da rede hidráulica, o projeto estrutural, o projeto para a instalação de som, automação e ar condicionado e o projeto de paisagismo bem como sua execução e manutenção não são de responsabilidade da CONTRATADA, devendo os mesmos ser elaborados por engenheiros responsáveis em cada área ou mesmo pelos profissionais que executarão os serviços.</p>
  <p style="text-align: justify;"><strong>CLÁUSULA SÉTIMA:</strong> Fica a cargo da CONTRATANTE a contratação da mão de obra para execução do projeto de interior bem como a compra do material a ser utilizado, podendo a CONTRATADA indicar mão de obra qualificada e lojas especializadas, porém não se responsabilizando pelos seus prazos, serviços e/ou produtos.</p>
  <p style="text-align: justify;"><strong>CLÁUSULA OITAVA:</strong> As Despesas com cópias adicionais dos projetos e os custos com impostos e emolumentos para aprovações dos mesmos perante órgãos públicos ficam por conta da CONTRATANTE.</p>
  <p style="text-align: justify;"><strong>IV- DA REMUNERAÇÃO E VALOR CONTRATUAL</strong></p>
  <!-- aqui-->
  <p style="text-align: justify;"><strong>CLÁUSULA NONA:</strong> Pela execução do objeto do presente contrato a CONTRATANTE pagará a CONTRATADA, a título de honorários, o valor de R$ #VALOR_CONTRATO# (#VALOR_EXTENSO#<?php echo Extenso::valorPorExtenso($nTotal) ?>), conforme orçamento aprovado em anexo, que faz parte integrante do presente contrato, #PARCELAMENTO#<?php echo ($nParcelas == 1) ? "valor que será pago integralmente na assinatura do contrato" : " valor este que será pago da seguinte forma:R$".$voPagamento[0]->getValorFormatado()." (".Extenso::valorPorExtenso($voPagamento[0]->getValorFormatado()) ."na assinatura do contrato, e as demais parcelas pagas mensalmente na data estipulada abaixo até a quitação total do valor do contrato"; ?></p>

  <div style="text-align: center">
      <table style="text-align: center;border-collapse: collapse;width: 100%">
          <tbody>
  		#FORMA_DESCRICAO_PAGAMENTO#
          <?php if ($nParcelas == 1){ ?>
              <tr>
                  <td style="border: 1px solid #000000; padding:4px; margin:4px;">
                      <strong>À VISTA: R$ <?php echo $voPagamento[0]->getValorFormatado(); ?></strong>
                  </td>
              </tr>
          <?php }else{ ?>
              <tr>
                  <th style="border: 1px solid #000000; padding:4px; margin:4px;">Parcela</th>
                  <th style="border: 1px solid #000000; padding:4px; margin:4px;">Valor</th>
                  <th style="border: 1px solid #000000; padding:4px; margin:4px;">Vencimento</th>
              </tr>
              <?php $nSum = 1; foreach ($voPagamento as $oPagamento){ ?>
                  <tr>
                      <td style="border: 1px solid #000000; padding:4px; margin:4px;"><?php echo $nSum++; ?></td>
                      <td style="border: 1px solid #000000; padding:4px; margin:4px;"><strong>R$ <?php echo $oPagamento->getValorFormatado(); ?> </strong>(<?php echo Extenso::valorPorExtenso($oPagamento->getValorFormatado()); ?>)</td>
                      <td style="border: 1px solid #000000; padding:4px; margin:4px;"><?php echo $oPagamento->getDataPrevisaoFormatado(); ?></td>
                  </tr>
              <?php } ?>
          <?php } ?>
          </tbody>
      </table>
  </div>

  <p style="text-align: justify;"><strong>IV - DOS PRAZOS, RESCISÃO E PENALIDADES CONTRATUAIS.</strong></p>
  <p style="text-align: justify;"><strong>CLÁUSULA DÉCIMA:</strong> Os serviços objeto do presente contrato deverão ser iniciados no máximo até o 2º (segundo) dia útil contado a partir da data de sua assinatura do contrato.</p>
  <p style="text-align: justify;"><strong>CLÁUSULA DÉCIMA PRIMEIRA:</strong> A CONTRATADA assume o compromisso de entregar os projetos nos prazos estipulados no item II. PARÁGRAFO ÚNICO: Os prazos de entrega dos projetos especificados na cláusula segunda podem ser prorrogados, por igual período, em razão da complexidade do projeto ou sempre que houver alterações após a aprovação dos mesmos pela CONTRATANTE. CLÁUSULA DÉCIMA SEGUNDO: O presente instrumento poderá ser rescindido caso qualquer uma das partes descumpra o disposto neste contrato, devendo a parte responsável pelo descumprimento de quaisquer das CLÁUSULAS nele previstas, sem prejuízo de responsabilidade civil e sem prejuízo de cobrança judicial por perdas e danos, pagar multa no valor de 10% e juros de mora no valor de 2% ao mês sobre o valor do contrato.</p>
  <p style="text-align: justify;"><strong>V- DAS ALTERAÇÕES CONTRATUAIS POSTERIORES</strong></p>
  <p style="text-align: justify;"><strong>CLÁUSULA DÉCIMA TERCEIRA:</strong> Serão incorporadas a este Contrato, mediante TERMOS ADITIVOS ou ORÇAMENTOS COMPLEMENTARES, quaisquer alterações nas quantidades dos serviços, nos prazos ou nos valores cobrados, decorrentes das alterações solicitadas pela CONTRATANTE.</p>
  <p style="text-align: justify;"><strong>VI - DAS DISPOSIÇÕES FINAIS</strong></p>
  <p style="text-align: justify;"><strong>CLÁUSULA DÉCIMA QUARTA</strong>: O presente instrumento passa a valer a partir da sua assinatura pelas partes.</p>
  <p style="text-align: justify;"><strong>CLÁUSULA DÉCIMA QUINTA</strong>: Os orçamentos aprovados pela CONTRATANTE passam a fazer parte integrante do presente contrato.</p>
  <p style="text-align: justify;"><strong>CLÁUSULA DÉCIMA SEXTA</strong>: As imagens produzidas neste trabalho poderão ser utilizadas pelo CONTRATADO como portfólio pessoal, tendo, para tanto, o direito de divulgá-las em publicações na internet, folhetos, boletins informativos, jornais, revistas ou congêneres, de acordo com o que julgar necessário.</p>
  <p style="text-align: justify;"><strong>CLÁUSULA DÉCIMA SÉTIMA</strong>: Para dirimir quaisquer controvérsias oriundas do presente CONTRATO será competente o foro da comarca de Belém - Pará</p>
  <p style="text-align: justify;">E por estarem, assim justos e contratados, firmam o presente instrumento, em duas vias de igual teor, juntamente com 2 duas testemunhas.</p>
  <p style="text-align: justify;"></p>

  <p style="text-align: justify;">#CIDADE_UFA# Belém (PA), #DATA_CONTRATO#<?php echo ucwords(utf8_encode(strftime("%d de %B de %Y", strtotime(date($oProjeto->getDataInicio()))))) ?>.</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <table style="width: 100%;">
      <tbody>
      <tr style="height: 21px;">
          <td style="width: 49%; height: 21px; border-bottom: 1px solid #000;">&nbsp;</td>
          <td style="width: 2%; height: 84px;" rowspan="4">&nbsp;</td>
          <td style="width: 49%; height: 21px; border-bottom: 1px solid #000;padding-left:50px;"></td>
      </tr>
      <tr style="height: 21px;">
          <td style="width: 49%; height: 21px; text-align: center;">#CLIENTE#<?php echo $oProjeto->getCliente()->getNome()?></td>
          <td style="width: 49%; height: 21px; text-align: center;">#RAZAO_SOCIAL#<?php echo $_SESSION['oEscritorio']->getRazaoSocial()?></td>
      </tr>
      <tr style="height: 21px;">
          <td style="width: 49%; height: 21px; text-align: center;">#IDENTIFICACAO#<?php echo $oProjeto->getCliente()->getIdentificacao()?> </td>
          <td style="width: 49%; height: 21px; text-align: center;">&nbsp;#ESCRITORIO_DOCUMENTO#<?php echo $_SESSION['oEscritorio']->getDocumento()?></td>
      </tr>
      <tr style="height: 21px;">
          <td style="width: 49%; height: 21px; text-align: center;">CONTRATANTE</td>
          <td style="width: 49%; text-align: center; height: 21px;">&nbsp;CONTRATADA</td>
      </tr>
      </tbody>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <table style="width: 100%;">
      <tbody>
      <tr style="height: 21px;">
          <td style="width: 49%; height: 21px; border-bottom: 1px solid #000;">&nbsp;</td>
          <td style="width: 2%; height: 84px;" rowspan="4">&nbsp;</td>
          <td style="width: 49%; height: 21px; border-bottom: 1px solid #000;">&nbsp;</td>
      </tr>
      <tr style="height: 21px;">
          <td style="width: 49%; height: 21px; text-align: center;">&nbsp;</td>
          <td style="width: 49%; height: 21px; text-align: center;">&nbsp;</td>
      </tr>
      <tr style="height: 21px;">
          <td style="width: 49%; height: 21px; text-align: center;">CPF:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="width: 49%; height: 21px; text-align: center;">&nbsp;CPF:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
      </tr>
      <tr style="height: 21px;">
          <td style="width: 49%; height: 21px; text-align: center;">TESTEMUNHA 1</td>
          <td style="width: 49%; text-align: center; height: 21px;">&nbsp;TESTEMUNHA 2</td>
      </tr>
      </tbody>
  </table>
  <p>&nbsp;</p>

</body>
</html>
<?php
// die();
$sPapel = "A4-P";
$sNomeArquivo = preg_replace("[^a-zA-Z0-9_]", "", strtr(mb_strtoupper($oProjeto->getProposta()->getNome(),"utf8"). "_".date('Y-m-d'), "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
$sNomeArquivo = preg_replace('<\W+>', "-", $sNomeArquivo);
$sNomeArquivo = "2.contrato_{$sNomeArquivo}.pdf";
include_once("includes/gera_pdf.php");
?>
