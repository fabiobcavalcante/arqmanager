<?php
ob_start();
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
$dHoje = utf8_encode(ucwords(strftime("%d de %B de %Y", strtotime('today'))));

// if ($_SERVER['HTTP_HOST'] != 'sistema.arqmanager.com.br')
//   $sPath = "/arqmanager/";
// else {
  $sPath = "/";
// }



?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p <?php echo ($_SESSION['oEscritorio']->getIdEscritorio() =='2') ? "style='background-color:#CCCCCC;'" : "" ?> ><img src="<?php echo $_SESSION['oEscritorio']->getLogomarca()?>" alt="" width="220" /></p>
<hr />
<div style="width: 630px;">
<table style="width: 100%;">
<tbody>
<tr>
<td>&nbsp;</td>
<td style="text-align: center;">
<h3 style="text-align: center;"><strong>RECIBO</strong></h3>
</td>
</tr>
</tbody>
</table>
<p style="padding-left: 60px;">&nbsp;</p>
<p style="padding-left: 60px;">&nbsp;</p>
<p style="padding-left: 60px; text-align: justify;">Eu, <?php echo $_SESSION['oEscritorio']->getRazaoSocial()?>, inscrita sob CPF/CNPJ  <?php echo $_SESSION['oEscritorio']->getDocumento()?>, declaro que recebi na data de hoje, de <?php echo $oCliente->getNome()?>, inscrito(a) sob <?php echo (strlen($oCliente->getIdentificacao() == 18)) ? "CNPJ ".$oCliente->getIdentificacao() : "CPF " . $oCliente->getIdentificacao() ?>, a import&acirc;ncia de <strong>R$<?php echo $oProjetoPagamento->getValorFormatado()?></strong> (<?php echo Extenso::valorPorExtenso($oProjetoPagamento->getValorFormatado())?>), como indica&ccedil;&atilde;o de pagamento da parcela <?php echo $oProjetoPagamento->getDescPagamento() ?> do projeto&nbsp; de <?php echo $oProjeto->getDescricao() ?> (<?php echo $oProposta->getIdentificacao() ?>).</p>
<p style="padding-left: 60px;">&nbsp;</p>
<p style="padding-left: 60px;">Sendo express&atilde;o de verdade e sem qualquer coa&ccedil;&atilde;o, firmo o presente.</p>
<p style="padding-left: 60px;">&nbsp;</p>
<p style="padding-left: 60px;">&nbsp;</p>
<p style="padding-left: 60px;">&nbsp;</p>
<p style="padding-left: 60px;">&nbsp;</p>
<p style="padding-left: 60px; text-align: center;"><?php echo $_SESSION['oEscritorio']->getCidadeUf()?>, <?php echo ($oProjetoPagamento->getDataEfetivacaoFormatado())?:$dHoje ?>.</p>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="padding-left: 60px; text-align: center;"><?php echo ($_SESSION['oEscritorio']->getAssinatura()) ? " <img style='width:180px' alt='assinatura' src='https://sistema.arqmanager.com.br/{$_SESSION['oEscritorio']->getAssinatura()}'>" : "" ?></span></p>
<p style="padding-left: 60px; text-align: center;"><span style="border-top: 1px solid #000;"><?php echo $_SESSION['oEscritorio']->getRazaoSocial()?>.</span></p>
</div>
<p><br />
<?php

$sPapel = "A4-P";
//$pdf->WriteHTMLCell(192,0,9,'',$html,0,1);

$sNomeArquivo = preg_replace("[^a-zA-Z0-9_]", "", strtr(mb_strtoupper($oProjeto->getProposta()->getNome(),"utf8"). "_".date('Y-m-d'), "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
$sNomeArquivo = preg_replace('<\W+>', "-", $sNomeArquivo);
$sNomeArquivo = "recibo_{$oProjetoPagamento->getDescPagamento()}_{$sNomeArquivo}.pdf";

$rodape = "<div align='center'><img src='dist/img/rodape.svg' height='50'></div>";

 include_once("includes/gera_pdf.php");
