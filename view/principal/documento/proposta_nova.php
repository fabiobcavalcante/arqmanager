<?php
ob_start();
set_time_limit(60);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Orçamento</title>
    <meta charset="utf-8">
    <style type="text/css">
        /*body {
            background-image: url('dist/img/grade.svg') ;
            background-repeat: repeat-y;
            background-position: right top;
        }*/
        /* @page {
        background: url('/full/path/to/image/background_300dpi.png') no-repeat 0 0;
        background-image-resize: 6; */
        }
    </style>
</head>
<body>


<table style="width:100%">
    <tr>
        <td> <img src="dist/img/logo.svg" width="220">
        </td>
        <td align="center"><h3>PROPOSTA DE SERVIÇO DE <?php echo strtoupper($oProposta->getServico()->getDescServico())?></h3></td>
    </tr>
</table>
<div style="width:630px">
    <p>
        <strong>A Sr(a). <?php echo $oProposta->getNome()?>.</strong>
    </p>
    <p>
        <strong></strong>
    </p>
    <p>
        Conforme solicitado, apresentamos proposta orçamentária para a realização de projeto <?php echo $oProposta->getServico()->getDescServico()?> para <?php echo $oProposta->getDescricao()?>.<!--, situado à <?php //echo "#ENDEREÇO#"//$oProposta->getEndereco()?>.-->
    </p>
    <p>
        Esta proposta contempla:
    </p>
    <ul>
        <?php foreach($voProjetoServicoMicroServico as $oMicroServico){?>
            <li>
                <em>
                    <?php
                    echo $oMicroServico->getCodProjeto();
                    if($oMicroServico->getQuantidade() >0){
                        echo " QTDE. " . $oMicroServico->getQuantidade() . (($oMicroServico->getDescricaoQuantidade()) ? " (".$oMicroServico->getDescricaoQuantidade().")" : "");
                    }elseif($oMicroServico->getDescricaoQuantidade()){
                        echo " (".$oMicroServico->getDescricaoQuantidade().")";
                    }
                    ?>;
                </em>
            </li>
        <?php } //foreach($voProjetoServicoMicroServico as $oMicroServico){ ?>
    </ul>

    <p>
        <em></em>
    </p>
    <p>
        <strong>VALOR DA PROPOSTA:</strong>
    </p>
    <p>
        O valor proposto para a execução do projeto acima mencionado é:
    </p>
    <p>
        * Projeto..................... R$ <?php echo $oProposta->getValorPropostaFormatado()?> (<?php echo strtoupper($oProposta->getValorPropostaFormatadoExtenso())?>)
    </p>
    <p>
        <strong></strong>
    </p>
    <p>
        <strong></strong>
    </p>
    <p>
        <strong>FORMA DE PAGAMENTO</strong>
        :
    </p>
    <div align="center">
        <table align="center" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td style="border: 1px solid #000000; padding:4px; margin:4px;">

                    <strong>À VISTA: R$ <?php echo $oProposta->getValorAvistaFormatado()?></strong>

                </td>
            </tr>
            <tr>
                <td style="border: 1px solid #000000; padding:4px; margin:4px;">

                    <strong> À PRAZO: R$ <?php echo $oProposta->getValorPropostaFormatado()?> </strong>
                    (<?php echo $oProposta->getValorParcelaAprazo()?>)

                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <p>
        <strong></strong>
    </p>
    <p>
        <strong>PRAZOS DE ENTREGA:</strong>
    </p>

    <?php
    $nEtapa=1;
    $sEtapaAnt = "";

    foreach($voProjetoServicoEtapa as $oEtapa){
        $sDescricao = $nEtapa ."ª Etapa: ";
        if($oEtapa->getPrazo() > 0){
            $sDescricao .= $oEtapa->getServicoEtapa()->getDescricao().", ".$oEtapa->getPrazo(). " dias a contar da ";
        }else {
            $sDescricao .= $oEtapa->getServicoEtapa()->getDescricao();
        }
        $sDescricao .= ($nEtapa ==1) ? "data de assinatura do contrato" : (($oEtapa->getPrazo() > 0)? " aprovação do(a) {$sEtapaAnt}":" - a combinar ");
        ?>
        <?php echo $sDescricao ?>;<br>
        <?php $nEtapa++;
        $sEtapaAnt = $oEtapa->getServicoEtapa()->getDescricao();
    } ?>
    <p>
        Atenciosamente,
    </p>
    <p>
        <strong></strong>
    </p>

    <table width="726" align="center">
        <tr>
            <td style="border-top:1px solid #000; width:45%; text-align:center"><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?></td>
            <td style="width:10%"></td>
            <td style="border-top:1px solid #000; width:45%; text-align:center"><?php echo mb_strtoupper($oProposta->getNome())?></td>
        </tr>
    </table>

    <p align="right">Aprovado em: ___/___/<?php echo date('Y')?>.</p>
    <p align="justify">
        <strong>OBS1:</strong>
        <?php if($oProposta->getVisitasIncluidas() > 0){?>
        Está incluso no presente orçamento <?php echo $oProposta->getVisitasIncluidas()?> visitas técnicas ao local com agendamento prévio. Para mais visitas poderá ser cobrada hora técnica no valor de R$ 250,00 (DUZENTOS E CINQUENTA REAIS).
      <?php } else{ ?>
        Visitas técnicas serão custeadas e acordadas com o cliente.
      <?php } ?>
    </p>
    <p align="justify">
        <strong>OBS2: </strong>
        Para gerenciamento de obra poderá ser feito um    <strong><em>orçamento da execução do projeto</em></strong>. Esse trabalho constitui na administração dos custos, que inclui as cotações de preços em  três fornecedores, a compra dos materiais, a indicação da mão de obra (pedreiro, encanador, eletricista, gesseiro, pintor, marceneiro, etc.) e a coordenação da execução de todos os serviços.
    </p>
    <p>
        <strong></strong>
    </p>
    <p align="justify">
        <strong>OBS3:</strong>
        O presente orçamento não contempla projetos de estrutura, de instalações elétricas e hidrossanitárias, assim como, instalação de som, automação e ar condicionado. <strong></strong>
    </p>
    <p align="justify">
        <strong>OBS4:</strong>
        O prazo para assessoria na execução do projeto é de <strong>6 meses</strong> a partir da entrega do mesmo. <strong></strong>
    </p>
    <p>
        <strong></strong>
    </p>
</div>
</body>
</html>
<?php
//  die();
$sPapel = "A4-P";
$sNomeArquivo = preg_replace("[^a-zA-Z0-9_]", "", strtr($oProposta->getNome(). "_".date('Y-m-d'), "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
$sNomeArquivo = preg_replace('<\W+>', "-", $sNomeArquivo);
$sNomeArquivo = "1.proposta_{$sNomeArquivo}.pdf";
include_once("includes/gera_pdf.php");
?>
