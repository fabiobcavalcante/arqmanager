<?php
 $sOP = $_REQUEST['sOP'];
 $oDocumento = $_REQUEST['oDocumento'];

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Documento - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?>  - Principal</h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Documento.preparaLista">Gerenciar Documentos</a>
 			<li class="active">documento - <?php echo $sOP?></li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Documento - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formDocumento" action="?action=Documento.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodDocumento" value="<?=(is_object($oDocumento)) ? $oDocumento->getCodDocumento() : ""?>" />
 				<div  class="box-body" id="form-group">
 								<input type='hidden' name='fCodDocumento' value='<?php echo ($oDocumento) ? $oDocumento->getCodDocumento() : ""?>'/>

 <div class="form-group col-md-4">
					<label for="Titulo">Título:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Titulo' placeholder='Título' name='fTitulo'  required   value='<?php echo ($oDocumento) ? $oDocumento->getTitulo() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="Conteudo">Conteúdo:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Conteudo' placeholder='Conteúdo' name='fConteudo'  required   value='<?php echo ($oDocumento) ? $oDocumento->getConteudo() : ""?>'/>
				</div>
			</div>
					<input type='hidden' name='fAtivo' value='<?php echo ($oDocumento) ? $oDocumento->getAtivo() : "1"?>'/>


 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(function($){
				 });
 </script>
 </body>
 </html>
