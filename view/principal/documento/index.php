<?php
 $voDocumento = $_REQUEST['voDocumento'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de documento </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Principal</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Documentos</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Documento</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formDocumento" id="formDocumento" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesDocumento" onChange="JavaScript: submeteForm('Documento')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=Documento.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo documento</option>
   						<option value="?action=Documento.preparaFormulario&sOP=Alterar" lang="1">Alterar documento selecionado</option>
   						<option value="?action=Documento.preparaFormulario&sOP=Detalhar" lang="1">Detalhar documento selecionado</option>
   						<option value="?action=Documento.processaFormulario&sOP=Excluir" lang="2">Excluir documento(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voDocumento)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('Documento')"><i class="icon fa fa-check"></a></th>
   					<th>Id</th>
					<th>Título</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voDocumento as $oDocumento){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('Documento')" type="checkbox" value="<?=$oDocumento->getCodDocumento()?>" name="fIdDocumento[]"/></td>
  					<td><?php echo $oDocumento->getCodDocumento()?></td>
					<td><?php echo $oDocumento->getTitulo()?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Id</th>
					<th>Título</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voDocumento)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
