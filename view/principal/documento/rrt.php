<?php
ob_start();
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
$oProposta = $_REQUEST['oProposta'];
$oCliente = $_REQUEST['oCliente'];

$oProjeto = $_REQUEST['oProjeto'];
$voProjetoServicoEtapa = $_REQUEST['voProjetoServicoEtapa'];
$voProjetoServicoMicroServico = $_REQUEST['voProjetoServicoMicroServico'];
$voEtapaProjeto = [];

$sDescricaoMicroServico = "";

if($voProjetoServicoMicroServico){
   foreach($voProjetoServicoMicroServico as $oMicroServico){
		 if($oMicroServico->getCodProjeto() != 'RRT - CAU'){
			 $sDescricaoMicroServico .= $oMicroServico->getServicoMicroservico()->getDescricao();
			 if($oMicroServico->getQuantidade() >0){
           $sDescricaoMicroServico .= " QTDE. " . $oMicroServico->getQuantidade() . (($oMicroServico->getDescricaoQuantidade()) ? " (".$oMicroServico->getDescricaoQuantidade()."); " : "");
       }elseif($oMicroServico->getDescricaoQuantidade()){
           $sDescricaoMicroServico.= " (".$oMicroServico->getDescricaoQuantidade()."); ";
       }else{
           $sDescricaoMicroServico .= "; ";
       }

		 }
   } //foreach($voProjetoServicoMicroServico as $oMicroServico){
$sDescricaoMicroServico = substr($sDescricaoMicroServico,0,-2);
}

$voPagamento = $_REQUEST['voPagamento'];
$nTotal = 0;
$nParcelas = count($voPagamento);
foreach ($voPagamento as $oPagamento) {
    $nTotal += $oPagamento->getValor();
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>RRT</title>
    <!--suppress CssUnknownTarget -->
    <style type="text/css">
        /* body {
             background-image: url('dist/img/grade.svg') ;
             background-repeat: repeat-y;
             background-position: right top;
         }
         /* @page {
         background: url('/full/path/to/image/background_300dpi.png') no-repeat 0 0;
         background-image-resize: 6;
         }*/
    </style>
</head>
<body>

<p style="text-align: left;"><strong>DADOS PARA ELABORAÇÃO DE RRT (<?php echo mb_strtoupper($oProposta->getServico()->getDescServico(),"utf8"); ?>)</strong></p>
<p style="text-align: left;"><strong>IDENTIFICAÇÃO: </strong><?php echo $oCliente->getIdentificacao(); ?></p>
<p style="text-align: left;"><strong>NOME: </strong><?php echo ($oCliente->getRazaoSocial()) ? $oCliente->getRazaoSocial() : $oCliente->getNome(); ?></p>
<p style="text-align: left;"><strong>EMAIL: </strong><?php echo $oCliente->getEmail(); ?></p>
<p style="text-align: left;"><strong>TELEFONES: </strong><?php echo $oCliente->getTelefones(); ?></p>
<p style="text-align: left;"><strong>METRAGEM(m²): </strong><?php echo ( $oProjeto->getMetragem()) ? $oProjeto->getMetragem():"não especificado"; ?></p>
<p style="text-align: center;"><strong>GRUPO: </strong></p>
<p style="text-align: justify;"><strong>DESCRIÇÃO:</strong> Realização do desenvolvimento completo do projeto de <?php echo mb_strtoupper($oProposta->getServico()->getDescServico(),"utf8"); ?>: <?php echo $oProposta->getDescricao(); ?> composto de dados concepcionais apresentados em escala adequada à perfeita compreensão dos elementos nele contidos.contendo as seguintes etapas: <?php echo $sDescricaoMicroServico;?></p>
<p style="text-align: left;"><strong>CONTRATO: </strong><?php echo $oProjeto->getNumeroProjetoFormatada() ?></p>
<p style="text-align: left;"><strong>VALOR: </strong>R$<?php echo number_format($nTotal,2,",","."); ?></p>
<p style="text-align: left;"><strong>Data Início: </strong><?php echo $oProjeto->getDataInicioFormatado() ?>.</p>
<p style="text-align: left;"><strong>Data Previsão Fim: </strong> <?php echo ($oProjeto->getDataPrevisao) ? $oProjeto->getDataPrevisaoFormatado() : "sem data específica" ?>.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php

$sPapel = "A4-P";
$sNomeArquivo = "4.DADOS_RRT_{$oProjeto->getCodProjeto()}.pdf";
include_once("includes/gera_pdf.php");
?>
