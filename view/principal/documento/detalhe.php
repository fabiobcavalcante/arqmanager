﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oDocumento = $_REQUEST['oDocumento'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Documento - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Principal</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Documento.preparaLista">Gerenciar Documentos</a>
 			<li class="active">Documento - <?php echo $sOP?></li>
 		</ol>

     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Documento - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="Titulo" class="control-label">Título:</label>
		<p><?php echo ($oDocumento) ? $oDocumento->getTitulo() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Conteudo" class="control-label">Conteúdo:</label>
		<p><?php echo ($oDocumento) ? $oDocumento->getConteudo() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Documento.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
