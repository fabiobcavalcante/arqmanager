<?php
 $voPropostaServicoEtapa = $_REQUEST['voPropostaServicoEtapa'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Projeto X Serviço X Etapa </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Projeto X Serviço X Etapa</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Projeto X Serviço X Etapa</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formPropostaServicoEtapa" id="formPropostaServicoEtapa" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesPropostaServicoEtapa" onChange="JavaScript: submeteForm('PropostaServicoEtapa')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=PropostaServicoEtapa.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Projeto X Serviço X Etapa</option>
   						<option value="?action=PropostaServicoEtapa.preparaFormulario&sOP=Alterar" lang="1">Alterar Projeto X Serviço X Etapa selecionado</option>
   						<option value="?action=PropostaServicoEtapa.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Projeto X Serviço X Etapa selecionado</option>
   						<option value="?action=PropostaServicoEtapa.processaFormulario&sOP=Excluir" lang="2">Excluir Projeto X Serviço X Etapa(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voPropostaServicoEtapa)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('PropostaServicoEtapa')"><i class="icon fa fa-check"></a></th>
   					<th>Id</th>
					<th>Proposta</th>
					<th>Projeto</th>
					<th>Descrição</th>
					<th>Prazo</th>
					<th>Inserido por</th>
					<th>Alterado por</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voPropostaServicoEtapa as $oPropostaServicoEtapa){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('PropostaServicoEtapa')" type="checkbox" value="<?=$oPropostaServicoEtapa->getCodEtapa()?>||<?=$oPropostaServicoEtapa->getCodProposta()?>" name="fIdPropostaServicoEtapa[]"/></td>
  					<td><?php echo $oPropostaServicoEtapa->getCodEtapa()?></td>
					<td><?php echo $oPropostaServicoEtapa->getCodProposta()?></td>
					<td><?php echo $oPropostaServicoEtapa->getCodProjeto()?></td>
					<td><?php echo $oPropostaServicoEtapa->getDescricao()?></td>
					<td><?php echo $oPropostaServicoEtapa->getPrazo()?></td>
					<td><?php echo $oPropostaServicoEtapa->getInseridoPor()?></td>
					<td><?php echo $oPropostaServicoEtapa->getAlteradoPor()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Id</th>
					<th>Proposta</th>
					<th>Projeto</th>
					<th>Descrição</th>
					<th>Prazo</th>
					<th>Inserido por</th>
					<th>Alterado por</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voPropostaServicoEtapa)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
