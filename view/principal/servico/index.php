<?php
$voServico = $_REQUEST['voServico'];
$voEtapa = $_REQUEST['voEtapa'];
?>
<!doctype html>
<html lang="pt-br">
<head>
    <title>Lista de Serviço </title>
    <?php include_once('includes/head.php')?>
    <style>
        a{
            cursor: pointer;
        }
    </style>
</head>
<body class="sidebar-mini wysihtml5-supported skin-green-light">
<div class="wrapper">
    <?php include_once('includes/header.php')?>
    <?php include_once('includes/menu.php')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
            <ol class="breadcrumb">
                <li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
                <li class="active">Gerenciar Serviços</li>
            </ol>

            <?php include_once('includes/mensagem.php')?>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Serviços / Etapas</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class='form-group col-md-4'>
                                <button type="button" class="btn btn-success" onclick="abreModal('Servico','Cadastrar')"><i class="fa fa-plus"></i> Cadastrar Serviço</button>
                                <button type="button" class="btn btn-primary" onclick="abreModal('Etapa','Cadastrar')"><i class="fa fa-plus"></i> Cadastrar Etapa</button>
                            </div>

                            <div class="col-md-12">

                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#divServico" data-toggle="tab">Serviços</a></li>
                                        <li><a href="#divEtapa" data-toggle="tab">Etapas</a></li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div id="divServico" class="tab-pane active">
                                        <?php if($voServico){?>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th style="width: 1%"></th>
                                                    <th style="width: 9%">Id</th>
                                                    <th style="width: 80%">Descrição</th>
                                                    <th style="width: 10%">Empresa</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($voServico as $oServico){ ?>
                                                    <tr>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ações<span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a onclick="abreModal('Servico','Alterar','<?php echo $oServico->getCodServico()?>')"><i class="fa fa-edit"></i> Alterar Serviço</a></li>

                                                                    <li><a onclick="abreModal('ServicoEtapa','','<?php echo $oServico->getCodServico()?>')"><i class="fa fa-list"></i> Gerenciar Entregas</a></li>

                                                                    <li><a href="?action=Servico.processaFormulario&sOP=Excluir&nCodServico=<?php echo $oServico->getCodServico()?>"><i class="fa fa-times"></i> Excluir Serviço</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                        <td><?php echo $oServico->getCodServico()?></td>
                                                        <td><?php echo $oServico->getDescServico()?></td>
                                                        <td><?php echo $oServico->getPercentualEmpresaFormatado()?>%</td>
                                                    </tr>
                                                <?php }?>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Id</th>
                                                    <th>Descrição</th>
                                                    <th>Empresa</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        <?php } ?>
                                    </div>
                                    <div id="divEtapa" class="tab-pane">
                                        <?php if($voEtapa){?>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th style="width: 1%"></th>
                                                    <th style="width: 9%">Id</th>
                                                    <th style="width: 80%">Descrição</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($voEtapa as $oEtapa){ ?>
                                                    <tr>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ações<span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a onclick="abreModal('Etapa','Alterar','<?php echo $oEtapa->getCodEtapa()?>')"><i class="fa fa-edit"></i> Alterar Etapa</a></li>

                                                                    <li><a href="?action=Servico.processaFormulario&sOP=Excluir&nCodServico=<?php echo $oEtapa->getCodEtapa()?>"><i class="fa fa-times"></i> Excluir Etapa</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                        <td><?php echo $oEtapa->getCodEtapa()?></td>
                                                        <td><?php echo $oEtapa->getDescricao()?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Id</th>
                                                    <th>Descrição</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('includes/footer.php')?>

    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <div class="modal-content" id="bodyModal"></div>
        </div>
    </div>

    <div class="modal fade" id="modalServicoEtapa">
        <div class="modal-dialog">
            <div class="modal-content" id="bodyServicoEtapa"></div>
        </div>
    </div>

    <div class="modal fade" id="modalMicroServico">
        <div class="modal-dialog">
            <div class="modal-content" id="bodyMicroServico"></div>
        </div>
    </div>

    <div class="modal fade" id="modalMicroServicoForm">
        <div class="modal-dialog">
            <div class="modal-content" id="bodyMicroServicoForm"></div>
        </div>
    </div>

</div>
<!-- ./wrapper -->
<?php include_once('includes/javascript.php')?>
</body>
</html>

<script>
    function abreModal(sFuncao,sOP,nParam=null,nParam2=null,nParam3=null){
        let sModal = "#modal";
        switch (sFuncao){
            case "Servico":
                switch (sOP){
                    case "Cadastrar":
                        recuperaConteudoDinamico("","action=Servico.preparaFormulario&sOP="+sOP, "bodyModal");
                        break;
                    case "Alterar":
                        recuperaConteudoDinamico("","action=Servico.preparaFormulario&nCodServico=" + nParam + "&sOP="+sOP, "bodyModal");
                        break;
                }
                break;
            case "Etapa":
                switch (sOP){
                    case "Cadastrar":
                        recuperaConteudoDinamico("","action=Etapa.preparaFormulario&sOP="+sOP, "bodyModal");
                        break;
                    case "Alterar":
                        recuperaConteudoDinamico("","action=Etapa.preparaFormulario&nCodEtapa=" + nParam + "&sOP="+sOP, "bodyModal");
                        break;
                }
                break;
            case "ServicoEtapa":
                switch (sOP){
                    case "Cadastrar":
                        sModal = "#modalServicoEtapa"
                        recuperaConteudoDinamico("","action=ServicoEtapa.preparaFormulario&sOP="+sOP+"&nCodServico="+nParam, "bodyServicoEtapa");
                        break;
                    case "Alterar":
                        sModal = "#modalServicoEtapa"
                        recuperaConteudoDinamico("","action=ServicoEtapa.preparaFormulario&nCodServicoEtapa=" + nParam2 + "&nCodServico=" + nParam + "&sOP="+sOP, "bodyServicoEtapa");
                        break;
                    default:
                        recuperaConteudoDinamico("","action=ServicoEtapa.preparaLista&nIdServico=" + nParam, "bodyModal");
                        break;
                }
                break;
            case "MicroServico":
                switch (sOP){
                    case "Cadastrar":
                        sModal = "#modalMicroServicoForm"
                        recuperaConteudoDinamico("","action=ServicoMicroServico.preparaFormulario&nCodServicoEtapa="+nParam2+"&nCodServico="+nParam+"&sOP="+sOP, "bodyMicroServicoForm");
                        break;
                    case "Alterar":
                        sModal = "#modalMicroServicoForm"
                        recuperaConteudoDinamico("","action=ServicoMicroServico.preparaFormulario&nCodMicroServico="+nParam3+"&nCodServicoEtapa="+nParam2+"&nCodServico="+nParam+"&sOP="+sOP, "bodyMicroServicoForm");
                        break;
                    default:
                        sModal = "#modalMicroServico"
                        recuperaConteudoDinamico("","action=ServicoMicroServico.preparaLista&nCodServicoEtapa="+nParam3+"&nCodEtapa="+nParam2+"&nCodServico="+nParam, "bodyMicroServico");
                        break;
                }
                break;
        }

        $(sModal).modal('show');
    }

    function processaFormulario(sForm){

        let form = $("#"+sForm);

        $.ajax({
            type: "POST",
            datatype: "json",
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function(){
                Swal.fire({
                    title: 'Enviando...',
                    didOpen: () => {
                        Swal.showLoading()
                    }
                })
            },
            error: function(oXMLRequest,sErrorType){
                console.log(form.attr('action'));
                console.log(oXMLRequest.responseText);
                console.log(oXMLRequest.status+' , '+sErrorType);
            },
            success: function(dados){
                console.log(dados);
                dados = jQuery.parseJSON(dados);

                if (dados.sClass === "success") {

                    recuperaConteudoDinamico("","action=ServicoEtapa.preparaLista&nCodEtapa=" + dados.nCodEtapa + "&nIdServico=" + dados.nCodServico, "bodyModal");

                    recuperaConteudoDinamico("","action=ServicoMicroServico.preparaLista&nCodServicoEtapa="+dados.nCodServicoEtapa+"&nCodEtapa=" + dados.nCodEtapa + "&nCodServico=" + dados.nCodServico, "bodyMicroServico");

                    $('#modalServicoEtapa').modal('hide');
                    $('#modalMicroServicoForm').modal('hide');
                }

                Swal.fire({
                    icon: dados.sClass,
                    title: dados.sMsg,
                    timer: 2000
                });
            }
        });
    }
</script>