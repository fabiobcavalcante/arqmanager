<?php
$sOP = $_REQUEST['sOP'];
$oServico = $_REQUEST['oServico'];
?>

<div class="box">
    <div class="modal-header">
        <h3 class="modal-title">Serviço - <?php echo $sOP ?></h3>
    </div>
    <!-- /.box-header -->
    <form method="post" class="form-horizontal" name="formServico" action="?action=Servico.processaFormulario">
        <input type="hidden" name="sOP" value="<?php echo $sOP?>" >
        <input type="hidden" name="fCodServico" value="<?=($oServico) ? $oServico->getCodServico() : ""?>" >
        <input type="hidden" name="fInseridoPor" value="<?=($oServico) ? $oServico->getInseridoPor() : ""?>" >
        <div class="modal-body">

            <div class="form-group row">

                <div class="col-md-6">
                    <label for="DescServico">Descrição:</label>
                    <input class="form-control" type='text' id='DescServico' placeholder='Descrição' name='fDescServico'  required   value='<?php echo ($oServico) ? $oServico->getDescServico() : ""?>'>
                </div>

                <div class="col-md-6">
                    <label for="DescDocumento">Descrição Documento:</label>
                    <input class="form-control" type='text' id='DescDocumento' placeholder='Descrição' name='fDescDocumento'  required   value='<?php echo ($oServico) ? $oServico->getDescDocumento() : ""?>'>
                </div>
            </div>
            <div class="form-group row">

                <div class="col-md-4">
                    <label for="PercentualUnit">% Empresa:</label>
                    <input class="form-control" type='text' id='PercentualUnit' placeholder='%' name='fPercentualEmpresa' onKeyPress="TodosNumero(event);" value='<?php echo ($oServico) ? $oServico->getPercentualEmpresaFormatado() : ""?>' required>
                </div>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="modal-footer">
            <div class="form-group col-md-12">
                <div class="col-sm-offset-5 col-sm-2">
                    <button type="submit" class="btn btn-lg btn-success"><?php echo $sOP?></button>
                </div>
            </div>
        </div>
    </form>
</div>