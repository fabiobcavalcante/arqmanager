<?php
 $voContaBancaria = $_REQUEST['voContaBancaria'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>- Lista de Conta Bancária </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Principal</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Conta Bancárias</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Conta Bancária</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formContaBancaria" id="formContaBancaria" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesContaBancaria" onChange="JavaScript: submeteForm('ContaBancaria')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=ContaBancaria.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Conta Bancária</option>
   						<option value="?action=ContaBancaria.preparaFormulario&sOP=Alterar" lang="1">Alterar Conta Bancária selecionado</option>
   						<option value="?action=ContaBancaria.processaFormulario&sOP=Excluir" lang="2">Excluir Conta Bancária(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voContaBancaria)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('ContaBancaria')"><i class="icon fa fa-check"></a></th>
   					<th>Código</th>
					<th>Responsável</th>
					<th>Agência</th>
					<th>Conta</th>
					<th>Tipo</th>
					<th>Banco</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voContaBancaria as $oContaBancaria){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('ContaBancaria')" type="checkbox" value="<?=$oContaBancaria->getCodContaBancaria()?>" name="fIdContaBancaria[]"/></td>
  					<td><?php echo $oContaBancaria->getCodContaBancaria()?></td>
					<td><?php echo $oContaBancaria->getCodColaborador()?></td>
					<td><?php echo $oContaBancaria->getAgencia()?></td>
					<td><?php echo $oContaBancaria->getConta()?></td>
					<td><?php echo $oContaBancaria->getTipoFormatado()?></td>
					<td><?php echo $oContaBancaria->getBanco()?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th>Código</th>
        					<th>Responsável</th>
        					<th>Agência</th>
        					<th>Conta</th>
        					<th>Tipo</th>
        					<th>Banco</th>
                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voContaBancaria)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
