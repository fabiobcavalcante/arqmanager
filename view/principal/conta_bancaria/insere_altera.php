<?php
 $sOP = $_REQUEST['sOP'];
 $oContaBancaria = $_REQUEST['oContaBancaria'];
 $voColaborador = $_REQUEST['voColaborador'];


 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Conta Bancária - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=ContaBancaria.preparaLista">Gerenciar Conta Bancárias</a>
 			<li class="active">Conta Bancária - <?php echo $sOP?></li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Conta Bancária - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formContaBancaria" action="?action=ContaBancaria.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodContaBancaria" value="<?=(is_object($oContaBancaria)) ? $oContaBancaria->getCodContaBancaria() : ""?>" />
 				<div  class="box-body" id="form-group">
 								<input type='hidden' name='fCodContaBancaria' value='<?php echo ($oContaBancaria) ? $oContaBancaria->getCodContaBancaria() : ""?>'/>

 <div class="form-group col-md-3 col-sm-6">
					<label for="Colaborador">Responsável:</label>
					<div class="input-group col-md-11 col-sm-12">
					<select name='fCodColaborador'  class="form-control select2"  required  >
						<option value=''>Selecione</option>
						<?php $sSelected = "";
						   if($voColaborador){
							   foreach($voColaborador as $oColaborador){
								   if($oContaBancaria){
									   $sSelected = ($oContaBancaria->getCodColaborador() == $oColaborador->getCodColaborador()) ? "selected" : "";
								   }
						?>
								   <option  <?php echo $sSelected?> value='<?php echo $oColaborador->getCodColaborador()?>'><?php echo $oColaborador->getNome()?></option>
						<?php
							   }
						   }
						?>
					</select>
			</div>
				</div>

 <div class="form-group col-md-2 col-sm-3">
					<label for="Agencia">Agência:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Agencia' placeholder='Agencia' name='fAgencia'  required   value='<?php echo ($oContaBancaria) ? $oContaBancaria->getAgencia() : ""?>'/>
			</div>
</div>
 <div class="form-group col-md-2 col-sm-3">
					<label for="Conta">Conta:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Conta' placeholder='Conta' name='fConta'  required   value='<?php echo ($oContaBancaria) ? $oContaBancaria->getConta() : ""?>'/>
				</div>
			</div>


 <div class="form-group col-md-4 col-sm-6">
					<label for="Tipo">Tipo:</label>
          <div class="input-group col-md-11 col-sm-12">
					<select name='fTipo'  class="form-control select2"  required  >
						<option value=''>Selecione</option>
								   <option  <?php echo (($oContaBancaria) && ($oContaBancaria->getTipo()==0))? " selected " : ""?> value='0'>Conta Corrente</option>
								   <option  <?php echo (($oContaBancaria) && ($oContaBancaria->getTipo()==1))? " selected " : ""?> value='1'>Conta Salario</option>
								   <option  <?php echo (($oContaBancaria) && ($oContaBancaria->getTipo()==2))? " selected " : ""?> value='2'>Conta Poupança</option>
								   <option  <?php echo (($oContaBancaria) && ($oContaBancaria->getTipo()==3))? " selected " : ""?> value='3'>Outros</option>
					</select>
				</div>
			</div>

 <div class="form-group col-md-3  col-sm-6">
					<label for="Banco">Banco:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Banco' placeholder='Banco' name='fBanco'  required   value='<?php echo ($oContaBancaria) ? $oContaBancaria->getBanco() : ""?>'/>
				</div>
			</div>
<?php if($sOP === 'Cadastrar'){?>
 <div class="form-group col-md-3  col-sm-6">
					<label for="Data">Data Início Sistema:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control date" type='text' id='Data' placeholder='Início Movimentação' name='fDataInicio'  required   value=''/>
				</div>
			</div>
 <div class="form-group col-md-3  col-sm-6">
					<label for="Saldo">Saldo Início Sistema:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Valor' placeholder='Saldo Inicial' name='fSaldoInicial'  onkeyup='FormataMoney(this);' required value=''/>
				</div>
			</div>
<?php } else{?>

 <div class="form-group col-md-3  col-sm-12">
					<label for="DataEncerramento">Data Encerramento:</label>
					<div class="input-group col-md-11 col-sm-12">
            <input type='hidden' name='fSaldoInicial' value='<?php echo ($oContaBancaria) ? $oContaBancaria->getSaldoInicialFormatado() : "" ?>'/>
            <input type='hidden' name='fDataInicio'   value='<?php echo ($oContaBancaria) ? $oContaBancaria->getDataInicioFormatado() : "" ?>'/>
					<input class="form-control" type='text' id='DataEncerramento' placeholder='Data Encerramento' name='fDataEncerramento'  value='<?php echo ($oContaBancaria) ? $oContaBancaria->getDataEncerramentoFormatado() : ""?>'/>
				</div>
			</div>
<?php } ?>
 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#DataEncerramento").inputmask("99/99/9999"), { 'placeholder': "99/99/9999" };
				 });
 </script>
 </body>
 </html>
