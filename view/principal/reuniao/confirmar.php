﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oReuniao = $_REQUEST['oReuniao'];
 $oProjeto = $_REQUEST['vProjeto'];
 $oEscritorio = $_REQUEST['oEscritorio'];


 ?>
 <!doctype html>
 <html>
 <head>
 <title>ARQMANAGER - Ata de Reunião</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light sidebar-collapse">
 <div class="wrapper">
 <?php //include_once('includes/header.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <div class="row">
       <div class="col-md-3">
         <img class="logo" src='<?php echo $oEscritorio->getLogomarca();?>' style="width:200px;">
     </div>
     <div class="col-md-6" align="center">
       <h2><?php echo $oEscritorio->getNomeFantasia();?></h2>
   </div>
   <div class="col-md-3"></div>
 </div>
     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <?php include_once('includes/mensagem.php')?>
         <div class="box-header with-border">
           <h3 class="box-title">Ata de Reunião</h3>
         </div>
				 <form method="post" class="form-horizontal" name="formReuniao" action="?action=Reuniao.processaFormulario">
						<input type="hidden" name="sOP" value="AceiteCliente" />
						<input type="hidden" name="fCodReuniao" value="<?=(is_object($oReuniao)) ? $oReuniao->getCodReuniao() : ""?>" />
         <div class="box-body">

 <div class="col-md-4">
 <label for="Projeto" class="control-label">Projeto:</label>
		<p><?php echo ($oProjeto) ? $oProjeto->identificacao  : ""?> - <?php echo ($oReuniao) ? $oReuniao->getCliente()->getNome() : ""?></p>
	</div>

 <div class="col-md-1">
 <label for="DataReuniao" class="control-label">Data / Hora:</label>
		<p><?php echo ($oReuniao) ? $oReuniao->getDataReuniaoFormatado() ." ". $oReuniao->getHoraReuniao() : ""?></p>
	</div>

  <div class="col-md-2">
 <label for="Responsavel" class="control-label">Responsável:</label>
		<p><?php echo ($oReuniao) ? $oReuniao->getResponsavel() : ""?></p>
	</div>

	<div class="col-md-2">
  <label for="Local" class="control-label">Local:</label>
 		<p><?php echo ($oReuniao) ? $oReuniao->getLocal() : ""?></p>
 	</div>
	<div class="col-md-3">
  <label for="Participantes" class="control-label">Participantes:</label>
 		<p><?php echo ($oReuniao) ? $oReuniao->getParticipantes() : ""?></p>
 	</div>
 <div class="col-md-12">
 <label for="Assunto" class="control-label">Assunto:</label>
		<p><?php echo ($oReuniao) ? $oReuniao->getAssunto() : ""?></p>
	</div>

 <div class="col-md-6" style="width: 48%; height: 200px; font-size: 12px; line-height: 14px; border: 1px solid #dddddd; padding: 10px;background:#F2F2F2;overflow:auto;">
 <label for="Decisao" class="control-label">Decisões:</label>
		<?php echo ($oReuniao) ? $oReuniao->getDecisao() : ""?>
	</div>

	<div class="col-md-6" style="width: 48%; height: 200px; font-size: 12px; line-height: 14px; border: 1px solid #dddddd; padding: 10px;background:#F2F2F2;overflow:auto;">
 <label for="Observacao" class="control-label">Observações:</label>
		<?php echo ($oReuniao) ? $oReuniao->getObservacao() : ""?>
	</div>
<div class="col-md-12">
	<hr>
</div>
 <div class="form-group col-md-12">
 <label for="ObsCliente" class="control-label">Observações Cliente:</label>
 		<?php if($oReuniao && $oReuniao->getDeAcordo() && $oReuniao->getObsCliente()){?>
			<p><?php echo ($oReuniao && $oReuniao->getDeAcordo() ) ? $oReuniao->getObsCliente() : ""?></p>
		<?php }else{?>
		<div class="input-group col-md-12">
				<textarea name="fObsCliente" id="obsCliente" class="textarea clean" placeholder="Observações" style="width: 100%; height: 200px; font-size: 14px; line-height: 14px; border: 1px solid #cccccc; padding: 10px;"><?php echo ($oReuniao) ? $oReuniao->getObsCliente() : ""?></textarea>
		</div>
	<?php } ?>
	</div>

	<div class="col-md-5">
  <label for="InseridoPor" class="control-label">Registrado por:</label>
 		<p><?php echo ($oReuniao) ? $oReuniao->getInseridoPor() : ""?></p>
 	</div>
 <div class="col-md-7">
<?php if($oReuniao && $oReuniao->getDeAcordo() && $oReuniao->getDataCliente()){?>
 <label for="DataCliente" class="control-label">Data Resposta:</label>
		<p><?php echo ($oReuniao) ? $oReuniao->getDataClienteFormatado() : ""?></p>
<?php }else{ ?>

	<input type="submit" name="fDeAcordo" onclick="return validateForm(this.value)" value="Concordo" class="btn btn-success">
	<input type="submit"  name="fDeAcordo"  onclick="return validateForm(this.value)"  value="Não Concordo" class="btn btn-danger">
<?php } ?>

</div>

 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <script>
		function validateForm(valor) {
		  var x = document.forms["formReuniao"]["fObsCliente"].value;

		  if (valor == "Não Concordo" && x == "") {
				document.getElementById("obsCliente").focus();
				swal("Informe o motivo!", {
				  buttons: false,
				  timer: 2000,
				});
		    return false;
		  }
		}
</script>

 </div>
 <!-- ./wrapper -->
 </body>
 </html>
