<?php
 $sOP = $_REQUEST['sOP'];
 $oReuniao = $_REQUEST['oReuniao'];
 $voProjeto = $_REQUEST['voProjeto'];

$voCliente = $_REQUEST['voCliente'];

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>ARQMANAGER - Ata de Reunião - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Servico.preparaLista">Gerenciar Atas de Reunião</a>
 			<li class="active">Atas de Reunião - <?php echo $sOP?></li>
 		</ol>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Atas de Reunião - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
<form method="post" class="form-horizontal" name="formReuniao" action="?action=Reuniao.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodReuniao" value="<?=(is_object($oReuniao)) ? $oReuniao->getCodReuniao() : ""?>" />
 				<div  class="box-body" id="form-group">
 								<input type='hidden' name='fCodReuniao' value='<?php echo ($oReuniao) ? $oReuniao->getCodReuniao() : ""?>'/>
				 <div class="form-group col-md-4">
					<label for="Cliente">Cliente:</label>
					<div class="input-group col-md-11">
					<select name='fCodCliente'  class="form-control select2"  required onchange="recuperaConteudoDinamico('index.php','action=Projeto.preparaListaAjax&nIdCliente='+this.value,'selectProjeto')" >
						<option value=''>Selecione</option>
						<?php $sSelected = "";
						   if($voCliente){
							   foreach($voCliente as $oCliente){
								   if($oReuniao){
									   $sSelected = ($oReuniao->getCodCliente() == $oCliente->getCodCliente()) ? "selected" : "";
								   }
						?>
								   <option  <?php echo $sSelected?> value='<?php echo $oCliente->getCodCliente()?>'><?php echo $oCliente->getNome()?></option>
						<?php
							   }
						   }
						?>
					</select>
			</div>
				</div>
 <div class="form-group col-md-4">
					<label for="Projeto">Projeto:</label>
					<div class="input-group col-md-11">
					<select name='fCodProjeto' id="selectProjeto"  class="form-control select2"  required  >
						<option value=''>Selecione</option>
						<?php $sSelected = "";
						   if($voProjeto){
							   foreach($voProjeto as $oProjeto){
								   if($oReuniao){
									   $sSelected = ($oReuniao->getCodProjeto() == $oProjeto->cod_projeto) ? "selected" : "";
								   }
						?>
								   <option  <?php echo $sSelected?> value='<?php echo $oProjeto->cod_projeto?>'><?php echo $oProjeto->nome . " - " . $oProjeto->identificacao?></option>
						<?php
							   }
						   }
						?>
					</select>
			</div>
				</div>

 <div class="form-group col-md-2">
					<label for="DataReuniao">Data:</label>
					<div class="input-group col-md-11">
					<input class="form-control date" type='text' id='DataReuniao' placeholder='Data' name='fDataReuniao'  required   value='<?php echo ($oReuniao) ? $oReuniao->getDataReuniao() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-2">
					<label for="HoraReuniao">Hora:</label>
					<div class="input-group col-md-11">
					<input class="form-control time-min" type='time' id='HoraReuniao' placeholder='Hora' name='fHoraReuniao'  required   value='<?php echo ($oReuniao) ? $oReuniao->getHoraReuniaoFormatado() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="Responsavel">Responsável:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Responsavel' placeholder='Responsável' name='fResponsavel'  required   value='<?php echo ($oReuniao) ? $oReuniao->getResponsavel() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="Participantes">Participantes:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Participantes' placeholder='Participantes' name='fParticipantes'  required   value='<?php echo ($oReuniao) ? $oReuniao->getParticipantes() : ""?>'/>
				</div>
			</div>
 <div class="form-group col-md-4">
					<label for="Local">Local:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Local' placeholder='Local' name='fLocal'  required   value='<?php echo ($oReuniao) ? $oReuniao->getLocal() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-12">
					<label for="Assunto">Assunto:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Assunto' placeholder='Assunto' name='fAssunto'  required   value='<?php echo ($oReuniao) ? $oReuniao->getAssunto() : ""?>'/>
				</div>
			</div>




<div class="form-group col-md-6">
	<label for="Observacao">Decisões/Recomendações:</label>
	<div class="input-group col-md-11">
                <textarea name="fDecisao" class="textarea clean" placeholder="Decisões/Recomendações" style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo ($oReuniao) ? $oReuniao->getDecisao() : ""?></textarea>
            </div>
</div>

<div class="form-group col-md-6">
	<label for="Observacao">Observações:</label>
	<div class="input-group col-md-11">
                <textarea name="fObservacao" class="textarea clean" placeholder="Observações" style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo ($oReuniao) ? $oReuniao->getObservacao() : ""?></textarea>
            </div>
</div>

 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>			           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" >
  $(function () {
    $('.textarea').wysihtml5()
  })
	$(function() {
  var textareas = $('textarea.clean');
  $.each(textareas, function(key, value) {
    $(this).val($(this).val().replace(/[ ]+/g, ' ').replace(/^[ ]+/m, ''));
  })
})
 </script>
 </body>
 </html>
