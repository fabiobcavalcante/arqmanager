﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oReuniao = $_REQUEST['oReuniao'];
 $oProjeto = $_REQUEST['vProjeto'];

 ?>
 <!doctype html>
 <html>
 <head>
 <title>ARQMANAGER - Ata de Reunião - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-red-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1>ARQMANAGER - Principal</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=Reuniao.preparaLista">Gerenciar Ata(s) de Reunião</a>
 			<li class="active">Ata de Reunião - <?php echo $sOP?></li>
 		</ol>	  
 
     </section>
 		
     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Ata de Reunião - Detalhe</h3>
         </div>
         <div class="box-body">
           	
           <div class="box-body">
  
   <div class="col-md-4">
   <label for="Projeto" class="control-label">Projeto:</label>
  		<p><?php echo ($oProjeto) ? $oProjeto->identificacao  : ""?> - <?php echo ($oReuniao) ? $oReuniao->getCliente()->getNome() : ""?></p>
  	</div>
  
   <div class="col-md-1">
   <label for="DataReuniao" class="control-label">Data / Hora:</label>
  		<p><?php echo ($oReuniao) ? $oReuniao->getDataReuniaoFormatado() ." ". $oReuniao->getHoraReuniao() : ""?></p>
  	</div>
  
    <div class="col-md-2">
   <label for="Responsavel" class="control-label">Responsável:</label>
  		<p><?php echo ($oReuniao) ? $oReuniao->getResponsavel() : ""?></p>
  	</div>
  
  	<div class="col-md-2">
    <label for="Local" class="control-label">Local:</label>
   		<p><?php echo ($oReuniao) ? $oReuniao->getLocal() : ""?></p>
   	</div>
  	<div class="col-md-3">
    <label for="Participantes" class="control-label">Participantes:</label>
   		<p><?php echo ($oReuniao) ? $oReuniao->getParticipantes() : ""?></p>
   	</div>
   <div class="col-md-12">
   <label for="Assunto" class="control-label">Assunto:</label>
  		<p><?php echo ($oReuniao) ? $oReuniao->getAssunto() : ""?></p>
  	</div>
  
   <div class="col-md-6" style="width: 48%; height: 200px; font-size: 12px; line-height: 14px; border: 1px solid #dddddd; padding: 10px;background:#F2F2F2;overflow:auto;">
   <label for="Decisao" class="control-label">Decisões:</label>
  		<?php echo ($oReuniao) ? $oReuniao->getDecisao() : ""?>
  	</div>
  
  	<div class="col-md-6" style="width: 48%; height: 200px; font-size: 12px; line-height: 14px; border: 1px solid #dddddd; padding: 10px;background:#F2F2F2;overflow:auto;">
   <label for="Observacao" class="control-label">Observações:</label>
  		<?php echo ($oReuniao) ? $oReuniao->getObservacao() : ""?>
  	</div>
  <div class="col-md-12">
  	<hr>
  </div>
   <div class="form-group col-md-12">
   <label for="ObsCliente" class="control-label">Observações Cliente:</label>
   		<?php if($oReuniao && $oReuniao->getDeAcordo() && $oReuniao->getObsCliente()){?>
  			<p><?php echo ($oReuniao && $oReuniao->getDeAcordo() ) ? $oReuniao->getObsCliente() : ""?></p>
  		<?php }else{?>
  		<div class="input-group col-md-12">
  				<textarea name="fObsCliente" id="obsCliente" class="textarea clean" placeholder="Observações" style="width: 100%; height: 200px; font-size: 14px; line-height: 14px; border: 1px solid #cccccc; padding: 10px;"><?php echo ($oReuniao) ? $oReuniao->getObsCliente() : ""?></textarea>
  		</div>
  	<?php } ?>
  	</div>
  
  	<div class="col-md-5">
    <label for="InseridoPor" class="control-label">Registrado por:</label>
   		<p><?php echo ($oReuniao) ? $oReuniao->getInseridoPor() : ""?></p>
   	</div>
   <div class="col-md-7">
  <?php if($oReuniao && $oReuniao->getDeAcordo() && $oReuniao->getDataCliente()){?>
   <label for="DataCliente" class="control-label">Data Resposta:</label>
  		<p><?php echo ($oReuniao) ? $oReuniao->getDataClienteFormatado() : ""?></p>
  <?php } ?>
  
  </div>


 		 
 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=Reuniao.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>		
 </div>
 <!-- ./wrapper -->
 </body>
 </html>