<?php
 $voReuniao = $_REQUEST['voReuniao'];
  $oPrincipal = $_REQUEST['oPrincipal'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Atas </title>
 <?php include_once('includes/head.php')?>
 
 <style >

/* Aqui a mensagem ja está com o display none para já começar escondida*/
.mensagem{
  display:none;
  position:absolute;
  top: -100%; /* Usei % para que voce entenda que da para se adaptar com o tamanho do botão/container ou qualquer outra coisa a qual a mensagem esta relacionada*/
  left:0;
  right:0;
  margin: auto;
  width:100px;
  height:20px;
  border:1px solid red;

/*Caso queira, por exemplo que fique ao lado 
é so mudar o top para 0 e o left ou o right setar com 100%
  top:0;
  left:100%;
  right:0;
  margin: auto;
  width:100px;
  height:20px;
  border:1px solid red;

*/
}
 </style>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Atas</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <!-- Info boxes -->
     <div class="row">
     <div class="col-md-4 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-teal"><i class="fa fa-plus"></i></span>

         <div class="info-box-content">
           <span class="info-box-text">Total de Atas</span>
             <span class="info-box-number"><?php echo $oPrincipal->total_reuniao?></span>

         </div>
         <!-- /.info-box-content -->
       </div>
       <!-- /.info-box -->
     </div>
     <!-- /.col -->
     <div class="col-md-4 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
         <div class="info-box-content">
           <span class="info-box-text">Atas confirmadas</span>
           <span class="info-box-number">
               <a href="?action=">
             <?php echo $oPrincipal->total_reuniao_confirmadas?></span>
           </a>
         </div>
         <!-- /.info-box-content -->
       </div>
       <!-- /.info-box -->
     </div>
     <!-- /.col -->

     <!-- fix for small devices only -->
     <div class="clearfix visible-sm-block"></div>

     <div class="col-md-4 col-sm-6 col-xs-12">
       <div class="info-box">
         <span class="info-box-icon bg-orange"><i class="fa fa-exclamation-triangle"></i></span>

         <div class="info-box-content">
           <span class="info-box-text">Atas pendentes de confirmação</span>
           <span class="info-box-number">
              <?php echo $oPrincipal->total_reuniao_nao_confirmadas;?>
            </span>
            <small><?php //echo $vRanking[1]?></small>
         </div>
         <!-- /.info-box-content -->
       </div>
       <!-- /.info-box -->
     </div>
     <!-- /.col -->
     <!-- /.col -->
     </div>
     <!-- /.row -->

       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Atas de Reunião</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formReuniao" id="formReuniao" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesReuniao" onChange="JavaScript: submeteForm('Reuniao')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=Reuniao.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar nova Ata de Reunião</option>
   						<option value="?action=Reuniao.preparaFormulario&sOP=Alterar" lang="1">Alterar Ata de Reunião selecionado</option>
   						<option value="?action=Reuniao.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Ata de Reunião selecionado</option>
   						<option value="?action=Reuniao.processaFormulario&sOP=Excluir" lang="2">Excluir Ata de Reunião(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voReuniao)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>

                    <th class="col-md-1">Ações</th>
					<th class="col-md-2">Cliente</th>
					<th class="col-md-2">Projeto</th>
					<th class="col-md-1">Data</th>
					<th class="col-md-2">Responsável</th>
					<th class="col-md-2">Assunto</th>
					<th class="col-md-2">Local</th>
					<th class="col-md-1">De acordo</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voReuniao as $oReuniao){ ?>
   				<tr>
                    <td style="white-space: nowrap;vertical-align: middle">

                        <!-- Single button -->
                        <div class="btn-group">
                            <?php //echo $oReuniao->getCodReuniao()?>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ações<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="?action=Reuniao.preparaFormulario&sOP=Detalhar&nIdReuniao=<?php echo $oReuniao->getCodReuniao()?>"><i class="fa fa-search"></i> Detalhar Ata</a></li>
                                <li><a href="?action=Reuniao.preparaFormulario&sOP=Alterar&nIdReuniao=<?php echo $oReuniao->getCodReuniao()?>"><i class="fa fa-edit"></i> Alterar Ata</a></li>
                                <li><a href="" data-toggle="modal" data-target="#confirm-delete" onclick="modalConfirmDelete(<?php echo $oReuniao->getCodReuniao(); ?>)"><i class="fa fa-close"></i> Excluir Ata</a></li>
                            </ul>
                        </div>

                    </td>
					<td><?php echo $oReuniao->getCodCliente()?></td>
					<td><?php echo $oReuniao->getCodProjeto()?></td>
					<td><?php echo $oReuniao->getDataReuniaoFormatado() ." ". $oReuniao->getHoraReuniao()?></td>
					<td><?php echo $oReuniao->getResponsavel()?></td>
					<td><?php echo $oReuniao->getAssunto()?></td>
					<td><?php echo $oReuniao->getLocal()?></td>
					<td>

						<?php if($oReuniao->getDeAcordo()){
											switch ($oReuniao->getDeAcordo()) {
												case 'S':	$sBotao = "<span class='btn btn-xs bg-red'><i class='fa fa-check' data-toggle='tooltip' data-placement='top' title='Confirmado em {}'></i></span>"; break;
												case 'N':	$sBotao = "<span class='btn btn-xs bg-red'><i class='fa fa-minus' data-toggle='tooltip' data-placement='top' title='Confirmado em {}'></i></span>"; break;
												default :	$sBotao = "<span class='btn btn-xs bg-red'><i class='fa fa-exclamation-triangle' data-toggle='tooltip' data-placement='top' title='Confirmado em {}'></i></span>"; break;
											}
											echo $sBotao;
									}else{ ?>

                    <p style="display:none" id='p<?php echo $oReuniao->getCodReuniao()?>'>https://sistema.arqmanager.com.br/?action=Reuniao.preparaFormulario&sOP=AceiteCliente&nIdReuniao=<?php echo base64_encode($oReuniao->getCodReuniao())?></p>
                    
                  <?php 
                  if($_SESSION['oEscritorio']->getEnvioEmail() ===' S'){?>
										<a href="?action=Reuniao.processaFormulario&sOP=EnviarEmail&nIdReuniao=<?php echo base64_encode($oReuniao->getCodReuniao())?>"><span class='btn btn-sm bg-blue'><i class='fa fa-envelope' data-toggle='tooltip' data-placement='top' title='Solicitar confirmação ao cliente'></i></span></a>
                  <?php } ?>
                    <span class='btn btn-sm bg-blue'><i class='fa fa-copy' onmouseover="showMessage()" onmouseout="hideMessage()" onclick='copyToClipboard("#p<?php echo $oReuniao->getCodReuniao()?>")' data-toggle='tooltip' data-placement='top' title='Copiar Link para envio ao Cliente'></i></span>
<?php							}
							?>
					</td>
					<!-- <td><?php //echo $oReuniao->getDataClienteFormatado()?></td> -->

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
							 <th>Ações</th>
			 					<th>Cliente</th>
			 					<th>Projeto</th>
			 					<th>Data</th>
			 					<th>Responsável</th>
			 					<th>Assunto</th>
			 					<th>Local</th>
			 					<th>Status</th>
                 </tr>
                 </tfoot>
               </table>
                <br><br><br><br>
  			<?php }//if(count($voReuniao)){?>
  			</form>
             </div>
             <div class="mensagem" >Olá mundo</div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script>

 function copyToClipboard(element) {
	  var $temp = $("<input>");
	  $("body").append($temp);
	  $temp.val($(element).text()).select();
	  document.execCommand("copy");
	  $temp.remove();

}


let mensagem = document.querySelector(".mensagem") ;

// mostra a mensagem
function showMessage(){   
   mensagem.style.display = "block";   
 }
// esconde a mensagem
function hideMessage(){
  mensagem.style.display = "none"; 
}
  </script>
 </body>
 </html>
