<?php
 $sOP = $_REQUEST['sOP'];
 $oFormaPagamento = $_REQUEST['oFormaPagamento'];
 
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> ARQUITETURA - Forma de pagamento - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
 
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> ARQUITETURA - Admin</h1>
 		<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li><a href="?action=FormaPagamento.preparaLista">Gerenciar Formas de pagamento</a>
 			<li class="active">Forma de pagamento - <?php echo $sOP?></li>
 		</ol>	  
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Forma de pagamento - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formFormaPagamento" action="?action=FormaPagamento.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodFormaPagamento" value="<?php echo (is_object($oFormaPagamento)) ? $oFormaPagamento->getCodFormaPagamento() : ""?>" />
 				<div  class="box-body" id="form-group">
				
 <div class="form-group col-md-4 col-sm-12">
					<label for="Descricao">Descrição:</label>
					<div class="input-group col-md-11 col-sm-12">
					<input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'  required   value='<?php echo ($oFormaPagamento) ? $oFormaPagamento->getDescricao() : ""?>'/>
				</div>
			</div>
				
 
 				
 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?php echo $sOP?></button>
 						</div>
 					</div>				
 				</div>
 				<!-- /.box-body -->
 			  </form>			
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 
 </body>
 </html>