<?php
 $voFormaPagamento = $_REQUEST['voFormaPagamento'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Forma de Pagamento </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?> - Formas de Pagamento</h1>
 	  	<ol class="breadcrumb">
 			<li><a href="?"><i class="fa fa-dashboard"></i> PRINCIPAL</a></li>
 			<li class="active">Gerenciar Formas de Pagamentos</li>
 		</ol>

 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Forma de Pagamento</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formFormaPagamento" id="formFormaPagamento" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesFormaPagamento" onChange="JavaScript: submeteForm('FormaPagamento')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=FormaPagamento.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Forma de Pagamento</option>
   						<option value="?action=FormaPagamento.preparaFormulario&sOP=Alterar" lang="1">Alterar Forma de Pagamento selecionado</option>
   						<option value="?action=FormaPagamento.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Forma de Pagamento selecionado</option>
   						<option value="?action=FormaPagamento.processaFormulario&sOP=Excluir" lang="2">Excluir Forma de Pagamento(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voFormaPagamento)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('FormaPagamento')"><i class="icon fa fa-check"></a></th>
   					<th class='Titulo'>Código</th>
					<th class='Titulo'>Descrição</th>


   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voFormaPagamento as $oFormaPagamento){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('FormaPagamento')" type="checkbox" value="<?=$oFormaPagamento->getCodFormaPagamento()?>" name="fIdFormaPagamento[]"/></td>
  					<td><?php echo $oFormaPagamento->getCodFormaPagamento()?></td>
					<td><?php echo $oFormaPagamento->getDescricao()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th class='Titulo'>Código</th>
					<th class='Titulo'>Descrição</th>


                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voFormaPagamento)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
