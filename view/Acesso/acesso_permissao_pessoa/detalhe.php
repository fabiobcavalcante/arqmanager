﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoPermissaoPessoa = $_REQUEST['oAcessoPermissaoPessoa'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Permissão por usuário - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Acesso</h1>
 	  	<ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li><a href="?action=AcessoPermissaoPessoa.preparaLista">Permissão por usuário</a></li>
 		<li class="active">Detalhar</li>
 	 </ol>
 
 	  <?php //include_once('includes/breadcrumb.php')?>
     </section>
 		
     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Permissão por usuário - Detalhe</h3>
         </div>
         <div class="box-body">
           	
 <div class="col-md-4">
		
 <label for="CodPessoa" class="control-label">Cod_pessoa:</label>
		<p><?php echo ($oAcessoPermissaoPessoa) ? $oAcessoPermissaoPessoa->getCodPessoa() : ""?></p>
	</div>
	
 <div class="col-md-4">
		
 <label for="CodTransacaoModulo" class="control-label">Cod_transacao_modulo:</label>
		<p><?php echo ($oAcessoPermissaoPessoa) ? $oAcessoPermissaoPessoa->getCodTransacaoModulo() : ""?></p>
	</div>

 		 
 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=AcessoPermissaoPessoa.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>		
 </div>
 <!-- ./wrapper -->
 </body>
 </html>