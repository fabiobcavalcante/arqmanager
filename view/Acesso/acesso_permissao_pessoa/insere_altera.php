<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoPermissaoPessoa = $_REQUEST['oAcessoPermissaoPessoa'];
 $voAcessoUsuario = $_REQUEST['voAcessoUsuario'];

$voAcessoTransacaoModulo = $_REQUEST['voAcessoTransacaoModulo'];


 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Permissão por usuário - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li><a href="?action=AcessoPermissaoPessoa.preparaLista">Permissão por usuário</a></li>
 		<li class="active"><?php echo $sOP?></li>
 	 </ol>
 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Permissão por usuário - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formAcessoPermissaoPessoa" action="?action=AcessoPermissaoPessoa.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodPessoa" value="<?=(is_object($oAcessoPermissaoPessoa)) ? $oAcessoPermissaoPessoa->getCodPessoa() : ""?>" />
			<input type="hidden" name="fCodTransacaoModulo" value="<?=(is_object($oAcessoPermissaoPessoa)) ? $oAcessoPermissaoPessoa->getCodTransacaoModulo() : ""?>" />
 				<div  class="box-body" id="form-group">

 <div class="form-group col-md-4">
					<label for="AcessoUsuario">Cod_pessoa:</label>
					<div class="input-group col-md-11">
					<select name='fCodPessoa'  class="form-control select2"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voAcessoUsuario){
							   foreach($voAcessoUsuario as $oAcessoUsuario){
								   if($oAcessoPermissaoPessoa){
									   $sSelected = ($oAcessoPermissaoPessoa->getCodPessoa() == $oAcessoUsuario->getCodUsuario()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oAcessoUsuario->getCodUsuario()?>'><?= $oAcessoUsuario->getCodUsuario()?></option>
						<?
							   }
						   }
						?>
					</select>
			</div>
				</div>

 <div class="form-group col-md-4">
					<label for="AcessoTransacaoModulo">Cod_transacao_modulo:</label>
					<div class="input-group col-md-11">
					<select name='fCodTransacaoModulo'  class="form-control select2"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voAcessoTransacaoModulo){
							   foreach($voAcessoTransacaoModulo as $oAcessoTransacaoModulo){
								   if($oAcessoPermissaoPessoa){
									   $sSelected = ($oAcessoPermissaoPessoa->getCodTransacaoModulo() == $oAcessoTransacaoModulo->getCodTransacaoModulo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oAcessoTransacaoModulo->getCodTransacaoModulo()?>'><?= $oAcessoTransacaoModulo->getCodTransacaoModulo()?></option>
						<?
							   }
						   }
						?>
					</select>
			</div>
				</div>


 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?=$sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 </script>
 </body>
 </html>
