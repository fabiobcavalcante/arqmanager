<?php
 $voAcessoPermissaoPessoa = $_REQUEST['voAcessoPermissaoPessoa'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Permissão por usuário </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li class="active">Gerenciar Permissão por usuário</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Permissão por usuário</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formAcessoPermissaoPessoa" id="formAcessoPermissaoPessoa" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesAcessoPermissaoPessoa" onChange="JavaScript: submeteForm('AcessoPermissaoPessoa')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=AcessoPermissaoPessoa.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Permissão por usuário</option>
   						<option value="?action=AcessoPermissaoPessoa.preparaFormulario&sOP=Alterar" lang="1">Alterar Permissão por usuário selecionado</option>
   						<option value="?action=AcessoPermissaoPessoa.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Permissão por usuário selecionado</option>
   						<option value="?action=AcessoPermissaoPessoa.processaFormulario&sOP=Excluir" lang="2">Excluir Permissão por usuário(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voAcessoPermissaoPessoa)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('AcessoPermissaoPessoa')"><i class="icon fa fa-check"></a></th>
   					<th class='Titulo'>CodPessoa</th>
					<th class='Titulo'>CodTransacaoModulo</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voAcessoPermissaoPessoa as $oAcessoPermissaoPessoa){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('AcessoPermissaoPessoa')" type="checkbox" value="<?=$oAcessoPermissaoPessoa->getCodPessoa()?>||<?=$oAcessoPermissaoPessoa->getCodTransacaoModulo()?>" name="fIdAcessoPermissaoPessoa[]"/></td>
  					<td><?= $oAcessoPermissaoPessoa->getCodPessoa()?></td>
					<td><?= $oAcessoPermissaoPessoa->getCodTransacaoModulo()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th class='Titulo'>CodPessoa</th>
					<th class='Titulo'>CodTransacaoModulo</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voAcessoPermissaoPessoa)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
