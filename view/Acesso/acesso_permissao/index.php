<?php
 $voAcessoPermissao = $_REQUEST['voAcessoPermissao'];
$voAcessoModulo = $_REQUEST['voAcessoModulo'];
$oGrupoUsuario = $_REQUEST['oGrupoUsuario'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Permissão </title>
 <?php include_once('includes/head.php');?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php');?>
 <?php include_once('includes/menu.php');?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
              <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li class="active">Gerenciar Permissão</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Permissão - <?php echo $oGrupoUsuario->getDescricao()?></h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			            <style type="text/css">
                .list-group li {
                    list-style:none;
                    padding-bottom:3px;
                    padding-left:1em;
                }
            </style>
            <form method="post" action="index.php?action=AcessoPermissao.processaFormulario" name="formAcessoPermissao" id="formAcessoPermissao" class="formulario">
                <input type="hidden" name="fCodGrupoUsuario" value="<?php echo $oGrupoUsuario->getCodGrupoUsuario()?>">
                <input type="hidden" name="sOP" value="Alterar">
                <?php foreach($voAcessoModulo as $oModulo){?>
                <div id="row">
                    <div class="list-group">
                        <a href="#" onclick="MostraEsconde2('transacao_<?php echo $oModulo->getCodModulo()?>');" class="list-group-item"><?php echo $oModulo->getDescricao()?></a>
                        <div style="display:none" id="transacao_<?php echo $oModulo->getCodModulo()?>">

                            <?php if($oModulo->getTransacaoModulo()) {?>
                                <?php foreach($oModulo->getTransacaoModulo() as $oTransacaoModulo) { ?>
                                    <li><input type="checkbox" value="<?php echo $oTransacaoModulo->getCodTransacaoModulo()?>" name="fCodTransacaoModulo[]" <?php echo($oGrupoUsuario->temPermissao($oTransacaoModulo->getCodTransacaoModulo())) ? "checked" : ""?>/><?php echo $oTransacaoModulo->getDescricao()?></li>
                                <?php  		}
                                ?>

                            <?php } ?>
                        </div>
                     <?php } ?>



                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-2">
                        <button type="submit" class="btn btn-primary" >Alterar</button>
                    </div>
                </div>
                <br/>
            </form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
