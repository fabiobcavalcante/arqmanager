﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoPermissao = $_REQUEST['oAcessoPermissao'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Permissão - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li><a href="?action=AcessoPermissao.preparaLista">Permissão</a></li>
 		<li class="active">Detalhar</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Permissão - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="CodTransacaoModulo" class="control-label">Codigo:</label>
		<p><?php echo ($oAcessoPermissao) ? $oAcessoPermissao->getCodTransacaoModulo() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="CodGrupoUsuario" class="control-label">Grupo de Usuário:</label>
		<p><?php echo ($oAcessoPermissao) ? $oAcessoPermissao->getCodGrupoUsuario() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=AcessoPermissao.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
