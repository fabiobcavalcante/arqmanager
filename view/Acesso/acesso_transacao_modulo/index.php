<?php

 $oModulo = $_REQUEST['oAcessoModulo'];
$voAcessoTransacaoModulo = $oModulo->getTransacaoModulo();

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Transação Módulo </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
        <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	  <ol class="breadcrumb">
			<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
            <li><a href="?action=AcessoModulo.preparaLista">Módulos</a></li>
              <li class="active"><a href="?action=AcessoTransacaoModulo.preparaLista&nCodModulo=<?php echo $oModulo->getCodModulo()?>">Gerenciar Transação do Módulo [<?php echo $oModulo->getDescricao()?>]</a></li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Transações do Módulo [<?php echo $oModulo->getDescricao()?>]</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formAcessoTransacaoModulo" id="formAcessoTransacaoModulo" class="formulario">
                <input type="hidden" name="nCodModulo" value="<?php echo $oModulo->getCodModulo()?>">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesAcessoTransacaoModulo" onChange="JavaScript: submeteForm('AcessoTransacaoModulo')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=AcessoTransacaoModulo.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Transação Modulo</option>
   						<option value="?action=AcessoTransacaoModulo.preparaFormulario&sOP=Alterar" lang="1">Alterar Transação Modulo selecionado</option>
   						<option value="?action=AcessoResponsavelTransacao.preparaLista" lang="1">Gerenciar métodos responsáveis pela transacao selecionada</option>
   						<option value="?action=AcessoTransacaoModulo.processaFormulario&sOP=Excluir" lang="2">Excluir Transação Modulo(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voAcessoTransacaoModulo)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('AcessoTransacaoModulo')"><i class="icon fa fa-check"></a></th>
   					<th>Código</th>
					<th>Descrição</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voAcessoTransacaoModulo as $oAcessoTransacaoModulo){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('AcessoTransacaoModulo')" type="checkbox" value="<?=$oAcessoTransacaoModulo->getCodTransacaoModulo()?>" name="fIdAcessoTransacaoModulo[]"/></td>
  					<td><?php echo $oAcessoTransacaoModulo->getCodTransacaoModulo()?></td>
					<td><?php echo $oAcessoTransacaoModulo->getDescricao()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th class='Titulo'>Código</th>
					<th class='Titulo'>Descrição</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voAcessoTransacaoModulo)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
