<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoUsuario = $_REQUEST['oAcessoUsuario'];
 $voAcessoGrupoUsuario = $_REQUEST['voAcessoGrupoUsuario'];


 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Usuário - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Acesso</h1>
 	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li><a href="?action=AcessoUsuario.preparaLista">Usuário</a></li>
 		<li class="active"><?php echo $sOP?></li>
 	 </ol>
 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Usuário - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formAcessoUsuario" action="?action=AcessoUsuario.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
 				<input type="hidden" name="fCodUsuario" value="<?=(is_object($oAcessoUsuario)) ? $oAcessoUsuario->getCodUsuario() : ""?>" />
 				<div  class="box-body" id="form-group">
 									<input type='hidden' name='fCodUsuario' value='<?= ($oAcessoUsuario) ? $oAcessoUsuario->getCodUsuario() : ""?>'/>

 <div class="form-group col-md-4">
					<label for="AcessoGrupoUsuario">Código Grupo Usuário:</label>
					<div class="input-group col-md-11">
					<select name='fCodGrupoUsuario'  class="form-control select2"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voAcessoGrupoUsuario){
							   foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){
								   if($oAcessoUsuario){
									   $sSelected = ($oAcessoUsuario->getCodGrupoUsuario() == $oAcessoGrupoUsuario->getCodGrupoUsuario()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?>'><?= $oAcessoGrupoUsuario->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
			</div>
				</div>

 <div class="form-group col-md-4">
					<label for="Login">Login:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Login' placeholder='Login' name='fLogin'  required   value='<?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getLogin() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="Senha">Senha:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Senha' placeholder='Senha' name='fSenha'  required   value='<?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getSenha() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="InseridoPor">Inserido por:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='InseridoPor' placeholder='Inserido por' name='fInseridoPor'  required   value='<?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getInseridoPor() : ""?>'/>
				</div>
			</div>

 <div class="form-group col-md-4">
					<label for="AlteradoPor">Alterado por:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='AlteradoPor' placeholder='Alterado por' name='fAlteradoPor'  required   value='<?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getAlteradoPor() : ""?>'/>
				</div>
			</div>
					<input type='hidden' name='fAtivo' value='<?= ($oAcessoUsuario) ? $oAcessoUsuario->getAtivo() : "1"?>'/>


 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?=$sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 </script>
 </body>
 </html>
