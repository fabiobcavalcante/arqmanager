﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoUsuario = $_REQUEST['oAcessoUsuario'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Usuário - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Acesso</h1>
 	  	<ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li><a href="?action=AcessoUsuario.preparaLista">Usuário</a></li>
 		<li class="active">Detalhar</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Usuário - Detalhe</h3>
         </div>
         <div class="box-body">
                       <!-- Content Header (Page header) -->
                       <header class="content-header container-fluid">
                           <div class="row">
                               <div class="col-lg-4">
                                   <h1 class="content-max-width">Main Header Component</h1>
                               </div>
                           </div>
                       </header>

                       <!-- Main content -->
                       <div class="content container-fluid">
                           <div class="row">
                               <div class="col-lg-4">
                                   <section class="content-max-width">
                                               <section id="main-header">
                   <div class="callout callout-warning">
                       <h4 class="call-warning">Reminder!</h4>
                       <p>
                           AdminLTE uses all of Bootstrap 3 components. It's a good start to review
                           the <a href="http://getbootstrap.com">Bootstrap documentation</a> to get an idea of the various
                           components that this documentation <b>does not</b> cover.
                       </p>
                   </div>
                   <div class="callout callout-info">
                       <h4>Tip!</h4>
                       <p>
                           If you go through the example pages and would like to copy a component, right-click on
                           the component and choose "inspect element" to get to the HTML quicker than scanning
                           the HTML page.
                       </p>
                   </div>
               </section>
                                   </section>
                               </div>

                           </div>
                       </div>
                       <!-- /.content -->








<div class="col-md-6">
 <div class="col-md-3">

 <label for="CodGrupoUsuario" class="control-label">Grupo Usuário:</label>
		<p><?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getAcessoGrupoUsuario()->getDescricao() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="Login" class="control-label">Login:</label>
		<p><?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getLogin() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="InseridoPor" class="control-label">Inserido por:</label>
		<p><?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getInseridoPor() : ""?></p>
	</div>

 <div class="col-md-3">

 <label for="AlteradoPor" class="control-label">Alterado por:</label>
		<p><?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getAlteradoPor() : ""?></p>
	</div>
</div>
<div class="col-md-6">
</div>
 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=AcessoUsuario.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
