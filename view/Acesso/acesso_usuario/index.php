<?php
 $voAcessoUsuario = $_REQUEST['voAcessoUsuario'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Lista de Usuário </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
 
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Acesso</h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li class="active">Gerenciar Usuário</li>
 	 </ol>
 
 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Usuário</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formAcessoUsuario" id="formAcessoUsuario" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesAcessoUsuario" onChange="JavaScript: submeteForm('AcessoUsuario')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=AcessoUsuario.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Usuário</option>
   						<option value="?action=AcessoUsuario.preparaFormulario&sOP=Alterar" lang="1">Alterar Usuário selecionado</option>
   						<option value="?action=AcessoUsuario.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Usuário selecionado</option>
   						<option value="?action=AcessoUsuario.processaFormulario&sOP=Excluir" lang="2">Excluir Usuário(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voAcessoUsuario)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('AcessoUsuario')"><i class="icon fa fa-check"></a></th>
   					<th class='Titulo'>Código</th>
					<th class='Titulo'>Grupo</th>
					<th class='Titulo'>Login</th>


   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voAcessoUsuario as $oAcessoUsuario){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('AcessoUsuario')" type="checkbox" value="<?=$oAcessoUsuario->getCodUsuario()?>" name="fIdAcessoUsuario[]"/></td>
  					<td><?= $oAcessoUsuario->getCodUsuario()?></td>
					<td><?= $oAcessoUsuario->getCodGrupoUsuario()?></td>
					<td><?= $oAcessoUsuario->getLogin()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th class='Titulo'></th>
					<th class='Titulo'></th>
					<th class='Titulo'></th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voAcessoUsuario)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>