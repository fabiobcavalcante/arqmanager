﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoUsuario = $_REQUEST['oAcessoUsuario'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Usuário - Detalhe</title>
 <?php include_once('includes/head.php')?>
 <style type="text/css">
 .responsive {
   width: 100%;
   max-width: 150px;
   height: auto;
 }
 </style>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo $_SESSION['oEscritorio']->getNomeFantasia(); ?> - Acesso</h1>
 	  	<ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li><a href="?action=AcessoUsuario.preparaLista">Usuário</a></li>
 		<li class="active">Detalhar</li>
 	 </ol>
 	  <?php //include_once('includes/breadcrumb.php')?>
    <?php include_once('includes/mensagem.php')?>
     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Usuário - Detalhe</h3>
         </div>
         <div class="box-body">

<div class="col-md-6">
  <div class="box-header with-border">
    <h4 class="box-title">Dados</h4>
  </div>
  <div class="col-md-6">

  <label for="CodGrupoUsuario" class="control-label">Grupo Usuário:</label>
 		<p><?php echo ($oAcessoUsuario &&  $oAcessoUsuario->getAcessoGrupoUsuario()) ? $oAcessoUsuario->getAcessoGrupoUsuario()->getDescricao() : ""?></p>
 	</div>

  <div class="col-md-6">

  <label for="Login" class="control-label">Login:</label>
 		<p><?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getLogin() : ""?></p>
 	</div>

  <div class="col-md-6">

  <label for="InseridoPor" class="control-label">Inserido:</label>
 		<p><?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getInseridoPor() : ""?></p>
 	</div>

  <div class="col-md-6">

  <label for="AlteradoPor" class="control-label">Alterado:</label>
 		<p><?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getAlteradoPor() : ""?></p>
 	</div>

</div>
<div class="col-md-6">
  <div class="box-header with-border">
    <h4 class="box-title">Foto</h4>
  </div>
<div class="form-group col-sm-12">
    <div class="box-body no-padding" align="center">
        <img class="responsive" src="<?php echo ($oAcessoUsuario && $oAcessoUsuario->getFoto()) ? "view" . DIRECTORY_SEPARATOR . "Acesso" . DIRECTORY_SEPARATOR."acesso_usuario" . DIRECTORY_SEPARATOR."foto".DIRECTORY_SEPARATOR.$oAcessoUsuario->getFoto() : "dist/img/no_avatar.jpg"?>" alt="User Image">
    </div>
                  <!-- /.box-footer -->

</div>
  <div class="form-group col-md-12">
    <div class="col-sm-offset-5 col-sm-2"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
              <i class="fa fa-picture-o" aria-hidden="true"></i> Alterar Foto </button></div>
  </div>
</div>

 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=AcessoUsuario.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->


   <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Escolher Foto para Perfil</h4>
              </div>
              <div class="modal-body">
                <form action="?action=AcessoUsuario.processaFormulario" enctype="multipart/form-data" name="frmUpload" method="post">
                  <input type="hidden" name="sOP" value="AlterarFoto">
                  <input type="hidden" name="fCodUsuario" value="<?php echo ($oAcessoUsuario) ? $oAcessoUsuario->getCodUsuario() : ""?>">
                  <input type="file" name="fFoto">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Fechar</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
