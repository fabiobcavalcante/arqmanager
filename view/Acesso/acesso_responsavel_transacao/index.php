<?php
$oAcessoTransacaoModulo = $_REQUEST['oAcessoTransacaoModulo'];
 $voAcessoResponsavelTransacao = $oAcessoTransacaoModulo->getResponsavelTransacao();
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Responsável Transação </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		    <li><a href="?action=AcessoModulo.preparaLista">Módulos</a></li>
			<li><a href="?action=AcessoTransacaoModulo.preparaLista&nCodModulo=<?php echo $oAcessoTransacaoModulo->getCodModulo()?>">Gerenciar Transação do Módulo [<?php echo $oAcessoTransacaoModulo->getAcessoModulo()->getDescricao()?>]</a></li>
			<li class="active">Gerenciar Responsável Transação</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Responsável Transação [<?php echo $oAcessoTransacaoModulo->getAcessoModulo()->getDescricao()?>]</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formAcessoResponsavelTransacao" id="formAcessoResponsavelTransacao" class="formulario">
			<input type="hidden" name="nCodTransacaoModulo" value="<?php echo $oAcessoTransacaoModulo->getCodTransacaoModulo()?>">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesAcessoResponsavelTransacao" onChange="JavaScript: submeteForm('AcessoResponsavelTransacao')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=AcessoResponsavelTransacao.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Responsável Transação</option>
   						<option value="?action=AcessoResponsavelTransacao.preparaFormulario&sOP=Alterar" lang="1">Alterar Responsável Transação selecionado</option>
   						<option value="?action=AcessoResponsavelTransacao.processaFormulario&sOP=Excluir" lang="2">Excluir Responsável Transação(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voAcessoResponsavelTransacao)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('AcessoResponsavelTransacao')"><i class="icon fa fa-check"></a></th>
   					<th class='Titulo'>Código</th>
					<th class='Titulo'>Método</th>
					<th class='Titulo'>Operação</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voAcessoResponsavelTransacao as $oAcessoResponsavelTransacao){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('AcessoResponsavelTransacao')" type="checkbox" value="<?php echo $oAcessoResponsavelTransacao->getCodResponsavelTransacao()?>" name="fIdAcessoResponsavelTransacao[]"/></td>
  					<td><?php echo  $oAcessoResponsavelTransacao->getCodResponsavelTransacao()?></td>
					<td><?php echo  $oAcessoResponsavelTransacao->getResponsavel()?></td>
					<td><?php echo  $oAcessoResponsavelTransacao->getOperacao()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th class='Titulo'>Código</th>
					<th class='Titulo'>Método</th>
					<th class='Titulo'>Operação</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voAcessoResponsavelTransacao)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
