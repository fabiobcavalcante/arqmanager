<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoResponsavelTransacao = $_REQUEST['oAcessoResponsavelTransacao'];
 if($oAcessoResponsavelTransacao){
	 $oAcessoTransacaoModulo = $oAcessoResponsavelTransacao->getAcessoTransacaoModulo();
}else{
	 $oAcessoTransacaoModulo = $_REQUEST['oAcessoTransacaoModulo'];
}

$oAcessoModulo = $oAcessoTransacaoModulo->getAcessoModulo();

 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Responsável Transação - <?php echo $sOP ?></title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
    <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li><a href="?action=AcessoModulo.preparaLista">Módulos</a></li>
		<li><a href="?action=AcessoTransacaoModulo.preparaLista&nCodModulo=<?php echo $oAcessoModulo->getCodModulo()?>">Gerenciar Transação do Módulo [<?php echo $oAcessoModulo->getDescricao()?>]</a></li>
		<li><a href="?action=AcessoResponsavelTransacao.preparaLista&nCodTransacaoModulo=<?php echo $oAcessoTransacaoModulo->getCodTransacaoModulo()?>">Gerenciar Responsáveis/Transação</a></li>
		<li class="active"><?php echo $sOP?></li>

 	 </ol>
 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
    <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Responsável Transação - <?php echo $sOP ?></h3>
             </div>
             <!-- /.box-header -->
 			  <form method="post" class="form-horizontal" name="formAcessoResponsavelTransacao" action="?action=AcessoResponsavelTransacao.processaFormulario">
 				<input type="hidden" name="sOP" value="<?php echo $sOP?>" />
				<input type="hidden" name='fCodTransacaoModulo' value="<?php echo (is_object($oAcessoResponsavelTransacao)) ? $oAcessoResponsavelTransacao->getCodTransacaoModulo() : $oAcessoTransacaoModulo->getCodTransacaoModulo()?>">
 				<input type="hidden" name="fCodResponsavelTransacao" value="<?php echo(is_object($oAcessoResponsavelTransacao)) ? $oAcessoResponsavelTransacao->getCodResponsavelTransacao() : ""?>" />
 				<div  class="box-body" id="form-group">
				<div class="form-group col-md-4">
					<label for="Responsavel">Responsável:</label>
					<div class="input-group col-md-11">
						<input class="form-control" type='text' id='Responsavel' placeholder='Responsável' name='fResponsavel'  required   value='<?php echo ($oAcessoResponsavelTransacao) ? $oAcessoResponsavelTransacao->getResponsavel() : ""?>'/>
					</div>
				</div>

 <div class="form-group col-md-4">
					<label for="Operacao">Operação:</label>
					<div class="input-group col-md-11">
					<input class="form-control" type='text' id='Operacao' placeholder='Operação' name='fOperacao'  required   value='<?php echo ($oAcessoResponsavelTransacao) ? $oAcessoResponsavelTransacao->getOperacao() : ""?>'/>
				</div>
			</div>


 					<div class="form-group col-md-12">
 						<div class="col-sm-offset-5 col-sm-2">
 							<button type="submit" class="btn btn-lg btn-success" ><?=$sOP?></button>
 						</div>
 					</div>
 				</div>
 				<!-- /.box-body -->
 			  </form>
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 <script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 </script>
 </body>
 </html>
