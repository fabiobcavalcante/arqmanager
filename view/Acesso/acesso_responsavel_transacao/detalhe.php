﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoResponsavelTransacao = $_REQUEST['oAcessoResponsavelTransacao'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Responsável Transação - Detalhe</title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	<ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li><a href="?action=AcessoResponsavelTransacao.preparaLista">Responsável Transação</a></li>
 		<li class="active">Detalhar</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
     </section>

     <!-- Main content -->
     <section class="content">
       <!-- Default box -->
       <div class="box">
         <div class="box-header with-border">
           <h3 class="box-title">Responsável Transação - Detalhe</h3>
         </div>
         <div class="box-body">

 <div class="col-md-4">

 <label for="CodTransacaoModulo" class="control-label">Código Transação:</label>
		<p><?php echo ($oAcessoResponsavelTransacao) ? $oAcessoResponsavelTransacao->getCodTransacaoModulo() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Responsavel" class="control-label">Responsável:</label>
		<p><?php echo ($oAcessoResponsavelTransacao) ? $oAcessoResponsavelTransacao->getResponsavel() : ""?></p>
	</div>

 <div class="col-md-4">

 <label for="Operacao" class="control-label">Operação:</label>
		<p><?php echo ($oAcessoResponsavelTransacao) ? $oAcessoResponsavelTransacao->getOperacao() : ""?></p>
	</div>


 			<div class="form-group col-md-12">
 				<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-lg btn-primary" href="?action=AcessoResponsavelTransacao.preparaLista">Voltar</a></div>
 			</div>
 		</div>
         <!-- /.box-body -->
         </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <?php include_once('includes/javascript.php')?>
 <?php include_once('includes/mensagem.php')?>
 </div>
 <!-- ./wrapper -->
 </body>
 </html>
