<?php
 $voAcessoModulo = $_REQUEST['voAcessoModulo'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Módulos </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li class="active">Gerenciar Módulos</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Módulo</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formAcessoModulo" id="formAcessoModulo" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesAcessoModulo" onChange="JavaScript: submeteForm('AcessoModulo')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=AcessoModulo.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Módulo</option>
   						<option value="?action=AcessoModulo.preparaFormulario&sOP=Alterar" lang="1">Alterar Módulo selecionado</option>
   						<option value="?action=AcessoTransacaoModulo.preparaLista" lang="1">Gerenciar Transações do Módulo selecionado</option>
   						<option value="?action=AcessoModulo.processaFormulario&sOP=Excluir" lang="2">Excluir Módulo(s) selecionado(s)</option>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voAcessoModulo)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('AcessoModulo')"><i class="icon fa fa-check"></a></th>
   					<th class='Titulo'>Módulo</th>
					<th class='Titulo'>Descrição</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voAcessoModulo as $oAcessoModulo){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('AcessoModulo')" type="checkbox" value="<?=$oAcessoModulo->getCodModulo()?>" name="fIdAcessoModulo[]"/></td>
  					<td><?= $oAcessoModulo->getCodModulo()?></td>
					<td><?= $oAcessoModulo->getDescricao()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th>#</th>
                   <th class='Titulo'>Módulo</th>
					<th class='Titulo'>Descrição</th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voAcessoModulo)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
