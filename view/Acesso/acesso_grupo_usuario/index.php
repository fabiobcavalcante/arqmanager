<?php
 $voAcessoGrupoUsuario = $_REQUEST['voAcessoGrupoUsuario'];
 ?>
 <!doctype html>
 <html lang="pt-br">
 <head>
 <title>Lista de Grupo de Usuários </title>
 <?php include_once('includes/head.php')?>
 </head>
 <body class="sidebar-mini wysihtml5-supported skin-green-light">
 <div class="wrapper">
 <?php include_once('includes/header.php')?>
 <?php include_once('includes/menu.php')?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1><?php echo  $_SESSION['oEscritorio']->getNomeFantasia()?></h1>
 	  	  <ol class="breadcrumb">
 		<li><a href="?"><i class="fa fa-dashboard"></i> Início</a></li>
 		<li class="active">Gerenciar Grupo de Usuários</li>
 	 </ol>

 	  <?php //include_once('includes/breadcrumb.php')?>
 	  <?php include_once('includes/mensagem.php')?>
     </section>
     <!-- Main content -->
     <section class="content">
       <div class="row">
         <div class="col-xs-12">
              <div class="box">
             <div class="box-header">
               <h3 class="box-title">Grupo de Usuários</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body table-responsive">
 			<form method="post" action="" name="formAcessoGrupoUsuario" id="formAcessoGrupoUsuario" class="formulario">
 			<div class='form-group col-md-4'>
 				<select class="form-control Acoes" name="acoes" id="acoesAcessoGrupoUsuario" onChange="JavaScript: submeteForm('AcessoGrupoUsuario')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=AcessoGrupoUsuario.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Grupo</option>
   						<option value="?action=AcessoGrupoUsuario.preparaFormulario&sOP=Alterar" lang="1">Alterar Grupo selecionado</option>
   						<option value="?action=AcessoGrupoUsuario.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Grupo selecionado</option>
          <?php if($_SESSION['oUsuarioAM'] ==1 || $_SESSION['oUsuarioAM'] ==2){?>
						<option value="?action=AcessoPermissao.preparaLista" lang="1">Gerenciar Permissões do grupo selecionado</option>
   						<option value="?action=AcessoGrupoUsuario.processaFormulario&sOP=Excluir" lang="2">Excluir Grupo(s) selecionado(s)</option>
		  <?php }?>
   				</select>
 			</div>
 			<div class='row'></div>
 			<?php if(is_array($voAcessoGrupoUsuario)){?>
 			  <table id="lista" class="table table-bordered table-striped">
   				<thead>
   				<tr>
   					<th width="1%"><a href="javascript: marcarTodosCheckBoxFormulario('AcessoGrupoUsuario')"><i class="icon fa fa-check"></a></th>
   					<th class='Titulo'>Id</th>
					<th class='Titulo'>Descrição</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){ ?>
   				<tr>
  					<td><input onClick="JavaScript: atualizaAcoes('AcessoGrupoUsuario')" type="checkbox" value="<?php echo $oAcessoGrupoUsuario->getCodGrupoUsuario()?>" name="fIdAcessoGrupoUsuario[]"/></td>
  					<td><?php echo $oAcessoGrupoUsuario->getCodGrupoUsuario()?></td>
					<td><?php echo  $oAcessoGrupoUsuario->getDescricao()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  			    <tfoot>
                 <tr>
                   <th></th>
                   <th class='Titulo'></th>
					<th class='Titulo'></th>

                 </tr>
                 </tfoot>
               </table>
  			<?php }//if(count($voAcessoGrupoUsuario)){?>
  			</form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
 <?php include_once('includes/footer.php')?>
 </div>
 <!-- ./wrapper -->
 <?php include_once('includes/javascript.php')?>
 </body>
 </html>
