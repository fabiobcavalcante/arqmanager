<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
    
<style>
    .label {
      cursor: pointer;
    }

    .progress {
      display: none;
      margin-bottom: 1rem;
    }

    .alert {
      display: none;
    }

    .img-container img {
      max-width: 100%;
    }
  </style>
    
</head>
<body>

<div class="container">
    <h1>Upload cropped image to server</h1>
    
    <form enctype="multipart/form-data">
    
        
        <label class="label" data-toggle="tooltip" title="" data-original-title="Change your avatar">
          <img class="rounded" id="avatar" src="https://avatars0.githubusercontent.com/u/3456749?s=160" alt="avatar">

          <input type="file" class="sr-only" id="input" name="image" accept="image/*">
            
            
        </label>
        <div class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
        </div>
        <div class="alert" role="alert"></div>
        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="img-container">
                  <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                </div>
              </div>
              <div class="modal-footer">
                <div class="row" id="actions">
                    <div class="col-md-9 docs-buttons">

                        <div class="btn-group">
                          <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate Left">
                            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.rotate(-90)">
                              <span class="fa fa-undo-alt"></span>
                            </span>
                          </button>
                          <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate Right">
                            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.rotate(90)">
                              <span class="fa fa-redo-alt"></span>
                            </span>
                          </button>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop">Crop</button>
              </div>
            </div>
          </div>
        </div>
    </form>
  </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.bundle.min.js"></script>
    <script src="node_modules/cropperjs/dist/cropper.min.js"></script>
    
    <script src="cropper.js"></script>
    
    

</body>
</html>