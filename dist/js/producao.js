function recuperaConteudo(sArquivo,sParametros=false,sIdDivInsert=false){
    sArquivo = sArquivo+sParametros;
    $("#" + sIdDivInsert).html("Carregando Informação <i class='fa fa-spin fa-refresh'></i>").load(sArquivo);
}

function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){

    sArquivo = sArquivo+'?'+sParametros;

    $.ajax({
        dataType: "html",
        type: "GET",
        beforeSend: function(){
            $("#"+sIdDivInsert).html("Carregando Informação <i class='fa fa-spin fa-refresh'></i>");
        },
        url: sArquivo,
        error: function(oXMLRequest,sErrorType){
            console.log(sArquivo);
            console.log(oXMLRequest.responseText);
            console.log(oXMLRequest.status+' , '+sErrorType);
        },
        success: function(data){
            $("#"+sIdDivInsert).html(data);
        }
    });
}

function atualizaAcoes(sNome){
    let i;
    let oSelect = document.getElementById('acoes' + sNome);
    let nChecados = retornaChecados(sNome);
    if(nChecados === 0){
        for(i=0; i < oSelect.length; i++){
            oSelect.options[i].disabled = oSelect.options[i].lang === '1' || oSelect.options[i].lang === '2';
        }
    }
    if(nChecados === 1){
        for(i=0; i < oSelect.length; i++){
            oSelect.options[i].disabled = oSelect.options[i].lang === '0';
        }
    }
    if(nChecados > 1){
        for(i = 0; i < oSelect.length; i++){
            oSelect.options[i].disabled = oSelect.options[i].lang === '0' || oSelect.options[i].lang === '1';
        }
    }
    oSelect.options[0].selected = false;
}

function validaAcoes(sNome){
    let oSelect = document.getElementById('acoes' + sNome);
    let nChecados = retornaChecados(sNome);
    switch(oSelect.options[oSelect.selectedIndex].lang){
        case '0':
            if(nChecados > 0){
                alert("Nenhum registro deve estar marcado para realizar esta ação!");
                oSelect.options[0].selected = true;
                return false;
            }
            return true;
        case '1':
            if(nChecados !== 1 ){
                alert("Apenas um registro deve estar marcado para realizar esta ação!");
                oSelect.options[0].selected = true;
                return false;
            }
            return true;
        case '2':
            if(nChecados < 1 ){
                alert("Pelo menos um registro deve estar marcado para realizar esta ação!");
                oSelect.options[0].selected = true;
                return false;
            }
            return true;
    }
}

function submeteForm(sNome) {
    let oSelect;
    let oForm;
    let camposMarcados;
    let sLink;
    if (validaAcoes(sNome)) {
        oSelect = document.getElementById('acoes' + sNome);
        oForm = document.getElementById('form' + sNome);
        oForm.action = oSelect.value;

        if (oSelect.value.indexOf("sOP=Excluir") > 0) {
            let nRegistros;
            camposMarcados = [];
            $("input[type=checkbox][name='fId" + sNome + "[]']:checked").each(function () {
                camposMarcados.push($(this).val());
            });
            nRegistros = camposMarcados.toString().split(',').join('____');
            sLink = oSelect.value + "&fId" + sNome + "=" + nRegistros;
            $("#confirm-delete").on("shown.bs.modal", function () {
                $(this).find('#ok').attr('href', sLink);
            });
            $('#confirm-delete').modal('show');

            return false;
        } else {
            oForm.submit();
            return true;
        }
    }

}

function retornaChecados(sNome){
    let nChecados = 0;
    let oForm = document.getElementById('form' + sNome);
    for(let i = 1; i < oForm.length; i++){
        if(oForm.elements[i].type === 'checkbox'){
            if(oForm.elements[i].checked)
                nChecados++;
        }
    }
    return nChecados;
}

function confirmaSenha(sSenha,sConfirmaSenha){
    if(document.getElementById(sSenha).value !== document.getElementById(sConfirmaSenha).value){
        alert('Os campos Senha e Confirma Senha não conferem');
        return false;
    } else
        return true;
}

function valida_cpf(cpf) {
    if (typeof cpf !== "string") return false
    cpf = cpf.replace(/[\s.-]*/igm, '')
    if (
        !cpf ||
        cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999"
    ) {
        return false
    }
    var soma = 0
    var resto
    for (var i = 1; i <= 9; i++)
        soma = soma + parseInt(cpf.substring(i-1, i)) * (11 - i)
    resto = (soma * 10) % 11
    if ((resto == 10) || (resto == 11))  resto = 0
    if (resto != parseInt(cpf.substring(9, 10)) ) return false
    soma = 0
    for (var i = 1; i <= 10; i++)
        soma = soma + parseInt(cpf.substring(i-1, i)) * (12 - i)
    resto = (soma * 10) % 11
    if ((resto == 10) || (resto == 11))  resto = 0
    if (resto != parseInt(cpf.substring(10, 11) ) ) return false
    return true
}

//valida o CNPJ digitado

function validar_CNPJ(cnpj) {

    cnpj = cnpj.replace(/[^\d]+/g,'');

    if(cnpj == '') return false;

    if (cnpj.length != 14)
        return false;

    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;

    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;

    return true;
}

//onblur="validacaoEmail(this)"
function validacaoEmail(valor) {
    let usuario = valor.substring(0, valor.indexOf("@"));
    let dominio = valor.substring(valor.indexOf("@") + 1, valor.length);

    return (usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") === -1) &&
        (dominio.search("@") === -1) &&
        (usuario.search(" ") === -1) &&
        (dominio.search(" ") === -1) &&
        (dominio.search(".") !== -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1);
}

function marcarTodosCheckBoxFormulario(sIdForm){
    let nChecados = retornaChecados(sIdForm);

    let bMarcado = nChecados < 1;

    let oForm = document.getElementById('form' + sIdForm);
    let vElements = oForm.getElementsByTagName('INPUT');
    for(let i = 0; i < vElements.length; i++){
        if(vElements[i].type === 'checkbox'){
            vElements[i].checked = bMarcado;
        }
    }

    atualizaAcoes(sIdForm);

}

function MostraEsconde2(div){
    if(document.getElementById(div).style.display === "none"){
        document.getElementById(div).style.display = "block";
    }else if(document.getElementById(div).style.display === "block"){
        document.getElementById(div).style.display = "none";
    }
}

//onKeyPress="TodosNumero(event);"
function TodosNumero(e){
    let isNS4 = (navigator.appName === "Netscape") ? 1 : 0;
    let codigo = e.keyCode ? e.keyCode : e.charCode;
    let expReg = /^\d*$/;
    let caracter = String.fromCharCode(codigo);

    if ((codigo !== 8) && (codigo !== 46) && (codigo !== 44) &&(codigo !== 37) && (codigo !== 39)&& (codigo !== 9)&& (codigo !== 127)) {
        if (!expReg.test(caracter)) {
            if (isNS4) {
                e.preventDefault();
            } else {
                e.returnValue = false;
            }
        }
    }
}

//onBlur='return validaData(this);'
function validaData(str) {
    let cons;
    let dia;
    let mes;
    let ano;
    if (str.value !== "") {
        dia = (str.value.substring(0, 2));
        mes = (str.value.substring(3, 5));
        ano = (str.value.substring(6, 10));

        cons = true;

        // verifica se foram digitados números
        if (isNaN(dia) || isNaN(mes) || isNaN(ano)) {
            alert("Preencha a data somente com numeros.");
            str.value = "";
            str.focus();
            return false;
        }

        // verifica o dia valido para cada mes
        if ((dia < 1) || (dia < 1 || dia > 30) &&
            (mes === 4 || mes === 6 ||
                mes === 9 || mes === 11) ||
            dia > 31) {
            cons = false;
        }

        // verifica se o mes e valido
        if (mes < 1 || mes > 12) {
            cons = false;
        }

        // verifica se e ano bissexto
        // noinspection JSCheckFunctionSignatures
        if (mes === 2 && (dia < 1 || dia > 29 ||
            (dia > 28 &&
                (parseInt(ano / 4) !== ano / 4)))) {
            cons = false;
        }

        if (cons === false) {
            alert("A data inserida não é válida: " + str.value);
            str.value = "";
            str.focus();
            return false;
        }
    }
}

function chamarModal(sIdCampo,sMensagem){
    $(function() {
        $('#alerta').modal('show'); //Abrir modal
        document.getElementById("sMsg").innerHTML = sMensagem;
        if(sIdCampo){
            $("#"+sIdCampo).css('background-color','#F89C8F');
        }
        $('#botao').attr('disabled','disabled'); //Desabilitar botão
        return false;
    });
}

function validar(valor,sIdCampo,sFuncao,sMensagem){
    if (eval(sFuncao)(valor) === false){
        chamarModal(sIdCampo,sMensagem);
        $('#botao').attr('disabled',true); //Habilitar botão
    }else{
        $("#"+sIdCampo).css('background-color','');
        $('#botao').attr('disabled',false); //Habilitar botão
    }
}

function FormataMoney(i) {
    let v = i.value.replace(/\D/g, '');
    v = (v/100).toFixed(2) + '';
    v = v.replace(".", ",");
    v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
    v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
    i.value = v;
}
