<?php
include_once "includes/__autoloader.php";
$oFachada = new FachadaPrincipalBD();
$oMailer = FabricaUtilitario::getUtilitario("Mailer");
$oMailer = new Mailer();


$voEscritorio = $oFachada->recuperarTodosEscritorio();
if($voEscritorio){
	foreach($voEscritorio as $oEscritorio){

		// ALTERA STATUS DE PROPOSTA COM MAIS DE 1 MES SEM RESPOSTA
		$nPrazo =30;
		$nCodStatus=1;
		$voProposta = $oFachada->recuperarTodosPropostaPorPrazoSemResposta($oEscritorio->getIdEscritorio(),$nPrazo,$nCodStatus);
//		$nProposta = 0;
		if($voProposta){
			foreach($voProposta as $oProposta){
				$oPropostaSelecionada = $oFachada->recuperarUmProposta($oProposta->getCodProposta());
				$oPropostaSelecionada->setCodStatus(4);
				$oPropostaSelecionada->setAlteradoPor("AUTOMÁTICO || ".date('d/m/Y'));
				$oFachada->alterarProposta($oPropostaSelecionada);
//					$nProposta++;
			}
			//echo "Total de Propostas alteradas: {$nProposta}<hr>";
			//die();
		}
		// FIM ALTERA STATUS DE PROPOSTA COM MAIS DE 1 MES SEM RESPOSTA

		//preparando template para envio de email
		$nCodEscritorio = $oEscritorio->getIdEscritorio();
		$dDataHora = date('Y-m-d H:i:s');

		$sCabecalhoEmail = $oEscritorio->getCabecalhoEmail();
		$sSite = ($oEscritorio->getSite()) ?:"";
		$sCabecalhoEmail = str_replace("#SITE#","{$sSite}",$sCabecalhoEmail);
		$sLogomarca = ($oEscritorio->getLogomarca())?:"https://arqmanager.com.br/wp-content/uploads/2021/06/logo_arq_azul.png";
		$sCabecalhoEmail = str_replace("#LOGO#","{$sLogomarca}",$sCabecalhoEmail);

		$sRodapeEmail = $oEscritorio->getRodapeEmail();
		$sRodapeEmail = str_replace("#ESCRITORIO#","{$oEscritorio->getNomeFantasia()}",$sRodapeEmail);
		$sRodapeEmail = str_replace("#ENDERECO_REDUZIDO#","{$oEscritorio->getEnderecoReduzido()}",$sRodapeEmail);

			$voEmailTipo = $oFachada->recuperarTodosEmailTipoPorEscritorio($oEscritorio->getIdEscritorio());
			if($voEmailTipo && $oEscritorio->getEnvioEmail()=='S'){
				foreach($voEmailTipo as $oEmailTipo){

					switch($oEmailTipo->getCodEmailTipo()){

						case 2:
							// FELIZ ANIVERSÁRIO
							$sConteudo =  $oEmailTipo->getTemplate();
							$voAniversariantes = $oFachada->recuperarTodosAniversariantesDia($nCodEscritorio);
							if($voAniversariantes){
								$nAniversariantes=0;
								foreach($voAniversariantes as $oAniversariante){
									$sConteudo = str_replace("#SAUDACAO#","{$oAniversariante->nome}",$sConteudo);
									$sConteudo = str_replace("#NOME_ESCRITORIO#","{$oEscritorio->getNomeFantasia()}",$sConteudo);
									$sConteudo = $sCabecalhoEmail . $sConteudo . $sRodapeEmail;
									$sConteudo = htmlspecialchars($sConteudo);

									$sEmail = $oAniversariante->email;
								//	$sEmail = "bc_fabio@hotmail.com";
									$oMailer->enviar("{$oAniversariante->nome}", "{$sEmail}", "[{$oEscritorio->getNomeFantasia()}] {$oEmailTipo->getTitulo()}", "template", ["sConteudo"=>$sConteudo]);
									// Inserir na Tabela de Email
									$sDescricaoEmail = $oEmailTipo->getTitulo();
									$oEmail = $oFachada->inicializarEmail($nCodEmail,$oAniversariante->codigo,$oEmailTipo->getCodEmailTipo(),$sDescricaoEmail,$dDataHora);
									$oFachada->inserirEmail($oEmail);
									unset($sConteudo);
									// $nAniversariantes++;
								}
								// echo "Total de aniversariantes: {$nAniversariantes}<hr>";
							}
						break;

						case 3: // Aviso de Vencimento
							$sConteudo = $oEmailTipo->getTemplate();
							$nDias=3;
							$nTipo=1;
							$voAviso = $oFachada->recuperarTodosProjetoPagamentoCron($nDias,$nTipo,$nCodEscritorio);

							if($voAviso){
								foreach($voAviso as $oAviso){
									$sConteudo = str_replace("#SAUDACAO#","{$oAviso->nome_cliente}",$sConteudo);
									$sConteudo = str_replace("#DESC_PROJETO#","{$oAviso->titulo}",$sConteudo);
									$sConteudo = str_replace("#VENCIMENTO#","{$oAviso->data_previsao_formatada}",$sConteudo);
									$sConteudo = str_replace("#DESC_PAGAMENTO#","{$oAviso->desc_pagamento}",$sConteudo);
									$sConteudo = str_replace("#VALOR#","{$oAviso->valor_formatado}",$sConteudo);
									//$sConteudo = str_replace("#FORMA_PAGAMENTO#","{$oAviso->nome_cliente}",$sConteudo);
									$sConteudo = $sCabecalhoEmail . $sConteudo . $sRodapeEmail;
									$sConteudo = htmlspecialchars($sConteudo);

									$sEmail = $oAviso->email;
									//$sEmail = "bc_fabio@hotmail.com";
									$oMailer->enviar("{$oAviso->nome_cliente}", "{$sEmail}", "[{$oEscritorio->getNomeFantasia()}] {$oEmailTipo->getTitulo()}", "template", ["sConteudo"=>$sConteudo]);
									// Inserir na Tabela de Email
									$sDescricaoEmail = $oEmailTipo->getTitulo();
									$oEmail = $oFachada->inicializarEmail($nCodEmail,$oAviso->cod_cliente,$nCodTipoEmail,$sDescricaoEmail,$dDataHora);
									$oFachada->inserirEmail($oEmail);
									unset($sConteudo);
								}
							}
						break;

						case "4":
						// 	-- and nao foi enviado nos ultimos 3 dias
						// Atraso no Pagamento
							$sConteudo = $oEmailTipo->getTemplate();
							$nDias=5;
							$nTipo=2;
							$voAtraso = $oFachada->recuperarTodosProjetoPagamentoCron($nDias,$nTipo,$nCodEscritorio);

							if($voAtraso){
								foreach($voAtraso as $oAtraso){
									$sConteudo = str_replace("#SAUDACAO#","{$oAtraso->nome_cliente}",$sConteudo);
									$sConteudo = str_replace("#DESC_PROJETO#","{$oAtraso->titulo}",$sConteudo);
									$sConteudo = str_replace("#DATA_VENCIMENTO#","{$oAtraso->data_previsao_formatada}",$sConteudo);
									$sConteudo = str_replace("#DESC_PAGAMENTO#","{$oAtraso->desc_pagamento}",$sConteudo);
									$sConteudo = str_replace("#VALOR#","{$oAtraso->valor_formatado}",$sConteudo);
									$sConteudo = str_replace("#NOME_ESCRITORIO#","{$oEscritorio->getNomeFantasia()}",$sConteudo);
									$oExtenso = new Extenso();
									$sValorExtenso = mb_strtoupper($oExtenso->valorPorExtenso($oAtraso->valor_formatado));
									$sConteudo = str_replace("#VALOR#","{$oAtraso->valor_formatado}",$sConteudo);
									$sConteudo = str_replace("#VALOR_EXTENSO#","{$sValorExtenso}",$sConteudo);
									$sConteudo = $sCabecalhoEmail . $sConteudo . $sRodapeEmail;
									$sConteudo = htmlspecialchars($sConteudo);

										//$sEmail = $oEscritorio->getEmailAdministrador();
								//	$sEmail = "bc_fabio@hotmail.com";
								//	$oMailer->enviar("{$oAtraso->nome_cliente}", "{$sEmail}", "[{$oEscritorio->getNomeFantasia()}] {$oEmailTipo->getTitulo()}", "template", ["sConteudo"=>$sConteudo]);
									$sEmail = $oAviso->email;
									//$sEmail = "bc_fabio@hotmail.com";
									$oMailer->enviar("{$oAtraso->nome_cliente}", "{$sEmail}", "[{$oEscritorio->getNomeFantasia()}] {$oEmailTipo->getTitulo()}", "template", ["sConteudo"=>$sConteudo]);
									// Inserir na Tabela de Email
									$sDescricaoEmail = $oEmailTipo->getTitulo();
									$oEmail = $oFachada->inicializarEmail($nCodEmail,$oAtraso->cod_cliente,$nCodTipoEmail,$sDescricaoEmail,$dDataHora);
									$oFachada->inserirEmail($oEmail);
									unset($sConteudo);
								}
							}

						break;
					} // switch

				}//foreach($voEmailTipo as $oEmailTipo){
			} //if($voEmailTipo && $oEscritorio->getEnvioEmail()=='S'){
		} // foreach($voEscritorio as $oEscritorio){
	} //if($voEscritorio){
?>
