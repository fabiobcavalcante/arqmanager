<?php
 class AcessoUsuarioCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaAcessoBD();

 		$voAcessoUsuario = $oFachada->recuperarTodosAcessoUsuario();

 		$_REQUEST['voAcessoUsuario'] = $voAcessoUsuario;


 		include_once("view/Acesso/acesso_usuario/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaAcessoBD();

 		$oAcessoUsuario = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" || $_REQUEST['sOP'] == "AlterarFoto"){
 			$nIdAcessoUsuario = ($_POST['fIdAcessoUsuario'][0]) ? $_POST['fIdAcessoUsuario'][0] : $_GET['nIdAcessoUsuario'];

 			if($nIdAcessoUsuario){
 				$vIdAcessoUsuario = explode("||",$nIdAcessoUsuario);
 				$oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($vIdAcessoUsuario[0]);
 			}
 		}

 		$_REQUEST['oAcessoUsuario'] = ($_SESSION['oAcessoUsuario']) ? $_SESSION['oAcessoUsuario'] : $oAcessoUsuario;
 		unset($_SESSION['oAcessoUsuario']);

 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();

    switch($_REQUEST['sOP']){
      case "Detalhar":
 			  include_once("view/Acesso/acesso_usuario/detalhe.php");
      break;
      case "Cadastrar":
      case "Alterar":
       			include_once("view/Acesso/acesso_usuario/insere_altera.php");
      break;
      case "AlterarSenha":
            include_once("view/Acesso/acesso_usuario/alterar_senha.php");
      break;
      case "AlterarFoto":
        include_once("view/Acesso/acesso_usuario/foto.php");
      break;
    }
 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaAcessoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];



 		if($sOP != "Excluir" && $sOP != "AlterarSenha" && $sOP!='AlterarFoto'){
      switch($sOP){
          case 'Cadastrar':
                 $_POST['fIncluidoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
          break;
          case 'Alterar':
              $oAcessoUsuario1 = $oFachada->recuperarUmAcessoUsuario($_POST['fCodUsuario']);
              $_POST['fIncluidoPor'] =  $oAcessoUsuario1->getIncluidoPor();
              $_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
          break;

      }

 			$oAcessoUsuario = $oFachada->inicializarAcessoUsuario($_POST['fCodUsuario'],$_POST['fCodGrupoUsuario'],$_POST['fLogin'],$_POST['fSenha'],$_POST['fFoto'],$_POST['fInseridoPor'],$_POST['fAlteradoPor'],1,$_POST['fIdEscritorio']);
 			$_SESSION['oAcessoUsuario'] = $oAcessoUsuario;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("CodGrupoUsuario", $oAcessoUsuario->getCodGrupoUsuario(), "number", "y");
			$oValidate->add_text_field("Login", $oAcessoUsuario->getLogin(), "text", "y");
			//$oValidate->add_text_field("Senha", $oAcessoUsuario->getSenha(), "text", "y");
			$oValidate->add_text_field("InseridoPor", $oAcessoUsuario->getInseridoPor(), "text", "y");
			//$oValidate->add_text_field("AlteradoPor", $oAcessoUsuario->getAlteradoPor(), "text", "y");
			$oValidate->add_number_field("Ativo", $oAcessoUsuario->getAtivo(), "number", "y");

 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoUsuario($oAcessoUsuario)){
 					unset($_SESSION['oAcessoUsuario']);
 					$_SESSION['sMsg'] = "Usuário inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Usuário!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoUsuario($oAcessoUsuario)){
 					unset($_SESSION['oAcessoUsuario']);
 					$_SESSION['sMsg'] = "Usuário alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Usuário!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario']."";
 				}
 			break;
      case "AlterarSenha":
      $nCodUsuario = $_REQUEST['fCodUsuario'];
      $oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($nCodUsuario);

      if($oAcessoUsuario->getSenha() == md5($_REQUEST['fSenhaAtual'])){

        if($_REQUEST['fSenhaNova'] == $_REQUEST['fSenhaNova2']){
          $oAcessoUsuario->setSenha(md5($_REQUEST['fSenhaNova']));
          $oAcessoUsuario->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
          $oFachada->alterarAcessoUsuario($oAcessoUsuario);
          unset($_SESSION['oAcessoUsuario']);
          $_SESSION['sMsg'] = "Senha alterada com sucesso!";
          $sHeader = "?";
        } else {
          $_SESSION['sMsg'] = "Os campos Nova Senha e Repete nova Senha estão diferentes!";
          $sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP;
        }
      }else{
         $_SESSION['sMsg'] = "Não foi possível alterar sua senha pois a senha informada está errada!";
         $sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP;
      }
     break;
      case "AlterarFoto":
      $bErro=0;
      $nCodUsuario = $_REQUEST['fCodUsuario'];
      $oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($nCodUsuario);
      $file = $_FILES['fFoto'];
      $vPermissao = array ("image/jpeg","image/jpg","image/png");

      if (!in_array($file['type'], $vPermissao)){
        $_SESSION['sMsg'] = "Este tipo de arquivo[".$file['type']."] não tem permissão para upload";
        $bErro=1;
      }
      $nTamanhoMaximoArquivo = 2000000;
      if ($file['size'] > $nTamanhoMaximoArquivo){
        $_SESSION['sMsg'] = "Erro no tamanho do arquivo: ".$file['size'] ." Kb. Tamanho Maximo é ".$nTamanhoMaximoArquivo." !";
        $bErro=1;
      }
      if ($file['error']){
      		throw new \Exception("Erro: ".$file['error']);
      	}

      $dirUploads = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."unitarquitetura".DIRECTORY_SEPARATOR."admin".DIRECTORY_SEPARATOR."view".DIRECTORY_SEPARATOR."Acesso".DIRECTORY_SEPARATOR."acesso_usuario" . DIRECTORY_SEPARATOR . "foto";

      if (!is_dir($dirUploads)){
      		mkdir($dirUploads);
      }
      if($bErro!=1){
        $sInformacoesArquivo = new SplFileInfo($file['name']);
        $sNomeFoto = $nCodUsuario.".".$sInformacoesArquivo->getExtension();
        if (move_uploaded_file($file['tmp_name'], $dirUploads.DIRECTORY_SEPARATOR.$sNomeFoto)){ //Salvar arquivo no servidor
          $url = DIRECTORY_SEPARATOR.$dirUploads.DIRECTORY_SEPARATOR.$sNomeFoto;
          unset($_SESSION['oAcessoUsuario']);
          $oAcessoUsuario->setFoto($sNomeFoto);
          $oFachada->alterarAcessoUsuario($oAcessoUsuario);
          $_SESSION['sMsg'] = "Foto alterada com sucesso!";
          $bErro=0;
        } else {
         $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a foto!";
         $bErro=1;
       }
   }
     $sHeader = "?bErro=".$bErro."&action=AcessoUsuario.preparaFormulario&sOP=Detalhar&nIdAcessoUsuario=".$nCodUsuario;

   break;

 			case "Excluir":
 				$bResultado = true;
 				$vIdPaiAcessoUsuario = explode("____",$_REQUEST['fIdAcessoUsuario']);
   				foreach($vIdPaiAcessoUsuario as $vIdFilhoAcessoUsuario){
  					$vIdAcessoUsuario = explode("||",$vIdFilhoAcessoUsuario);
 					foreach($vIdAcessoUsuario as $nIdAcessoUsuario){
  						$bResultado &= $oFachada->excluirAcessoUsuario($vIdAcessoUsuario[0],$vIdAcessoUsuario[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Usuário(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Usuário!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
