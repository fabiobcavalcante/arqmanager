<?php
 class AcessoResponsavelTransacaoCTR implements IControle{
 
 	public function preparaLista(){
 		$oFachada = new FachadaAcessoBD();
 
        $nCodTransacaoModulo = ($_POST['fIdAcessoTransacaoModulo'][0]) ? $_POST['fIdAcessoTransacaoModulo'][0] : $_REQUEST['nCodTransacaoModulo'];
 		$oAcessoTransacaoModulo = $oFachada->recuperarUmAcessoTransacaoModulo($nCodTransacaoModulo);
 		$_REQUEST['oAcessoTransacaoModulo'] = $oAcessoTransacaoModulo;
 		include_once("view/Acesso/acesso_responsavel_transacao/index.php");
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaAcessoBD();
 
 		$oAcessoResponsavelTransacao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
  			$nIdAcessoResponsavelTransacao = ($_POST['fIdAcessoResponsavelTransacao'][0]) ? $_POST['fIdAcessoResponsavelTransacao'][0] : $_GET['nIdAcessoResponsavelTransacao'];
 			if($nIdAcessoResponsavelTransacao){
 				$vIdAcessoResponsavelTransacao = explode("||",$nIdAcessoResponsavelTransacao);
 				$oAcessoResponsavelTransacao = $oFachada->recuperarUmAcessoResponsavelTransacao($vIdAcessoResponsavelTransacao[0]);
 			}
 		}else{
			$_REQUEST['oAcessoTransacaoModulo'] = $oFachada->recuperarUmAcessoTransacaoModulo($_REQUEST['nCodTransacaoModulo']);
			
		}
 		
 		$_REQUEST['oAcessoResponsavelTransacao'] = ($_SESSION['oAcessoResponsavelTransacao']) ? $_SESSION['oAcessoResponsavelTransacao'] : $oAcessoResponsavelTransacao;
 		unset($_SESSION['oAcessoResponsavelTransacao']);
 
 		//$_REQUEST['voAcessoTransacaoModulo'] = $oFachada->recuperarTodosAcessoTransacaoModulo();

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Acesso/acesso_responsavel_transacao/detalhe.php");
 		else
 			include_once("view/Acesso/acesso_responsavel_transacao/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaAcessoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oAcessoResponsavelTransacao = $oFachada->inicializarAcessoResponsavelTransacao($_POST['fCodResponsavelTransacao'],$_POST['fCodTransacaoModulo'],$_POST['fResponsavel'],$_POST['fOperacao']);
 			$_SESSION['oAcessoResponsavelTransacao'] = $oAcessoResponsavelTransacao;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			//$oValidate->add_number_field("CodTransacaoModulo", $oAcessoResponsavelTransacao->getCodTransacaoModulo(), "number", "y");
			//$oValidate->add_text_field("Responsavel", $oAcessoResponsavelTransacao->getResponsavel(), "text", "y");
			//$oValidate->add_text_field("Operacao", $oAcessoResponsavelTransacao->getOperacao(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoResponsavelTransacao.preparaFormulario&sOP=".$sOP."&nIdAcessoResponsavelTransacao=".$_POST['fCodResponsavelTransacao']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoResponsavelTransacao($oAcessoResponsavelTransacao)){
 					unset($_SESSION['oAcessoResponsavelTransacao']);
 					$_SESSION['sMsg'] = "Responsável Transação inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoResponsavelTransacao.preparaLista&nCodTransacaoModulo=".$oAcessoResponsavelTransacao->getCodTransacaoModulo();
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Responsável Transação!";
 					$sHeader = "?bErro=1&action=AcessoResponsavelTransacao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoResponsavelTransacao($oAcessoResponsavelTransacao)){
 					unset($_SESSION['oAcessoResponsavelTransacao']);
 					$_SESSION['sMsg'] = "Responsável Transação alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoResponsavelTransacao.preparaLista&nCodTransacaoModulo=".$oAcessoResponsavelTransacao->getCodTransacaoModulo();
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Responsável Transação!";
 					$sHeader = "?bErro=1&action=AcessoResponsavelTransacao.preparaFormulario&sOP=".$sOP."&nIdAcessoResponsavelTransacao=".$_POST['fCodResponsavelTransacao']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiAcessoResponsavelTransacao = explode("____",$_REQUEST['fIdAcessoResponsavelTransacao']);
   				foreach($vIdPaiAcessoResponsavelTransacao as $vIdFilhoAcessoResponsavelTransacao){
  					$vIdAcessoResponsavelTransacao = explode("||",$vIdFilhoAcessoResponsavelTransacao);
 					foreach($vIdAcessoResponsavelTransacao as $nIdAcessoResponsavelTransacao){
  						$bResultado &= $oFachada->excluirAcessoResponsavelTransacao($vIdAcessoResponsavelTransacao[0],$vIdAcessoResponsavelTransacao[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Responsável Transação(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoResponsavelTransacao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Responsável Transação!";
 					$sHeader = "?bErro=1&action=AcessoResponsavelTransacao.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>