<?php
 class AcessoPermissaoCTR implements IControle{



 	public function preparaLista(){
 		$oFachada = new FachadaAcessoBD();

        $nIdAcessoPermissao = ($_POST['fIdAcessoGrupoUsuario'][0]) ? $_POST['fIdAcessoGrupoUsuario'][0] : $_GET['nIdGrupoUsuario'];
		$_REQUEST['voAcessoTransacaoModulo'] = $oFachada->recuperarTodosAcessoModulo();
		$voAcessoPermissao = $oFachada->recuperarTodosAcessoPermissaoPorGrupoUsuario($nIdAcessoPermissao);
		$_REQUEST['oGrupoUsuario'] = $oFachada->recuperarUmAcessoGrupoUsuario($nIdAcessoPermissao);
		$_REQUEST['voAcessoModulo'] = $oFachada->recuperarTodosAcessoModulo();
 		$_REQUEST['voAcessoPermissao'] = $voAcessoPermissao;

 		include_once("view/Acesso/acesso_permissao/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaAcessoBD();

 		$oAcessoPermissao = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoPermissao = ($_POST['fIdAcessoPermissao'][0]) ? $_POST['fIdAcessoPermissao'][0] : $_GET['nIdAcessoPermissao'];

 			if($nIdAcessoPermissao){
 				$vIdAcessoPermissao = explode("||",$nIdAcessoPermissao);
 				$oAcessoPermissao = $oFachada->recuperarUmAcessoPermissao($vIdAcessoPermissao[0],$vIdAcessoPermissao[1]);
 			}
 		}

 		$_REQUEST['oAcessoPermissao'] = ($_SESSION['oAcessoPermissao']) ? $_SESSION['oAcessoPermissao'] : $oAcessoPermissao;
 		unset($_SESSION['oAcessoPermissao']);

 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Acesso/acesso_permissao/detalhe.php");
 		else
 			include_once("view/Acesso/acesso_permissao/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaAcessoBD();

 		         $bResultado = true;
		$i=0;
	    set_time_limit(3600); // 30 minutos

        if($oFachada->excluirAcessoPermissaoPorGrupoUsuario($_POST['fCodGrupoUsuario'])){

		   foreach($_POST['fCodTransacaoModulo'] as $nCodTransacaoModulo){
                $oPermissao = $oFachada->inicializarAcessoPermissao($nCodTransacaoModulo, $_POST['fCodGrupoUsuario']);
                $bResultado &= $oFachada->inserirAcessoPermissao($oPermissao);
				$i++;
            }


        } else
            $bResultado = false;

        if($bResultado){
 			$_SESSION['oPermissao'] = array();
            $_SESSION['sMsg'] = "Permiss&atilde;o alterada com sucesso!";
        } else {
            $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Permiss&atilde;o!";
        }

 		header("Location: index.php?action=AcessoPermissao.preparaLista&nIdGrupoUsuario=".$_POST['fCodGrupoUsuario']);

        die();

 	}

 }


 ?>
