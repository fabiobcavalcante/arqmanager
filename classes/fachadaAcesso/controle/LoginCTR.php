<?php
class LoginCTR implements IControle{

    public function preparaLista(){
        $this->preparaFormulario();
    }

    public function preparaFormulario(){
        include_once("view/login/index.php");
        exit();
    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $oFachadaAcesso = new FachadaAcessoBD();

        $sOP = $_REQUEST['sOP'];

        if($sOP != "Logoff"){
            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;
            $oValidate->add_text_field("Login", $_REQUEST['fLogin'], "text", "y");
            $oValidate->add_text_field("Senha", $_REQUEST['fSenha'], "text", "y");
            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=Login.preparaFormulario";
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Logon":
                $oColaborador = $oFachada->recuperarUmColaboradorPorLogin($_REQUEST['fLogin']);

                if($oColaborador){

                    if($oColaborador->getSenha() == md5($_REQUEST['fSenha'])){
                        $voGrupo = $oFachada->recuperarTodosColaboradorEscritorioGrupoPorColaborador($oColaborador->getCodColaborador());

                        if (count($voGrupo) > 1 && !isset($_POST['fIdEscritorio'])) {
                            $_REQUEST['voEscritorio'] = $oFachada->recuperarTodosEscritorioColaborador($oColaborador->getCodColaborador());

                            include_once("view/login/index.php");
                            die();
                        }else if (isset($_POST['fIdEscritorio']))
                            $oGrupo = $oFachada->recuperarUmColaboradorEscritorioGrupo($oColaborador->getCodColaborador(),$_POST['fIdEscritorio']);
                        else {
                            $oGrupo = $voGrupo[0];
                            $_POST['fIdEscritorio'] = $oGrupo->getIdEscritorio();
                        }
                        $voPermissoes = $oFachadaAcesso->recuperarTodosAcessoPermissaoPorGrupoUsuario($oGrupo->getCodGrupoUsuario());
                        $aPermissao =  array();
                        $oEscritorio = $oFachada->recuperarUmEscritorio($_POST['fIdEscritorio']);
                        foreach($voPermissoes as $oPermissao){
                            $aPermissao[] = $oPermissao->getCodTransacaoModulo();
                        }
                        $_SESSION['oEscritorio'] = $oEscritorio;
                        $_SESSION['voPermissao'] = $aPermissao;

                        $_SESSION['oUsuarioAM'] = $oColaborador;
                        $_SESSION['oGrupoUsuario'] = $oGrupo;
                        $voUsuarioDesde = explode($_SESSION['oUsuarioAM']->getInseridoPor(), '||');
                        $_SESSION['oUsuarioAM']->setInseridoPor($voUsuarioDesde[1]) ;

                        if($this->verificaSenhaAtualizada()){
                            $sHeader = "?";
                        }else{
                            $_REQUEST['oColaborador'] = $oColaborador;
                            include_once("view/login/alterar_senha.php");
                            die();
                        }
                    } else {
                        $_SESSION['sMsg2'] = "Senha incorreta!";
                        $sHeader = "?";
                    }
                } else {
                    $_SESSION['sMsg2'] = "Usuário não cadastrado no sistema!";
                    $sHeader = "?";
                }

                break;
            case "Logoff":
                $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
                session_unset();
                unset($_SESSION['oUsuarioAM']);
                // $sHeader = "?nIdEscritorio=".base64_encode($nIdEscritorio);
                $sHeader = "?";
                break;

        }
        header("Location: ".$sHeader);
        exit();

    }

    public function verificaSenhaAtualizada(){
        $oFachada = new FachadaPrincipalBD();
        $oColaborador = $oFachada->recuperarUmColaboradorPorLogin($_REQUEST['fLogin']);

        $vSinais = array(".", "-");
        $sSenhaInicial = str_replace($vSinais, "", $oColaborador->getCpf());
        $sSenha = md5($sSenhaInicial);

        if($oColaborador->getSenha() == $sSenha)
            return false;
        else
            return true;
    }
}
