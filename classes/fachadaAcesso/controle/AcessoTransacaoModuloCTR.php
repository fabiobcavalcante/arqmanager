<?php
 class AcessoTransacaoModuloCTR implements IControle{
 
 
 	public function preparaLista(){
 		$oFachada = new FachadaAcessoBD();
         $nCodModulo = ($_POST['fIdAcessoModulo'][0]) ? $_POST['fIdAcessoModulo'][0] : $_REQUEST['nCodModulo'];
       
        $oModulo = $oFachada->recuperarUmAcessoModulo($nCodModulo);
        $_REQUEST['oAcessoModulo'] = $oModulo;
        
 
 		include_once("view/Acesso/acesso_transacao_modulo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaAcessoBD();

 		$oAcessoTransacaoModulo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoTransacaoModulo = ($_POST['fIdAcessoTransacaoModulo'][0]) ? $_POST['fIdAcessoTransacaoModulo'][0] : $_GET['nIdAcessoTransacaoModulo'];
 	
 			if($nIdAcessoTransacaoModulo){
 				$vIdAcessoTransacaoModulo = explode("||",$nIdAcessoTransacaoModulo);
 				$oAcessoTransacaoModulo = $oFachada->recuperarUmAcessoTransacaoModulo($vIdAcessoTransacaoModulo[0]);
                $nCodModulo = $oAcessoTransacaoModulo->getCodModulo();
 			}
 		}else{
               $nCodModulo = $_REQUEST['nCodModulo']; 
        }
        
 		
 		$_REQUEST['oAcessoTransacaoModulo'] = ($_SESSION['oAcessoTransacaoModulo']) ? $_SESSION['oAcessoTransacaoModulo'] : $oAcessoTransacaoModulo;
 		unset($_SESSION['oAcessoTransacaoModulo']);
        $_REQUEST['oAcessoModulo'] = $oFachada->recuperarUmAcessoModulo($nCodModulo); 
 
 		//$_REQUEST['voAcessoModulo'] = $oFachada->recuperarTodosAcessoModulo();

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Acesso/acesso_transacao_modulo/detalhe.php");
 		else
 			include_once("view/Acesso/acesso_transacao_modulo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaAcessoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
            
 			$oAcessoTransacaoModulo = $oFachada->inicializarAcessoTransacaoModulo($_POST['fCodTransacaoModulo'],$_POST['nCodModulo'],$_POST['fDescricao'],$_POST['fAtivo']);

            $_SESSION['oAcessoTransacaoModulo'] = $oAcessoTransacaoModulo;
            $nCodModulo = $_POST['nCodModulo'];
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			//$oValidate->add_number_field("CodModulo", $oAcessoTransacaoModulo->getCodModulo(), "number", "y");
			$oValidate->add_text_field("Descrição", $oAcessoTransacaoModulo->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oAcessoTransacaoModulo->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoTransacaoModulo.preparaFormulario&sOP=".$sOP."&nIdAcessoTransacaoModulo=".$_POST['fCodTransacaoModulo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoTransacaoModulo($oAcessoTransacaoModulo)){
 					unset($_SESSION['oAcessoTransacaoModulo']);
 					$_SESSION['sMsg'] = "Transação Modulo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoTransacaoModulo.preparaLista&nCodModulo=".$nCodModulo;
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Transação Modulo!";
 					$sHeader = "?bErro=1&action=AcessoTransacaoModulo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
                
 				if($oFachada->alterarAcessoTransacaoModulo($oAcessoTransacaoModulo)){
 					unset($_SESSION['oAcessoTransacaoModulo']);
 					$_SESSION['sMsg'] = "Transação Módulo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoTransacaoModulo.preparaLista&nCodModulo=".$nCodModulo;
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Transação Modulo!";
 					$sHeader = "?bErro=1&action=AcessoTransacaoModulo.preparaFormulario&sOP=".$sOP."&nIdAcessoTransacaoModulo=".$_POST['fCodTransacaoModulo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiAcessoTransacaoModulo = explode("____",$_REQUEST['fIdAcessoTransacaoModulo']);
   				foreach($vIdPaiAcessoTransacaoModulo as $vIdFilhoAcessoTransacaoModulo){
  					$vIdAcessoTransacaoModulo = explode("||",$vIdFilhoAcessoTransacaoModulo);
 					foreach($vIdAcessoTransacaoModulo as $nIdAcessoTransacaoModulo){
  						$bResultado &= $oFachada->excluirAcessoTransacaoModulo($vIdAcessoTransacaoModulo[0],$vIdAcessoTransacaoModulo[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Transação Modulo(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoTransacaoModulo.preparaLista&nCodModulo=".$nCodModulo;
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Transação Modulo!";
 					$sHeader = "?bErro=1&action=AcessoTransacaoModulo.preparaLista&nCodModulo=".$nCodModulo;
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>