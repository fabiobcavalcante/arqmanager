<?php
 class AcessoModuloCTR implements IControle{
 
 	
 
 	public function preparaLista(){
 		$oFachada = new FachadaAcessoBD();
  		$voAcessoModulo = $oFachada->recuperarTodosAcessoModulo();
  		$_REQUEST['voAcessoModulo'] = $voAcessoModulo;
  		include_once("view/Acesso/acesso_modulo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaAcessoBD();
 
 		$oAcessoModulo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoModulo = ($_POST['fIdAcessoModulo'][0]) ? $_POST['fIdAcessoModulo'][0] : $_GET['nIdAcessoModulo'];
 	
 			if($nIdAcessoModulo){
 				$vIdAcessoModulo = explode("||",$nIdAcessoModulo);
 				$oAcessoModulo = $oFachada->recuperarUmAcessoModulo($vIdAcessoModulo[0]);
 			}
 		}
 		
 		$_REQUEST['oAcessoModulo'] = ($_SESSION['oAcessoModulo']) ? $_SESSION['oAcessoModulo'] : $oAcessoModulo;
 		unset($_SESSION['oAcessoModulo']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Acesso/acesso_modulo/detalhe.php");
 		else
 			include_once("view/Acesso/acesso_modulo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaAcessoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oAcessoModulo = $oFachada->inicializarAcessoModulo($_POST['fCodModulo'],$_POST['fDescricao'],$_POST['fAtivo']);
 			$_SESSION['oAcessoModulo'] = $oAcessoModulo;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			//$oValidate->add_text_field("Descricao", $oAcessoModulo->getDescricao(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oAcessoModulo->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoModulo.preparaFormulario&sOP=".$sOP."&nIdAcessoModulo=".$_POST['fCodModulo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoModulo($oAcessoModulo)){
 					unset($_SESSION['oAcessoModulo']);
 					$_SESSION['sMsg'] = "Modulo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoModulo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Modulo!";
 					$sHeader = "?bErro=1&action=AcessoModulo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoModulo($oAcessoModulo)){
 					unset($_SESSION['oAcessoModulo']);
 					$_SESSION['sMsg'] = "Modulo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoModulo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Modulo!";
 					$sHeader = "?bErro=1&action=AcessoModulo.preparaFormulario&sOP=".$sOP."&nIdAcessoModulo=".$_POST['fCodModulo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiAcessoModulo = explode("____",$_REQUEST['fIdAcessoModulo']);
   				foreach($vIdPaiAcessoModulo as $vIdFilhoAcessoModulo){
  					$vIdAcessoModulo = explode("||",$vIdFilhoAcessoModulo);
 					foreach($vIdAcessoModulo as $nIdAcessoModulo){
  						$bResultado &= $oFachada->excluirAcessoModulo($vIdAcessoModulo[0],$vIdAcessoModulo[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Modulo(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoModulo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Modulo!";
 					$sHeader = "?bErro=1&action=AcessoModulo.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>