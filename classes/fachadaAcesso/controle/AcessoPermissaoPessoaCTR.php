<?php
 class AcessoPermissaoPessoaCTR implements IControle{
 
 
 	public function preparaLista(){
 		$oFachada = new FachadaAcessoBD();
 
 		$voAcessoPermissaoPessoa = $oFachada->recuperarTodosAcessoPermissaoPessoa();
 
 		$_REQUEST['voAcessoPermissaoPessoa'] = $voAcessoPermissaoPessoa;
 		
 		
 		include_once("view/Acesso/acesso_permissao_pessoa/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaAcessoBD();
 
 		$oAcessoPermissaoPessoa = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoPermissaoPessoa = ($_POST['fIdAcessoPermissaoPessoa'][0]) ? $_POST['fIdAcessoPermissaoPessoa'][0] : $_GET['nIdAcessoPermissaoPessoa'];
 	
 			if($nIdAcessoPermissaoPessoa){
 				$vIdAcessoPermissaoPessoa = explode("||",$nIdAcessoPermissaoPessoa);
 				$oAcessoPermissaoPessoa = $oFachada->recuperarUmAcessoPermissaoPessoa($vIdAcessoPermissaoPessoa[0],$vIdAcessoPermissaoPessoa[1]);
 			}
 		}
 		
 		$_REQUEST['oAcessoPermissaoPessoa'] = ($_SESSION['oAcessoPermissaoPessoa']) ? $_SESSION['oAcessoPermissaoPessoa'] : $oAcessoPermissaoPessoa;
 		unset($_SESSION['oAcessoPermissaoPessoa']);
 
 		$_REQUEST['voAcessoUsuario'] = $oFachada->recuperarTodosAcessoUsuario();
		$_REQUEST['voAcessoTransacaoModulo'] = $oFachada->recuperarTodosAcessoTransacaoModulo();
		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Acesso/acesso_permissao_pessoa/detalhe.php");
 		else
 			include_once("view/Acesso/acesso_permissao_pessoa/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaAcessoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oAcessoPermissaoPessoa = $oFachada->inicializarAcessoPermissaoPessoa($_POST['fCodPessoa'],$_POST['fCodTransacaoModulo']);
 			$_SESSION['oAcessoPermissaoPessoa'] = $oAcessoPermissaoPessoa;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			//$oValidate->add_number_field("CodPessoa", $oAcessoPermissaoPessoa->getCodPessoa(), "number", "y");
			//$oValidate->add_number_field("CodTransacaoModulo", $oAcessoPermissaoPessoa->getCodTransacaoModulo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoPermissaoPessoa.preparaFormulario&sOP=".$sOP."&nIdAcessoPermissaoPessoa=".$_POST['fCodPessoa']."||".$_POST['fCodTransacaoModulo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoPermissaoPessoa($oAcessoPermissaoPessoa)){
 					unset($_SESSION['oAcessoPermissaoPessoa']);
 					$_SESSION['sMsg'] = "Permissão por usuário inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoPermissaoPessoa.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Permissão por usuário!";
 					$sHeader = "?bErro=1&action=AcessoPermissaoPessoa.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoPermissaoPessoa($oAcessoPermissaoPessoa)){
 					unset($_SESSION['oAcessoPermissaoPessoa']);
 					$_SESSION['sMsg'] = "Permissão por usuário alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoPermissaoPessoa.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Permissão por usuário!";
 					$sHeader = "?bErro=1&action=AcessoPermissaoPessoa.preparaFormulario&sOP=".$sOP."&nIdAcessoPermissaoPessoa=".$_POST['fCodPessoa']."||".$_POST['fCodTransacaoModulo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiAcessoPermissaoPessoa = explode("____",$_REQUEST['fIdAcessoPermissaoPessoa']);
   				foreach($vIdPaiAcessoPermissaoPessoa as $vIdFilhoAcessoPermissaoPessoa){
  					$vIdAcessoPermissaoPessoa = explode("||",$vIdFilhoAcessoPermissaoPessoa);
 					foreach($vIdAcessoPermissaoPessoa as $nIdAcessoPermissaoPessoa){
  						$bResultado &= $oFachada->excluirAcessoPermissaoPessoa($vIdAcessoPermissaoPessoa[0],$vIdAcessoPermissaoPessoa[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Permissão por usuário(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoPermissaoPessoa.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Permissão por usuário!";
 					$sHeader = "?bErro=1&action=AcessoPermissaoPessoa.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>