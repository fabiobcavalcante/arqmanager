<?php

class FachadaAcessoBD {
    /***
     *
     * Metodo para inicializar um Objeto AcessoGrupoUsuario
     *
     * @return AcessoGrupoUsuario
     */
    public function inicializarAcessoGrupoUsuario($nCodGrupoUsuario,$sDescricao,$nAtivo): AcessoGrupoUsuario
    {
        $oAcessoGrupoUsuario = new AcessoGrupoUsuario();

        $oAcessoGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
        $oAcessoGrupoUsuario->setDescricao($sDescricao);
        $oAcessoGrupoUsuario->setAtivo($nAtivo);
        return $oAcessoGrupoUsuario;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto AcessoGrupoUsuario
     *
     * @return boolean
     */
    public function inserirAcessoGrupoUsuario($oAcessoGrupoUsuario){
        $oPersistencia = new Persistencia($oAcessoGrupoUsuario);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto AcessoGrupoUsuario
     *
     * @return boolean
     */
    public function alterarAcessoGrupoUsuario($oAcessoGrupoUsuario){
        $oPersistencia = new Persistencia($oAcessoGrupoUsuario);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto AcessoGrupoUsuario
     *
     * @return boolean
     */
    public function excluirAcessoGrupoUsuario($nCodGrupoUsuario){
        $oAcessoGrupoUsuario = new AcessoGrupoUsuario();

        $oAcessoGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
        $oPersistencia = new Persistencia($oAcessoGrupoUsuario);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto AcessoGrupoUsuario na base de dados
     *
     * @return boolean
     */
    public function presenteAcessoGrupoUsuario($nCodGrupoUsuario){
        $oAcessoGrupoUsuario = new AcessoGrupoUsuario();

        $oAcessoGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
        $oPersistencia = new Persistencia($oAcessoGrupoUsuario);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto AcessoGrupoUsuario da base de dados
     *
     * @return AcessoGrupoUsuario|false
     */
    public function recuperarUmAcessoGrupoUsuario($nCodGrupoUsuario){
        $oAcessoGrupoUsuario = new AcessoGrupoUsuario();
        $oPersistencia = new Persistencia($oAcessoGrupoUsuario);
        $sTabelas = "acesso_grupo_usuario";
        $sCampos = "*";
        $sComplemento = " WHERE cod_grupo_usuario = $nCodGrupoUsuario";
        $voAcessoGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoGrupoUsuario)
            return $voAcessoGrupoUsuario[0];
        return false;
    }



    /**
     *
     * Método para recuperar um objeto AcessoGrupoUsuario da base de dados
     *
     * @return AcessoPermissao|false
     */
    public function recuperarUmPermissao($sAction,$sOP,$nCodGrupoUsuario){
        $oAcessoPermissao = new AcessoPermissao();
        $oPersistencia = new Persistencia($oAcessoPermissao);
        $sTabelas = "v_permissao";
        $sCampos = "*";
        $sComplemento = " WHERE responsavel='{$sAction}' and Operacao='{$sOP}' and cod_grupo_usuario ={$nCodGrupoUsuario}";
//        echo "Select $sCampos From $sTabelas $sComplemento";
        $voAcessoGrupoUsuario = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voAcessoGrupoUsuario)
            return $voAcessoGrupoUsuario[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos AcessoGrupoUsuario da base de dados
     *
     * @return AcessoGrupoUsuario[]|false
     */
    public function recuperarTodosAcessoGrupoUsuario(){
        $oAcessoGrupoUsuario = new AcessoGrupoUsuario();
        $oPersistencia = new Persistencia($oAcessoGrupoUsuario);
        $sTabelas = "acesso_grupo_usuario";
        $sCampos = "*";
        $sComplemento = "WHERE cod_grupo_usuario not in(1,14) and ativo=1";
        $voAcessoGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoGrupoUsuario)
            return $voAcessoGrupoUsuario;
        return false;
    }



    /**
     *
     * Método para inicializar um Objeto AcessoModulo
     *
     * @return AcessoModulo
     */
    public function inicializarAcessoModulo($nCodModulo,$sDescricao,$nAtivo){
        $oAcessoModulo = new AcessoModulo();

        $oAcessoModulo->setCodModulo($nCodModulo);
        $oAcessoModulo->setDescricao($sDescricao);
        $oAcessoModulo->setAtivo($nAtivo);
        return $oAcessoModulo;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto AcessoModulo
     *
     * @return boolean
     */
    public function inserirAcessoModulo($oAcessoModulo){
        $oPersistencia = new Persistencia($oAcessoModulo);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto AcessoModulo
     *
     * @return boolean
     */
    public function alterarAcessoModulo($oAcessoModulo){
        $oPersistencia = new Persistencia($oAcessoModulo);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto AcessoModulo
     *
     * @return boolean
     */
    public function excluirAcessoModulo($nCodModulo){
        $oAcessoModulo = new AcessoModulo();

        $oAcessoModulo->setCodModulo($nCodModulo);
        $oPersistencia = new Persistencia($oAcessoModulo);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto AcessoModulo na base de dados
     *
     * @return boolean
     */
    public function presenteAcessoModulo($nCodModulo){
        $oAcessoModulo = new AcessoModulo();

        $oAcessoModulo->setCodModulo($nCodModulo);
        $oPersistencia = new Persistencia($oAcessoModulo);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto AcessoModulo da base de dados
     *
     * @return AcessoModulo|false
     */
    public function recuperarUmAcessoModulo($nCodModulo){
        $oAcessoModulo = new AcessoModulo();
        $oPersistencia = new Persistencia($oAcessoModulo);
        $sTabelas = "acesso_modulo";
        $sCampos = "*";
        $sComplemento = " WHERE cod_modulo = $nCodModulo";
        $voAcessoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoModulo)
            return $voAcessoModulo[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos AcessoModulo da base de dados
     *
     * @return AcessoModulo[]|false
     */
    public function recuperarTodosAcessoModulo(){
        $oAcessoModulo = new AcessoModulo();
        $oPersistencia = new Persistencia($oAcessoModulo);
        $sTabelas = "acesso_modulo";
        $sCampos = "*";
        $sComplemento = "";
        $voAcessoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoModulo)
            return $voAcessoModulo;
        return false;
    }



    /**
     *
     * Método para inicializar um Objeto AcessoPermissao
     *
     * @return AcessoPermissao
     */
    public function inicializarAcessoPermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
        $oAcessoPermissao = new AcessoPermissao();

        $oAcessoPermissao->setCodTransacaoModulo($nCodTransacaoModulo);
        $oAcessoPermissao->setCodGrupoUsuario($nCodGrupoUsuario);
        return $oAcessoPermissao;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto AcessoPermissao
     *
     * @return boolean
     */
    public function inserirAcessoPermissao($oAcessoPermissao){
        $oPersistencia = new Persistencia($oAcessoPermissao);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto AcessoPermissao
     *
     * @return boolean
     */
    public function alterarAcessoPermissao($oAcessoPermissao){
        $oPersistencia = new Persistencia($oAcessoPermissao);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto AcessoPermissao
     *
     * @return boolean
     */
    public function excluirAcessoPermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
        $oAcessoPermissao = new AcessoPermissao();

        $oAcessoPermissao->setCodTransacaoModulo($nCodTransacaoModulo);
        $oAcessoPermissao->setCodGrupoUsuario($nCodGrupoUsuario);
        $oPersistencia = new Persistencia($oAcessoPermissao);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }


    public function excluirAcessoPermissaoPorGrupoUsuario($nCodGrupoUsuario){
        $oAcessoPermissao = new AcessoPermissao();
        $sComplemento = " WHERE cod_grupo_usuario = $nCodGrupoUsuario";
        $oPersistencia = new Persistencia($oAcessoPermissao);
        if($oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento))
            return true;
        else
            return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto AcessoPermissao na base de dados
     *
     * @return boolean
     */
    public function presenteAcessoPermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
        $oAcessoPermissao = new AcessoPermissao();

        $oAcessoPermissao->setCodTransacaoModulo($nCodTransacaoModulo);
        $oAcessoPermissao->setCodGrupoUsuario($nCodGrupoUsuario);
        $oPersistencia = new Persistencia($oAcessoPermissao);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto AcessoPermissao da base de dados
     *
     * @return AcessoPermissao|false
     */
    public function recuperarUmAcessoPermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
        $oAcessoPermissao = new AcessoPermissao();
        $oPersistencia = new Persistencia($oAcessoPermissao);
        $sTabelas = "acesso_permissao";
        $sCampos = "*";
        $sComplemento = " WHERE cod_transacao_modulo = $nCodTransacaoModulo AND cod_grupo_usuario = $nCodGrupoUsuario";
        $voAcessoPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoPermissao)
            return $voAcessoPermissao[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos AcessoPermissao da base de dados
     *
     * @return AcessoPermissao[]|false
     */
    public function recuperarTodosAcessoPermissao(){
        $oAcessoPermissao = new AcessoPermissao();
        $oPersistencia = new Persistencia($oAcessoPermissao);
        $sTabelas = "acesso_permissao";
        $sCampos = "*";
        $sComplemento = "";
        $voAcessoPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoPermissao)
            return $voAcessoPermissao;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos AcessoPermissao da base de dados
     *
     * @return AcessoPermissao[]|false
     */
    public function recuperarTodosAcessoPermissaoPorGrupoUsuario($nCodGrupoUsuario){
        $oAcessoPermissao = new AcessoPermissao();
        $oPersistencia = new Persistencia($oAcessoPermissao);
        $sTabelas = "acesso_permissao";
        $sCampos = "*";
        $sComplemento = "WHERE cod_grupo_usuario = $nCodGrupoUsuario";
        $voAcessoPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoPermissao)
            return $voAcessoPermissao;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto AcessoPermissaoPessoa
     *
     * @return AcessoPermissaoPessoa
     */
    public function inicializarAcessoPermissaoPessoa($nCodPessoa,$nCodTransacaoModulo): AcessoPermissaoPessoa
    {
        $oAcessoPermissaoPessoa = new AcessoPermissaoPessoa();

        $oAcessoPermissaoPessoa->setCodPessoa($nCodPessoa);
        $oAcessoPermissaoPessoa->setCodTransacaoModulo($nCodTransacaoModulo);
        return $oAcessoPermissaoPessoa;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto AcessoPermissaoPessoa
     *
     * @return boolean
     */
    public function inserirAcessoPermissaoPessoa($oAcessoPermissaoPessoa){
        $oPersistencia = new Persistencia($oAcessoPermissaoPessoa);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto AcessoPermissaoPessoa
     *
     * @return boolean
     */
    public function alterarAcessoPermissaoPessoa($oAcessoPermissaoPessoa){
        $oPersistencia = new Persistencia($oAcessoPermissaoPessoa);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto AcessoPermissaoPessoa
     *
     * @return boolean
     */
    public function excluirAcessoPermissaoPessoa($nCodPessoa,$nCodTransacaoModulo){
        $oAcessoPermissaoPessoa = new AcessoPermissaoPessoa();

        $oAcessoPermissaoPessoa->setCodPessoa($nCodPessoa);
        $oAcessoPermissaoPessoa->setCodTransacaoModulo($nCodTransacaoModulo);
        $oPersistencia = new Persistencia($oAcessoPermissaoPessoa);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto AcessoPermissaoPessoa na base de dados
     *
     * @return boolean
     */
    public function presenteAcessoPermissaoPessoa($nCodPessoa,$nCodTransacaoModulo){
        $oAcessoPermissaoPessoa = new AcessoPermissaoPessoa();

        $oAcessoPermissaoPessoa->setCodPessoa($nCodPessoa);
        $oAcessoPermissaoPessoa->setCodTransacaoModulo($nCodTransacaoModulo);
        $oPersistencia = new Persistencia($oAcessoPermissaoPessoa);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto AcessoPermissaoPessoa da base de dados
     *
     * @return AcessoPermissaoPessoa|false
     */
    public function recuperarUmAcessoPermissaoPessoa($nCodPessoa,$nCodTransacaoModulo){
        $oAcessoPermissaoPessoa = new AcessoPermissaoPessoa();
        $oPersistencia = new Persistencia($oAcessoPermissaoPessoa);
        $sTabelas = "acesso_permissao_pessoa";
        $sCampos = "*";
        $sComplemento = " WHERE cod_pessoa = $nCodPessoa AND cod_transacao_modulo = $nCodTransacaoModulo";
        $voAcessoPermissaoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoPermissaoPessoa)
            return $voAcessoPermissaoPessoa[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos AcessoPermissaoPessoa da base de dados
     *
     * @return AcessoPermissaoPessoa[]|false
     */
    public function recuperarTodosAcessoPermissaoPessoa(){
        $oAcessoPermissaoPessoa = new AcessoPermissaoPessoa();
        $oPersistencia = new Persistencia($oAcessoPermissaoPessoa);
        $sTabelas = "acesso_permissao_pessoa";
        $sCampos = "*";
        $sComplemento = "";
        $voAcessoPermissaoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoPermissaoPessoa)
            return $voAcessoPermissaoPessoa;
        return false;
    }



    /**
     *
     * Método para inicializar um Objeto AcessoResponsavelTransacao
     *
     * @return AcessoResponsavelTransacao
     */
    public function inicializarAcessoResponsavelTransacao($nCodResponsavelTransacao,$nCodTransacaoModulo,$sResponsavel,$sOperacao){
        $oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();

        $oAcessoResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
        $oAcessoResponsavelTransacao->setCodTransacaoModulo($nCodTransacaoModulo);
        $oAcessoResponsavelTransacao->setResponsavel($sResponsavel);
        $oAcessoResponsavelTransacao->setOperacao($sOperacao);
        return $oAcessoResponsavelTransacao;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto AcessoResponsavelTransacao
     *
     * @return boolean
     */
    public function inserirAcessoResponsavelTransacao($oAcessoResponsavelTransacao){
        $oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto AcessoResponsavelTransacao
     *
     * @return boolean
     */
    public function alterarAcessoResponsavelTransacao($oAcessoResponsavelTransacao){
        $oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto AcessoResponsavelTransacao
     *
     * @return boolean
     */
    public function excluirAcessoResponsavelTransacao($nCodResponsavelTransacao){
        $oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();

        $oAcessoResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
        $oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto AcessoResponsavelTransacao na base de dados
     *
     * @return boolean
     */
    public function presenteAcessoResponsavelTransacao($nCodResponsavelTransacao){
        $oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();

        $oAcessoResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
        $oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto AcessoResponsavelTransacao da base de dados
     *
     * @return AcessoResponsavelTransacao|false
     */
    public function recuperarUmAcessoResponsavelTransacao($nCodResponsavelTransacao){
        $oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();
        $oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
        $sTabelas = "acesso_responsavel_transacao";
        $sCampos = "*";
        $sComplemento = " WHERE cod_responsavel_transacao = $nCodResponsavelTransacao";
        $voAcessoResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoResponsavelTransacao)
            return $voAcessoResponsavelTransacao[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos AcessoResponsavelTransacao da base de dados
     *
     * @return AcessoResponsavelTransacao[]|false
     */
    public function recuperarTodosAcessoResponsavelTransacao(){
        $oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();
        $oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
        $sTabelas = "acesso_responsavel_transacao";
        $sCampos = "*";
        $sComplemento = "";
        $voAcessoResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoResponsavelTransacao)
            return $voAcessoResponsavelTransacao;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos AcessoResponsavelTransacao da base de dados
     *
     * @return AcessoResponsavelTransacao[]|false
     */
    public function recuperarTodosAcessoResponsavelTransacaoPorTransacao($nCodTransacaoModulo){
        $oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();
        $oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
        $sTabelas = "acesso_responsavel_transacao";
        $sCampos = "*";
        $sComplemento = "WHERE cod_transacao_modulo = " . $nCodTransacaoModulo ;
        $voAcessoResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoResponsavelTransacao)
            return $voAcessoResponsavelTransacao;
        return false;
    }
    /**
     *
     * Método para inicializar um Objeto AcessoTransacaoModulo
     *
     * @return AcessoTransacaoModulo
     */
    public function inicializarAcessoTransacaoModulo($nCodTransacaoModulo,$nCodModulo,$sDescricao,$nAtivo){
        $oAcessoTransacaoModulo = new AcessoTransacaoModulo();

        $oAcessoTransacaoModulo->setCodTransacaoModulo($nCodTransacaoModulo);
        $oAcessoTransacaoModulo->setCodModulo($nCodModulo);
        $oAcessoTransacaoModulo->setDescricao($sDescricao);
        $oAcessoTransacaoModulo->setAtivo($nAtivo);
        return $oAcessoTransacaoModulo;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto AcessoTransacaoModulo
     *
     * @return boolean
     */
    public function inserirAcessoTransacaoModulo($oAcessoTransacaoModulo){
        $oPersistencia = new Persistencia($oAcessoTransacaoModulo);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto AcessoTransacaoModulo
     *
     * @return boolean
     */
    public function alterarAcessoTransacaoModulo($oAcessoTransacaoModulo){
        $oPersistencia = new Persistencia($oAcessoTransacaoModulo);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto AcessoTransacaoModulo
     *
     * @return boolean
     */
    public function excluirAcessoTransacaoModulo($nCodTransacaoModulo){
        $oAcessoTransacaoModulo = new AcessoTransacaoModulo();

        $oAcessoTransacaoModulo->setCodTransacaoModulo($nCodTransacaoModulo);
        $oPersistencia = new Persistencia($oAcessoTransacaoModulo);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto AcessoTransacaoModulo na base de dados
     *
     * @return boolean
     */
    public function presenteAcessoTransacaoModulo($nCodTransacaoModulo){
        $oAcessoTransacaoModulo = new AcessoTransacaoModulo();

        $oAcessoTransacaoModulo->setCodTransacaoModulo($nCodTransacaoModulo);
        $oPersistencia = new Persistencia($oAcessoTransacaoModulo);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto AcessoTransacaoModulo da base de dados
     *
     * @return AcessoTransacaoModulo|false
     */
    public function recuperarUmAcessoTransacaoModulo($nCodTransacaoModulo){
        $oAcessoTransacaoModulo = new AcessoTransacaoModulo();
        $oPersistencia = new Persistencia($oAcessoTransacaoModulo);
        $sTabelas = "acesso_transacao_modulo";
        $sCampos = "*";
        $sComplemento = " WHERE cod_transacao_modulo = $nCodTransacaoModulo";
        $voAcessoTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoTransacaoModulo)
            return $voAcessoTransacaoModulo[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos AcessoTransacaoModulo da base de dados
     *
     * @return AcessoTransacaoModulo[]|false
     */
    public function recuperarTodosAcessoTransacaoModulo(){
        $oAcessoTransacaoModulo = new AcessoTransacaoModulo();
        $oPersistencia = new Persistencia($oAcessoTransacaoModulo);
        $sTabelas = "acesso_transacao_modulo";
        $sCampos = "*";
        $sComplemento = "";
        $voAcessoTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoTransacaoModulo)
            return $voAcessoTransacaoModulo;
        return false;
    }

    /***
     *
     * Método para recuperar um vetor de objetos TransacaoModulo da base de dados
     *
     * @return AcessoTransacaoModulo[]|false
     */
    public function recuperarTodosAcessoTransacaoModuloPorModulo($nCodModulo){
        $oAcessoTransacaoModulo = new AcessoTransacaoModulo();
        $oPersistencia = new Persistencia($oAcessoTransacaoModulo);
        $sTabelas = "acesso_transacao_modulo";
        $sCampos = "*";
        $sComplemento = "WHERE ativo=1 AND cod_modulo=".$nCodModulo;
        $voAcessoTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoTransacaoModulo)
            return $voAcessoTransacaoModulo;
        return false;
    }
    /* metodos adicionado dia 24 de novembro, copiados da FachadaPermissaoBD do projeto gdg antigo*/

    public function recuperarUmTransacaoModuloPorResponsavelOperacao($sResponsavel,$sOperacao){
        $oAcessoTransacaoModulo = new AcessoTransacaoModulo();
        $oPersistencia = new Persistencia($oAcessoTransacaoModulo);
        $sTabelas = "acesso_transacao_modulo";
        $sCampos = "acesso_transacao_modulo.*";
        $sComplemento = " INNER JOIN acesso_responsavel_transacao ON acesso_transacao_modulo.cod_transacao_modulo = acesso_responsavel_transacao.cod_transacao_modulo
						  WHERE responsavel = '$sResponsavel' AND operacao= '$sOperacao' AND acesso_transacao_modulo.ativo = 1";

//		echo "Select $sCampos From $sTabelas $sComplemento";

        $voTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voTransacaoModulo)
            return $voTransacaoModulo[0];
        return false;
    }



    /**
     *
     * Método para inicializar um Objeto AcessoUsuario
     *
     * @return AcessoUsuario
     */
    public function inicializarAcessoUsuario($nCodUsuario,$nCodGrupoUsuario,$sLogin,$sSenha,$sFoto,$sInseridoPor,$sAlteradoPor,$nIdEscritorio,$nAtivo){
        $oAcessoUsuario = new AcessoUsuario();

        $oAcessoUsuario->setCodUsuario($nCodUsuario);
        $oAcessoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
        $oAcessoUsuario->setLogin($sLogin);
        $oAcessoUsuario->setSenha($sSenha);
        $oAcessoUsuario->setFoto($sFoto);
        $oAcessoUsuario->setInseridoPor($sInseridoPor);
        $oAcessoUsuario->setAlteradoPor($sAlteradoPor);
        $oAcessoUsuario->setAtivo($nIdEscritorio);
        $oAcessoUsuario->setAtivo($nAtivo);
        return $oAcessoUsuario;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto AcessoUsuario
     *
     * @return boolean
     */
    public function inserirAcessoUsuario($oAcessoUsuario){
        $oPersistencia = new Persistencia($oAcessoUsuario);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto AcessoUsuario
     *
     * @return boolean
     */
    public function alterarAcessoUsuario($oAcessoUsuario){
        $oPersistencia = new Persistencia($oAcessoUsuario);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto AcessoUsuario
     *
     * @return boolean
     */
    public function excluirAcessoUsuario($nCodUsuario){
        $oAcessoUsuario = new AcessoUsuario();

        $oAcessoUsuario->setCodUsuario($nCodUsuario);
        $oPersistencia = new Persistencia($oAcessoUsuario);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto AcessoUsuario na base de dados
     *
     * @return boolean
     */
    public function presenteAcessoUsuario($nCodUsuario){
        $oAcessoUsuario = new AcessoUsuario();

        $oAcessoUsuario->setCodUsuario($nCodUsuario);
        $oPersistencia = new Persistencia($oAcessoUsuario);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto AcessoUsuario da base de dados
     *
     * @return AcessoUsuario|false
     */
    public function recuperarUmAcessoUsuario($nCodUsuario){
        $oAcessoUsuario = new AcessoUsuario();
        $oPersistencia = new Persistencia($oAcessoUsuario);
        $sTabelas = "acesso_usuario";
        $sCampos = "*";
        $sComplemento = " WHERE cod_usuario = $nCodUsuario";
        $voAcessoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoUsuario)
            return $voAcessoUsuario[0];
        return false;
    }

    /**
     *
     * Método para recuperar um objeto AcessoUsuario da base de dados
     *
     * @return AcessoUsuario|false
     */
    public function recuperarUmAcessoUsuarioPorLogin($sLogin){
        $oAcessoUsuario = new AcessoUsuario();
        $oPersistencia = new Persistencia($oAcessoUsuario);
        $sTabelas = "acesso_usuario";
        $sCampos = "*";
        $sComplemento = " WHERE login = '".$sLogin."'";
        $voAcessoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoUsuario)
            return $voAcessoUsuario[0];
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos AcessoUsuario da base de dados
     *
     * @return AcessoUsuario[]|false
     */
    public function recuperarTodosAcessoUsuario(){
        $oAcessoUsuario = new AcessoUsuario();
        $oPersistencia = new Persistencia($oAcessoUsuario);
        $sTabelas = "acesso_usuario";
        $sCampos = "cod_usuario,concat(acesso_usuario.cod_grupo_usuario,' - ',acesso_grupo_usuario.descricao) as cod_grupo_usuario,login,incluido_por,alterado_por";
        $sComplemento = "INNER JOIN acesso_grupo_usuario ON acesso_grupo_usuario.cod_grupo_usuario = acesso_usuario.cod_grupo_usuario
                         WHERE acesso_usuario.ativo=1
                        ";
        //  echo "SELECT ". $sCampos . " FROM ". $sTabelas ." ". $sComplemento;
        $voAcessoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voAcessoUsuario)
            return $voAcessoUsuario;
        return false;
    }
}
