<?php
 /**
  * @author Auto-Generated 
  * @package fachadaAcesso 
  * @SGBD mysql 
  * @tabela acesso_transacao_modulo 
  */
 class AcessoTransacaoModulo{
 	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodTransacaoModulo;
	/**
	* @campo cod_modulo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodModulo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oAcessoModulo;
	private $voResponsavelTransacao;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setCodModulo($nCodModulo){
		$this->nCodModulo = $nCodModulo;
	}
	public function getCodModulo(){
		return $this->nCodModulo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setAcessoModulo($oAcessoModulo){
		$this->oAcessoModulo = $oAcessoModulo;
	}
	public function getAcessoModulo(){
		$oFachada = new FachadaAcessoBD();
		$this->oAcessoModulo = $oFachada->recuperarUmAcessoModulo($this->getCodModulo());
		return $this->oAcessoModulo;
	}

	public function getResponsavelTransacao(){
		$oFachada = new FachadaAcessoBD();
		$this->voAcessoResponsavelTransacao = $oFachada->recuperarTodosAcessoResponsavelTransacaoPorTransacao($this->getCodTransacaoModulo());
		return $this->voAcessoResponsavelTransacao;
	}
     
     
 }
 ?>
