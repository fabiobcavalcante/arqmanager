<?php
 /**
  * @author Auto-Generated 
  * @package fachadaAcesso 
  * @SGBD mysql 
  * @tabela acesso_responsavel_transacao 
  */
 class AcessoResponsavelTransacao{
 	/**
	* @campo cod_responsavel_transacao
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodResponsavelTransacao;
	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodTransacaoModulo;
	/**
	* @campo responsavel
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sResponsavel;
	/**
	* @campo operacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sOperacao;
	private $oAcessoTransacaoModulo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodResponsavelTransacao($nCodResponsavelTransacao){
		$this->nCodResponsavelTransacao = $nCodResponsavelTransacao;
	}
	public function getCodResponsavelTransacao(){
		return $this->nCodResponsavelTransacao;
	}
	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setResponsavel($sResponsavel){
		$this->sResponsavel = $sResponsavel;
	}
	public function getResponsavel(){
		return $this->sResponsavel;
	}
	public function setOperacao($sOperacao){
		$this->sOperacao = $sOperacao;
	}
	public function getOperacao(){
		return $this->sOperacao;
	}
	public function setAcessoTransacaoModulo($oAcessoTransacaoModulo){
		$this->oAcessoTransacaoModulo = $oAcessoTransacaoModulo;
	}
	public function getAcessoTransacaoModulo(){
		$oFachada = new FachadaAcessoBD();
		$this->oAcessoTransacaoModulo = $oFachada->recuperarUmAcessoTransacaoModulo($this->getCodTransacaoModulo());
		return $this->oAcessoTransacaoModulo;
	}
	
 }
 ?>
