<?php
 /**
  * @author Auto-Generated 
  * @package fachadaAcesso 
  * @SGBD mysql 
  * @tabela acesso_permissao 
  */
 class AcessoPermissao{
 	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodTransacaoModulo;
	/**
	* @campo cod_grupo_usuario
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodGrupoUsuario;
	private $oAcessoGrupoUsuario;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}
	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaAcessoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getCodGrupoUsuario());
		return $this->oAcessoGrupoUsuario;
	}
	
 }
 ?>
