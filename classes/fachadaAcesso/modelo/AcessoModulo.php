<?php
 /**
  * @author Auto-Generated 
  * @package fachadaAcesso 
  * @SGBD mysql 
  * @tabela acesso_modulo 
  */
 class AcessoModulo{
 	/**
	* @campo cod_modulo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodModulo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
     
    private $voAcessoTransacaoModulo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodModulo($nCodModulo){
		$this->nCodModulo = $nCodModulo;
	}
	public function getCodModulo(){
		return $this->nCodModulo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
    public function getTransacaoModulo(){
    	$oFachada = new FachadaAcessoBD();
        $this->voAcessoTransacaoModulo = $oFachada->recuperarTodosAcessoTransacaoModuloPorModulo($this->nCodModulo);
        if($this->voAcessoTransacaoModulo)
        	return $this->voAcessoTransacaoModulo;
        else
            return false;
            

        }
 }
 ?>
