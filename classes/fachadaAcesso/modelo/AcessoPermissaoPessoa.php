<?php
 /**
  * @author Auto-Generated 
  * @package fachadaAcesso 
  * @SGBD mysql 
  * @tabela acesso_permissao_pessoa 
  */
 class AcessoPermissaoPessoa{
 	/**
	* @campo cod_pessoa
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodPessoa;
	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodTransacaoModulo;
	private $oAcessoUsuario;
	private $oAcessoTransacaoModulo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodPessoa($nCodPessoa){
		$this->nCodPessoa = $nCodPessoa;
	}
	public function getCodPessoa(){
		return $this->nCodPessoa;
	}
	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setAcessoUsuario($oAcessoUsuario){
		$this->oAcessoUsuario = $oAcessoUsuario;
	}
	public function getAcessoUsuario(){
		$oFachada = new FachadaAcessoBD();
		$this->oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($this->getCodPessoa());
		return $this->oAcessoUsuario;
	}
	public function setAcessoTransacaoModulo($oAcessoTransacaoModulo){
		$this->oAcessoTransacaoModulo = $oAcessoTransacaoModulo;
	}
	public function getAcessoTransacaoModulo(){
		$oFachada = new FachadaAcessoBD();
		$this->oAcessoTransacaoModulo = $oFachada->recuperarUmAcessoTransacaoModulo($this->getCodTransacaoModulo());
		return $this->oAcessoTransacaoModulo;
	}
	
 }
 ?>
