<?php
 /**
  * @author Auto-Generated
  * @package fachadaAcesso
  * @SGBD mysql
  * @tabela acesso_usuario
  */
 class AcessoUsuario{
 	/**
	* @campo cod_usuario
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodUsuario;
	/**
	* @campo cod_grupo_usuario
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodGrupoUsuario;
	/**
	* @campo login
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sLogin;
	/**
	* @campo senha
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sSenha;
  /**
	* @campo foto
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sFoto;
	/**
	* @campo incluido_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sInseridoPor;
	/**
	* @campo alterado_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAlteradoPor;
	/**
	* @campo id_escritorio
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nIdEscritorio;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oAcessoGrupoUsuario;
	private $oEscritorio;


 	public function __construct(){

 	}

 	public function setCodUsuario($nCodUsuario){
		$this->nCodUsuario = $nCodUsuario;
	}
	public function getCodUsuario(){
		return $this->nCodUsuario;
	}
	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}
	public function setLogin($sLogin){
		$this->sLogin = $sLogin;
	}
	public function getLogin(){
		return $this->sLogin;
	}

  public function setFoto($sFoto){
    $this->sFoto = $sFoto;
  }
  public function getFoto(){
    return $this->sFoto;
  }
	public function setSenha($sSenha){
		$this->sSenha = $sSenha;
	}
	public function getSenha(){
		return $this->sSenha;
	}
	public function setInseridoPor($sInseridoPor){
		$this->sInseridoPor = $sInseridoPor;
	}
	public function getInseridoPor(){
		return $this->sInseridoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}

	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaAcessoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getCodGrupoUsuario());
		return $this->oAcessoGrupoUsuario;
	}

 }
 ?>
