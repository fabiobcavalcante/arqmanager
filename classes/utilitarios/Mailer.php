<?php
require "vendor/autoload.php";

use Rain\Tpl;

class Mailer {
    // const USERNAME = "notificacao@luanapalheta.com.br";
    // const PASSWORD = "teste1234";

		const USERNAME = "notificacao@arqmanager.com.br";
		const PASSWORD = "@rqM4n8eR";

    //const NAME_FROM = $_SESSION['oEscritorio']->getNomeFantasia();
    // const REPLY = "contato@luanapalheta.com.br";
		// echo ($_SESSION['oEscritorio']->getCodEscritorio());
		// die();
    // const REPLY = $_SESSION['oEscritorio']->getEmail();
		private $sEmail;
		private $sEscritorio;

    public function enviar($sNome, $sEmail, $sAssunto, $tplName, $data = array())
    {
        $config = array(
            "base_url"      => null,
            "tpl_dir"       => "emails/",
            "cache_dir"     => "emails/",
            "debug"         => true // set to false to improve the speed
        );



        Tpl::configure($config);

        $tpl = new Tpl();


        foreach ($data as $key => $value) {
            $tpl->assign($key, $value);
        }

        //E-mail template
        $html = $tpl->draw($tplName, true);

        $mail = new PHPMailer();

        $mail->CharSet = 'UTF-8';
        $mail->IsSMTP();

        $mail->SMTPAuth  = true;

        $mail->Host = 'mail.luanapalheta.com.br';

        $mail->Port  = '465';

        $mail->SMTPSecure = 'ssl';

        $mail->Username  = Mailer::USERNAME;

        $mail->Password  = Mailer::PASSWORD;

        $mail->From  = Mailer::USERNAME;

				$this->sEmail = $_SESSION['oEscritorio']->getEmail();
				// $this->sEscritorio = utf8_encode($_SESSION['oEscritorio']->getNomeFantasia());
				$this->sEscritorio = $_SESSION['oEscritorio']->getNomeFantasia();

        $mail->FromName  = $this->sEscritorio;


        //E-mail de resposta
        $mail->addReplyTo($this->sEmail, $this->sEscritorio);

        $mail->isHTML();

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 0;

        //Subject e-mail
        // $mail->Subject =utf8_encode($sAssunto) ;
        $mail->Subject =$sAssunto;

        $mail->msgHTML($html);

        //Receiver data
        //$mail->AddAddress($sEmail,utf8_encode($sNome));
        $mail->AddAddress($sEmail,$sNome);

        try{

            $mail->send();
            $mail->ClearAddresses();

            $sMensagem = "E-mail enviado || {$sNome} || {$sEmail} || ".date("d/m/Y H:i:s");

            $fp = fopen('log_email.txt', 'a');//opens file in append mode
            fwrite($fp, "{$sMensagem}\n");
            fclose($fp);

         //   echo $sMensagem;

            return true;

        } catch (Exception $e) {

            return $e;

        }

    }

}
