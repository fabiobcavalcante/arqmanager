<?php
class Upload {

    var $vPermissao;
    var $vAceito;
    var $sDir;
    var $max_filesize;
    var $error = 0;
    var $file_type;
    var $file_name;
    var $nSize;
    var $arquivo;
    var $file_path;
    var $warning = 0;
    var $temp;
    var $sExtensao;
    var $nAltura,$nLargura;

    public function __construct($vPermissao=null,$nSize=null)
    {
        $this->max_filesize = ($nSize) ? $nSize : 4194304;

        if (!$vPermissao)
            $this->vPermissao = [
                "pdf"=>"application/pdf",
                "jpeg"=>"image/jpeg",
                "png"=>"image/png"
            ];
        else
            $this->vPermissao = $vPermissao;

        foreach ($this->vPermissao as $sKey=>$sPermissao) {
            $this->vAceito[] = $sKey;
        }
    }

    public function geraNomeArquivo($sNomeArquivo){

        return mb_strtolower(removeEspacos(removeAcentos($sNomeArquivo)), 'utf8');

    }

    function pegaExtensao($file)
    {
        $this->sExtensao = strrchr($file, '.');
    }

    public function geraNomeUnico($path, $name) {
        $name = str_replace(" ", "_", mb_eregi_replace('\s+', ' ', $name));

        $i = 0;
        $uniqueName = $name;
        while (file_exists("$path/$uniqueName".$this->sExtensao)) {
            $i++;
            $uniqueName =  "{$name}_$i";
        }
        return $uniqueName;
    }

    /**
     * @param $vFile
     * @param $sFolder
     * @param null $sArquivoDescricao
     * @return array
     * @throws Exception
     */
    public function saveFile($vFile, $sFolder, $sArquivoDescricao = null): array
    {
        $this->nSize = $vFile['size'];
        $this->file_type = strtok($vFile['type'], ";");
        $this->temp = $vFile['tmp_name'];

        //Valida se arquivo foi enviado
        if ($vFile['name'] == '')
            return $this->Error(11);

        //Validar erro do PHP
        if ($vFile['error'])
            return $this->Error($vFile['error']);

        //Validar extensão
        if (!in_array($this->file_type, $this->vPermissao))
            return $this->Error(12);

        //Validar tamanho do arquivo
        if ($this->nSize <= 0 || $this->nSize > $this->max_filesize)
            return $this->Error(2);

				$sDirUploads = "uploads/".$sFolder;
        $this->pegaExtensao($vFile['name']);

        if (!is_dir($sDirUploads))
            mkdir($sDirUploads, 0755, true);

        $sNomeArquivo = $this->geraNomeArquivo($sArquivoDescricao);
        $sNomeArquivo = $this->geraNomeUnico($_SERVER['DOCUMENT_ROOT']."/$sDirUploads",$sNomeArquivo);

        $sNomeArquivo = $sNomeArquivo.$this->sExtensao;

        if (move_uploaded_file($vFile['tmp_name'], $_SERVER['DOCUMENT_ROOT']."/".$sDirUploads."/".$sNomeArquivo)){
            $sUrl = "$sDirUploads/$sNomeArquivo";

            return ['url'=>$sUrl,"size"=>$this->nSize,"sucesso"=>1,"sMsg"=>"Arquivo enviado com sucesso!","sClass"=>"alert alert-success"];
        } else
            return $this->Error(3);
    }

    public function deleteFile($sArquivo): string
    {

        if (unlink($sArquivo))
            return "Arquivo apagado";
        else
            return "Erro ao apagar arquivo";

    }

    function Error($op): array
    {
        switch ($op){
            case 1:
            case 2:
                $this->error = "Erro 2: Erro no tamanho do arquivo: ".By2M($this->nSize) .". Tamanho máximo permitido é de ".By2M($this->max_filesize)." !"; break;
            case 3:
            case 4:
            case 6:
            case 7:
            case 8:
                $this->error = "Erro 3: Ocorreu um erro na transfêrencia do arquivo."; break;

            case 9: $this->error = "Erro 6: A altura do arquivo deve ser $this->nAltura px";break;
            case 10: $this->error = "Erro 7: A largura do arquivo deve ser $this->nLargura px";break;
            case 11: $this->error = "Nenhum arquivo foi anexado no formulário ou o arquivo é muito grande, tamanho máximo permitido é de ".By2M($this->max_filesize)." !";break;
            case 12: $this->error = "Erro 1: Este tipo de arquivo não tem permissão para upload. Formatos aceitos: [". implode(", ",$this->vAceito)."]"; break;
        }

        return ['url'=>'','size'=>'','sucesso'=>0,'sMsg'=>$this->error, "sClass"=>"alert alert-danger"];
    }
}
