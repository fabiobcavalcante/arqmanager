<?php
class EtapaCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $voEtapa = $oFachada->recuperarTodosEtapa($nIdEscritorio);
        $_REQUEST['voEtapa'] = $voEtapa;

        include_once("view/principal/etapa/index.php");
        exit();

    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $oEtapa = false;

        if($_REQUEST['sOP'] == "Alterar")
            $oEtapa = $oFachada->recuperarUmEtapa($_GET['nCodEtapa']);

        $_REQUEST['oEtapa'] = $oEtapa;

        include_once("view/principal/etapa/insere_altera.php");
        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $sHeader = "?action=Servico.preparaLista";;

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        if($sOP != "Excluir"){
            $oEtapa = $oFachada->inicializarEtapa($_POST['fCodEtapa'],$_POST['fDescricao'],$_POST['fOrdem'],$_POST['fExibir'],$nIdEscritorio);
        }

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirEtapa($oEtapa)){
                    unset($_SESSION['oEtapa']);
                    $_SESSION['sMsg'] = "Etapa inserida com sucesso!";
                    $sHeader = "?bErro=0&action=Servico.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "Não foi possível inserir a Etapa!";
                    $sHeader = "?bErro=1&action=Servico.preparaLista";
                }
                break;
            case "Alterar":
                if($oFachada->alterarEtapa($oEtapa)){
                    unset($_SESSION['oEtapa']);
                    $_SESSION['sMsg'] = "Etapa alterada com sucesso!";
                    $sHeader = "?bErro=0&action=Servico.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "Não foi possível alterar a Etapa!";
                    $sHeader = "?bErro=1&action=Servico.preparaLista";
                }
                break;
            case "Excluir":
                $nCodEtapa = $_GET['nCodEtapa'];

                $bResultado = $oFachada->excluirEtapa($nCodEtapa);

                if($bResultado){
                    $_SESSION['sMsg'] = "Etapa excluída com sucesso!";
                    $sHeader = "?bErro=0&action=Servico.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "Não foi possível excluir a Etapa!";
                    $sHeader = "?bErro=1&action=Servico.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);
    }
}