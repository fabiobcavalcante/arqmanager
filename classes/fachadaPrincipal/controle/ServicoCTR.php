<?php
class ServicoCTR implements IControle{

	public function preparaLista(){
		$oFachada = new FachadaPrincipalBD();
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
		$voServico = $oFachada->recuperarTodosServico($nIdEscritorio);
        $voEtapa = $oFachada->recuperarTodosEtapa($nIdEscritorio);
		$_REQUEST['voServico'] = $voServico;
        $_REQUEST['voEtapa'] = $voEtapa;

		include_once("view/principal/servico/index.php");
		exit();

	}

	public function preparaFormulario(){
		$oFachada = new FachadaPrincipalBD();

		$oServico = false;

		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
            $oServico = $oFachada->recuperarUmServico($_GET['nCodServico']);
		}

		$_REQUEST['oServico'] = $_SESSION['oServico'] ?? $oServico;
		unset($_SESSION['oServico']);


		if($_REQUEST['sOP'] == "Detalhar"){
			include_once("view/principal/servico/detalhe.php");
		}else
			include_once("view/principal/servico/insere_altera.php");

		exit();

	}

	public function processaFormulario(){
		$oFachada = new FachadaPrincipalBD();

		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
		if($sOP != "Excluir"){
			switch($sOP){
				case 'Cadastrar':
					$_POST['fInseridoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
					break;
				case 'Alterar':
					$oServico1 = $oFachada->recuperarUmServico($_POST['fCodServico']);
					$_POST['fInseridoPor'] =  $oServico1->getInseridoPor();
					$_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
					break;

			}
			$oServico = $oFachada->inicializarServico($_POST['fCodServico'],$_POST['fDescServico'],$_POST['fDescDocumento'],$_POST['fPercentualEmpresa'],$_POST['fInseridoPor'],$_POST['fAlteradoPor'],1,$nIdEscritorio);
			$_SESSION['oServico'] = $oServico;

			$oValidate = FabricaUtilitario::getUtilitario("Validate");
			$oValidate->check_4html = true;

			$oValidate->add_text_field("Descrição", $oServico->getDescServico());
			$oValidate->add_text_field("Inserido Por", $oServico->getInseridoPor());
			if($sOP == 'Alterar')
				$oValidate->add_text_field("Alterado Por", $oServico->getAlteradoPor());
			$oValidate->add_number_field("Ativo", $oServico->getAtivo());

			if (!$oValidate->validation()) {
				$_SESSION['sMsg'] = $oValidate->create_msg();
				$sHeader = "?bErro=1&action=Servico.preparaLista";
				header("Location: ".$sHeader);
				die();
			}
		}

		switch($sOP){
			case "Cadastrar":
				if($oFachada->inserirServico($oServico)){
					unset($_SESSION['oServico']);
					$_SESSION['sMsg'] = "Serviço inserido com sucesso!";
					$sHeader = "?bErro=0&action=Servico.preparaLista";

				} else {
					$_SESSION['sMsg'] = "Não foi possível inserir o Serviço!";
					$sHeader = "?bErro=1&action=Servico.preparaLista";
				}
				break;
			case "Alterar":
				if($oFachada->alterarServico($oServico)){
					unset($_SESSION['oServico']);
					$_SESSION['sMsg'] = "Serviço alterado com sucesso!";
					$sHeader = "?bErro=0&action=Servico.preparaLista";

				} else {
					$_SESSION['sMsg'] = "Não foi possível alterar o Serviço!";
					$sHeader = "?bErro=1&action=Servico.preparaLista";
				}
				break;
			case "Excluir":
                $nCodServico = $_GET['nCodServico'];
                
                $bResultado = $oFachada->excluirServico($nCodServico);

				if($bResultado){
					$_SESSION['sMsg'] = "Serviço(s) excluído(s) com sucesso!";
					$sHeader = "?bErro=0&action=Servico.preparaLista";
				} else {
					$_SESSION['sMsg'] = "Não foi possível excluir o(s) Serviço!";
					$sHeader = "?bErro=1&action=Servico.preparaLista";
				}
				break;
		}

		header("Location: ".$sHeader);
	}
}