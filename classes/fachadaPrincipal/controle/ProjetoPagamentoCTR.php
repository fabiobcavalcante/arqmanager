<?php
class ProjetoPagamentoCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $voProjetoPagamento = $oFachada->recuperarTodosProjetoPagamento($nIdEscritorio);
        $_REQUEST['voProjetoPagamento'] = $voProjetoPagamento;

        include_once("view/principal/projeto_pagamento/index.php");
        exit();

    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        if($_REQUEST['sOP'] == "ConfirmarPagamento" || $_REQUEST['sOP'] == "Detalhar"){
            $nCodProjetoPagamento = $_GET['nCodProjetoPagamento'];
            $_REQUEST['oProjetoPagamento'] = $oFachada->recuperarUmProjetoPagamento($nCodProjetoPagamento);
            $_REQUEST['voContaBancaria'] = $oFachada->recuperarTodosContaBancaria($nIdEscritorio);
            $_REQUEST['voFormaPagamento'] = $oFachada->recuperarTodosFormaPagamento();
        }

        if($_REQUEST['sOP'] == "Detalhar")
            include_once("view/principal/projeto_pagamento/detalhe.php");
        elseif ($_REQUEST['sOP'] == "ConfirmarPagamento")
            include_once("view/principal/projeto_pagamento/form_ajax.php");
        else
            include_once("view/principal/projeto_pagamento/insere_altera.php");

        exit();

    }

    /**
     * @throws Exception
     */
    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
        $oProjetoPagamento = null;
        $dDataAtual = date("d/m/Y");

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirProjetoPagamento($oProjetoPagamento)){
                    unset($_SESSION['oProjetoPagamento']);
                    $_SESSION['sMsg'] = "Pagamento do Projeto inserido com sucesso!";
                    $sHeader = "?bErro=0&action=ProjetoPagamento.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "Não foi possível inserir o Pagamento do Projeto!";
                    $sHeader = "?bErro=1&action=ProjetoPagamento.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Alterar":
                $i=0;
                foreach($_POST['fCodProjetoPagamento'] as $nCodProjetoPagamento){
                    if(!$_POST['fDataEfetivacao'][$i]){
                        $oProjetoPagamento = $oFachada->inicializarProjetoPagamento($nCodProjetoPagamento,$_POST['fCodProjeto'],$_POST['fCodFormaPagamento'][$i],$_POST['fValor'][$i],$_POST['fDataPrevisao'][$i],$_POST['fDataEfetivacao'][$i],$_POST['fComprovante'][$i],$_POST['fDescPagamento'][$i],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1);
                        if($nCodProjetoPagamento !=""){
                            $oProjetoPagamento1 = $oFachada->recuperarUmProjetoPagamento($nCodProjetoPagamento);
                            $oProjetoPagamento->setIncluidoPor($oProjetoPagamento1->getIncluidoPor());
                            $oProjetoPagamento->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
                            $oFachada->alterarProjetoPagamento($oProjetoPagamento);
                        }else{
                            $oProjetoPagamento->setIncluidoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
                            $oFachada->inserirProjetoPagamento($oProjetoPagamento);
                        }
                    }
                    $i++;
                }

                $_SESSION['sMsg'] = "Pagamento do Projeto alterado com sucesso!";
                unset($_SESSION['oProjetoPagamento']);
                $sHeader = "?bErro=0&action=Movimento.preparaFormulario&sOP=Cadastrar";

                break;
            case "ConfirmarPagamento":
                $bErro=0;
                $oProjetoPagamento = $oFachada->recuperarUmProjetoPagamento($_REQUEST['nCodProjetoPagamento']);

                $oProjeto = $oProjetoPagamento->getProjeto();
                $oCliente = $oProjeto->getCliente();
                $oProposta =  $oProjeto->getProposta();

                $dDataConfirmacao = $_REQUEST['fDataConfirmacao'];
                $oProjetoPagamento->setDataEfetivacaoBanco($dDataConfirmacao);
                $vUpload = $this->arquivo($_REQUEST['nCodProjetoPagamento']);

                if ($vUpload['sucesso'] == 1){
                    $oProjetoPagamento->setComprovante($vUpload['url']);

                    if(!$_REQUEST['sLink']){
                        $oProjetoPagamento->setCodFormaPagamento($_REQUEST['fCodFormaPagamento']);
                        $oProjetoPagamento->setValorBanco($_REQUEST['fValor']);
                        $oProjetoPagamento->setDataPrevisaoBanco($_REQUEST['fDataPrevisao']);
                        $oProjetoPagamento->setDescPagamento($_REQUEST['fDescPagamento']);
                    }

                    $dDataPrimeiroPagamento = $oFachada->recuperarUmProjetoPagamentoEfetivadoProjeto($oProjetoPagamento->getCodProjeto());
                    $oProjetoPagamento->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
                    if($_REQUEST['fCodContaBancaria']){

                        if($oFachada->alterarProjetoPagamento($oProjetoPagamento)){

                            if(!$dDataPrimeiroPagamento){
//                            fazer alteração de data inicio do projeto e atualizar Prazos
                                $oProjeto->setDataInicioBanco($dDataConfirmacao);
                                $voEtapa = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta(null,$oProjeto->getCodProjeto());
                                foreach ($voEtapa as $nKey => $oEtapa) {
                                    $nSumDias = 0;
                                    $nCount = 1;
                                    $voMicroservico = $oFachada->recuperarTodosPropostaMicroServicoPorProjetoEtapaView($oProjeto->getCodProjeto(),$oEtapa->getCodEtapa());
                                    $nMicroservico = ($voMicroservico) ? count($voMicroservico) : 0;
                                    if($voMicroservico){
                                        foreach ($voMicroservico as $oMicroServico) {
                                            if ($nMicroservico > 0)
                                                $nDias = $oEtapa->getPrazo() / $nMicroservico;
                                            else
                                                $nDias = 0;

                                            $nDias = (int)$nDias;

                                            if ($nCount == $nMicroservico) {
                                                $nDias = ($nSumDias < $oEtapa->getPrazo()) ? ($oEtapa->getPrazo() - $nSumDias) : $nDias;
                                            }

                                            if (!$oMicroservico->dias) {
                                                $oPropostaMicroServico = $oFachada->recuperarUmPropostaMicroServico($oMicroServico->cod_microservico,$oMicroServico->cod_proposta);
                                                $oPropostaMicroServico->setDias($nDias);
                                                $oFachada->alterarPropostaMicroServico($oPropostaMicroServico);
                                            }

                                            $nSumDias += (int)$nDias;
                                            $nCount+=1;
                                        }
                                    }
                                }

                                if ($_SESSION['oEscritorio']->getIdEscritorio() == 1){
                                    $oFachada->atualizarPrazosProjeto($oProjeto->getCodProjeto(),1);
                                    $oFachada->atualizarPrazosProjeto($oProjeto->getCodProjeto(),2);
                                }

                                if($sDataPrevisaoFimProjeto = $oFachada->recuperarUmPropostaMicroServicoPrazoMaximoPorProjeto($oProjeto->getCodProjeto())){
                                    //recuperar ultima data de entrega do microservico para atualização do final do projeto
                                    $oProjeto->setDataPrevisaoFim($sDataPrevisaoFimProjeto);
                                    $bResultado = $oFachada->alterarProjeto($oProjeto);
                                }
                            }

                            $nCodContaBancaria = $_REQUEST['fCodContaBancaria'];
                            $nNovoSaldoInicial = $oFachada->recuperarUmContaBancariaUltimoSaldo($nCodContaBancaria)->getValorFormatado();
                            $nValorReceita = $oProjetoPagamento->getValorFormatado();
                            $oContaMovimentacao = $oFachada->inicializarContaMovimentacao(null,$nCodContaBancaria,'E',$nNovoSaldoInicial,NULL,$nValorReceita,$dDataConfirmacao,1,NULL,$oProjetoPagamento->getCodProjetoPagamento());

                            $oFachada->inserirContaMovimentacao($oContaMovimentacao);
                            $_SESSION['sMsg'] = "Pagamento do Projeto confirmado com sucesso!";
                            $_SESSION['oEscritorio']->setEnvioEmail('N');
                            if($_SESSION['oEscritorio']->getEnvioEmail() == 'S'){
                                //preparando template para envio de email
                                $nCodTipoEmail=5; // comprovante de pagamento
                                $oEmailTipo = $oFachada->recuperarUmEmailTipo($nCodTipoEmail);
                                $oEmailModeloEscritorio = $oFachada->recuperarUmEmailTipoEscritorio($nCodTipoEmail,$oProposta->getIdEscritorio());
                                if($oEmailModeloEscritorio){
                                    $sConteudo = $oEmailModeloEscritorio->getTemplate();
                                    $sConteudo = str_replace("#SAUDACAO#","{$oCliente->getNome()} ",$sConteudo);
                                    $sConteudo = str_replace("#DESC_PROJETO#","{$oProposta->getIdentificacao()} ",$sConteudo);
                                    $sConteudo = str_replace("#VENCIMENTO#","{$oProjetoPagamento->getDataPrevisaoFormatado()} ",$sConteudo);
                                    $sConteudo = str_replace("#DESC_PAGAMENTO#","{$oProjetoPagamento->getDescPagamento()} ",$sConteudo);
                                    $sConteudo = str_replace("#DATA_CONFIRMACAO#","{$oProjetoPagamento->getDataEfetivacaoFormatado()} ",$sConteudo);
                                    $sConteudo = str_replace("#FORMA_PAGAMENTO#","{$oProjetoPagamento->getFormaPagamento()->getDescricao()} ",$sConteudo);
                                    $sConteudo = str_replace("#VALOR#","{$oProjetoPagamento->getValorFormatado()} ",$sConteudo);
                                    $sConteudo = htmlspecialchars($sConteudo);
                                    // Envio de EMAIL
                                    $oMailer = new Mailer();
                                    if($_SESSION['oEscritorio']->getEnvioEmail() == 'S'){
                                        $sEmail = $oCliente->getEmail();
                                        //      }else{
                                         //$sEmail = "bc_fabio@hotmail.com";
                                        //        }
                                        // montando o cabecalho do email
                                        $sCabecalho = $_SESSION['oEscritorio']->getCabecalhoEmail();
                                        $sSite = ($_SESSION['oEscritorio']->getSite()) ? $_SESSION['oEscritorio']->getSite() : "http://www.arqmanager.com.br";
                                        $sCabecalho = str_replace("#SITE#","{$sSite} ",$sCabecalho);
                                        $sCabecalho = str_replace("#LOGO#","{$_SESSION['oEscritorio']->getLogomarca()} ",$sCabecalho);
                                        // montando o rodape do email
                                        $sRodape = $_SESSION['oEscritorio']->getRodapeEmail();
                                        $sRodape = str_replace("#ESCRITORIO#","{$_SESSION['oEscritorio']->getNomeFantasia()} ",$sRodape);
                                        $sRodape = str_replace("#ENDERECO_REDUZIDO#","{$_SESSION['oEscritorio']->getEnderecoReduzido()} ",$sRodape);

                                        $sAssunto = "[{$_SESSION['oEscritorio']->getNomeFantasia()}] {$oEmailTipo->getTitulo()}";
                                        $oMailer->enviar("{$oCliente->getNome()}", "{$sEmail}", "{$sAssunto}", "template", ["sCabecalho"=>$sCabecalho,"sConteudo"=>$sConteudo,"sRodape"=>$sRodape]);
                                        // Inserir na Tabela de Email
                                        $sDescricaoEmail = $oProposta->getIdentificacao() . " - " . $oProjetoPagamento->getDescPagamento();
                                        $dDataHora = date('Y-m-d H:i:s');
                                        $oEmail = $oFachada->inicializarEmail(null,$oCliente->getCodCliente(),$nCodTipoEmail,$sDescricaoEmail,$dDataHora);
                                        $oFachada->inserirEmail($oEmail);
                                    }
                                }
                            }

                            if($_REQUEST['fOrigem'] == 'projeto'){
                                $sHeader = "?bErro=".$bErro."&action=Projeto.preparaFormulario&sOP=Alterar&nIdProjeto=".$oProjetoPagamento->getCodProjeto();
                                header("Location: ".$sHeader);
                            }
                        } else {
                            $bErro=1;
                            $_SESSION['sMsg'] = "Não foi possível Confirmar o Pagamento do Projeto!";
                        }
                    }else{
                        $bErro=1;
                        $_SESSION['sMsg'] = "Não foi possível Confirmar o Pagamento do Projeto!(Conta não especificada)";
                    }

                    if($_REQUEST['sLink'] == 'Movimento'){
                        $sHeader = "?bErro=".$bErro."&action=Movimento.preparaFormulario&sOP=Cadastrar";
                    }elseif($_REQUEST['sDestino'] == 'Financeiro') {
                        $sClass = ($bErro) ? "alert alert-danger" : "alert alert-success";
                        $sMsg = $_SESSION['sMsg'];
                        unset($_SESSION['sMsg']);
                        echo json_encode(['sMsg'=>$sMsg,'sClass'=>$sClass]);
                        die();
                    }
                } else {
                    $bErro=1;
                    $_SESSION['sMsg'] = $vUpload['sMsg'];
                }

                $sHeader = "?bErro=".$bErro."&action=Projeto.preparaFormulario&sOP=Alterar&nIdProjeto=".$oProjetoPagamento->getCodProjeto();
                unset($_SESSION['oProjetoPagamento']);
                break;

            case "ReenviarComprovante":
                $oProjetoPagamento = $oFachada->recuperarUmProjetoPagamento($_REQUEST['nIdComprovante']);
                $oProjeto = $oProjetoPagamento->getProjeto();
                $oProposta = $oProjeto->getProposta();
                $oCliente = $oProjeto->getCliente();
                //preparando template para envio de email
                $nCodTipoEmail=5; // comprovante de pagamento
                $oEmailModelo = $oFachada->recuperarUmEmailTipo($nCodTipoEmail);
                $sConteudo = $oEmailModelo->getTemplate();
                $sConteudo = str_replace("#SAUDACAO#","{$oCliente->getNome()} ",$sConteudo);
                $sConteudo = str_replace("#DESC_PROJETO#","{$oProposta->getIdentificacao()} ",$sConteudo);
                $sConteudo = str_replace("#VENCIMENTO#","{$oProjetoPagamento->getDataPrevisaoFormatado()} ",$sConteudo);
                $sConteudo = str_replace("#DESC_PAGAMENTO#","{$oProjetoPagamento->getDescPagamento()} ",$sConteudo);
                $sConteudo = str_replace("#DATA_CONFIRMACAO#","{$oProjetoPagamento->getDataEfetivacaoFormatado()} ",$sConteudo);
                $sConteudo = str_replace("#FORMA_PAGAMENTO#","{$oProjetoPagamento->getFormaPagamento()->getDescricao()} ",$sConteudo);
                $sConteudo = str_replace("#VALOR#","{$oProjetoPagamento->getValorFormatado()} ",$sConteudo);
                $sConteudo = htmlspecialchars($sConteudo);
                // Envio de EMAIL
                $oMailer = new Mailer();

                $sEmail = $oCliente->getEmail();
                // $sEmailOculto = "bc_fabio@hotmail.com";
                // $mail->addBCC("{$sEmailOculto}");
                $sAssunto = "[{$_SESSION['oEscritorio']->getNomeFantasia()}] {$oEmailModelo->getTitulo()}";
                if($oMailer->enviar("{$oCliente->getNome()}", "{$sEmail}", "{$sAssunto}", "template", ["sConteudo"=>$sConteudo])){
                    // Inserir na Tabela de Email
                    $sDescricaoEmail = $oProposta->getIdentificacao() . " - " . $oProjetoPagamento->getDescPagamento();
                    $dDataHora = date('Y-m-d H:i:s');
                    $oEmail = $oFachada->inicializarEmail(null,$oCliente->getCodCliente(),$nCodTipoEmail,$sDescricaoEmail,$dDataHora);
                    $oFachada->inserirEmail($oEmail);
                    $_SESSION['sMsg'] = "Comprovante enviado para o email {$oCliente->getEmail()}";
                    $sHeader = "?bErro=0&action=Projeto.preparaFormulario&sOP=Alterar&nIdProjeto=".$oProjetoPagamento->getCodProjeto();
                }else{
                    $_SESSION['sMsg'] = "Não foi possível enviar comprovante para o email {$oCliente->getEmail()}";
                    $sHeader = "?bErro=1&action=Projeto.preparaFormulario&sOP=Alterar&nIdProjeto=".$oProjetoPagamento->getCodProjeto();
                }


                header("Location: ".$sHeader);

                break;
            case "Excluir":
                //$bResultado = true;

                $oProjetoPagamento = $oFachada->recuperarUmProjetoPagamento($_GET['nCodProjetoPagamento']);
                $nCodProjeto = $oProjetoPagamento->getCodProjeto();
                // verificar se pagamento foi confirmado, caso sim não pode ser Excluido
                if(!$oProjetoPagamento->getDataEfetivacao()){
                    if($oFachada->excluirProjetoPagamento($oProjetoPagamento->getCodProjetoPagamento())){
                        $_SESSION['sMsg'] = "Pagamento do Projeto(s) exclu&iacute;do(s) com sucesso!";
                    } else {
                        $_SESSION['sMsg'] = "Não foi possível excluir o(s) Pagamento do Projeto!";
                    }
                }else{
                    $_SESSION['sMsg'] = "Este pagamento já foi confirmado e não pode ser Excluído!";
                }

                if($_GET['nPagina'] == 1)
                    $sHeader = "?bErro=0&action=Movimento.preparaFormulario&sOP=Cadastrar";
                else
                    $sHeader = "?bErro=0&action=Projeto.preparaFormulario&sOP=Alterar&nIdProjeto=".$nCodProjeto;
                break;
        }

        header("Location: ".$sHeader);

    }

    /**
     * @throws Exception
     */
    public function arquivo($nCodProjetoPagamento): array
    {
        if ($_FILES['fComprovante']){
            $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
            $sUrl = $_SERVER['HTTP_HOST'];

            if ($sUrl == 'sistema.arqmanager.com.br'){
                $sDiretorio = "$nIdEscritorio/comprovantes";
            }else{
                $sDiretorio = "uploads/$nIdEscritorio/comprovantes";
            }

            //VERIFICA EXISTENCIA DE DirectorIO
            if (!file_exists($sDiretorio)){
                mkdir("$sDiretorio", 0700);
            }
            // ANTIGO 12000000
            $oUpload = new Upload(null,2097152);
            return $oUpload->saveFile($_FILES['fComprovante'],$sDiretorio,"comprovante_$nCodProjetoPagamento");

        } else
            return ['url'=>'',"size"=>'',"sucesso"=>0,"sMsg"=>"Erro ao enviar arquivo!","sClass"=>"alert alert-danger"];
    }

}
