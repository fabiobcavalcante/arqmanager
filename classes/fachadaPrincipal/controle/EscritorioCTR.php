<?php
 class EscritorioCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
 		$voEscritorio = $oFachada->recuperarTodosEscritorio();
 		$_REQUEST['voEscritorio'] = $voEscritorio;

 		include_once("view/principal/escritorio/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$oEscritorio = false;
		$nIdEscritorio1 = $_SESSION['oEscritorio']->getIdEscritorio();

    $voEmailTipoEscritorio = $oFachada->recuperarTodosEmailTipoPorEscritorio($nIdEscritorio1);

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdEscritorio = ($_POST['fIdEscritorio'][0]) ? $_POST['fIdEscritorio'][0] : $_GET['nIdEscritorio'];
 			if($nIdEscritorio){
 				$vIdEscritorio = explode("||",$nIdEscritorio);
 				$oEscritorio = $oFachada->recuperarUmEscritorio($vIdEscritorio[0]);
 			}
 		}

 		$_REQUEST['oEscritorio'] = ($oEscritorio) ? $oEscritorio : $_SESSION['oEscritorio'];
 		 unset($_SESSION['oEscritorio1']);
     $_REQUEST['voEmailTipo'] = $voEmailTipoEscritorio;// $oFachada->recuperarTodosEmailTipo();
     $_REQUEST['voEmailTipoEscritorio'] = $voEmailTipoEscritorio;


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/escritorio/detalhe.php");
 		else
 			include_once("view/principal/escritorio/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 		if($sOP != "Excluir"){
 			$oEscritorio = $oFachada->inicializarEscritorio($_POST['fIdEscritorio'],$_POST['fNomeFantasia'],$_POST['fRazaoSocial'],$_POST['fDocumento'],$_POST['fEnderecoReduzido'],$_POST['fFone'],$_POST['fEnderecoCompleto'],$_POST['fSite'],$_POST['fEmail'],$_POST['fInstagram'],$_POST['fEmailAdministrador'],$_POST['fLogomarca'],$_POST['fLogomarcaMini'],$_POST['fCabecalhoEmail'],$_POST['fRodapeEmail'],$_POST['fDiasUteis'],$_POST['fEnvioEmail'],$_POST['fHoraTecnica'],$_POST['fCidadeUf'],$_POST['fAssinatura']);
 			$_SESSION['oEscritorio'] = $oEscritorio;
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_text_field("NomeFantasia", $oEscritorio->getNomeFantasia(), "text", "y");
			//$oValidate->add_text_field("RazaoSocial", $oEscritorio->getRazaoSocial(), "text", "y");
			$oValidate->add_text_field("Documento", $oEscritorio->getDocumento(), "text", "y");
			//$oValidate->add_text_field("EnderecoReduzido", $oEscritorio->getEnderecoReduzido(), "text", "y");
			//$oValidate->add_text_field("Fone", $oEscritorio->getFone(), "text", "y");
			//$oValidate->add_text_field("EnderecoCompleto", $oEscritorio->getEnderecoCompleto(), "text", "y");
			//$oValidate->add_text_field("Site", $oEscritorio->getSite(), "text", "y");
			$oValidate->add_text_field("Email", $oEscritorio->getEmail(), "text", "y");
			$oValidate->add_text_field("Administrador", $oEscritorio->getEmailAdministrador(), "text", "y");
			//$oValidate->add_text_field("Instagram", $oEscritorio->getInstagram(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Escritorio.preparaFormulario&sOP=".$sOP."&nIdEscritorio=".$_POST['fIdEscritorio']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirEscritorio($oEscritorio)){
 					// unset($_SESSION['oEscritorio']);
 					$_SESSION['sMsg'] = "Escritório inserido com sucesso!";
 					$sHeader = "?bErro=0&action=Escritorio.preparaLista";

          foreach ($_POST['fCodEmailTipo'] as $nCodEmailTipo) {
            $oEmailTipo = $oFachada->inserirEmailTipo($nCodEmailTipo);
            $oEmailTipoEscritorio = $oFachada->inicializarEmailTipoEscritorio($nCodEmailTipo,$oEscritorio->getIdEscritorio(),$oEmailTipo->getTemplate(),1);
            $oFachada->inserirEmailTipoEscritorio($oEmailTipoEscritorio);
          }

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Escritório!";
 					$sHeader = "?bErro=1&action=Escritorio.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
        $bErro=0;
 				if($oFachada->alterarEscritorio($oEscritorio)){
          if($_POST['fCodEmailTipo']){ //marcado

            $voEmailTipoEscritorio = $oFachada->recuperarTodosEmailTipoPorEscritorio($oEscritorio->getIdEscritorio()); //banco
            if($voEmailTipoEscritorio){
              $aIdTipoEscritorio = array();
              foreach ($voEmailTipoEscritorio as $oEmailEscritorio) {
                $aIdTipoEscritorio[] = $oEmailEscritorio->getCodEmailTipo();
                $oEmailTipo = $oFachada->recuperarUmEmailTipo($oEmailEscritorio->getCodEmailTipo());
                if(!in_array($oEmailEscritorio->getCodEmailTipo(),$_POST['fCodEmailTipo'])){
                    //verifica se existe
                    if($oJaExiste = $oFachada->recuperarUmEmailTipoEscritorio($oEmailEscritorio->getCodEmailTipo(),$oEscritorio->getIdEscritorio())){
                      $oJaExiste->setAtivo(0);
                      if(!$oFachada->alterarEmailTipoEscritorio($oJaExiste))
                        $bErro++;
                    }
                }else{
                  if($oJaExiste = $oFachada->recuperarUmEmailTipoEscritorio($oEmailEscritorio->getCodEmailTipo(),$oEscritorio->getIdEscritorio())){
                    $oJaExiste->setAtivo(1);
                    if(!$oFachada->alterarEmailTipoEscritorio($oJaExiste))
                      $bErro++;
                  }
                }
              }
            }

            foreach($_POST['fCodEmailTipo'] as $nCodTipoNovo){
              if(!in_array($nCodTipoNovo,$aIdTipoEscritorio)){
                $oEmailTipoNovo = $oFachada->recuperarUmEmailTipo($nCodTipoNovo);
                $oEmailTipoEscritorio = $oFachada->inicializarEmailTipoEscritorio($nCodTipoNovo,$oEscritorio->getIdEscritorio(),$oEmailTipoNovo->getTemplate(),1);
                if($oFachada->inserirEmailTipoEscritorio($oEmailTipoEscritorio))
                  $bErro++;
              }
            }
          }
        }

 					if($bErro==0){
   					$_SESSION['sMsg'] = "Escritório alterado com sucesso!";
 				  } else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Escritório!";
 					$bErro=1;
 				}

        $sHeader = "?bErro={$bErro}&action=Escritorio.preparaFormulario&sOP=".$sOP."&nIdEscritorio=".$_POST['fIdEscritorio'];
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiEscritorio = explode("____",$_REQUEST['fIdEscritorio']);
   				foreach($vIdPaiEscritorio as $vIdFilhoEscritorio){
  					$vIdEscritorio = explode("||",$vIdFilhoEscritorio);
 					foreach($vIdEscritorio as $nIdEscritorio){
  						$bResultado &= $oFachada->excluirEscritorio($vIdEscritorio[0],$vIdEscritorio[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Escritório(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=Escritorio.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Escritório!";
 					$sHeader = "?bErro=1&action=Escritorio.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}
 }
 ?>
