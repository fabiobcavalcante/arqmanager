<?php
class NotificacaoCTR implements IControle{

    public function preparaLista()
    {
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $_REQUEST['voNotificacao'] = $oFachada->recuperarTodosNotificacao($nIdEscritorio);

        include_once("view/principal/notificacao/index.php");
        exit();
    }

    public function preparaFormulario()
    {


    }

    public function processaFormulario()
    {


    }

}
