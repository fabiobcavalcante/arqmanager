<?php
class MovimentoCTR implements IControle
{

    public function preparaLista()
    {
        $oFachada = new FachadaPrincipalBD();
        $sDescricao = "";

        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $sComplemento = "id_escritorio={$nIdEscritorio} ";

        if ($_REQUEST['fEntradaSaida']) {
            $sComplemento .= " AND tipo ='" . $_REQUEST['fEntradaSaida'] . "'";
            $sDescricao .= "[ Tipo: <strong>" . $_REQUEST['fEntradaSaida'] . "</strong>] ";
        }
        if ($_REQUEST['fPagamento']) {
            //  $sComplemento .= " AND '" . $_REQUEST['fPagamento'] . "''";
            $sCampoData = ($_REQUEST['fPagamento'] == 'Efetivado') ? " data_efetivacao " : " data ";

            if ($_REQUEST['fPagamento'] == 'Previsto') {
                $sComplemento .= " AND data_efetivacao is null";
            }
            $sDescricao .= "[ Pagamento: <strong>" . $_REQUEST['fPagamento'] . "</strong>] ";

        }

        if ($_REQUEST['fPeriodo']) {
            $sDescricao .= " [Período: <strong>" . $_REQUEST['fPeriodo'] . "</strong>] ";
            $vPeriodo = explode(' - ', $_REQUEST['fPeriodo']);
            $vDataInicial = explode('/', $vPeriodo[0]);
            $sDataInicial = $vDataInicial[2] . '-' . $vDataInicial[1] . '-' . $vDataInicial[0];
            $vDataFinal = explode('/', $vPeriodo[1]);
            $sDataFinal = $vDataFinal[2] . '-' . $vDataFinal[1] . '-' . $vDataFinal[0];
            $sComplemento .= " AND " . $sCampoData . " BETWEEN '" . $sDataInicial . "' AND '" . $sDataFinal . "'";
        }

        $_REQUEST['sPesquisa'] = $sDescricao;
        $voMovimento = $oFachada->recuperarTodosMovimentoComplemento($sComplemento);

        $nReceber = 0;
        $nRecebido = 0;
        $nPagar = 0;
        $nPago = 0;
        if ($voMovimento) {
            foreach ($voMovimento as $oMovimento) {
                if ($oMovimento->tipo == 'SAIDA') {
                    if ($oMovimento->data_efetivacao == null)
                        $nPagar += $oMovimento->valor;
                    if ($oMovimento->data_efetivacao != null)
                        $nPago += $oMovimento->valor;
                } else {
                    if ($oMovimento->data_efetivacao == null)
                        $nReceber += $oMovimento->valor;
                    if ($oMovimento->data_efetivacao != null)
                        $nRecebido += $oMovimento->valor;
                }
            }
        }
        $_REQUEST['nReceber'] = $nReceber;
        $_REQUEST['nRecebido'] = $nRecebido;
        $_REQUEST['nRecebido'] = $nRecebido;
        $_REQUEST['nPago'] = $nPago;

        $_REQUEST['voMovimento'] = $voMovimento;
        include_once("view/principal/movimento/index.php");
        exit();

    }

    public function preparaListaAjax2()
    {
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        $sTipo = $_REQUEST['sTipo'];
        $sComplemento = "data_efetivacao is null AND tipo='{$sTipo}' AND id_escritorio={$nIdEscritorio}";
        $voMovimentoPendente = $oFachada->recuperarTodosMovimentoComplemento($sComplemento);

        $_REQUEST['voMovimentoPendente'] = $voMovimentoPendente;
        include_once("view/principal/movimento/lista_ajax.php");
        exit();
    }

    public function preparaFormulario()
    {
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        $oFachada = new FachadaPrincipalBD();
        $sOP = $_REQUEST['sOP'];

        if ($_REQUEST['sTipo'] == 'RECEITA') {
            $_REQUEST['sTitulo'] = "{$sOP} Receita - Fonte Externa";
        } else {
            $_REQUEST['sTipo'] = 'DESPESA';
            $_REQUEST['sTitulo'] = "{$sOP} Despesa";
        }
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $_REQUEST['voDespesaReceitaTipo'] = $oFachada->recuperarTodosDespesaReceitaTipoPorTipo($_REQUEST['sTipo'], $nIdEscritorio);
        $_REQUEST['voContaBancaria'] = $oFachada->recuperarTodosContaBancariaSaldo($nIdEscritorio);
        $_REQUEST['voFormaPagamento'] = $oFachada->recuperarTodosFormaPagamento();
        $_REQUEST['voProjeto'] = $oFachada->recuperarTodosVProjeto($nIdEscritorio);

        switch ($_REQUEST['sTipo']) {
            case 'RECEITA':
                $_REQUEST['voPlanoContas'] = $oFachada->recuperarTodosPlanoContasPorGrupoPorVisibilidade($nIdEscritorio, "1.", '1');
                $_REQUEST['nTipo'] = 1;
                break;
            case 'DESPESA':
                $_REQUEST['voPlanoContas'] = $oFachada->recuperarTodosPlanoContasPorGrupoPorVisibilidade($nIdEscritorio, "2.", '0');
                $_REQUEST['nTipo'] = 2;
                break;
        }

        switch ($sOP) {
            case "Detalhar":
                include_once("view/principal/movimento/detalhe.php");
                break;
            case "Alterar":
                $nCodMovimento = $_REQUEST['nCodMovimento'];
                $_REQUEST['oMovimento'] = $oFachada->recuperarUmMovimento($nCodMovimento);
                include_once("view/principal/movimento/insere_altera.php");
                break;
            case "Cadastrar":
            case "CadastrarAjax":
                include_once("view/principal/movimento/insere_altera.php");
                break;
            case "Cadastrar2":
                include_once("view/principal/movimento/insere_altera_novo.php");
                break;

        }
        exit();

    }

    public function processaFormulario()
    {
        $oFachada = new FachadaPrincipalBD();
        $sHeader = "";

        $sOP = (array_key_exists('sOP', $_POST)) ? $_POST['sOP'] : $_GET['sOP'];
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        if ($sOP != "Excluir") {
            switch ($sOP) {
                case 'Cadastrar':
                    $_POST['fInseridoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    break;
                case 'Alterar':
                    $oMovimento1 = $oFachada->recuperarUmMovimento($_POST['fCodMovimento']);
                    $_POST['fInseridoPor'] = $oMovimento1->getInseridoPor();
                    $_POST['fAlteradoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    break;

            }

            if (isset($_POST['fEfetivacao'])) {
                $_POST['fDataEfetivacao'] = ($sOP == 'Cadastrar') ? $_POST['fDataEntrada'] : date('d/m/Y');
            } else {
                $_POST['fDataEfetivacao'] = NULL;
            }
            $dCompetencia = "01/" . $_POST['fCompetencia'];
            $oMovimento = $oFachada->inicializarMovimento($_POST['fCodMovimento'], $_POST['fCodDespesaReceitaTipo'], $_POST['fDataEntrada'], $_POST['fDescricao'], $_POST['fDataEfetivacao'], null, $_POST['fValor'], $_POST['fObservacao'], $_POST['fInseridoPor'], null, 1, $nIdEscritorio, $dCompetencia, $_POST['fCodPlanoContas'], $_POST['fCodProjeto']);

            $_SESSION['oMovimento'] = $oMovimento;
            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            $oValidate->add_text_field("Valor", $oMovimento->getValor());
            $oValidate->add_number_field("Despesa Tipo", $oMovimento->getCodDespesaReceitaTipo());
            $oValidate->add_number_field("Apropriação", $oMovimento->getCodPlanoContas());
            $oValidate->add_date_field("Data Entrada", $oMovimento->getDataEntrada(), "date", "y");
            $oValidate->add_date_field("Competência", $oMovimento->getCompetencia(), "date", "y");
            $oValidate->add_text_field("Descrição", $oMovimento->getDescricao());

            $oValidate->add_text_field("Inserido Por", $oMovimento->getInseridoPor());
            if ($sOP == 'Alterar')
                $oValidate->add_text_field("Alterado Por", $oMovimento->getAlteradoPor());

            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=Movimento.preparaFormulario&fEntradaSaida=" . $_POST['fEntradaSaida'] . "&sOP=" . $sOP . "&nIdMovimento=" . $_POST['fCodMovimento'] . "";
                echo json_encode(['sMsg' => $oValidate->create_msg()]);
                die();
            }
        }

        switch ($sOP) {
            case "Cadastrar":
                $sTipo = ($_POST['fCodDespesaReceitaTipo'] == 1) ? "E" : "S";
                $sTipoDesc = ($_POST['fCodDespesaReceitaTipo'] == 1) ? "RECEITA" : "DESPESA";
                $nErro = 0;
                if ($nCodMovimento = $oFachada->inserirMovimento($oMovimento)) {
                    unset($_SESSION['oMovimento']);
                    if (isset($_POST['fCodContaBancaria'])) {
                        $nSaldoInicial = $oFachada->recuperarUmContaBancariaUltimoSaldo($_POST['fCodContaBancaria'])->getValorFormatado();
                        //$nCodigo = ;
                        // ao criar uma conta bancaria, disparar trigger de lançamento de saldo inicial = 0;
                        $oContaMovimentacao = $oFachada->inicializarContaMovimentacao($_POST['fCodContaMovimentacao'], $_POST['fCodContaBancaria'], 'S', $nSaldoInicial, $_POST['fValor'], 0, $_POST['fDataEfetivacao'], 1, $nCodMovimento, NULL);
                        if ($oFachada->inserirContaMovimentacao($oContaMovimentacao)) {
                            $this->arquivo($nCodMovimento);
                            $nErro = 0;
                            echo json_encode(['sMsg' => 'Lançamento efetivado com sucesso!', 'sClass' => 'success', 'bResultado' => 1, 'nTipo' => $sTipoDesc]);
                            die();
                        }
                    } else {
                        $nErro = 0;
                        echo json_encode(['sMsg' => 'Lançamento inserido com sucesso, porém não efetivado!', 'sClass' => 'success', 'bResultado' => 1, 'nTipo' => $sTipoDesc]);
                        die();
                    }

                } else {
                    $nErro = 1;
                    echo json_encode(['sMsg' => 'Não foi possível inserir o Lançamento!', 'sClass' => 'danger', 'bResultado' => 0, 'nTipo' => $sTipoDesc]);
                    die();
                }

                $sHeader = "?bErro=" . $nErro . "&action=Relatorio.preparaExtratoBancario&sOP=Visualizar&nAno={$_POST['nAno']}&nMes={$_POST['nMes']}&nContaBancaria={$_POST['nContaBancaria']}";
                break;
            case "Alterar":
                if ($oFachada->alterarMovimento($oMovimento)) {
                    if ($_POST['fCodContaBancaria']) {
                        $nSaldoInicial = $oFachada->recuperarUmContaBancariaUltimoSaldo($_POST['fCodContaBancaria'])->getValorFormatado();
                        // ao criar uma conta bancaria, disparar trigger de lançamento de saldo inicial = 0;
                        $sTipo = ($_POST['fCodDespesaReceitaTipo'] == 1) ? "E" : "S";
                        $sTipoDesc = ($_POST['fCodDespesaReceitaTipo'] == 1) ? "RECEITA" : "DESPESA";
                        $_POST['fValorReceita'] = ($_POST['fCodDespesaReceitaTipo'] == 1) ? $_POST['fValor'] : 0;
                        $_POST['fValorDespesa'] = ($_POST['fCodDespesaReceitaTipo'] == 2) ? $_POST['fValor'] : 0;

                        $oContaMovimentacao = $oFachada->inicializarContaMovimentacao($_POST['fCodContaMovimentacao'], $_POST['fCodContaBancaria'], $sTipo, $nSaldoInicial, $_POST['fValorDespesa'], $_POST['fValorReceita'], $_POST['fDataEfetivacao'], 1, $oMovimento->getCodMovimento(), NULL);
                        if ($oFachada->inserirContaMovimentacao($oContaMovimentacao)) {
                            $this->arquivo($oMovimento->getCodMovimento());
                            unset($_SESSION['oMovimento']);
                            echo json_encode(['sMsg' => 'Lançamento alterado/efetivado com sucesso!', 'sClass' => 'success', 'bResultado' => 1, 'nTipo' => $sTipoDesc, 'nCodMovimento' => $_POST['fCodMovimento']]);
                        } else {
                            echo json_encode(['sMsg' => 'Não foi possível efetivar/alterar o Lançamento!', 'sClass' => 'danger', 'bResultado' => 0, 'nTipo' => $sTipoDesc, 'nCodMovimento' => $_POST['fCodMovimento']]);
                        }

                    } else {
                        unset($_SESSION['oMovimento']);
                        echo json_encode(['sMsg' => 'Lançamento alterado com sucesso!', 'sClass' => 'success', 'bResultado' => 1, 'nTipo' => $sTipoDesc, 'nCodMovimento' => $_POST['fCodMovimento']]);
                    }
                } else {
                    echo json_encode(['sMsg' => 'Não foi possível efetivar/alterar o Lançamento!', 'sClass' => 'danger', 'bResultado' => 0, 'nTipo' => $sTipoDesc, 'nCodMovimento' => $_POST['fCodMovimento']]);
                }

                die();

            case "Excluir":
                $nCodMovimento = $_REQUEST['nCodMovimento'];
                $bResultado = $oFachada->excluirMovimento($nCodMovimento);

                if ($bResultado) {
                    $_SESSION['sMsg'] = "Lançamento(s) excluído(s) com sucesso!";
                    $sHeader = "?bErro=0&action=Movimento.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "Não foi possível excluir o(s) Lançamento!";
                    $sHeader = "?bErro=1&action=Movimento.preparaLista";
                }
                break;
        }

        header("Location: " . $sHeader);

    }

    public function arquivo($nCodMovimento)
    {

        $oFachada = new FachadaPrincipalBD();
        $bErro = true;

        if ($_FILES['fComprovante']) {
          
          $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
          $sDiretorio = "{$nIdEscritorio}/comprovantes";

            if (!is_dir("uploads" . DIRECTORY_SEPARATOR .$sDiretorio)) {
                mkdir("uploads" . DIRECTORY_SEPARATOR .$sDiretorio, 0700);
            }
            $oUpload = new Upload(null, 12000000);
            $oArquivo = $oUpload->saveFile($_FILES['fComprovante'],$sDiretorio, "movimento_{$nCodMovimento}");

            if ($oArquivo['erro'] == 0) {
                $oMovimento = $oFachada->recuperarUmMovimento($nCodMovimento);
                $oMovimento->setComprovante($oArquivo['url']);

                if ($oFachada->alterarMovimento($oMovimento))
                    $bErro = false;
            }
            if ($bErro)
                return false;
            else
                return true;

        } else
            return false;
    }

}