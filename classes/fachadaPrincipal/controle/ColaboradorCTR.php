<?php
class ColaboradorCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $voColaborador = $oFachada->recuperarTodosColaborador($nIdEscritorio);
        $_REQUEST['voColaborador'] = $voColaborador;
        include_once("view/principal/colaborador/index.php");
        exit();
    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $oFachadaAcesso = new FachadaAcessoBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $oColaborador = false;
        $sOP = $_REQUEST['sOP'];

        if($sOP == "Alterar" || $sOP == "Detalhar" || $sOP == "AlterarFotoTemp" ) {
            $nCodColaborador = $_GET['nCodColaborador'] ?? $_POST['fCodColaborador'];
            $oColaborador = $oFachada->recuperarUmColaborador($nCodColaborador);
            $_REQUEST['oGrupoColaborador'] = $oFachada->recuperarUmColaboradorEscritorioGrupo($oColaborador->getCodColaborador(),$nIdEscritorio);
        }

        $_REQUEST['voAcessoGrupoUsuario'] = $oFachadaAcesso->recuperarTodosAcessoGrupoUsuario();;
        $_REQUEST['oColaborador'] = $_SESSION['oColaborador'] ?? $oColaborador;


        unset($_SESSION['oColaborador']);

        switch($_REQUEST['sOP']){
            case "Detalhar":
                $_REQUEST['voProjetoColaborador'] = $oFachada->recuperarTodosProjetoPorColaborador($nCodColaborador);
                include_once("view/principal/colaborador/detalhe3.php");
                break;
            case "AlterarSenha":
                $nCodColaborador = $_SESSION['oUsuarioAM']->getCodColaborador();
                $_REQUEST['oColaborador'] = $oFachada->recuperarUmColaborador($nCodColaborador);
                include_once("view/login/alterar_senha.php");
                break;
            case "AlterarFoto":
                include_once("view/principal/colaborador/detalhe3.php");
                break;
            case "Cadastrar":
            case "Alterar":
                include_once("view/principal/colaborador/insere_altera.php");
                break;
        }
        exit();

    }


    public function recuperarUmColaboradorCpf(){
        $oFachada = new FachadaPrincipalBD();
        $oColaborador = $oFachada->recuperarUmColaboradorPorCpf($_REQUEST['sCPF']);

        if($oColaborador){
            $vColaborador = [
                'sMsg2'=>'Usuário já existente no sistema!',
                'sOP'=>'Alterar',
                'CodColaborador'=>$oColaborador->getCodColaborador(),
                'Nome'=>$oColaborador->getNome(),
                'Email'=>$oColaborador->getEmail(),
                'DataNascimento'=>$oColaborador->getDataNascimentoFormatado(),
                'TipoColaborador'=>$oColaborador->getTipo(),
                'TipoConta'=>$oColaborador->getTipoConta(),
                'Banco'=>$oColaborador->getBanco(),
                'Agencia'=>$oColaborador->getAgencia(),
                'Conta'=>$oColaborador->getConta(),
                'PixTipo'=>$oColaborador->getPixTipo(),
                'PixChave'=>$oColaborador->getPixChave(),
                'Login'=>$oColaborador->getLogin()
            ];

            echo json_encode($vColaborador);
        }
    }

    public function colaboradorFoto()
    {
        $oFachada = new FachadaPrincipalBD();
        $oColaborador = $oFachada->recuperarUmColaborador($_REQUEST['CodColaborador']);
        if ($oColaborador->getFoto() != null){
            header('Access-Control-Allow-Origin: *');
            header( "Content-type: ".$oColaborador->getFotoTipo());
            echo base64_decode($oColaborador->getFoto());
            die();
        }

        include_once("view/principal/colaborador/foto.php");
    }


    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        if($sOP != "Excluir" && $sOP != "AlterarSenha" && $sOP!='AlterarFoto' && $sOP != "LimparSenha"){
            switch($sOP){
                case 'Cadastrar':
                    $_POST['fInseridoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    $_POST['fAlteradoPor'] = null;
                    $sSenha = md5($_POST['fCpf']);
                    $_POST['fFoto'] = null;
                    break;
                case 'Alterar':
                    $oColaborador1 = $oFachada->recuperarUmColaborador($_POST['fCodColaborador']);
                    $_POST['fInseridoPor'] =  $oColaborador1->getInseridoPor();
                    $_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    $_POST['fFoto'] = $oColaborador1->getFoto();
                    if ($oColaborador1->getSenha() == NULL){

                        $vSinais = array(".", "-");
                        $sSenhaInicial = str_replace($vSinais, "", $oColaborador1->getCpf());
                        $sSenha = md5($sSenhaInicial);
                    }else
                        $sSenha = $oColaborador1->getSenha();
                    break;
                default:
                    $sSenha = null;
                    $_POST['fFoto'] = null;
                    break;
            }

            $oColaborador = $oFachada->inicializarColaborador($_POST['fCodColaborador'],$_POST['fNome'],$_POST['fDataNascimento'],$_POST['fBanco'],$_POST['fAgencia'],$_POST['fTipoConta'],$_POST['fConta'],$_POST['fPixTipo'],$_POST['fPixChave'],$_POST['fCpf'],$_POST['fLogin'],$sSenha,$_POST['fFoto'],$_POST['fEmail'],$_POST['fInseridoPor'],$_POST['fAlteradoPor'],1,$_REQUEST['fTipo']);

            $_SESSION['oColaborador'] = $oColaborador;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            if($sOP == 'Alterar'){
                $oValidate->add_number_field("Colaborador", $oColaborador->getCodColaborador(), "number", "y");
                $oValidate->add_text_field("Alterado Por", $oColaborador->getAlteradoPor(), "text", "y");
            }

            $oValidate->add_text_field("Nome", $oColaborador->getNome(), "text", "y");
            $oValidate->add_date_field("Data Nascimento", $oColaborador->getDataNascimento(), "date", "y");
            
            if($_SESSION['oEscritorio']->getIdEscritorio() == 1){
              $oValidate->add_text_field("Banco", $oColaborador->getBanco(), "text", "y");
              $oValidate->add_text_field("Agência", $oColaborador->getAgencia(), "text", "y");
              $oValidate->add_text_field("Tipo de Conta", $oColaborador->getTipoConta(), "text", "y");
              $oValidate->add_text_field("Conta", $oColaborador->getConta(), "text", "y");
            }
            
            $oValidate->add_text_field("CPF", $oColaborador->getCpf(), "text", "y");
            $oValidate->add_text_field("Email", $oColaborador->getEmail(), "text", "y");

            $oValidate->add_text_field("Inserido Por", $oColaborador->getInseridoPor(), "text", "y");
            $oValidate->add_number_field("Ativo", $oColaborador->getAtivo(), "number", "y");

            if($oColaborador->getTipo()!= 2){
                $oValidate->add_text_field("Login", $oColaborador->getLogin(), "text", "y");
            }


            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=".$sOP."&nCodColaborador=".$_POST['fCodColaborador']."";
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":

                if($nCodcolaborador = $oFachada->inserirColaborador($oColaborador)){
                    $oColaboradorEscritorioGrupo = $oFachada->inicializarColaboradorEscritorioGrupo($nCodcolaborador,$nIdEscritorio,$_POST['fCodGrupoUsuario']);
                    if ($oFachada->inserirColaboradorEscritorioGrupo($oColaboradorEscritorioGrupo)){
                        $_SESSION['sMsg'] = "Colaborador inserido com sucesso!";
                        $sHeader = "?bErro=0&action=Colaborador.preparaLista";
                        unset($_SESSION['oColaborador']);
                        unset($_SESSION['oColaboradorEscritorioGrupo']);
                    }else{
                        $_SESSION['sMsg'] = "Não foi possível inserir o Colaborador[1]!";
                        $sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=$sOP";
                    }
                } else {
                    $_SESSION['sMsg'] = "Não foi possível inserir o Colaborador!";
                    $sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=".$sOP;
                }

                break;
            case "Alterar":
                if($oFachada->alterarColaborador($oColaborador)){
                    $oColaboradorEscritorioGrupo = $oFachada->inicializarColaboradorEscritorioGrupo($oColaborador->getCodColaborador(),$nIdEscritorio,$_POST['fCodGrupoUsuario']);

                    $oGrupo = $oFachada->recuperarUmColaboradorEscritorioGrupo($oColaborador->getCodColaborador(),$nIdEscritorio);

                    if ($oGrupo){
                        if($oFachada->alterarColaboradorEscritorioGrupo($oColaboradorEscritorioGrupo)){
                            $_SESSION['sMsg'] = "Colaborador alterado com sucesso!";
                            $sHeader = "?bErro=0&action=Colaborador.preparaLista";
                            unset($_SESSION['oColaborador']);
                            unset($_SESSION['oColaboradorEscritorioGrupo']);
                        }else{
                            $_SESSION['sMsg'] = "Não foi possível alterar o grupo de permissão do Colaborador!";
                            $sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=$sOP&nCodColaborador={$oColaborador->getCodColaborador()}";
                        }
                    }else{
                        if ($oFachada->inserirColaboradorEscritorioGrupo($oColaboradorEscritorioGrupo)){
                            $_SESSION['sMsg'] = "Colaborador alterado com sucesso!";
                            $sHeader = "?bErro=0&action=Colaborador.preparaLista";
                            unset($_SESSION['oColaborador']);
                            unset($_SESSION['oColaboradorEscritorioGrupo']);
                        }else{
                            $_SESSION['sMsg'] = "Não foi possível alterar o Colaborador[1]!";
                            $sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=$sOP&nCodColaborador={$oColaborador->getCodColaborador()}";
                        }
                    }
                } else {
                    $_SESSION['sMsg'] = "Não foi possível alterar o Colaborador!";
                    $sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=$sOP&nCodColaborador={$_POST['fCodColaborador']}";
                }
                break;
            case "AlterarSenha":
                $nCodColaborador = $_SESSION['oUsuarioAM']->getCodColaborador();
                $oColaborador = $oFachada->recuperarUmColaborador($nCodColaborador);

                if($oColaborador->getSenha() == md5($_REQUEST['fSenhaAtual'])){

                    if($_REQUEST['fSenhaNova'] == $_REQUEST['fSenhaNova2']){
                        $oColaborador->setSenha(md5($_REQUEST['fSenhaNova']));
                        $oColaborador->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
                        $oFachada->alterarColaborador($oColaborador);
                        unset($_SESSION['oColaborador']);
                        $_SESSION['sMsg'] = "Senha alterada com sucesso!";
                        $sHeader = "?";
                    } else {
                        $_SESSION['sMsg'] = "Os campos Nova Senha e Repete nova Senha estão diferentes!";
                        $sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=AlterarSenha";
                    }
                }else{
                    $_SESSION['sMsg'] = "Não foi possível alterar sua senha pois a senha atual informada está errada!";
                    $sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=AlterarSenha";
                }
                break;

            case "LimparSenha":
                $nCodColaborador = ($_POST['fIdColaborador'][0]) ? $_POST['fIdColaborador'][0] : $_GET['nCodColaborador'];
                $oColaborador = $oFachada->recuperarUmColaborador($nCodColaborador);
                if($oColaborador->getTipo()!=2){
                    $vSinais = array(".", "-");
                    $sSenhaInicial = str_replace($vSinais, "", $oColaborador->getCpf());
                    $sSenha = md5($sSenhaInicial);
                    $oColaborador->setSenha($sSenha);
                    if($oFachada->alterarColaborador($oColaborador)){
                        $_SESSION['sMsg'] = "Senha alterada com sucesso! Senha inicial é o CPF do colaborador";
                        $sHeader = "?bErro=0&action=Colaborador.preparaLista";
                    }else{
                        $_SESSION['sMsg'] = "Não foi possível alterar sua senha!";
                        $sHeader = "?bErro=1&action=Colaborador.preparaLista";
                    }
                }else{
                    $_SESSION['sMsg'] = "Este colaborador não tem acesso ao sistema!";
                    $sHeader = "?bErro=1&action=Colaborador.preparaLista";
                }
                break;
            case "AlterarFoto":
                $bErro=0;
                $nCodColaborador = $_REQUEST['fCodColaborador'];
                $oColaborador = $oFachada->recuperarUmColaborador($nCodColaborador);
                $nCodColaborador = $oColaborador->getCodColaborador();

                $file = $_FILES['avatar'];
                $vPermissao = array ("image/jpeg","image/jpg","image/png");

                if (!in_array($file['type'], $vPermissao)){
                    $_SESSION['sMsg'] = "Este tipo de arquivo[".$file['type']."] não tem permissão para upload";
                    $bErro=1;
                }
                $nTamanhoMaximoArquivo = 2097152;
                if ($file['size'] > $nTamanhoMaximoArquivo){
                    $_SESSION['sMsg'] = "Erro no tamanho do arquivo: ".$file['size'] ." Kb. Tamanho Maximo é ".$nTamanhoMaximoArquivo." !";
                    $bErro=1;
                }
                if ($file['error']){
                    throw new \Exception("Erro: ".$file['error']);
                }

                if($bErro!=1){

                    $nTamanho = $file['size'];
                    $fp = fopen($file['tmp_name'], "rb");
                    $sConteudo = fread($fp, $nTamanho);
                    $sConteudo = base64_encode($sConteudo);
                    fclose($fp);

                    $oColaborador->setFoto($sConteudo);
                    $oColaborador->setFotoTipo($file['type']);

                    if ($oFachada->alterarColaborador($oColaborador)){ //Salvar arquivo no banco

                        if ($_SESSION['oUsuarioAM']->getCodColaborador() == $nCodColaborador)
                            $_SESSION['oUsuarioAM']->setFoto($sConteudo);

                        $_SESSION['sMsg'] = "Foto alterada com sucesso!";

                    } else {
                        $_SESSION['sMsg'] = "Não foi possível alterar a foto!";
                    }
                }

                echo json_encode(['mensagem' => $_SESSION['sMsg']]);
                die();
            case "Excluir":
                $bResultado = true;

                $vIdPaiColaborador = explode("____",$_REQUEST['fIdColaborador']);
                foreach($vIdPaiColaborador as $vIdFilhoColaborador){
                    $vIdColaborador = explode("||",$vIdFilhoColaborador);
                    foreach($vIdColaborador as $nIdColaborador){
                        $bResultado &= $oFachada->excluirColaborador($vIdColaborador[0],$vIdColaborador[1]);
                    }
                }

                if($bResultado){
                    $_SESSION['sMsg'] = "Colaborador(s) exclu&iacute;do(s) com sucesso!";
                    $sHeader = "?bErro=0&action=Colaborador.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "Não foi possível excluir o(s) Colaborador!";
                    $sHeader = "?bErro=1&action=Colaborador.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);

    }
}