<?php
class PropostaMicroServicoCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $oFachadaAcesso = new FachadaAcessoBD();

        switch($_REQUEST['sOP'] ){
            case 'Servico':
                $_REQUEST['voPropostaMicroServico'] = $oFachada->recuperarTodosPropostaMicroServico($_REQUEST['nIdServico']);
                include_once("view/principal/projeto_servico_etapa_microservico/index.php");
                break;
            case 'Etapa':

                $_REQUEST['oVProjeto'] = $oFachada->recuperarUmProjetoViewPorProjeto($_REQUEST['nCodProjeto']);
                $_REQUEST['oEtapa'] = $oEtapa = $oFachada->recuperarUmServicoEtapa($_REQUEST['nCodServicoEtapa']);
                $_REQUEST['voPropostaMicroServico'] = $voPropostaMicroServico = $oFachada->recuperarTodosPropostaMicroServicoPorProjetoEtapaView($_REQUEST['nCodProjeto'],$_REQUEST['nCodServicoEtapa']);

                $_REQUEST['bPermissaoCalendario'] = $oFachadaAcesso->recuperarUmPermissao("Projeto.preparaFormulario", "Calendario",$_SESSION['oGrupoUsuario']->getCodGrupoUsuario());

                foreach($voPropostaMicroServico as $oPropostaMicroServico){
                    if ($oPropostaMicroServico->data_previsao_inicio_microservico || $oPropostaMicroServico->data_previsao_fim_microservico || $oPropostaMicroServico->dias == 0)
                        $_REQUEST['bCalendario'] = 1;
                    else
                        $_REQUEST['bCalendario'] = 0;
                }

                include_once("view/principal/projeto_servico_microservico/lista_ajax.php");
                break;

            default:
                $_REQUEST['voPropostaMicroServico'] = $oFachada->recuperarTodosPropostaMicroServico();
                include_once("view/principal/projeto_servico_etapa_microservico/index.php");
                break;
        }
        exit();

    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $_REQUEST['oPropostaMicroServico'] = $oFachada->recuperarUmPropostaMicroServico($_GET['nCodMicroServico'],$_GET['nCodProposta']);
        $_REQUEST['oProjeto'] = $oFachada->recuperarUmProjetoPorProposta($_GET['nCodProposta']);
        if ($_REQUEST['sOP'] == "Justificar")
            include_once("view/principal/proposta_microservico/form_justificar.php");
        else
            include_once("view/principal/proposta_microservico/form_conclusao.php");
        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

        if($sOP != "Excluir" && $sOP != "Justificar" && $sOP != "Concluir"){
            if($sOP == 'Cadastrar'){
                $_POST['fInseridoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                $_POST['fAlteradoPor'] = null;
            }else{
                $_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
            }
            $oPropostaMicroServico = $oFachada->inicializarPropostaMicroServico($_POST['fCodMicroservico'],$_POST['fCodProposta'],$_POST['fCodProjeto'],$_POST['fQuantidade'],$_POST['fDescricaoQuantidade'],$_POST['fDias'],$_POST['fDataPrevisaoInicio'],$_POST['fDataEfetivacaoInicio'],$_POST['fDataPrevisaoFim'],null,null,$_POST['fInseridoPor'],$_POST['fAlteradoPor']);
            $_SESSION['oPropostaMicroServico'] = $oPropostaMicroServico;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            $oValidate->add_number_field("CodProjeto", $oPropostaMicroServico->getCodProjeto(), "number", "y");
            $oValidate->add_text_field("InseridoPor", $oPropostaMicroServico->getInseridoPor(), "text", "y");

            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=PropostaMicroServico.preparaFormulario&sOP=".$sOP."&nIdPropostaMicroServico=".$_POST['fCodMicroservico']."||".$_POST['fCodProjeto']."";
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirPropostaMicroServico($oPropostaMicroServico)){
                    unset($_SESSION['oProjetoServicoMicroservico']);
                    $_SESSION['sMsg'] = "Micro Serviço inserido com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaMicroServico.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Micro Serviço!";
                    $sHeader = "?bErro=1&action=PropostaMicroServico.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Alterar":
                if($oFachada->alterarPropostaMicroServico($oPropostaMicroServico)){
                    unset($_SESSION['oProjetoServicoMicroservico']);
                    $_SESSION['sMsg'] = "Micro Serviço alterado com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaMicroServico.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Micro Serviço!";
                    $sHeader = "?bErro=1&action=PropostaMicroServico.preparaFormulario&sOP=".$sOP."&nIdProjetoServicoMicroservico=".$_POST['fCodEtapa']."||".$_POST['fCodProposta']."";
                }
                break;
            case "Concluir":
                $oPropostaMicroServico = $oFachada->recuperarUmPropostaMicroServico($_REQUEST['fCodMicroservico'],$_REQUEST['fCodProposta']);
                $oProposta = $oFachada->recuperarUmProposta($_REQUEST['fCodProposta']);
                $oPropostaMicroServico->setDataEfetivacaoFimBanco($_REQUEST['fDataConclusao']);
                $oPropostaMicroServico->setObservacao($_REQUEST['fObservacao']);
                $oProjeto = $oFachada->recuperarUmProjetoPorProposta($_REQUEST['fCodProposta']);
                $nCodEtapa = $oPropostaMicroServico->getServicoMicroservico()->getServicoEtapa()->getCodEtapa();
                $nCodServicoEtapa = $oPropostaMicroServico->getServicoMicroservico()->getServicoEtapa()->getCodServicoEtapa();

                if(isset($_REQUEST['fMetragem'])){
                    $oProjeto->setMetragemBanco($_REQUEST['fMetragem']);
                    $oFachada->alterarProjeto($oProjeto);
                }

                if($oFachada->alterarPropostaMicroServico($oPropostaMicroServico)){
                    // verificar se ainda tem algum microservico do projeto a ser CONCLUIDO

                    $oServicoEtapa = $oFachada->recuperarUmServicoEtapa($nCodServicoEtapa);

                    $nMicroservico = $oFachada->verificaUltimaEtapaPropostaMicroServico($oProjeto->getCodProjeto(),$nCodServicoEtapa);

                    if($nMicroservico == 0){

                        if($oServicoEtapa->getOrdem() == 1){

                            $oCliente = $oProjeto->getCliente();
                            $voProjetoServicoMicroServico  = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($oProjeto->getCodProposta(),1);
                            $voProjetoServicoEtapa = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta(null,$oProjeto->getCodProjeto());
                            $voPagamento = $oFachada->recuperarTodosProjetoPagamento($oProjeto->getCodProjeto());
                            $_REQUEST['nPagina']=1;

                            $voEtapaProjeto = [];
                            $sDescricaoMicroServico = "";
                            if($voProjetoServicoMicroServico){
                                foreach($voProjetoServicoMicroServico as $oMicroServico){
                                    $sDescricaoMicroServico .= $oMicroServico->getServicoMicroservico()->getDescricao();
                                    if($oMicroServico->getQuantidade() >0){
                                        $sDescricaoMicroServico .= " QTDE. " . $oMicroServico->getQuantidade() . (($oMicroServico->getDescricaoQuantidade()) ? " (".$oMicroServico->getDescricaoQuantidade()."); " : "");
                                    }elseif($oMicroServico->getDescricaoQuantidade()){
                                        $sDescricaoMicroServico.= " (".$oMicroServico->getDescricaoQuantidade()."); ";
                                    }else{
                                        $sDescricaoMicroServico .= "; ";
                                    }
                                } //foreach($voProjetoServicoMicroServico as $oMicroServico){
                                $sDescricaoMicroServico = substr($sDescricaoMicroServico,0,-2);
                            }

                            $sDescricaoProjeto = $oProjeto->getDescricao() . "<br> Serão desenvolvidas as seguintes subetapas: <br>" . $sDescricaoMicroServico;

                            $nTotal = 0;
                            $nParcelas = count($voPagamento);
                            foreach ($voPagamento as $oPagamento) {
                                $nTotal += $oPagamento->getValor();
                            }
                            
                            
                              $nCodTipoEmail=9;
                              $oEmailModelo = $oFachada->recuperarUmEmailTipo($nCodTipoEmail);
                              $sConteudo = $oEmailModelo->getTemplate();

                              $sConteudo = str_replace("#SERVICO#",mb_strtoupper($oProposta->getServico()->getDescServico(),"utf8") ,$sConteudo);
                              $sConteudo = str_replace("#IDENTIFICAÇÃO#",$oCliente->getIdentificacao() ,$sConteudo);
                              $sConteudo = str_replace("#NOME#",($oCliente->getCodTipoPessoa()==1) ? $oCliente->getNome(): $oCliente->getRazaoSocial() ,$sConteudo);
                              $sConteudo = str_replace("#EMAIL#",$oCliente->getEmail() ,$sConteudo);
                              $sConteudo = str_replace("#TELEFONES#", $oCliente->getTelefones() ,$sConteudo);
                              $sConteudo = str_replace("#AREA#", ( $oProjeto->getMetragem()) ? $oProjeto->getMetragem():"Não Especificado" ,$sConteudo);
                              $sConteudo = str_replace("#DESCRICAOPROJETO#",$sDescricaoProjeto ,$sConteudo);
                              $sConteudo = str_replace("#CONTRATO#",$oProjeto->getCodProjeto() ,$sConteudo);
                              $sConteudo = str_replace("#VALOR#",number_format($nTotal,2,",",".") ,$sConteudo);
                              $sConteudo = str_replace("#DATAINICIO#",$oProjeto->getDataInicioFormatado() ,$sConteudo);
                              $sConteudo = str_replace("#DATAPREVISAOFIM#",($oProjeto->getDataPrevisaoFim()) ? $oProjeto->getDataPrevisaoFimFormatado() : "sem data específica" ,$sConteudo);

                              $sConteudo = htmlspecialchars($sConteudo);
                              /*<!--composto de dados concepcionais apresentados em escala adequada à perfeita compreensão dos elementos nele contidos, contendo as seguintes etapas: #DESCRICAO#-->*/

                              $vConteudo = ["sConteudo"=>$sConteudo,"sEscritorio"=>$_SESSION['oEscritorio']->getNomeFantasia()];
                              if($_SESSION['oEscritorio']->getEnvioEmail() ==='S'){
                                $oMailer = new Mailer();
                                //	$sEmail = $oCliente->getEmail();
                                $sEmail = $_SESSION['oEscritorio']->getEmailAdministrador();
                                $sAssunto = "[{$oProposta->getIdentificacao()}] {$oEmailModelo->getTitulo()}";
                                $oMailer->enviar($oCliente->getNome(), $sEmail, $sAssunto, "template", $vConteudo);
                              }
                            // Inserir na Tabela de Email
                            $sDescricaoEmail = $oProposta->getIdentificacao();
                            $dDataHora = date('Y-m-d H:i:s');
                            $oEmail = $oFachada->inicializarEmail(null,$oCliente->getCodCliente(),$nCodTipoEmail,$sDescricaoEmail,$dDataHora);
                            $oFachada->inserirEmail($oEmail);


                        }

                        // atualizar prazos servico_etapa
                        $oPropostaServicoEtapa = $oFachada->recuperarUmPropostaServicoEtapa($nCodServicoEtapa,$_REQUEST['fCodProposta']);

                        $oPropostaServicoEtapa->setDataConclusaoBanco($_REQUEST['fDataConclusao']);
                        $oFachada->alterarPropostaServicoEtapa($oPropostaServicoEtapa);

                        $nCodTipoEmail=8;
                        $oEmailModelo = $oFachada->recuperarUmEmailTipo($nCodTipoEmail);
                        $oCliente = $oProjeto->getCliente();
                        $oEtapa = $oFachada->recuperarUmServicoEtapa($nCodServicoEtapa);
                        $sConteudo = $oEmailModelo->getTemplate();

                        $sConteudo = str_replace("#SAUDACAO#","{$oCliente->getNome()} ",$sConteudo);
                        $sConteudo = str_replace("#DESC_ETAPA#","{$oEtapa->getDescricao()} ",$sConteudo);

                        $voMicroServico  = $oFachada->recuperarTodosPropostaMicroServicoPorPropostaEtapa($_REQUEST['fCodProposta'],$nCodServicoEtapa);

                        $sServicos = '<ul style="padding-left: 90px;">';
                        foreach($voMicroServico as $oMicroServico){
                            $sServicos .=  '<li><em>'. $oMicroServico->getDescricao().';</em></li>';
                        }
                        $sServicos .= '</ul>';

                        //DESCRICAO MICROSERVICOS
                        $sConteudo = str_replace("#DESC_MICROSERVICOS#","{$sServicos} ",$sConteudo);
                        $sConteudo = str_replace("#DATA_PREVISAO#","{$oPropostaMicroServico->getDataPrevisaoInicioFormatado()} ",$sConteudo);
                        $sConteudo = str_replace("#DATA_CONFIRMACAO#","{$_REQUEST['fDataConclusao']} ",$sConteudo);
                        //DESCRICAO ENTRAVES

                        // $sConteudo = str_replace("#OBSERVACOES#","{$oProjetoPagamento->getFormaPagamento()->getDescricao()} ",$sConteudo);
                        $sConteudo = htmlspecialchars($sConteudo);
                        $vConteudo = ["sConteudo"=>$sConteudo,"sEscritorio"=>$_SESSION['oEscritorio']->getNomeFantasia()];
                        // Envio de EMAIL
                        if($_SESSION['oEscritorio']->getEnvioEmail() ==='S'){
                          $oMailer = new Mailer();
                          $sEmail = $oCliente->getEmail();
                        //  $sEmail = "bc_fabio@hotmail.com";
                          $sAssunto = "[{$_SESSION['oEscritorio']->getNomeFantasia()}] {$oEmailModelo->getTitulo()}";
                          $oMailer->enviar($oCliente->getNome(), $sEmail, $sAssunto, "template", $vConteudo);
                        }
                        // Inserir na Tabela de Email
                        $sDescricaoEmail = $oProposta->getIdentificacao();
                        $dDataHora = date('Y-m-d H:i:s');
                        $oEmail = $oFachada->inicializarEmail(null,$oCliente->getCodCliente(),$nCodTipoEmail,$sDescricaoEmail,$dDataHora);
                        $oFachada->inserirEmail($oEmail);

                    }

                    $nMicroServicoAberto = $oFachada->verificaUltimaEtapaPropostaMicroServico($oProjeto->getCodProjeto());
                    if ($nMicroServicoAberto == 0){
                        $oProjeto->setDataFimBanco($_REQUEST['fDataConclusao']);
                        $oFachada->alterarProjeto($oProjeto);
                    }

                    echo json_encode(['sMsg'=>'Etapa de serviço concluído com sucesso!','sClass'=>'success','nCodEtapa'=>$nCodServicoEtapa,'nCodProjeto'=>$oProjeto->getCodProjeto()]);
                } else {
                    echo json_encode(['sMsg'=>'Não foi possível concluir a etapa de serviço!','sClass'=>'danger','nCodEtapa'=>$nCodServicoEtapa,'nCodProjeto'=>$oProjeto->getCodProjeto()]);
                }
                die();
            case "Justificar":
                $oPropostaMicroServico = $oFachada->recuperarUmPropostaMicroServico($_POST['fCodMicroservico'],$_POST['fCodProposta']);
                $oPropostaMicroServico->setObservacao($_POST['fObservacao']);

                if($oFachada->alterarPropostaMicroServico($oPropostaMicroServico))
                    echo json_encode(['sMsg'=>'Etapa Justificada com sucesso!','sClass'=>'success','nCodEtapa'=>$_POST['fCodEtapa'],'nCodProjeto'=>$_POST['fCodProjeto']]);
                else
                    echo json_encode(['sMsg'=>'Não foi possível justificar a etapa!','sClass'=>'danger','nCodEtapa'=>$_POST['fCodEtapa'],'nCodProjeto'=>$_POST['fCodProjeto']]);
                die();

            case "Excluir":
                $bResultado = true;

                $vIdPaiPropostaMicroServico = explode("____",$_REQUEST['fIdPropostaMicroServico']);
                foreach($vIdPaiProjetoServicoMicroservico as $vIdFilhoPropostaMicroServico){
                    $vIdProjetoServicoMicroservico = explode("||",$vIdFilhoPropostaMicroServico);
                    foreach($vIdPropostaMicroServico as $nIdPropostaMicroServico){
                        $bResultado &= $oFachada->excluirPropostaMicroServico($vIdPropostaMicroServico[0],$vIdPropostaMicroServico[1]);
                    }
                }

                if($bResultado){
                    $_SESSION['sMsg'] = "Micro Serviço(s) exclu&iacute;do(s) com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaMicroServico.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Micro Serviço!";
                    $sHeader = "?bErro=1&action=PropostaMicroServico.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);

    }

}


?>
