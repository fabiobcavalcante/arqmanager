<?php
 class EmailTipoCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
 		$voEmailTipo = $oFachada->recuperarTodosEmailTipo();
 		$_REQUEST['voEmailTipo'] = $voEmailTipo;

 		include_once("view/principal/email_tipo/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$oEmailTipo = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdEmailTipo = ($_POST['fIdEmailTipo'][0]) ? $_POST['fIdEmailTipo'][0] : $_GET['nIdEmailTipo'];

 			if($nIdEmailTipo){
 				$vIdEmailTipo = explode("||",$nIdEmailTipo);
 				$oEmailTipo = $oFachada->recuperarUmEmailTipo($vIdEmailTipo[0]);
 			}
 		}

 		$_REQUEST['oEmailTipo'] = ($_SESSION['oEmailTipo']) ? $_SESSION['oEmailTipo'] : $oEmailTipo;
 		unset($_SESSION['oEmailTipo']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/email_tipo/detalhe.php");
 		else
 			include_once("view/principal/email_tipo/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 			$oEmailTipo = $oFachada->inicializarEmailTipo($_POST['fIdEmailTipo'],$_POST['fTitulo'],$_POST['fTemplate'],$_POST['fAtivo']);
 			$_SESSION['oEmailTipo'] = $oEmailTipo;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_text_field("NomeFantasia", $oEmailTipo->getTitulo(), "text", "y");
			$oValidate->add_text_field("RazaoSocial", $oEmailTipo->getTemplate(), "text", "y");

 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=EmailTipo.preparaFormulario&sOP=".$sOP."&nIdEmailTipo=".$_POST['fIdEmailTipo']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirEmailTipo($oEmailTipo)){
 					unset($_SESSION['oEmailTipo']);
 					$_SESSION['sMsg'] = " Tipo de Email inserido com sucesso!";
 					$sHeader = "?bErro=0&action=EmailTipo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o  Tipo de Email!";
 					$sHeader = "?bErro=1&action=EmailTipo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarEmailTipo($oEmailTipo)){
 					unset($_SESSION['oEmailTipo']);
 					$_SESSION['sMsg'] = "Tipo de Email alterado com sucesso!";
 					$sHeader = "?bErro=0&action=EmailTipo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Tipo de Email!";
 					$sHeader = "?bErro=1&action=EmailTipo.preparaFormulario&sOP=".$sOP."&nIdEmailTipo=".$_POST['fIdEmailTipo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiEmailTipo = explode("____",$_REQUEST['fIdEmailTipo']);
   				foreach($vIdPaiEmailTipo as $vIdFilhoEmailTipo){
  					$vIdEmailTipo = explode("||",$vIdFilhoEmailTipo);
 					foreach($vIdEmailTipo as $nIdEmailTipo){
  						$bResultado &= $oFachada->excluirEmailTipo($vIdEmailTipo[0],$vIdEmailTipo[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = " Tipo(s) de Email(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=EmailTipo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s)  Tipo(s) de Email!";
 					$sHeader = "?bErro=1&action=EmailTipo.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
