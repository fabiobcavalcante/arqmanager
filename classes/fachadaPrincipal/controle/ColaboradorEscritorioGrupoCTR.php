<?php
class ColaboradorEscritorioGrupoCTR implements IControle{

	public function preparaLista(){
		$oFachada = new FachadaPrincipalBD();
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
		$voColaboradorEscritorioGrupo = $oFachada->recuperarTodosColaboradorEscritorioGrupo();
		$_REQUEST['voColaboradorEscritorioGrupo'] = $voColaboradorEscritorioGrupo;
		include_once("view/principal/colaborador_escritorio_grupo/index.php");
		exit();

	}

	public function preparaFormulario(){
		$oFachada = new FachadaPrincipalBD();
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

		$oColaboradorEscritorioGrupo = false;

		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
			$voColaboradorEscritorioGrupo = ($_POST['fIdColaboradorEscritorioGrupo'][0]) ? $_POST['fIdColaboradorEscritorioGrupo'][0] : $_GET['nIdColaboradorEscritorioGrupo'];

			if($nIdColaboradorEscritorioGrupo){
				$vIdColaboradorEscritorioGrupo = explode("||",$nIdColaboradorEscritorioGrupo);
				$oColaboradorEscritorioGrupo = $oFachada->recuperarUmColaboradorEscritorioGrupo($vIdColaboradorEscritorioGrupo[0],$vIdColaboradorEscritorioGrupo[1],$vIdColaboradorEscritorioGrupo[2]);
			}
		}

		$_REQUEST['oColaboradorEscritorioGrupo'] = ($_SESSION['oColaboradorEscritorioGrupo']) ? $_SESSION['oColaboradorEscritorioGrupo'] : $oColaboradorEscritorioGrupo;
		unset($_SESSION['oColaboradorEscritorioGrupo']);



		if($_REQUEST['sOP'] == "Detalhar")
			include_once("view/principal/colaborador_escritorio_grupo/detalhe.php");
		else
			include_once("view/principal/colaborador_escritorio_grupo/insere_altera.php");

		exit();

	}

	public function processaFormulario(){
		$oFachada = new FachadaPrincipalBD();

		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

		if($sOP != "Excluir"){
			$oColaboradorEscritorioGrupo = $oFachada->inicializarColaboradorEscritorioGrupo($_POST['fCodColaborador'],$_POST['fIdEscritorio'],$_POST['fCodGrupoUsuario']);
			$_SESSION['oColaboradorEscritorioGrupo'] = $oColaboradorEscritorioGrupo;

			$oValidate = FabricaUtilitario::getUtilitario("Validate");
			$oValidate->check_4html = true;

			$oValidate->add_number_field("Colaborador", $oColaboradorEscritorioGrupo->getCodColaborador(), "number", "y");
			$oValidate->add_number_field("Escritório", $oColaboradorEscritorioGrupo->getIdEscritorio(), "number", "y");
			$oValidate->add_number_field("Grupo de Usuário", $oColaboradorEscritorioGrupo->getCodGrupoUsuario(), "number", "y");


			if (!$oValidate->validation()) {
				$_SESSION['sMsg'] = $oValidate->create_msg();
				$sHeader = "?bErro=1&action=ColaboradorEscritorioGrupo.preparaFormulario&sOP=".$sOP."&nIdColaboradorEscritorioGrupo=".$_POST['fCodGrupoUsuario']."";
				header("Location: ".$sHeader);
				die();
			}
		}

		switch($sOP){
			case "Cadastrar":
				if($oFachada->inserirColaboradorEscritorioGrupo($oColaboradorEscritorioGrupo)){
					unset($_SESSION['oColaboradorEscritorioGrupo']);
					$_SESSION['sMsg'] = "Permissão inserida com sucesso!";
					$sHeader = "?bErro=0&action=ColaboradorEscritorioGrupo.preparaLista";

				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a permissão para o usuário!";
					$sHeader = "?bErro=1&action=ColaboradorEscritorioGrupo.preparaFormulario&sOP=".$sOP;
				}
				break;
			case "Alterar":
				if($oFachada->alterarColaboradorEscritorioGrupo($oColaboradorEscritorioGrupo)){
					unset($_SESSION['oColaboradorEscritorioGrupo']);
					$_SESSION['sMsg'] = "Permissão alterada com sucesso!";
					$sHeader = "?bErro=0&action=ColaboradorEscritorioGrupo.preparaLista";

				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Grupo!";
					$sHeader = "?bErro=1&action=ColaboradorEscritorioGrupo.preparaFormulario&sOP=".$sOP."&nIdColaboradorEscritorioGrupo=".$_POST['fCodGrupoUsuario']."";
				}
				break;
			case "Excluir":
				$nCodColaborador = $_REQUEST['nCodColaborador'];
				$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
				if($oFachada->excluirColaboradorEscritorioGrupoPorColaboradorEscritorio($nCodColaborador,$nIdEscritorio)){
					$_SESSION['sMsg'] = "Colaborador(a) exclu&iacute;do(a) com sucesso!";
					$sHeader = "?bErro=0&action=Colaborador.preparaLista";
				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a(s) Permissão(ões)!";
					$sHeader = "?bErro=1&action=Colaborador.preparaLista";
				}
				break;
		}

		header("Location: ".$sHeader);

	}

}