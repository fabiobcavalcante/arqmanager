<?php
class ClienteCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $voCliente = $oFachada->recuperarTodosCliente($nIdEscritorio);
        $_REQUEST['voCliente'] = $voCliente;
        $_REQUEST['oPrincipal'] = $oFachada->recuperarUmRelatorioPrincipal($nIdEscritorio);
        include_once("view/principal/cliente/index.php");
        exit();

    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $nIdEscritorio = ($_SESSION['oEscritorio']) ? $_SESSION['oEscritorio']->getIdEscritorio() : "";
        $oCliente = false;

        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
            $nIdCliente = ($_POST['fIdCliente'][0]) ?: $_GET['nIdCliente'];

            if($nIdCliente){
                $vIdCliente = explode("||",$nIdCliente);
                $oCliente = $oFachada->recuperarUmCliente($vIdCliente[0]);
                $_REQUEST['voProjeto'] = $oFachada->recuperarTodosProjetoPorCliente($vIdCliente[0]);
            }
        }

        $_REQUEST['oCliente'] = ($_SESSION['oCliente']) ?: $oCliente;
        unset($_SESSION['oCliente']);

        $_REQUEST['voIndicacao'] = $oFachada->recuperarTodosIndicacao($nIdEscritorio);

        if($_REQUEST['sOP'] == "Detalhar")
            include_once("view/principal/cliente/detalhe.php");
        else
            include_once("view/principal/cliente/insere_altera.php");

        exit();

    }

    public function listaAniversariantes(){
        $oFachada = new FachadaPrincipalBD();
        unset($_SESSION['oCliente']);
        $_REQUEST['voMeses'] = $oFachada->listarMeses();
        $_REQUEST['nMesSelecionado'] = ($_GET['nMesSelecionado']) ?: date('m');
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $_REQUEST['voAniversariantes'] = $oFachada->recuperarTodosAniversariantesMes($_REQUEST['nMesSelecionado'],$nIdEscritorio);
        include_once("view/principal/cliente/lista_aniversariantes.php");
        exit();

    }

    public function sobre(){
        include_once("view/principal/cliente/sobre.php");
        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = ($_SESSION['oEscritorio']) ? $_SESSION['oEscritorio']->getIdEscritorio() : "";
        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

        if($sOP != "Excluir"){
            switch($sOP){
                case 'Cadastrar':
                    $_POST['fIncluidoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    $_POST['fAlteradoPor'] = null;
                    break;
                case 'Alterar':
                    $oCliente1 = $oFachada->recuperarUmCliente($_POST['fCodCliente']);
                    $_POST['fIncluidoPor'] = $oCliente1->getIncluidoPor();
                    $_POST['fAlteradoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    break;
            }


            $oCliente = $oFachada->inicializarCliente($_POST['fCodCliente'],mb_strtoupper($_POST['fNome'],"utf8"),$_POST['fCodTipoPessoa'],$_POST['fIdentificacao'],mb_strtolower($_POST['fEmail'],"utf8"),$_POST['fCep'],$_POST['fLogradouro'],$_POST['fNumero'],$_POST['fComplemento'],$_POST['fBairro'],$_POST['fCidade'],$_POST['fUf'],$_POST['fTelefones'],$_POST['fDataNascimento'],$_POST['fRazaoSocial'],$_POST['fInscricaoEstadual'],$_POST['fInscricaoMunicipal'],$_POST['fObservacao'],$_POST['fIndicadoPor'],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1,$nIdEscritorio);

            $_SESSION['oCliente'] = $oCliente;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            $oValidate->add_text_field("Indicado por", $oCliente->getIndicadoPor());
            $oValidate->add_text_field("Nome", $oCliente->getNome());
            $oValidate->add_number_field("Tipo Pessoa", $oCliente->getCodTipoPessoa());

            if($oCliente->getCodTipoPessoa() == 1){
                $oValidate->add_text_field("Email", $oCliente->getEmail());
                $oValidate->add_text_field("CPF", $oCliente->getIdentificacao());
                $oValidate->add_text_field("Data de Nascimento", $oCliente->getDataNascimento());
            }else{
                $oValidate->add_text_field("Razão Social", $oCliente->getRazaoSocial());
                $oValidate->add_text_field("CNPJ", $oCliente->getIdentificacao());
            }
            $oValidate->add_text_field("Cep", $oCliente->getCep());
            $oValidate->add_text_field("Logradouro", $oCliente->getLogradouro());
            $oValidate->add_text_field("Numero", $oCliente->getNumero());
            $oValidate->add_text_field("Bairro", $oCliente->getBairro());
            $oValidate->add_text_field("Cidade", $oCliente->getCidade());
            $oValidate->add_text_field("Uf", $oCliente->getUf());
            $oValidate->add_text_field("Telefones", $oCliente->getTelefones());

            if($sOP == 'Cadastrar'){
                $oValidate->add_text_field("Incluido Por", $oCliente->getIncluidoPor());
            }else{
                $oValidate->add_text_field("Alterado Por", $oCliente->getAlteradoPor());
            }

            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=Cliente.preparaFormulario&sOP=".$sOP."&nIdCliente=".$_POST['fCodCliente']."";
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":
                if( $nCodCliente = $oFachada->inserirCliente($oCliente)){
                  
                    //preparando template para envio de email
                    $nCodTipoEmail=1;
                    $oEmailModelo = $oFachada->recuperarUmEmailTipo($nCodTipoEmail);
                    $oEmailModeloEscritorio = $oFachada->recuperarUmEmailTipoEscritorio($nCodTipoEmail,$nIdEscritorio);
                    $oCliente1 = $oFachada->recuperarUmCliente($nCodCliente);


                    $sCabecalho = $_SESSION['oEscritorio']->getCabecalhoEmail();
                    $sSite = ($_SESSION['oEscritorio']->getSite()) ? $_SESSION['oEscritorio']->getSite() : "https://sistema.arqmanager.com.br";
                    $sCabecalho = str_replace("#SITE#","{$sSite} ",$sCabecalho);
                    $sCabecalho = str_replace("#LOGO#","{$_SESSION['oEscritorio']->getLogomarca()} ",$sCabecalho);
                    // montando o rodape do email
                    $sRodape = $_SESSION['oEscritorio']->getRodapeEmail();
                    $sRodape = str_replace("#ESCRITORIO#","{$_SESSION['oEscritorio']->getNomeFantasia()} ",$sRodape);
                    $sRodape = str_replace("#ENDERECO_REDUZIDO#","{$_SESSION['oEscritorio']->getEnderecoReduzido()} ",$sRodape);


                    $sConteudo = ($oEmailModeloEscritorio) ? $oEmailModeloEscritorio->getTemplate() : $oEmailModelo->getTemplate();
                    $sConteudo = str_replace("#SAUDACAO#","Olá {$oCliente1->getNome()} ",$sConteudo);
                    $sConteudo = str_replace("#INSTAGRAM#","{$_SESSION['oEscritorio']->getInstagram()} ",$sConteudo);
                    $sConteudo = htmlspecialchars($sConteudo);
                    if(($_SESSION['oEscritorio']->getEnvioEmail() === 'S') && ($_REQUEST['sEnviarEmail'] == 'S')) {
                      $oMailer = new Mailer();
                      $sPara = $oCliente->getEmail();
                      //$sPara = "bc_fabio@hotmail.com";

                      $sAssunto = "[".$_SESSION['oEscritorio']->getNomeFantasia()."] ".$oEmailModelo->getTitulo();
                      $oMailer->enviar("{$oCliente->getNome()}", "{$sPara}", "{$sAssunto}", "template", ["sCabecalho"=>$sCabecalho,"sConteudo"=>$sConteudo,"sRodape"=>$sRodape]);
                    }
                    // Inserir na Tabela de Email
                    $sDescricaoEmail = $oEmailModelo->getTitulo();
                    $dDataHora = date('Y-m-d H:i:s');

                    $oEmail = $oFachada->inicializarEmail(null,$oCliente1->getCodCliente(),$nCodTipoEmail,$sDescricaoEmail,$dDataHora);
                    $oFachada->inserirEmail($oEmail);


                    unset($_SESSION['oCliente']);
                    $_SESSION['sMsg'] = "Cliente inserido com sucesso!";
                    $sHeader = $_POST['fHeader'] . "&nIdCliente=".$nCodCliente;

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o cliente!";
                    $sHeader = "?bErro=1&action=Cliente.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Alterar":
                if($oFachada->alterarCliente($oCliente)){
                    unset($_SESSION['oCliente']);
                    $_SESSION['sMsg'] = "Cliente alterado com sucesso!";
                    $sHeader = $_POST['fHeader'];

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o cliente!";
                    $sHeader = "?bErro=1&action=Cliente.preparaFormulario&sOP=".$sOP."&nIdCliente=".$_POST['fCodCliente'];
                }
                break;
            case "Excluir":
                $bResultado = true;
                $vIdPaiCliente = explode("____",$_REQUEST['fIdCliente']);
                foreach($vIdPaiCliente as $vIdFilhoCliente){
                    $vIdCliente = explode("||",$vIdFilhoCliente);
                    if($vIdCliente){
                        foreach($vIdCliente as $nIdCliente){
                            // verifica se cliente tem Proposta
                            if(!$oFachada->recuperarTodosPropostaPorCliente($vIdCliente[0])){
                                //Apaga Email
                                $oFachada->excluirEmailPorCliente($vIdCliente[0]);
                                $bResultado &= $oFachada->excluirCliente($vIdCliente[0],$vIdCliente[1]);
                            }
                        }
                    }
                }
                if($bResultado){
                    $_SESSION['sMsg'] = "Cliente(s) exclu&iacute;do(s) com sucesso!";
                    $sHeader = "?bErro=0&action=Cliente.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Cliente(s)!";
                    $sHeader = "?bErro=1&action=Cliente.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);

    }

}
