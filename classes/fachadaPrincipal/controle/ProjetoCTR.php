<?php
class ProjetoCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $sStatus = (isset($_REQUEST['sStatus'])) ? $_REQUEST['sStatus'] : null;
        $nCodDocumento=2;//Contrato;
        $_REQUEST['oDocumentoContrato'] = $oFachada->recuperarUmDocumentoEscritorioPorDocumento($nIdEscritorio,$nCodDocumento);
        $nCodDocumento=4;//RRT
        $_REQUEST['oDocumentoRRT'] = $oFachada->recuperarUmDocumentoEscritorioPorDocumento($nIdEscritorio,$nCodDocumento);
        $nCodDocumento=5;//TermoEntrega;
        $_REQUEST['oDocumentoTE'] = $oFachada->recuperarUmDocumentoEscritorioPorDocumento($nIdEscritorio,$nCodDocumento);
        if($sStatus == 'ANDAMENTO'){
            $voProjeto = $oFachada->recuperarTodosProjetoStatus($nIdEscritorio,1);
        }elseif($sStatus == 'CONCLUIDO'){
            $voProjeto = $oFachada->recuperarTodosProjetoStatus($nIdEscritorio,2);
        }else {
            $voProjeto = $oFachada->recuperarTodosProjetoStatus($nIdEscritorio,3);
        }
        $_REQUEST['voProjeto'] = $voProjeto;
        include_once("view/principal/projeto/index.php");
        exit();
    }

    public function preparaListaAjax(){
        $oFachada = new FachadaPrincipalBD();
        if($_REQUEST['nIdCliente']){
            $_REQUEST['voProjeto'] = $oFachada->recuperarTodosProjetoPorCliente($_REQUEST['nIdCliente']);
            include_once("view/principal/projeto/projeto_select_ajax.php");
        }else{
            $_REQUEST['voProjeto'] = $oFachada->recuperarTodosProjetoAtivo();
            include_once("view/principal/projeto/select_ajax.php");
        }

        exit();

    }

    public function preparaListaAjax2(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $voProjetoPagamento = $oFachada->recuperarTodosProjetoPagamentoPendente($nIdEscritorio);
        $_REQUEST['voFormaPagamento'] = $oFachada->recuperarTodosFormaPagamento();
        $_REQUEST['voProjetoPagamento'] = $voProjetoPagamento;
        include_once("view/principal/projeto/lista_ajax.php");
        exit();
    }


    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $oProjeto = false;
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" || $_REQUEST['sOP'] == "AlterarAjax" || $_REQUEST['sOP'] == "Grafico" || $_REQUEST['sOP'] == 'Calendario' || $_REQUEST['sOP'] == 'AlterarStatus'){
            $nIdProjeto = isset($_POST['fIdProjeto']) ? $_POST['fIdProjeto'][0] : ($_GET['nIdProjeto'] ?? $_GET['nCodProjeto']);

            if($nIdProjeto){
                $vIdProjeto = explode("||",$nIdProjeto);
                $oProjeto = $oFachada->recuperarUmProjeto($vIdProjeto[0]);
                $_REQUEST['oCliente'] = $oFachada->recuperarUmCliente($oProjeto->getCodCliente());
                $_REQUEST['oServico'] = $oFachada->recuperarUmServico($oProjeto->getCodServico());
                $_REQUEST['oProposta'] = $oFachada->recuperarUmProposta($oProjeto->getCodProposta());
                $_REQUEST['nPrazo'] = $oFachada->recuperarTotalDiasUteisProposta($oProjeto->getCodProposta());
                $_REQUEST['voProjetoPagamento'] = $oProjeto->getPagamentos();

                $_REQUEST['voColaborador'] = $oFachada->recuperarTodosColaborador($nIdEscritorio);

                switch($_REQUEST['sOP']){
                    case 'Grafico':
                        $_REQUEST['voEtapas'] = $oFachada->recuperarTodosPropostaServicoEtapaPorProjeto($oProjeto->getCodProjeto());

                        $_REQUEST['voPrazos'] = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta(null,$oProjeto->getCodProjeto());
                        $_REQUEST['oProjeto'] = (isset($_SESSION['oProjeto'])) ? $_SESSION['oProjeto'] : $oProjeto;
                        include_once("view/principal/projeto/grafico.php");
                        break;
                    case "Calendario":
                        $_REQUEST['oProjeto'] = (isset($_SESSION['oProjeto'])) ? $_SESSION['oProjeto'] : $oProjeto;
                        $_REQUEST['voPropostaMicroServico'] = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($oProjeto->getCodProposta());
                        $_REQUEST['voEtapa'] = $voEtapa = $oFachada->recuperarTodosPropostaServicoEtapaPorProjeto($oProjeto->getCodProjeto());
                        $_REQUEST['nPrazo'] = (int) $oFachada->recuperarUmPropostaServicoEtapaPrazoPorProposta($oProjeto->getCodProposta())->getPrazo();
                        $_REQUEST['oArquivamento'] = $oFachada->recuperarUltimoProjetoArquivamentoPorProjeto($oProjeto->getCodProjeto());

                        include_once("view/principal/projeto/calendario.php");
                        break;
                    case 'AlterarStatus':
                        $_REQUEST['voProjetoArquivamento'] = $oFachada->recuperarTodosProjetoArquivamento($oProjeto->getCodProjeto());
                        $_REQUEST['fCodStatus'] = $_REQUEST['CodStatus'];
                        $_REQUEST['oProjeto'] = $oFachada->recuperarUmVProjeto($oProjeto->getCodProjeto());
                        include_once("view/principal/projeto/form_arquivamento.php");
                        break;
                }
            }
        }elseif($_REQUEST['sOP'] == 'Cadastrar'){
            $_REQUEST['voIndicacao'] = $oFachada->recuperarTodosIndicacao($nIdEscritorio);
 
            if($nIdEscritorio !=2){
                if(isset($_POST['fCodProposta'])){
                    $oProposta = $oFachada->recuperarUmProposta($_REQUEST['fCodProposta']);
                    $_REQUEST['oProposta'] = $oProposta;
                    $_REQUEST['voProposta'] = null;
                } else{
                    $nCodStatus='1,2';
                    $_REQUEST['voProposta'] = $oFachada->recuperarTodosPropostaSemProjeto($nCodStatus,$nIdEscritorio);
                }
            }else{
            
                


            }
        }




        
        $_REQUEST['oProjeto'] = (isset($_SESSION['oProjeto'])) ? $_SESSION['oProjeto'] : $oProjeto;
        unset($_SESSION['oProjeto']);

        $_REQUEST['voServico'] = $oFachada->recuperarTodosServico($nIdEscritorio);
        $_REQUEST['voCliente'] = $oFachada->recuperarTodosCliente($nIdEscritorio);
        $_REQUEST['voFormaPagamento'] = $oFachada->recuperarTodosFormaPagamento();
        $_REQUEST['voContaBancaria'] = $oFachada->recuperarTodosContaBancaria($nIdEscritorio);
        $_REQUEST['voClienteColaborador'] = $oFachada->recuperarTodosClienteColaborador($nIdEscritorio);

        if($_REQUEST['sOP'] == "Detalhar"){
            $_REQUEST['voPropostaMicroServico'] = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($oProjeto->getCodProposta());
            $_REQUEST['voPrazos'] = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta(null,$oProjeto->getCodProjeto());
            $_REQUEST['voPagamento'] = $oFachada->recuperarTodosProjetoPagamentoPorProjetoView($oProjeto->getCodProjeto());

            include_once("view/principal/projeto/detalhe.php");
            exit();
        }

        switch($_REQUEST['sOP']){
            case "Cadastrar":
            case "Alterar":
                if($nIdEscritorio !=2)
                    include_once("view/principal/projeto/insere_altera.php");
                else
                    include_once("view/principal/projeto/insere_altera_larissa1.php");
                
                break;
            case "AlterarAjax":
                include_once("view/principal/projeto/insere_altera_ajax.php");
                break;
        }
        exit();
    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
        $oProjeto = null;
        $sHeader = "";
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        if($sOP != "Excluir" && $sOP != "Concluir" && $sOP != "AtualizarPrazos" && $sOP != "AlterarStatus"){
            switch($sOP){
                case 'Cadastrar':
                    $_POST['fIncluidoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    $_POST['fAlteradoPor'] = null;
                    $_POST['fAnoProjeto'] = date('Y');
                    $_POST['fCodStatus'] = 7;
                    $_POST['fNumeroProjeto'] = $oFachada->recuperarProximoProjeto($nIdEscritorio,$_POST['fAnoProjeto'])->numero_projeto;
                    break;
                case 'Alterar':
                    if(is_array($_POST['fCodProjeto']))
                        $_POST['fCodProjeto'] = $_POST['fCodProjeto'][0];
                    $oProjeto1 = $oFachada->recuperarUmProjeto($_POST['fCodProjeto']);
                    $_POST['fIncluidoPor'] =  $oProjeto1->getIncluidoPor();
                    $_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    $_POST['fCodStatus'] = $oProjeto1->getCodStatus();
                    break;
            }
            if($nIdEscritorio ==2){

                $sFormaPagamento = ($_POST['fFormaPagamento']) ?? null;
                $_POST['fValorAvista'] = null;
                $_POST['fValorParcelaAprazo'] = $_POST['fPrazoProposta'];
                $_POST['fCodStatus'] = ($_POST['fCodStatus'])?:'7'; // em andamento
                $_POST['fVisitasIncluidas'] = null;
                $_POST['fInseridoPor'] = $_POST['fIncluidoPor'];
                $_POST['fNumeroProposta'] = $oFachada->recuperarProximoProposta($nIdEscritorio,$_POST['fAnoProjeto'])->numero_proposta;

                $_POST['fAnoProposta'] = $_POST['fAnoProjeto'];
                if($_POST['fCodCliente']){
                    $oCliente = $oFachada->recuperarUmCliente($_POST['fCodCliente']);
                    $_POST['fNome'] = $oCliente->getNome();
                }

                $oProposta = $oFachada->inicializarProposta($_POST['fCodProposta'],$_POST['fCodCliente'],$_POST['fCodServico'],$_POST['fNome'],$_POST['fDescricao'],$_POST['fIdentificacao'],$_POST['fValorProposta'],$_POST['fValorAvista'],$_POST['fValorParcelaAprazo'],$_POST['fCodStatus'],$_POST['fDataProposta'],$_POST['fVisitasIncluidas'],$_POST['fInseridoPor'],$_POST['fAlteradoPor'],$nIdEscritorio,$_POST['fNumeroProposta'],$_POST['fAnoProposta'],$_POST['fEntregaParcial'],$sFormaPagamento,$_POST['fPrazoProposta']);
                $_POST['fDataInicio'] = $_POST['fDataProposta'];
                if($_POST['fCodProposta']){
                    $oFachada->alterarProposta($oProposta);
                }else{
                    $_POST['fCodProposta'] = $oFachada->inserirProposta($oProposta);
                }

            }
            $oProjeto = $oFachada->inicializarProjeto($_POST['fCodProjeto'],$_POST['fCodCliente'],$_POST['fCodServico'],$_POST['fCodProposta'],$_POST['fDescricao'],$_POST['fDataInicio'],null,null,$_POST['fMetragem'],$_POST['fObservacao'],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1,$_POST['fNumeroProjeto'],$_POST['fAnoProjeto'],$_POST['fCodStatus']);
            //Gerar data Previsão Fim
            $voServicoEtapa = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta($_POST['fCodProposta']);
           
            $nDias = 0;
            if($voServicoEtapa){
                foreach ($voServicoEtapa as $oServicoEtapa) {
                    $nDias += $oServicoEtapa->getPrazo();
                }
                try {
                    $dData = new DateTime($oProjeto->getDataInicio());
                    $vDataFim = $dData->add(new DateInterval("P{$nDias}D"));
                    $oProjeto->setDataPrevisaoFim($vDataFim->format("Y-m-d"));
                } catch (Exception $e) {
                }

                $_SESSION['oProjeto'] = $oProjeto;

                $oValidate = FabricaUtilitario::getUtilitario("Validate");
                $oValidate->check_4html = true;

                if($sOP =='Alterar')
                    $oValidate->add_text_field("Alterado Por", $oProjeto->getAlteradoPor());

                if (!$oValidate->validation()) {
                    $_SESSION['sMsg'] = $oValidate->create_msg();
                    $sHeader = "?bErro=1&action=Projeto.preparaFormulario&sOP=".$sOP."&nIdProjeto=".$_POST['fCodProjeto'];
                    header("Location: ".$sHeader);
                    die();
                }
            }
            if ($_POST['fEntregaParcial'] == "Sim"){
                if($_POST['fCodFracionamento']){
                    foreach ($_POST['fCodFracionamento'] as $nKey=>$nCodFracionamento) {
                        $oPropostaFracionada = $oFachada->inicializarPropostaFracionada($_POST['fCodFracionamento'][$nKey],$nCodProposta,$_POST['fNumeroEntrega'][$nKey],$_POST['fDescricaoEntrega'][$nKey],$_POST['fPercentual'][$nKey],$_POST['fDataPrevista'][$nKey],null,$_POST['fValor'][$nKey],$_POST['fParcelas'][$nKey],$_POST['fIndiceCorrecao'][$nKey],null);
    
                        if (!$oFachada->inserirPropostaFracionada($oPropostaFracionada)){
                            $_SESSION['sMsg'] = "Erro ao inserir Etapas!";
                            $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=Alterar&nIdProposta=$nCodProposta";
                            header("Location: ".$sHeader);
                            die();
                        }
                    }
    
                }
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($nCodProjeto = $oFachada->inserirProjeto($oProjeto)){
//                    atualizar proposta para concluida
                    $oPropostaStatus = $oFachada->recuperarUmProposta($oProjeto->getCodProposta());
                    $oPropostaStatus->setCodStatus(5);
                    $oFachada->alterarProposta($oPropostaStatus);

                    foreach ($_POST['fValor'] as $nKey=>$nValor) {
                        $oProjetoPagamento = $oFachada->inicializarProjetoPagamento('',$nCodProjeto,$_POST['fCodFormaPagamento'][$nKey],$_POST['fValor'][$nKey],$_POST['fDataPrevisao'][$nKey],null,null,$_POST['fDescPagamento'][$nKey],$_POST['fIncluidoPor'],null,1);
                        $oFachada->inserirProjetoPagamento($oProjetoPagamento);
                    }

                    /**
                     * Atualizar Micro Servico
                     */
                    $voServicoMicroServico = $oFachada->recuperarTodosServicoMicroServico($_POST['fCodServico'],$oProjeto->getCodProposta());

                    $vMicroServico = [];
                    if($voServicoMicroServico){
                        foreach ($voServicoMicroServico as $oServicoMicroServico) {
                            $vMicroServico[] = $oServicoMicroServico->getCodMicroservico();
                        }
                    }

                    /**
                     * Excluir Microserviços
                     */
                    if($vMicroServico){
                        foreach($vMicroServico as $nCodMicroServico){
                            if (!in_array($nCodMicroServico,$_POST['fIdServicoMicroServico']))
                                $oFachada->excluirPropostaMicroServico($nCodMicroServico,$oProjeto->getCodProposta());
                        }
                    }

                    foreach ($_POST['fIdServicoMicroServico'] as $nKey=>$nCodMicroServico) {
                        /**
                         * Vincular Microserviços
                         */
                        $sCampoDesc = "fDescricaoQuantidade$nCodMicroServico";
                        $sCampoQtde = "fQuantidade$nCodMicroServico";
                        $oPropostaMicroServico = $oFachada->recuperarUmPropostaMicroServico($nCodMicroServico,$oProjeto->getCodProposta());

                        if ($oPropostaMicroServico){
                            $oPropostaMicroServico->setCodProjeto($nCodProjeto);
                            $oPropostaMicroServico->setQuantidade($_POST[$sCampoQtde]);
                            $oPropostaMicroServico->setDescricaoQuantidade($_POST[$sCampoDesc]);
                            $oPropostaMicroServico->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));

                            if(!$oFachada->alterarPropostaMicroServico($oPropostaMicroServico))
                                $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoMicroServico ALTERAR]!";

                        } else{
                            /**
                             * Inserir Microserviços
                             */
                            // campo nDias
                            $oPropostaMicroServico = $oFachada->inicializarPropostaMicroServico($nCodMicroServico,$_POST['fCodProposta'],$nCodProjeto,$_POST[$sCampoQtde],$_POST[$sCampoDesc],null,null,null,null,null,null,$_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'),$_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));

                            if(!$oFachada->inserirPropostaMicroServico($oPropostaMicroServico))
                                $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoMicroServico INSERIR]!";

                        }
                    }

                    /**
                     * Atualizar Etapa
                     */
                    $voServicoEtapa = $oFachada->recuperarTodosServicoEtapaPorServico($_POST['fCodServico']);
                  
                    $vServicoEtapa = [];
                    if($voServicoEtapa){
                        foreach ($voServicoEtapa as $oServicoEtapa) {
                            $vServicoEtapa[] = $oServicoEtapa->getCodServicoEtapa();
                        }
                    }
                   
                    if($vServicoEtapa){
                        foreach($vServicoEtapa as $nKey=>$nCodEtapa){
                            if($_POST['fIdServicoEtapa']){
                                if (in_array($nCodEtapa,$_POST['fIdServicoEtapa'])) {
                                    $_POST['fDataConclusao'][$nKey] = NULL;
                                    $oPropostaServicoEtapa = $oFachada->inicializarPropostaServicoEtapa($nCodEtapa,$_POST['fCodProposta'],$nCodProjeto,$_POST['fDescricaoEtapa'][$nKey],$_POST['fPrazo'][$nKey],$_POST['fDataPrevisao'][$nKey],$_POST['fDataConclusao'][$nKey],'');
        
                                    if(!$oFachada->alterarPropostaServicoEtapa($oPropostaServicoEtapa)){
                                        $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoEtapa ALTERAR]!";
                                    }
                                } else{
                                    //Etapas desmarcadas excluir
                                    if($oFachada->presentePropostaServicoEtapa($nCodEtapa,$_POST['fCodProposta']))
                                        $oFachada->excluirPropostaServicoEtapa($nCodEtapa,$_POST['fCodProposta']);
                                }
                            }
                            
                        }
                    }
                    
                    if($_POST['fIdServicoEtapa'] ){
                        foreach ($_POST['fIdServicoEtapa'] as $nKey => $nIdServicoEtapa) {
                            if(!$oFachada->recuperarUmPropostaServicoEtapa($nIdServicoEtapa,$_POST['fCodProposta'])){
                                $oPropostaServicoEtapa = $oFachada->inicializarPropostaServicoEtapa($nIdServicoEtapa,$_POST['fCodProposta'],$nCodProjeto,$_POST['fDescricaoEtapa'][$nKey],$_POST['fPrazo'][$nKey],$_POST['fDataPrevista'][$nKey],$_POST['fDataConclusao'][$nKey],'');
    
                                if(!$oFachada->inserirPropostaServicoEtapa($oPropostaServicoEtapa)){
                                    $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoEtapa INSERIR]!";
                                }
                            }
                        }
                    }
                    
                    $oFachada->atualizarPrazosProjeto($nCodProjeto,1);
                    $oFachada->atualizarPrazosProjeto($nCodProjeto,2);

                    unset($_SESSION['oProjeto']);
                    $_SESSION['sMsg'] = "Projeto inserido com sucesso!";
                    $sHeader = "?action=Projeto.preparaFormulario&sOP=Alterar&nIdProjeto={$nCodProjeto}";

                }else{
                echo     $_SESSION['sMsg'] = "Não foi possível inserir o Projetoo!";

                    $sHeader = "?bErro=1&action=Projeto.preparaFormulario&nIdProposta={$_POST['fCodProposta']}&sOP={$sOP}";
                }

                break;

            case "Alterar":
                $voPagamentoConfirmado = $oFachada->recuperarTodosProjetoPagamentoConfirmados($oProjeto->getCodProjeto());
              
                if($oFachada->alterarProjeto($oProjeto)){
                    $nCodProjeto = $oProjeto->getCodProjeto();
                    foreach($_POST['fCodFormaPagamento'] as $nKey=>$nCodFormaPagamento){
                        if(!$_POST['fDataEfetivacao'][$nKey]){
                            if($_POST['fValor'][$nKey] >0){
                                $oProjetoPagamento = $oFachada->inicializarProjetoPagamento($_POST['fCodProjetoPagamento'][$nKey],$_POST['fCodProjeto'],$_POST['fCodFormaPagamento'][$nKey],$_POST['fValor'][$nKey],$_POST['fDataPrevisao'][$nKey],$_POST['fDataEfetivacao'][$nKey],$_POST['fComprovante'][$nKey],$_POST['fDescPagamento'][$nKey],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1);
                                if($_POST['fCodProjetoPagamento'][$nKey] !=''){
                                    $oProjetoPagamento->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
                                    $oFachada->alterarProjetoPagamento($oProjetoPagamento);
                                }else{
                                    $oFachada->inserirProjetoPagamento($oProjetoPagamento);
                                }
                            }
                        }
                    }

                    //Atualizar Micro Servico
                    // verificar se ja foi efetuado algum pagamento;

                    if(!$voPagamentoConfirmado){
                        $voServicoMicroServico = $oFachada->recuperarTodosServicoMicroServico($_POST['fCodServico'],$oProjeto->getCodProposta());
                        $vMicroServico = [];
                        foreach ($voServicoMicroServico as $oServicoMicroServico) {
                            $vMicroServico[] = $oServicoMicroServico->getCodMicroservico();
                        }

                        /**
                         * Excluir Microserviços
                         */
                        if(!$voPagamentoConfirmado){
                            if($vMicroServico){
                                foreach($vMicroServico as $nCodMicroServico){
                                    if (!in_array($nCodMicroServico,$_POST['fIdServicoMicroServico'])){
                                        $oFachada->excluirPropostaMicroServico($nCodMicroServico,$oProjeto->getCodProposta());
                                    }
                                }
                            }

                            foreach ($_POST['fIdServicoMicroServico'] as $nKey=>$nCodMicroServico) {
                                /**
                                 * Vincular Microserviços
                                 */
                                $sCampoDesc = "fDescricaoQuantidade$nCodMicroServico";
                                $sCampoQtde = "fQuantidade$nCodMicroServico";
                                $oPropostaMicroServico = $oFachada->recuperarUmPropostaMicroServico($nCodMicroServico,$oProjeto->getCodProposta());

                                if ($oPropostaMicroServico){
                                    $oPropostaMicroServico->setCodProjeto($nCodProjeto);
                                    $oPropostaMicroServico->setQuantidade($_POST[$sCampoQtde]);
                                    $oPropostaMicroServico->setDescricaoQuantidade($_POST[$sCampoDesc]);
                                    $oPropostaMicroServico->setDias($_POST['fDias'][$nKey]);
                                    $oPropostaMicroServico->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));

                                    if(!$oFachada->alterarPropostaMicroServico($oPropostaMicroServico))
                                        $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoMicroServico ALTERAR]!";

                                } else{
                                    /**
                                     * Inserir Microserviços
                                     */
                                    // campo nDias
                                    $oPropostaMicroServico = $oFachada->inicializarPropostaMicroServico($nCodMicroServico,$_POST['fCodProposta'],$nCodProjeto,$_POST[$sCampoQtde],$_POST[$sCampoDesc],null,null,null,null,null,null,$_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'),$_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
                                    if(!$oFachada->inserirPropostaMicroServico($oPropostaMicroServico))
                                        $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoMicroServico INSERIR]!";
                                }
                            }

                            //Atualizar Etapa
                            $voServicoEtapa = $oFachada->recuperarTodosServicoEtapaPorServico($_POST['fCodServico']);
                            $vServicoEtapa = [];
                            foreach ($voServicoEtapa as $oServicoEtapa) {
                                $vServicoEtapa[] = $oServicoEtapa->getCodServicoEtapa();
                            }

                            foreach($vServicoEtapa as $nKey=>$nCodEtapa){
                                if (in_array($nCodEtapa,$_POST['fIdServicoEtapa'])) {   
                                    $_POST['fDataConclusao'][$nKey] = NULL;
                                    $oPropostaServicoEtapa = $oFachada->inicializarPropostaServicoEtapa($nCodEtapa,$oProjeto->getCodProposta(),$oProjeto->getCodProjeto(),$_POST['fDescricaoEtapa'][$nKey],$_POST['fPrazo'][$nKey],$_POST['fDataPrevista'][$nKey],$_POST['fDataConclusao'][$nKey],'');
                                    if($oPropostaServicoEtapaBanco = $oFachada->recuperarUmPropostaServicoEtapa($nCodEtapa,$oProjeto->getCodProposta())){
                                        if(!$oFachada->alterarPropostaServicoEtapa($oPropostaServicoEtapa)){
                                            $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoEtapa ALTERAR]!";
                                        }
                                    }else{
                                        $oFachada->inserirPropostaServicoEtapa($oPropostaServicoEtapa);
                                    }
                                } else{
                                    if($oFachada->recuperarUmPropostaServicoEtapa($nCodEtapa,$oProjeto->getCodProposta()))
                                        $oFachada->excluirPropostaServicoEtapa($nCodEtapa,$oProjeto->getCodProposta());
                                }
                            }

                            foreach ($_POST['fIdServicoEtapa'] as $nKey => $nIdServicoEtapa) {
                                if(!$oFachada->recuperarUmPropostaServicoEtapa($nIdServicoEtapa,$oProjeto->getCodProposta())){
                                    $oPropostaServicoEtapa = $oFachada->inicializarPropostaServicoEtapa($nIdServicoEtapa,$oProjeto->getCodProposta(),$oProjeto->getCodProjeto(),$_POST['fDescricaoEtapa'][$nKey],$_POST['fPrazo'][$nKey],$_POST['fDataPrevista'][$nKey],$_POST['fDataConclusao'][$nKey],'');
                                    if(!$oFachada->inserirPropostaServicoEtapa($oPropostaServicoEtapa)){
                                        $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoEtapa INSERIR]!";
                                    }
                                }
                            }
                            $oFachada->atualizarPrazosProjeto($oProjeto->getCodProjeto(),1);
                            $oFachada->atualizarPrazosProjeto($oProjeto->getCodProjeto(),2);
                        }
                    } // if($sPagamentoConfirmado == 0){

                    unset($_SESSION['oProjeto']);
                    $_SESSION['sMsg'] = "Projeto alterado com sucesso!";
                    $sHeader = "?bErro=0&action=Projeto.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "Não foi possível alterar o Projeto!";
                    $sHeader = "?bErro=1&action=Projeto.preparaFormulario&sOP={$sOP}&nIdProjeto={$_POST['fCodProjeto']}";
                }
                break;

            case "AtualizarPrazos":
                if($_POST['fDias']){
                    foreach($_POST['fDias'] as $nKey=>$nDias){
                        if(!$oFachada->alterarPropostaMicroServicoPrazo($_POST['fCodProposta'],$_POST['fCodMicroservico'][$nKey],$nDias)){
                            $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoMicroServico ALTERAR]!";
                        }
                    }
                }
                $oFachada->atualizarPrazosProjeto($_POST['fCodProjeto'],1);
                $oFachada->atualizarPrazosProjeto($_POST['fCodProjeto'],2);

                if ($_POST['fIdArquivamento']){
                    $oArquivamento = $oFachada->recuperarUmProjetoArquivamento($_POST['fIdArquivamento']);
                    $oArquivamento->setFlagCalendario(0);

                    $oFachada->alterarProjetoArquivamento($oArquivamento);
                }


                unset($_SESSION['oProjeto']);

                $_SESSION['sMsg'] = "Prazos alterados com sucesso!";
                $sHeader = "?bErro=0&action=Projeto.preparaFormulario&sOP=Calendario&nCodProjeto=".$_POST['fCodProjeto'];


                break;
            case "AlterarStatus":


                $oProjeto = $oFachada->recuperarUmProjeto($_REQUEST['fCodProjeto']);
                $oProjeto->setCodStatus($_REQUEST['fCodStatus']);
                $sAcao = ($_REQUEST['fCodStatus']=='6')? "Arquivado" : "Desarquivado";
                $nFlagCalendario = ($sAcao == "Arquivado") ? "0" : 1;
                if($oFachada->alterarProjeto($oProjeto)){

                    $oProjetoArquivamento = $oFachada->inicializarProjetoArquivamento($nId,$_REQUEST['fCodProjeto'],$_REQUEST['fCodStatus'],$_SESSION['oUsuarioAM']->getCodColaborador(),$_REQUEST['fDataAcao'],$_REQUEST['fMotivoAcao'],$nFlagCalendario);

                    if($oFachada->inserirProjetoArquivamento($oProjetoArquivamento)){
                        echo json_encode(['sMsg'=>"Projeto $sAcao!",'sClass'=>'success','nCodProjeto'=>$oProjeto->getCodProjeto()]);
                    }else{
                        echo json_encode(['sMsg'=>'Não foi possível arquivar/desarquivar projeto!','sClass'=>'danger','nCodProjeto'=>$oProjeto->getCodProjeto()]);
                    }
                }else{
                    echo json_encode(['sMsg'=>'Status do Projeto não foi alterado!','sClass'=>'danger','nCodProjeto'=>$oProjeto->getCodProjeto()]);
                }
                die();
                break;
            case "Excluir":
                $nCodProjeto = $_REQUEST['fIdProjeto'];
                $oProjeto = $oFachada->recuperarUmProjeto($nCodProjeto);
                // verifica pagamento confirmado
                if($oFachada->recuperarTodosProjetoPagamentoConfirmados($nCodProjeto)){
                    $_SESSION['sMsg'] = "Não foi possível excluir o Projeto pois existe pagamento efetivado nele!";
                    $sHeader = "?bErro=1&action=Projeto.preparaLista";
                }else{
                    //apaga pagamento do projeto
                    $oFachada->excluirProjetoPagamentoPorProjeto($nCodProjeto);
                    // seta como null o campo cod_projeto nas tabelas proposta_microservico e proposta_servico_etapa
                    //alterarSemChavePrimaria
                    //FALTA FINALIZAR
                    $oFachada->alterarPropostaMicroServicoPorProjeto($nCodProjeto);
                    $oFachada->alterarPropostaServicoEtapaPorProjeto($nCodProjeto);

                    // altera status da proposta para 1
                    $oProposta = $oFachada->recuperarUmProposta($oProjeto->getCodProposta());
                    $oProposta->setCodStatus(1);
                    $oFachada->alterarProposta($oProposta);
                    $bResultado = $oFachada->excluirProjeto($nCodProjeto);

                    if($bResultado){
                        $_SESSION['sMsg'] = "Projeto(s) excluído(s) com sucesso!";
                        $sHeader = "?bErro=0&action=Projeto.preparaLista";
                    } else {
                        $_SESSION['sMsg'] = "Não foi possível excluir o(s) Projeto!";
                        $sHeader = "?bErro=1&action=Projeto.preparaLista";
                    }

                }
                break;
        }
        header("Location: ".$sHeader);

    }

}
