<?php
class RelatorioCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $sOP = $_REQUEST['sOP'];
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $_REQUEST['voColaborador'] = $oFachada->recuperarTodosColaborador($nIdEscritorio);
        switch ($sOP){
            case "Concluido":
                $_REQUEST['voProjeto'] = $oFachada->recuperarTodosProjetoStatus($nIdEscritorio,2);
                include_once("view/principal/relatorio/projetos_concluidos.php");
            break;
            case "Arquivados":

                $_REQUEST['voProjetoArquivamento'] = $voProjetoArquivamento = $oFachada->recuperarTodosProjetoArquivados($nIdEscritorio);
                $_REQUEST['sEntregas']='Arquivados';
                include_once("view/principal/relatorio/arquivados_ajax.php");
                break;
            case "Entrega":
                $_REQUEST['voServicoEtapa'] = $voServicoEtapa = $oFachada->recuperarTodosPropostaServicoEtapaEmAndamento($nIdEscritorio);
                $_REQUEST['voEtapa'] = $voEtapa = $oFachada->recuperarTodosEtapa($nIdEscritorio);
                    $_REQUEST['voProjeto'] = $voProjeto = $oFachada->recuperarTodosProjetoEmAndamento($nIdEscritorio);
                    $vEntrega = [];
                    if ($voProjeto){
                        foreach ($voProjeto as $oProjeto) {
                            $nCodProjeto = $oProjeto->cod_projeto;

                            $vEntrega[$oProjeto->cod_projeto] = [
                                "Identificacao"=>$oProjeto->projeto,
                                "Calendario"=>$oProjeto->flag_calendario
                            ];
                            $dDataPrevista = null;
                            $dDataConclusao = null;
                            $dDataAtual = new DateTime();

                            foreach ($voEtapa as $oEtapa) {
                                foreach ($voServicoEtapa as $oServicoEtapa) {
                                    $nCodServicoEtapa = $oServicoEtapa->cod_servico_etapa;
                                    if ($oServicoEtapa->cod_projeto == $nCodProjeto And $oServicoEtapa->cod_etapa == $oEtapa->getCodEtapa()) {
                                        $dDataPrevista = $oServicoEtapa->data_prevista;
                                        $dDataConclusao = $oServicoEtapa->data_conclusao;
                                        break;
                                    } else {
                                        $dDataPrevista = null;
                                        $dDataConclusao = null;
                                    }
                                }
                                $oDataPrevista = new DateTime($dDataPrevista);
                                $interval = $dDataAtual->diff($oDataPrevista);
                                $nDias = $interval->format('%R%a');

                                if ($dDataConclusao || !$oDataPrevista){
                                    $sClasse = 'success';
                                    $sCor = "#ff7f7f";
                                }else if ($nDias < 0){
                                    $sClasse = "danger";
                                    $sCor = "#ffb0b0";
                                }else if ($nDias >= 1 && $nDias <= 7){
                                    $sClasse = 'warning';
                                    $sCor = "#fff873";
                                }else{
                                    $sClasse = 'success';
                                    $sCor = "#c6f2b4";
                                }

                                $vEntrega[$oProjeto->cod_projeto]['Etapas'][$oEtapa->getCodEtapa()] = [
                                    'Descricao'=>$oEtapa->getDescricao(),
                                    'DataPrevista'=>maskInvData($dDataPrevista),
                                    'DataConclusao'=>$dDataConclusao,
                                    'Classe'=>$sClasse,
                                    'Cor'=>$sCor,
                                    'Acao'=>($dDataPrevista) ? 1 : 0,
                                    'Dias'=>$nDias,
                                    'ServicoEtapa'=>$nCodServicoEtapa
                                ];
                            }
                        }
                    }
                    $_REQUEST['vEntrega'] = $vEntrega;
                    include_once("view/principal/relatorio/entregas.php");
                break;
        }
        exit();
    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        exit();

    }

    public function calculaPrazo(){
        $oFachada = new FachadaPrincipalBD();
        $dDataBanco = $oFachada->calculaDataDiasUteis($_REQUEST['sTipo'],maskData($_REQUEST['dData']),$_REQUEST['nDias']);
        $oData = new DateTime($dDataBanco);
        echo $_REQUEST['dDataFormatada'] =  $oData->format("d/m/Y");
        //return $dDataBanco;
        exit();

    }


    public function preparaFinanceiroMensalOld(){
        $oFachada = new FachadaPrincipalBD();
        $voResultado = false;
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $_REQUEST['voPeriodo'] = $oFachada->recuperarTodosProjetoPagamentoMesAnoPorTipo(0);

        if($_REQUEST['fPeriodo']){
            $vPeriodo = explode('/',$_REQUEST['fPeriodo']);
            $_REQUEST['nMes'] = $vPeriodo[0];
            $_REQUEST['nAno'] = $vPeriodo[1];
            $_REQUEST['oTotais'] = $oFachada->recuperarUmProjetoPagamentoPorMesAnoEfetivadoTotais($_REQUEST['nMes'],$_REQUEST['nAno']);
            $_REQUEST['voResultado'] = $oFachada->recuperarTodosProjetoPagamentoView($_REQUEST['nMes'],$_REQUEST['nAno']);
            $_REQUEST['voServico'] = $oFachada->recuperarTodosServicoEmpresaPorMesPorAnoAno($_REQUEST['nMes'],$_REQUEST['nAno']);
        }
        include_once("view/principal/relatorio/financeiro_mensal.php");
        exit();

    }

    public function preparaFinanceiroMensal(){
        $oFachada = new FachadaPrincipalBD();
        $voResultado = false;
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $_REQUEST['voMeses'] = $oFachada->listarMeses();
        $_REQUEST['oPrincipal'] = $oFachada->recuperarUmRelatorioPrincipal($nIdEscritorio);
        $_REQUEST['nMesSelecionado'] = ($_GET['nMesSelecionado']) ? $_GET['nMesSelecionado'] : date('m');
        $_REQUEST['nAno'] = ($_REQUEST['nAno']) ? $_REQUEST['nAno'] : date('Y');
        $_REQUEST['nEfetivado'] = ($_REQUEST['nEfetivado']) ? $_REQUEST['nEfetivado'] : 1;

        switch($_REQUEST['nEfetivado']){
            case 1: $_REQUEST['sStatus'] = "Previstos"; break;
            case 2: $_REQUEST['sStatus'] = "Confirmados"; break;
            case 3: $_REQUEST['sStatus'] = "Previstos X Confirmados"; break;
        }

        $_REQUEST['voResultado'] = $oFachada->recuperarTodosProjetoPagamentoView($_REQUEST['nMesSelecionado'],$_REQUEST['nAno'],$_REQUEST['nEfetivado'],$nIdEscritorio);
        $_REQUEST['voContaBancaria'] = $oFachada->recuperarTodosContaBancaria($nIdEscritorio);

        include_once("view/principal/relatorio/financeiro_mensal2.php");
        exit();

    }

    public function preparaFinanceiroMensalPrint(){
        $oFachada = new FachadaPrincipalBD();
        $voResultado = false;
        $_REQUEST['voResultado'] = $oFachada->recuperarTodosProjetoColaboradorPorMesAnoColaboradorEfetivadoPrint($_REQUEST['nMes'],$_REQUEST['nAno'],$_REQUEST['nCodColaborador']);
        $_REQUEST['oColaborador'] = $oFachada->recuperarUmColaborador($_REQUEST['nCodColaborador']);
        $_REQUEST['Mes'] = $_REQUEST['nMes'];
        $_REQUEST['Ano'] = $_REQUEST['nAno'];
        $_REQUEST['sLoad'] = ($_GET['sImpressao'] == 'sim') ? $_GET['sImpressao'] : "nao";
        include_once("view/principal/relatorio/financeiro_print.php");
        exit();
    }

    public function preparaFinanceiroColaborador(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $_REQUEST['voColaborador'] = $oFachada->recuperarTodosColaborador($nIdEscritorio);
        $_REQUEST['voAnos'] = $oFachada->recuperarTodosProjetoAno($nIdEscritorio);

        if($_REQUEST['fCodColaborador'] && $_REQUEST['fAno']){
            $_REQUEST['oColaboradorSelecionado'] = $oFachada->recuperarUmColaborador($_REQUEST['fCodColaborador']);
            $_REQUEST['voResultado'] = $oFachada->listarProducaoColaboradorAno($_REQUEST['fCodColaborador'],$_REQUEST['fAno']);
            $_REQUEST['nAnoSelecionado'] = $_REQUEST['fAno'];
        }

        include_once("view/principal/relatorio/financeiro_colaborador.php");
        exit();

    }

    public function preparaExtratoBancario(){
        $oFachada = new FachadaPrincipalBD();
        $oTotais = false;
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        $_REQUEST['voContaBancaria'] = $oFachada->recuperarTodosContaBancaria($nIdEscritorio);
        $_REQUEST['voFormaPagamento'] = $oFachada->recuperarTodosFormaPagamento();
        $_REQUEST['voAno'] = $oFachada->recuperarTodosAno($nIdEscritorio);
        $_REQUEST['voMes'] = $oFachada->recuperarTodosMes();
        $_REQUEST['voPlanoContas'] = $oFachada->recuperarTodosPlanoContasPorGrupoPorVisibilidade($nIdEscritorio,$nCodigo="2.",'0');
        $_REQUEST['voReceita'] = $oFachada->recuperarTodosPlanoContasPorGrupoPorVisibilidade($nIdEscritorio, "1.",'1');
        $_REQUEST['voProjeto'] = $oFachada->recuperarTodosVProjeto($nIdEscritorio);
        $_REQUEST['oPrincipal'] = $oFachada->recuperarUmRelatorioPrincipal($nIdEscritorio);

        include_once("view/principal/relatorio/financeiro.php");
        exit();
    }

    public function preparaExtratoConta()
    {
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $nCodConta = $_POST['nContaBancaria'];

        $_REQUEST['oPrincipalMes'] = $oFachada->recuperarUmContaMovimentacaoPrincipalAnoMesConta($_POST['nAno'],$_POST['nMes'],$nCodConta);
        $_REQUEST['oContaSelecionada'] = $oFachada->recuperarUmContaBancaria($nCodConta);
        $_REQUEST['voContaMovimentacao'] = $oFachada->recuperarTodosContaMovimentacaoPorContaPorPeriodo($nCodConta,$_POST['nMes'],$_POST['nAno'],$nIdEscritorio);

        include_once("view/principal/relatorio/extrato_mes.php");
        exit();
    }

    public function preparaPainelFinanceiro()
    {

        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $_REQUEST['vAno'] = $oFachada->recuperarTodosAno($nIdEscritorio);
        $nAno = $_REQUEST['Ano'];

        $sFinanceiro = "";
        $voGrafico = [];
        switch ($_REQUEST['Tipo']){
            case "Previsto":
                $voPrevisto = $oFachada->recuperarPrevistoRealizadoView($nAno, 'Previsto', $nIdEscritorio);
                $voRealizado = $oFachada->recuperarPrevistoRealizadoView($nAno, 'Realizado', $nIdEscritorio);
                $_REQUEST['vCor'] = "['#00c0ef','#00a65a']";
                $_REQUEST['sTitulo'] = "'"."Previsto / Realizado - $nAno"."'";

                $voGrafico[0] = ["'"."Mês"."'","'"."Previsto"."'","'"."Realizado"."'"];
                if($voPrevisto) {
                    foreach ($voPrevisto as $nKey => $oPrevisto) {
                        $oDate = DateTime::createFromFormat('m', $oPrevisto->mes);
                        $sMes = strftime('%b/' . $nAno, $oDate->getTimestamp());
                        $nPrevisto = ($oPrevisto->valor) ?: 0;
                        $nRealizado = ($voRealizado[$nKey]->valor) ?: 0;
                        $voGrafico[] = ["'" . $sMes . "'", $nPrevisto, $nRealizado];
                    }
                }
                break;
            case "Entrada":
                $voEntrada = $oFachada->recuperarEntradaSaidaView($nAno, 'E', $nIdEscritorio);
                $voSaida = $oFachada->recuperarEntradaSaidaView($nAno, 'S', $nIdEscritorio);
                $_REQUEST['vCor'] = "['#00a65a','#f56954']";
                $_REQUEST['sTitulo'] = "'"."Entradas / Saídas - $nAno"."'";

                $voGrafico[0] = ["'"."Mês"."'","'"."Entrada"."'","'"."Saída"."'"];
                if($voEntrada) {
                    foreach ($voEntrada as $nKey => $oEntrada) {
                        $oDate = DateTime::createFromFormat('m', $oEntrada->mes_efetivacao);
                        $sMes = strftime('%b/' . $nAno, $oDate->getTimestamp());
                        $nEntrada = ($oEntrada->valor) ?: 0;
                        $nSaida = ($voSaida[$nKey]->valor) ?: 0;
                        $voGrafico[] = ["'" . $sMes . "'", $nEntrada, $nSaida];
                    }
                }
                break;
        }

        foreach ($voGrafico as $vGrafico) {
            $sString = implode(",",$vGrafico);
            $sFinanceiro .= "[$sString],";
        }
        $_REQUEST['sFinanceiro'] = substr($sFinanceiro,0,-1);

        include_once("view/principal/relatorio/painel.php");
        exit();
    }
    public function recuperarTotalQuadros() {
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $oMedia = $oFachada->recuperarMediasAnuaisFinanceiro($nIdEscritorio,$_REQUEST['nAno']);
        if($_REQUEST['sTipo'] == "Entrada")
            $vDados = array(
                "media_receita"=>"{$oMedia->media_entrada}",
                "texto_receita_mensal"=>"Média de Entrada",
                "cor1"=>"#008d4d",
                "media_despesa"=>"{$oMedia->media_saida}",
                "texto_despesa_mensal"=>"Média de Saída",
                "cor2"=>"#f56954"
            );

        if($_REQUEST['sTipo'] == "Previsto")
            $vDados = array(
                "media_receita"=>"{$oMedia->media_previsto}",
                "texto_receita_mensal"=>"Média Prevista",
                "cor1"=>"#00a3cb",
                "media_despesa"=>"{$oMedia->media_realizado}",
                "texto_despesa_mensal"=>"Média Efetivada",
                "cor2"=>"#00a65a"
            );
        echo json_encode($vDados);
    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

        if($sOP != "Excluir" && $sOP != "Concluir"){
            switch($sOP){
                case 'Cadastrar':
                    $_POST['fIncluidoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    break;
                case 'Alterar':
                    $oProjeto1 = $oFachada->recuperarUmProjeto($_POST['fCodProjeto']);
                    $_POST['fIncluidoPor'] =  $oProjeto1->getIncluidoPor();
                    $_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    break;
            }
            $oProjeto = $oFachada->inicializarProjeto($_POST['fCodProjeto'],$_POST['fCodCliente'],$_POST['fCodServico'],$_POST['fDescricao'],$_POST['fDataInicio'],$_POST['fDataPrevisaoFim'],$_POST['fDataFim'],$_POST['fObservacao'],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1);

            $_SESSION['oProjeto'] = $oProjeto;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            $oValidate->add_number_field("Código", $oProjeto->getCodCliente(), "number", "y");
            $oValidate->add_number_field("Serviço", $oProjeto->getCodServico(), "number", "y");
            $oValidate->add_text_field("Descrição", $oProjeto->getDescricao(), "text", "y");
            $oValidate->add_date_field("Data Início", $oProjeto->getDataInicio(), "date", "y");
            $oValidate->add_date_field("Data Previsão Fim", $oProjeto->getDataPrevisaoFim(), "date", "y");
            $oValidate->add_text_field("Incluído Por", $oProjeto->getIncluidoPor(), "text", "y");
            if($sOP =='Alterar')
                $oValidate->add_text_field("Alterado Por", $oProjeto->getAlteradoPor(), "text", "y");

            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=Projeto.preparaFormulario&sOP=".$sOP."&nIdProjeto=".$_POST['fCodProjeto'];
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($nCodProjeto = $oFachada->inserirProjeto($oProjeto)){
                    $nTotal = count($_REQUEST['fValor']);

                    $i=0;
                    while($i <= $nTotal) {
                        $oProjetoPagamento = $oFachada->inicializarProjetoPagamento($_POST['fCodProjetoPagamento'][$i],$nCodProjeto,$_POST['fCodFormaPagamento'][$i],$_POST['fValor'][$i],$_POST['fDataPrevisao'][$i],$_POST['fDataEfetivacao'][$i],NULL,$_POST['fDescPagamento'][$i],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1);
                        $oFachada->inserirProjetoPagamento($oProjetoPagamento);
                        $i++;
                    }

                    unset($_SESSION['oProjeto']);
                    $_SESSION['sMsg'] = "Projeto inserido com sucesso!";
                    $sHeader = "?bErro=0&action=Projeto.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Projeto!";
                    $sHeader = "?bErro=1&action=Projeto.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Concluir":
                $oProjeto = $oFachada->recuperarUmProjeto($_REQUEST['nCodProjeto']);
                $oProjeto->setDataFimBanco(date('d/m/Y'));
                $oProjeto->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));

                if($oFachada->alterarProjeto($oProjeto)){
                    unset($_SESSION['oProjeto']);
                    $_SESSION['sMsg'] = "Projeto concluído com sucesso!";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel  o Projeto!";
                }
                $sHeader = "?bErro=0&action=Projeto.preparaLista";
                break;
            case "Alterar":
                if($oFachada->alterarProjeto($oProjeto)){
                    $i=0;
                    foreach($_POST['fCodProjetoPagamento'] as $nCodProjetoPagamento){
                        if(!$_POST['fDataEfetivacao'][$i]){
                            $oProjetoPagamento = $oFachada->inicializarProjetoPagamento($nCodProjetoPagamento,$_POST['fCodProjeto'],$_POST['fCodFormaPagamento'][$i],$_POST['fValor'][$i],$_POST['fDataPrevisao'][$i],$_POST['fDataEfetivacao'][$i],$_POST['fComprovante'][$i],$_POST['fDescPagamento'][$i],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1);
                            if($nCodProjetoPagamento !=""){
                                $oProjetoPagamento->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
                                $oFachada->alterarProjetoPagamento($oProjetoPagamento);
                            }else{
                                $oFachada->inserirProjetoPagamento($oProjetoPagamento);
                            }
                        }
                        $i++;
                    }

                    unset($_SESSION['oProjeto']);
                    $_SESSION['sMsg'] = "Projeto alterado com sucesso!";
                    $sHeader = "?bErro=0&action=Projeto.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Projeto!";
                    $sHeader = "?bErro=1&action=Projeto.preparaFormulario&sOP=".$sOP."&nIdProjeto=".$_POST['fCodProjeto']."";
                }
                break;
            case "Excluir":
                $bResultado = true;

                $vIdPaiProjeto = explode("____",$_REQUEST['fIdProjeto']);
                foreach($vIdPaiProjeto as $vIdFilhoProjeto){
                    $vIdProjeto = explode("||",$vIdFilhoProjeto);
                    foreach($vIdProjeto as $nIdProjeto){
                        $bResultado &= $oFachada->excluirProjeto($vIdProjeto[0],$vIdProjeto[1]);
                    }
                }

                if($bResultado){
                    $_SESSION['sMsg'] = "Projeto(s) exclu&iacute;do(s) com sucesso!";
                    $sHeader = "?bErro=0&action=Projeto.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Projeto!";
                    $sHeader = "?bErro=1&action=Projeto.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);
    }
}