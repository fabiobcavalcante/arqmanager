<?php
 class StatusCTR implements IControle{



 	public function preparaLista(){
 		$oFachada = new FachadaAcessoBD();
 		$voStatus = $oFachada->recuperarTodosStatus();
 		$_REQUEST['voStatus'] = $voStatus;
 		include_once("view/principal/status/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaAcessoBD();
 		$oStatus = false;
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdStatus = ($_POST['fIdStatus'][0]) ? $_POST['fIdStatus'][0] : $_GET['nIdStatus'];

 			if($nIdStatus){
 				$vIdStatus = explode("||",$nIdStatus);
 				$oStatus = $oFachada->recuperarUmStatus($vIdStatus[0]);
 			}
 		}

 		$_REQUEST['oStatus'] = ($_SESSION['oStatus']) ? $_SESSION['oStatus'] : $oStatus;
 		unset($_SESSION['oStatus']);

 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/status/detalhe.php");
 		else
 			include_once("view/principal/status/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaAcessoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oStatus = $oFachada->inicializarStatus($_POST['fCodStatus'],$_POST['fDescricao'],$_POST['fCor'],$_POST['fAtivo']);
 			$_SESSION['oStatus'] = $oStatus;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_text_field("Descricao", $oStatus->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oStatus->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Status.preparaFormulario&sOP=".$sOP."&nIdStatus=".$_POST['fCodStatus']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirStatus($oStatus)){
 					unset($_SESSION['oStatus']);
 					$_SESSION['sMsg'] = "Status inserido com sucesso!";
 					$sHeader = "?bErro=0&action=Status.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Status!";
 					$sHeader = "?bErro=1&action=Status.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarStatus($oStatus)){
 					unset($_SESSION['oStatus']);
 					$_SESSION['sMsg'] = "Status alterado com sucesso!";
 					$sHeader = "?bErro=0&action=Status.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Status!";
 					$sHeader = "?bErro=1&action=Status.preparaFormulario&sOP=".$sOP."&nIdStatus=".$_POST['fCodStatus']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiStatus = explode("____",$_REQUEST['fIdStatus']);
   				foreach($vIdPaiStatus as $vIdFilhoStatus){
  					$vIdStatus = explode("||",$vIdFilhoStatus);
 					foreach($vIdStatus as $nIdStatus){
  						$bResultado &= $oFachada->excluirStatus($vIdStatus[0],$vIdStatus[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Status exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=Status.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Status!";
 					$sHeader = "?bErro=1&action=Status.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }
 ?>
