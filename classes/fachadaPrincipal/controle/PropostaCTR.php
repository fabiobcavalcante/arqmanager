<?php /** @noinspection PhpUnused */

class PropostaCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $voProposta = $oFachada->recuperarTodosProposta($nIdEscritorio);
        $_REQUEST['voProposta'] = $voProposta;
        $_REQUEST['voStatus'] = $oFachada->recuperarTodosStatusProposta($nIdEscritorio);
        $_REQUEST['oPrincipal'] = $oFachada->recuperarUmRelatorioPrincipal($nIdEscritorio);

        if(isset($_REQUEST['nCodProposta'])){
            $_REQUEST['oPropostaImpressao'] =  $oFachada->recuperarUmProposta($_REQUEST['nCodProposta']);
        }
        include_once("view/principal/proposta/index.php");
        exit();

    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $oProposta = false;
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
            $nIdProposta = isset($_POST['fIdProposta']) ? $_POST['fIdProposta'][0] : $_GET['nIdProposta'];
            if($nIdProposta){
                $vIdProposta = explode("||",$nIdProposta);
                $oProposta = $oFachada->recuperarUmProposta($vIdProposta[0]);
                $_REQUEST['oProjeto'] = $oFachada->recuperarUmProjetoPorProposta($nIdProposta);
            }
        }

        $_REQUEST['oProposta'] = isset($_SESSION['oProposta']) ? $_SESSION['oProposta']: $oProposta;
        unset($_SESSION['oProposta']);
        $_REQUEST['voServico'] = $oFachada->recuperarTodosServico($nIdEscritorio);
        $_REQUEST['voStatus'] = $oFachada->recuperarTodosStatus();
        $_REQUEST['voCliente'] = $oFachada->recuperarTodosCliente($nIdEscritorio);

        if($_REQUEST['sOP'] == "Detalhar"){
            include_once("view/principal/proposta/detalhe.php");
        }else{
            if(isset($_REQUEST['oProjeto']) && isset($_REQUEST['sOP2']) && $_REQUEST['sOP2'] != 'Cadastrar'){
                $_REQUEST['sBotao'] = 'Voltar';
            }
            include_once("view/principal/proposta/insere_altera.php");
        }
        exit();
    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        if($sOP != "Excluir"){

            switch($sOP){
                case 'Cadastrar':
                    $_POST['fInseridoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    $_POST['fAlteradoPor'] = null;
                    $_POST['fAnoProposta'] = date('Y');

                    $_POST['fNumeroProposta'] = $oFachada->recuperarProximoProposta($nIdEscritorio,$_POST['fAnoProposta'])->numero_proposta;
                    if($nIdEscritorio ==2){
                        $_POST['fNumeroProjeto'] = $oFachada->recuperarProximoProjeto($nIdEscritorio,$_POST['fAnoProposta'])->numero_proposta;
                        $_POST['fNome'] = $_POST['fCodCliente'];
                        $_POST['fValorAvista'] = null;
                        $_POST['fCodStatus'] = 5;
                        $_POST['fVisitasIncluidas'] = null;
                        $_POST['fPrazoProposta'] = null;
                    }
                    break;
                case 'Alterar':
                case 'AlterarStatus':
                    if(is_array($_REQUEST['fCodProposta']))
                        $_REQUEST['fCodProposta'] = $_POST['fCodProposta'][0];
                    $oProposta1 = $oFachada->recuperarUmProposta($_REQUEST['fCodProposta']);
                    $_POST['fInseridoPor'] =  $oProposta1->getInseridoPor();
                    $_POST['fCodStatus'] =  $oProposta1->getCodStatus();
                    $_POST['fDataProposta'] = $_POST['fDataProposta'] ?? date('d/m/Y');
                    $_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    break;

            }
            if($sOP != "AlterarStatus"){
                $sFormaPagamento = ($_POST['fFormaPagamento']) ?? null;
                $oProposta = $oFachada->inicializarProposta($_POST['fCodProposta'],$_POST['fCodCliente'],$_POST['fCodServico'],$_POST['fNome'],$_POST['fDescricao'],$_POST['fIdentificacao'],$_POST['fValorProposta'],$_POST['fValorAvista'],$_POST['fValorParcelaAprazo'],$_POST['fCodStatus'],$_POST['fDataProposta'],$_POST['fVisitasIncluidas'],$_POST['fInseridoPor'],$_POST['fAlteradoPor'],$nIdEscritorio,$_POST['fNumeroProposta'],$_POST['fAnoProposta'],$_POST['fEntregaParcial'],$sFormaPagamento,$_POST['fPrazoProposta']);

                $_SESSION['oProposta'] = $oProposta;

                $oValidate = FabricaUtilitario::getUtilitario("Validate");
                $oValidate->check_4html = true;

                $oValidate->add_number_field("Serviço", $oProposta->getCodServico());
                $oValidate->add_number_field("Status", $oProposta->getCodStatus());
                $oValidate->add_text_field("Nome", $oProposta->getNome());
                $oValidate->add_text_field("Descrição", $oProposta->getDescricao());
                $oValidate->add_text_field("Identificação", $oProposta->getIdentificacao());
                $oValidate->add_date_field("Data Proposta", $oProposta->getDataProposta(), "date", "y");
                $oValidate->add_number_field("Valor Proposta", $oProposta->getValorProposta(), "decimal", "y",2);
//                $oValidate->add_text_field("Valor a prazo", $oProposta->getValorParcelaAprazo());
                $oValidate->add_number_field("Visitas", $oProposta->getVisitasIncluidas());
                $oValidate->add_text_field("Inserido Por", $oProposta->getInseridoPor());
                if($sOP == 'Alterar')
                    $oValidate->add_text_field("Alterado Por", $oProposta->getAlteradoPor());

                if (!$oValidate->validation()) {
                    $_SESSION['sMsg'] = $oValidate->create_msg();
                    $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=".$sOP."&nIdProposta=".$_POST['fCodProposta']."";
                    header("Location: ".$sHeader);
                    die();
                }
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($nCodProposta = $oFachada->inserirProposta($oProposta)){
                    if($nIdEscritorio ==2){
                        $oProjeto = $oFachada->inicializarProjeto($_POST['fCodProjeto'],$_POST['fCodCliente'],$_POST['fCodServico'],$nCodProposta,$_POST['fDescricao'],$_POST['fDataProposta'],null,null,$_POST['fMetragem'],$_POST['fObservacao'],$_POST['fInseridoPor'],$_POST['fAlteradoPor'],1,$_POST['fNumeroProjeto'],$_POST['fAnoProposta'],7);
                        $nCodProjeto = $oFachada->inserirProjeto($oProjeto);
                    }else{
                        $nCodProjeto = null;
                    }


                    foreach ($_POST['fIdServicoMicroServico'] as $nCodMicroServico) {
                        /**
                         * Vincular Microserviços
                         */
                        $sCampoDesc = "fDescricaoQuantidade$nCodMicroServico";
                        $sCampoQtde = "fQuantidade$nCodMicroServico";

                        /**
                         * Inserir Microserviços
                         */
                        // campo nDias
                        
                        $oPropostaMicroServico = $oFachada->inicializarPropostaMicroServico($nCodMicroServico,$nCodProposta,$nCodProjeto,$_POST[$sCampoQtde],$_POST[$sCampoDesc],null,null,null,null,null,null,$_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'),$_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));

                        if(!$oFachada->inserirPropostaMicroServico($oPropostaMicroServico))
                            $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoMicroServico INSERIR]!";
                    }

                    $voServicoEtapa = $oFachada->recuperarTodosServicoEtapaPorServico($_POST['fCodServico']);
                    foreach ($voServicoEtapa as $oServicoEtapa) {
                        $vServicoEtapa[] = $oServicoEtapa->getCodServicoEtapa();
                    }

                    foreach($vServicoEtapa as $nKey=>$nCodEtapa){
                        $oPropostaServicoEtapa = $oFachada->inicializarPropostaServicoEtapa($nCodEtapa,$nCodProposta,$nCodProjeto,$_POST['fDescricaoEtapa'][$nKey],$_POST['fPrazo'][$nKey],$_POST['fDataPrevisao'][$nKey],null,null);
                        $oFachada->inserirPropostaServicoEtapa($oPropostaServicoEtapa);

                    }

                    if ($_POST['fEntregaParcial'] == "Sim"){
                        foreach ($_POST['fCodFracionamento'] as $nKey=>$nCodFracionamento) {
                            $oPropostaFracionada = $oFachada->inicializarPropostaFracionada($_POST['fCodFracionamento'][$nKey],$nCodProposta,$_POST['fNumeroEntrega'][$nKey],$_POST['fDescricaoEntrega'][$nKey],$_POST['fPercentual'][$nKey],$_POST['fDataPrevista'][$nKey],null,$_POST['fValor'][$nKey],$_POST['fParcelas'][$nKey],$_POST['fIndiceCorrecao'][$nKey],null);

                            if (!$oFachada->inserirPropostaFracionada($oPropostaFracionada)){
                                $_SESSION['sMsg'] = "Erro ao inserir Etapas!";
                                $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=Alterar&nIdProposta=$nCodProposta";
                                header("Location: ".$sHeader);
                                die();
                            }
                        }
                    }

                    unset($_SESSION['oProposta']);
                    if($_REQUEST['fImprimirProposta'] == '1'){
                        $sHeader = "?bErro=0&action=Proposta.preparaLista&nCodProposta=".$nCodProposta;
                    }else{
                        $_SESSION['sMsg'] = "Orçamento inserido com sucesso!";
                        $sHeader = "?bErro=0&action=Proposta.preparaLista";
                    }

                } else {
                    $_SESSION['sMsg'] = "Não foi possível inserir o Orçamento!";
                    $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Alterar":
                //verificar projeto
                $oProjeto = $oFachada->recuperarUmProjetoPorProposta($oProposta->getCodProposta());
                if(!$oProjeto){
                    if($oFachada->alterarProposta($oProposta)){

                        /**
                         * Atualizar Micro Servico
                         */
                        $voServicoMicroServico = $oFachada->recuperarTodosServicoMicroServico($_POST['fCodServico'],$oProposta->getCodProposta());

                        $vMicroServico = [];
                        foreach ($voServicoMicroServico as $oServicoMicroServico) {
                            $vMicroServico[] = $oServicoMicroServico->getCodMicroservico();
                        }

                        /**
                         * Excluir Microserviços
                         */
                        if($vMicroServico){
                            foreach($vMicroServico as $nCodMicroServico){
                                if (!in_array($nCodMicroServico,$_POST['fIdServicoMicroServico']))
                                    $oFachada->excluirPropostaMicroServico($nCodMicroServico,$oProposta->getCodProposta());
                            }
                        }

                        foreach ($_POST['fIdServicoMicroServico'] as $nCodMicroServico) {
                            /**
                             * Vincular Microserviços
                             */
                            $sCampoDesc = "fDescricaoQuantidade$nCodMicroServico";
                            $sCampoQtde = "fQuantidade$nCodMicroServico";
                            $oPropostaMicroServico = $oFachada->recuperarUmPropostaMicroServico($nCodMicroServico,$oProposta->getCodProposta());

                            if ($oPropostaMicroServico){
                                $oPropostaMicroServico->setQuantidade($_POST[$sCampoQtde]);
                                $oPropostaMicroServico->setDescricaoQuantidade($_POST[$sCampoDesc]);
                                $oPropostaMicroServico->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));

                                if(!$oFachada->alterarPropostaMicroServico($oPropostaMicroServico))
                                    $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoMicroServico ALTERAR]!";

                            } else{
                                /**
                                 * Inserir Microserviços
                                 */
                                // campo nDias
                                $oPropostaMicroServico = $oFachada->inicializarPropostaMicroServico($nCodMicroServico,$oProposta->getCodProposta(),'',$_POST[$sCampoQtde],$_POST[$sCampoDesc],null,null,null,null,null,null,$_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'),$_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));

                                if(!$oFachada->inserirPropostaMicroServico($oPropostaMicroServico))
                                    $_SESSION['sMsg'] = "Não foi possível inserir o Projeto [ServicoMicroServico INSERIR]!";

                            }
                        }

                        if (isset($_POST['fIdServicoEtapa'])){
                            $voEtapa = $oFachada->recuperarTodosServicoEtapaPorServico($_POST['fCodServico']);
                            foreach ($voEtapa as $oEtapa) {
                                $vEtapa[] = $oEtapa->getCodServicoEtapa();
                            }
                            $oFachada->excluirPropostaServicoEtapaPorProposta($oProposta->getCodProposta());
                            foreach($vEtapa as $nKey=>$nCodEtapa){
                                if (in_array($nCodEtapa,$_POST['fIdServicoEtapa'])) {
                                    $oPropostaServicoEtapa = $oFachada->inicializarPropostaServicoEtapa($nCodEtapa,$oProposta->getCodProposta(),null,$_POST['fDescricaoEtapa'][$nKey],$_POST['fPrazo'][$nKey],$_POST['fDataPrevisao'][$nKey],null,null);
                                    $oFachada->inserirPropostaServicoEtapa($oPropostaServicoEtapa);
                                }
                            }
                        }

                        if ($_POST['fEntregaParcial'] == "Sim"){
                            $voPropostaFracionada = $oFachada->recuperarTodosPropostaFracionadaPorProposta($oProposta->getCodProposta());
                            $vCodFracionamento = [];

                            /**
                             * Cadastrar/Alterar Entrega parcial
                             */
                            foreach ($_POST['fCodFracionamento'] as $nKey=>$nCodFracionamento) {
                                $vCodFracionamento[] = $nCodFracionamento;
                                $oPropostaFracionada = $oFachada->inicializarPropostaFracionada($_POST['fCodFracionamento'][$nKey],$oProposta->getCodProposta(),$_POST['fNumeroEntrega'][$nKey],$_POST['fDescricaoEntrega'][$nKey],$_POST['fPercentual'][$nKey],$_POST['fDataPrevista'][$nKey],null,$_POST['fValor'][$nKey],$_POST['fParcelas'][$nKey],$_POST['fIndiceCorrecao'][$nKey],null);

                                if ($nCodFracionamento){
                                    if (!$oFachada->alterarPropostaFracionada($oPropostaFracionada)){
                                        $_SESSION['sMsg'] = "Erro ao alterar Etapas!";
                                        $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=Alterar&nIdProposta={$oProposta->getCodProposta()}";
                                        header("Location: ".$sHeader);
                                        die();
                                    }
                                } elseif (!empty($_POST['fNumeroEntrega'][$nKey]) && !empty($_POST['fDescricaoEntrega'][$nKey]) && !empty($_POST['fPercentual'][$nKey])){
                                    if (!$oFachada->inserirPropostaFracionada($oPropostaFracionada)){
                                        $_SESSION['sMsg'] = "Erro ao inserir Etapas!";
                                        $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=Alterar&nIdProposta={$oProposta->getCodProposta()}";
                                        header("Location: ".$sHeader);
                                        die();
                                    }
                                }
                            }

                            /**
                             * Excluir Entrega parcial
                             */
                            foreach ($voPropostaFracionada as $oPropostaFracionada) {
                                if (!in_array($oPropostaFracionada->getCodFracionamento(),$vCodFracionamento)){
                                    $oFachada->excluirPropostaFracionada($oPropostaFracionada->getCodFracionamento());
                                }
                            }
                        }

                        unset($_SESSION['oProposta']);
                        $_SESSION['sMsg'] = "Orçamento alterado com sucesso!";
                        $sHeader = "?bErro=0&action=Proposta.preparaLista";
                    } else {
                        $_SESSION['sMsg'] = "Não foi possível alterar o Orçamento!";
                        $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=".$sOP."&nIdProposta=".$_POST['fCodProposta'];
                    }
                }else{
                    $_SESSION['sMsg'] = "Não é possível alterar Orçamento de contratos existentes!";
                    $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=".$sOP."&nIdProposta=".$_POST['fCodProposta'];
                }
                break;
            case "AlterarStatus":

                $oProposta1->setCodStatus($_GET['nStatus']);
                $oProposta1->setAlteradoPor($_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s'));
                if($oFachada->alterarProposta($oProposta1)){
                    unset($_SESSION['oProposta']);
                    $_SESSION['sMsg'] = "Status de proposta alterado com sucesso!";
                    $sHeader = "?bErro=0&action=Proposta.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "Não foi possível alterar o status do Orçamento!";
                    $sHeader = "?bErro=1&action=Proposta.preparaFormulario&sOP=".$sOP."&nIdProposta=".$_POST['fCodProposta']."";
                }
                break;
            case "Excluir":
                $bResultado = true;

                $vIdPaiProposta = explode("____",$_REQUEST['fIdProposta']);
                foreach($vIdPaiProposta as $vIdFilhoProposta){
                    $vIdProposta = explode("||",$vIdFilhoProposta);
                    foreach($vIdProposta as $nIdProposta){
                        $oProposta = $oFachada->recuperarUmProposta($nIdProposta);
                        $oProposta->setCodStatus(4);//cancelado
                        $bResultado &= $oFachada->alterarProposta($oProposta);
                    }
                }
                if($bResultado){
                    $_SESSION['sMsg'] = "Orçamento(s) cancelado(s) com sucesso!";
                    $sHeader = "?bErro=0&action=Proposta.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "Não foi possível cancelar o(s)  Orçamento(s)!";
                    $sHeader = "?bErro=1&action=Proposta.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);

    }

}