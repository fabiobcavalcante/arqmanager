<?php
class DocumentoEscritorioCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
				$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $voDocumentoEscritorio = $oFachada->recuperarTodosDocumentoEscritorio($nIdEscritorio);
        $_REQUEST['voDocumentoEscritorio'] = $voDocumento;

        include_once("view/principal/documento_escritorio/index.php");
        exit();

    }

      public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $oDocumentoEscritorio = false;

        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
            $nIdDocumentoEscritorio = ($_POST['fIdDocumentoEscritorio'][0]) ? $_POST['fIdDocumentoEscritorio'][0] : $_GET['nIdDocumentoEscritorio'];

            if($nIdDocumentoEscritorio){
                $vIdDocumentoEscritorio = explode("||",$nIdDocumentoEscritorio);
                $oDocumentoEscritorio = $oFachada->recuperarUmDocumentoEscritorio($vIdDocumentoEscritorio[0],$vIdDocumentoEscritorio[1]);
            }
        }

        $_REQUEST['oDocumentoEscritorio'] = ($_SESSION['oDocumentoEscritorio']) ? $_SESSION['oDocumentoEscritorio'] : $oDocumentoEscritorio;
        unset($_SESSION['oDocumentoEscritorio']);



        if($_REQUEST['sOP'] == "Detalhar")
            include_once("view/principal/documento_escritorio/detalhe.php");
        else
            include_once("view/principal/documento_escritorio/insere_altera.php");

        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

        if($sOP != "Excluir"){
            $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
            $oDocumentoEscritorio = $oFachada->inicializarDocumentoEscritorio($_POST['fCodDocumento'],$_POST['fIdEscritorio'],$_POST['fTemplate']);
            $_SESSION['oDocumentoEscritorio'] = $oDocumentoEscritorio;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            //$oValidate->add_text_field("Titulo", $oDocumento->getTitulo(), "text", "y");
            //$oValidate->add_text_field("Conteudo", $oDocumento->getConteudo(), "text", "y");
            //$oValidate->add_text_field("Ativo", $oDocumento->getAtivo(), "text", "y");


            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=DocumentoEscritorio.preparaFormulario&sOP={$sOP}&nIdDocumento={$_POST['fCodDocumento']}&nIdEscritorio={$_POST['fIdEscritorio']}";
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirDocumentoEscritorio($oDocumentoEscritorio)){
                    unset($_SESSION['oDocumentoEscritorio']);
                    $_SESSION['sMsg'] = "Template inserido com sucesso!";
                    $sHeader = "?bErro=0&action=DocumentoEscritorio.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Template!";
                    $sHeader = "?bErro=1&action=DocumentoEscritorio.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Alterar":
                if($oFachada->alterarDocumento($oDocumento)){
                    unset($_SESSION['oDocumento']);
                    $_SESSION['sMsg'] = "Template alterado com sucesso!";
                    $sHeader = "?bErro=0&action=DocumentoEscritorio.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Template!";
                    $sHeader = "?bErro=1&action=DocumentoEscritorio.preparaFormulario&sOP=".$sOP."&nIdDocumento=".$_POST['fCodDocumento']."";
                }
                break;
            case "Excluir":
                $bResultado = true;

                $vIdPaiDocumentoEscritorio = explode("____",$_REQUEST['fIdDocumentoEscritorio']);
                foreach($vIdPaiDocumentoEscritorio as $vIdFilhoDocumentoEscritorio){
                    $vIdDocumentoEscritorio = explode("||",$vIdFilhoDocumentoEscritorio);
                    foreach($vIdDocumentoEscritorio as $nIdDocumentoEscritorio){
                        $bResultado &= $oFachada->excluirDocumentoEscritorio($vIdDocumento[0],$vIdDocumento[1]);
                    }
                }

                if($bResultado){
                    $_SESSION['sMsg'] = "Template(s) exclu&iacute;do(s) com sucesso!";
                    $sHeader = "?bErro=0&action=DocumentoEscritorio.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Template(s)!";
                    $sHeader = "?bErro=1&action=DocumentoEscritorio.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);

    }

}
