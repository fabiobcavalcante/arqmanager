<?php
 class ContaMovimentacaoCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();

    $nDias=30;
    $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
    switch($_REQUEST['sOP']){
      case "DespesasEfetivadas":

        $voContaMovimentacao = $oFachada->recuperarTodosContaMovimentacaoPorTipoPorIntervalo('S',$nDias,$nIdEscritorio);
        $_REQUEST['sDescricao'] = "Despesas Efetivadas nos últimos ".$nDias." Dias";
      break;
      case "EntradasEfetivadas":
        $voContaMovimentacao = $oFachada->recuperarTodosContaMovimentacaoPorTipoPorIntervalo('E',$nDias,$nIdEscritorio);
        $_REQUEST['sDescricao'] = "Entradas Efetivadas nos últimos ".$nDias." Dias";
      break;
      default:
        $voContaMovimentacao = $oFachada->recuperarTodosContaMovimentacao();
        $_REQUEST['sDescricao'] = "Movimentação";
      break;

    }
 	 	$_REQUEST['voContaMovimentacao'] = $voContaMovimentacao;
 		include_once("view/principal/conta_movimentacao/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$oContaMovimentacao = false;
    $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdContaMovimentacao = ($_POST['fIdContaMovimentacao'][0]) ? $_POST['fIdContaMovimentacao'][0] : $_GET['nIdContaMovimentacao'];

 			if($nIdContaMovimentacao){
 				$vIdContaMovimentacao = explode("||",$nIdContaMovimentacao);
 				$oContaMovimentacao = $oFachada->recuperarUmContaMovimentacao($vIdContaMovimentacao[0]);
 			}
 		}

 		$_REQUEST['oContaMovimentacao'] = ($_SESSION['oContaMovimentacao']) ? $_SESSION['oContaMovimentacao'] : $oContaMovimentacao;
 		unset($_SESSION['oContaMovimentacao']);

 		$_REQUEST['voContaBancaria'] = $oFachada->recuperarTodosContaBancaria($nIdEscritorio);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/conta_movimentacao/detalhe.php");
 		else
 			include_once("view/principal/conta_movimentacao/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oContaMovimentacao = $oFachada->inicializarContaMovimentacao($_POST['fCodContaMovimentacao'],$_POST['fCodContaBancaria'],$_POST['fValorInicial'],$_POST['fValorDespesa'],$_POST['fValorReceita'],$_POST['fDataMovimentacao'],$_POST['fAtivo']);
 			$_SESSION['oContaMovimentacao'] = $oContaMovimentacao;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("CodContaBancaria", $oContaMovimentacao->getCodContaBancaria(), "number", "y");
			//$oValidate->add_number_field("ValorInicial", $oContaMovimentacao->getValorInicial(), "number", "y");
			//$oValidate->add_number_field("ValorDespesa", $oContaMovimentacao->getValorDespesa(), "number", "y");
			//$oValidate->add_number_field("ValorReceita", $oContaMovimentacao->getValorReceita(), "number", "y");
			//$oValidate->add_date_field("DataMovimentacao", $oContaMovimentacao->getDataMovimentacao(), "date", "y");
			//$oValidate->add_text_field("InseridoPor", $oContaMovimentacao->getInseridoPor(), "text", "y");
			//$oValidate->add_text_field("AlteradoPor", $oContaMovimentacao->getAlteradoPor(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oContaMovimentacao->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=ContaMovimentacao.preparaFormulario&sOP=".$sOP."&nIdContaMovimentacao=".$_POST['fCodContaMovimentacao']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirContaMovimentacao($oContaMovimentacao)){
 					unset($_SESSION['oContaMovimentacao']);
 					$_SESSION['sMsg'] = "Movimentação inserida com sucesso!";
 					$sHeader = "?bErro=0&action=ContaMovimentacao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a Movimentação!";
 					$sHeader = "?bErro=1&action=ContaMovimentacao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarContaMovimentacao($oContaMovimentacao)){
 					unset($_SESSION['oContaMovimentacao']);
 					$_SESSION['sMsg'] = "Movimentação alterado com sucesso!";
 					$sHeader = "?bErro=0&action=ContaMovimentacao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Movimentação!";
 					$sHeader = "?bErro=1&action=ContaMovimentacao.preparaFormulario&sOP=".$sOP."&nIdContaMovimentacao=".$_POST['fCodContaMovimentacao']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiContaMovimentacao = explode("____",$_REQUEST['fIdContaMovimentacao']);
   				foreach($vIdPaiContaMovimentacao as $vIdFilhoContaMovimentacao){
  					$vIdContaMovimentacao = explode("||",$vIdFilhoContaMovimentacao);
 					foreach($vIdContaMovimentacao as $nIdContaMovimentacao){
  						$bResultado &= $oFachada->excluirContaMovimentacao($vIdContaMovimentacao[0],$vIdContaMovimentacao[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Movimentação(s) exclu&iacute;da(s) com sucesso!";
 					$sHeader = "?bErro=0&action=ContaMovimentacao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a(s) Movimentação4!";
 					$sHeader = "?bErro=1&action=ContaMovimentacao.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
