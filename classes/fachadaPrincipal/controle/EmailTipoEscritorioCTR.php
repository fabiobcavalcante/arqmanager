<?php
 class EmailTipoEscritorioCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
 		$voEmailTipoEscritorio = $oFachada->recuperarTodosEmailTipoEscritorio();
 		$_REQUEST['voEmailTipoEscritorio'] = $voEmailTipoEscritorio;

 		include_once("view/principal/email_tipo_escritorio/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$oEmailTipoEscritorio = false;
    $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdEmailTipoEscritorio = ($_POST['fIdEmailTipoEscritorio'][0]) ? $_POST['fIdEmailTipoEscritorio'][0] : $_GET['nIdEmailTipoEscritorio'];

 			if($nIdEmailTipoEscritorio){
 				$vIdEmailTipoEscritorio = explode("||",$nIdEmailTipoEscritorio);
 				$oEmailTipoEscritorio = $oFachada->recuperarUmEmailTipoEscritorio($vIdEmailTipoEscritorio[0],$vIdEmailTipoEscritorio[1]);
 			}
 		}

 		$_REQUEST['oEmailTipoEscritorio'] = ($_SESSION['oEmailTipoEscritorio']) ? $_SESSION['oEmailTipoEscritorio'] : $oEmailTipoEscritorio;
 		unset($_SESSION['oEmailTipoEscritorio']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/email_tipo_escritorio/detalhe.php");
 		else
 			include_once("view/principal/email_tipo_escritorio/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 			$oEmailTipoEscritorio = $oFachada->inicializarEmailTipoEscritorio($_POST['fIdEmailTipo'],$nIdEscritorio,$_POST['fTemplate']);
 			$_SESSION['oEmailTipoEscritorio'] = $oEmailTipoEscritorio;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

// 			$oValidate->add_text_field("NomeFantasia", $oEmailTipoEscritorio->getTitulo(), "text", "y");
//			$oValidate->add_text_field("RazaoSocial", $oEmailTipoEscritorio->getTemplate(), "text", "y");

 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=EmailTipoEscritorio.preparaFormulario&sOP=".$sOP."&nIdEmailTipo=".$_POST['fIdEmailTipo']."&nIdEscritorio=".$_POST['fIdEscritorio'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirEmailTipoEscritorio($oEmailTipoEscritorio)){
 					unset($_SESSION['oEmailTipoEscritorio']);
 					$_SESSION['sMsg'] = " Tipo de Email inserido com sucesso!";
 					$sHeader = "?bErro=0&action=EmailTipoEscritorio.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o  Tipo de Email!";
 					$sHeader = "?bErro=1&action=EmailTipoEscritorio.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarEmailTipoEscritorio($oEmailTipoEscritorio)){
 					unset($_SESSION['oEmailTipoEscritorio']);
 					$_SESSION['sMsg'] = "Tipo de Email alterado com sucesso!";
 					$sHeader = "?bErro=0&action=EmailTipoEscritorio.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Tipo de Email!";
 					$sHeader = "?bErro=1&action=EmailTipoEscritorio.preparaFormulario&sOP=".$sOP."&nIdEmailTipoEscritorio=".$_POST['fIdEmailTipoEscritorio']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiEmailTipoEscritorio = explode("____",$_REQUEST['fIdEmailTipoEscritorio']);
   				foreach($vIdPaiEmailTipoEscritorio as $vIdFilhoEmailTipoEscritorio){
  					$vIdEmailTipoEscritorio = explode("||",$vIdFilhoEmailTipoEscritorio);
 					foreach($vIdEmailTipoEscritorio as $nIdEmailTipoEscritorio){
  						$bResultado &= $oFachada->excluirEmailTipoEscritorio($vIdEmailTipoEscritorio[0],$vIdEmailTipoEscritorio[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = " Tipo(s) de Email(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=EmailTipoEscritorio.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s)  Tipo(s) de Email!";
 					$sHeader = "?bErro=1&action=EmailTipoEscritorio.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
