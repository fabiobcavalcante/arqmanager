<?php
 class ReuniaoCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
		$nIdEscritorio = ($_SESSION['oEscritorio']) ? $_SESSION['oEscritorio']->getIdEscritorio() : "";
 		$voReuniao = $oFachada->recuperarTodosReuniao($nIdEscritorio);
		$_REQUEST['oPrincipal'] = $oFachada->recuperarUmRelatorioPrincipal($nIdEscritorio);
 		$_REQUEST['voReuniao'] = $voReuniao;

 		include_once("view/principal/reuniao/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();
    $nIdEscritorio = ($_SESSION['oEscritorio']) ? $_SESSION['oEscritorio']->getIdEscritorio() : "";

 		$oReuniao = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" || $_REQUEST['sOP'] =='AceiteCliente'){
			if($_REQUEST['sOP'] =='AceiteCliente'){
				$nIdReuniao = base64_decode($_GET['nIdReuniao']);
			}else{
				$nIdReuniao = ($_POST['fIdReuniao'][0]) ? $_POST['fIdReuniao'][0] : $_GET['nIdReuniao'];
			}

 			if($nIdReuniao){
 				$vIdReuniao = explode("||",$nIdReuniao);
 				$oReuniao = $oFachada->recuperarUmReuniao($vIdReuniao[0]);
 			}
 		}

 		$_REQUEST['oReuniao'] = ($_SESSION['oReuniao']) ? $_SESSION['oReuniao'] : $oReuniao;
 		unset($_SESSION['oReuniao']);



		switch ($_REQUEST['sOP']) {
			case 'Detalhar':
				$_REQUEST['vProjeto'] = $oFachada->recuperarUmProjetoViewPorProjeto($oReuniao->getCodProjeto());
				include_once("view/principal/reuniao/detalhe.php");
				break;
			case 'AceiteCliente':
				$_REQUEST['vProjeto'] = $oFachada->recuperarUmProjetoViewPorProjeto($oReuniao->getCodProjeto());
        $_REQUEST['oEscritorio'] = $oFachada->recuperarUmEscritorio($oReuniao->getIdEscritorio());
				include_once("view/principal/reuniao/confirmar.php");
				break;
			case 'Cadastrar':
			case 'Alterar':
				$_REQUEST['voProjeto'] = $oFachada->recuperarTodosVProjeto($nIdEscritorio);
				$_REQUEST['voCliente'] = $oFachada->recuperarTodosCliente($nIdEscritorio);
				include_once("view/principal/reuniao/insere_altera.php");
				break;
		}
 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

 		if($sOP != "Excluir" && $sOP != "AceiteCliente" && $sOP != "EnviarEmail"){

			if($sOP === 'Cadastrar'){
				$_POST['fInseridoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
				$_POST['fDeAcordo'] = NULL;
				$_POST['fObsCliente'] = NULL;
				$_POST['fDataCliente'] = NULL;
			}else{
				$oReuniao1 = $oFachada->recuperarUmReuniao($_POST['fCodReuniao']);
                $_POST['fInseridoPor'] = $oReuniao1->getInseridoPor();
				$_POST['fDeAcordo'] = ($oReuniao1->getDeAcordo()) ? :NULL;
				$_POST['fObsCliente'] = ($oReuniao1->getObsCliente()) ? :NULL;
				$_POST['fDataCliente'] = ($oReuniao1->getDataClienteFormatado()) ? :NULL;

			}

 			$oReuniao = $oFachada->inicializarReuniao($_POST['fCodReuniao'],$_POST['fCodProjeto'],$_POST['fDataReuniao'],$_POST['fHoraReuniao'],$_POST['fResponsavel'],$_POST['fParticipantes'],$_POST['fAssunto'],$_POST['fLocal'],$_POST['fDecisao'],$_POST['fObservacao'],$_POST['fInseridoPor'],$_POST['fCodCliente'],$_POST['fDeAcordo'],$_POST['fObsCliente'],$_POST['fDataCliente'],$nIdEscritorio);
 			$_SESSION['oReuniao'] = $oReuniao;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("CodProjeto", $oReuniao->getCodProjeto(), "number", "y");
			$oValidate->add_text_field("Data", $oReuniao->getDataReuniao(), "text", "y");
			$oValidate->add_text_field("Hora", $oReuniao->getHoraReuniao(), "text", "y");
			$oValidate->add_text_field("Responsável", $oReuniao->getResponsavel(), "text", "y");
			$oValidate->add_text_field("Participantes", $oReuniao->getParticipantes(), "text", "y");
			$oValidate->add_text_field("Assunto", $oReuniao->getAssunto(), "text", "y");
			$oValidate->add_text_field("Local", $oReuniao->getLocal(), "text", "y");
			$oValidate->add_text_field("Decisões", $oReuniao->getDecisao(), "text", "y");
			//$oValidate->add_text_field("Observacao", $oReuniao->getObservacao(), "text", "y");
			$oValidate->add_text_field("InseridoPor", $oReuniao->getInseridoPor(), "text", "y");
			$oValidate->add_number_field("Cliente", $oReuniao->getCodCliente(), "number", "y");
			//$oValidate->add_text_field("DeAcordo", $oReuniao->getDeAcordo(), "text", "y");
			//$oValidate->add_text_field("ObsCliente", $oReuniao->getObsCliente(), "text", "y");
			//$oValidate->add_date_field("DataCliente", $oReuniao->getDataCliente(), "date", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Reuniao.preparaFormulario&sOP=".$sOP."&nIdReuniao=".$_POST['fCodReuniao']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":

 				if($oFachada->inserirReuniao($oReuniao)){
 					unset($_SESSION['oReuniao']);
 					$_SESSION['sMsg'] = "Ata inserida com sucesso!";
 					$sHeader = "?bErro=0&action=Reuniao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a Ata de Reunião!";
 					$sHeader = "?bErro=1&action=Reuniao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarReuniao($oReuniao)){
 					unset($_SESSION['oReuniao']);
 					$_SESSION['sMsg'] = "Ata alterada com sucesso!";
 					$sHeader = "?bErro=0&action=Reuniao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Ata!";
 					$sHeader = "?bErro=1&action=Reuniao.preparaFormulario&sOP=".$sOP."&nIdReuniao=".$_POST['fCodReuniao']."";
 				}
 			break;
 			case "AceiteCliente":
        $oReuniao = $oFachada->recuperarUmReuniao($_POST['fCodReuniao']);
        if($_POST['fObsCliente'])
          $oReuniao->setObsCliente($_POST['fObsCliente']);
					if($_POST['fDeAcordo'] ==='Concordo')
						$sDeAcordo='S';
					else {
						$sDeAcordo='N';
					}
        $oReuniao->setDeAcordo($sDeAcordo);
        $oReuniao->setDataCliente(date('Y-m-d'));

 				if($oFachada->alterarReuniao($oReuniao)){
 					unset($_SESSION['oReuniao']);
          if($sDeAcordo =='S'){
            $_SESSION['sMsg'] = "Ata de reunião confirmada com sucesso!";
            $bErro=0;
          }else {
            $_SESSION['sMsg'] = "Ata de reunião alterada porém não confirmada por divergências de entendimento!";
            $bErro=1;
          }
 					$sHeader = "?bErro={$bErro}&action=Reuniao.preparaFormulario&sOP=AceiteCliente&nIdReuniao=".base64_encode($_POST['fCodReuniao']);
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel confirmar a Ata de Reunião!";
          $sHeader = "?bErro=1&action=Reuniao.preparaFormulario&sOP=AceiteCliente&nIdReuniao=".base64_encode($_POST['fCodReuniao']);
 				}
 			break;
 			case "EnviarEmail":
      if($_SESSION['oEscritorio']->getEnvioEmail()=='S'){
          $nIdReuniao = base64_decode($_GET['nIdReuniao']);
          $oReuniao = $oFachada->recuperarUmReuniao($nIdReuniao);
          $oProjeto = $oReuniao->getProjeto();
          $oProposta = $oProjeto->getProposta();
          $oCliente = $oProjeto->getCliente();

          //preparando template para envio de email
          $nCodTipoEmail=10; // Envio de confirmação de ata de reunião
          $oEmailModelo = $oFachada->recuperarUmEmailTipo($nCodTipoEmail);

          if($oEmailTipoEscritorio = $oFachada->recuperarUmEmailTipoEscritorio($nCodTipoEmail,$_SESSION['oEscritorio']->getIdEscritorio())){
            if($oEmailTipoEscritorio->getTemplate()){
              $sConteudo = $oEmailTipoEscritorio->getTemplate();
            }else{
              $sConteudo = $oEmailModelo->getTemplate();
            }

            $sCabecalho = $_SESSION['oEscritorio']->getCabecalhoEmail();
            $sSite = ($_SESSION['oEscritorio']->getSite()) ? $_SESSION['oEscritorio']->getSite() : "http://www.arqmanager.com.br";
            $sCabecalho = str_replace("#SITE#","{$sSite} ",$sCabecalho);
            $sCabecalho = str_replace("#LOGO#","{$_SESSION['oEscritorio']->getLogomarca()} ",$sCabecalho);
            // montando o rodape do email
            $sRodape = $_SESSION['oEscritorio']->getRodapeEmail();
            $sRodape = str_replace("#ESCRITORIO#","{$_SESSION['oEscritorio']->getNomeFantasia()} ",$sRodape);
            $sRodape = str_replace("#ENDERECO_REDUZIDO#","{$_SESSION['oEscritorio']->getEnderecoReduzido()} ",$sRodape);

              $sConteudo = str_replace("#PROJETO#","{$oCliente->getNome()} ",$sConteudo);
              $sConteudo = str_replace("#DATA#","{$oReuniao->getDataReuniaoFormatado()} - {$oReuniao->getHoraReuniao()} ",$sConteudo);
              $sConteudo = str_replace("#RESPONSAVEL#","{$oReuniao->getResponsavel()} ",$sConteudo);
              $sConteudo = str_replace("#PARTICIPANTES#","{$oReuniao->getParticipantes()} ",$sConteudo);
              $sConteudo = str_replace("#LOCAL#","{$oReuniao->getLocal()} ",$sConteudo);
              $sConteudo = str_replace("#ASSUNTO#","{$oReuniao->getAssunto()} ",$sConteudo);
            //  $sConteudo = str_replace("#DECISAO#","{$oReuniao->getDecisao()} ",$sConteudo);
            //  $sConteudo = str_replace("#OBSERVACAO#","{$oReuniao->getObservacao()} ",$sConteudo);
              $sConteudo = str_replace("#IDREUNIAO#","{$_GET['nIdReuniao']} ",$sConteudo);
              $sConteudo = htmlspecialchars($sConteudo);
              // Envio de EMAIL
              $oMailer = new Mailer();

              // $sEmail = $oCliente->getEmail();
               $sEmail = "bc_fabio@hotmail.com";
               // $sEmailOculto = "bc_fabio@hotmail.com";
               // $mail->addBCC("{$sEmailOculto}");
              $sAssunto = "[{$_SESSION['oEscritorio']->getNomeFantasia()}] {$oEmailModelo->getTitulo()}";
              if($oMailer->enviar("{$oCliente->getNome()}", "{$sEmail}", "{$sAssunto}", "template", ["sCabecalho"=>$sCabecalho,"sConteudo"=>$sConteudo,"sRodape"=>$sRodape])){
                  // Inserir na Tabela de Email
                  $sDescricaoEmail = $oProposta->getIdentificacao() . " - " . $oReuniao->getAssunto();
                  $dDataHora = date('Y-m-d H:i:s');
                  $oEmail = $oFachada->inicializarEmail(null,$oCliente->getCodCliente(),$nCodTipoEmail,$sDescricaoEmail,$dDataHora);
                  $oFachada->inserirEmail($oEmail);
                  $_SESSION['sMsg'] = "Ata de reunião enviada para o email {$oCliente->getEmail()}";
              }else{
                  $_SESSION['sMsg'] = "Não foi possível enviar Ata de reunião para o email {$oCliente->getEmail()}";
              }

          }
        }else{
          $_SESSION['sMsg'] = "Escritório não habilitado para este envio de email.";
        }
          $sHeader = "?bErro=0&action=Reuniao.preparaLista";
        header("Location: ".$sHeader);

 			break;
 			case "Excluir":
 				$bResultado = true;
        
 				$vIdPaiReuniao = explode("____",$_REQUEST['fIdReuniao']);
        $oReuniao = $oFachada->recuperarUmReuniao($vIdPaiReuniao[0]);
        if($oReuniao-getDeAcordo()){
          $_SESSION['sMsg'] = "Atas confirmadas pelo cliente não podem ser excluídas!";
 					$sHeader = "?bErro=1&action=Reuniao.preparaLista";
        }else{
          foreach($vIdPaiReuniao as $vIdFilhoReuniao){
  					$vIdReuniao = explode("||",$vIdFilhoReuniao);
 					foreach($vIdReuniao as $nIdReuniao){
  						$bResultado &= $oFachada->excluirReuniao($vIdReuniao[0],$vIdReuniao[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Ata de Reunião(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=Reuniao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Ata de Reunião!";
 					$sHeader = "?bErro=1&action=Reuniao.preparaLista";
 				}
        
        }
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }
 ?>
