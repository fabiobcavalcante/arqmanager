<?php
 class DespesaReceitaTipoCTR implements IControle{
 
 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
 		$voDespesaReceitaTipo = $oFachada->recuperarTodosDespesaReceitaTipo();
 		$_REQUEST['voDespesaReceitaTipo'] = $voDespesaReceitaTipo;
 		include_once("view/principal/despesa_receita_tipo/index.php");
 		exit();
 	}
 	public function preparaListaAjax(){
 		$oFachada = new FachadaPrincipalBD();
 		$voDespesaReceitaTipo = $oFachada->recuperarTodosDespesaReceitaTipoPorTipo($_GET['fTipo']);
 		$_REQUEST['voDespesaReceitaTipo'] = $voDespesaReceitaTipo;
 		include_once("view/principal/despesa_receita_tipo/select_ajax.php");
 		exit();
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();
 
 		$oDespesaReceitaTipo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdDespesaReceitaTipo = ($_POST['fIdDespesaReceitaTipo'][0]) ? $_POST['fIdDespesaReceitaTipo'][0] : $_GET['nIdDespesaReceitaTipo'];
 	
 			if($nIdDespesaReceitaTipo){
 				$vIdDespesaReceitaTipo = explode("||",$nIdDespesaReceitaTipo);
 				$oDespesaReceitaTipo = $oFachada->recuperarUmDespesaReceitaTipo($vIdDespesaReceitaTipo[0]);
 			}
 		}
 		
 		$_REQUEST['oDespesaReceitaTipo'] = ($_SESSION['oDespesaReceitaTipo']) ? $_SESSION['oDespesaReceitaTipo'] : $oDespesaReceitaTipo;
 		unset($_SESSION['oDespesaReceitaTipo']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/despesa_receita_tipo/detalhe.php");
 		else
 			include_once("view/principal/despesa_receita_tipo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
             switch($sOP){
                case 'Cadastrar':
                       $_POST['fIncluidoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                break;
                case 'Alterar':
                    $oDespesaReceitaTipo1 = $oFachada->recuperarUmDespesaReceitaTipo($_POST['fCodDespesaReceitaTipo']);
                    $_POST['fIncluidoPor'] =  $oDespesaReceitaTipo1->getIncluidoPor();
                    $_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                break;
                
            }
            
 			$oDespesaReceitaTipo = $oFachada->inicializarDespesaReceitaTipo($_POST['fCodDespesaReceitaTipo'],$_POST['fAbreviacao'],$_POST['fDescricao'],$_POST['fEntradaSaida'],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1);
 			$_SESSION['oDespesaReceitaTipo'] = $oDespesaReceitaTipo;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			//$oValidate->add_text_field("Abreviacao", $oDespesaReceitaTipo->getAbreviacao(), "text", "y");
			//$oValidate->add_text_field("Descricao", $oDespesaReceitaTipo->getDescricao(), "text", "y");
			//$oValidate->add_text_field("IncluidoPor", $oDespesaReceitaTipo->getIncluidoPor(), "text", "y");
			//$oValidate->add_text_field("AlteradoPor", $oDespesaReceitaTipo->getAlteradoPor(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oDespesaReceitaTipo->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=DespesaReceitaTipo.preparaFormulario&sOP=".$sOP."&nIdDespesaReceitaTipo=".$_POST['fCodDespesaReceitaTipo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirDespesaReceitaTipo($oDespesaReceitaTipo)){
 					unset($_SESSION['oDespesaReceitaTipo']);
 					$_SESSION['sMsg'] = "Tipo de Despesa X Receita inserido com sucesso!";
 					$sHeader = "?bErro=0&action=DespesaReceitaTipo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Tipo de Despesa X Receita!";
 					$sHeader = "?bErro=1&action=DespesaReceitaTipo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarDespesaReceitaTipo($oDespesaReceitaTipo)){
 					unset($_SESSION['oDespesaReceitaTipo']);
 					$_SESSION['sMsg'] = "Tipo de Despesa X Receita alterado com sucesso!";
 					$sHeader = "?bErro=0&action=DespesaReceitaTipo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Tipo de Despesa X Receita!";
 					$sHeader = "?bErro=1&action=DespesaReceitaTipo.preparaFormulario&sOP=".$sOP."&nIdDespesaReceitaTipo=".$_POST['fCodDespesaReceitaTipo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiDespesaReceitaTipo = explode("____",$_REQUEST['fIdDespesaReceitaTipo']);
   				foreach($vIdPaiDespesaReceitaTipo as $vIdFilhoDespesaReceitaTipo){
  					$vIdDespesaReceitaTipo = explode("||",$vIdFilhoDespesaReceitaTipo);
 					foreach($vIdDespesaReceitaTipo as $nIdDespesaReceitaTipo){
  						$bResultado &= $oFachada->excluirDespesaReceitaTipo($vIdDespesaReceitaTipo[0],$vIdDespesaReceitaTipo[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Tipo de Despesa X Receita(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=DespesaReceitaTipo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Tipo de Despesa X Receita!";
 					$sHeader = "?bErro=1&action=DespesaReceitaTipo.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>