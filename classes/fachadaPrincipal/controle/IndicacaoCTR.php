<?php
 class IndicacaoCTR implements IControle{



 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
		if($_REQUEST['sOP']=='Ranking'){
			$voIndicacao = $oFachada->recuperarTodosIndicacaoRanking($nIdEscritorio);
			$_REQUEST['voIndicacao'] = $voIndicacao;
			include_once("view/principal/indicacao/ranking.php");
		}else{
			$voIndicacao = $oFachada->recuperarTodosIndicacao($nIdEscritorio);
			$_REQUEST['voIndicacao'] = $voIndicacao;
			include_once("view/principal/indicacao/index.php");
		}
 		exit();

 	}



	public function preparaListaAjax(){
		$oFachada = new FachadaPrincipalBD();
      $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
		$voIndicacao = $oFachada->recuperarTodosIndicacao($nIdEscritorio);
		$_REQUEST['voIndicacao'] = $voIndicacao;
		include_once("view/principal/cliente/indicados.php");
		exit();

	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$oIndicacao = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdIndicacao = ($_REQUEST['nCodIndicacao'])?: $_GET['nIdIndicacao'];

 			if($nIdIndicacao){
 				$oIndicacao = $oFachada->recuperarUmIndicacao($nIdIndicacao);
 			}
 		}

 		$_REQUEST['oIndicacao'] = ($_SESSION['oIndicacao']) ? $_SESSION['oIndicacao'] : $oIndicacao;
 		unset($_SESSION['oIndicacao']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/indicacao/detalhe.php");
 		else
 			include_once("view/principal/indicacao/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 		if($sOP == "Alterar"){
      		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 			$oIndicacao = $oFachada->inicializarIndicacao($_REQUEST['fCodIndicacao'],$_REQUEST['fDescricao'],1,$nIdEscritorio);
 			// $_SESSION['oIndicacao'] = $oIndicacao;


 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_text_field("Descricao", $oIndicacao->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oIndicacao->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Indicacao.preparaFormulario&sOP=".$sOP."&nIdIndicacao=".$_POST['fCodIndicacao']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
     			$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
      			$oIndicacao = $oFachada->inicializarIndicacao($_GET['fCodIndicacao'],$_GET['fDescricao'],1,$nIdEscritorio);
 				if($nCodIndicacao = $oFachada->inserirIndicacao($oIndicacao)) {
 					$_SESSION['sMsg'] = "Indicação inserida com sucesso!";
						if($_REQUEST['nOrigem'] == 1){
							echo(json_encode(array("sMsg"=>"Indicação inserida com sucesso!","bErro"=>"alert alert-success  alert-dismissible","sIcone"=>"icon fa fa-check","nCodIndicacao"=>$nCodIndicacao)));
							die();
						}
						echo json_encode(["sMsg"=>"N&atilde;o foi poss&iacute;vel inserir Indicação!","bErro"=>"alert alert-danger  alert-dismissible","sIcone"=>"icon fa fa-ban"]);
						die();
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarIndicacao($oIndicacao)){
 					unset($_SESSION['oIndicacao']);
 					$_SESSION['sMsg'] = "Indicação alterado com sucesso!";
 					$sHeader = "?bErro=0&action=Indicacao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar Indicação!";
 					$sHeader = "?bErro=1&action=Indicacao.preparaFormulario&sOP=".$sOP."&nIdIndicacao=".$_POST['fCodIndicacao'];
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiIndicacao = explode("____",$_REQUEST['fIdIndicacao']);
   				foreach($vIdPaiIndicacao as $vIdFilhoIndicacao){
  					$vIdIndicacao = explode("||",$vIdFilhoIndicacao);
 					foreach($vIdIndicacao as $nIdIndicacao){
  						$bResultado &= $oFachada->excluirIndicacao($vIdIndicacao[0],$vIdIndicacao[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Indicação(ões) exclu&iacute;da(s) com sucesso!";
 					$sHeader = "?bErro=0&action=Indicacao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a(s) Indicação(ões)!";
 					$sHeader = "?bErro=1&action=Indicacao.preparaLista";
 				}
 			break;
 		}
 		header("Location: ".$sHeader);

 	}

 }


 ?>
