<?php
 class FormaPagamentoCTR implements IControle{
 
 	public function preparaLista(){
 		$oFachada = new FachadaAdminBD();
 		$voFormaPagamento = $oFachada->recuperarTodosFormaPagamento();
 		$_REQUEST['voFormaPagamento'] = $voFormaPagamento;
 		include_once("view/principal/forma_pagamento/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaAdminBD();
 
 		$oFormaPagamento = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdFormaPagamento = ($_POST['fIdFormaPagamento'][0]) ? $_POST['fIdFormaPagamento'][0] : $_GET['nIdFormaPagamento'];
 	
 			if($nIdFormaPagamento){
 				$vIdFormaPagamento = explode("||",$nIdFormaPagamento);
 				$oFormaPagamento = $oFachada->recuperarUmFormaPagamento($vIdFormaPagamento[0]);
 			}
 		}
 		
 		$_REQUEST['oFormaPagamento'] = ($_SESSION['oFormaPagamento']) ? $_SESSION['oFormaPagamento'] : $oFormaPagamento;
 		unset($_SESSION['oFormaPagamento']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/forma_pagamento/detalhe.php");
 		else
 			include_once("view/principal/forma_pagamento/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaAdminBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
         switch($sOP){
                case 'Cadastrar':
                       $_POST['fIncluidoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                break;
                case 'Alterar':
                    $_POST['fAlteradoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
                    $oFormaPagamento1 = $oFachada->recuperarUmFormaPagamento($_POST['fCodFormaPagamento']);
                    $_POST['fIncluidoPor'] = $oFormaPagamento1->getIncluidoPor();
                break;
                
            }            
 			$oFormaPagamento = $oFachada->inicializarFormaPagamento($_POST['fCodFormaPagamento'],$_POST['fDescricao'],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],1);
 			$_SESSION['oFormaPagamento'] = $oFormaPagamento;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Descrição", $oFormaPagamento->getDescricao(), "text", "y");
			$oValidate->add_text_field("Incluido Por", $oFormaPagamento->getIncluidoPor(), "text", "y");
			if($sOP == 'Alterar')
                $oValidate->add_text_field("Alterado Por", $oFormaPagamento->getAlteradoPor(), "text", "y");
			

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=FormaPagamento.preparaFormulario&sOP=".$sOP."&nIdFormaPagamento=".$_POST['fCodFormaPagamento']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirFormaPagamento($oFormaPagamento)){
 					unset($_SESSION['oFormaPagamento']);
 					$_SESSION['sMsg'] = "Forma de pagamento inserido com sucesso!";
 					$sHeader = "?bErro=0&action=FormaPagamento.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Forma de pagamento!";
 					$sHeader = "?bErro=1&action=FormaPagamento.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarFormaPagamento($oFormaPagamento)){
 					unset($_SESSION['oFormaPagamento']);
 					$_SESSION['sMsg'] = "Forma de pagamento alterado com sucesso!";
 					$sHeader = "?bErro=0&action=FormaPagamento.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Forma de pagamento!";
 					$sHeader = "?bErro=1&action=FormaPagamento.preparaFormulario&sOP=".$sOP."&nIdFormaPagamento=".$_POST['fCodFormaPagamento']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiFormaPagamento = explode("____",$_REQUEST['fIdFormaPagamento']);
   				foreach($vIdPaiFormaPagamento as $vIdFilhoFormaPagamento){
  					$vIdFormaPagamento = explode("||",$vIdFilhoFormaPagamento);
 					foreach($vIdFormaPagamento as $nIdFormaPagamento){
  						$bResultado &= $oFachada->excluirFormaPagamento($vIdFormaPagamento[0],$vIdFormaPagamento[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Forma de pagamento(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=FormaPagamento.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Forma de pagamento!";
 					$sHeader = "?bErro=1&action=FormaPagamento.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>