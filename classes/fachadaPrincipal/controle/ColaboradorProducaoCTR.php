<?php
 class ColaboradorProducaoCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
		$nIdColaborador = ($_POST['fIdColaborador'][0]) ? $_POST['fIdColaborador'][0] : $_GET['nIdColaborador'];
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
		if($nIdColaborador){
			$_REQUEST['oColaborador'] = $oFachada->recuperarUmColaborador($nIdColaborador);
			// $voColaboradorProducao = $oFachada->recuperarTodosColaboradorProducaoPorColaborador($nIdColaborador);
			$voColaboradorProducao = $oFachada->recuperarTodosProjetoPorColaborador($nIdColaborador);
			$voProjetoVinculo = $oFachada->recuperarTodosProjetoVinculoPorColaborador($nIdColaborador,$nIdEscritorio);

		}

 		$_REQUEST['voColaboradorProducao'] = $voColaboradorProducao;
		$_REQUEST['voProjetoVinculo'] = $voProjetoVinculo;


 		include_once("view/principal/colaborador_producao/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$oColaboradorProducao = false;
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" ){
 			$nIdColaboradorProducao = ($_POST['fIdCodProducao'][0]) ? $_POST['fIdCodProducao'][0] : $_GET['nIdColaboradorProducao'];
 			if($nIdColaboradorProducao){
 				$vIdColaborador = explode("||",$nIdColaboradorProducao);
 				$oColaboradorProducao = $oFachada->recuperarUmColaboradorProducao($vIdColaborador[0]);
 			}
 		}

    $_REQUEST['voProjeto'] = $oFachada->recuperarTodosProjeto();
    $_REQUEST['voColaborador'] = $oFachada->recuperarTodosColaborador();
 		$_REQUEST['oColaboradorProducao'] = ($_SESSION['oColaboradorProducao']) ? $_SESSION['oColaboradorProducao'] : $oColaboradorProducao;
 		unset($_SESSION['oColaboradorProducao']);
    // $_REQUEST['voColaboradorCarreira'] = $voColaboradorCarreira;

    switch($_REQUEST['sOP']){
      case "Detalhar":
            //include_once("view/principal/colaborador/detalhe2.php");
			// $_REQUEST['voProjetoColaborador'] = $oFachada->recuperarTodosProjetoPorColaborador($nIdColaborador);
            include_once("view/principal/colaborador_producao/detalhe3.php");
      break;
      case "Cadastrar":
      case "Alterar":
            include_once("view/principal/colaborador_producao/insere_altera.php");
      break;
    }


 		exit();


 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];


 			$oColaboradorProducao = $oFachada->inicializarColaboradorProducao($_POST['fCodProducao'],$_POST['fCodColaborador'],$_POST['fCodProjeto'],$_POST['fPercentual'],$_POST['fValor']);
 			$_SESSION['oColaboradorProducao'] = $oColaboradorProducao;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
      $oValidate->add_number_field("Colaborador", $oColaboradorProducao->getCodColaborador(), "number", "y");
      $oValidate->add_number_field("Projeto", $oColaboradorProducao->getCodProjeto(), "number", "y");

 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=".$sOP."&nIdColaborador=".$_POST['fCodColaborador']."";
 				header("Location: ".$sHeader);
 				die();
 			}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirColaboradorProducao($oColaboradorProducao)){
 					unset($_SESSION['oColaboradorProducao']);
 					$_SESSION['sMsg'] = "Vínculo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=ColaboradorProducao.preparaLista&nIdColaborador={$_POST['fCodColaborador']}";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Vínculo!";
 					$sHeader = "?bErro=1&action=ColaboradorProducao.preparaLista&nIdColaborador={$_POST['fCodColaborador']}";
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarColaboradorProducao($oColaboradorProducao)){
 					unset($_SESSION['oColaboradorProducao']);
 					$_SESSION['sMsg'] = "ColaboradorProducao alterado com sucesso!";
 					$sHeader = "?bErro=0&action=ColaboradorProducao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o ColaboradorProducao!";
 					$sHeader = "?bErro=1&action=ColaboradorProducao.preparaFormulario&sOP=".$sOP."&nIdColaboradorProducao=".$_POST['fCodColaboradorProducao']."";
 				}
 			break;

 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiColaboradorProducao = explode("____",$_REQUEST['fIdColaboradorProducao']);
   				foreach($vIdPaiColaboradorProducao as $vIdFilhoColaboradorProducao){
  					$vIdColaboradorProducao = explode("||",$vIdFilhoColaboradorProducao);
 					foreach($vIdColaboradorProducao as $nIdColaboradorProducao){
  						$bResultado &= $oFachada->excluirColaboradorProducao($vIdColaboradorProducao[0],$vIdColaboradorProducao[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "ColaboradorProducao(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=ColaboradorProducao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) ColaboradorProducao!";
 					$sHeader = "?bErro=1&action=ColaboradorProducao.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}
 }
 ?>
