<?php
 class ContaBancariaCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
    $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 		$voContaBancaria = $oFachada->recuperarTodosContaBancaria($nIdEscritorio);
 		$_REQUEST['voContaBancaria'] = $voContaBancaria;

 		include_once("view/principal/conta_bancaria/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();
    $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 		$oContaBancaria = false;

 		if($_REQUEST['sOP'] == "Alterar"){
 			$nIdContaBancaria = ($_POST['fIdContaBancaria'][0]) ? $_POST['fIdContaBancaria'][0] : $_GET['nIdContaBancaria'];

 			if($nIdContaBancaria){
 				$vIdContaBancaria = explode("||",$nIdContaBancaria);
 				$oContaBancaria = $oFachada->recuperarUmContaBancaria($vIdContaBancaria[0]);
 			}
 		}

 		$_REQUEST['oContaBancaria'] = ($_SESSION['oContaBancaria']) ? $_SESSION['oContaBancaria'] : $oContaBancaria;
 		unset($_SESSION['oContaBancaria']);
 		$_REQUEST['voColaborador'] = $oFachada->recuperarTodosColaborador($nIdEscritorio);

		include_once("view/principal/conta_bancaria/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){

      switch($sOP){
          case 'Cadastrar':
                 $_POST['fIncluidoPor'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
          break;
          case 'Alterar':
              $oContaBancaria1 = $oFachada->recuperarUmContaBancaria($_POST['fCodContaBancaria']);
              $_POST['fIncluidoPor'] =  $oContaBancaria1->getIncluidoPor();
              $_POST['fAlteradoPor'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
          break;

      }
      $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 			$oContaBancaria = $oFachada->inicializarContaBancaria($_POST['fCodContaBancaria'],$_POST['fCodColaborador'],$_POST['fAgencia'],$_POST['fConta'],$_POST['fTipo'],$_POST['fBanco'],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],$_POST['fDataEncerramento'],1,$nIdEscritorio,$_POST['fSaldoInicial'],$_POST['fDataInicio']);
 			$_SESSION['oContaBancaria'] = $oContaBancaria;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("Responsável", $oContaBancaria->getCodColaborador(), "number", "y");
			$oValidate->add_text_field("Agência", $oContaBancaria->getAgencia(), "text", "y");
			$oValidate->add_text_field("Conta", $oContaBancaria->getConta(), "text", "y");
			$oValidate->add_number_field("Tipo", $oContaBancaria->getTipo(), "number", "y");
			$oValidate->add_text_field("Banco", $oContaBancaria->getBanco(), "text", "y");
			$oValidate->add_text_field("Incluido Por", $oContaBancaria->getIncluidoPor(), "text", "y");
			//$oValidate->add_text_field("Alterado Por", $oContaBancaria->getAlteradoPor(), "text", "y");
			//$oValidate->add_date_field("Data Encerramento", $oContaBancaria->getDataEncerramento(), "date", "y");
      if($sOP ==='Cadastrar'){
        $oValidate->add_date_field("Data Início Sistema", $oContaBancaria->getDataInicio(), "date", "y");
        $oValidate->add_text_field("Saldo Início Sistema", $oContaBancaria->getSaldoInicial());
      }
			$oValidate->add_number_field("Ativo", $oContaBancaria->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=ContaBancaria.preparaFormulario&sOP=".$sOP."&nIdContaBancaria=".$_POST['fCodContaBancaria']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($nIdContaBancaria = $oFachada->inserirContaBancaria($oContaBancaria)){
          $oContaBancariaNova = $oFachada->recuperarUmContaBancaria($nIdContaBancaria);
          $oContaMovimentacao = $oFachada->inicializarContaMovimentacao($_POST['fCodContaMovimentacao'],$nIdContaBancaria,NULL,$oContaBancariaNova->getSaldoInicialFormatado(),NULL,NULL,$oContaBancariaNova->getDataInicioFormatado(),1,NULL,NULL);
          $oFachada->inserirContaMovimentacao($oContaMovimentacao);
 					unset($_SESSION['oContaBancaria']);
 					$_SESSION['sMsg'] = "Conta Bancária inserida com sucesso!";
 					$sHeader = "?bErro=0&action=ContaBancaria.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a Conta Bancária!";
 					$sHeader = "?bErro=1&action=ContaBancaria.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarContaBancaria($oContaBancaria)){
 					unset($_SESSION['oContaBancaria']);
 					$_SESSION['sMsg'] = "Conta Bancária alterada com sucesso!";
 					$sHeader = "?bErro=0&action=ContaBancaria.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Conta Bancária!";
 					$sHeader = "?bErro=1&action=ContaBancaria.preparaFormulario&sOP=".$sOP."&nIdContaBancaria=".$_POST['fCodContaBancaria']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiContaBancaria = explode("____",$_REQUEST['fIdContaBancaria']);
   				foreach($vIdPaiContaBancaria as $vIdFilhoContaBancaria){
  					$vIdContaBancaria = explode("||",$vIdFilhoContaBancaria);
 					foreach($vIdContaBancaria as $nIdContaBancaria){
  						$bResultado &= $oFachada->excluirContaBancaria($vIdContaBancaria[0],$vIdContaBancaria[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Conta Bancária(s) exclu&iacute;da(s) com sucesso!";
 					$sHeader = "?bErro=0&action=ContaBancaria.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a(s) Conta Bancária(s)!";
 					$sHeader = "?bErro=1&action=ContaBancaria.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }

 ?>
