<?php
class ServicoMicroServicoCTR implements IControle{
    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $_REQUEST['vSelecao'] = null;
        $sOP = $_REQUEST['sOP'] ?? null;

        if($sOP == "Servico"){
            $_REQUEST['sOP2'] = $_GET['sOP2'] ?? null;
            $nCodServico = $_REQUEST['nIdServico'];
            $voServicoMicroServico = $oFachada->recuperarTodosServicoMicroServico($nCodServico);
            $nCodProposta = $_REQUEST['nCodProposta'] ?? null;
            if($nCodProposta){
                $voServicoMicroServicoSelecionados = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($nCodProposta);
                $vSelecao = [];
                if($voServicoMicroServicoSelecionados){
                    foreach($voServicoMicroServicoSelecionados as $oSelecao){
                        $vSelecao[$oSelecao->getCodMicroservico()] = [$oSelecao->getCodMicroservico(),$oSelecao->getQuantidade(),$oSelecao->getDescricaoQuantidade(),$oSelecao->getDias(),$oSelecao->getDataPrevisaoInicio(),$oSelecao->getDataEfetivacaoInicio(),$oSelecao->getDataPrevisaoFim(),$oSelecao->getDataEfetivacaoFim()];
                    }
                }
                $_REQUEST['vSelecao'] = $vSelecao;
            }

            $_REQUEST['voServicoMicroServico'] = $voServicoMicroServico;
            include_once("view/principal/servico_microservico/lista_microservico.php");
        } elseif ($sOP == "Proposta") {
            $nCodProposta = $_REQUEST['CodProposta'];
            $voServicoMicroServico = $oFachada->recuperarTodosMicroServicoPorProposta($nCodProposta);
            $_REQUEST['voServicoMicroServico'] = $voServicoMicroServico;
            include_once("view/Principal/servico_microservico/lista_proposta.php");
        } else{
            $nCodServico = $_REQUEST['nCodServico'] ?? null;
            $nCodEtapa = $_REQUEST['nCodEtapa'] ?? null;

            $_REQUEST['oServico'] = $oFachada->recuperarUmServico($nCodServico);
            $_REQUEST['voMicroServico'] = $oFachada->recuperarTodosServicoMicroServicoPorEtapaServico($nCodEtapa,$nCodServico);

            include_once("view/principal/servico_microservico/index.php");
        }
        exit();
    }


    public function preparaLista2(){
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $oFachada = new FachadaPrincipalBD();
        $nCodServicoEtapa = ($_POST['fIdServicoEtapa'][0]) ?: $_GET['nIdServicoEtapa'];

        if($_REQUEST['sOP'] == "ServicoEtapa" || ($nCodServicoEtapa)){
            $_REQUEST['sOP2'] = $_GET['sOP2'];
            $_REQUEST['oEtapa'] = $oFachada->recuperarUmServicoEtapa($nCodServicoEtapa);
            $voServicoMicroServico = $oFachada->recuperarTodosServicoMicroServicoPorEtapaServico($nCodServicoEtapa,$_REQUEST['oEtapa']->getCodServico());

            $_REQUEST['voServicoMicroServico'] = $voServicoMicroServico;
            include_once("view/principal/servico_microservico/index.php");
            exit();
        }
    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $oServicoMicroServico = false;

        if (isset($_REQUEST['nCodServicoEtapa']))
            $_REQUEST['oServicoEtapa'] = $oFachada->recuperarUmServicoEtapa($_REQUEST['nCodServicoEtapa']);
        else
            $_REQUEST['voServicoEtapa'] = $oFachada->recuperarTodosServicoEtapa($_GET['nCodServico'],$nIdEscritorio);

        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar")
            $oServicoMicroServico = $oFachada->recuperarUmServicoMicroServico($_GET['nCodMicroServico']);

        $_REQUEST['oServicoMicroServico'] = $oServicoMicroServico;

        $_REQUEST['voServico'] = $oFachada->recuperarTodosServico($nIdEscritorio);
        if($_REQUEST['sOP'] == "Detalhar")
            include_once("view/principal/servico_microservico/detalhe.php");
        else{
            include_once("view/principal/servico_microservico/insere_altera.php");
        }
        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
        $oServicoMicroServico = null;

        if($sOP != "Excluir"){
            $_POST['fCodEtapa'] = $_POST['fCodEtapa'] ?? null;
            $oServicoMicroServico = $oFachada->inicializarServicoMicroServico($_POST['fCodMicroservico'],$_POST['fCodServicoEtapa'],$_POST['fDescricao'],$_POST['fCor'],$_POST['fOrdem'],$_POST['fPrazo'],$_POST['fDetalhe']);
        }

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirServicoMicroServico($oServicoMicroServico)){
                    echo json_encode([
                        'sMsg'=>"Microserviço inserido com sucesso!",
                        'sClass'=>'success',
                        'nCodServico'=>$_POST['fCodServico'],
                        'nCodEtapa'=>$_POST['fCodEtapa'],
                        'nCodServicoEtapa'=>$_POST['fCodServicoEtapa']
                    ]);
                } else {
                    echo json_encode([
                        'sMsg'=>"Não foi possível inserir o Microserviço!",
                        'sClass'=>'error',
                        'nCodServico'=>$_POST['fCodServico'],
                        'nCodEtapa'=>$_POST['fCodEtapa'],
                        'nCodServicoEtapa'=>$_POST['fCodServicoEtapa']
                    ]);
                }
                break;
            case "Alterar":
                if($oFachada->alterarServicoMicroServico($oServicoMicroServico)){
                    echo json_encode([
                        'sMsg'=>"Microserviço alterado com sucesso!",
                        'sClass'=>'success',
                        'nCodServico'=>$_POST['fCodServico'],
                        'nCodEtapa'=>$_POST['fCodEtapa'],
                        'nCodServicoEtapa'=>$_POST['fCodServicoEtapa']
                    ]);
                } else {
                    echo json_encode([
                        'sMsg'=>"Não foi possível alterar o Microserviço!",
                        'sClass'=>'error',
                        'nCodServico'=>$_POST['fCodServico'],
                        'nCodEtapa'=>$_POST['fCodEtapa'],
                        'nCodServicoEtapa'=>$_POST['fCodServicoEtapa']
                    ]);
                }
                die();
            case "Excluir":
                $bResultado = $oFachada->excluirServicoMicroServico($_GET['nCodMicroServico']);
                if($bResultado){
                    echo json_encode([
                        'sMsg'=>"Microserviço excluído com sucesso!",
                        'sClass'=>'success',
                        'nCodServico'=>$_GET['fCodServico'],
                        'nCodEtapa'=>$_GET['fCodEtapa'],
                        'nCodServicoEtapa'=>$_POST['fCodServicoEtapa']
                    ]);
                } else {
                    echo json_encode([
                        'sMsg'=>"Não foi possível excluir o Microserviço!",
                        'sClass'=>'success',
                        'nCodServico'=>$_GET['fCodServico'],
                        'nCodEtapa'=>$_GET['fCodEtapa'],
                        'nCodServicoEtapa'=>$_POST['fCodServicoEtapa']
                    ]);
                }
                break;
        }

        die();

    }

}
