<?php
 class ColaboradorPagamentoCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
			$nIdColaborador = ($_POST['fIdColaborador'][0]) ? $_POST['fIdColaborador'][0] : $_GET['nIdColaborador'];

 		switch($_REQUEST['sOP'] ){
			case 'Producao':
				$_REQUEST['voColaboradorPagamento'] = $oFachada->recuperarTodosColaboradorPagamentoPorProducao($_REQUEST['fCodProducao']);
				$_REQUEST['oColaboradorProducao'] = $oFachada->recuperarUmVColaboradorProducaoPorProducao($_REQUEST['fCodProducao']);
				include_once("view/principal/colaborador_producao/lista_ajax.php");
			break;
			default:
				$voColaboradorProducao = $oFachada->recuperarTodosColaboradorPagamentoPorColaborador($nIdColaborador);
				$_REQUEST['voColaboradorProducao'] = $voColaboradorProducao;
				include_once("view/principal/colaborador_producao/index.php");
			break;
		}




 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$oColaboradorProducao = false;
/*
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" )
 			$nIdColaboradorProducao = ($_POST['fIdCodProducao'][0]) ? $_POST['fIdCodProducao'][0] : $_GET['nIdColaboradorProducao'];


 			if($nIdColaboradorProducao){
 				$vIdColaborador = explode("||",$nIdColaboradorProducao);
 				$oColaboradorProducao = $oFachada->recuperarUmColaboradorProducao($vIdColaborador[0]);
 			}
 		}

    $_REQUEST['voProjeto'] = $oFachada->recuperarTodosProjeto();
    $_REQUEST['voColaborador'] = $oFachada->recuperarTodosColaborador();
 		$_REQUEST['oColaboradorProducao'] = ($_SESSION['oColaboradorProducao']) ? $_SESSION['oColaboradorProducao'] : $oColaboradorProducao;
 		unset($_SESSION['oColaboradorProducao']);
    // $_REQUEST['voColaboradorCarreira'] = $voColaboradorCarreira;

    switch($_REQUEST['sOP']){
      case "Detalhar":
            //include_once("view/principal/colaborador/detalhe2.php");
			// $_REQUEST['voProjetoColaborador'] = $oFachada->recuperarTodosProjetoPorColaborador($nIdColaborador);
            include_once("view/principal/colaborador_producao/detalhe3.php");
      break;
      case "Cadastrar":
      case "Alterar":
            include_once("view/principal/colaborador_producao/insere_altera.php");
      break;
    }


 		exit();
		*/

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();
/*
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];


 			$oColaboradorProducao = $oFachada->inicializarColaboradorProducao($_POST['fCodProducao'],$_POST['fCodColaborador'],$_POST['fCodProjeto'],$_POST['fPercentual'],$_POST['fValor']);
 			$_SESSION['oColaboradorProducao'] = $oColaboradorProducao;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
      $oValidate->add_number_field("Colaborador", $oColaborador->getCodColaborador(), "number", "y");
      $oValidate->add_number_field("Projeto", $oColaborador->getCodProjeto(), "number", "y");

 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Colaborador.preparaFormulario&sOP=".$sOP."&nIdColaborador=".$_POST['fCodColaborador']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirColaboradorProducao($oColaboradorProducao)){
 					unset($_SESSION['oColaboradorProducao']);
 					$_SESSION['sMsg'] = "ColaboradorProducao inserido com sucesso!";
 					$sHeader = "?bErro=0&action=ColaboradorProducao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o ColaboradorProducao!";
 					$sHeader = "?bErro=1&action=ColaboradorProducao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarColaboradorProducao($oColaboradorProducao)){
 					unset($_SESSION['oColaboradorProducao']);
 					$_SESSION['sMsg'] = "ColaboradorProducao alterado com sucesso!";
 					$sHeader = "?bErro=0&action=ColaboradorProducao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o ColaboradorProducao!";
 					$sHeader = "?bErro=1&action=ColaboradorProducao.preparaFormulario&sOP=".$sOP."&nIdColaboradorProducao=".$_POST['fCodColaboradorProducao']."";
 				}
 			break;

 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiColaboradorProducao = explode("____",$_REQUEST['fIdColaboradorProducao']);
   				foreach($vIdPaiColaboradorProducao as $vIdFilhoColaboradorProducao){
  					$vIdColaboradorProducao = explode("||",$vIdFilhoColaboradorProducao);
 					foreach($vIdColaboradorProducao as $nIdColaboradorProducao){
  						$bResultado &= $oFachada->excluirColaboradorProducao($vIdColaboradorProducao[0],$vIdColaboradorProducao[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "ColaboradorProducao(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=ColaboradorProducao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) ColaboradorProducao!";
 					$sHeader = "?bErro=1&action=ColaboradorProducao.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);
*/
 	}
 }
 ?>
