<?php /** @noinspection PhpUnused */

class ServicoEtapaMicroServicoCTR implements IControle{

	public function preparaLista(){
		$oFachada = new FachadaPrincipalBD();
		if($_REQUEST['sOP'] == "Servico"){
			$_REQUEST['sOP2'] = $_GET['sOP2'];
			$nCodServico = $_REQUEST['nIdServico'];
			$voServicoMicroServico = $oFachada->recuperarTodosServicoMicroServico($nCodServico);
			if($nCodProposta = $_REQUEST['nCodProposta']){
				$voServicoMicroServicoSelecionados = $oFachada->recuperarTodosServicoMicroServicoProposta($nCodServico,$nCodProposta);

				$vSelecao = [];
				foreach($voServicoMicroServicoSelecionados as $oSelecao){
					$vSelecao[$oSelecao->getCodMicroservico()] = [$oSelecao->getCodMicroservico(),$oSelecao->getQuantidade(),$oSelecao->getDescricaoQuantidade()];
				}
				$_REQUEST['vSelecao'] = $vSelecao;
			}
			$_REQUEST['voServicoMicroServico'] = $voServicoMicroServico;

			include_once("view/principal/servico_microservico/lista_servico.php");
			exit();
		} elseif ($_REQUEST['sOP'] == "Proposta") {
			$nCodProposta = $_REQUEST['CodProposta'];
			$voServicoMicroServico = $oFachada->recuperarTodosMicroServicoPorProposta($nCodProposta);
			$_REQUEST['voServicoMicroServico'] = $voServicoMicroServico;
			include_once("view/Principal/servico_microservico/lista_proposta.php");
			exit();
		}
		else{
			$voServicoMicroServico = $oFachada->recuperarTodosServicoMicroServico();
			$_REQUEST['voServicoMicroServico'] = $voServicoMicroServico;

			include_once("view/Principal/servico_microservico/index.php");
			exit();
		}
	}

	public function preparaFormulario(){
		$oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
		$oServicoMicroServico = false;

		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
			$nIdServicoMicroServico = ($_POST['fIdServicoMicroServico'][0]) ? $_POST['fIdServicoMicroServico'][0] : $_GET['nIdServicoMicroServico'];

			if($nIdServicoMicroServico){
				$vIdServicoMicroServico = explode("||",$nIdServicoMicroServico);
				$oServicoMicroServico = $oFachada->recuperarUmServicoMicroServico($vIdServicoMicroServico[0]);
			}
		}

		$_REQUEST['oServicoMicroServico'] = ($_SESSION['oServicoMicroServico']) ? $_SESSION['oServicoMicroServico'] : $oServicoMicroServico;
		unset($_SESSION['oServicoMicroServico']);
		$_REQUEST['voServico'] = $oFachada->recuperarTodosServico($nIdEscritorio);
		if($_REQUEST['sOP'] == "Detalhar")
			include_once("view/Principal/servico_microservico/detalhe.php");
		else
			include_once("view/Principal/servico_microservico/insere_altera.php");
		exit();

	}

	public function processaFormulario(){
		$oFachada = new FachadaPrincipalBD();
		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
		$sHeader = "";
		$oServicoMicroServico = null;

		if($sOP != "Excluir"){
			$oServicoMicroServico = $oFachada->inicializarServicoMicroServico($_POST['fCodMicroservico'],$_POST['fCodServico'],$_POST['fCodEtapa'],$_POST['fDescricao'],$_POST['fOrdem']);
			$_SESSION['oServicoMicroServico'] = $oServicoMicroServico;

			$oValidate = FabricaUtilitario::getUtilitario("Validate");
			$oValidate->check_4html = true;

			if (!$oValidate->validation()) {
				$_SESSION['sMsg'] = $oValidate->create_msg();
				$sHeader = "?bErro=1&action=ServicoMicroServico.preparaFormulario&sOP=".$sOP."&nIdServicoMicroServico=".$_POST['fCodMicroservico']."";
				header("Location: ".$sHeader);
				die();
			}
		}

		switch($sOP){
			case "Cadastrar":
				if($oFachada->inserirServicoMicroServico($oServicoMicroServico)){
					unset($_SESSION['oServicoMicroServico']);
					$_SESSION['sMsg'] = "servico_microservico inserido com sucesso!";
					$sHeader = "?bErro=0&action=ServicoMicroServico.preparaLista";

				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o servico_microservico!";
					$sHeader = "?bErro=1&action=ServicoMicroServico.preparaFormulario&sOP=".$sOP;
				}
				break;
			case "Alterar":
				if($oFachada->alterarServicoMicroServico($oServicoMicroServico)){
					unset($_SESSION['oServicoMicroServico']);
					$_SESSION['sMsg'] = "servico_microservico alterado com sucesso!";
					$sHeader = "?bErro=0&action=ServicoMicroServico.preparaLista";

				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o servico_microservico!";
					$sHeader = "?bErro=1&action=ServicoMicroServico.preparaFormulario&sOP=".$sOP."&nIdServicoMicroServico=".$_POST['fCodMicroservico']."";
				}
				break;
			case "Excluir":

				$CodServicoMicroServico = explode("____",$_REQUEST['fIdServicoMicroServico']);
				$bResultado = $oFachada->excluirServicoMicroServico($CodServicoMicroServico);
				if($bResultado){
					$_SESSION['sMsg'] = "servico_microservico(s) excluído(s) com sucesso!";
					$sHeader = "?bErro=0&action=ServicoMicroServico.preparaLista";
				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) servico_microservico!";
					$sHeader = "?bErro=1&action=ServicoMicroServico.preparaLista";
				}
				break;
		}

		header("Location: ".$sHeader);

	}

}
