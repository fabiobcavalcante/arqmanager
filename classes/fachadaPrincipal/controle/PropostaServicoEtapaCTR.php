<?php
class PropostaServicoEtapaCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $voPropostaServicoEtapa = $oFachada->recuperarTodosPropostaServicoEtapa();
        $_REQUEST['voPropostaServicoEtapa'] = $voPropostaServicoEtapa;

        include_once("view/principal/proposta_servico_etapa/index.php");
        exit();


    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $oPropostaServicoEtapa = false;

        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
            $nIdPropostaServicoEtapa = ($_POST['fIdPropostaServicoEtapa'][0]) ? $_POST['fIdPropostaServicoEtapa'][0] : $_GET['nIdPropostaServicoEtapa'];

            if($nIdPropostaServicoEtapa){
                $vIdPropostaServicoEtapa = explode("||",$nIdPropostaServicoEtapa);
                $oPropostaServicoEtapa = $oFachada->recuperarUmPropostaServicoEtapa($vIdPropostaServicoEtapa[0],$vIdPropostaServicoEtapa[1]);
            }
        }

        $_REQUEST['oPropostaServicoEtapa'] = ($_SESSION['oPropostaServicoEtapa']) ? $_SESSION['oPropostaServicoEtapa'] : $oPropostaServicoEtapa;
        unset($_SESSION['oPropostaServicoEtapa']);

        $_REQUEST['voProjeto'] = $oFachada->recuperarTodosProjeto($nIdEscritorio);
        $_REQUEST['voServicoEtapa'] = $oFachada->recuperarTodosServicoEtapa($nIdEscritorio);
        $_REQUEST['voProposta'] = $oFachada->recuperarTodosProposta($nIdEscritorio);

        if($_REQUEST['sOP'] == "Detalhar")
            include_once("view/principal/proposta_servico_etapa/detalhe.php");
        else
            include_once("view/principal/proposta_servico_etapa/insere_altera.php");

        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

        if($sOP != "Excluir"){
            $oPropostaServicoEtapa = $oFachada->inicializarPropostaServicoEtapa($_POST['fCodEtapa'],$_POST['fCodProposta'],$_POST['fCodProjeto'],$_POST['fDescricao'],$_POST['fPrazo'],$_POST['fDataPrevista'],$_POST['fDataConclusao'],$_POST['fObservacao']);
            $_SESSION['oPropostaServicoEtapa'] = $oPropostaServicoEtapa;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=PropostaServicoEtapa.preparaFormulario&sOP=".$sOP."&nIdPropostaServicoEtapa=".$_POST['fCodEtapa']."||".$_POST['fCodProposta']."";
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirPropostaServicoEtapa($oPropostaServicoEtapa)){
                    unset($_SESSION['oPropostaServicoEtapa']);
                    $_SESSION['sMsg'] = "projeto_servico_etapa inserido com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaServicoEtapa.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o projeto_servico_etapa!";
                    $sHeader = "?bErro=1&action=PropostaServicoEtapa.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Alterar":
                if($oFachada->alterarPropostaServicoEtapa($oPropostaServicoEtapa)){
                    unset($_SESSION['oPropostaServicoEtapa']);
                    $_SESSION['sMsg'] = "projeto_servico_etapa alterado com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaServicoEtapa.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o projeto_servico_etapa!";
                    $sHeader = "?bErro=1&action=PropostaServicoEtapa.preparaFormulario&sOP=".$sOP."&nIdPropostaServicoEtapa=".$_POST['fCodEtapa']."||".$_POST['fCodProposta']."";
                }
                break;
            
            case "Justificar_old":
                $oPropostaServicoEtapa = $oFachada->recuperarUmPropostaServicoEtapa($_REQUEST['nCodEtapa'],$_GET['nCodProposta']);
                $oPropostaServicoEtapa->setObservacao($_REQUEST['fObservacao']);
                if($oFachada->alterarPropostaServicoEtapa($oPropostaServicoEtapa)){
                    echo json_encode(['sMsg'=>'Etapa Justificada com sucesso!','sClass'=>'success']);
                } else {
                    echo json_encode(['sMsg'=>'Não foi possível Justificar a etapa!','sClass'=>'danger']);
                }
                die();
            case "Excluir":
                $bResultado = true;

                $vIdPaiPropostaServicoEtapa = explode("____",$_REQUEST['fIdPropostaServicoEtapa']);
                foreach($vIdPaiPropostaServicoEtapa as $vIdFilhoPropostaServicoEtapa){
                    $vIdPropostaServicoEtapa = explode("||",$vIdFilhoPropostaServicoEtapa);
                    foreach($vIdPropostaServicoEtapa as $nIdPropostaServicoEtapa){
                        $bResultado &= $oFachada->excluirPropostaServicoEtapa($vIdPropostaServicoEtapa[0],$vIdPropostaServicoEtapa[1]);
                    }
                }

                if($bResultado){
                    $_SESSION['sMsg'] = "projeto_servico_etapa(s) exclu&iacute;do(s) com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaServicoEtapa.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) projeto_servico_etapa!";
                    $sHeader = "?bErro=1&action=PropostaServicoEtapa.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);

    }
}
