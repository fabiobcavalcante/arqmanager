<?php
 class DespesaReceitaCTR implements IControle{

 	public function preparaLista(){
 		$oFachada = new FachadaPrincipalBD();
    $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 		$voDespesaReceita = $oFachada->recuperarTodosDespesaReceita($nIdEscritorio);
 		$_REQUEST['voDespesaReceita'] = $voDespesaReceita;

 		include_once("view/principal/despesa_receita/index.php");
 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$oDespesaReceita = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdDespesaReceita = ($_POST['fIdDespesaReceita'][0]) ? $_POST['fIdDespesaReceita'][0] : $_GET['nIdDespesaReceita'];

 			if($nIdDespesaReceita){
 				$vIdDespesaReceita = explode("||",$nIdDespesaReceita);
 				$oDespesaReceita = $oFachada->recuperarUmDespesaReceita($vIdDespesaReceita[0]);
 			}
 		}

 		$_REQUEST['oDespesaReceita'] = ($_SESSION['oDespesaReceita']) ? $_SESSION['oDespesaReceita'] : $oDespesaReceita;
 		unset($_SESSION['oDespesaReceita']);

 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/principal/despesa_receita/detalhe.php");
 		else
 			include_once("view/principal/despesa_receita/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPrincipalBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
      $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
 			$oDespesaReceita = $oFachada->inicializarDespesaReceita($_POST['fCodDespesaReceita'],$_POST['fCodDespesaReceitaTipo'],$_POST['fDescricao'],$_POST['fIncluidoPor'],$_POST['fAlteradoPor'],$_POST['fAtivo'],$nIdEscritorio);
 			$_SESSION['oDespesaReceita'] = $oDespesaReceita;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("CodDespesaReceitaTipo", $oDespesaReceita->getCodDespesaReceitaTipo(), "number", "y");
			//$oValidate->add_text_field("Descricao", $oDespesaReceita->getDescricao(), "text", "y");
			//$oValidate->add_text_field("IncluidoPor", $oDespesaReceita->getIncluidoPor(), "text", "y");
			//$oValidate->add_text_field("AlteradoPor", $oDespesaReceita->getAlteradoPor(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oDespesaReceita->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=DespesaReceita.preparaFormulario&sOP=".$sOP."&nIdDespesaReceita=".$_POST['fCodDespesaReceita']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirDespesaReceita($oDespesaReceita)){
 					unset($_SESSION['oDespesaReceita']);
 					$_SESSION['sMsg'] = "Despesa X Receita inserido com sucesso!";
 					$sHeader = "?bErro=0&action=DespesaReceita.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Despesa X Receita!";
 					$sHeader = "?bErro=1&action=DespesaReceita.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarDespesaReceita($oDespesaReceita)){
 					unset($_SESSION['oDespesaReceita']);
 					$_SESSION['sMsg'] = "Despesa X Receita alterado com sucesso!";
 					$sHeader = "?bErro=0&action=DespesaReceita.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Despesa X Receita!";
 					$sHeader = "?bErro=1&action=DespesaReceita.preparaFormulario&sOP=".$sOP."&nIdDespesaReceita=".$_POST['fCodDespesaReceita']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiDespesaReceita = explode("____",$_REQUEST['fIdDespesaReceita']);
   				foreach($vIdPaiDespesaReceita as $vIdFilhoDespesaReceita){
  					$vIdDespesaReceita = explode("||",$vIdFilhoDespesaReceita);
 					foreach($vIdDespesaReceita as $nIdDespesaReceita){
  						$bResultado &= $oFachada->excluirDespesaReceita($vIdDespesaReceita[0],$vIdDespesaReceita[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Despesa X Receita(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=DespesaReceita.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Despesa X Receita!";
 					$sHeader = "?bErro=1&action=DespesaReceita.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
