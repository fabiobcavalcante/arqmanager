<?php
class PropostaFracionadaCTR implements IControle{

    public function preparaLista(){
        

        $oFachada = new FachadaPrincipalBD();
        if($_GET['sOP'] ==='Fracionamento'){
            $voPropostaFracionada = $oFachada->recuperarTodosPropostaFracionadaPorProposta($_REQUEST['nIdProposta']);
            $_REQUEST['voPropostaFracionada'] = $voPropostaFracionada;
            include_once("view/principal/proposta_fracionada/lista_fracionamento.php");
        }else{
            $voPropostaFracionada = $oFachada->recuperarTodosPropostaFracionada();
            $_REQUEST['voPropostaFracionada'] = $voPropostaFracionada;
            include_once("view/principal/proposta_fracionada/index.php");

        }
        exit();

    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $oPropostaFracionada = false;

        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
            $nIdPropostaFracionada = ($_POST['fIdPropostaFracionada'][0]) ? $_POST['fIdPropostaFracionada'][0] : $_GET['nIdPropostaFracionada'];

            if($nIdPropostaFracionada){
                $vIdPropostaFracionada = explode("||",$nIdPropostaFracionada);
                $oPropostaFracionada = $oFachada->recuperarUmPropostaFracionada($vIdPropostaFracionada[0]);
            }
        }

        $_REQUEST['oPropostaFracionada'] = ($_SESSION['oPropostaFracionada']) ? $_SESSION['oPropostaFracionada'] : $oPropostaFracionada;
        unset($_SESSION['oPropostaFracionada']);

        if($_REQUEST['sOP'] == "Detalhar")
            include_once("view/principal/proposta_fracionada/detalhe.php");
        else
            include_once("view/principal/proposta_fracionada/insere_altera.php");

        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

        if($sOP != "Excluir"){
            $oPropostaFracionada = $oFachada->inicializarPropostaFracionada($_POST['fCodFracionamento'],$_POST['fCodProposta'],$_POST['fNumeroEntrega'],$_POST['fPercentual'],$_POST['fDataPrevista'],$_POST['fDataEfetivacao'],$_POST['fValor'],$_POST['fIndiceCorrecao'],$_POST['fValorFinal']);
            $_SESSION['oPropostaFracionada'] = $oPropostaFracionada;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            $oValidate->add_number_field("Proposta", $oPropostaFracionada->getCodProposta(), "number", "y");
            $oValidate->add_number_field("Numero", $oPropostaFracionada->getNumeroEntrega(), "number", "y");
            $oValidate->add_number_field("Percentual", $oPropostaFracionada->getPercentual(), "number", "y");
//            $oValidate->add_date_field("Data Prevista", $oPropostaFracionada->getDataPrevista(), "date", "y");
            //$oValidate->add_date_field("DataEfetivacao", $oPropostaFracionada->getDataEfetivacao(), "date", "y");
            $oValidate->add_number_field("Valor", $oPropostaFracionada->getValor(), "number", "y");
            $oValidate->add_number_field("Indice", $oPropostaFracionada->getIndiceCorrecao(), "number", "y");
            //$oValidate->add_number_field("ValorFinal", $oPropostaFracionada->getValorFinal(), "number", "y");


            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=PropostaFracionada.preparaFormulario&sOP=".$sOP."&nIdPropostaFracionada=".$_POST['fCodFracionamento']."";
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirPropostaFracionada($oPropostaFracionada)){
                    unset($_SESSION['oPropostaFracionada']);
                    $_SESSION['sMsg'] = "Entregas inserido com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaFracionada.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Entregas!";
                    $sHeader = "?bErro=1&action=PropostaFracionada.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Alterar":
                if($oFachada->alterarPropostaFracionada($oPropostaFracionada)){
                    unset($_SESSION['oPropostaFracionada']);
                    $_SESSION['sMsg'] = "Entregas alterado com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaFracionada.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Entregas!";
                    $sHeader = "?bErro=1&action=PropostaFracionada.preparaFormulario&sOP=".$sOP."&nIdPropostaFracionada=".$_POST['fCodFracionamento']."";
                }
                break;
            case "Excluir":
                $bResultado = true;

                $vIdPaiPropostaFracionada = explode("____",$_REQUEST['fIdPropostaFracionada']);
                foreach($vIdPaiPropostaFracionada as $vIdFilhoPropostaFracionada){
                    $vIdPropostaFracionada = explode("||",$vIdFilhoPropostaFracionada);
                    foreach($vIdPropostaFracionada as $nIdPropostaFracionada){
                        $bResultado &= $oFachada->excluirPropostaFracionada($vIdPropostaFracionada[0],$vIdPropostaFracionada[1]);
                    }
                }

                if($bResultado){
                    $_SESSION['sMsg'] = "Entregas(s) exclu&iacute;do(s) com sucesso!";
                    $sHeader = "?bErro=0&action=PropostaFracionada.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Entregas!";
                    $sHeader = "?bErro=1&action=PropostaFracionada.preparaLista";
                }
                break;
        }
        header("Location: ".$sHeader);
    }
}
?>