<?php
class DocumentoCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $voDocumento = $oFachada->recuperarTodosDocumento();
        $_REQUEST['voDocumento'] = $voDocumento;

        include_once("view/principal/documento/index.php");
        exit();

    }

    public function preparaDocumento(){
        $oFachada = new FachadaPrincipalBD();
        $oDocumento = $oFachada->recuperarUmDocumento($_REQUEST['nCodDocumento']);
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $oEscritorio = $_SESSION['oEscritorio'];

        if(!$oDocumento){
            $_SESSION['sMsg'] = "Não existe modelo de {$oDocumento->getTitulo()} definido para ". $_SESSION['oEscritorio']->getNomeFantasia();
            $sHeader = "?bErro=1&action=Proposta.preparaLista";
            header("Location: ".$sHeader);
            die();
        }

        switch($oDocumento->getCodDocumento()) {
            case 1: //Proposta
                $nCodProposta = $_REQUEST['nCodProposta'];
                $_REQUEST['oProposta'] = $oProposta = $oFachada->recuperarUmProposta($nCodProposta);
                $voPropostaMicroServico = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($nCodProposta);
                $sMicroServico = "";

                switch ($_SESSION['oEscritorio']->getIdEscritorio()) {
                    case 1:
                        foreach ($voPropostaMicroServico as $oMicroServico) {
                            $sMicroServico .= "<li>
							                <em>
							                    {$oMicroServico->getCodProjeto()}";

                            if ($oMicroServico->getQuantidade() > 0) {
                                $sMicroServico .= " QTDE. {$oMicroServico->getQuantidade()}" . (($oMicroServico->getDescricaoQuantidade()) ? " (" . $oMicroServico->getDescricaoQuantidade() . ")" : "");
                            } elseif ($oMicroServico->getDescricaoQuantidade()) {
                                $sMicroServico .= " (" . $oMicroServico->getDescricaoQuantidade() . ")";
                            }
                            $sMicroServico .= ";
							                </em>
							            </li>";
                        }

                        $voProjetoServicoEtapa = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta($nCodProposta);
                        $nEtapa = 1;
                        $sEtapaAnt = "";
                        $sEntregas = "";

                        foreach ($voProjetoServicoEtapa as $oEtapa) {
                            $sEntregas .= $nEtapa . "ª Etapa: ";
                            if ($oEtapa->getPrazo() > 0) {
                                $sEntregas .= $oEtapa->getServicoEtapa()->getDescricao() . ", " . $oEtapa->getPrazo() . " dias a contar da ";
                            } else {
                                $sEntregas .= $oEtapa->getServicoEtapa()->getDescricao();
                            }
                            $sEntregas .= ($nEtapa == 1) ? "data de assinatura do contrato" : (($oEtapa->getPrazo() > 0) ? " aprovação do(a) {$sEtapaAnt}" : " - a combinar ");
                            $sEntregas .= ";<br>";
                            $nEtapa++;
                            $sEtapaAnt = $oEtapa->getServicoEtapa()->getDescricao();
                        }

                        if ($oProposta->getVisitasIncluidas() > 0) {
                            $sObservacao1 = "Está incluso no presente orçamento {$oProposta->getVisitasIncluidas()} visitas técnicas ao local com agendamento prévio. Para mais visitas poderá ser cobrada hora técnica no valor de {$_SESSION['oEscritorio']->getHoraTecnica()}";
                        } else {
                            $sObservacao1 = "Visitas técnicas serão custeadas e acordadas com o cliente.";
                        }

                        $oDocumentoEscritorio = $oFachada->recuperarUmDocumentoEscritorio($oDocumento->getCodDocumento(), $nIdEscritorio);
                        if ($oDocumentoEscritorio) {
                            $sConteudo = $oDocumentoEscritorio->getTemplate();

                            $sConteudo = str_replace("#LOGOMARCA#", "{$_SESSION['oEscritorio']->getLogomarca()} ", $sConteudo);
                            $sConteudo = str_replace("#SERVICO#", mb_strtoupper($oProposta->getServico()->getDescServico(), 'utf8'), $sConteudo);
                            $sConteudo = str_replace("#NOME#", "{$oProposta->getNome()} ", $sConteudo);
                            $sConteudo = str_replace("#DESCRICAO#", "{$oProposta->getDescricao()} ", $sConteudo);
                            $sConteudo = str_replace("#MICROSERVICOS#", "{$sMicroServico} ", $sConteudo);
                            $sConteudo = str_replace("#VALOR_FORMATADO#", "{$oProposta->getValorPropostaFormatado()} ", $sConteudo);
                            $sConteudo = str_replace("#VALOR_EXTENSO#", strtoupper($oProposta->getValorPropostaFormatadoExtenso()), $sConteudo);
                            $sConteudo = str_replace("#VALOR_AVISTA#", "{$oProposta->getValorAvistaFormatado()} ", $sConteudo);
                            $sConteudo = str_replace("#VALOR_APRAZO#", "{$oProposta->getValorPropostaFormatado()} ", $sConteudo);
                            $sConteudo = str_replace("#FORMA_PARCELAMENTO#", "{$oProposta->getValorParcelaAprazo()} ", $sConteudo);
                            $sConteudo = str_replace("#ENTREGAS#", "{$sEntregas} ", $sConteudo);
                            $sConteudo = str_replace("#OBS1#", "{$sObservacao1} ", $sConteudo);
                            $_REQUEST['Conteudo'] = htmlspecialchars($sConteudo);
                        }
                        break;
                    case 2:
                        if ($voPropostaMicroServico) {
                            $sMicroServico .= "<ol style='list-style-type: upper-alpha;'>";
                            foreach ($voPropostaMicroServico as $oMicroServico) {
                                $sMicroServico .= "<li>{$oMicroServico->getCodProjeto()}";
                                if ($oMicroServico->getQuantidade() > 0)
                                    $sMicroServico .= " Qtde. {$oMicroServico->getQuantidade()}";
                                $sMicroServico .= ";</li>";
                            }
                            $sMicroServico .= "</ol>";
                        }

                        if ($oProposta->getEntregaParcial() == "Sim"){
                            $voPropostaFracionada = $oFachada->recuperarTodosPropostaFracionadaPorProposta($oProposta->getCodProposta());
                            $sEntregaParcial = "<p>";
                            foreach ($voPropostaFracionada as $oPropostaFracionada) {
                                $sNumero = ($oPropostaFracionada->getNumeroEntrega()) ? "<strong>Etapa {$oPropostaFracionada->getNumeroEntrega()}: </strong>" : "";
                                $sDescricao = ($oPropostaFracionada->getDescricao()) ? "{$oPropostaFracionada->getDescricao()}" : "";
                                $sPercentual = ($oPropostaFracionada->getPercentual()) ? " que corresponde a {$oPropostaFracionada->getPercentualFormatado()}% do valor total" : "";
                                $sValor = ($oPropostaFracionada->getValor()) ? ": R$ {$oPropostaFracionada->getValorFormatado()} ({$oPropostaFracionada->getValorFormatadoExtenso()})" : "";
                                $sParcelas = ($oPropostaFracionada->getParcelas()) ? " que devem ser pagos em {$oPropostaFracionada->getParcelas()} parcelas" : "";

                                $sEntregaParcial .= "$sNumero$sDescricao$sPercentual$sValor$sParcelas.</p>";
                            }
                        } else
                            $sEntregaParcial = ($oProposta->getFormaPagamento())?:" A combinar.";

                        $oDocumentoEscritorio = $oFachada->recuperarUmDocumentoEscritorio($oDocumento->getCodDocumento(), $nIdEscritorio);
                        $dDataFormatada = strftime('%d de %B de %Y', strtotime($oProposta->getDataProposta()));

                        if ($oDocumentoEscritorio) {
                            $sConteudo = $oDocumentoEscritorio->getTemplate();
                            $sConteudo = str_replace("#DATACOMPLETA#", "Belém,".$dDataFormatada, $sConteudo);
                            $sConteudo = str_replace("#NOME#", "{$oProposta->getNome()} ", $sConteudo);
                            $sConteudo = str_replace("#SERVICO#", mb_strtoupper($oProposta->getServico()->getDescServico(), 'utf8'), $sConteudo);
                            $sConteudo = str_replace("#DESCRICAO#", "{$oProposta->getDescricao()} ", $sConteudo);
                            $sConteudo = str_replace("#MICROSERVICOS#", "{$sMicroServico} ", $sConteudo);
                            $sConteudo = str_replace("#VALOR_FORMATADO#", "{$oProposta->getValorPropostaFormatado()} ", $sConteudo);
                            $sConteudo = str_replace("#VALOR_EXTENSO#", strtoupper($oProposta->getValorPropostaFormatadoExtenso()), $sConteudo);
                            $sConteudo = str_replace("#FORMA_DE_PAGAMENTO#", "$sEntregaParcial ", $sConteudo);
                            $sConteudo = str_replace("#PRAZO#", "{$oProposta->getPrazo()} ", $sConteudo);


                            // $sConteudo = str_replace("#OBS1#", "{$sObservacao1} ", $sConteudo);
                            $sConteudo = str_replace("#ASSINATURA#", "{$_SESSION['oEscritorio']->getAssinaturaFormatada()}", $sConteudo);
                            $_REQUEST['Conteudo'] = htmlspecialchars($sConteudo);
                        }
                        break;
                } // switch case escritorio proposta.

                include_once("view/principal/documento/proposta.php");

                break;
            case 2: //Contrato
                $nCodProjeto = ($_POST['fIdProjeto'][0]) ? $_POST['fIdProjeto'][0] : $_GET['nCodProjeto'];
                $oProjeto = $oFachada->recuperarUmProjeto($nCodProjeto);
                $oProposta = $oFachada->recuperarUmProposta($oProjeto->getCodProposta());
                $voProjetoServicoEtapa = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta(null,$nCodProjeto);
                $_REQUEST['voPagamento'] = $oFachada->recuperarTodosProjetoPagamento($nCodProjeto);
                $_REQUEST['nPagina']=1;
                $oCliente = $oProjeto->getCliente();
                $sEnderecoCliente = $oCliente->getLogradouro() . ",".$oCliente->getNumero() . ". ". ($oCliente->getComplemento() ? $oCliente->getComplemento() :"") . ". ".$oCliente->getBairro().". ". $oCliente->getCidade() . "/". $oCliente->getUf();

                //inicio descricao_microservico
                $voProjetoServicoMicroServico = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($oProjeto->getCodProposta());
                $sDescricaoMicroServico = "";
                if($voProjetoServicoMicroServico){
                    foreach($voProjetoServicoMicroServico as $oMicroServico){
                        $sDescricaoMicroServico .= $oMicroServico->getServicoMicroservico()->getDescricao();
                        if($oMicroServico->getQuantidade() >0){
                            $sDescricaoMicroServico .= " QTDE. " . $oMicroServico->getQuantidade() . (($oMicroServico->getDescricaoQuantidade()) ? " (".$oMicroServico->getDescricaoQuantidade()."); " : "");
                        }elseif($oMicroServico->getDescricaoQuantidade()){
                            $sDescricaoMicroServico.= " (".$oMicroServico->getDescricaoQuantidade()."); ";
                        }else{
                            $sDescricaoMicroServico .= "; ";
                        }
                    } //foreach($voProjetoServicoMicroServico as $oMicroServico){
                    $sDescricaoMicroServico = substr($sDescricaoMicroServico,0,-2);
                }
                //fim microservico

                $voEtapaProjeto = [];
                $nEtapa=1;
                $sEtapaAnt = "";

                foreach($voProjetoServicoEtapa as $oEtapa){
                    $sDescricaoEtapa .= "<p style='text-align: justify'><strong>{$nEtapa}. </strong>";
                    if($oEtapa->getPrazo() > 0){
                        $sDescricaoEtapa .= "<strong>{$oEtapa->getServicoEtapa()->getDescricao()}:</strong> ";
                        $sDescricaoEtapa .= $oEtapa->getServicoEtapa()->getDescricaoContrato();
                        if (strpos($oEtapa->getServicoEtapa()->getDescricao(),"EXECUTIVO") !== false) {
                            $sDescricaoEtapa .=  $sDescricaoMicroServico;
                        }

                        $sDescricaoEtapa .= " com prazo de entrega de {$oEtapa->getPrazo()} dias úteis a contar da ";
                    }else {
                        $sDescricaoEtapa .= "<strong>{$oEtapa->getServicoEtapa()->getDescricao()}:</strong> {$oEtapa->getServicoEtapa()->getDescricaoContrato()}";
                    }
                    $sDescricaoEtapa .= ($nEtapa ==1) ? "data de assinatura do contrato" : (($oEtapa->getPrazo() > 0)? " aprovação do(a) {$sEtapaAnt}":" - a combinar ");
                    $sDescricaoEtapa .= ".</p>";

                    $sEtapaAnt = $oEtapa->getServicoEtapa()->getDescricao();
                    $nEtapa++;
                }
                if($voEtapaProjeto){
                    $i=1;
                    foreach($voEtapaProjeto as $oEtapaProjeto){
                        $sDescricaoEtapa.= "<p style='text-align: justify;'><strong>{$i}. {$oEtapaProjeto->getDescricao()}</strong>";
                        $i++;
                        echo "{$sDescricaoEtapa}<hr>";
                    }
                }
                // fim descricao_etapa

                $voPagamento = $_REQUEST['voPagamento'];
                $nTotal = 0;
                $nParcelas = count($voPagamento);
                foreach ($voPagamento as $oPagamento) {
                    $nTotal += $oPagamento->getValor();
                }
                if($nParcelas == 1){
                    $sParcelas = "valor que será pago integralmente na assinatura do contrato";
                    $sFORMA_DESCRICAO_PAGAMENTO=" <tr>
                      <td style='border: 1px solid #000000; padding:4px; margin:4px;'>
                          <strong>À VISTA: R$ {$voPagamento[0]->getValorFormatado()}</strong>
                      </td>
                  </tr>";
                }else{
                    $sParcelas = "valor este que será pago da seguinte forma:R$".$voPagamento[0]->getValorFormatado()." (".Extenso::valorPorExtenso($voPagamento[0]->getValorFormatado()) ."na assinatura do contrato, e as demais parcelas pagas nas datas estipuladas abaixo até a quitação total do valor do contrato";

                    $sFORMA_DESCRICAO_PAGAMENTO=" <tr>
                      <th style='border: 1px solid #000000; padding:4px; margin:4px;'>Parcela</th>
                      <th style='border: 1px solid #000000; padding:4px; margin:4px;'>Valor</th>
                      <th style='border: 1px solid #000000; padding:4px; margin:4px;'>Vencimento</th>
                  </tr>";
                    $nSum = 1;
                    foreach ($voPagamento as $oPagamento){
                        $sFORMA_DESCRICAO_PAGAMENTO.="<tr>
                          <td style='border: 1px solid #000000; padding:4px; margin:4px;'>".$nSum++."</td>
                          <td style='border: 1px solid #000000; padding:4px; margin:4px;'><strong>R$ {$oPagamento->getValorFormatado()}</strong>(".Extenso::valorPorExtenso($oPagamento->getValorFormatado()).")</td>
                          <td style='border: 1px solid #000000; padding:4px; margin:4px;'>{$oPagamento->getDataPrevisaoFormatado()}</td>
                      </tr>";
                    }

                }


                if($oProposta->getVisitasIncluidas() > 0){
                    $sVisitas ="As visitas técnicas serão realizadas em número de {$oProposta->getVisitasIncluidas()} visitas ao local de execução dos projetos, previamente agendadas. Havendo necessidade de mais visitas por interesse da CONTRATANTE será cobrada pela CONTRATADA hora técnica no valor de R$#HORA_TECNICA# por visita adicional. Poderá, ainda, a critério da CONTRATANTE, ser contratado o serviço de ACOMPANHAMENTO PROGRAMADO, sendo, neste caso, cobrado o valor de 01 (um) salário mínimo por mês com direito a duas visitas técnicas semanais.";
                }else{
                    $sVisitas ="Visitas técnicas serão custeadas e acordadas com o cliente.";
                }

                $oDocumentoEscritorio = $oFachada->recuperarUmDocumentoEscritorio($oDocumento->getCodDocumento(),$nIdEscritorio);

                if($oDocumentoEscritorio){
                    $sConteudo = $oDocumentoEscritorio->getTemplate();
                }

                $sConteudo = str_replace("#SERVICO#",mb_strtoupper($oProposta->getServico()->getDescServico(),"utf8"),$sConteudo);
                $sConteudo = str_replace("#CLIENTE#",mb_strtoupper($oCliente->getNome(),"utf8"),$sConteudo);
                $sConteudo = str_replace("#RAZAO_SOCIAL#",mb_strtoupper($_SESSION['oEscritorio']->getRazaoSocial()),$sConteudo);
                $sConteudo = str_replace("#IDENTIFICACAO#","{$oCliente->getIdentificacao()} ",$sConteudo);
                $sConteudo = str_replace("#ENDERECO_CLIENTE#","{$sEnderecoCliente} ",$sConteudo);
                $sConteudo = str_replace("#ESCRITORIO_DOCUMENTO#","{$_SESSION['oEscritorio']->getDocumento()} ",$sConteudo);
                $sConteudo = str_replace("#ESCRITORIO_ENDERECO#","{$_SESSION['oEscritorio']->getEnderecoCompleto()} ",$sConteudo);
                $sConteudo = str_replace("#MICROSERVICOS#","{$sDescricaoMicroServico} ",$sConteudo);
                $sConteudo = str_replace("#PROPOSTA_DESCRICAO#","{$oProposta->getDescricao()} ",$sConteudo);
                $sConteudo = str_replace("#ETAPAS#","{$sDescricaoEtapa} ",$sConteudo);
                $sConteudo = str_replace("#VISITAS#","{$sVisitas} ",$sConteudo);
                $sConteudo = str_replace("#HORA_TECNICA#","{$_SESSION['oEscritorio']->getHoraTecnica()} ",$sConteudo);
                $sConteudo = str_replace("#VALOR_CONTRATO#",number_format($nTotal,2,",","."),$sConteudo);
                $sConteudo = str_replace("#VALOR_EXTENSO#",Extenso::valorPorExtenso($nTotal),$sConteudo);
                $sConteudo = str_replace("#PARCELAMENTO#",$sParcelas,$sConteudo);
                $sConteudo = str_replace("#FORMA_DESCRICAO_PAGAMENTO#",$sFORMA_DESCRICAO_PAGAMENTO,$sConteudo);
                $sConteudo = str_replace("#DATA_CONTRATO#",ucwords(utf8_encode(strftime("%d de %B de %Y", strtotime(date($oProjeto->getDataInicio()))))),$sConteudo);
                if($_SESSION['oEscritorio']->getAssinatura())
                    $sConteudo = str_replace("#ASSINATURA#","{$_SESSION['oEscritorio']->getAssinaturaFormatada()}",$sConteudo);
                else
                    $sConteudo = str_replace("#ASSINATURA#","",$sConteudo);
                $sConteudo = str_replace("#CIDADE_UF#",$_SESSION['oEscritorio']->getCidadeUf(),$sConteudo);

                $_REQUEST['Conteudo'] = htmlspecialchars($sConteudo);

                include_once("view/principal/documento/contrato.php");
                die();
                break;
            case 3: //Recibo
                $nCodProjetoPagamento = $_REQUEST['nCodProjetoPagamento'];
                $oProjetoPagamento = $oFachada->recuperarUmProjetoPagamento($nCodProjetoPagamento);
                $oProjeto = $oProjetoPagamento->getProjeto();
                $oCliente = $oProjeto->getCliente();
                $oProposta = $oProjeto->getProposta();
                $_REQUEST['oProjetoPagamento'] = $_REQUEST['ProjetoPagamento'];
                $_REQUEST['oProjeto'] = $oProjeto;
                $_REQUEST['oCliente'] = $oCliente;
                $_REQUEST['oProposta'] = $oProposta;
                include_once("view/principal/documento/recibo.php");
                die();
                break;
            case 4: //Dados RRT
                $nCodProjeto = ($_POST['fIdProjeto'][0]) ? $_POST['fIdProjeto'][0] : $_GET['nCodProjeto'];
                $oProjeto = $oFachada->recuperarUmProjeto($nCodProjeto);
                $oCliente = $oProjeto->getCliente();
                $oProposta = $oFachada->recuperarUmProposta($oProjeto->getCodProposta());
                $_REQUEST['voProjetoServicoMicroServico']  = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($oProjeto->getCodProposta());
                $_REQUEST['voProjetoServicoEtapa'] = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta(null,$nCodProjeto);
                $_REQUEST['voPagamento'] = $oFachada->recuperarTodosProjetoPagamento($nCodProjeto);
                $_REQUEST['oProjeto'] = $oProjeto;
                $_REQUEST['oProposta'] = $oProposta;
                $_REQUEST['oCliente'] = $oCliente;
                $_REQUEST['nPagina']=1;

                include_once("view/principal/documento/rrt.php");
                die();
                break;
            case 5: //Termo de Entrega

                switch ($oEscritorio->getIdEscritorio()) {
                    case 1:

                        $nCodProjeto = ($_REQUEST['fIdProjeto'][0]) ?: $_REQUEST['nCodProjeto'];
                        $_REQUEST['oProjeto'] = $oProjeto = $oFachada->recuperarUmProjeto($nCodProjeto);
                        $nCodProposta = $oProjeto->getCodProposta();
                        $_REQUEST['oProposta'] = $oProposta = $oFachada->recuperarUmProposta($nCodProposta);

                        $oDocumentoEscritorio = $oFachada->recuperarUmDocumentoEscritorio($oDocumento->getCodDocumento(),$nIdEscritorio);
                        $dDataFormatada = strftime('%d de %B de %Y', strtotime($oProjeto->getDataFim()));
                        $voPropostaMicroServico = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($nCodProposta);
                        $sMicroServico = "";

                        if ($voPropostaMicroServico) {
                            $sMicroServico .= "<ol style=\"list-style-type: lower-alpha;\">";
                            foreach ($voPropostaMicroServico as $oMicroServico) {
                                $sMicroServico .= "<li>{$oMicroServico->getCodProjeto()}";
                                if ($oMicroServico->getQuantidade() > 0)
                                    $sMicroServico .= " Qtde. {$oMicroServico->getQuantidade()}";
                                $sMicroServico .= ";</li>";
                            }
                            $sMicroServico .= "</ol>";
                        }

                        if($oDocumentoEscritorio){
                            $sConteudo = $oDocumentoEscritorio->getTemplate();
                            $sConteudo = str_replace("#DATACOMPLETA#", "Belém, ".$dDataFormatada, $sConteudo);
                            $sConteudo = str_replace("#NOME#", "{$oProposta->getNome()} ", $sConteudo);
                            $sConteudo = str_replace("#SERVICO#", mb_strtoupper($oProposta->getServico()->getDescServico(), 'utf8'), $sConteudo);
                            $sConteudo = str_replace("#DESCRICAO#", "{$oProposta->getDescricao()} ", $sConteudo);
                            $sConteudo = str_replace("#MICROSERVICOS#", "{$sMicroServico} ", $sConteudo);

                            $_REQUEST['Conteudo'] = htmlspecialchars($sConteudo);
                        }
                    case 2:
                        $nCodProjeto = ($_REQUEST['fIdProjeto'][0]) ?: $_REQUEST['nCodProjeto'];
                        $_REQUEST['oProjeto'] = $oProjeto = $oFachada->recuperarUmProjeto($nCodProjeto);
                        $nCodProposta = $oProjeto->getCodProposta();
                        $_REQUEST['oProposta'] = $oProposta = $oFachada->recuperarUmProposta($nCodProposta);

                        $oDocumentoEscritorio = $oFachada->recuperarUmDocumentoEscritorio($oDocumento->getCodDocumento(),$nIdEscritorio);
                        $dDataFormatada = strftime('%d de %B de %Y', strtotime($oProjeto->getDataFim()));
                        $voPropostaMicroServico = $oFachada->recuperarTodosPropostaMicroServicoPorProposta($nCodProposta);
                        $sMicroServico = "";

                        if ($voPropostaMicroServico) {
                            $sMicroServico .= "<ol style=\"list-style-type: lower-alpha;\">";
                            foreach ($voPropostaMicroServico as $oMicroServico) {
                                $sMicroServico .= "<li>{$oMicroServico->getCodProjeto()}";
                                if ($oMicroServico->getQuantidade() > 0)
                                    $sMicroServico .= " Qtde. {$oMicroServico->getQuantidade()}";
                                $sMicroServico .= ";</li>";
                            }
                            $sMicroServico .= "</ol>";
                        }

                        if($oDocumentoEscritorio){
                            $sConteudo = $oDocumentoEscritorio->getTemplate();
                            $sConteudo = str_replace("#DATACOMPLETA#", "Belém, ".$dDataFormatada, $sConteudo);
                            $sConteudo = str_replace("#NOME#", "{$oProposta->getNome()} ", $sConteudo);
                            $sConteudo = str_replace("#SERVICO#", mb_strtoupper($oProposta->getServico()->getDescServico(), 'utf8'), $sConteudo);
                            $sConteudo = str_replace("#DESCRICAO#", "{$oProposta->getDescricao()} ", $sConteudo);
                            $sConteudo = str_replace("#MICROSERVICOS#", "{$sMicroServico} ", $sConteudo);

                            $_REQUEST['Conteudo'] = htmlspecialchars($sConteudo);
                        }
                        break;

                }

                if(!$_REQUEST['oProjeto']->getDataFim()){
                    $_SESSION['sMsg'] = "Não foi possível gerar o Termo de Entrega de [{$_REQUEST['oProjeto']->getCliente()->getNome()}] pois existe(m) etapa(s) a ser(em) concluída(s)!";
                    $sHeader = "?bErro=1&action=Projeto.preparaLista";
                    header("Location: ".$sHeader);
                    die();
                }

                include_once("view/principal/documento/termo_de_entrega.php");
                die();
        }
    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $oDocumento = false;

        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
            $nIdDocumento = ($_POST['fIdDocumento'][0]) ? $_POST['fIdDocumento'][0] : $_GET['nIdDocumento'];

            if($nIdDocumento){
                $vIdDocumento = explode("||",$nIdDocumento);
                $oDocumento = $oFachada->recuperarUmDocumento($vIdDocumento[0]);
            }
        }

        $_REQUEST['oDocumento'] = ($_SESSION['oDocumento']) ? $_SESSION['oDocumento'] : $oDocumento;
        unset($_SESSION['oDocumento']);



        if($_REQUEST['sOP'] == "Detalhar")
            include_once("view/principal/documento/detalhe.php");
        else
            include_once("view/principal/documento/insere_altera.php");

        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

        if($sOP != "Excluir"){
            $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
            $oDocumento = $oFachada->inicializarDocumento($_POST['fCodDocumento'],$_POST['fTitulo'],$_POST['fConteudo'],$_POST['fAtivo'],$nIdEscritorio);
            $_SESSION['oDocumento'] = $oDocumento;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            //$oValidate->add_text_field("Titulo", $oDocumento->getTitulo(), "text", "y");
            //$oValidate->add_text_field("Conteudo", $oDocumento->getConteudo(), "text", "y");
            //$oValidate->add_text_field("Ativo", $oDocumento->getAtivo(), "text", "y");


            if (!$oValidate->validation()) {
                $_SESSION['sMsg'] = $oValidate->create_msg();
                $sHeader = "?bErro=1&action=Documento.preparaFormulario&sOP=".$sOP."&nIdDocumento=".$_POST['fCodDocumento']."";
                header("Location: ".$sHeader);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirDocumento($oDocumento)){
                    unset($_SESSION['oDocumento']);
                    $_SESSION['sMsg'] = "Documento inserido com sucesso!";
                    $sHeader = "?bErro=0&action=Documento.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Documento!";
                    $sHeader = "?bErro=1&action=Documento.preparaFormulario&sOP=".$sOP;
                }
                break;
            case "Alterar":
                if($oFachada->alterarDocumento($oDocumento)){
                    unset($_SESSION['oDocumento']);
                    $_SESSION['sMsg'] = "Documento alterado com sucesso!";
                    $sHeader = "?bErro=0&action=Documento.preparaLista";

                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Documento!";
                    $sHeader = "?bErro=1&action=Documento.preparaFormulario&sOP=".$sOP."&nIdDocumento=".$_POST['fCodDocumento']."";
                }
                break;
            case "Excluir":
                $bResultado = true;

                $vIdPaiDocumento = explode("____",$_REQUEST['fIdDocumento']);
                foreach($vIdPaiDocumento as $vIdFilhoDocumento){
                    $vIdDocumento = explode("||",$vIdFilhoDocumento);
                    foreach($vIdDocumento as $nIdDocumento){
                        $bResultado &= $oFachada->excluirDocumento($vIdDocumento[0],$vIdDocumento[1]);
                    }
                }

                if($bResultado){
                    $_SESSION['sMsg'] = "Documento(s) exclu&iacute;do(s) com sucesso!";
                    $sHeader = "?bErro=0&action=Documento.preparaLista";
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Documento!";
                    $sHeader = "?bErro=1&action=Documento.preparaLista";
                }
                break;
        }

        header("Location: ".$sHeader);

    }

}
