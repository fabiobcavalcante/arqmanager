<?php
class EmailCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $sOP = $_REQUEST['sOP'];
        switch ($sOP){
            case "Entrega":
              $nStatus=0;
              $_REQUEST['voEtapa'] = $oFachada->recuperarTodosProjetoServicoEtapa($nStatus);
            break;
            case "Concluido":
              $nStatus=1;
              $_REQUEST['voEtapa'] = $oFachada->recuperarTodosProjetoServicoEtapa($nStatus);
            break;

        }
        include_once("view/principal/relatorio/entregas.php");
        exit();
    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        exit();

    }


    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
        switch($_REQUEST['sIdTipo']){
            case "3D":
                $oProjetoPagamento = $oFachada->recuperarUmProjetoPagamentoView($_REQUEST['fCodProjetoPagamento']);
                $vdataPrevista = explode("-", $oProjetoPagamento->data_previsao);

                $oMailer = new Mailer();
                $sAssunto = $_SESSION['oEscritorio']->getNomeFantasia();
                if($oMailer->enviar("{$oProjetoPagamento->nome_cliente}", 'bc_fabio@hotmail.com', "[$sAssunto] Lembrete de Vencimento de Fatura", "lembrete3", [
                    "sNome"=>$oProjetoPagamento->nome_cliente,
                    "sParcela"=>$oProjetoPagamento->desc_pagamento,
                    "sProjeto"=>$oProjetoPagamento->titulo,
                    "sVencimento"=>$oProjetoPagamento->data_previsao_formatada,
                    "sDescricao"=>$oProjetoPagamento->desc_pagamento,
                    "sValor"=>$oProjetoPagamento->valor_formatado
                ])){

                    $_SESSION['sMsg'] = "Email enviado com sucesso!";
                    $sHeader = "?bErro=0&action=Relatorio.preparaFinanceiroMensal&sOP=Visualizar&nEfetivado=1&nAno={$vdataPrevista['0']}&nMesSelecionado=".$vdataPrevista['1'];
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel enviar email ao cliente!";
                    $sHeader = "?bErro=0&action=Relatorio.preparaFinanceiroMensal&sOP=Visualizar&nEfetivado=1&nAno={$vdataPrevista['0']}&nMesSelecionado=".$vdataPrevista['1'];
                }
                break;

        }

        header("Location: ".$sHeader);

    }

}
