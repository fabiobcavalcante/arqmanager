<?php
class PlanoContasCTR implements IControle{

	public function preparaLista(){
		$oFachada = new FachadaPrincipalBD();
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

		$voPlanoContas = $oFachada->recuperarTodosPlanoContas($nIdEscritorio);
		$_REQUEST['voPlanoContas'] = $voPlanoContas;

		include_once("view/principal/plano_contas/index.php");
		exit();

	}

	public function preparaFormulario(){
		$oFachada = new FachadaPrincipalBD();

		$oPlanoContas = false;
		$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
			$nIdPlanoContas = ($_POST['fIdPlanoContas'][0]) ?: $_GET['nIdPlanoContas'];

			if($nIdPlanoContas){
				$vIdPlanoContas = explode("||",$nIdPlanoContas);
				$oPlanoContas = $oFachada->recuperarUmPlanoContas($vIdPlanoContas[0]);
			}
		}

		$_REQUEST['oPlanoContas'] = ($_SESSION['oPlanoContas']) ?: $oPlanoContas;
		unset($_SESSION['oPlanoContas']);

		$_REQUEST['voPai'] = $oFachada->recuperarTodosPlanoContas($nIdEscritorio);

		if($_REQUEST['sOP'] == "Detalhar")
			include_once("view/principal/plano_contas/detalhe.php");
		else
			include_once("view/principal/plano_contas/insere_altera.php");

		exit();

	}

	public function processaFormulario(){
		$oFachada = new FachadaPrincipalBD();

		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

		if($sOP != "Excluir"){

			switch($sOP){
				case 'Cadastrar':
					$_POST['fUsuInc'] = $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
					break;
				case 'Alterar':
					if(is_array($_REQUEST['fCodPlanoContas']))
						$_REQUEST['fCodPlanoContas'] = $_POST['fCodPlanoContas'][0];
					$oPPlanoContas1 = $oFachada->recuperarUmPlanoContas($_REQUEST['fCodPlanoContas']);
					$_POST['fUsuInc'] =  $oPPlanoContas1->getUsuInc();
					$_POST['fUsuAlt'] =  $_SESSION['oUsuarioAM']->getLogin() . " || " . date('d/m/Y h:i:s');
					break;

			}


			$nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
			$oPlanoContas = $oFachada->inicializarPlanoContas($_POST['fCodPlanoContas'],$_POST['fCodigo'],$_POST['fDescricao'],$_POST['fVisivel'],$_POST['fUsuInc'],$_POST['fUsuAlt'],1,$_REQUEST['fIdPai'],$nIdEscritorio);
			$_SESSION['oPlanoContas'] = $oPlanoContas;

			$oValidate = FabricaUtilitario::getUtilitario("Validate");
			$oValidate->check_4html = true;

			$oValidate->add_text_field("Codigo", $oPlanoContas->getCodigo(), "text", "y");
			$oValidate->add_text_field("Descricao", $oPlanoContas->getDescricao(), "text", "y");
			$oValidate->add_number_field("Visivel", $oPlanoContas->getVisivel(), "number", "y");
			$oValidate->add_text_field("UsuInc", $oPlanoContas->getUsuInc(), "text", "y");
			if($sOP == 'Alterar')
				$oValidate->add_text_field("UsuAlt", $oPlanoContas->getUsuAlt(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oPlanoContas->getAtivo(), "number", "y");


			if (!$oValidate->validation()) {
				$_SESSION['sMsg'] = $oValidate->create_msg();
				$sHeader = "?bErro=1&action=PlanoContas.preparaFormulario&sOP=".$sOP."&nIdPlanoContas=".$_POST['fCodPlanoContas']."";
				header("Location: ".$sHeader);
				die();
			}
		}

		switch($sOP){
			case "Cadastrar":
				if($oFachada->inserirPlanoContas($oPlanoContas)){
					unset($_SESSION['oPlanoContas']);
					$_SESSION['sMsg'] = "Plano de Contas inserido com sucesso!";
					$sHeader = "?bErro=0&action=PlanoContas.preparaLista";

				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Plano de Contas!";
					$sHeader = "?bErro=1&action=PlanoContas.preparaFormulario&sOP=".$sOP;
				}
				break;
			case "Alterar":
				if($oFachada->alterarPlanoContas($oPlanoContas)){
					unset($_SESSION['oPlanoContas']);
					$_SESSION['sMsg'] = "Plano de Contas alterado com sucesso!";
					$sHeader = "?bErro=0&action=PlanoContas.preparaLista";

				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Plano de Contas!";
					$sHeader = "?bErro=1&action=PlanoContas.preparaFormulario&sOP=".$sOP."&nIdPlanoContas=".$_POST['fCodPlanoContas']."";
				}
				break;
			case "Excluir":
				$bResultado = true;

				$vIdPaiPlanoContas = explode("____",$_REQUEST['fIdPlanoContas']);
				foreach($vIdPaiPlanoContas as $vIdFilhoPlanoContas){
					$vIdPlanoContas = explode("||",$vIdFilhoPlanoContas);
					foreach($vIdPlanoContas as $nIdPlanoContas){
						$bResultado &= $oFachada->excluirPlanoContas($vIdPlanoContas[0],$vIdPlanoContas[1]);
					}
				}

				if($bResultado){
					$_SESSION['sMsg'] = "Plano de Contas(s) exclu&iacute;do(s) com sucesso!";
					$sHeader = "?bErro=0&action=PlanoContas.preparaLista";
				} else {
					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Plano de Contas!";
					$sHeader = "?bErro=1&action=PlanoContas.preparaLista";
				}
				break;
		}

		header("Location: ".$sHeader);

	}

}