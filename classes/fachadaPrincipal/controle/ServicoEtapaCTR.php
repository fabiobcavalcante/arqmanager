<?php
class ServicoEtapaCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();

        $nCodServico = isset($_POST['fIdServico']) ? $_POST['fIdServico'][0] : $_GET['nIdServico'];
        $_REQUEST['vSelecaoEtapa'] = null;
        $sOP = $_REQUEST['sOP'] ?? null;

        if($sOP == "Servico" || ($nCodServico)){
            $_REQUEST['sOP2'] = isset($_GET['sOP2']) ?? null;
            $_REQUEST['oServico'] = $oFachada->recuperarUmServico($nCodServico);
            $voServicoEtapa = $oFachada->recuperarTodosServicoEtapaPorServico($nCodServico);
            $_REQUEST['voServicoEtapa'] = $voServicoEtapa;
            $nCodProposta = $_REQUEST['nCodProposta'] ?? null;

            if($nCodProposta){
                $voServicoEtapaSelecionados = $oFachada->recuperarTodosPropostaServicoEtapaPorProposta($nCodProposta);
                $vSelecaoEtapa = array();
                if($voServicoEtapaSelecionados){
                    foreach($voServicoEtapaSelecionados as $oSelecao){
                        $vSelecaoEtapa[$oSelecao->getCodEtapa()] = [$oSelecao->getCodEtapa(),$oSelecao->getDescricao(),$oSelecao->getPrazo(),$oSelecao->getDataPrevistaFormatado()];
                    }
                }
                $_REQUEST['vSelecaoEtapa'] = $vSelecaoEtapa;
            }
            if($sOP == "Servico"){
                include_once("view/principal/servico_etapa/lista_servico.php");
            } else{
                include_once("view/principal/servico_etapa/index.php");
            }
        }
        exit();
    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

        $oServicoEtapa = false;
        $_REQUEST['oServico'] = $oFachada->recuperarUmServico($_GET['nCodServico']);
        if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
            $oServicoEtapa = $oFachada->recuperarUmServicoEtapa($_GET['nCodServicoEtapa']);
        }

        $_REQUEST['oServicoEtapa'] = $_SESSION['oServicoEtapa'] ?? $oServicoEtapa;
        unset($_SESSION['oServicoEtapa']);

        if($_REQUEST['sOP'] == "Detalhar")
            include_once("view/principal/servico_etapa/detalhe.php");
        else {
            $_REQUEST['voEtapa'] = $oFachada->recuperarTodosEtapa($nIdEscritorio);
            include_once("view/principal/servico_etapa/insere_altera.php");
        }

        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();

        $sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

        if($sOP != "Excluir"){
            $_POST['fDescricaoContrato'] = ($_POST['fDescricaoContrato'])?:NULL;
            $oServicoEtapa = $oFachada->inicializarServicoEtapa($_POST['fCodServicoEtapa'],$_POST['fDescricao'],$_POST['fDescricaoContrato'],$_POST['fOrdem'],$_POST['fCodServico'],$_POST['fCodEtapa']);
            $_SESSION['oServicoEtapa'] = $oServicoEtapa;

            $oValidate = FabricaUtilitario::getUtilitario("Validate");
            $oValidate->check_4html = true;

            if (!$oValidate->validation()) {
                echo json_encode([
                    'sMsg'=>$oValidate->create_msg(),
                    'sClass'=>'error',
                    'nCodServico'=>$_POST['fCodServico']
                ]);
                die();
            }
        }

        switch($sOP){
            case "Cadastrar":
                if($oFachada->inserirServicoEtapa($oServicoEtapa)){
                    echo json_encode([
                        'sMsg'=>"Etapa de Serviço inserida com sucesso!",
                        'sClass'=>'success',
                        'nCodServico'=>$_POST['fCodServico']
                    ]);
                } else {
                    echo json_encode([
                        'sMsg'=>"Não foi possível inserir o Etapa de Serviço!",
                        'sClass'=>'error',
                        'nCodServico'=>$_POST['fCodServico'],
                        'nCodEtapa'=>$_POST['fCodEtapa']
                    ]);
                }
                die();
            case "Alterar":
                if($oFachada->alterarServicoEtapa($oServicoEtapa)){
                    echo json_encode([
                        'sMsg'=>"Etapa de Serviço alterada com sucesso!",
                        'sClass'=>'success',
                        'nCodServico'=>$_POST['fCodServico'],
                        'nCodEtapa'=>$_POST['fCodEtapa']
                    ]);
                } else {
                    echo json_encode([
                        'sMsg'=>"Não foi possível alterar a Etapa de Serviço!",
                        'sClass'=>'error',
                        'nCodServico'=>$_POST['fCodServico'],
                        'nCodEtapa'=>$_POST['fCodEtapa']
                    ]);
                }
                die();
            case "Excluir":
                $bResultado = $oFachada->excluirServicoEtapa($_GET['nCodServicoEtapa']);

                if($bResultado)
                    echo json_encode([
                        'sMsg'=>"Etapa de Serviço excluída com sucesso!",
                        'sClass'=>'success',
                        'nCodServico'=>$_GET['fCodServico'],
                        'nCodEtapa'=>$_GET['fCodEtapa']
                    ]);
                else
                    echo json_encode([
                        'sMsg'=>"Não foi possível excluir a Etapa de Serviço!",
                        'sClass'=>'error',
                        'nCodServico'=>$_GET['fCodServico'],
                        'nCodEtapa'=>$_GET['fCodEtapa']
                    ]);

                die();
        }
    }
}
