<?php
class ProjetoServicoEtapaColaboradorCTR implements IControle{

    public function preparaLista(){
        $oFachada = new FachadaPrincipalBD();
        $voProjetoServicoEtapaColaborador = $oFachada->recuperarTodosProjetoServicoEtapaColaborador($_GET['CodProjeto'],$_GET['CodMicroservico']);
        $_REQUEST['voProjetoServicoEtapaColaborador'] = $voProjetoServicoEtapaColaborador;

//        include_once("view/Principal/projeto_microservico_colaborador/index.php");
//        exit();

    }

    public function preparaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();
        $voProjetoServicoEtapaColaborador = $oFachada->recuperarTodosProjetoServicoEtapaColaborador($_GET['CodProjeto'],$_GET['CodEtapa']);
        $vColaboradorEtapa = [];
        if ($voProjetoServicoEtapaColaborador)
            foreach ($voProjetoServicoEtapaColaborador as $oProjetoServicoEtapaColaborador) {
                $vColaboradorEtapa[] = $oProjetoServicoEtapaColaborador->getCodColaborador();
            }

        $_REQUEST['voColaborador'] = $oFachada->recuperarTodosColaborador($nIdEscritorio);
        $_REQUEST['vColaboradorEtapa'] = $vColaboradorEtapa;

        include_once("view/principal/projeto_servico_etapa_colaborador/insere_altera.php");
        exit();

    }

    public function processaFormulario(){
        $oFachada = new FachadaPrincipalBD();
        $voColaboradorMicroservico = $oFachada->recuperarTodosProjetoServicoEtapaColaborador($_POST['fCodProjeto'],$_POST['fCodEtapa']);
        $vColaboradorMicroservico = [];
        if ($voColaboradorMicroservico){
            foreach ($voColaboradorMicroservico as $oColaboradorMicroservico) {
                if (!isset($_POST['fColaboradores']) || !in_array($oColaboradorMicroservico->getCodColaborador(),$_POST['fColaboradores'])){
                    $oFachada->excluirProjetoServicoEtapaColaborador($oColaboradorMicroservico->getCodProjetoServicoEtapaColaborador());
                }
                $vColaboradorMicroservico[] = $oColaboradorMicroservico->getCodColaborador();
            }
        }
        if (isset($_POST['fColaboradores'])){
            foreach ($_POST['fColaboradores'] as $nColaboradorEtapa) {
                $oProjetoColaborador = $oFachada->inicializarProjetoServicoEtapaColaborador(null,$_POST['fCodProjeto'],$nColaboradorEtapa,$_POST['fCodEtapa'],$_POST['fPercentual']);
                $oFachada->inserirProjetoServicoEtapaColaborador($oProjetoColaborador);
            }
        }

        echo json_encode(['sMsg'=>'Colaboradores vinculados com sucesso!','sClass'=>'success']);
        die();
    }

}
