<?php
class ProjetoDocumentoCTR implements IControle{

    public function preparaLista()
    {
        $oFachada = new FachadaPrincipalBD();
        $_REQUEST['voProjetoDocumento'] = $oFachada->recuperarTodosProjetoDocumento($_REQUEST['nCodProjeto']);
        $_REQUEST['oProjeto'] = $oFachada->recuperarUmProjeto($_REQUEST['nCodProjeto']);

        include_once("view/principal/projeto_documento/index.php");
        exit();

    }

    public function preparaFormulario()
    {

    }

    /**
     * @throws Exception
     */
    public function processaFormulario()
    {
        $oFachada = new FachadaPrincipalBD();
        $sOP = $_POST['sOP'] ?? ($_GET['sOP'] ?? "");
        $nCodProjeto = $_POST['fCodProjeto'] ?? ($_GET['nCodProjeto'] ?? "");

        switch ($sOP){
            case "Cadastrar":
                $oProjetoDocumento = $oFachada->inicializarProjetoDocumento($_POST['fCodProjetoDocumento'], $_POST['fDescricao'], null, $_POST['fCodProjeto']);

                $nIdEscritorio = $_SESSION['oEscritorio']->getIdEscritorio();

                if ($nCodProjetoDocumento = $oFachada->inserirProjetoDocumento($oProjetoDocumento)){
                    $sFolder = "$nIdEscritorio/documentos/{$_POST['fCodProjeto']}";

                    $oUpload = new Upload();
                    $vUpload = $oUpload->saveFile($_FILES['fDocumento'],$sFolder,$_POST['fDescricao']);

                    if ($vUpload['url'] != ''){
                        $oFachada->atualizarProjetoDocumentoUrl($nCodProjetoDocumento,$vUpload['url'] );
                        $_SESSION['sMsg'] = "Documento inserido com sucesso!";
                        $sHeader = "?bErro=0&action=ProjetoDocumento.preparaLista&nCodProjeto=$nCodProjeto";
                    } else{
                        $oFachada->excluirProjetoDocumento($nCodProjetoDocumento);
                        $_SESSION['sMsg'] = $vUpload['sMsg'];
                        $sHeader = "?bErro=1&action=ProjetoDocumento.preparaLista&nCodProjeto=$nCodProjeto";
                    }
                    
                } else{
                    $_SESSION['sMsg'] = "Erro ao inserir documento!";
                    $sHeader = "?bErro=1&action=ProjetoDocumento.preparaLista&nCodProjeto=$nCodProjeto";
                }
                break;

            case "Excluir":
                $nCodProjetoDocumento = $_GET['nCodProjetoDocumento'];

                $bResultado = $oFachada->excluirProjetoDocumento($nCodProjetoDocumento);

                if($bResultado){
                    $_SESSION['sMsg'] = "Documento excluído com sucesso!";
                    $sHeader = "?bErro=0&action=ProjetoDocumento.preparaLista&nCodProjeto=$nCodProjeto";
                } else {
                    $_SESSION['sMsg'] = "Não foi possível excluir o Documento!";
                    $sHeader = "?bErro=1&action=ProjetoDocumento.preparaLista&nCodProjeto=$nCodProjeto";
                }
                break;

            default:
                $_SESSION['sMsg'] = "Operação não definida!";
                $sHeader = "?bErro=1&action=ProjetoDocumento.preparaLista&nCodProjeto=$nCodProjeto";
                break;
        }

        header("Location: ".$sHeader);
    }

}