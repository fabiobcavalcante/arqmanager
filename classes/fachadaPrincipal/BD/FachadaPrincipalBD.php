<?php

class FachadaPrincipalBD {
    /**
     *
     * Método para inicializar um Objeto Cliente
     *
     * @param $nCodCliente
     * @param $sNome
     * @param $nCodTipoPessoa
     * @param $sIdentificacao
     * @param $sEmail
     * @param $sCep
     * @param $sLogradouro
     * @param $sNumero
     * @param $sComplemento
     * @param $sBairro
     * @param $sCidade
     * @param $sUf
     * @param $sTelefones
     * @param $sDataNascimento
     * @param $sRazaoSocial
     * @param $sInscricaoEstadual
     * @param $sInscricaoMunicipal
     * @param $sObservacao
     * @param $sIndicadoPor
     * @param $sIncluidoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @param $nIdEscritorio
     * @return Cliente
     */
    public function inicializarCliente($nCodCliente,$sNome,$nCodTipoPessoa,$sIdentificacao,$sEmail,$sCep,$sLogradouro,$sNumero,$sComplemento,$sBairro,$sCidade,$sUf,$sTelefones,$sDataNascimento,$sRazaoSocial,$sInscricaoEstadual,$sInscricaoMunicipal,$sObservacao,$sIndicadoPor,$sIncluidoPor,$sAlteradoPor,$nAtivo,$nIdEscritorio){
        $oCliente = new Cliente();

        $oCliente->setCodCliente($nCodCliente);
        $oCliente->setNome($sNome);
        $oCliente->setCodTipoPessoa($nCodTipoPessoa);
        $oCliente->setIdentificacao($sIdentificacao);
        $oCliente->setEmail($sEmail);
        $oCliente->setCep($sCep);
        $oCliente->setLogradouro($sLogradouro);
        $oCliente->setNumero($sNumero);
        $oCliente->setComplemento($sComplemento);
        $oCliente->setBairro($sBairro);
        $oCliente->setCidade($sCidade);
        $oCliente->setUf($sUf);
        $oCliente->setTelefones($sTelefones);
        $oCliente->setDataNascimentoBanco($sDataNascimento);
        $oCliente->setRazaoSocial($sRazaoSocial);
        $oCliente->setInscricaoEstadual($sInscricaoEstadual);
        $oCliente->setInscricaoMunicipal($sInscricaoMunicipal);
        $oCliente->setObservacao($sObservacao);
        $oCliente->setIndicadoPor($sIndicadoPor);
        $oCliente->setIncluidoPor($sIncluidoPor);
        $oCliente->setAlteradoPor($sAlteradoPor);
        $oCliente->setAtivo($nAtivo);
        $oCliente->setIdEscritorio($nIdEscritorio);
        return $oCliente;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Cliente
     *
     * @param $oCliente
     * @return boolean
     */
    public function inserirCliente($oCliente){
        $oPersistencia = new Persistencia($oCliente);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Cliente
     *
     * @param $oCliente
     * @return boolean
     */
    public function alterarCliente($oCliente){
        $oPersistencia = new Persistencia($oCliente);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Cliente
     *
     * @param $nCodCliente
     * @return boolean
     */
    public function excluirCliente($nCodCliente){
        $oCliente = new Cliente();

        $oCliente->setCodCliente($nCodCliente);
        $oPersistencia = new Persistencia($oCliente);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Cliente da base de dados
     *
     * @param $nCodCliente
     * @return Cliente|false
     */
    public function recuperarUmCliente($nCodCliente){
        $oCliente = new Cliente();
        $oPersistencia = new Persistencia($oCliente);
        $sTabelas = "cliente";
        $sCampos = "*";
        $sComplemento = " WHERE cod_cliente = $nCodCliente";
        $voCliente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voCliente)
            return $voCliente[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Cliente da base de dados
     *
     * @return Cliente[]|false
     */
    public function recuperarTodosCliente($nIdEscritorio=1){
        $oCliente = new Cliente();
        $oPersistencia = new Persistencia($oCliente);
        $sTabelas = "cliente";
        $sCampos = "cliente.id_escritorio as id_escritorio,cod_cliente,	nome,	cod_tipo_pessoa,	identificacao,	email,	cep,	logradouro,	numero,	complemento,	bairro,	cidade,	uf,	telefones,	data_nascimento,	razao_social,	inscricao_estadual,	inscricao_municipal,	indicacao.descricao AS indicado_por,	observacao,	incluido_por,	alterado_por,	cliente.ativo ";
        $sComplemento = "INNER JOIN indicacao on indicacao.cod_indicacao = cliente.indicado_por AND indicacao.id_escritorio=$nIdEscritorio where cliente.id_escritorio=$nIdEscritorio ORDER by nome ASC";
// echo "SELECT {$sCampos} from {$sTabela} {$sComplemento}";
        $voCliente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voCliente)
            return $voCliente;
        return false;
    }
    /**
     *
     * Método para recuperar um vetor de objetos Cliente/Colaborador da base de dados
     *
     * @return Cliente[]|false
     */
    public function recuperarTodosClienteColaborador($nCodEscritorio=1){
        $oCliente = new Cliente();
        $oPersistencia = new Persistencia($oCliente);
        $sTabelas = "cliente";
        $sCampos = "cod_cliente,'CLIENTE' AS inserido_por, nome, data_nascimento,identificacao WHERE cod_escritorio=$nCodEscritorio";
        $sComplemento = " UNION
                        SELECT 	cod_colaborador as cod_cliente, 'COLABORADOR' AS inserido_por, nome, data_nascimento,cpf as identificacao  FROM colaborador WHERE cod_escritorio=$nCodEscritorio
                        ORDER BY 2,3 ASC";
        //  echo "SELECT " . $sCampos ." FROM ". $sTabelas . " " . $sComplemento;
        $voCliente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voCliente)
            return $voCliente;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Cliente/Colaborador da base de dados
     *
     * @param $nIdentificacao
     * @return Cliente[]|false
     * @noinspection PhpUnused
     */
    public function recuperarUmClienteColaboradorPorIdentificacao($nIdentificacao){
        $oCliente = new Cliente();
        $oPersistencia = new Persistencia($oCliente);
        $sTabelas = "cliente";
        $sCampos = "'CLIENTE' AS inserido_por, nome, data_nascimento,identificacao";
        $sComplemento = "
						WHERE identificacao = '".$nIdentificacao."'
						UNION
                        SELECT 'COLABORADOR' AS inserido_por, nome, data_aniversario,cpf as identificacao
						FROM colaborador
						WHERE cpf = '".$nIdentificacao."'
                        ORDER BY 2,3 ASC";
        $voCliente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voCliente)
            return $voCliente[0];
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos Cliente/Colaborador da base de dados
     *
     * @param $nMes
     * @param int $nCodEscritorio
     * @return Cliente[]|false
     */
    public function recuperarTodosAniversariantesMes($nMes,$nCodEscritorio=1){
        $oCliente = new Cliente();
        $oPersistencia = new Persistencia($oCliente);
        $sTabelas = "v_aniversariantes";
        $sCampos = "	tipo as ativo,
											codigo as cod_cliente,
											nome,
											data_nascimento,
											email,
											mes as incluido_por,
											dia as alterado_por,
											enviado_em as observacao
									";
        $sComplemento = " WHERE id_escritorio=$nCodEscritorio and mes = $nMes ORDER BY 8,1,2";
        //   echo "select {$sCampos} from {$sTabelas} {$sComplemento}";
        $voCliente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voCliente)
            return $voCliente;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Cliente/Colaborador da base de dados
     *
     * @return array|false
     */
    public function recuperarTodosAniversariantesDia($nIdEscritorio){
        $dDia=date('d');
        $dMes=date('m');
        $oCliente = new Cliente();
        $oPersistencia = new Persistencia($oCliente);
        $sTabelas = "v_aniversariantes";
        $sCampos = "*";
        $sComplemento = " WHERE id_escritorio=$nIdEscritorio and mes = $dMes and dia=$dDia ORDER BY 5,6,2,1 ASC";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voCliente = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voCliente)
            return $voCliente;
        return false;
    }

    public function listarMeses(){
        $oCliente = new Cliente();
        $oPersistencia = new Persistencia($oCliente);
        $sTabelas = "v_mes";
        $sCampos = "*";
        $sComplemento = "" ;
        $voCliente = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voCliente)
            return $voCliente;
        return false;
    }


    public function calculaDataDiasUteis($sTipo,$dData,$nDias){
        $oCliente = new Cliente();
        $oPersistencia = new Persistencia($oCliente);
        $sProcedure = "Prazo";
        $sParametros = "'".$sTipo."','".$dData . "'," . $nDias ;
        $voCliente = $oPersistencia->consultarProcedureLista($sProcedure,$sParametros);
        if($voCliente)
            return $voCliente[0]->FIM;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto Colaborador
     *
     * @param $nCodColaborador
     * @param $sNome
     * @param $dDataNascimento
     * @param $sBanco
     * @param $sAgencia
     * @param $sTipoConta
     * @param $sConta
     * @param $sPixTipo
     * @param $sPixChave
     * @param $sCpf
     * @param $sLogin
     * @param $sSenha
     * @param $sFoto
     * @param $sEmail
     * @param $sInseridoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @param $nTipo
     * @return Colaborador
     */
    public function inicializarColaborador($nCodColaborador,$sNome,$dDataNascimento,$sBanco,$sAgencia,$sTipoConta,$sConta,$sPixTipo,$sPixChave,$sCpf,$sLogin,$sSenha,$sFoto,$sEmail,$sInseridoPor,$sAlteradoPor,$nAtivo,$nTipo): Colaborador
    {

        $oColaborador = new Colaborador();
        $oColaborador->setCodColaborador($nCodColaborador);
        $oColaborador->setNome($sNome);
        $oColaborador->setDataNascimentoBanco($dDataNascimento);
        $oColaborador->setBanco($sBanco);
        $oColaborador->setAgencia($sAgencia);
        $oColaborador->setTipoConta($sTipoConta);
        $oColaborador->setConta($sConta);
        $oColaborador->setPixTipo($sPixTipo);
        $oColaborador->setPixChave($sPixChave);
        $oColaborador->setCpf($sCpf);
        $oColaborador->setLogin($sLogin);
        $oColaborador->setSenha($sSenha);
        $oColaborador->setFoto($sFoto);
        $oColaborador->setEmail($sEmail);
        $oColaborador->setInseridoPor($sInseridoPor);
        $oColaborador->setAlteradoPor($sAlteradoPor);
        $oColaborador->setAtivo($nAtivo);
        $oColaborador->setTipo($nTipo);
        return $oColaborador;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Colaborador
     *
     * @param $oColaborador
     * @return boolean
     */
    public function inserirColaborador($oColaborador){
        $oPersistencia = new Persistencia($oColaborador);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Colaborador
     *
     * @param $oColaborador
     * @return boolean
     */
    public function alterarColaborador($oColaborador){
        $oPersistencia = new Persistencia($oColaborador);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Colaborador
     *
     * @param $nCodColaborador
     * @return boolean
     */
    public function excluirColaborador($nCodColaborador){
        $oColaborador = new Colaborador();

        $oColaborador->setCodColaborador($nCodColaborador);
        $oPersistencia = new Persistencia($oColaborador);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Colaborador da base de dados
     *
     * @param $nCodColaborador
     * @return Colaborador|false
     */
    public function recuperarUmColaborador($nCodColaborador){
        $oColaborador = new Colaborador();
        $oPersistencia = new Persistencia($oColaborador);
        $sTabelas = "colaborador";
        $sCampos = "*";
        $sComplemento = " WHERE cod_colaborador = $nCodColaborador";
        //  echo "SELECT * FROM " . $sTabelas . " " . $sComplemento;
        $voColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaborador)
            return $voColaborador[0];
        return false;
    }


    /**
     *
     * Método para recuperar um objeto Colaborador da base de dados
     *
     * @param $sCPF
     * @return Colaborador|false
     */
    public function recuperarUmColaboradorPorCpf($sCPF){
        $oColaborador = new Colaborador();
        $oPersistencia = new Persistencia($oColaborador);
        $sTabelas = "colaborador";
        $sCampos = "*";
        $sComplemento = " WHERE cpf = '$sCPF'";
        //  echo "SELECT * FROM " . $sTabelas . " " . $sComplemento;
        $voColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaborador)
            return $voColaborador[0];
        return false;
    }

    /**
     *
     * Método para recuperar um objeto Colaborador da base de dados
     *
     * @param $sLogin
     * @return Colaborador|false
     */
    public function recuperarUmColaboradorPorLogin($sLogin){
        $oColaborador = new Colaborador();
        $oPersistencia = new Persistencia($oColaborador);
        $sTabelas = "colaborador";
        $sCampos = "*";
        $sComplemento = " WHERE login = '$sLogin'";
//        echo "Select $sCampos From $sTabelas $sComplemento";
        $voColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaborador)
            return $voColaborador[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Colaborador da base de dados
     *
     * @return Colaborador[]|false
     */
    public function recuperarTodosColaborador($nIdEscritorio){
        $oColaborador = new Colaborador();
        $oPersistencia = new Persistencia($oColaborador);
        $sTabelas = "v_colaborador";
        $sCampos = "*";
        $sComplemento = "WHERE id_escritorio=$nIdEscritorio and cod_colaborador not in(7,12) and ativo=1 order by login asc";
        $voColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaborador)
            return $voColaborador;
        return false;
    }

    public function listarProducaoColaboradorAno($nCodColaborador,$nAno){
        $oColaborador = new Colaborador();
        $oPersistencia = new Persistencia($oColaborador);
        $sProcedure = "sp_producao_ano_colaborador";
        $sParametros = $nCodColaborador . "," . $nAno ;
        $voColaborador = $oPersistencia->consultarProcedureLista($sProcedure,$sParametros);
        if($voColaborador)
            return $voColaborador;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto ColaboradorEscritorioGrupo
     *
     * @param $nCodColaborador
     * @param $nCodGrupoUsuario
     * @param $nIdEscritorio
     * @return ColaboradorEscritorioGrupo
     */
    public function inicializarColaboradorEscritorioGrupo($nCodColaborador,$nIdEscritorio,$nCodGrupoUsuario): ColaboradorEscritorioGrupo
    {

        $oColaboradorEscritorioGrupo = new ColaboradorEscritorioGrupo();

        $oColaboradorEscritorioGrupo->setCodColaborador($nCodColaborador);
        $oColaboradorEscritorioGrupo->setCodGrupoUsuario($nCodGrupoUsuario);
        $oColaboradorEscritorioGrupo->setIdEscritorio($nIdEscritorio);
        return $oColaboradorEscritorioGrupo;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ColaboradorEscritorioGrupo
     *
     * @param $oColaboradorEscritorioGrupo
     * @return boolean
     */
    public function inserirColaboradorEscritorioGrupo($oColaboradorEscritorioGrupo){
        $oPersistencia = new Persistencia($oColaboradorEscritorioGrupo);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto ColaboradorEscritorioGrupo
     *
     * @param $oColaboradorEscritorio
     * @return boolean
     */
    public function alterarColaboradorEscritorioGrupo($oColaboradorEscritorio){
        $oPersistencia = new Persistencia($oColaboradorEscritorio);
        $sComplemento = "Where cod_colaborador = {$oColaboradorEscritorio->getCodColaborador()} And id_escritorio = {$oColaboradorEscritorio->getIdEscritorio()}";
        if($oPersistencia->alterarSemChavePrimaria($sComplemento))
            return true;
        return false;
    }



    /**
     *
     * Método para excluir da base de dados um Objeto ColaboradorEscritorioGrupo
     *
     * @param $nCodColaborador
     * @param $nIdEscritorio
     * @return boolean
     */
    public function excluirColaboradorEscritorioGrupoPorColaboradorEscritorio($nCodColaborador,$nIdEscritorio){
        $oColaboradorEscritorioGrupo = new ColaboradorEscritorioGrupo();
        $oPersistencia = new Persistencia($oColaboradorEscritorioGrupo);
        $sComplemento = " WHERE cod_colaborador={$nCodColaborador} AND id_escritorio={$nIdEscritorio}";
        $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para excluir da base de dados um Objeto ColaboradorEscritorioGrupo
     *
     * @param $nCodColaborador
     * @param $nCodGrupoUsuario
     * @param $nIdEscritorio
     * @return boolean
     */
    public function excluirColaboradorEscritorioGrupo($nCodColaborador,$nCodGrupoUsuario,$nIdEscritorio){
        $oColaboradorEscritorioGrupo = new ColaboradorEscritorioGrupo();

        $oColaboradorEscritorioGrupo->setCodColaborador($nCodColaborador);
        $oColaboradorEscritorioGrupo->setCodGrupoUsuario($nCodGrupoUsuario);
        $oColaboradorEscritorioGrupo->setIdEscritorio($nIdEscritorio);
        $oPersistencia = new Persistencia($oColaboradorEscritorioGrupo);
        $bExcluir = $oPersistencia->excluirFisicamente();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto ColaboradorEscritorioGrupo da base de dados
     *
     * @param $nCodColaborador
     * @param $nIdEscritorio
     * @return ColaboradorEscritorioGrupo|false
     */
    public function recuperarUmColaboradorEscritorioGrupo($nCodColaborador,$nIdEscritorio){
        $oColaboradorEscritorioGrupo = new ColaboradorEscritorioGrupo();
        $oPersistencia = new Persistencia($oColaboradorEscritorioGrupo);
        $sTabelas = "colaborador_escritorio_grupo";
        $sCampos = "*";
        $sComplemento = "Where cod_colaborador = $nCodColaborador And id_escritorio=$nIdEscritorio";
//        echo "Select $sCampos From $sTabelas $sComplemento";
        $voColaboradorEscritorioGrupo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorEscritorioGrupo)
            return $voColaboradorEscritorioGrupo[0];
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ColaboradorEscritorioGrupo da base de dados
     *
     * @return ColaboradorEscritorioGrupo[]|false
     */
    public function recuperarTodosColaboradorEscritorioGrupoPorEscritorio($nIdEscritorio){
        $oColaboradorEscritorioGrupo = new ColaboradorEscritorioGrupo();
        $oPersistencia = new Persistencia($oColaboradorEscritorioGrupo);
        $sTabelas = "colaborador_escritorio_grupo";
        $sCampos = "*";
        $sComplemento = "WHERE id_escritorio=$nIdEscritorio and cod_colaborador not in(7,12)";
        $voColaboradorEscritorioGrupo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorEscritorioGrupo)
            return $voColaboradorEscritorioGrupo;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ColaboradorEscritorioGrupo da base de dados
     *
     * @return ColaboradorEscritorioGrupo[]|false
     */
    public function recuperarTodosColaboradorEscritorioGrupoPorLogin($sLogin){
        $oColaboradorEscritorioGrupo = new ColaboradorEscritorioGrupo();
        $oPersistencia = new Persistencia($oColaboradorEscritorioGrupo);
        $sTabelas = "colaborador_escritorio_grupo";
        $sCampos = "*";
        $sComplemento = "INNER JOIN colaborador on colaborador.cod_colaborador = colaborador_escritorio_grupo.cod_colaborador
                          where login='{$sLogin}'";
//        echo "Select * From $sTabelas $sComplemento";
        $voColaboradorEscritorioGrupo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorEscritorioGrupo)
            return $voColaboradorEscritorioGrupo;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ColaboradorEscritorioGrupo da base de dados
     *
     * @return ColaboradorEscritorioGrupo[]|false
     */
    public function recuperarTodosColaboradorEscritorioGrupoPorColaborador($nCodColaborador){
        $oColaboradorEscritorioGrupo = new ColaboradorEscritorioGrupo();
        $oPersistencia = new Persistencia($oColaboradorEscritorioGrupo);
        $sTabelas = "colaborador_escritorio_grupo";
        $sCampos = "*";
        $sComplemento = "WHERE cod_colaborador=$nCodColaborador";
//        echo "Select * From $sTabelas $sComplemento";
        $voColaboradorEscritorioGrupo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorEscritorioGrupo)
            return $voColaboradorEscritorioGrupo;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto Colaborador
     *
     * @param $nCodProducao
     * @param $nCodColaborador
     * @param $nCodProjeto
     * @param $nPercentual
     * @param $nValor
     * @return ColaboradorProducao
     */
    public function inicializarColaboradorProducao($nCodProducao,$nCodColaborador,$nCodProjeto,$nPercentual,$nValor){
        $oColaboradorProducao = new ColaboradorProducao();
        $oColaboradorProducao->setCodProducao($nCodProducao);
        $oColaboradorProducao->setCodColaborador($nCodColaborador);
        $oColaboradorProducao->setCodProjeto($nCodProjeto);
        $oColaboradorProducao->setPercentualBanco($nPercentual);
        $oColaboradorProducao->setValorBanco($nValor);
        return $oColaboradorProducao;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ColaboradorProducao
     *
     * @param $oColaboradorProducao
     * @return boolean
     */
    public function inserirColaboradorProducao($oColaboradorProducao){
        $oPersistencia = new Persistencia($oColaboradorProducao);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Colaborador
     *
     * @param $oColaboradorProducao
     * @return boolean
     */
    public function alterarColaboradorProducao($oColaboradorProducao){
        $oPersistencia = new Persistencia($oColaboradorProducao);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto oColaboradorProducao
     *
     * @param $nCodProducao
     * @return boolean
     */
    public function excluiroColaboradorProducao($nCodProducao){
        $oColaboradorProducao = new ColaboradorProducao();

        $oColaboradorProducao->setCodProducao($nCodProducao);
        $oPersistencia = new Persistencia($oColaboradorProducao);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Colaborador da base de dados
     *
     * @param $nCodProducao
     * @return ColaboradorProducao|false
     */
    public function recuperarUmColaboradorProducao($nCodProducao){
        $oColaboradorProducao = new ColaboradorProducao();
        $oPersistencia = new Persistencia($oColaboradorProducao);
        $sTabelas = "colaborador_producao";
        $sCampos = "*";
        $sComplemento = " WHERE cod_producao = $nCodProducao";
        // echo "SELECT * FROM " . $sTabelas . " " . $sComplemento;
        $voColaboradorProducao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorProducao)
            return $voColaboradorProducao[0];
        return false;
    }


    /**
     *
     * Método para recuperar um objeto Colaborador da base de dados
     *
     * @param $nCodProducao
     * @return ColaboradorProducao|false
     */
    public function recuperarUmVColaboradorProducaoPorProducao($nCodProducao){
        $oColaboradorProducao = new ColaboradorProducao();
        $oPersistencia = new Persistencia($oColaboradorProducao);
        $sTabelas = "v_colaborador_producao";
        $sCampos = "*";
        $sComplemento = " WHERE cod_producao = $nCodProducao";
        // echo "SELECT * FROM " . $sTabelas . " " . $sComplemento;
        $voColaboradorProducao = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorProducao)
            return $voColaboradorProducao[0];
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos Colaborador da base de dados
     *
     * @return Colaborador[]|false
     */
    public function recuperarTodosColaboradorProducao($nIdEscritorio){
        $oColaboradorProducao = new ColaboradorProducao();
        $oPersistencia = new Persistencia($oColaboradorProducao);
        $sTabelas = "colaborador_producao";
        $sCampos = "*";
        $sComplemento = "WHERE id_escritorio={$nIdEscritorio}";
        $voColaboradorProducao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorProducao)
            return $voColaboradorProducao;
        return false;
    }



    /**
     *
     * Método para recuperar um vetor de objetos Colaborador da base de dados
     *
     * @return Colaborador[]|false
     */
    public function recuperarTodosColaboradorProducaoPorColaborador($nIdColaborador){
        $oColaboradorProducao = new ColaboradorProducao();
        $oPersistencia = new Persistencia($oColaboradorProducao);
        $sTabelas = "colaborador_producao";
        $sCampos = "*";
        $sComplemento = "WHERE cod_colaborador={$nIdColaborador}";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voColaboradorProducao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorProducao)
            return $voColaboradorProducao;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto Colaborador
     *
     * @param $nCodColaboradorPagamento
     * @param $nCodProducao
     * @param $nValor
     * @param $dDataPagamento
     * @return ColaboradorPagamento
     */
    public function inicializarColaboradorPagamento($nCodColaboradorPagamento,$nCodProducao,$nValor,$dDataPagamento){

        $oColaboradorPagamento = new ColaboradorPagamento();

        $oColaboradorPagamento->setCodColaboradorPagamento($nCodColaboradorPagamento);
        $oColaboradorPagamento->setCodProducao($nCodProducao);
        $oColaboradorPagamento->setValorBanco($nValor);
        $oColaboradorPagamento->setDataPagamentoBanco($dDataPagamento);

        return $oColaboradorPagamento;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ColaboradorPagamento
     *
     * @param $oColaboradorPagamento
     * @return boolean
     */
    public function inserirColaboradorPagamento($oColaboradorPagamento){
        $oPersistencia = new Persistencia($oColaboradorPagamento);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Colaborador
     *
     * @param $oColaboradorPagamento
     * @return boolean
     */
    public function alterarColaboradorPagamento($oColaboradorPagamento){
        $oPersistencia = new Persistencia($oColaboradorPagamento);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto oColaboradorPagamento
     *
     * @param $nCodColaboradorPagamento
     * @return boolean
     */
    public function excluiroColaboradorPagamento($nCodColaboradorPagamento){
        $oColaboradorPagamento = new ColaboradorPagamento();

        $oColaboradorPagamento->setCodColaboradorPagamento($nCodColaboradorPagamento);
        $oPersistencia = new Persistencia($oColaboradorPagamento);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Colaborador da base de dados
     *
     * @param $nCodColaboradorPagamento
     * @return ColaboradorPagamento|false
     */
    public function recuperarUmColaboradorPagamento($nCodColaboradorPagamento){
        $oColaboradorPagamento = new ColaboradorPagamento();
        $oPersistencia = new Persistencia($oColaboradorPagamento);
        $sTabelas = "colaborador_pagamento";
        $sCampos = "*";
        $sComplemento = " WHERE cod_colaborador_pagamento = $nCodColaboradorPagamento";
        //  echo "SELECT * FROM " . $sTabelas . " " . $sComplemento;
        $voColaboradorPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorPagamento)
            return $voColaboradorPagamento[0];
        return false;
    }



    /**
     *
     * Método para recuperar um vetor de objetos Colaborador da base de dados
     *
     * @return Colaborador[]|false
     */
    public function recuperarTodosColaboradorPagamento(){
        $oColaboradorPagamento = new ColaboradorPagamento();
        $oPersistencia = new Persistencia($oColaboradorPagamento);
        $sTabelas = "colaborador_pagamento";
        $sCampos = "*";
        $sComplemento = "";
        $voColaboradorPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorPagamento)
            return $voColaboradorPagamento;
        return false;
    }



    /**
     *
     * Método para recuperar um vetor de objetos Colaborador da base de dados
     *
     * @return Colaborador[]|false
     */
    public function recuperarTodosColaboradorPagamentoPorColaborador($nIdColaborador){
        $oColaboradorPagamento = new ColaboradorPagamento();
        $oPersistencia = new Persistencia($oColaboradorPagamento);
        $sTabelas = "colaborador_pagamento";
        $sCampos = "*";
        $sComplemento = "INNER JOIN colaborador_producao on colaborador_producao.cod_producao = colaborador_pagamento.cod_producao WHERE cod_colaborador={$nIdColaborador}";
        //    echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voColaboradorPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorPagamento)
            return $voColaboradorPagamento;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos Colaborador da base de dados
     *
     * @return Colaborador[]|false
     */
    public function recuperarTodosColaboradorPagamentoPorProducao($nCodProducao){
        $oColaboradorPagamento = new ColaboradorPagamento();
        $oPersistencia = new Persistencia($oColaboradorPagamento);
        $sTabelas = "colaborador_pagamento";
        $sCampos = "*";
        $sComplemento = "WHERE cod_producao={$nCodProducao}";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voColaboradorPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorPagamento)
            return $voColaboradorPagamento;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto DespesaReceita
     *
     * @param $nCodDespesaReceita
     * @param $nCodDespesaReceitaTipo
     * @param $sDescricao
     * @param $sIncluidoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @param $nIdEscritorio
     * @return DespesaReceita
     */
    public function inicializarDespesaReceita($nCodDespesaReceita,$nCodDespesaReceitaTipo,$sDescricao,$sIncluidoPor,$sAlteradoPor,$nAtivo,$nIdEscritorio){
        $oDespesaReceita = new DespesaReceita();

        $oDespesaReceita->setCodDespesaReceita($nCodDespesaReceita);
        $oDespesaReceita->setCodDespesaReceitaTipo($nCodDespesaReceitaTipo);
        $oDespesaReceita->setDescricao($sDescricao);
        $oDespesaReceita->setIncluidoPor($sIncluidoPor);
        $oDespesaReceita->setAlteradoPor($sAlteradoPor);
        $oDespesaReceita->setAtivo($nAtivo);
        $oDespesaReceita->setIdEscritorio($nIdEscritorio);
        return $oDespesaReceita;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto DespesaReceita
     *
     * @param $oDespesaReceita
     * @return boolean
     */
    public function inserirDespesaReceita($oDespesaReceita){
        $oPersistencia = new Persistencia($oDespesaReceita);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto DespesaReceita
     *
     * @param $oDespesaReceita
     * @return boolean
     */
    public function alterarDespesaReceita($oDespesaReceita){
        $oPersistencia = new Persistencia($oDespesaReceita);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto DespesaReceita
     *
     * @param $nCodDespesaReceita
     * @return boolean
     */
    public function excluirDespesaReceita($nCodDespesaReceita){
        $oDespesaReceita = new DespesaReceita();

        $oDespesaReceita->setCodDespesaReceita($nCodDespesaReceita);
        $oPersistencia = new Persistencia($oDespesaReceita);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto DespesaReceita na base de dados
     *
     * @param $nCodDespesaReceita
     * @return boolean
     */
    public function presenteDespesaReceita($nCodDespesaReceita){
        $oDespesaReceita = new DespesaReceita();

        $oDespesaReceita->setCodDespesaReceita($nCodDespesaReceita);
        $oPersistencia = new Persistencia($oDespesaReceita);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto DespesaReceita da base de dados
     *
     * @param $nCodDespesaReceita
     * @return DespesaReceita|false
     */
    public function recuperarUmDespesaReceita($nCodDespesaReceita){
        $oDespesaReceita = new DespesaReceita();
        $oPersistencia = new Persistencia($oDespesaReceita);
        $sTabelas = "despesa_receita";
        $sCampos = "*";
        $sComplemento = " WHERE cod_despesa_receita = $nCodDespesaReceita";
        $voDespesaReceita = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDespesaReceita)
            return $voDespesaReceita[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos DespesaReceita da base de dados
     *
     * @return DespesaReceita[]|false
     */
    public function recuperarTodosDespesaReceita(){
        $oDespesaReceita = new DespesaReceita();
        $oPersistencia = new Persistencia($oDespesaReceita);
        $sTabelas = "despesa_receita";
        $sCampos = "*";
        $sComplemento = "";
        $voDespesaReceita = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDespesaReceita)
            return $voDespesaReceita;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto DespesaReceitaTipo
     *
     * @param $nCodDespesaReceitaTipo
     * @param $sAbreviacao
     * @param $sDescricao
     * @param $sEntradaSaida
     * @param $sIncluidoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @return DespesaReceitaTipo
     */
    public function inicializarDespesaReceitaTipo($nCodDespesaReceitaTipo,$sAbreviacao,$sDescricao,$sEntradaSaida,$sIncluidoPor,$sAlteradoPor,$nAtivo){
        $oDespesaReceitaTipo = new DespesaReceitaTipo();

        $oDespesaReceitaTipo->setCodDespesaReceitaTipo($nCodDespesaReceitaTipo);
        $oDespesaReceitaTipo->setAbreviacao($sAbreviacao);
        $oDespesaReceitaTipo->setDescricao($sDescricao);
        $oDespesaReceitaTipo->setEntradaSaida($sEntradaSaida);
        $oDespesaReceitaTipo->setIncluidoPor($sIncluidoPor);
        $oDespesaReceitaTipo->setAlteradoPor($sAlteradoPor);
        $oDespesaReceitaTipo->setAtivo($nAtivo);
        return $oDespesaReceitaTipo;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto DespesaReceitaTipo
     *
     * @param $oDespesaReceitaTipo
     * @return boolean
     */
    public function inserirDespesaReceitaTipo($oDespesaReceitaTipo){
        $oPersistencia = new Persistencia($oDespesaReceitaTipo);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto DespesaReceitaTipo
     *
     * @param $oDespesaReceitaTipo
     * @return boolean
     */
    public function alterarDespesaReceitaTipo($oDespesaReceitaTipo){
        $oPersistencia = new Persistencia($oDespesaReceitaTipo);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto DespesaReceitaTipo
     *
     * @param $nCodDespesaReceitaTipo
     * @return boolean
     */
    public function excluirDespesaReceitaTipo($nCodDespesaReceitaTipo){
        $oDespesaReceitaTipo = new DespesaReceitaTipo();

        $oDespesaReceitaTipo->setCodDespesaReceitaTipo($nCodDespesaReceitaTipo);
        $oPersistencia = new Persistencia($oDespesaReceitaTipo);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto DespesaReceitaTipo da base de dados
     *
     * @param $nCodDespesaReceitaTipo
     * @return DespesaReceitaTipo|false
     */
    public function recuperarUmDespesaReceitaTipo($nCodDespesaReceitaTipo){
        $oDespesaReceitaTipo = new DespesaReceitaTipo();
        $oPersistencia = new Persistencia($oDespesaReceitaTipo);
        $sTabelas = "despesa_receita_tipo";
        $sCampos = "*";
        $sComplemento = " WHERE cod_despesa_receita_tipo = $nCodDespesaReceitaTipo";
        $voDespesaReceitaTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDespesaReceitaTipo)
            return $voDespesaReceitaTipo[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos DespesaReceitaTipo da base de dados
     *
     * @return DespesaReceitaTipo[]|false
     */
    public function recuperarTodosDespesaReceitaTipo(){
        $oDespesaReceitaTipo = new DespesaReceitaTipo();
        $oPersistencia = new Persistencia($oDespesaReceitaTipo);
        $sTabelas = "despesa_receita_tipo";
        $sCampos = "*";
        $sComplemento = "";
        $voDespesaReceitaTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDespesaReceitaTipo)
            return $voDespesaReceitaTipo;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos DespesaReceitaTipo da base de dados
     *
     * @param $sTipo
     * @return DespesaReceitaTipo[]|false
     */
    public function recuperarTodosDespesaReceitaTipoPorTipo($sTipo,$nIdEscritorio){
        $oDespesaReceitaTipo = new DespesaReceitaTipo();
        $oPersistencia = new Persistencia($oDespesaReceitaTipo);
        $sTabelas = "despesa_receita_tipo";
        $sCampos = "*";
        $sComplemento = "WHERE entrada_saida = '".$sTipo."' ORDER BY abreviacao, descricao";
        $voDespesaReceitaTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDespesaReceitaTipo)
            return $voDespesaReceitaTipo;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto Email
     *
     * @param $nCodEmail
     * @param $nCodCliente
     * @param $nCodTipoEmail
     * @param $sDescricao
     * @param $dDataHora
     * @return Email
     */
    public function inicializarEmail($nCodEmail,$nCodCliente,$nCodTipoEmail,$sDescricao,$dDataHora){
        $oEmail = new Email();

        $oEmail->setCodEmail($nCodEmail);
        $oEmail->setCodCliente($nCodCliente);
        $oEmail->setCodTipoEmail($nCodTipoEmail);
        $oEmail->setDescricao($sDescricao);
        $oEmail->setDataHora($dDataHora);
        return $oEmail;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Email
     *
     * @param $oEmail
     * @return boolean
     */
    public function inserirEmail($oEmail){
        $oPersistencia = new Persistencia($oEmail);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Email
     *
     * @param $oEmail
     * @return boolean
     */
    public function alterarEmail($oEmail){
        $oPersistencia = new Persistencia($oEmail);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Email
     *
     * @param $nCodCliente
     * @return boolean
     */
    public function excluirEmailPorCliente($nCodCliente){
        $oEmail = new Email();
        $sComplemento = " WHERE cod_cliente=".$nCodCliente;
        //die();
        $oPersistencia = new Persistencia($oEmail);
        $bExcluir =  $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);
        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para inicializar um Objeto EmailTipo
     *
     * @param $nCodEmailTipo
     * @param $sTitulo
     * @param $sTemplate
     * @param $nAtivo
     * @return EmailTipo
     */
    public function inicializarEmailTipo($nCodEmailTipo,$sTitulo,$sTemplate,$nAtivo){
        $oEmailTipo = new EmailTipo();

        $oEmailTipo->setCodEmailTipo($nCodEmailTipo);
        $oEmailTipo->setTitulo($sTitulo);
        $oEmailTipo->setTemplate($sTemplate);
        $oEmailTipo->setAtivo($nAtivo);
        return $oEmailTipo;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto EmailTipo
     *
     * @param $oEmailTipo
     * @return boolean
     */
    public function inserirEmailTipo($oEmailTipo){
        $oPersistencia = new Persistencia($oEmailTipo);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto EmailTipo
     *
     * @param $oEmailTipo
     * @return boolean
     */
    public function alterarEmailTipo($oEmailTipo){
        $oPersistencia = new Persistencia($oEmailTipo);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto EmailTipo
     *
     * @param $nCodEmailTipo
     * @return boolean
     */
    public function excluirEmailTipo($nCodEmailTipo){
        $oEmailTipo = new EmailTipo();

        $oEmailTipo->setCodEmailTipo($nCodEmailTipo);
        $oPersistencia = new Persistencia($oEmailTipo);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto EmailTipo da base de dados
     *
     * @param $nCodEmailTipo
     * @return EmailTipo|false
     */
    public function recuperarUmEmailTipo($nCodEmailTipo){
        $oEmailTipo = new EmailTipo();
        $oPersistencia = new Persistencia($oEmailTipo);
        $sTabelas = "email_tipo";
        $sCampos = "*";
        $sComplemento = " WHERE cod_email_tipo = $nCodEmailTipo";
        $voEmailTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEmailTipo)
            return $voEmailTipo[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos EmailTipo da base de dados
     *
     * @return EmailTipo[]|false
     */
    public function recuperarTodosEmailTipo(){
        $oEmailTipo = new EmailTipo();
        $oPersistencia = new Persistencia($oEmailTipo);
        $sTabelas = "email_tipo";
        $sCampos = "*";
        $sComplemento = "";
        $voEmailTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEmailTipo)
            return $voEmailTipo;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos EmailTipo da base de dados
     *
     * @return EmailTipo[]|false
     */
    public function recuperarTodosEmailTipoPorEscritorio($nIdEscritorio){
        $oEmailTipo = new EmailTipo();
        $oPersistencia = new Persistencia($oEmailTipo);
        $sTabelas = "email_tipo";
        $sCampos = "email_tipo_escritorio.ativo as ativo, email_tipo.cod_email_tipo,titulo,
			              IFNULL(email_tipo_escritorio.template,email_tipo.template) as template";
        $sComplemento = "inner join email_tipo_escritorio ON email_tipo_escritorio.cod_email_tipo = email_tipo.cod_email_tipo
        where id_escritorio = $nIdEscritorio";
        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voEmailTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEmailTipo)
            return $voEmailTipo;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos EmailTipo da base de dados
     *
     * @return EmailTipo[]|false
     */
    public function recuperarUmEmailTipoPorEscritorio($nCodEmailTipo, $nIdEscritorio){
        $oEmailTipo = new EmailTipo();
        $oPersistencia = new Persistencia($oEmailTipo);
        $sTabelas = "email_tipo";
        $sCampos = "email_tipo.cod_email_tipo,titulo,
                    IFNULL(email_tipo_escritorio.template,email_tipo.template) as template";
        $sComplemento = "inner join email_tipo_escritorio ON email_tipo_escritorio.cod_email_tipo = email_tipo.cod_email_tipo and ativo=1
        where id_escritorio = $nIdEscritorio And email_tipo.cod_email_tipo = $nIdEscritorio";
        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voEmailTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEmailTipo)
            return $voEmailTipo[0];
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto EmailTipo
     *
     * @param $nCodEmailTipo
     * @return EmailTipo
     * @param $sTemplate
     */
    public function inicializarEmailTipoEscritorio($nCodEmailTipo,$nIdEscritorio,$sTemplate,$nAtivo){
        $oEmailTipoEscritorio = new EmailTipoEscritorio();

        $oEmailTipoEscritorio->setCodEmailTipo($nCodEmailTipo);
        $oEmailTipoEscritorio->setIdEscritorio($nIdEscritorio);
        $oEmailTipoEscritorio->setTemplate($sTemplate);
        $oEmailTipoEscritorio->setAtivo($nAtivo);

        return $oEmailTipoEscritorio;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto EmailTipoEscritorio
     *
     * @param $oEmailTipoEscritorio
     * @return boolean
     */
    public function inserirEmailTipoEscritorio($oEmailTipoEscritorio){
        $oPersistencia = new Persistencia($oEmailTipoEscritorio);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto EmailTipo
     *
     * @param $oEmailTipo
     * @return boolean
     */
    public function alterarEmailTipoEscritorio($oEmailTipoEscritorio){
        $oPersistencia = new Persistencia($oEmailTipoEscritorio);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto EmailTipoEscritorio
     *
     * @param $nCodEmailTipo
     * @param $nIdEscritorio
     * @return boolean
     */
    public function excluirEmailTipoEscritorio($nCodEmailTipo, $nIdEscritorio){
        $oEmailTipoEscritorio = new EmailTipoEscritorio();

        $oEmailTipoEscritorio->setCodEmailTipo($nCodEmailTipo);
        $oEmailTipoEscritorio->setIdEscritorio($nIdEscritorio);
        $oPersistencia = new Persistencia($oEmailTipoEscritorio);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto EmailTipoEscritorio da base de dados
     *
     * @param $nCodEmailTipo
     * @param $nIdEscritorio
     * @return EmailTipoEscritorio|false
     */
    public function recuperarUmEmailTipoEscritorio($nCodEmailTipo,$nIdEscritorio){
        $oEmailTipoEscritorio = new EmailTipoEscritorio();
        $oPersistencia = new Persistencia($oEmailTipoEscritorio);
        $sTabelas = "email_tipo_escritorio";
        $sCampos = "*";
        $sComplemento = " WHERE cod_email_tipo = {$nCodEmailTipo} AND id_escritorio={$nIdEscritorio}";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";

        $voEmailTipoEscritorio = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEmailTipoEscritorio)
            return $voEmailTipoEscritorio[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos EmailTipo da base de dados
     *
     * @return EmailTipo[]|false
     */
    public function recuperarTodosEmailTipoEscritorio(){
        $oEmailTipoEscritorio = new EmailTipoEscritorio();
        $oPersistencia = new Persistencia($oEmailTipoEscritorio);
        $sTabelas = "email_tipo_escritorio";
        $sCampos = "*";
        $sComplemento = "";
        $voEmailTipoEscritorio = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEmailTipoEscritorio)
            return $voEmailTipoEscritorio;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto FormaPagamento
     *
     * @param $nCodFormaPagamento
     * @param $sDescricao
     * @param $sIncluidoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @return FormaPagamento
     * @noinspection PhpUnused
     */
    public function inicializarFormaPagamento($nCodFormaPagamento,$sDescricao,$sIncluidoPor,$sAlteradoPor,$nAtivo){
        $oFormaPagamento = new FormaPagamento();

        $oFormaPagamento->setCodFormaPagamento($nCodFormaPagamento);
        $oFormaPagamento->setDescricao($sDescricao);
        $oFormaPagamento->setIncluidoPor($sIncluidoPor);
        $oFormaPagamento->setAlteradoPor($sAlteradoPor);
        $oFormaPagamento->setAtivo($nAtivo);
        return $oFormaPagamento;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto FormaPagamento
     *
     * @param $nCodFormaPagamento
     * @return boolean
     * @noinspection PhpUnused
     */
    public function excluirFormaPagamento($nCodFormaPagamento){
        $oFormaPagamento = new FormaPagamento();

        $oFormaPagamento->setCodFormaPagamento($nCodFormaPagamento);
        $oPersistencia = new Persistencia($oFormaPagamento);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto FormaPagamento da base de dados
     *
     * @param $nCodFormaPagamento
     * @return false|FormaPagamento
     */
    public function recuperarUmFormaPagamento($nCodFormaPagamento){
        $oFormaPagamento = new FormaPagamento();
        $oPersistencia = new Persistencia($oFormaPagamento);
        $sTabelas = "forma_pagamento";
        $sCampos = "*";
        $sComplemento = " WHERE cod_forma_pagamento = $nCodFormaPagamento";
        $voFormaPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voFormaPagamento)
            return $voFormaPagamento[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos FormaPagamento da base de dados
     *
     * @return false|FormaPagamento[]
     */
    public function recuperarTodosFormaPagamento(){
        $oFormaPagamento = new FormaPagamento();
        $oPersistencia = new Persistencia($oFormaPagamento);
        $sTabelas = "forma_pagamento";
        $sCampos = "*";
        $sComplemento = "WHERE ativo=1";
        $voFormaPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voFormaPagamento)
            return $voFormaPagamento;
        return false;
    }


    /***
     *
     * Metodo para inicializar um Objeto Indicacao
     *
     * @return Indicacao
     */
    public function inicializarIndicacao($nCodIndicacao,$sDescricao,$nAtivo,$nIdEscritorio){
        $oIndicacao = new Indicacao();

        $oIndicacao->setCodIndicacao($nCodIndicacao);
        $oIndicacao->setDescricao($sDescricao);
        $oIndicacao->setAtivo($nAtivo);
        $oIndicacao->setIdEscritorio($nIdEscritorio);
        return $oIndicacao;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Indicacao
     *
     * @return boolean
     */
    public function inserirIndicacao($oIndicacao){
        $oPersistencia = new Persistencia($oIndicacao);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Indicacao
     *
     * @return boolean
     */
    public function alterarIndicacao($oIndicacao){
        $oPersistencia = new Persistencia($oIndicacao);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Indicacao
     *
     * @return boolean
     */
    public function excluirIndicacao($nCodIndicacao){
        $oIndicacao = new Indicacao();

        $oIndicacao->setCodIndicacao($nCodIndicacao);
        $oPersistencia = new Persistencia($oIndicacao);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oUsuarioAM']->getCodIndicacao() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Indicacao da base de dados
     *
     * @return false|Indicacao
     */
    public function recuperarUmIndicacao($nCodIndicacao){
        $oIndicacao = new Indicacao();
        $oPersistencia = new Persistencia($oIndicacao);
        $sTabelas = "indicacao";
        $sCampos = "*";
        $sComplemento = " WHERE cod_indicacao = $nCodIndicacao";
        $voIndicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voIndicacao)
            return $voIndicacao[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Indicacao da base de dados
     *
     * @return false|Indicacao[]
     */
    public function recuperarTodosIndicacao($nIdEscritorio=1){
        $oIndicacao = new Indicacao();
        $oPersistencia = new Persistencia($oIndicacao);
        $sTabelas = "indicacao";
        $sCampos = "*";
        $sComplemento = "WHERE id_escritorio=$nIdEscritorio ORDER BY descricao ASC";
        $voIndicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voIndicacao)
            return $voIndicacao;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos Indicacao da base de dados
     *
     * @return false|Indicacao[]
     */
    public function recuperarTodosIndicacaoRanking($nIdEscritorio=1){
        $oIndicacao = new Indicacao();
        $oPersistencia = new Persistencia($oIndicacao);
        $sTabelas = "indicacao";
        $sCampos = "cod_indicacao,
										descricao,
										count(*) as indicacoes,
										(select format(sum(valor),2,'de_DE') from v_financeiro_projeto where indicacao.cod_indicacao = v_financeiro_projeto.indicado_por AND data_efetivacao IS NOT NULL) as total_recebido,
										(select format(sum(valor),2,'de_DE') from v_financeiro_projeto where indicacao.cod_indicacao = v_financeiro_projeto.indicado_por AND data_efetivacao IS NULL) as total_a_receber";
        $sComplemento = "	inner join cliente on cliente.indicado_por=indicacao.cod_indicacao
                        where cliente.id_escritorio=$nIdEscritorio
												GROUP BY cod_indicacao,descricao
												order by 3 desc";

        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voIndicacao = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voIndicacao)
            return $voIndicacao;
        return false;
    }




    /**
     *
     * Método para inicializar um Objeto Movimento
     *
     * @param $sCodMovimento
     * @param $nCodDespesaReceitaTipo
     * @param $dDataEntrada
     * @param $sDescricao
     * @param $dDataEfetivacao
     * @param $sComprovante
     * @param $nValor
     * @param $sObservacao
     * @param $sInseridoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @param $nIdEscritorio
     * @param $dCompetencia
     * @param $nCodPlanoContas
     * @param $nCodProjeto
     * @return Movimento
     */
    public function inicializarMovimento($sCodMovimento,$nCodDespesaReceitaTipo,$dDataEntrada,$sDescricao,$dDataEfetivacao,$sComprovante,$nValor,$sObservacao,$sInseridoPor,$sAlteradoPor,$nAtivo,$nIdEscritorio,$dCompetencia,$nCodPlanoContas,$nCodProjeto){
        $oMovimento = new Movimento();

        $oMovimento->setCodMovimento($sCodMovimento);
        $oMovimento->setCodDespesaReceitaTipo($nCodDespesaReceitaTipo);
        $oMovimento->setDataEntradaBanco($dDataEntrada);
        $oMovimento->setDescricao($sDescricao);
        $oMovimento->setDataEfetivacaoBanco($dDataEfetivacao);
        $oMovimento->setComprovante($sComprovante);
        $oMovimento->setValorBanco($nValor);
        $oMovimento->setObservacao($sObservacao);
        $oMovimento->setInseridoPor($sInseridoPor);
        $oMovimento->setAlteradoPor($sAlteradoPor);
        $oMovimento->setAtivo($nAtivo);
        $oMovimento->setIdEscritorio($nIdEscritorio);
        $oMovimento->setCompetenciaBanco($dCompetencia);
        $oMovimento->setCodPlanoContas($nCodPlanoContas);
        $oMovimento->setCodProjeto($nCodProjeto);
        return $oMovimento;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Movimento
     *
     * @param $oMovimento
     * @return boolean
     */
    public function inserirMovimento($oMovimento){
        $oPersistencia = new Persistencia($oMovimento);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Movimento
     *
     * @param $oMovimento
     * @return boolean
     */
    public function alterarMovimento($oMovimento){
        $oPersistencia = new Persistencia($oMovimento);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Movimento
     *
     * @param $sCodMovimento
     * @return boolean
     */
    public function excluirMovimento($sCodMovimento){
        $oMovimento = new Movimento();

        $oMovimento->setCodMovimento($sCodMovimento);
        $oPersistencia = new Persistencia($oMovimento);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Movimento da base de dados
     *
     * @param $sCodMovimento
     * @return false|Movimento
     */
    public function recuperarUmMovimento($sCodMovimento){
        $oMovimento = new Movimento();
        $oPersistencia = new Persistencia($oMovimento);
        $sTabelas = "movimento";
        $sCampos = "*";
        $sComplemento = " WHERE cod_movimento = '$sCodMovimento'";
        $voMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voMovimento)
            return $voMovimento[0];
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos Movimento da base de dados
     *
     * @return boolean | array
     */
    public function recuperarUmRelatorioPrincipal($nIdEscritorio){
        $oMovimento = new Movimento();
        $oPersistencia = new Persistencia($oMovimento);
        $sProcedure = "sp_principal";
        $sParametros = $nIdEscritorio;
        $voMovimento = $oPersistencia->consultarProcedureLista($sProcedure,$sParametros);
        if($voMovimento)
            return $voMovimento[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Movimento da base de dados
     *
     * @param $sComplemento
     * @return false|Movimento[]
     */
    public function recuperarTodosMovimentoComplemento($sComplemento){
        $oMovimento = new Movimento();
        $oPersistencia = new Persistencia($oMovimento);
        $sProcedure = "sp_movimento";
        $sParametros = '"'.$sComplemento.'"';
//        echo "Call $sProcedure ($sComplemento)";
        $voMovimento = $oPersistencia->consultarProcedureLista($sProcedure,$sParametros);
        if($voMovimento)
            return $voMovimento;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto PlanoContas
     *
     * @param $nCodPlanoContas
     * @param $sCodigo
     * @param $sDescricao
     * @param $nVisivel
     * @param $sUsuInc
     * @param $sUsuAlt
     * @param $nAtivo
     * @param $nIdPai
     * @param $nIdEscritorio
     * @return PlanoContas
     */
    public function inicializarPlanoContas($nCodPlanoContas,$sCodigo,$sDescricao,$nVisivel,$sUsuInc,$sUsuAlt,$nAtivo,$nIdPai,$nIdEscritorio){
        $oPlanoContas = new PlanoContas();

        $oPlanoContas->setCodPlanoContas($nCodPlanoContas);
        $oPlanoContas->setCodigo($sCodigo);
        $oPlanoContas->setDescricao($sDescricao);
        $oPlanoContas->setVisivel($nVisivel);
        $oPlanoContas->setUsuInc($sUsuInc);
        $oPlanoContas->setUsuAlt($sUsuAlt);
        $oPlanoContas->setAtivo($nAtivo);
        $oPlanoContas->setIdPai($nIdPai);
        $oPlanoContas->setIdEscritorio($nIdEscritorio);
        return $oPlanoContas;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto PlanoContas
     *
     * @param $oPlanoContas
     * @return boolean
     */
    public function inserirPlanoContas($oPlanoContas){
        $oPersistencia = new Persistencia($oPlanoContas);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto PlanoContas
     *
     * @param $oPlanoContas
     * @return boolean
     */
    public function alterarPlanoContas($oPlanoContas){
        $oPersistencia = new Persistencia($oPlanoContas);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto PlanoContas
     *
     * @param $nCodPlanoContas
     * @return boolean
     */
    public function excluirPlanoContas($nCodPlanoContas){
        $oPlanoContas = new PlanoContas();

        $oPlanoContas->setCodPlanoContas($nCodPlanoContas);
        $oPersistencia = new Persistencia($oPlanoContas);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto PlanoContas da base de dados
     *
     * @param $nCodPlanoContas
     * @return false|PlanoContas
     */
    public function recuperarUmPlanoContas($nCodPlanoContas){
        $oPlanoContas = new PlanoContas();
        $oPersistencia = new Persistencia($oPlanoContas);
        $sTabelas = "plano_contas";
        $sCampos = "*";
        $sComplemento = " WHERE cod_plano_contas = $nCodPlanoContas";
//        echo "select * from {$sTabelas} {$sComplemento}";
        $voPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPlanoContas)
            return $voPlanoContas[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PlanoContas da base de dados
     *
     * @return false|PlanoContas[]
     */
    public function recuperarTodosPlanoContas($nIdEscritorio){
        $oPlanoContas = new PlanoContas();
        $oPersistencia = new Persistencia($oPlanoContas);
        $sTabelas = "plano_contas";
        $sCampos = "*";
        $sComplemento = "WHERE id_escritorio=$nIdEscritorio";
//        echo "select * from {$sTabelas} {$sComplemento}";

        $voPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPlanoContas)
            return $voPlanoContas;
        return false;
    }
    /**
     *
     * Método para recuperar um vetor de objetos PlanoContas da base de dados
     *
     * @return false|PlanoContas[]
     */
    public function recuperarTodosPlanoContasPorGrupoPorVisibilidade($nIdEscritorio,$nGrupo,$nVisibilidade=1){
        $oPlanoContas = new PlanoContas();
        $oPersistencia = new Persistencia($oPlanoContas);
        $sTabelas = "plano_contas";
        $sCampos = "*,REPLACE(codigo,'.','00') as ordem ";
        $sComplemento = "WHERE id_escritorio=$nIdEscritorio AND codigo like '{$nGrupo}%' AND visivel={$nVisibilidade} order by ordem asc";
//        echo "Select $sCampos From $sTabelas $sComplemento";

        $voPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPlanoContas)
            return $voPlanoContas;
        return false;
    }
    /**
     *
     * Método para recuperar um vetor de objetos PlanoContas da base de dados
     *
     * @return false|PlanoContas[]
     */
    public function recuperarTodosPlanoContasPorPai($nIdEscritorio,$nIdPai,$nVisibilidade=1){
        $oPlanoContas = new PlanoContas();
        $oPersistencia = new Persistencia($oPlanoContas);
        $sTabelas = "plano_contas";
        $sCampos = "*,REPLACE(codigo,'.','00') as ordem ";
        $sComplemento = "WHERE id_escritorio=$nIdEscritorio AND id_pai ={$nIdPai} AND visivel={$nVisibilidade} order by ordem asc";
        // echo "select {$sCampos} from {$sTabelas} {$sComplemento}";
        $voPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPlanoContas)
            return $voPlanoContas;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto Projeto
     *
     * @param $nCodProjeto
     * @param $nCodCliente
     * @param $nCodServico
     * @param $nCodProposta
     * @param $dDescricao
     * @param $dDataInicio
     * @param $dDataPrevisaoFim
     * @param $nMetragem
     * @param $sObservacao
     * @param $sIncluidoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @param $nNumeroProjeto
     * @param $nAnoProjeto
     * @param $nCodStatus
     * @return Projeto
     */
    public function inicializarProjeto($nCodProjeto,$nCodCliente,$nCodServico,$nCodProposta,$dDescricao,$dDataInicio,$dDataPrevisaoFim,$dDataFim,$nMetragem,$sObservacao,$sIncluidoPor,$sAlteradoPor,$nAtivo,$nNumeroProjeto,$nAnoProjeto,$nCodStatus){
        $oProjeto = new Projeto();

        $oProjeto->setCodProjeto($nCodProjeto);
        $oProjeto->setCodCliente($nCodCliente);
        $oProjeto->setCodServico($nCodServico);
        $oProjeto->setCodProposta($nCodProposta);
        $oProjeto->setDescricao($dDescricao);
        $oProjeto->setDataInicioBanco($dDataInicio);
        $oProjeto->setDataPrevisaoFimBanco($dDataPrevisaoFim);
        $oProjeto->setDataFimBanco($dDataFim);
        $oProjeto->setMetragemBanco($nMetragem);
        $oProjeto->setObservacao($sObservacao);
        $oProjeto->setIncluidoPor($sIncluidoPor);
        $oProjeto->setAlteradoPor($sAlteradoPor);
        $oProjeto->setAtivo($nAtivo);
        $oProjeto->setNumeroProJeto($nNumeroProjeto);
        $oProjeto->setAnoProjeto($nAnoProjeto);
        $oProjeto->setCodStatus($nCodStatus);
        return $oProjeto;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Projeto
     *
     * @param $oProjeto
     * @return boolean
     */
    public function inserirProjeto($oProjeto){
        $oPersistencia = new Persistencia($oProjeto);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Projeto
     *
     * @param $oProjeto
     * @return boolean
     */
    public function alterarProjeto($oProjeto){
        $oPersistencia = new Persistencia($oProjeto);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Projeto
     *
     * @param $nCodProjeto
     * @return boolean
     */
    public function excluirProjeto($nCodProjeto){
        $oProjeto = new Projeto();

        $oProjeto->setCodProjeto($nCodProjeto);
        $oPersistencia = new Persistencia($oProjeto);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Projeto da base de dados
     *
     * @param $nCodProjeto
     * @return false|Projeto
     */
    public function recuperarUmProjeto($nCodProjeto){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "projeto";
        $sCampos = "*";
        $sComplemento = " WHERE cod_projeto = $nCodProjeto";
        //echo "SELECT * FROM projeto ". $sComplemento;
        $voProjeto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto[0];
        return false;
    }


    /**
     *
     * Método para recuperar um objeto Colaborador da base de dados
     *
     * @param $nCodProjeto
     * @return Projeto |false
     */
    public function recuperarUmVProjeto($nCodProjeto){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "v_projeto";
        $sCampos = "*";
        $sComplemento = " WHERE cod_projeto = $nCodProjeto";
        // echo "SELECT * FROM " . $sTabelas . " " . $sComplemento;
        $voColaboradorProducao = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voColaboradorProducao)
            return $voColaboradorProducao[0];
        return false;
    }

    public function recuperarProximoProjeto($nIdEscritorio,$nAno){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "projeto";
        $sCampos = "IFNULL(max(numero_projeto)+1,1) as numero_projeto";
        $sComplemento = " inner join proposta on proposta.cod_proposta = projeto.cod_proposta WHERE id_escritorio={$nIdEscritorio} and ano_projeto={$nAno}";
        //echo "SELECT * FROM projeto ". $sComplemento;
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto[0];
        return false;
    }


    /**
     *
     * Método para recuperar um objeto Projeto da base de dados
     *
     * @param $nCodProjeto
     * @return false|Projeto
     */
    public function recuperarUmProjetoViewPorProjeto($nCodProjeto){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "v_projeto";
        $sCampos = "*";
        $sComplemento = " WHERE cod_projeto = $nCodProjeto";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto[0];
        return false;
    }

    /**
     *
     * Método para recuperar um objeto Projeto da base de dados
     *
     * @param $nCodProposta
     * @return false|Projeto
     */
    public function recuperarUmProjetoPorProposta($nCodProposta){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "projeto";
        $sCampos = "*";
        $sComplemento = " WHERE cod_proposta = $nCodProposta";
        // echo "SELECT * FROM projeto ". $sComplemento;
        $voProjeto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto[0];
        return false;
    }

    /**
     *
     * Método para recuperar um objeto Projeto da base de dados
     *
     * @param $nCodProjeto
     * @param $nOpcao
     * @return bool
     */
    public function atualizarPrazosProjeto($nCodProjeto,$nOpcao){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        if($nOpcao == 1)
            $sTabelas = "f_projeto_prazo_etapa";
        else {
            $sTabelas = "f_projeto_prazo_microservico";
        }
        $sComplemento = "'{$_SESSION['oEscritorio']->getDiasUteis()}',$nCodProjeto";

        $voPropostaServicoEtapa = $oPersistencia->atualizarViaProcedure($sTabelas,$sComplemento);
        if($voPropostaServicoEtapa)
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @return false|Projeto[]
     */
    public function recuperarTodosProjeto(){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "projeto";
        $sCampos = "numero_projeto,ano_projeto,projeto.cod_projeto,projeto.cod_proposta,projeto.observacao,servico.desc_servico as cod_servico,cliente.nome as cod_cliente,projeto.descricao,projeto.data_inicio,projeto.data_previsao_fim,projeto.incluido_por,projeto.alterado_por,projeto.ativo";
        $sComplemento = "INNER JOIN cliente ON projeto.cod_cliente = cliente.cod_cliente
                         INNER JOIN servico ON projeto.cod_servico = servico.cod_servico";
        // echo "Select $sCampos From $sTabelas $sComplemento";
        $voProjeto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }
    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @return false|Projeto[]
     */
    public function recuperarTodosVProjeto($nIdEscritorio){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "v_projeto";
        $sCampos = "*";
        $sComplemento = "WHERE id_escritorio={$nIdEscritorio}";
        //      echo "Select {$sCampos} From {$sTabelas} {$sComplemento}";
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @param $nCodColaborador
     * @return false|Projeto[]
     */
    // public function recuperarTodosProjetoPorColaboradorOld($nCodColaborador){
    //     $oProjeto = new Projeto();
    //     $oPersistencia = new Persistencia($oProjeto);
    //     $sTabelas = "projeto";
    //
    //     $sCampos = "DISTINCT 	projeto.descricao,
    // 						data_previsao_fim,
    // 						desc_servico as cod_servico,
    // 						cliente.nome as cod_cliente,
    // 						CONCAT(FORMAT(f_percentual_projeto_colaborador(projeto.cod_projeto,projeto_colaborador.cod_colaborador)*100,2,'de_DE'),'%') as ativo,
    // 						IFNULL(FORMAT(f_valor_recebido_a_receber_projeto(projeto.cod_projeto,'RECEBER'), 2, 'de_DE'),0) as incluido_por,
    // 						IFNULL(FORMAT(f_valor_recebido_a_receber_projeto(projeto.cod_projeto,'RECEBIDO'), 2, 'de_DE'),0) as alterado_por,
    // 						IF(data_fim is null,'Em Andamento','Concluído') as observacao";
    //     $sComplemento = "	INNER JOIN projeto_colaborador ON projeto.cod_projeto = projeto_colaborador.cod_projeto
    // 					INNER JOIN cliente ON cliente.cod_cliente = projeto.cod_cliente
    // 					INNER JOIN servico ON projeto.cod_servico = servico.cod_servico
    // 					WHERE cod_colaborador=$nCodColaborador ORDER BY 8 DESC";
    //     $voProjeto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
    //
    //     if($voProjeto)
    //         return $voProjeto;
    //     return false;
    // }



    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @param $nCodColaborador
     * @return false|Projeto[]
     */
    public function recuperarTodosProjetoPorColaborador($nCodColaborador){
        $oColaboradorProducao = new ColaboradorProducao();
        $oPersistencia = new Persistencia($oColaboradorProducao);
        $sTabelas = "v_colaborador_producao";

        $sCampos = "*";
        $sComplemento = "	WHERE cod_colaborador={$nCodColaborador}";
        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }



    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @param $nCodColaborador
     * @return false|Projeto[]
     */
    public function recuperarTodosProjetoVinculoPorColaborador($nCodColaborador,$nIdEscritorio=1){
        $oColaboradorProducao = new ColaboradorProducao();
        $oPersistencia = new Persistencia($oColaboradorProducao);
        $sTabelas = "v_projeto";

        $sCampos = "*";
        $sComplemento = "	where id_escritorio={$nIdEscritorio} and cod_projeto not in(select cod_projeto from colaborador_producao where cod_colaborador={$nCodColaborador}) and data_fim is null";
        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @return false|Projeto[]
     */
    public function recuperarTodosProjetoAno($nIdEscritorio=1){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "projeto";
        $sCampos = "DISTINCT YEAR(data_inicio) as data_fim";
        $sComplemento = "inner join proposta on proposta.cod_proposta = projeto.cod_proposta and proposta.id_escritorio=$nIdEscritorio order by 1 desc ";
        $voProjeto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @return false|Projeto[]
     */
    public function recuperarTodosProjetoStatus($nIdEscritorio,$nCodStatus){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "v_projeto";
        $sCampos = "*";
        switch($nCodStatus){
            case 1: // em Andamento
                $sComplemento = "where ativo=1 and pagamento is not null  and id_escritorio={$nIdEscritorio}  and data_fim is null";
                break;
            case 2: // Concluido
                $sComplemento = "where ativo=1 and pagamento is not null and id_escritorio={$nIdEscritorio}  and data_fim is not null";
                break;
            default: // todos
                $sComplemento = "where ativo=1 and pagamento is not null and  id_escritorio={$nIdEscritorio} ";
                break;

        }
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }






    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @return false|Projeto[]
     */
    public function recuperarTodosProjetoAtivo(){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "projeto";
        $sCampos = "projeto.cod_projeto,	CONCAT(cliente.nome,'. Serviço: ', servico.desc_servico ,'. Descrição: ',projeto.descricao) as descricao";
        $sComplemento = "INNER JOIN cliente ON projeto.cod_cliente = cliente.cod_cliente
                         INNER JOIN servico ON projeto.cod_servico = servico.cod_servico
                         WHERE data_fim is null AND projeto.ativo=1";
        $voProjeto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @param $nCodCliente
     * @return false|Projeto[]
     */
    public function recuperarTodosProjetoPorCliente($nCodCliente){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "projeto";
        $sCampos = "projeto.cod_projeto,servico.desc_servico as cod_servico,cliente.nome as cod_cliente,projeto.descricao,projeto.data_inicio,projeto.data_previsao_fim,projeto.data_fim,projeto.incluido_por,proposta.identificacao as alterado_por,projeto.ativo";
        $sComplemento = "INNER JOIN proposta ON projeto.cod_proposta = proposta.cod_proposta
						 INNER JOIN cliente ON projeto.cod_cliente = cliente.cod_cliente
                         INNER JOIN servico ON projeto.cod_servico = servico.cod_servico
                         WHERE projeto.ativo=1 and projeto.cod_cliente = ".$nCodCliente;
        $voProjeto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto ProjetoPagamento
     *
     * @param $nCodProjetoPagamento
     * @param $nCodProjeto
     * @param $nCodFormaPagamento
     * @param $nValor
     * @param $dDataPrevisao
     * @param $dDataEfetivacao
     * @param $sComprovante
     * @param $sDescPagamento
     * @param $sIncluidoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @return ProjetoPagamento
     */
    public function inicializarProjetoPagamento($nCodProjetoPagamento,$nCodProjeto,$nCodFormaPagamento,$nValor,$dDataPrevisao,$dDataEfetivacao,$sComprovante,$sDescPagamento,$sIncluidoPor,$sAlteradoPor,$nAtivo){
        $oProjetoPagamento = new ProjetoPagamento();

        $oProjetoPagamento->setCodProjetoPagamento($nCodProjetoPagamento);
        $oProjetoPagamento->setCodProjeto($nCodProjeto);
        $oProjetoPagamento->setCodFormaPagamento($nCodFormaPagamento);
        $oProjetoPagamento->setValorBanco($nValor);
        $oProjetoPagamento->setDataPrevisaoBanco($dDataPrevisao);
        $oProjetoPagamento->setDataEfetivacaoBanco($dDataEfetivacao);
        $oProjetoPagamento->setComprovante($sComprovante);
        $oProjetoPagamento->setDescPagamento($sDescPagamento);
        $oProjetoPagamento->setIncluidoPor($sIncluidoPor);
        $oProjetoPagamento->setAlteradoPor($sAlteradoPor);
        $oProjetoPagamento->setAtivo($nAtivo);
        return $oProjetoPagamento;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ProjetoPagamento
     *
     * @param $oProjetoPagamento
     * @return boolean
     */
    public function inserirProjetoPagamento($oProjetoPagamento){
        $oPersistencia = new Persistencia($oProjetoPagamento);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto ProjetoPagamento
     *
     * @param $oProjetoPagamento
     * @return boolean
     */
    public function alterarProjetoPagamento($oProjetoPagamento){
        $oPersistencia = new Persistencia($oProjetoPagamento);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto ProjetoPagamento
     *
     * @param $nCodProjetoPagamento
     * @return boolean
     */
    public function excluirProjetoPagamento($nCodProjetoPagamento){
        $oProjetoPagamento = new ProjetoPagamento();

        $oProjetoPagamento->setCodProjetoPagamento($nCodProjetoPagamento);
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para excluir da base de dados um Objeto ProjetoPagamento
     *
     * @param $nCodProjeto
     * @return boolean
     */
    public function excluirProjetoPagamentoPorProjeto($nCodProjeto){
        $oProjetoPagamento = new ProjetoPagamento();

        $oProjetoPagamento->setCodProjeto($nCodProjeto);
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $bExcluir =  $oPersistencia->excluirFisicamenteSemChavePrimaria("WHERE cod_projeto=".$nCodProjeto);

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto ProjetoPagamento da base de dados
     *
     * @param $nCodProjetoPagamento
     * @return false|ProjetoPagamento
     */
    public function recuperarUmProjetoPagamento($nCodProjetoPagamento){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "projeto_pagamento";
        $sCampos = "*";
        $sComplemento = " WHERE cod_projeto_pagamento = $nCodProjetoPagamento";
        $voProjetoPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento[0];
        return false;
    }

    /**
     *
     * Método para recuperar um objeto ProjetoPagamento da base de dados
     *
     * @param $nCodProjeto
     * @return false|ProjetoPagamento
     */
    public function recuperarUmProjetoPagamentoEfetivadoProjeto($nCodProjeto){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "projeto_pagamento";
        $sCampos = "data_efetivacao";
        $sComplemento = "where cod_projeto_pagamento = (select min(cod_projeto_pagamento) from projeto_pagamento where cod_projeto=$nCodProjeto and data_efetivacao is not null)";
        $voProjetoPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento[0]->data_efetivacao;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ProjetoPagamento da base de dados
     *
     * @param $nCodProjeto
     * @return false|ProjetoPagamento[]
     */
    public function recuperarTodosProjetoPagamento($nCodProjeto){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "projeto_pagamento";
        $sCampos = "*";
        $sComplemento = "Where cod_projeto = $nCodProjeto";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voProjetoPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ProjetoPagamento da base de dados
     *
     * @param $nCodProjeto
     * @return false|ProjetoPagamento[]
     */
    public function recuperarTodosProjetoPagamentoConfirmados($nCodProjeto){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "projeto_pagamento";
        $sCampos = "*";
        $sComplemento = "Where cod_projeto = $nCodProjeto and data_efetivacao is not null";
        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voProjetoPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoPagamento da base de dados
     *
     * @return false|ProjetoPagamento[]
     */
    public function recuperarTodosProjetoPagamentoPendente($nIdEscritorio=1){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "projeto_pagamento";
        $sCampos = "projeto_pagamento.cod_projeto,
                projeto_pagamento.cod_projeto_pagamento,
                cast( concat( proposta.nome, ' [', proposta.identificacao, '] PARCELA ', projeto_pagamento.desc_pagamento ) AS CHAR charset utf8mb4 ) AS desc_pagamento,
                data_previsao,
                valor,
                projeto_pagamento.desc_pagamento as ativo";

        $sComplemento = "inner join projeto on projeto.cod_projeto = projeto_pagamento.cod_projeto
                      inner join proposta on projeto.cod_proposta = proposta.cod_proposta
                      inner JOIN cliente ON projeto.cod_cliente = cliente.cod_cliente
                      where data_efetivacao is null and proposta.id_escritorio=".$nIdEscritorio;
        $voProjetoPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @return false|Projeto[]
     */
    public function recuperarTodosAno($nIdEscritorio=1){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "v_financeiro_projeto";
        $sCampos = "Distinct Year(data_previsao) as ano";
        $sComplemento = "Where id_escritorio=$nIdEscritorio Order By 1 desc ";
        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Projeto da base de dados
     *
     * @return false|Projeto[]
     */
    public function recuperarTodosMes(){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "v_mes";
        $sCampos = "*";
        $sComplemento = "order by 1";
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @return false|PropostaServicoEtapa[]
     */
    public function recuperarTodosProjetoPagamentoView($nMesSelecionado,$nAno,$nStatus=1,$nIdEscritorio=1){

        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabelas = "v_financeiro_projeto";
        $sCampos = "*";
        $sComplemento = "";

        switch($nStatus){
            //previsto
            case 1: $sComplemento = " WHERE id_escritorio=$nIdEscritorio and year(data_previsao) = $nAno AND  month(data_previsao) = $nMesSelecionado and data_efetivacao IS NULL   ORDER BY date(data_previsao) asc"; break;
            // Efetivado
            case 2: $sComplemento = " WHERE id_escritorio=$nIdEscritorio and  year(data_efetivacao) = $nAno and month(data_efetivacao) = $nMesSelecionado ORDER BY date(data_efetivacao) DESC"; break;
            // PREVISTO X efetivado
            case 3: $sComplemento = " WHERE id_escritorio=$nIdEscritorio and  data_conclusao is not null order by date(data_conclusao) desc"; break;

        }
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";

        $voPropostaServicoEtapa = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @return false|PropostaServicoEtapa[]
     */
    public function recuperarPrevistoRealizadoView($nAno,$sTipo,$nIdEscritorio){
        $oPersistencia = new Persistencia();
        $sTabelas = "v_previsto_realizado";
        $sCampos = "*";
        $sComplemento = "Where ano = $nAno And tipo = '$sTipo' And id_escritorio = $nIdEscritorio Order By ano,mes";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voFinanceiro = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voFinanceiro)
            return $voFinanceiro;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @return false|PropostaServicoEtapa[]
     */
    public function recuperarEntradaSaidaView($nAno,$sTipo,$nIdEscritorio){
        $oPersistencia = new Persistencia();
        $sTabelas = "v_extrato_consolidado";
        $sCampos = "cast(mes_efetivacao as SIGNED) as mes_efetivacao,ano_efetivacao,tipo,valor,id_escritorio";
        $sComplemento = "Where ano_efetivacao = $nAno And tipo = '$sTipo' And id_escritorio = $nIdEscritorio Order By ano_efetivacao,mes_efetivacao";
//         echo "SELECT * FROM $sTabelas $sComplemento";
        $voFinanceiro = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voFinanceiro)
            return $voFinanceiro;
        return false;
    }
    public function recuperarMediasAnuaisFinanceiro($nIdEscritorio,$nAno){
        $oMovimento = new Movimento();
        $oPersistencia = new Persistencia($oMovimento);
        $sProcedure = "sp_media_financeiro_anual";
        $sParametros = "$nIdEscritorio,$nAno" ;
        $voMovimento = $oPersistencia->consultarProcedureLista($sProcedure,$sParametros);
        if($voMovimento)
            return $voMovimento[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @param $nDias
     * @param $nTipo
     * @return false|array
     */
    public function recuperarTodosProjetoPagamentoCron($nDias,$nTipo,$nIdEscritorio){

        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "v_financeiro_projeto";
        $sCampos = "*";

        switch($nTipo){
            // Aviso de Vencimento
            case 1: $sComplemento = "Where id_escritorio={$nIdEscritorio} AND data_efetivacao is null and data_previsao BETWEEN CURRENT_DATE() and ADDDATE(CURRENT_DATE(), INTERVAL $nDias DAY)";
                break;
            // atraso no pagamento
            case 2: $sComplemento = "Where
                        id_escritorio={$nIdEscritorio} AND
                        data_efetivacao Is Null
                        And data_previsao < ADDDATE(CURRENT_DATE (), INTERVAL - $nDias DAY)
                        And (Select count(*) From email Where cod_tipo_email = 4 And cod_cliente = v_financeiro_projeto.cod_cliente And Date(data_hora) BETWEEN ADDDATE(CURRENT_DATE(), INTERVAL - $nDias DAY) And CURRENT_DATE()) = 0";
                break;

        }
        // echo "SELECT * FROM v_financeiro_projeto {$sComplemento}";

        $voProjetoPagamento = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     */
    public function recuperarUmProjetoPagamentoView($nCodProjetoPagamento){

        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "v_financeiro_projeto";
        $sCampos = "*";
        $sComplemento = " WHERE cod_projeto_pagamento=$nCodProjetoPagamento";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";

        $voProjetoPagamento = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoPagamento da base de dados
     *
     * @param $nCodProjeto
     * @return false|ProjetoPagamento[]
     */
    public function recuperarTodosProjetoPagamentoPorProjeto($nCodProjeto){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "projeto_pagamento";
        $sCampos = "*";
        $sComplemento = "WHERE cod_projeto=".$nCodProjeto . " ORDER BY data_previsao ASC";
        $voProjetoPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoPagamento da base de dados
     *
     * @param $nCodProjeto
     * @return false|ProjetoPagamento[]
     */
    public function recuperarTodosProjetoPagamentoPorProjetoView($nCodProjeto){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "v_financeiro_projeto";
        $sCampos = "*";
        $sComplemento = "WHERE cod_projeto=".$nCodProjeto . " ORDER BY data_previsao ASC";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voProjetoPagamento = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ProjetoPagamento da base de dados
     * @param $nMes
     * @param $nAno
     * @return false|ProjetoPagamento[]
     */
    public function recuperarUmProjetoPagamentoPorMesAnoEfetivadoTotais($nMes,$nAno){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);
        $sTabelas = "projeto_pagamento";
        /*$sCampos = "count(cod_projeto_pagamento) as ativo,
                      sum(ifnull(valor,0)) as valor ,
                    (select sum(ifnull(valor,0)) from projeto_pagamento where month(data_previsao)=$nMes and year(data_previsao)=$nAno ) as incluido_por,
                    round(sum(ifnull(valor,0)) * 100 / (select sum(ifnull(valor,0)) from projeto_pagamento where month(data_previsao)=$nMes and year(data_previsao)=$nAno ),2) as alterado_por";*/

        $sCampos = "(count(cod_projeto_pagamento)+ (SELECT IFNULL(COUNT(cod_movimento),0) FROM movimento INNER JOIN despesa_receita_tipo ON despesa_receita_tipo.cod_despesa_receita_tipo = movimento.cod_despesa_receita_tipo  AND entrada_saida='E' WHERE MONTH(data_efetivacao)=$nMes and YEAR(data_efetivacao)=$nAno)) as ativo,
			 (sum(ifnull(valor,0)) +
	(SELECT ROUND(IFNULL(SUM(valor),0),2) FROM movimento INNER JOIN despesa_receita_tipo ON despesa_receita_tipo.cod_despesa_receita_tipo = movimento.cod_despesa_receita_tipo  AND entrada_saida='E' WHERE MONTH(data_efetivacao)=$nMes and YEAR(data_efetivacao)=$nAno ))
		as valor ,
			 (select ifnull(sum(valor),0) from projeto_pagamento where month(data_previsao)=$nMes and year(data_previsao)=$nAno ) as incluido_por,
			 ifnull(round(sum(ifnull(valor,0)) * 100 / (select sum(ifnull(valor,0)) from projeto_pagamento where month(data_previsao)=$nMes and year(data_previsao)=$nAno ),2),0) as alterado_por ";
        $sComplemento = " WHERE MONTH(data_efetivacao)=$nMes and YEAR(data_efetivacao)=$nAno";
//echo "SELECT " . $sCampos . " from " . $sTabelas . " " . $sComplemento;
        $voProjetoPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);


        if($voProjetoPagamento)
            return $voProjetoPagamento[0];
        return false;

    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoColaborador da base de dados nivel 1 de detalhamento do pagamento mensal do funcionário
     * @param $nMes
     * @param $nAno
     * @param $nCodColaborador
     * @return false|ProjetoServicoEtapaColaborador[]
     */
    public function recuperarTodosProjetoColaboradorPorMesAnoColaboradorEfetivado($nMes,$nAno,$nCodColaborador){
        $oProjetoColaborador = new ProjetoServicoEtapaColaborador();
        $oPersistencia = new Persistencia($oProjetoColaborador);
        $sTabelas = "projeto_colaborador pc";
        //concat(cliente.nome, ' - ',servico.desc_servico, ' - ', p.descricao, ' - ', IFNULL(p.observacao,'-') ) AS cod_servico_detalhe,
        $sCampos = "
            distinct

            concat(servico.cod_servico, '||',servico.desc_servico,'||',cliente.nome,'||',pc.cod_projeto) AS cod_servico_detalhe,
            pc.cod_colaborador as cod_colaborador,
            	f_valor_recebido_projeto_mes_ano ( pc.cod_projeto, $nMes, $nAno ) AS cod_projeto,
            f_percentual_projeto_colaborador(pc.cod_projeto,pc.cod_colaborador)*100 as percentual,
            f_percentual_projeto_colaborador(pc.cod_projeto,pc.cod_colaborador) * f_valor_recebido_projeto_mes_ano(pc.cod_projeto,$nMes,$nAno) as cod_projeto_colaborador
            ";

        $sComplemento = " INNER JOIN projeto p ON pc.cod_projeto = p.cod_projeto
                    INNER JOIN servico ON servico.cod_servico = p.cod_servico
                    	INNER JOIN cliente ON p.cod_cliente = cliente.cod_cliente
                    where pc.cod_colaborador=$nCodColaborador AND f_valor_recebido_projeto_mes_ano ( pc.cod_projeto, $nMes, $nAno )>0";
//echo "SELECT " . $sCampos . " from " . $sTabelas . " " . $sComplemento;
        $voProjetoColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);


        if($voProjetoColaborador)
            return $voProjetoColaborador;
        return false;

    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoPagamento da base de dados
     *
     * @param $nEfetivacao
     * @return false|ProjetoPagamento[]
     */
    public function recuperarTodosProjetoPagamentoMesAnoPorTipo($nEfetivacao){
        $oProjetoPagamento = new ProjetoPagamento();
        $oPersistencia = new Persistencia($oProjetoPagamento);

        $sTabelas = "projeto_pagamento";
        if($nEfetivacao ==1){
            $sCampos = "distinct DATE_FORMAT(data_efetivacao,'%m/%Y') as data_efetivacao  ";
            $sComplemento = " where data_efetivacao is not null ";
        }else{
            $sCampos = "distinct DATE_FORMAT(data_previsao,'%m/%Y') as data_efetivacao  ";
            $sComplemento = " where data_efetivacao is null ";
        }
//        echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voProjetoPagamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoPagamento)
            return $voProjetoPagamento;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto Servico
     *
     * @param $nCodServico
     * @param $sDescServico
     * @param $sDescDocumento
     * @param $nPercentualEmpresa
     * @param $sInseridoPor
     * @param $sAlteradoPor
     * @param $nAtivo
     * @return Servico
     */
    public function inicializarServico($nCodServico,$sDescServico,$sDescDocumento,$nPercentualEmpresa,$sInseridoPor,$sAlteradoPor,$nAtivo,$nIdEscritorio){
        $oServico = new Servico();

        $oServico->setCodServico($nCodServico);
        $oServico->setDescServico($sDescServico);
        $oServico->setDescDocumento($sDescDocumento);
        $oServico->setPercentualEmpresaBanco($nPercentualEmpresa);
        $oServico->setInseridoPor($sInseridoPor);
        $oServico->setAlteradoPor($sAlteradoPor);
        $oServico->setAtivo($nAtivo);
        $oServico->setIdEscritorio($nIdEscritorio);
        return $oServico;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Servico
     *
     * @param $oServico
     * @return boolean
     */
    public function inserirServico($oServico){
        $oPersistencia = new Persistencia($oServico);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Servico
     *
     * @param $oServico
     * @return boolean
     */
    public function alterarServico($oServico){
        $oPersistencia = new Persistencia($oServico);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Servico
     *
     * @param $nCodServico
     * @return boolean
     */
    public function excluirServico($nCodServico){
        $oServico = new Servico();

        $oServico->setCodServico($nCodServico);
        $oPersistencia = new Persistencia($oServico);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Servico da base de dados
     *
     * @param $nCodServico
     * @return false|Servico
     */
    public function recuperarUmServico($nCodServico){
        $oServico = new Servico();
        $oPersistencia = new Persistencia($oServico);
        $sTabelas = "servico";
        $sCampos = "*";
        $sComplemento = " WHERE cod_servico = $nCodServico";
        $voServico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServico)
            return $voServico[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Servico da base de dados
     *
     * @return false|Servico[]
     */
    public function recuperarTodosServico($nIdEscritorio){
        $oServico = new Servico();
        $oPersistencia = new Persistencia($oServico);
        $sTabelas = "servico";
        $sCampos = "*";
        $sComplemento = " Where ativo = 1 And id_escritorio = $nIdEscritorio Order By desc_servico asc";

        $voServico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServico)
            return $voServico;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos Servico da base de dados
     *
     * @param $nMes
     * @param $nAno
     * @param int $nIdEscritorio
     * @return false|Servico[]
     */
    public function recuperarTodosServicoEmpresaPorMesPorAnoAno($nMes,$nAno,$nIdEscritorio=1){
        $oServico = new Servico();
        $oPersistencia = new Persistencia($oServico);
        $sTabelas = "servico";
        $sCampos = "	cod_servico as cod_servico,
                  	desc_servico as desc_servico,
                  	round(percentual_empresa / 100, 2 ) as percentual_empresa,
                  	f_valor_recebido_servico_mes_ano (cod_servico, $nMes, $nAno ) as inserido_por,
                  	f_valor_recebido_servico_mes_ano (cod_servico, $nMes, $nAno ) * round( percentual_empresa / 100, 2 ) as alterado_por";
        $sComplemento = "where id_escritorio=$nIdEscritorio";
        $voServico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServico)
            return $voServico;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto ProjetoServicoEtapaColaborador
     *
     * @return ProjetoServicoEtapaColaborador
     */
    public function inicializarProjetoServicoEtapaColaborador($nCodProjetoServicoEtapaColaborador,$nCodProjeto,$nCodColaborador,$nCodEtapa,$nPercentual){
        $oProjetoServicoEtapaColaborador = new ProjetoServicoEtapaColaborador();

        $oProjetoServicoEtapaColaborador->setCodProjetoServicoEtapaColaborador($nCodProjetoServicoEtapaColaborador);
        $oProjetoServicoEtapaColaborador->setCodProjeto($nCodProjeto);
        $oProjetoServicoEtapaColaborador->setCodColaborador($nCodColaborador);
        $oProjetoServicoEtapaColaborador->setCodEtapa($nCodEtapa);
        $oProjetoServicoEtapaColaborador->setPercentualBanco($nPercentual);
        return $oProjetoServicoEtapaColaborador;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ProjetoServicoEtapaColaborador
     *
     * @return boolean
     */
    public function inserirProjetoServicoEtapaColaborador($oProjetoServicoEtapaColaborador){
        $oPersistencia = new Persistencia($oProjetoServicoEtapaColaborador);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto ProjetoServicoEtapaColaborador
     *
     * @return boolean
     */
    public function excluirProjetoServicoEtapaColaborador($nCodProjetoServicoEtapaColaborador){
        $oProjetoServicoEtapaColaborador = new ProjetoServicoEtapaColaborador();

        $oProjetoServicoEtapaColaborador->setCodProjetoServicoEtapaColaborador($nCodProjetoServicoEtapaColaborador);
        $oPersistencia = new Persistencia($oProjetoServicoEtapaColaborador);
        $bExcluir = $oPersistencia->excluirFisicamente();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoServicoEtapaColaborador da base de dados
     *
     * @param $nCodProjeto
     * @param $nCodEtapa
     * @return false|ProjetoServicoEtapaColaborador[]
     */
    public function recuperarTodosProjetoServicoEtapaColaborador($nCodProjeto,$nCodEtapa){
        $oProjetoServicoEtapaColaborador = new ProjetoServicoEtapaColaborador();
        $oPersistencia = new Persistencia($oProjetoServicoEtapaColaborador);
        $sTabelas = "projeto_servico_etapa_colaborador";
        $sCampos = "*";
        $sComplemento = "Where cod_projeto = $nCodProjeto And cod_etapa = $nCodEtapa";
        $voProjetoServicoEtapaColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoServicoEtapaColaborador)
            return $voProjetoServicoEtapaColaborador;

        return false;
    }



    /**
     *
     * Método para inicializar um Objeto ContaBancaria
     *
     * @param $nCodContaBancaria
     * @param $nCodColaborador
     * @param $sAgencia
     * @param $sConta
     * @param $nTipo
     * @param $sBanco
     * @param $sIncluidoPor
     * @param $sAlteradoPor
     * @param $dDataEncerramento
     * @param $nAtivo
     * @param $nIdEscritorio
     * @param $nSaldoInicial
     * @param $dDataInicio
     * @return ContaBancaria
     */
    public function inicializarContaBancaria($nCodContaBancaria,$nCodColaborador,$sAgencia,$sConta,$nTipo,$sBanco,$sIncluidoPor,$sAlteradoPor,$dDataEncerramento,$nAtivo,$nIdEscritorio,$nSaldoInicial,$dDataInicio){
        $oContaBancaria = new ContaBancaria();

        $oContaBancaria->setCodContaBancaria($nCodContaBancaria);
        $oContaBancaria->setCodColaborador($nCodColaborador);
        $oContaBancaria->setAgencia($sAgencia);
        $oContaBancaria->setConta($sConta);
        $oContaBancaria->setTipo($nTipo);
        $oContaBancaria->setBanco($sBanco);
        $oContaBancaria->setIncluidoPor($sIncluidoPor);
        $oContaBancaria->setAlteradoPor($sAlteradoPor);
        $oContaBancaria->setDataEncerramentoBanco($dDataEncerramento);
        $oContaBancaria->setAtivo($nAtivo);
        $oContaBancaria->setIdEscritorio($nIdEscritorio);
        $oContaBancaria->setSaldoInicialBanco($nSaldoInicial);
        $oContaBancaria->setDataInicioBanco($dDataInicio);

        return $oContaBancaria;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ContaBancaria
     *
     * @param $oContaBancaria
     * @return boolean
     */
    public function inserirContaBancaria($oContaBancaria){
        $oPersistencia = new Persistencia($oContaBancaria);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto ContaBancaria
     *
     * @param $oContaBancaria
     * @return boolean
     */
    public function alterarContaBancaria($oContaBancaria){
        $oPersistencia = new Persistencia($oContaBancaria);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto ContaBancaria
     *
     * @param $nCodContaBancaria
     * @return boolean
     */
    public function excluirContaBancaria($nCodContaBancaria){
        $oContaBancaria = new ContaBancaria();

        $oContaBancaria->setCodContaBancaria($nCodContaBancaria);
        $oPersistencia = new Persistencia($oContaBancaria);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto ContaBancaria da base de dados
     *
     * @param $nCodContaBancaria
     * @return ContaBancaria|false
     */
    public function recuperarUmContaBancaria($nCodContaBancaria){
        $oContaBancaria = new ContaBancaria();
        $oPersistencia = new Persistencia($oContaBancaria);
        $sTabelas = "conta_bancaria";
        $sCampos = "*";
        $sComplemento = " WHERE cod_conta_bancaria = $nCodContaBancaria";
        $voContaBancaria = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voContaBancaria)
            return $voContaBancaria[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ContaBancaria da base de dados
     *
     * @return ContaBancaria[]|false
     */
    public function recuperarTodosContaBancaria($nIdEscritorio=1){
        $oContaBancaria = new ContaBancaria();
        $oPersistencia = new Persistencia($oContaBancaria);
        $sTabelas = "conta_bancaria";
        $sCampos = "cod_conta_bancaria,nome as cod_colaborador,conta_bancaria.agencia,conta_bancaria.conta,conta_bancaria.tipo,conta_bancaria.banco,conta_bancaria.incluido_por,conta_bancaria.alterado_por,data_encerramento,(f_saldo_conta(cod_conta_bancaria)) as ativo,conta_bancaria.id_escritorio as id_escritorio";
        $sComplemento = "INNER JOIN colaborador ON colaborador.cod_colaborador = conta_bancaria.cod_colaborador WHERE conta_bancaria.ativo=1 and conta_bancaria.id_escritorio=$nIdEscritorio";
        // echo "SELECT $sCampos FROM $sTabelas $sComplemento";
        $voContaBancaria = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voContaBancaria)
            return $voContaBancaria;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ContaBancaria da base de dados
     *
     * @return ContaBancaria[]|false
     */
    public function recuperarTodosContaBancariaSaldo($nIdEscritorio=1){
        $oContaBancaria = new ContaBancaria();
        $oPersistencia = new Persistencia($oContaBancaria);
        $sTabelas = "conta_bancaria cb";
        $sCampos = "	cb.cod_conta_bancaria, colaborador.nome as cod_colaborador, cb.banco,cb.agencia,cb.conta,
        	(f_saldo_conta(cb.cod_conta_bancaria)) AS valor,cb.id_escritorio as id_escritorio ";
        $sComplemento = "inner join colaborador on colaborador.cod_colaborador = cb.cod_colaborador
        where cb.ativo=1 and cb.id_escritorio=$nIdEscritorio
        ORDER BY 2 ASC";
//        echo "Select $sCampos From $sTabelas $sComplemento";
        $voContaBancaria = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voContaBancaria)
            return $voContaBancaria;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ContaBancaria da base de dados
     *
     * @param $nCodContaBancaria
     * @return ContaBancaria|false
     */
    public function recuperarUmContaBancariaUltimoSaldo($nCodContaBancaria){
        $oContaBancaria = new ContaBancaria();
        $oPersistencia = new Persistencia($oContaBancaria);
        $sTabelas = "conta_bancaria cb";
        $sCampos = "cb.cod_conta_bancaria, colaborador.nome as cod_colaborador, cb.banco,cb.agencia,cb.conta,
        (f_saldo_conta(cb.cod_conta_bancaria)) AS valor,cb.id_escritorio as id_escritorio ";
        $sComplemento = "inner join colaborador on colaborador.cod_colaborador = cb.cod_colaborador
        where cb.ativo=1 AND cb.cod_conta_bancaria=".$nCodContaBancaria."
        ORDER BY 2 ASC";
//        echo "SELECT ". $sCampos . " FROM " . $sTabelas . " " . $sComplemento;
        $voContaBancaria = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voContaBancaria)
            return $voContaBancaria[0];
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto ContaMovimentacao
     *
     * @param $nCodContaMovimentacao
     * @param $nCodContaBancaria
     * @param $sTipo
     * @param $nValorInicial
     * @param $nValorDespesa
     * @param $nValorReceita
     * @param $dDataMovimentacao
     * @param $nAtivo
     * @param $nCodMovimento
     * @param $nCodProjetoPagamento
     * @return ContaMovimentacao
     */

    public function inicializarContaMovimentacao($nCodContaMovimentacao,$nCodContaBancaria,$sTipo,$nValorInicial,$nValorDespesa,$nValorReceita,$dDataMovimentacao,$nAtivo,$nCodMovimento,$nCodProjetoPagamento){
        $oContaMovimentacao = new ContaMovimentacao();

        $oContaMovimentacao->setCodContaMovimentacao($nCodContaMovimentacao);
        $oContaMovimentacao->setCodContaBancaria($nCodContaBancaria);
        $oContaMovimentacao->setTipo($sTipo);
        $oContaMovimentacao->setValorInicialBanco($nValorInicial);
        $oContaMovimentacao->setValorDespesaBanco($nValorDespesa);
        $oContaMovimentacao->setValorReceitaBanco($nValorReceita);
        $oContaMovimentacao->setDataMovimentacaoBanco($dDataMovimentacao);
        $oContaMovimentacao->setAtivo($nAtivo);
        $oContaMovimentacao->setCodProjetoPagamento($nCodProjetoPagamento);
        $oContaMovimentacao->setCodMovimento($nCodMovimento);

        return $oContaMovimentacao;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ContaMovimentacao
     *
     * @param $oContaMovimentacao
     * @return boolean
     */
    public function inserirContaMovimentacao($oContaMovimentacao){
        $oPersistencia = new Persistencia($oContaMovimentacao);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto ContaMovimentacao
     *
     * @param $oContaMovimentacao
     * @return boolean
     */
    public function alterarContaMovimentacao($oContaMovimentacao){
        $oPersistencia = new Persistencia($oContaMovimentacao);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto ContaMovimentacao
     *
     * @param $nCodContaMovimentacao
     * @return boolean
     */
    public function excluirContaMovimentacao($nCodContaMovimentacao){
        $oContaMovimentacao = new ContaMovimentacao();

        $oContaMovimentacao->setCodContaMovimentacao($nCodContaMovimentacao);
        $oPersistencia = new Persistencia($oContaMovimentacao);
        $bExcluir = ($_SESSION[''] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto ContaMovimentacao da base de dados
     *
     * @param $nCodContaMovimentacao
     * @return false
     */
    public function recuperarUmContaMovimentacao($nCodContaMovimentacao){
        $oContaMovimentacao = new ContaMovimentacao();
        $oPersistencia = new Persistencia($oContaMovimentacao);
        $sTabelas = "conta_movimentacao";
        $sCampos = "*";
        $sComplemento = " WHERE cod_conta_movimentacao = $nCodContaMovimentacao";
        $voContaMovimentacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voContaMovimentacao)
            return $voContaMovimentacao[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ContaMovimentacao da base de dados
     *
     * @return ContaMovimentacao[]|false
     */
    public function recuperarTodosContaMovimentacao($nIdEscritorio=1){
        $oContaMovimentacao = new ContaMovimentacao();
        $oPersistencia = new Persistencia($oContaMovimentacao);
        $sTabelas = "conta_movimentacao";
        $sCampos = "conta_movimentacao.*";
        $sComplemento = "inner join conta_bancaria on conta_bancaria.cod_conta_bancaria = conta_movimentacao.cod_conta_bancaria AND id_escritorio=$nIdEscritorio";
        $voContaMovimentacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voContaMovimentacao)
            return $voContaMovimentacao;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ContaMovimentacao da base de dados
     *
     * @param $nCodContaBancaria
     * @param $dMes
     * @param $dAno
     * @return ContaMovimentacao[]|false
     */
    public function recuperarTodosContaMovimentacaoPorContaPorPeriodo($nCodContaBancaria,$nMes,$nAno,$nIdEscritorio){
        $oContaMovimentacao = new ContaMovimentacao();
        $oPersistencia = new Persistencia($oContaMovimentacao);
        $sTabelas = "v_extrato_conta";
        $sCampos = "*";
        $sComplemento = "Where cod_conta_bancaria=$nCodContaBancaria And mes_efetivacao=$nMes And ano_efetivacao=$nAno And id_escritorio=$nIdEscritorio";
//        echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        $voContaMovimentacao = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);

        if($voContaMovimentacao)
            return $voContaMovimentacao;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ContaMovimentacao da base de dados
     *
     * @param $sTipo
     * @param $nDias
     * @param int $nIdEscritorio
     * @return ContaMovimentacao[]|false
     */
    public function recuperarTodosContaMovimentacaoPorTipoPorIntervalo($sTipo,$nDias,$nIdEscritorio=1){
        $oContaMovimentacao = new ContaMovimentacao();
        $oPersistencia = new Persistencia($oContaMovimentacao);
        $sTabelas = "conta_movimentacao";
        //$sCampos = "cod_conta_movimentacao,CONCAT(colaborador.nome,' - Ag:',conta_bancaria.agencia,' - Conta: ',conta_bancaria.conta) AS ativo,conta_movimentacao.cod_conta_bancaria,conta_movimentacao.tipo,cod_movimento, cod_projeto_pagamento,valor_inicial,valor_despesa,valor_receita,data_movimentacao,	(f_descricao_conta_movimentacao ( codigo_movimento,cod_projeto_pagamento, conta_movimentacao.tipo)) AS descricao";
        $sCampos = "cod_conta_movimentacao,CONCAT(colaborador.nome,' - Ag:',conta_bancaria.agencia,' - Conta: ',conta_bancaria.conta) AS ativo,conta_movimentacao.cod_conta_bancaria,conta_movimentacao.tipo,cod_movimento, cod_projeto_pagamento,valor_inicial,valor_despesa,valor_receita,data_movimentacao";
        $sComplemento = " inner join conta_bancaria ON conta_bancaria.cod_conta_bancaria = conta_movimentacao.cod_conta_bancaria and conta_bancaria.id_escritorio=$nIdEscritorio
                          inner join colaborador ON conta_bancaria.cod_colaborador = colaborador.cod_colaborador
                          WHERE conta_movimentacao.tipo='".$sTipo."' AND data_movimentacao BETWEEN DATE_SUB(CURDATE(),INTERVAL ".$nDias." DAY) and CURRENT_DATE() AND cod_movimento is not null or cod_projeto_pagamento is not null";
        // echo "SELECT " . $sCampos ." FROM " . $sTabelas . " ". $sComplemento;
        $voContaMovimentacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voContaMovimentacao)
            return $voContaMovimentacao;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ContaMovimentacao da base de dados
     *
     * @param $nAno
     * @param $nMes
     * @param $nCodContaBancaria
     * @return ContaMovimentacao[]|false
     */
    public function recuperarUmContaMovimentacaoPrincipalAnoMesConta($nAno,$nMes,$nCodContaBancaria){
        $oContaMovimentacao = new ContaMovimentacao();
        $oPersistencia = new Persistencia($oContaMovimentacao);
        $sProcedure = "sp_extrato_conta_principal";
        $sParametros = "$nAno,$nMes,$nCodContaBancaria" ;
        $voContaMovimentacao = $oPersistencia->consultarProcedureLista($sProcedure,$sParametros);
        if($voContaMovimentacao)
            return $voContaMovimentacao[0];
        return false;

    }


    /**
     *
     * Método para inicializar um Objeto Documento
     *
     * @param $nCodDocumento
     * @param $sTitulo
     * @param $sConteudo
     * @param $sAtivo
     * @param $nIdEscritorio
     * @return Documento
     */
    public function inicializarDocumento($nCodDocumento,$sTitulo,$sAtivo){
        $oDocumento = new Documento();

        $oDocumento->setCodDocumento($nCodDocumento);
        $oDocumento->setTitulo($sTitulo);
        $oDocumento->setConteudo("");
        $oDocumento->setAtivo($sAtivo);
        return $oDocumento;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Documento
     *
     * @param $oDocumento
     * @return boolean
     */
    public function inserirDocumento($oDocumento){
        $oPersistencia = new Persistencia($oDocumento);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Documento
     *
     * @param $oDocumento
     * @return boolean
     */
    public function alterarDocumento($oDocumento){
        $oPersistencia = new Persistencia($oDocumento);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Documento
     *
     * @param $nCodDocumento
     * @return boolean
     */
    public function excluirDocumento($nCodDocumento){
        $oDocumento = new Documento();

        $oDocumento->setCodDocumento($nCodDocumento);
        $oPersistencia = new Persistencia($oDocumento);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto Documento na base de dados
     *
     * @param $nCodDocumento
     * @return boolean
     */
    public function presenteDocumento($nCodDocumento){
        $oDocumento = new Documento();

        $oDocumento->setCodDocumento($nCodDocumento);
        $oPersistencia = new Persistencia($oDocumento);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto Documento da base de dados
     *
     * @param $nCodDocumento
     * @return Documento|false
     */
    public function recuperarUmDocumento($nCodDocumento){
        $oDocumento = new Documento();
        $oPersistencia = new Persistencia($oDocumento);
        $sTabelas = "documento";
        $sCampos = "*";
        $sComplemento = " WHERE cod_documento = $nCodDocumento";
        $voDocumento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDocumento)
            return $voDocumento[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Documento da base de dados
     *
     * @return Documento[]|false
     */
    public function recuperarTodosDocumento(){
        $oDocumento = new Documento();
        $oPersistencia = new Persistencia($oDocumento);
        $sTabelas = "documento";
        $sCampos = "*";
        $sComplemento = "";
        $voDocumento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDocumento)
            return $voDocumento;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto DocumentoEscritorio
     *
     * @param $nCodDocumento
     * @param $nIdEscritorio
     * @param $sTemplate
     * @return DocumentoEscritorio
     */
    public function inicializarDocumentoEscritorio($nCodDocumento,$nIdEscritorio,$sTemplate){
        $oDocumentoEscritorio = new DocumentoEscritorio();

        $oDocumentoEscritorio->setCodDocumento($nCodDocumento);
        $oDocumentoEscritorio->setIdEscritorio($nIdEscritorio);
        $oDocumentoEscritorio->setTemplate($sTemplate);
        return $oDocumentoEscritorio;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto DocumentoEscritorio
     *
     * @param $oDocumentoEscritorio
     * @return boolean
     */
    public function inserirDocumentoEscritorio($oDocumentoEscritorio){
        $oPersistencia = new Persistencia($oDocumentoEscritorio);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto DocumentoEscritorio
     *
     * @param $oDocumentoEscritorio
     * @return boolean
     */
    public function alterarDocumentoEscritorio($oDocumentoEscritorio){
        $oPersistencia = new Persistencia($oDocumentoEscritorio);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto DocumentoEscritorio
     *
     * @param $nCodDocumento
     * @param $nIdEscritorio
     * @return boolean
     */
    public function excluirDocumentoEscritorio($nCodDocumento,$nIdEscritorio){
        $oDocumentoEscritorio = new DocumentoEscritorio();

        $oDocumentoEscritorio->setCodDocumento($nCodDocumento);
        $oDocumentoEscritorio->setIdEscritorio($nIdEscritorio);
        $oPersistencia = new Persistencia($oDocumentoEscritorio);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto DocumentoEscritorio na base de dados
     *
     * @param $nCodDocumento
     * @param $nIdEscritorio
     * @return boolean
     */
    public function presenteDocumentoEscritorio($nCodDocumento,$nIdEscritorio){
        $oDocumentoEscritorio = new DocumentoEscritorio();

        $oDocumentoEscritorio->setCodDocumento($nCodDocumento);
        $oDocumentoEscritorio->setIdEscritorio($nIdEscritorio);
        $oPersistencia = new Persistencia($oDocumentoEscritorio);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto DocumentoEscritorio da base de dados
     *
     * @param $nCodDocumento
     * @param $nIdEscritorio
     * @return DocumentoEscritorio|false
     */
    public function recuperarUmDocumentoEscritorio($nCodDocumento,$nIdEscritorio){
        $oDocumentoEscritorio = new DocumentoEscritorio();
        $oPersistencia = new Persistencia($oDocumentoEscritorio);
        $sTabelas = "documento_escritorio";
        $sCampos = "*";
        $sComplemento = " WHERE cod_documento ={$nCodDocumento} AND id_escritorio={$nIdEscritorio}";
        $voDocumentoEscritorio = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDocumentoEscritorio)
            return $voDocumentoEscritorio[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos DocumentoEscritorio da base de dados
     *
     * @return DocumentoEscritorio[]|false
     */
    public function recuperarTodosDocumentoEscritorio($nIdEscritorio=1){
        $oDocumentoEscritorio = new DocumentoEscritorio();
        $oPersistencia = new Persistencia($oDocumentoEscritorio);
        $sTabelas = "documento_escritorio";
        $sCampos = "*";
        $sComplemento = "WHERE id_escritorio=$nIdEscritorio";
        $voDocumentoEscritorio = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDocumentoEscritorio)
            return $voDocumentoEscritorio;
        return false;
    }
    /**
     *
     * Método para recuperar um vetor de objetos DocumentoEscritorio da base de dados
     *
     * @return DocumentoEscritorio[]|false
     */
    public function recuperarUmDocumentoEscritorioPorDocumento($nIdEscritorio,$nCodDocumento){
        $oDocumentoEscritorio = new DocumentoEscritorio();
        $oPersistencia = new Persistencia($oDocumentoEscritorio);
        $sTabelas = "documento_escritorio";
        $sCampos = "*";
        $sComplemento = "WHERE id_escritorio={$nIdEscritorio} AND cod_documento={$nCodDocumento}";
        $voDocumentoEscritorio = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voDocumentoEscritorio)
            return $voDocumentoEscritorio[0];
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto Escritorio
     *
     * @param $nIdEscritorio
     * @param $sNomeFantasia
     * @param $sRazaoSocial
     * @param $sDocumento
     * @param $sEnderecoReduzido
     * @param $sFone
     * @param $sEnderecoCompleto
     * @param $sSite
     * @param $sEmail
     * @param $sInstagram
     * @param $sEmailAdministrador
     * @param $sLogomarca
     * @param $sLogomarcaMini
     * @param $sCabecalhoEmail
     * @param $sRodapeEmail
     * @param $sDiasUteisCorridos
     * @param $sEnvioEmail
     * @param $sHoraTecnica
     * @param $sCidadeUf
     * @return Escritorio
     */
    public function inicializarEscritorio($nIdEscritorio,$sNomeFantasia,$sRazaoSocial,$sDocumento,$sEnderecoReduzido,$sFone,$sEnderecoCompleto,$sSite,$sEmail,$sInstagram,$sEmailAdministrador,$sLogomarca,$sLogomarcaMini,$sCabecalhoEmail,$sRodapeEmail,$sDiasUteis,$sEnvioEmail,$sHoraTecnica,$sCidadeUf,$sAssinatura){
        $oEscritorio = new Escritorio();

        $oEscritorio->setIdEscritorio($nIdEscritorio);
        $oEscritorio->setNomeFantasia($sNomeFantasia);
        $oEscritorio->setRazaoSocial($sRazaoSocial);
        $oEscritorio->setDocumento($sDocumento);
        $oEscritorio->setEnderecoReduzido($sEnderecoReduzido);
        $oEscritorio->setFone($sFone);
        $oEscritorio->setEnderecoCompleto($sEnderecoCompleto);
        $oEscritorio->setSite($sSite);
        $oEscritorio->setEmail($sEmail);
        $oEscritorio->setInstagram($sInstagram);
        $oEscritorio->setEmailAdministrador($sEmailAdministrador);
        $oEscritorio->setLogomarca($sLogomarca);
        $oEscritorio->setLogomarcaMini($sLogomarcaMini);
        $oEscritorio->setCabecalhoEmail($sCabecalhoEmail);
        $oEscritorio->setRodapeEmail($sRodapeEmail);
        $oEscritorio->setDiasUteis($sDiasUteis);
        $oEscritorio->setEnvioEmail($sEnvioEmail);
        $oEscritorio->setHoraTecnica($sHoraTecnica);
        $oEscritorio->setCidadeUf($sCidadeUf);
        $oEscritorio->setAssinatura($sAssinatura);
        return $oEscritorio;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Escritorio
     *
     * @param $oEscritorio
     * @return boolean
     */
    public function inserirEscritorio($oEscritorio){
        $oPersistencia = new Persistencia($oEscritorio);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Escritorio
     *
     * @param $oEscritorio
     * @return boolean
     */
    public function alterarEscritorio($oEscritorio){
        $oPersistencia = new Persistencia($oEscritorio);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Escritorio
     *
     * @param $nIdEscritorio
     * @return boolean
     */
    public function excluirEscritorio($nIdEscritorio){
        $oEscritorio = new Escritorio();

        $oEscritorio->setIdEscritorio($nIdEscritorio);
        $oPersistencia = new Persistencia($oEscritorio);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto Escritorio da base de dados
     *
     * @param $nIdEscritorio
     * @return Escritorio|false
     */
    public function recuperarUmEscritorio($nIdEscritorio){
        $oEscritorio = new Escritorio();
        $oPersistencia = new Persistencia($oEscritorio);
        $sTabelas = "escritorio";
        $sCampos = "*";
        $sComplemento = " WHERE id_escritorio = $nIdEscritorio";
        // echo "SELECT{$sCampos} FROM {$sTabelas} {$sComplemento}";
        // die();
        $voEscritorio = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEscritorio)
            return $voEscritorio[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Escritorio da base de dados
     *
     * @return Escritorio[]|false
     */
    public function recuperarTodosEscritorio(){
        $oEscritorio = new Escritorio();
        $oPersistencia = new Persistencia($oEscritorio);
        $sTabelas = "escritorio";
        $sCampos = "*";
        $sComplemento = "where id_escritorio=2";
        $voEscritorio = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEscritorio)
            return $voEscritorio;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Escritorio da base de dados
     *
     * @return Escritorio[]|false
     */
    public function recuperarTodosEscritorioColaborador($nCodColaborador){
        $oEscritorio = new Escritorio();
        $oPersistencia = new Persistencia($oEscritorio);
        $sTabelas = "escritorio esc";
        $sCampos = "*";
        $sComplemento = "Inner Join colaborador_escritorio_grupo AS ceg ON ceg.id_escritorio = esc.id_escritorio Where cod_colaborador = $nCodColaborador";
        $voEscritorio = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voEscritorio)
            return $voEscritorio;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto PropostaServicoEtapa
     *
     * @param $nCodEtapa
     * @param $nCodProposta
     * @param $nCodProjeto
     * @param $sDescricao
     * @param $nPrazo
     * @param $dDataPrevista
     * @param $dDataConclusao
     * @param $sObservacao
     * @return PropostaServicoEtapa
     */
    public function inicializarPropostaServicoEtapa($nCodEtapa,$nCodProposta,$nCodProjeto,$sDescricao,$nPrazo,$dDataPrevista,$dDataConclusao,$sObservacao){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();

        $oPropostaServicoEtapa->setCodEtapa($nCodEtapa);
        $oPropostaServicoEtapa->setCodProposta($nCodProposta);
        $oPropostaServicoEtapa->setCodProjeto($nCodProjeto);
        $oPropostaServicoEtapa->setDescricao($sDescricao);
        $oPropostaServicoEtapa->setPrazo($nPrazo);
        $oPropostaServicoEtapa->setDataPrevistaBanco($dDataPrevista);
        $oPropostaServicoEtapa->setDataConclusaoBanco($dDataConclusao);
        $oPropostaServicoEtapa->setObservacao($sObservacao);
        return $oPropostaServicoEtapa;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto PropostaServicoEtapa
     *
     * @param $oPropostaServicoEtapa
     * @return boolean
     */
    public function inserirPropostaServicoEtapa($oPropostaServicoEtapa){
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto PropostaServicoEtapa
     *
     * @param $oPropostaServicoEtapa
     * @return boolean
     */
    public function alterarPropostaServicoEtapa($oPropostaServicoEtapa){
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto PropostaServicoEtapa
     *
     * @param $nCodEtapa
     * @param $nCodProposta
     * @return boolean
     */
    public function excluirPropostaServicoEtapa($nCodEtapa,$nCodProposta){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();

        $oPropostaServicoEtapa->setCodEtapa($nCodEtapa);
        $oPropostaServicoEtapa->setCodProposta($nCodProposta);
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $bExcluir = $oPersistencia->excluirFisicamente();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para excluir da base de dados um Objeto PropostaMicroServico
     *
     * @param $nCodProposta
     * @return boolean
     */
    public function excluirPropostaServicoEtapaPorProposta($nCodProposta){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $bExcluir =  $oPersistencia->excluirFisicamenteSemChavePrimaria("WHERE cod_proposta=".$nCodProposta);

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto PropostaServicoEtapa da base de dados
     *
     * @param $nCodEtapa
     * @param $nCodProposta
     * @return false|PropostaServicoEtapa
     */
    public function recuperarUmPropostaServicoEtapa($nCodEtapa,$nCodProposta){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabelas = "proposta_servico_etapa";
        $sCampos = "*";
        $sComplemento = " WHERE cod_etapa = $nCodEtapa AND cod_proposta = $nCodProposta";
        $voPropostaServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa[0];
        return false;
    }



    /**
     *
     * Método para recuperar um objeto PropostaServicoEtapa da base de dados
     *
     * @param $nCodEtapa
     * @param $nCodProposta
     * @return false|PropostaServicoEtapa
     */
    public function presentePropostaServicoEtapa($nCodEtapa,$nCodProposta){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabelas = "proposta_servico_etapa";
        $sCampos = "*";
        $sComplemento = " WHERE cod_etapa = $nCodEtapa AND cod_proposta = $nCodProposta";
        $voPropostaServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return true;
        return false;
    }


    /**
     *
     * Método para recuperar um objeto PropostaServicoEtapa da base de dados
     *
     * @param $nCodEtapa
     * @param $nCodProposta
     * @return false|PropostaServicoEtapa
     */
    public function recuperarUmPropostaServicoEtapaPrazoPorProposta($nCodProposta){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabelas = "proposta_servico_etapa";
        $sCampos = "sum(prazo) as prazo";
        $sComplemento = " WHERE  cod_proposta = $nCodProposta";
        $voPropostaServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa[0];
        return false;
    }



    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @return false|PropostaServicoEtapa[]
     */
    public function recuperarTodosPropostaServicoEtapa($nStatus=0,$nIdEscritorio=1){

        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabelas = "lista_etapas";
        $sCampos = "*";
        $sComplemento = " WHERE prazo<>0 and id_escritorio=$nIdEscritorio";
        switch($nStatus){
            case 0: $sComplemento .= " AND data_conclusao is null "; break;
            case 1: $sComplemento .= " AND data_conclusao is not null order by data_conclusao desc"; break;

        }

        $voPropostaServicoEtapa = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa;
        return false;
    }
    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @return false|PropostaServicoEtapa[]
     */
    public function recuperarTotalDiasUteisProposta($nCodProposta){

        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabelas = "proposta_servico_etapa";
        $sCampos = "sum(prazo) as prazo";
        $sComplemento = "where cod_proposta={$nCodProposta}";
        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voPropostaServicoEtapa = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa[0]->prazo;
        return false;

    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @return false|PropostaServicoEtapa[]
     */
    public function recuperarTodosPropostaServicoEtapaPorProjeto($nCodProjeto){

        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabelas = "v_lista_microservico";
        $sCampos = "DISTINCT *";
        $sComplemento = " WHERE prazo<>0  and cod_projeto=$nCodProjeto";
        // $sComplemento = " WHERE prazo<>0 and dias>=1 and cod_projeto=$nCodProjeto";
        // echo "Select $sCampos From $sTabelas $sComplemento";
        $voPropostaServicoEtapa = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa;
        return false;
    }

    public function recuperarTodosPropostaServicoEtapaEmAndamento($nIdEscritorio)
    {
        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);

        $sTabelas = "lista_etapas";
        $sCampos = "*";
        $sComplemento = "Where id_escritorio = $nIdEscritorio And prazo <> 0 And cod_projeto Is Not Null Order By nome";
        //echo "SELECT $sCampos from $sTabelas $sComplemento";
        $voPropostaServicoEtapa = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @return false|PropostaServicoEtapa[]
     */
    public function recuperarTodosPropostaServicoEtapaEmAndamento_($nIdEscritorio=1){

        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);

        $sTabelas = "lista_etapas le";
        $sCampos = "distinct le.cod_projeto,le.cod_proposta,concat(le.nome,' [',le.identificacao,']') as projeto,
					 le1.data_prevista as etapa1_prevista,
					 DATE_FORMAT(le1.data_prevista ,'%d/%m/%Y')as etapa1_prevista_formatada,
					 le1.data_conclusao as etapa1_conclusao,
					 le1.etapa_observacao as etapa1_observacao,
					 le1.cod_etapa as etapa1,
					 le1.ordem as ordem1,

					 le2.data_prevista as etapa2_prevista,
					 DATE_FORMAT(le2.data_prevista ,'%d/%m/%Y')as etapa2_prevista_formatada,
					 le2.data_conclusao as etapa2_conclusao,
					 le2.etapa_observacao as etapa2_observacao,
					 le2.cod_etapa as etapa2,
					 le2.ordem as ordem2,

					 le3.data_prevista  as etapa3_prevista,
					 DATE_FORMAT(le3.data_prevista ,'%d/%m/%Y')as etapa3_prevista_formatada,
					 le3.data_conclusao as etapa3_conclusao,
					 le3.etapa_observacao  as etapa3_observacao,
					 le3.cod_etapa as etapa3,
					 le3.ordem as ordem3";

        $sComplemento = "left join lista_etapas le1 on le.cod_projeto = le1.cod_projeto and le1.cod_etapa=(SELECT distinct CASE cod_servico WHEN '4' THEN 1 WHEN '1' THEN 12 WHEN '5' THEN 5 ELSE (99) END AS etapa1 FROM lista_etapas WHERE cod_projeto=le.cod_projeto limit 1) -- etapa 1
					left join lista_etapas le2 on le2.cod_projeto = le.cod_projeto and le2.cod_etapa=(SELECT distinct CASE cod_servico WHEN '4' THEN 2 WHEN '1' THEN 13 WHEN '5' THEN 6 ELSE (99) END AS etapa2 FROM lista_etapas WHERE cod_projeto=le.cod_projeto limit 1) -- etapa 2
					left join lista_etapas le3 on le3.cod_projeto = le.cod_projeto and le3.cod_etapa=(SELECT distinct CASE cod_servico WHEN '4' THEN 3 WHEN '1' THEN 14 WHEN '5' THEN 7 ELSE (99) END AS etapa2 FROM lista_etapas WHERE cod_projeto=le.cod_projeto limit 1) -- etapa 2
													where le.id_escritorio=$nIdEscritorio and le.prazo<>0 AND le.data_conclusao is null and le.cod_projeto is not null";
        //echo "SELECT $sCampos from $sTabelas $sComplemento";
        $voPropostaServicoEtapa = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa;
        return false;
    }






    /**
     *
     * Método para recuperar um vetor de objetos PropostaServicoEtapa da base de dados
     *
     * @param $nCodProposta
     * @param null $nCodProjeto
     * @return false|PropostaServicoEtapa[]
     */
    public function recuperarTodosPropostaServicoEtapaPorProposta($nCodProposta=null,$nCodProjeto=null){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabelas = "proposta_servico_etapa";
        $sCampos = "*";
        $sComplemento = ($nCodProjeto) ? " Where cod_projeto = $nCodProjeto" : "Where cod_proposta = $nCodProposta";
        // echo "Select * From $sTabelas $sComplemento";
        $voPropostaServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaServicoEtapa)
            return $voPropostaServicoEtapa;
        return false;
    }






    /**
     *
     * Método para inicializar um Objeto PropostaMicroServico
     *
     * @param $nCodMicroservico
     * @param $nCodProposta
     * @param $nCodProjeto
     * @param $nQuantidade
     * @param $sDescricaoQuantidade
     * @param $nDias
     * @param $dDataPrevisaoInicio
     * @param $dDataEfetivacaoInicio
     * @param $dDataPrevisaoFim
     * @param $dDataEfetivacaoFim
     * @param $sObservacao
     * @param $sInseridoPor
     * @param $sAlteradoPor
     * @return PropostaMicroServico
     */
    public function inicializarPropostaMicroservico($nCodMicroservico,$nCodProposta,$nCodProjeto,$nQuantidade,$sDescricaoQuantidade,$nDias,$dDataPrevisaoInicio,$dDataEfetivacaoInicio,$dDataPrevisaoFim,$dDataEfetivacaoFim,$sObservacao,$sInseridoPor,$sAlteradoPor): PropostaMicroServico
    {
        $oPropostaMicroservico = new PropostaMicroServico();

        $oPropostaMicroservico->setCodMicroservico($nCodMicroservico);
        $oPropostaMicroservico->setCodProposta($nCodProposta);
        $oPropostaMicroservico->setCodProjeto($nCodProjeto);
        $oPropostaMicroservico->setQuantidade($nQuantidade);
        $oPropostaMicroservico->setDescricaoQuantidade($sDescricaoQuantidade);
        $oPropostaMicroservico->setDias($nDias);
        $oPropostaMicroservico->setDataPrevisaoInicioBanco($dDataPrevisaoInicio);
        $oPropostaMicroservico->setDataEfetivacaoInicioBanco($dDataEfetivacaoInicio);
        $oPropostaMicroservico->setDataPrevisaoFimBanco($dDataPrevisaoFim);
        $oPropostaMicroservico->setDataEfetivacaoFimBanco($dDataEfetivacaoFim);
        $oPropostaMicroservico->setObservacao($sObservacao);
        $oPropostaMicroservico->setInseridoPor($sInseridoPor);
        $oPropostaMicroservico->setAlteradoPor($sAlteradoPor);

        return $oPropostaMicroservico;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto oPropostaMicroservico
     *
     * @param $oPropostaMicroservico
     * @return boolean
     */
    public function inserirPropostaMicroservico($oPropostaMicroservico){
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto oPropostaMicroservico
     *
     * @param $oPropostaMicroservico
     * @return boolean
     */
    public function alterarPropostaMicroservico($oPropostaMicroservico){
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para alterar na base de dados um Objeto oPropostaMicroservico
     *
     * @param $nCodProjeto
     * @return boolean
     */
    public function alterarPropostaMicroservicoPorProjeto($nCodProjeto){
        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabela = "proposta_microservico";
        $sCampos = "cod_projeto = null";
        $sComplemento = "WHERE cod_projeto = $nCodProjeto";
        if($oPersistencia->AlteraSomenteUmaColuna($sTabela,$sCampos,$sComplemento))
            return true;
        return false;
    }


    /**
     *
     * Método para alterar na base de dados um Objeto oPropostaMicroservico
     *
     * @param $nCodProposta
     * @param $nCodMicroServico
     * @param $nDias
     * @return boolean
     */
    public function alterarPropostaMicroservicoPrazo($nCodProposta,$nCodMicroServico,$nDias){
        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabela = "proposta_microservico";
        $sCampos = "dias=$nDias";
        $sComplemento = "WHERE cod_proposta = $nCodProposta AND cod_microservico=$nCodMicroServico";
        if($oPersistencia->AlteraSomenteUmaColuna($sTabela,$sCampos,$sComplemento))
            return true;
        return false;
    }
    /**
     *
     * Método para alterar na base de dados um Objeto oPropostaMicroservico
     *
     * @param $nCodProjeto
     * @return boolean
     */
    public function alterarPropostaServicoEtapaPorProjeto($nCodProjeto){
        $oPropostaServicoEtapa = new PropostaServicoEtapa();
        $oPersistencia = new Persistencia($oPropostaServicoEtapa);
        $sTabela = "proposta_servico_etapa";
        $sCampos = "cod_projeto = null";
        $sComplemento = "WHERE cod_projeto = $nCodProjeto";
        if($oPersistencia->AlteraSomenteUmaColuna($sTabela,$sCampos,$sComplemento))
            return true;
        return false;
    }

    /**
     *
     * Método para excluir da base de dados um Objeto oPropostaMicroservico
     *
     * @param $nCodMicroservico
     * @param $nCodProposta
     * @return boolean
     */
    public function excluirPropostaMicroservico($nCodMicroservico,$nCodProposta){
        $oPropostaMicroservico = new PropostaMicroServico();

        $oPropostaMicroservico->setCodMicroservico($nCodMicroservico);
        $oPropostaMicroservico->setCodProposta($nCodProposta);
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para excluir da base de dados um Objeto oPropostaMicroservico
     *
     * @param $nCodProposta
     * @return boolean
     */
    public function excluirPropostaMicroservicoPorProposta($nCodProposta){
        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $bExcluir =  $oPersistencia->excluirFisicamenteSemChavePrimaria("WHERE cod_proposta=".$nCodProposta);

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto PropostaMicroServico da base de dados
     *
     * @param $nCodProjeto
     * @param null $nCodServicoEtapa
     * @return false|PropostaMicroServico
     */
    public function verificaUltimaEtapaPropostaMicroservico($nCodProjeto,$nCodServicoEtapa=null){
        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabelas = "v_lista_microservico";
        $sCampos = "count(*) as cod_etapa";
        if ($nCodServicoEtapa)
            $sComplemento = "Where cod_projeto = $nCodProjeto and cod_servico_etapa = $nCodServicoEtapa AND data_efetivacao_fim_microservico is null";
        else
            $sComplemento = "Where cod_projeto = $nCodProjeto And data_efetivacao_fim_microservico is null";
//        echo "Select $sCampos From $sTabelas $sComplemento";
        $voPropostaMicroservico = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaMicroservico)
            return $voPropostaMicroservico[0]->cod_etapa;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto PropostaMicroServico da base de dados
     *
     * @param $nCodMicroservico
     * @param $nCodProposta
     * @return false|PropostaMicroServico
     */
    public function recuperarUmPropostaMicroservico($nCodMicroservico,$nCodProposta){
        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabelas = "proposta_microservico";
        $sCampos = "*";
        $sComplemento = " WHERE cod_microservico = $nCodMicroservico AND cod_proposta = $nCodProposta";
        $voPropostaMicroservico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaMicroservico)
            return $voPropostaMicroservico[0];
        return false;
    }


    /**
     *
     * Método para recuperar um objeto PropostaMicroServico da base de dados
     *
     * @param $nCodProjeto
     * @return false|PropostaMicroServico
     */
    public function recuperarUmPropostaMicroservicoPrazoMaximoPorProjeto($nCodProjeto){
        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabelas = "proposta_microservico";
        $sCampos = "max(data_previsao_fim) as data_previsao_fim";
        $sComplemento = " WHERE cod_projeto = $nCodProjeto";
        $voPropostaMicroservico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaMicroservico)
            return $voPropostaMicroservico[0]->getDataPrevisaoFim();
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoServicoEtapa da base de dados
     *
     * @return false|PropostaMicroServico[]
     */
    public function recuperarTodosPropostaMicroservico($nStatus=0,$nIdEscritorio=1){

        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabelas = "lista_etapas";
        $sCampos = "*";
        $sComplemento = " WHERE prazo<>0 and id_escritorio=$nIdEscritorio";
        switch($nStatus){
            case 0: $sComplemento .= " AND data_conclusao is null "; break;
            case 1: $sComplemento .= " AND data_conclusao is not null order by date(data_conclusao) desc"; break;

        }
        // echo "SELECT * from {$sTabelas} {$sComplemento}";
        $voPropostaMicroservico = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaMicroservico)
            return $voPropostaMicroservico;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ProjetoServicoEtapa da base de dados
     *
     * @return false|PropostaMicroServico[]
     */
    public function recuperarTodosPropostaMicroservicoPorProposta($nCodProposta,$nPrazo=3){

        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabelas = "proposta_microservico";
        // $sCampos = "proposta_microservico.cod_microservico,cod_proposta,servico_microservico.descricao as cod_projeto,quantidade,descricao_quantidade,dias,data_previsao_inicio,data_efetivacao_inicio,data_previsao_fim,data_efetivacao_fim,observacao,inserido_por,alterado_por";
        $sCampos = "proposta_microservico.cod_microservico,cod_proposta,servico_microservico.descricao as cod_projeto,quantidade,descricao_quantidade,dias,data_previsao_inicio,data_efetivacao_inicio,data_previsao_fim,data_efetivacao_fim,observacao,inserido_por,alterado_por";
        $sComplemento = "
        inner JOIN servico_microservico ON proposta_microservico.cod_microservico = servico_microservico.cod_microservico
        INNER JOIN servico_etapa ON servico_etapa.cod_servico_etapa = servico_microservico.cod_etapa
        where cod_proposta=".$nCodProposta;


        switch($nPrazo){
            case 0: $sComplemento .= " AND prazo=0 order by servico_etapa.ordem, servico_microservico.ordem"; break;
            case 1: $sComplemento .= " AND prazo=1 order by servico_etapa.ordem, servico_microservico.ordem"; break;
            default: $sComplemento.= " order by servico_etapa.ordem, servico_microservico.ordem"; break;

        }
//        echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voPropostaMicroservico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaMicroservico)
            return $voPropostaMicroservico;
        return false;
    }

    public function recuperarTodosPropostaMicroservicoPorPropostaEtapa($nCodProposta,$nCodEtapa){

        $oServicoMicroservico = new ServicoMicroservico();
        $oPersistencia = new Persistencia($oServicoMicroservico);
        $sTabelas = "servico_microservico";
        $sCampos = "*";
        $sComplemento = " INNER JOIN proposta_microservico on servico_microservico.cod_microservico = proposta_microservico.cod_microservico WHERE cod_proposta=$nCodProposta  and cod_etapa=$nCodEtapa order by servico_microservico.ordem asc ";
        // echo "SELECT * FROM {$sTabelas} {$sComplemento}";
        // die();
        $voServicoMicroservico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoMicroservico)
            return $voServicoMicroservico;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaMicroServico da base de dados
     *
     * @return false|PropostaMicroServico[]
     */
    public function recuperarTodosPropostaMicroservicoPorProjetoEtapaView($nCodProjeto,$nCodServicoEtapa){

        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabelas = "v_lista_microservico";
        $sCampos = "*";
        $sComplemento = " WHERE prazo<>0 and cod_projeto=$nCodProjeto AND cod_servico_etapa=$nCodServicoEtapa order by ordem_microservico,data_previsao_inicio_microservico asc ";
//        echo "SELECT $sCampos FROM $sTabelas $sComplemento";

        $voPropostaMicroservico = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voPropostaMicroservico)
            return $voPropostaMicroservico;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto Proposta
     *
     * @param $nCodProposta
     * @param $nCodCliente
     * @param $nCodServico
     * @param $sNome
     * @param $sDescricao
     * @param $sIdentificacao
     * @param $nValorProposta
     * @param $nValorAvista
     * @param $sValorParcelaAprazo
     * @param $nCodStatus
     * @param $dDataProposta
     * @param $nVisitasIncluidas
     * @param $sInseridoPor
     * @param $sAlteradoPor
     * @param $nIdEscritorio
     * @param $nAnoProposta
     * @param $nNumeroProposta
     * @param $sEntregaParcial
     * @param $sFormaPagamento
     * @return Proposta
     */
    public function inicializarProposta($nCodProposta,$nCodCliente,$nCodServico,$sNome,$sDescricao,$sIdentificacao,$nValorProposta,$nValorAvista,$sValorParcelaAprazo,$nCodStatus,$dDataProposta,$nVisitasIncluidas,$sInseridoPor,$sAlteradoPor,$nIdEscritorio,$nNumeroProposta,$nAnoProposta,$sEntregaParcial,$sFormaPagamento,$sPrazo){
        $oProposta = new Proposta();

        $oProposta->setCodProposta($nCodProposta);
        $oProposta->setCodCliente($nCodCliente);
        $oProposta->setCodServico($nCodServico);
        $oProposta->setNome(mb_strtoupper($sNome));
        $oProposta->setDescricao($sDescricao);
        $oProposta->setIdentificacao(mb_strtoupper($sIdentificacao));
        $oProposta->setCodStatus($nCodStatus);
        $oProposta->setDataPropostaBanco($dDataProposta);
        $oProposta->setValorPropostaBanco($nValorProposta);
        $oProposta->setValorAvistaBanco($nValorAvista);
        $oProposta->setValorParcelaAprazo($sValorParcelaAprazo);
        $oProposta->setVisitasIncluidas($nVisitasIncluidas);
        $oProposta->setInseridoPor($sInseridoPor);
        $oProposta->setAlteradoPor($sAlteradoPor);
        $oProposta->setIdEscritorio($nIdEscritorio);
        $oProposta->setNumeroProposta($nNumeroProposta);
        $oProposta->setAnoProposta($nAnoProposta);
        $oProposta->setEntregaParcial($sEntregaParcial);
        $oProposta->setFormaPagamento($sFormaPagamento);
        $oProposta->setPrazo($sPrazo);
        return $oProposta;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Proposta
     *
     * @param $oProposta
     * @return boolean
     */
    public function inserirProposta($oProposta){
        $oPersistencia = new Persistencia($oProposta);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Proposta
     *
     * @param $oProposta
     * @return boolean
     */
    public function alterarProposta($oProposta){
        $oPersistencia = new Persistencia($oProposta);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para recuperar um objeto Proposta da base de dados
     *
     * @param $nCodProposta
     * @return false|Proposta
     */
    public function recuperarUmProposta($nCodProposta){
        $oProposta = new Proposta();
        $oPersistencia = new Persistencia($oProposta);
        $sTabelas = "proposta";
        $sCampos = "*";
        $sComplemento = " WHERE cod_proposta = $nCodProposta";
        $voProposta = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProposta)
            return $voProposta[0];
        return false;
    }

    public function recuperarProximoProposta($nIdEscritorio,$nAno){
        $oProposta = new Proposta();
        $oPersistencia = new Persistencia($oProposta);
        $sTabelas = "proposta";
        $sCampos = "IFNULL(max(numero_proposta)+1,1) as numero_proposta";
        $sComplemento = " WHERE id_escritorio={$nIdEscritorio} and ano_proposta={$nAno}";
        // echo "SELECT {$sCampos} FROM proposta ". $sComplemento;
        $voProposta = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProposta)
            return $voProposta[0];
        return false;
    }



    /**
     *
     * Método para recuperar um vetor de objetos Proposta da base de dados
     *
     * @param int $nIdEscritorio
     * @return false|Proposta[]
     */
    public function recuperarTodosProposta($nIdEscritorio=1){
        $oProposta = new Proposta();
        $oPersistencia = new Persistencia($oProposta);
        $sTabelas = "proposta";
        $sCampos = "numero_proposta,ano_proposta,status.cor as visitas_incluidas,status.descricao as cod_status,servico.desc_servico as cod_servico,cod_proposta,cod_cliente,nome,proposta.descricao as descricao,proposta.identificacao as identificacao,data_proposta,valor_proposta,valor_avista,valor_parcela_aprazo,proposta.inserido_por as inserido_por,proposta.alterado_por as alterado_por,proposta.id_escritorio";
        $sComplemento = " INNER JOIN servico ON proposta.cod_servico = servico.cod_servico
						  INNER JOIN `status` ON proposta.cod_status = `status`.cod_status
              where proposta.id_escritorio=$nIdEscritorio";
        //  echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;

        $voProposta = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProposta)
            return $voProposta;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Proposta da base de dados
     *
     * @param $nCodCliente
     * @return false|Proposta[]
     */
    public function recuperarTodosPropostaPorCliente($nCodCliente){
        $oProposta = new Proposta();
        $oPersistencia = new Persistencia($oProposta);
        $sTabelas = "proposta";
        $sCampos = "*";
        $sComplemento = " where cod_cliente=$nCodCliente";
        //  echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;

        $voProposta = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProposta)
            return $voProposta;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Proposta da base de dados
     *
     * @param int $nCodStatus
     * @return false|Proposta[]
     */
    public function recuperarTodosPropostaSemProjeto($nCodStatus,$nIdEscritorio){
        $oProposta = new Proposta();
        $oPersistencia = new Persistencia($oProposta);
        $sTabelas = "proposta";
        $sCampos = "*";
        $sComplemento = "Where cod_status In($nCodStatus) and id_escritorio=$nIdEscritorio Order By nome";
//        echo "SELECT $sCampos FROM $sTabelas $sComplemento";

        $voProposta = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProposta)
            return $voProposta;
        return false;
    }




    /**
     *
     * Método para recuperar um vetor de objetos Proposta da base de dados
     *
     * @param int $nCodStatus
     * @return false|Proposta[]
     */
    public function recuperarTodosPropostaPorPrazoSemResposta($nIdEscritorio,$nDias,$nCodStatus){
        $oProposta = new Proposta();
        $oPersistencia = new Persistencia($oProposta);
        $sTabelas = "proposta";
        $sCampos = "*";
        $sComplemento = "where id_escritorio = $nIdEscritorio AND cod_status = $nCodStatus AND data_proposta < ADDDATE(CURRENT_DATE(), INTERVAL -$nDias DAY)";
        // echo "SELECT $sCampos FROM $sTabelas $sComplemento";

        $voProposta = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProposta)
            return $voProposta;
        return false;
    }
    /**
     *
     * Método para inicializar um Objeto PropostaFracionada
     *
     * @return PropostaFracionada
     */
    public function inicializarPropostaFracionada($nCodFracionamento,$nCodProposta,$nNumeroEntrega,$sDescricao,$nPercentual,$dDataPrevista,$dDataEfetivacao,$nValor,$nParcelas,$nIndiceCorrecao,$nValorFinal): PropostaFracionada
    {
        $oPropostaFracionada = new PropostaFracionada();

        $oPropostaFracionada->setCodFracionamento($nCodFracionamento);
        $oPropostaFracionada->setCodProposta($nCodProposta);
        $oPropostaFracionada->setNumeroEntrega($nNumeroEntrega);
        $oPropostaFracionada->setDescricao($sDescricao);
        $oPropostaFracionada->setPercentualBanco($nPercentual);
        $oPropostaFracionada->setDataPrevistaBanco($dDataPrevista);
        $oPropostaFracionada->setDataEfetivacaoBanco($dDataEfetivacao);
        $oPropostaFracionada->setValorBanco($nValor);
        $oPropostaFracionada->setParcelas($nParcelas);
        $oPropostaFracionada->setIndiceCorrecaoBanco($nIndiceCorrecao);
        $oPropostaFracionada->setValorFinalBanco($nValorFinal);
        return $oPropostaFracionada;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto PropostaFracionada
     *
     * @return boolean
     */
    public function inserirPropostaFracionada($oPropostaFracionada){
        $oPersistencia = new Persistencia($oPropostaFracionada);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto PropostaFracionada
     *
     * @return boolean
     */
    public function alterarPropostaFracionada($oPropostaFracionada){
        $oPersistencia = new Persistencia($oPropostaFracionada);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto PropostaFracionada
     *
     * @return boolean
     */
    public function excluirPropostaFracionada($nCodFracionamento){
        $oPropostaFracionada = new PropostaFracionada();

        $oPropostaFracionada->setCodFracionamento($nCodFracionamento);
        $oPersistencia = new Persistencia($oPropostaFracionada);
        $bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto PropostaFracionada na base de dados
     *
     * @return boolean
     */
    public function presentePropostaFracionada($nCodFracionamento){
        $oPropostaFracionada = new PropostaFracionada();

        $oPropostaFracionada->setCodFracionamento($nCodFracionamento);
        $oPersistencia = new Persistencia($oPropostaFracionada);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto PropostaFracionada da base de dados
     *
     * @return PropostaFracionada
     */
    public function recuperarUmPropostaFracionada($nCodFracionamento){
        $oPropostaFracionada = new PropostaFracionada();
        $oPersistencia = new Persistencia($oPropostaFracionada);
        $sTabelas = "proposta_fracionada";
        $sCampos = "*";
        $sComplemento = " WHERE cod_fracionamento = $nCodFracionamento";
        $voPropostaFracionada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaFracionada)
            return $voPropostaFracionada[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos PropostaFracionada da base de dados
     *
     * @return false|PropostaFracionada[]
     */
    public function recuperarTodosPropostaFracionada(){
        $oPropostaFracionada = new PropostaFracionada();
        $oPersistencia = new Persistencia($oPropostaFracionada);
        $sTabelas = "proposta_fracionada";
        $sCampos = "*";
        $sComplemento = "";
        $voPropostaFracionada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaFracionada)
            return $voPropostaFracionada;
        return false;
    }
    /**
     *
     * Método para recuperar um vetor de objetos PropostaFracionada da base de dados
     *
     * @return false|PropostaFracionada[]
     */
    public function recuperarTodosPropostaFracionadaPorProposta($nIdProposta){
        $oPropostaFracionada = new PropostaFracionada();
        $oPersistencia = new Persistencia($oPropostaFracionada);
        $sTabelas = "proposta_fracionada";
        $sCampos = "*";
        $sComplemento = "where cod_proposta={$nIdProposta}";
        $voPropostaFracionada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaFracionada)
            return $voPropostaFracionada;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto Reuniao
     *
     * @return Reuniao
     */
    public function inicializarReuniao($nCodReuniao,$nCodProjeto,$dDataReuniao,$sHoraReuniao,$sResponsavel,$sParticipantes,$sAssunto,$sLocal,$sDecisao,$sObservacao,$sInseridoPor,$nCodCliente,$sDeAcordo,$sObsCliente,$dDataCliente,$nIdEscritorio): Reuniao
    {
        $oReuniao = new Reuniao();

        $oReuniao->setCodReuniao($nCodReuniao);
        $oReuniao->setCodProjeto($nCodProjeto);
        $oReuniao->setDataReuniaoBanco($dDataReuniao);
        $oReuniao->setHoraReuniao($sHoraReuniao);
        $oReuniao->setResponsavel($sResponsavel);
        $oReuniao->setParticipantes($sParticipantes);
        $oReuniao->setAssunto($sAssunto);
        $oReuniao->setLocal($sLocal);
        $oReuniao->setDecisao($sDecisao);
        $oReuniao->setObservacao($sObservacao);
        $oReuniao->setInseridoPor($sInseridoPor);
        $oReuniao->setCodCliente($nCodCliente);
        $oReuniao->setDeAcordo($sDeAcordo);
        $oReuniao->setObsCliente($sObsCliente);
        $oReuniao->setDataClienteBanco($dDataCliente);
        $oReuniao->setIdEscritorio($nIdEscritorio);
        return $oReuniao;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Reuniao
     *
     * @return boolean
     */
    public function inserirReuniao($oReuniao){
        $oPersistencia = new Persistencia($oReuniao);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Reuniao
     *
     * @return boolean
     */
    public function alterarReuniao($oReuniao){
        $oPersistencia = new Persistencia($oReuniao);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto Reuniao
     *
     * @return boolean
     */
    public function excluirReuniao($nCodReuniao){
        $oReuniao = new Reuniao();

        $oReuniao->setCodReuniao($nCodReuniao);
        $oPersistencia = new Persistencia($oReuniao);
        $bExcluir = ($_SESSION['oUsuarioLogadoArqmanager'] && $_SESSION['oUsuarioLogadoArqmanager']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto Reuniao na base de dados
     *
     * @return boolean
     */
    public function presenteReuniao($nCodReuniao){
        $oReuniao = new Reuniao();

        $oReuniao->setCodReuniao($nCodReuniao);
        $oPersistencia = new Persistencia($oReuniao);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto Reuniao da base de dados
     *
     * @return false
     */
    public function recuperarUmReuniao($nCodReuniao){
        $oReuniao = new Reuniao();
        $oPersistencia = new Persistencia($oReuniao);
        $sTabelas = "reuniao";
        $sCampos = "*";
        $sComplemento = " WHERE cod_reuniao = $nCodReuniao";
        $voReuniao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voReuniao)
            return $voReuniao[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Reuniao da base de dados
     *
     * @return false|Reuniao[]
     */
    public function recuperarTodosReuniao($nIdEscritorio){
        $oReuniao = new Reuniao();
        $oPersistencia = new Persistencia($oReuniao);
        $sTabelas = "reuniao";
        $sCampos = "v_projeto.identificacao as cod_projeto,
									v_projeto.nome as cod_cliente,
									reuniao.cod_reuniao,
									reuniao.data_reuniao,
									reuniao.hora_reuniao,
									reuniao.responsavel,
									reuniao.participantes,
									reuniao.assunto,
									reuniao.`local`,
									reuniao.decisao,
									reuniao.recomendacao,
									reuniao.observacao,
									reuniao.inserido_por,
									reuniao.de_acordo,
									reuniao.obs_cliente,
									reuniao.data_cliente,
									reuniao.id_escritorio";
        $sComplemento = "	INNER JOIN v_projeto ON reuniao.cod_projeto = v_projeto.cod_projeto AND reuniao.cod_cliente = v_projeto.cod_cliente AND reuniao.id_escritorio = v_projeto.id_escritorio
												WHERE reuniao.id_escritorio={$nIdEscritorio}";
        $voReuniao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voReuniao)
            return $voReuniao;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto ServicoEtapa da base de dados
     *
     * @param $nCodEtapa
     * @param $sDescricao
     * @param $nOrdem
     * @param $nExibir
     * @param $nIdEscritorio
     * @return Etapa
     */
    public function inicializarEtapa($nCodEtapa,$sDescricao,$nOrdem,$nExibir,$nIdEscritorio): Etapa
    {
        $oEtapa = new Etapa();

        $oEtapa->setCodEtapa($nCodEtapa);
        $oEtapa->setDescricao($sDescricao);
        $oEtapa->setOrdem($nOrdem);
        $oEtapa->setExibir($nExibir);
        $oEtapa->setIdEscritorio($nIdEscritorio);
        return $oEtapa;
    }

    /**
     *
     * Método para recuperar um objeto ServicoEtapa da base de dados
     *
     * @param $nCodEtapa
     * @return false|Etapa
     */
    public function recuperarUmEtapa($nCodEtapa){
        $oEtapa = new Etapa();
        $oPersistencia = new Persistencia($oEtapa);
        $sTabelas = "etapa";
        $sCampos = "*";
        $sComplemento = " WHERE cod_etapa = $nCodEtapa";
        $oEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($oEtapa)
            return $oEtapa[0];
        return false;
    }

    /**
     *
     * Método para recuperar um objeto ServicoEtapa da base de dados
     *
     * @param $nIdEscritorio
     * @return false|array
     */
    public function recuperarTodosEtapa($nIdEscritorio){
        $oServicoEtapa = new Etapa();
        $oPersistencia = new Persistencia($oServicoEtapa);
        $sTabelas = "etapa";
        $sCampos = "*";
        $sComplemento = " WHERE id_escritorio = $nIdEscritorio And exibir = 1";
        $voServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoEtapa)
            return $voServicoEtapa;
        return false;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Etapa
     *
     * @param $oEtapa
     * @return boolean
     */
    public function inserirEtapa($oEtapa){
        $oPersistencia = new Persistencia($oEtapa);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto Etapa
     *
     * @param $oEtapa
     * @return boolean
     */
    public function alterarEtapa($oEtapa): bool
    {
        $oPersistencia = new Persistencia($oEtapa);
        if($oPersistencia->alterar())
            return true;
        return false;
    }

    /**
     *
     * Método para excluir da base de dados um Objeto Etapa
     *
     * @param $nCodEtapa
     * @return boolean
     */
    public function excluirEtapa($nCodEtapa): bool
    {
        $oEtapa = new Etapa();

        $oEtapa->setCodEtapa($nCodEtapa);
        $oPersistencia = new Persistencia($oEtapa);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto ServicoEtapa da base de dados
     *
     * @param $nIdEscritorio
     * @return false|array
     */
    public function recuperarTodosProjetoEmAndamento($nIdEscritorio){
        $oProjeto = new Projeto();
        $oPersistencia = new Persistencia($oProjeto);
        $sTabelas = "proposta_servico_etapa pse";
        $sCampos = "Distinct sea.cod_servico_etapa AS cod_servico_etapa,
                    sea.cod_servico AS cod_servico,
                    sea.ordem AS ordem,
                    eta.cod_etapa AS cod_etapa,

                    pse.cod_projeto AS cod_projeto,
                    prp.cod_proposta AS cod_proposta,
                    concat(prp.nome,' [',prp.identificacao,']') as projeto,
                    prp.id_escritorio AS id_escritorio,
                    arq.flag_calendario AS flag_calendario";
        $sComplemento = "
        JOIN projeto pro ON pse.cod_projeto = pro.cod_projeto
		JOIN servico_etapa sea ON pse.cod_etapa = sea.cod_servico_etapa
		JOIN etapa eta ON eta.cod_etapa = sea.cod_etapa
		JOIN proposta prp ON prp.cod_proposta = pro.cod_proposta
		LEFT JOIN (SELECT t1.id_arquivamento AS id_arquivamento,
											t1.cod_projeto AS cod_projeto,
											t1.cod_status AS cod_status,
											t1.cod_colaborador AS cod_colaborador,
											t1.data_acao AS data_acao,
											t1.motivo_acao AS motivo_acao,
											t1.flag_calendario AS flag_calendario
							FROM projeto_arquivamento t1
				JOIN (SELECT projeto_arquivamento.cod_projeto AS cod_projeto,
										projeto_arquivamento.cod_status AS cod_status,
										projeto_arquivamento.cod_colaborador AS cod_colaborador,
										projeto_arquivamento.motivo_acao AS motivo_acao,
										max( projeto_arquivamento.id_arquivamento ) AS id_arquivamento
							FROM projeto_arquivamento
							GROUP BY projeto_arquivamento.cod_projeto
						) t2 ON t2.cod_projeto = t1.cod_projeto AND  t2.id_arquivamento = t1.id_arquivamento ) arq ON arq.cod_projeto = pro.cod_projeto
		Where prp.id_escritorio = $nIdEscritorio And pro.cod_projeto Is Not Null And data_fim Is Null and pro.cod_status=7
ORDER BY pse.data_prevista";
//       echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";

        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }


    /**
     *
     * Método para recuperar um objeto ServicoEtapa da base de dados
     *
     * @param $nIdEscritorio
     * @return false|array
     */
    public function recuperarTodosProjetoArquivados($nIdEscritorio){
        $oProjetoArquivamento = new ProjetoArquivamento();
        $oPersistencia = new Persistencia($oProjetoArquivamento);
        $sTabelas = "v_projeto pro";
        $sCampos = "id_arquivamento,pro.cod_projeto,data_acao,date_format(data_acao,'%d/%m/%Y') as data_acao_formatada,motivo_acao,login as cod_colaborador,identificacao_projeto";
        $sComplemento = "Inner Join (
						Select t1.*
						From projeto_arquivamento t1
						Inner Join (Select cod_projeto,cod_status,cod_colaborador,motivo_acao,max(id_arquivamento) id_arquivamento
									From projeto_arquivamento Group By cod_projeto
									) t2 On t2.cod_projeto = t1.cod_projeto And t2.id_arquivamento = t1.id_arquivamento
						) arq On arq.cod_projeto = pro.cod_projeto
                        Inner Join colaborador col On col.cod_colaborador = arq.cod_colaborador
                        Where id_escritorio=$nIdEscritorio And cod_status = 6";

//        echo "Select $sCampos From $sTabelas $sComplemento";
        $voProjeto = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voProjeto)
            return $voProjeto;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto ServicoEtapa
     *
     * @param $nCodEtapa
     * @param $nCodServico
     * @param $sDescricao
     * @param $sDescricaoContrato
     * @param $nOrdem
     * @return ServicoEtapa
     */
    public function inicializarServicoEtapa($nCodServicoEtapa,$sDescricao,$sDescricaoContrato,$nOrdem,$nCodServico,$nCodEtapa): ServicoEtapa
    {
        $oServicoEtapa = new ServicoEtapa();

        $oServicoEtapa->setCodServicoEtapa($nCodServicoEtapa);
        $oServicoEtapa->setDescricao($sDescricao);
        $oServicoEtapa->setDescricaoContrato($sDescricaoContrato);
        $oServicoEtapa->setOrdem($nOrdem);
        $oServicoEtapa->setCodServico($nCodServico);
        $oServicoEtapa->setCodEtapa($nCodEtapa);
        return $oServicoEtapa;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ServicoEtapa
     *
     * @param $oServicoEtapa
     * @return boolean
     */
    public function inserirServicoEtapa($oServicoEtapa){
        $oPersistencia = new Persistencia($oServicoEtapa);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto ServicoEtapa
     *
     * @param $oServicoEtapa
     * @return boolean
     */
    public function alterarServicoEtapa($oServicoEtapa){
        $oPersistencia = new Persistencia($oServicoEtapa);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto ServicoEtapa
     *
     * @param $nCodEtapa
     * @return boolean
     */
    public function excluirServicoEtapa($nCodEtapa){
        $oServicoEtapa = new ServicoEtapa();

        $oServicoEtapa->setCodEtapa($nCodEtapa);
        $oPersistencia = new Persistencia($oServicoEtapa);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto ServicoEtapa da base de dados
     *
     * @param $nCodServicoEtapa
     * @return false|ServicoEtapa
     */
    public function recuperarUmServicoEtapa($nCodServicoEtapa){
        $oServicoEtapa = new ServicoEtapa();
        $oPersistencia = new Persistencia($oServicoEtapa);
        $sTabelas = "servico_etapa";
        $sCampos = "*";
        // $sComplemento = " WHERE cod_servico_etapa = $nCodServicoEtapa";
        $sComplemento = " WHERE cod_servico_etapa = $nCodServicoEtapa";
        // echo "SELECT * from {$sTabelas} {$sComplemento}";
        $voServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoEtapa)
            return $voServicoEtapa[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ServicoEtapa da base de dados
     *
     * @return false|ServicoEtapa[]
     */
    public function recuperarTodosServicoEtapa($nCodServico=null,$nIdEscritorio=1){
        $oServicoEtapa = new ServicoEtapa();
        $oPersistencia = new Persistencia($oServicoEtapa);
        $sTabelas = "servico_etapa";
        $sCampos = "servico_etapa.*";
        $sComplemento = "inner join servico on servico.cod_servico = servico_etapa.cod_servico WHERE id_escritorio=$nIdEscritorio";
        if($nCodServico)
            $sComplemento .= " and servico_etapa.cod_servico=$nCodServico";

        $voServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoEtapa)
            return $voServicoEtapa;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ServicoEtapa da base de dados
     *
     * @param $nCodServico
     * @return false|ServicoEtapa[]
     */
    public function recuperarTodosServicoEtapaPorServico($nCodServico){
        $oServicoEtapa = new ServicoEtapa();
        $oPersistencia = new Persistencia($oServicoEtapa);
        $sTabelas = "servico_etapa";
        $sCampos = "*";
        $sComplemento = "WHERE cod_servico=". $nCodServico . " order by ordem asc";
        //  echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoEtapa)
            return $voServicoEtapa;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ServicoEtapa da base de dados
     *
     * @param $nCodProposta
     * @return false|ServicoEtapa[]
     */
    public function recuperarTodosMicroServicoPorProposta($nCodProposta){
        $oServicoEtapa = new ServicoMicroServico();
        $oPersistencia = new Persistencia($oServicoEtapa);
        $sTabelas = "proposta_microservico pm";
        $sCampos = "smi.*";
        $sComplemento = "Inner Join servico_microservico smi ON pm.cod_microservico = smi.cod_microservico
        WHERE pm.cod_proposta = $nCodProposta";
        // echo "Select $sCampos From $sTabelas $sComplemento";
        $voServicoEtapa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoEtapa)
            return $voServicoEtapa;
        return false;
    }


    /**
     *
     * Método para recuperar um objeto Status da base de dados
     *
     * @param $nCodStatus
     * @return false|Status
     */
    public function recuperarUmStatus($nCodStatus){
        $oStatus = new Status();
        $oPersistencia = new Persistencia($oStatus);
        $sTabelas = "status";
        $sCampos = "*";
        $sComplemento = " WHERE cod_status = $nCodStatus";
        $voStatus = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voStatus)
            return $voStatus[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Status da base de dados
     *
     * @return false|Status[]
     */
    public function recuperarTodosStatus(){
        $oStatus = new Status();
        $oPersistencia = new Persistencia($oStatus);
        $sTabelas = "status";
        $sCampos = "cod_status,descricao,cor,ativo";
        $sComplemento = "where ativo=1 order by descricao asc";
        //echo "SELECT * FROM $sTabelas " . $sComplemento;
        $voStatus = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voStatus)
            return $voStatus;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos Status da base de dados
     *
     * @return false|Status[]
     */
    public function recuperarTodosStatusProposta($nIdEscritorio){
        $oStatus = new Status();
        $oPersistencia = new Persistencia($oStatus);
        $sTabelas = "status";
        $sCampos = "status.cod_status,status.descricao,cor,count(cod_proposta) as quantidade";
        $sComplemento = "Left Join proposta on proposta.cod_status = status.cod_status
                    Where status.ativo = 1 And id_escritorio = $nIdEscritorio
                    Group By `status`.cod_status,`status`.descricao,cor
                    Order By `status`.descricao asc";
//        echo "Select $sCampos From $sTabelas $sComplemento";
        $voStatus = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voStatus)
            return $voStatus;
        return false;
    }

    /**
     *
     * Método para inicializar um Objeto PropostaServicoMicroServico
     *
     * @param $nCodMicroServico
     * @param $nCodProposta
     * @param $nCodProjeto
     * @param $nQuantidade
     * @param $sDescricaoQuantidade
     * @return PropostaMicroServico
     */
    public function inicializarPropostaServicoMicroServico($nCodMicroServico,$nCodProposta,$nCodProjeto,$nQuantidade,$sDescricaoQuantidade){
        $oPropostaServicoMicroServico = new PropostaMicroServico();

        $oPropostaServicoMicroServico->setCodMicroservico($nCodMicroServico);
        $oPropostaServicoMicroServico->setCodProposta($nCodProposta);
        $oPropostaServicoMicroServico->setCodProjeto($nCodProjeto);
        $oPropostaServicoMicroServico->setQuantidade($nQuantidade);
        $oPropostaServicoMicroServico->setDescricaoQuantidade($sDescricaoQuantidade);

        return $oPropostaServicoMicroServico;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto PropostaServicoMicroServico
     *
     * @param $oPropostaServicoMicroServico
     * @return boolean
     */
    public function inserirPropostaServicoMicroServico($oPropostaServicoMicroServico){
        $oPersistencia = new Persistencia($oPropostaServicoMicroServico);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto PropostaServicoMicroServico
     *
     * @param $oPropostaServicoMicroServico
     * @return boolean
     */
    public function alterarPropostaServicoMicroServico($oPropostaServicoMicroServico){
        $oPersistencia = new Persistencia($oPropostaServicoMicroServico);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto PropostaServicoMicroServico
     *
     * @param $nCodMicroservico
     * @param $nCodProposta
     * @return boolean
     */
    public function excluirPropostaServicoMicroServico($nCodMicroservico,$nCodProposta){
        $oPropostaServicoMicroServico = new PropostaMicroServico();

        $oPropostaServicoMicroServico->setCodMicroservico($nCodMicroservico);
        $oPropostaServicoMicroServico->setCodProposta($nCodProposta);
        $oPersistencia = new Persistencia($oPropostaServicoMicroServico);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um objeto PropostaServicoMicroServico da base de dados
     *
     * @param $nCodEtapa
     * @param $nCodProposta
     * @return false|PropostaMicroServico
     */
    public function recuperarUmPropostaServicoMicroServico($nCodEtapa,$nCodProposta){
        $oPropostaServicoMicroServico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaServicoMicroServico);
        $sTabelas = "proposta_servico_microservico";
        $sCampos = "*";
        $sComplemento = " WHERE cod_etapa = $nCodEtapa AND cod_proposta = $nCodProposta";
        $voPropostaMicroservico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaMicroservico)
            return $voPropostaMicroservico[0];
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto ServicoMicroServico
     *
     * @param $nCodMicroservico
     * @param $nCodEtapa
     * @param $sDescricao
     * @param $sCor
     * @param $nOrdem
     * @param $nPrazo
     * @param $nDetalhe
     * @return ServicoMicroServico
     */
    public function inicializarServicoMicroServico($nCodMicroservico,$nCodEtapa,$sDescricao,$sCor,$nOrdem,$nPrazo,$nDetalhe){
        $oServicoMicroServico = new ServicoMicroServico();
        $oServicoMicroServico->setCodMicroservico($nCodMicroservico);
        $oServicoMicroServico->setCodEtapa($nCodEtapa);
        $oServicoMicroServico->setDescricao($sDescricao);
        $oServicoMicroServico->setCor($sCor);
        $oServicoMicroServico->setOrdem($nOrdem);
        $oServicoMicroServico->setPrazo($nPrazo);
        $oServicoMicroServico->setDetalhe($nDetalhe);
        return $oServicoMicroServico;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ServicoMicroServico
     *
     * @param $oServicoMicroServico
     * @return boolean
     */
    public function inserirServicoMicroServico($oServicoMicroServico){
        $oPersistencia = new Persistencia($oServicoMicroServico);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para alterar na base de dados um Objeto ServicoMicroServico
     *
     * @param $oServicoMicroServico
     * @return boolean
     */
    public function alterarServicoMicroServico($oServicoMicroServico){
        $oPersistencia = new Persistencia($oServicoMicroServico);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


    /**
     *
     * Método para excluir da base de dados um Objeto ServicoMicroServico
     *
     * @param $nCodMicroservico
     * @return boolean
     */
    public function excluirServicoMicroServico($nCodMicroservico){
        $oServicoMicroServico = new ServicoMicroServico();

        $oServicoMicroServico->setCodMicroservico($nCodMicroservico);
        $oPersistencia = new Persistencia($oServicoMicroServico);
        $bExcluir = ($_SESSION['oUsuarioAM'] && $_SESSION['oGrupoUsuario']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

        if($bExcluir)
            return true;
        return false;

    }

    /**
     *
     * Método para verificar se existe um Objeto ServicoMicroServico na base de dados
     *
     * @param $nCodMicroservico
     * @return boolean
     */
    public function presenteServicoMicroServico($nCodMicroservico){
        $oServicoMicroServico = new ServicoMicroServico();

        $oServicoMicroServico->setCodMicroservico($nCodMicroservico);
        $oPersistencia = new Persistencia($oServicoMicroServico);
        if($oPersistencia->presente())
            return true;
        return false;
    }

    /**
     *
     * Método para recuperar um objeto ServicoMicroServico da base de dados
     *
     * @param $nCodMicroservico
     * @return false|ServicoMicroServico
     */
    public function recuperarUmServicoMicroServico($nCodMicroservico){
        $oServicoMicroServico = new ServicoMicroServico();
        $oPersistencia = new Persistencia($oServicoMicroServico);
        $sTabelas = "servico_microservico";
        $sCampos = "*";
        $sComplemento = " WHERE cod_microservico = $nCodMicroservico";
        // echo "SELECT * FROM servico_microservico " . $sComplemento;
        $voServicoMicroServico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoMicroServico)
            return $voServicoMicroServico[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ServicoMicroServico da base de dados
     *
     * @param null $nCodServico
     * @param null $nCodProposta
     * @return false|ServicoMicroServico[]
     */
    public function recuperarTodosServicoMicroServico($nCodServico=null,$nCodProposta=null){
        $oServicoMicroServico = new ServicoMicroServico();
        $oPersistencia = new Persistencia($oServicoMicroServico);
        $sTabelas = "servico_microservico";
        $sCampos = "servico_microservico.*";
        if(!$nCodProposta){
            $sComplemento = (($nCodServico) ? "inner join servico_etapa on servico_etapa.cod_servico_etapa = servico_microservico.cod_etapa WHERE cod_servico = $nCodServico " : "") .  " ORDER BY servico_etapa.ordem, servico_microservico.ordem ASC";
            // $sComplemento = (($nCodServico) ? "inner join servico_etapa on servico_etapa.cod_servico_etapa = servico_microservico.cod_etapa WHERE cod_servico = $nCodServico " : "") .  " ORDER BY servico_microservico.ordem ASC";
        }else {
            $sComplemento = "inner join servico_etapa on servico_etapa.cod_servico_etapa = servico_microservico.cod_etapa INNER JOIN proposta_microservico on proposta_microservico.cod_microservico = servico_microservico.cod_microservico
								where cod_proposta=$nCodProposta  ORDER BY servico_etapa.ordem, servico_microservico.ordem ASC";
        }
        // echo "SELECT {$sCampos} FROM {$sTabelas} {$sComplemento}";
        $voServicoMicroServico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoMicroServico)
            return $voServicoMicroServico;
        return false;
    }


    /**
     *
     * Método para recuperar um vetor de objetos ServicoMicroServico da base de dados
     *
     * @param null $nCodServico
     * @param null $nCodEtapa
     * @return false|ServicoMicroServico[]
     */
    public function recuperarTodosServicoMicroServicoPorEtapaServico($nCodEtapa,$nCodServico){
        $oServicoMicroServico = new ServicoMicroServico();
        $oPersistencia = new Persistencia($oServicoMicroServico);
        $sTabelas = "v_microservico";
        $sCampos = "*";
        $sComplemento = " where cod_etapa={$nCodEtapa} and cod_servico={$nCodServico} and id_escritorio={$_SESSION['oEscritorio']->getIdEscritorio()}  order by micro_ordem asc";
//        echo "Select * From $sTabelas $sComplemento";

        $voServicoMicroServico = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voServicoMicroServico)
            return $voServicoMicroServico;
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ServicoMicroServico da base de dados
     *
     * @param $nCodMicroServico
     * @param $nCodProposta
     * @return false|ServicoMicroServico[]
     */
    public function recuperarUmServicoMicroServicoProposta($nCodMicroServico,$nCodProposta){
        $oServicoMicroServico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oServicoMicroServico);
        $sTabelas = "proposta_microservico";
        $sCampos = "*";
        $sComplemento = "Where cod_microservico = $nCodMicroServico And cod_proposta = $nCodProposta";
        // echo "Select * From $sTabelas $sComplemento<br>";
        $voServicoMicroServico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voServicoMicroServico)
            return $voServicoMicroServico[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ServicoMicroServico da base de dados
     *
     * @param null $nCodServico
     * @param null $nCodProposta
     * @return false|ServicoMicroServico[]
     */
    public function recuperarTodosServicoMicroServicoProposta($nCodServico=null,$nCodProposta=null){
        $oPropostaMicroservico = new PropostaMicroServico();
        $oPersistencia = new Persistencia($oPropostaMicroservico);
        $sTabelas = "servico_microservico";
        $sCampos = "*";
        if(!$nCodProposta)
            $sComplemento = (($nCodServico) ? "Where cod_servico = $nCodServico" : "") .  " order by ordem asc";
        else
            $sComplemento = " INNER JOIN proposta_microservico on proposta_microservico.cod_microservico = servico_microservico.cod_microservico where cod_proposta=$nCodProposta order by ordem asc";

        $voPropostaMicroservico = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voPropostaMicroservico)
            return $voPropostaMicroservico;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto Colaborador
     *
     * @param $nCodProjetoDocumento
     * @param $sDescricao
     * @param $sUrl
     * @param $nCodProjeto
     * @return ProjetoDocumento
     */
    public function inicializarProjetoDocumento($nCodProjetoDocumento, $sDescricao, $sUrl, $nCodProjeto){

        $oProjetoDocumento = new ProjetoDocumento();

        $oProjetoDocumento->setCodProjetoDocumento($nCodProjetoDocumento);
        $oProjetoDocumento->setDescricao($sDescricao);
        $oProjetoDocumento->setUrl($sUrl);
        $oProjetoDocumento->setCodProjeto($nCodProjeto);
        return $oProjetoDocumento;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoDocumento da base de dados
     *
     * @param $nCodProjeto
     * @return false|ServicoMicroServico[]
     */
    public function recuperarTodosProjetoDocumento($nCodProjeto){
        $oProjetoDocumento = new ProjetoDocumento();
        $oPersistencia = new Persistencia($oProjetoDocumento);
        $sTabelas = "projeto_documento";
        $sCampos = "*";
        $sComplemento = "Where cod_projeto = $nCodProjeto";

        $voProjetoDocumento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoDocumento)
            return $voProjetoDocumento;
        return false;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Cliente
     *
     * @param $oProjetoDocumento
     * @return boolean
     */
    public function inserirProjetoDocumento($oProjetoDocumento){
        $oPersistencia = new Persistencia($oProjetoDocumento);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto Cliente
     *
     * @param $nCodProjetoDocumento
     * @return boolean
     */
    public function excluirProjetoDocumento($nCodProjetoDocumento): bool
    {
        $oProjetoDocumento = new ProjetoDocumento();
        $oProjetoDocumento->setCodProjetoDocumento($nCodProjetoDocumento);
        $oPersistencia = new Persistencia($oProjetoDocumento);
        if($oPersistencia->excluirFisicamente())
            return true;
        return false;
    }

    /**
     * @param $nCodProjetoDocumento
     * @param $sUrl
     * @return bool
     */
    public function atualizarProjetoDocumentoUrl($nCodProjetoDocumento, $sUrl): bool
    {
        $oProjetoDocumento = new ProjetoDocumento();
        $oPersistencia = new Persistencia($oProjetoDocumento);
        $sTabela = "projeto_documento";
        $sCampos  = "url = '$sUrl'";
        $sComplemento = "Where cod_projeto_documento = $nCodProjetoDocumento";
        if($oPersistencia->AlteraSomenteUmaColuna($sTabela,$sCampos,$sComplemento))
            return true;
        return false;

    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoDocumento da base de dados
     *
     * @param $nIdEscritorio
     * @return false|ServicoMicroServico[]
     */
    public function recuperarTodosNotificacao($nIdEscritorio){
        $oPersistencia = new Persistencia();
        $sTabelas = "cliente cli";
        $sCampos = "cli.cod_cliente,nome,cod_tipo_email,descricao,data_hora,id_escritorio";
        $sComplemento = "Inner Join email ema On cli.cod_cliente = ema.cod_cliente Where id_escritorio = $nIdEscritorio Order By data_hora Desc Limit 50";

        $voNotificacao = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
        if($voNotificacao)
            return $voNotificacao;
        return false;
    }


    /**
     *
     * Método para inicializar um Objeto ProjetoArquivamento
     *
     * @param $nIdArquivamento
     * @param $nCodProjeto
     * @param $nCodStatus
     * @param $nCodColaborador
     * @param $dDataAcao
     * @param $sMotivoAcao
     * @return ProjetoArquivamento
     */
    public function inicializarProjetoArquivamento($nIdArquivamento, $nCodProjeto, $nCodStatus, $nCodColaborador,$dDataAcao,$sMotivoAcao,$nFlagCalendario){

        $oProjetoArquivamento = new ProjetoArquivamento();

        $oProjetoArquivamento->setIdArquivamento($nIdArquivamento);
        $oProjetoArquivamento->setCodProjeto($nCodProjeto);
        $oProjetoArquivamento->setCodStatus($nCodStatus);
        $oProjetoArquivamento->setCodColaborador($nCodColaborador);
        $oProjetoArquivamento->setDataAcaoBanco($dDataAcao);
        $oProjetoArquivamento->setMotivoAcao($sMotivoAcao);
        $oProjetoArquivamento->setFlagCalendario($nFlagCalendario);
        return $oProjetoArquivamento;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoArquivamento da base de dados
     *
     * @param $nIdArquivamento
     * @return false|ProjetoArquivamento
     */
    public function recuperarUmProjetoArquivamento($nIdArquivamento){
        $oProjetoArquivamento = new ProjetoArquivamento();
        $oPersistencia = new Persistencia($oProjetoArquivamento);
        $sTabelas = "projeto_Arquivamento";
        $sCampos = "*";
        $sComplemento = "Where id_arquivamento = $nIdArquivamento";
        $voProjetoArquivamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoArquivamento)
            return $voProjetoArquivamento[0];
        return false;
    }

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoArquivamento da base de dados
     *
     * @param $nCodProjeto
     * @return false|ProjetoArquivamento[]
     */
    public function recuperarTodosProjetoArquivamento($nCodProjeto=null){
        $oProjetoArquivamento = new ProjetoArquivamento();
        $oPersistencia = new Persistencia($oProjetoArquivamento);
        $sTabelas = "projeto_Arquivamento";
        $sCampos = "*";
        $sComplemento = ($nCodProjeto) ? "Where cod_projeto = $nCodProjeto" :"";
        $voProjetoArquivamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoArquivamento)
            return $voProjetoArquivamento;
        return false;
    }

//    select max(id_arquivamento) as id_arquivamento, login as cod_colaborador, data_acao,motivo_acao
//FROm projeto_arquivamento
//inner join colaborador on colaborador.cod_colaborador = projeto_arquivamento.cod_colaborador
//inner join v_projeto projeto_arquivamento.cod_projeto = v_projeto.cod_projeto
//where cod_projeto=34 and cod_status=6

    /**
     *
     * Método para recuperar um vetor de objetos ProjetoArquivamento da base de dados
     *
     * @param $nCodProjeto
     * @return false|ProjetoArquivamento[]
     */
    public function recuperarUltimoProjetoArquivamentoPorProjeto($nCodProjeto){
        $oProjetoArquivamento = new ProjetoArquivamento();
        $oPersistencia = new Persistencia($oProjetoArquivamento);
        $sTabelas = "projeto_Arquivamento t1";
        $sCampos = "t1.*";
        $sComplemento = "Inner Join (
							Select cod_projeto,cod_status,cod_colaborador,motivo_acao,max(id_arquivamento) id_arquivamento
							From projeto_arquivamento Group By cod_projeto
						) t2 On t2.cod_projeto = t1.cod_projeto And t2.id_arquivamento = t1.id_arquivamento
						Where t1.cod_projeto={$nCodProjeto}";
//        echo "Select $sCampos From $sTabelas $sComplemento ";
        $voProjetoArquivamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        if($voProjetoArquivamento)
            return $voProjetoArquivamento[0];
        return false;
    }

    /**
     *
     * Método para inserir na base de dados um Objeto ProjetoArquivamento
     *
     * @param $oProjetoArquivamento
     * @return boolean
     */
    public function inserirProjetoArquivamento($oProjetoArquivamento): bool
    {
        $oPersistencia = new Persistencia($oProjetoArquivamento);
        if($nId = $oPersistencia->inserir())
            return $nId;
        return false;
    }

    /**
     * @param $oProjetoArquivamento
     * @return bool
     */
    public function alterarProjetoArquivamento($oProjetoArquivamento): bool
    {
        $oPersistencia = new Persistencia($oProjetoArquivamento);
        if($oPersistencia->alterar())
            return true;
        return false;
    }


}
