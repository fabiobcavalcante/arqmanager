<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela projeto_pagamento
 */
class ProjetoPagamento{
	/**
	 * @campo cod_projeto_pagamento
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodProjetoPagamento;
	/**
	 * @campo cod_projeto
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodProjeto;
	/**
	 * @campo cod_forma_pagamento
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodFormaPagamento;
	/**
	 * @campo valor
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nValor;
	/**
	 * @campo data_previsao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $dDataPrevisao;
	/**
	 * @campo data_efetivacao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $dDataEfetivacao;
	/**
	 * @campo comprovante
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sComprovante;
	/**
	 * @campo desc_pagamento
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sDescPagamento;
	/**
	 * @campo incluido_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sIncluidoPor;
	/**
	 * @campo alterado_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sAlteradoPor;
	/**
	 * @campo ativo
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nAtivo;
	private $oProjeto;
	private $oFormaPagamento;


	public function __construct(){

	}

	public function setCodProjetoPagamento($nCodProjetoPagamento){
		$this->nCodProjetoPagamento = $nCodProjetoPagamento;
	}
	public function getCodProjetoPagamento(){
		return $this->nCodProjetoPagamento;
	}
	public function setCodProjeto($nCodProjeto){
		$this->nCodProjeto = $nCodProjeto;
	}
	public function getCodProjeto(){
		return $this->nCodProjeto;
	}
	public function setCodFormaPagamento($nCodFormaPagamento){
		$this->nCodFormaPagamento = $nCodFormaPagamento;
	}
	public function getCodFormaPagamento(){
		return $this->nCodFormaPagamento;
	}
	public function setValor($nValor){
		$this->nValor = $nValor;
	}
	public function getValor(){
		return $this->nValor;
	}
	public function getValorFormatado(){
		$vRetorno = number_format($this->nValor , 2, ',', '.');
		return $vRetorno;
	}
	public function setValorBanco($nValor){
		if($nValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValor = str_replace($sOrigem, $sDestino, $nValor);

		}else{
			$this->nValor = 'null';
		}
	}
	public function setDataPrevisao($dDataPrevisao){
		$this->dDataPrevisao = $dDataPrevisao;
	}
	public function getDataPrevisao(){
		return $this->dDataPrevisao;
	}
	public function getDataPrevisaoFormatado(){
		$oData = new DateTime($this->dDataPrevisao);
		return $oData->format("d/m/Y");
	}
	public function setDataPrevisaoBanco($dDataPrevisao){
		if($dDataPrevisao){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataPrevisao);
			$this->dDataPrevisao = $oData->format('Y-m-d') ;
		}
	}
	public function setDataEfetivacao($dDataEfetivacao){
		$this->dDataEfetivacao = $dDataEfetivacao;
	}
	public function getDataEfetivacao(){
		return $this->dDataEfetivacao;
	}
	public function getDataEfetivacaoFormatado(){
		$oData = new DateTime($this->dDataEfetivacao);
		return $oData->format("d/m/Y");
	}
	public function setDataEfetivacaoBanco($dDataEfetivacao){
		if($dDataEfetivacao){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataEfetivacao);
			$this->dDataEfetivacao = $oData->format('Y-m-d') ;
		}
	}
	public function setComprovante($sComprovante){
		$this->sComprovante = $sComprovante;
	}
	public function getComprovante(){
		return $this->sComprovante;
	}
	public function setDescPagamento($sDescPagamento){
		$this->sDescPagamento = $sDescPagamento;
	}
	public function getDescPagamento(){
		return $this->sDescPagamento;
	}
	public function setIncluidoPor($sIncluidoPor){
		$this->sIncluidoPor = $sIncluidoPor;
	}
	public function getIncluidoPor(){
		return $this->sIncluidoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setProjeto($oProjeto){
		$this->oProjeto = $oProjeto;
	}
	public function getProjeto(){
		$oFachada = new FachadaPrincipalBD();
		$this->oProjeto = $oFachada->recuperarUmProjeto($this->getCodProjeto());
		return $this->oProjeto;
	}
	public function setFormaPagamento($oFormaPagamento){
		$this->oFormaPagamento = $oFormaPagamento;
	}
	public function getFormaPagamento(){
		$oFachada = new FachadaPrincipalBD();
		$this->oFormaPagamento = $oFachada->recuperarUmFormaPagamento($this->getCodFormaPagamento());
		return $this->oFormaPagamento;
	}
}
