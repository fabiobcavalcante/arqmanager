<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela projeto
 */
class Projeto{
    /**
     * @campo cod_projeto
     * @var number
     * @primario true
     * @nulo false
     * @auto-increment true
     */
    private $nCodProjeto;
    /**
     * @campo cod_cliente
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodCliente;
    /**
     * @campo cod_servico
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodServico;
    /**
     * @campo cod_proposta
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodProposta;

    /**
     * @campo descricao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $dDescricao;
    /**
     * @campo data_inicio
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $dDataInicio;
    /**
     * @campo data_previsao_fim
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $dDataPrevisaoFim;
    /**
     * @campo data_fim
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $dDataFim;
    /**
     * @campo metragem
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nMetragem;

    /**
     * @campo incluido_por
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sIncluidoPor;
    /**
     * @campo alterado_por
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sAlteradoPor;
    /**
     * @campo observacao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sObservacao;
    /**
     * @campo ativo
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nAtivo;
    /**
     * @campo numero_projeto
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nNumeroProjeto;
    /**
     * @campo ano_projeto
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nAnoProjeto;
    /**
     * @campo cod_status
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodStatus;

    private $oServico;
    private $oProposta;
    private $oCliente;
    private $voPagamento;

    public function setCodProjeto($nCodProjeto){
        $this->nCodProjeto = $nCodProjeto;
    }
    public function getCodProjeto(){
        return $this->nCodProjeto;
    }
    public function setCodCliente($nCodCliente){
        $this->nCodCliente = $nCodCliente;
    }
    public function getCodCliente(){
        return $this->nCodCliente;
    }
    public function setCodServico($nCodServico){
        $this->nCodServico = $nCodServico;
    }
    public function getCodServico(){
        return $this->nCodServico;
    }
    public function setCodProposta($nCodProposta){
        $this->nCodProposta = $nCodProposta;
    }
    public function getCodProposta(){
        return $this->nCodProposta;
    }

    public function setDescricao($dDescricao){
        $this->dDescricao = $dDescricao;
    }
    public function getDescricao(){
        return $this->dDescricao;
    }

    public function setDataInicio($dDataInicio){
        $this->dDataInicio = $dDataInicio;
    }
    public function getDataInicio(){
        return $this->dDataInicio;
    }
    public function getDataInicioFormatado(){
        if($this->dDataInicio){
            try {
                $oData = new DateTime($this->dDataInicio);
                return $oData->format("d/m/Y");
            } catch (Exception $e) {
                return false;
            }
        }else {
            return false;
        }
    }
    public function setDataInicioBanco($dDataInicio){
        if($dDataInicio){
            $oData = DateTime::createFromFormat('d/m/Y', $dDataInicio);
            $this->dDataInicio = $oData->format('Y-m-d') ;
        }
    }
    public function setDataPrevisaoFim($dDataPrevisaoFim){
        $this->dDataPrevisaoFim = $dDataPrevisaoFim;
    }
    public function getDataPrevisaoFim(){
        return $this->dDataPrevisaoFim;
    }

    /** @noinspection PhpUnused */
    public function getDataPrevisaoFimFormatado(){
        try {
            $oData = new DateTime($this->dDataPrevisaoFim);
            return $oData->format("d/m/Y");
        } catch (Exception $e) {
            return false;
        }
    }
    /** @noinspection PhpUnhandledExceptionInspection */
    public function getDataPrevisaoFimFormatado2(){

        if(!$this->dDataFim){
            $dDataAtual = date('Y-m-d');
            $datetime1 = new DateTime($dDataAtual);
            $datetime2 = new DateTime($this->dDataPrevisaoFim);
            $interval = $datetime1->diff($datetime2);
            $nDias = $interval->format('%R%a');

            if($nDias <= 0){
                return "<small class='badge badge-danger'><i class='fa fa-clock-o'></i> ".$nDias ."d em atraso</small>";
            }elseif($nDias >= 1 && $nDias < 4){
                return "<small class='badge badge-warning'><i class='fa fa-clock-o'></i> ".$nDias ."d para entrega</small>";
            }elseif($nDias >= 4 && $nDias < 7){
                return "<small class='badge badge-info'><i class='fa fa-clock-o'></i> ". $nDias ."d para entrega</small>";
            }elseif($nDias >= 7){
                return "<small class='badge badge-success'><i class='fa fa-clock-o'></i> ". $nDias ."d no prazo</small>";
            }
        }else{
            $oData = new DateTime($this->dDataFim);
            return $oData->format("d/m/Y");
        }
        return false;

    }
    /** @noinspection PhpUnused */
    public function setDataPrevisaoFimBanco($dDataPrevisaoFim){
        if($dDataPrevisaoFim){
            $oData = DateTime::createFromFormat('d/m/Y', $dDataPrevisaoFim);
            $this->dDataPrevisaoFim = $oData->format('Y-m-d') ;
        }
    }
    public function setDataFim($dDataFim){
        $this->dDataFim = $dDataFim;
    }
    public function getDataFim(){
        return $this->dDataFim;
    }

    /** @noinspection PhpUnused */
    public function getDataFimFormatado(){
        try {
            $oData = new DateTime($this->dDataFim);
            return $oData->format("d/m/Y");
        } catch (Exception $e) {
            return false;
        }
    }
    public function setDataFimBanco($dDataFim){
        if($dDataFim){
            $oData = DateTime::createFromFormat('d/m/Y', $dDataFim);
            $this->dDataFim = $oData->format('Y-m-d') ;
        }
    }


    public function setMetragem($nMetragem){
        $this->nMetragem = $nMetragem;
    }
    public function getMetragem(){
        return $this->nMetragem;
    }


    public function getMetragemFormatado(){
        $vRetorno = number_format($this->nMetragem , 2, ',', '.');
        return $vRetorno;
    }
    public function setMetragemBanco($nMetragem){
        if($nMetragem){
            $sOrigem = array('.',',');
            $sDestino = array('','.');
            $this->nMetragem = str_replace($sOrigem, $sDestino, $nMetragem);

        }else{
            $this->nMetragem = 'null';
        }
    }


    public function setIncluidoPor($sIncluidoPor){
        $this->sIncluidoPor = $sIncluidoPor;
    }
    public function getIncluidoPor(){
        return $this->sIncluidoPor;
    }
    public function setAlteradoPor($sAlteradoPor){
        $this->sAlteradoPor = $sAlteradoPor;
    }
    public function getAlteradoPor(){
        return $this->sAlteradoPor;
    }
    public function setObservacao($sObservacao){
        $this->sObservacao = $sObservacao;
    }
    public function getObservacao(){
        return $this->sObservacao;
    }
    public function setAtivo($nAtivo){
        $this->nAtivo = $nAtivo;
    }
    public function getAtivo(){
        return $this->nAtivo;
    }

    public function setNumeroProjeto($nNumeroProjeto){
        $this->nNumeroProjeto = $nNumeroProjeto;
    }
    public function getNumeroProjeto(){
        return $this->nNumeroProjeto;
    }
    public function setAnoProjeto($nAnoProjeto){
        $this->nAnoProjeto = $nAnoProjeto;
    }
    public function getAnoProjeto(){
        return $this->nAnoProjeto;
    }

    public function getNumeroProjetoFormatada(){
        return $this->nAnoProjeto . "/".str_pad($this->nNumeroProjeto , 4 , '0' , STR_PAD_LEFT);
    }
    public function setServico($oServico){
        $this->oServico = $oServico;
    }

    public function setCodStatus($nCodStatus){
        $this->nCodStatus = $nCodStatus;
    }
    public function getCodStatus(){
        return $this->nCodStatus;
    }
    public function getServico(){
        $oFachada = new FachadaPrincipalBD();
        $this->oServico = $oFachada->recuperarUmServico($this->getCodServico());
        return $this->oServico;
    }
    public function setProposta($oProposta){
        $this->oProposta = $oProposta;
    }
    public function getProposta(){
        $oFachada = new FachadaPrincipalBD();
        $this->oProposta = $oFachada->recuperarUmProposta($this->getCodProposta());
        return $this->oProposta;
    }
    public function setCliente($oCliente){
        $this->oCliente = $oCliente;
    }
    public function getCliente(){
        $oFachada = new FachadaPrincipalBD();
        $this->oCliente = $oFachada->recuperarUmCliente($this->getCodCliente());
        return $this->oCliente;
    }

    public function getPagamentos(){
        $oFachada = new FachadaPrincipalBD();
        $this->voPagamento = $oFachada->recuperarTodosProjetoPagamentoPorProjeto($this->nCodProjeto);
        return $this->voPagamento;
    }
}
