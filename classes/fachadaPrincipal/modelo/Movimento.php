<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela movimento
  */
 class Movimento{
 	/**
	* @campo cod_movimento
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodMovimento;
	/**
	* @campo cod_despesa_receita_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodDespesaReceitaTipo;
	/**
	* @campo data_entrada
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dDataEntrada;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo data_efetivacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dDataEfetivacao;
	/**
	* @campo valor
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nValor;
     /**
	* @campo observacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sObservacao;
     /**
	* @campo comprovante
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sComprovante;

	/**
	* @campo inserido_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sInseridoPor;
	/**
	* @campo alterado_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAlteradoPor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
  /**
  * @campo id_escritorio
  * @var number
  * @primario false
  * @nulo false
  * @auto-increment false
  */
  private $nIdEscritorio;
  /**
  * @campo cod_plano_contas
  * @var number
  * @primario false
  * @nulo false
  * @auto-increment false
  */
  private $nCodPlanoContas;
  /**
  * @campo cod_projeto
  * @var number
  * @primario false
  * @nulo false
  * @auto-increment false
  */
  private $nCodProjeto;
  /**
  * @campo competencia
  * @var String
  * @primario false
  * @nulo false
  * @auto-increment false
  */
  private $dCompetencia;
  private $oEscritorio;
  private $oProjeto;


	private $oDespesaReceitaTipo;


 	public function __construct(){

 	}

 	public function setCodMovimento($nCodMovimento){
		$this->nCodMovimento = $nCodMovimento;
	}
	public function getCodMovimento(){
		return $this->nCodMovimento;
	}
	public function setCodDespesaReceitaTipo($nCodDespesaReceitaTipo){
		$this->nCodDespesaReceitaTipo = $nCodDespesaReceitaTipo;
	}
	public function getCodDespesaReceitaTipo(){
		return $this->nCodDespesaReceitaTipo;
	}
	public function setDataEntrada($dDataEntrada){
		$this->dDataEntrada = $dDataEntrada;
	}
	public function getDataEntrada(){
		return $this->dDataEntrada;
	}
	public function getDataEntradaFormatado(){
		$oData = new DateTime($this->dDataEntrada);
		 return $oData->format("d/m/Y");
	}
	public function setDataEntradaBanco($dDataEntrada){
		 if($dDataEntrada){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataEntrada);
			 $this->dDataEntrada = $oData->format('Y-m-d') ;
	}
		 }
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}

	public function setDataEfetivacao($dDataEfetivacao){
		$this->dDataEfetivacao = $dDataEfetivacao;
	}
	public function getDataEfetivacao(){
		return $this->dDataEfetivacao;
	}
	public function getDataEfetivacaoFormatado(){
        if($this->dDataEfetivacao){
		  $oData = new DateTime($this->dDataEfetivacao);
		  return $oData->format("d/m/Y");
        }
	}
	public function setDataEfetivacaoBanco($dDataEfetivacao){
		 if($dDataEfetivacao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataEfetivacao);
			 $this->dDataEfetivacao = $oData->format('Y-m-d') ;
	}
		 }
     public function setComprovante($sComprovante){
   		$this->sComprovante = $sComprovante;
   	}
   	public function getComprovante(){
   		return $this->sComprovante;
   	}
	public function setObservacao($sObservacao){
		$this->sObservacao = $sObservacao;
	}
	public function getObservacao(){
		return $this->sObservacao;
	}

    public function setValor($nValor){
		$this->nValor = $nValor;
	}
	public function getValor(){
		return $this->nValor;
	}

	public function getValorFormatado(){
		 $vRetorno = number_format($this->nValor , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorBanco($nValor){
		if($nValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValor = str_replace($sOrigem, $sDestino, $nValor);

		}else{
		$this->nValor = 'null';
			}
		}


	public function setInseridoPor($sInseridoPor){
		$this->sInseridoPor = $sInseridoPor;
	}
	public function getInseridoPor(){
		return $this->sInseridoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

  public function setIdEscritorio($nIdEscritorio){
    $this->nIdEscritorio = $nIdEscritorio;
  }
  public function getIdEscritorio(){
    return $this->nIdEscritorio;
  }
  public function setCodPlanoContas($nCodPlanoContas){
    $this->nCodPlanoContas = $nCodPlanoContas;
  }
  public function getCodPlanoContas(){
    return $this->nCodPlanoContas;
  }
  public function setCodProjeto($nCodProjeto){
    $this->nCodProjeto = $nCodProjeto;
  }
  public function getCodProjeto(){
    return $this->nCodProjeto;
  }

	public function setCompetencia($dCompetencia){
		$this->dCompetencia = $dCompetencia;
	}
	public function getCompetencia(){
		return $this->dCompetencia;
	}
	public function getCompetenciaFormatado(){
		$oData = new DateTime($this->dCompetencia);
		 return $oData->format("m/Y");
	}
	public function setCompetenciaBanco($dCompetencia){
		 if($dCompetencia){
			 $oData = DateTime::createFromFormat('d/m/Y', $dCompetencia);
			 $this->dCompetencia = $oData->format('Y-m-d') ;
	}
		 }



  public function getEscritorio($nIdEscritorio){
    $oFachada = new FachadaAdminBD();
    $this->oEscritorio = $oFachada->recuperarUmEscritorio($nIdEscritorio);
    return $this->oEscritorio;
  }
	public function setDespesaReceitaTipo($oDespesaReceitaTipo){
		$this->oDespesaReceitaTipo = $oDespesaReceitaTipo;
	}
	public function getDespesaReceitaTipo(){
		$oFachada = new FachadaPrincipalBD();
		$this->oDespesaReceitaTipo = $oFachada->recuperarUmDespesaReceitaTipo($this->getCodDespesaReceitaTipo());
		return $this->oDespesaReceitaTipo;
	}
	public function setPlanoContas($oPlanoContas){
		$this->oPlanoContas = $oPlanoContas;
	}
	public function getPlanoContas(){
		$oFachada = new FachadaPrincipalBD();
		$this->oPlanoContas = $oFachada->recuperarUmPlanoContas($this->getCodPlanoContas());
		return $this->oPlanoContas;
	}
	public function setProjeto($oProjeto){
		$this->oProjeto = $oProjeto;
	}
	public function getProjeto(){
		$oFachada = new FachadaPrincipalBD();
		$this->oProjeto = $oFachada->recuperarUmProjeto($this->getCodProjeto());
		return $this->oProjeto;
	}



 }
 ?>
