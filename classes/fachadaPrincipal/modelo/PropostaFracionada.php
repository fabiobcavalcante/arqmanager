<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela proposta_fracionada
 */
class PropostaFracionada{
	/**
	 * @campo cod_fracionamento
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodFracionamento;
	/**
	 * @campo cod_proposta
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodProposta;
	/**
	 * @campo numero_entrega
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nNumeroEntrega;
	/**
	 * @campo descricao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sDescricao;
	/**
	 * @campo percentual
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nPercentual;
	/**
	 * @campo data_prevista
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $dDataPrevista;
	/**
	 * @campo data_efetivacao
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $dDataEfetivacao;
	/**
	 * @campo valor
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nValor;
	/**
	 * @campo parcelas
	 * @var number
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $nParcelas;
	/**
	 * @campo indice_correcao
	 * @var number
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $nIndiceCorrecao;
	/**
	 * @campo valor_final
	 * @var number
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $nValorFinal;


	public function __construct(){

	}

	public function setCodFracionamento($nCodFracionamento){
		$this->nCodFracionamento = $nCodFracionamento;
	}
	public function getCodFracionamento(){
		return $this->nCodFracionamento;
	}
	public function setCodProposta($nCodProposta){
		$this->nCodProposta = $nCodProposta;
	}
	public function getCodProposta(){
		return $this->nCodProposta;
	}
	public function setNumeroEntrega($nNumeroEntrega){
		$this->nNumeroEntrega = $nNumeroEntrega;
	}
	public function getNumeroEntrega(){
		return $this->nNumeroEntrega;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setPercentual($nPercentual){
		$this->nPercentual = $nPercentual;
	}
	public function getPercentual(){
		return $this->nPercentual;
	}
	public function getPercentualFormatado(){
		$vRetorno = number_format($this->nPercentual , 2, ',', '.');
		return $vRetorno;
	}
	public function setPercentualBanco($nPercentual){
		if($nPercentual){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nPercentual = str_replace($sOrigem, $sDestino, $nPercentual);

		}else{
			$this->nPercentual = 'null';
		}
	}
	public function setDataPrevista($dDataPrevista){
		$this->dDataPrevista = $dDataPrevista;
	}
	public function getDataPrevista(){
		return $this->dDataPrevista;
	}
	public function getDataPrevistaFormatado(){
		$oData = new DateTime($this->dDataPrevista);
		return $oData->format("d/m/Y");
	}
	public function setDataPrevistaBanco($dDataPrevista){
		if($dDataPrevista){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataPrevista);
			$this->dDataPrevista = $oData->format('Y-m-d') ;
		}
	}
	public function setDataEfetivacao($dDataEfetivacao){
		$this->dDataEfetivacao = $dDataEfetivacao;
	}
	public function getDataEfetivacao(){
		return $this->dDataEfetivacao;
	}
	public function getDataEfetivacaoFormatado(){
		$oData = new DateTime($this->dDataEfetivacao);
		return $oData->format("d/m/Y");
	}
	public function setDataEfetivacaoBanco($dDataEfetivacao){
		if($dDataEfetivacao){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataEfetivacao);
			$this->dDataEfetivacao = $oData->format('Y-m-d') ;
		}
	}
	public function setValor($nValor){
		$this->nValor = $nValor;
	}
	public function getValor(){
		return $this->nValor;
	}
	public function getValorFormatado(){
		$vRetorno = number_format($this->nValor , 2, ',', '.');
		return $vRetorno;
	}
	public function setValorBanco($nValor){
		if($nValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValor = str_replace($sOrigem, $sDestino, $nValor);

		}else{
			$this->nValor = 'null';
		}
	}
    public function getValorFormatadoExtenso(){
        $vRetorno = number_format($this->nValor , 2, ',', '.');
        return Extenso::valorPorExtenso($vRetorno);
    }
	public function setParcelas($nParcelas){
		$this->nParcelas = $nParcelas;
	}
	public function getParcelas(){
		return $this->nParcelas;
	}
	public function setIndiceCorrecao($nIndiceCorrecao){
		$this->nIndiceCorrecao = $nIndiceCorrecao;
	}
	public function getIndiceCorrecao(){
		return $this->nIndiceCorrecao;
	}
	public function getIndiceCorrecaoFormatado(){
		$vRetorno = number_format($this->nIndiceCorrecao , 2, ',', '.');
		return $vRetorno;
	}
	public function setIndiceCorrecaoBanco($nIndiceCorrecao){
		if($nIndiceCorrecao){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nIndiceCorrecao = str_replace($sOrigem, $sDestino, $nIndiceCorrecao);

		}else{
			$this->nIndiceCorrecao = 'null';
		}
	}
	public function setValorFinal($nValorFinal){
		$this->nValorFinal = $nValorFinal;
	}
	public function getValorFinal(){
		return $this->nValorFinal;
	}
	public function getValorFinalFormatado(){
		$vRetorno = number_format($this->nValorFinal , 2, ',', '.');
		return $vRetorno;
	}
	public function setValorFinalBanco($nValorFinal){
		if($nValorFinal){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorFinal = str_replace($sOrigem, $sDestino, $nValorFinal);

		}else{
			$this->nValorFinal = 'null';
		}
	}

}