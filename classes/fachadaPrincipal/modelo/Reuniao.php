<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela reuniao
  */
 class Reuniao{
 	/**
	* @campo cod_reuniao
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodReuniao;
	/**
	* @campo cod_projeto
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodProjeto;
	/**
	* @campo data_reuniao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dDataReuniao;
	/**
	* @campo hora_reuniao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sHoraReuniao;
	/**
	* @campo responsavel
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sResponsavel;
	/**
	* @campo participantes
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sParticipantes;
	/**
	* @campo assunto
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAssunto;
	/**
	* @campo local
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sLocal;
	/**
	* @campo decisao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDecisao;
	/**
	* @campo observacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sObservacao;
	/**
	* @campo inserido_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sInseridoPor;
	/**
	* @campo cod_cliente
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodCliente;
	/**
	* @campo de_acordo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDeAcordo;
	/**
	* @campo obs_cliente
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sObsCliente;
	/**
	* @campo data_cliente
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dDataCliente;
	/**
	* @campo id_escritorio
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nIdEscritorio;
	private $oProjeto;
	private $oCliente;


 	public function __construct(){

 	}

 	public function setCodReuniao($nCodReuniao){
		$this->nCodReuniao = $nCodReuniao;
	}
	public function getCodReuniao(){
		return $this->nCodReuniao;
	}
	public function setCodProjeto($nCodProjeto){
		$this->nCodProjeto = $nCodProjeto;
	}
	public function getCodProjeto(){
		return $this->nCodProjeto;
	}
	public function setDataReuniao($dDataReuniao){
		$this->dDataReuniao = $dDataReuniao;
	}
	public function getDataReuniao(){
		return $this->dDataReuniao;
	}

	public function getDataReuniaoFormatado(){
		if($this->dDataReuniao){
		$oData = new DateTime($this->dDataReuniao);
		 return $oData->format("d/m/Y");
		}else{
			return "";
		}
	}
	public function setDataReuniaoBanco($dDataReuniao){
		if($dDataReuniao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataReuniao);
			 $this->dDataReuniao = $oData->format('Y-m-d');
		}else{
			$this->dDataReuniao = 'NULL';
		}
	}


	public function setHoraReuniao($sHoraReuniao){
		$this->sHoraReuniao = $sHoraReuniao;
	}
	public function getHoraReuniao(){
		return $this->sHoraReuniao;
	}
	public function getHoraReuniaoFormatado(){
		return $this->sHoraReuniao;
	}
	public function setResponsavel($sResponsavel){
		$this->sResponsavel = $sResponsavel;
	}
	public function getResponsavel(){
		return $this->sResponsavel;
	}
	public function setParticipantes($sParticipantes){
		$this->sParticipantes = $sParticipantes;
	}
	public function getParticipantes(){
		return $this->sParticipantes;
	}
	public function setAssunto($sAssunto){
		$this->sAssunto = $sAssunto;
	}
	public function getAssunto(){
		return $this->sAssunto;
	}
	public function setLocal($sLocal){
		$this->sLocal = $sLocal;
	}
	public function getLocal(){
		return $this->sLocal;
	}
	public function setDecisao($sDecisao){
		$this->sDecisao = $sDecisao;
	}
	public function getDecisao(){
		return $this->sDecisao;
	}

	public function setObservacao($sObservacao){
		$this->sObservacao = $sObservacao;
	}
	public function getObservacao(){
		return $this->sObservacao;
	}
	public function setInseridoPor($sInseridoPor){
		$this->sInseridoPor = $sInseridoPor;
	}
	public function getInseridoPor(){
		return $this->sInseridoPor;
	}
	public function setCodCliente($nCodCliente){
		$this->nCodCliente = $nCodCliente;
	}
	public function getCodCliente(){
		return $this->nCodCliente;
	}
	public function setDeAcordo($sDeAcordo){
		$this->sDeAcordo = $sDeAcordo;
	}
	public function getDeAcordo(){
		return $this->sDeAcordo;
	}
	public function setObsCliente($sObsCliente){
		$this->sObsCliente = $sObsCliente;
	}
	public function getObsCliente(){
		return $this->sObsCliente;
	}
	public function setDataCliente($dDataCliente){
		$this->dDataCliente = $dDataCliente;
	}
	public function getDataCliente(){
		return $this->dDataCliente;
	}
	public function getDataClienteFormatado(){
		$oData = new DateTime($this->dDataCliente);		 return $oData->format("d/m/Y");
	}
	public function setDataClienteBanco($dDataCliente){
		 if($dDataCliente){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataCliente);
			 $this->dDataCliente = $oData->format('Y-m-d') ;
	}
		 }

	public function setIdEscritorio($nIdEscritorio){
		$this->nIdEscritorio = $nIdEscritorio;
	}
	public function getIdEscritorio(){
		return $this->nIdEscritorio;
	}
	public function setProjeto($oProjeto){
		$this->oProjeto = $oProjeto;
	}
	public function getProjeto(){
		$oFachada = new FachadaPrincipalBD();
		$this->oProjeto = $oFachada->recuperarUmProjeto($this->getCodProjeto());
		return $this->oProjeto;
	}
	public function setCliente($oCliente){
		$this->oCliente = $oCliente;
	}
	public function getCliente(){
		$oFachada = new FachadaPrincipalBD();
		$this->oCliente = $oFachada->recuperarUmCliente($this->getCodCliente());
		return $this->oCliente;
	}

 }
 ?>
