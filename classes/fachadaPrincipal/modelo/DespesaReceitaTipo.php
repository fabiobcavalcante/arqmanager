<?php
 /**
  * @author Auto-Generated 
  * @package fachadaPrincipal 
  * @SGBD mysql 
  * @tabela despesa_receita_tipo 
  */
 class DespesaReceitaTipo{
 	/**
	* @campo cod_despesa_receita_tipo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodDespesaReceitaTipo;
	/**
	* @campo abreviacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAbreviacao;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo entrada_saida
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEntradaSaida;     
	/**
	* @campo incluido_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sIncluidoPor;
	/**
	* @campo alterado_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAlteradoPor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodDespesaReceitaTipo($nCodDespesaReceitaTipo){
		$this->nCodDespesaReceitaTipo = $nCodDespesaReceitaTipo;
	}
	public function getCodDespesaReceitaTipo(){
		return $this->nCodDespesaReceitaTipo;
	}
	public function setAbreviacao($sAbreviacao){
		$this->sAbreviacao = $sAbreviacao;
	}
	public function getAbreviacao(){
		return $this->sAbreviacao;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
    public function getEntradaSaida(){
		return $this->sEntradaSaida;
	} 
     
    public function getEntradaSaidaFormatada(){
        switch($this->sEntradaSaida){
            case 'E':
               $sFormatado = "<div class='btn-primary'>ENTRADA</div>" ;
            break;
            case 'S':
              $sFormatado = "<div class='btn-danger'>SAÍDA</div>" ;             
            break;
                
        }
		return $sFormatado;
	}  
	public function setEntradaSaida($sEntradaSaida){
        $this->sEntradaSaida = $sEntradaSaida;
    }
	public function setIncluidoPor($sIncluidoPor){
		$this->sIncluidoPor = $sIncluidoPor;
	}
	public function getIncluidoPor(){
		return $this->sIncluidoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
 }
 ?>
