<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela servico_microservico
 */
class ServicoMicroServico{
	/**
	 * @campo cod_microservico
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodMicroservico;

	/**
	 * @campo cod_etapa
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodEtapa;
	/**
	 * @campo descricao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sDescricao;
	/**
	 * @campo cor
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sCor;
	/**
	 * @campo ordem
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nOrdem;
	/**
	 * @campo prazo
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nPrazo;
	/**
	 * @campo detalhe
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nDetalhe;
	private $oServicoEtapa;

	public function setCodMicroservico($nCodMicroservico){
		$this->nCodMicroservico = $nCodMicroservico;
	}
	public function getCodMicroservico(){
		return $this->nCodMicroservico;
	}

	public function setCodEtapa($nCodEtapa){
		$this->nCodEtapa = $nCodEtapa;
	}
	public function getCodEtapa(){
		return $this->nCodEtapa;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setCor($sCor){
		$this->sCor = $sCor;
	}
	public function getCor(){
		return $this->sCor;
	}
	public function setOrdem($nOrdem){
		$this->nOrdem = $nOrdem;
	}
	public function getOrdem(){
		return $this->nOrdem;
	}
	public function setPrazo($nPrazo){
		$this->nPrazo = $nPrazo;
	}
	public function getPrazo(){
		return $this->nPrazo;
	}
	public function setDetalhe($nDetalhe){
		$this->nDetalhe = $nDetalhe;
	}
	public function getDetalhe(){
		return $this->nDetalhe;
	}
	public function setServicoEtapa($oServicoEtapa){
		$this->oServicoEtapa = $oServicoEtapa;
	}
	public function getServicoEtapa(){
		$oFachada = new FachadaPrincipalBD();
		$this->oServicoEtapa = $oFachada->recuperarUmServicoEtapa($this->getCodEtapa());
		return $this->oServicoEtapa;
	}

}