<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela status
  */
 class Status{
 	/**
	* @campo cod_status
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodStatus;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
  /**
  * @campo cor
  * @var String
  * @primario false
  * @nulo false
  * @auto-increment false
  */
  private $sCor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;



 	public function __construct(){

 	}

 	public function setCodStatus($nCodStatus){
		$this->nCodStatus = $nCodStatus;
	}
	public function getCodStatus(){
		return $this->nCodStatus;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
  public function setCor($sCor){
    $this->sCor = $sCor;
  }
  public function getCor(){
    return $this->sCor;
  }
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
 }
 ?>
