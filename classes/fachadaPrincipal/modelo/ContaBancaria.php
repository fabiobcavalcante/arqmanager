<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela conta_bancaria
 */
class ContaBancaria{
    /**
     * @campo cod_conta_bancaria
     * @var number
     * @primario true
     * @nulo false
     * @auto-increment true
     */
    private $nCodContaBancaria;
    /**
     * @campo cod_colaborador
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodColaborador;
    /**
     * @campo agencia
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sAgencia;
    /**
     * @campo conta
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sConta;
    /**
     * @campo tipo
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nTipo;
    /**
     * @campo banco
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sBanco;
    /**
     * @campo incluido_por
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sIncluidoPor;
    /**
     * @campo alterado_por
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sAlteradoPor;
    /**
     * @campo data_encerramento
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $dDataEncerramento;

    /**
     * @campo ativo
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nAtivo;
    /**
     * @campo valor
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment true
     */
    private $nValor;
    /**
     * @campo id_escritorio
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nIdEscritorio;
    /**
     * @campo saldo_inicial
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nSaldoInicial;
    /**
     * @campo data_inicio
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $dDataInicio;

    private $oEscritorio;
    private $oColaborador;

    public function setCodContaBancaria($nCodContaBancaria){
        $this->nCodContaBancaria = $nCodContaBancaria;
    }
    public function getCodContaBancaria(){
        return $this->nCodContaBancaria;
    }
    public function setCodColaborador($nCodColaborador){
        $this->nCodColaborador = $nCodColaborador;
    }
    public function getCodColaborador(){
        return $this->nCodColaborador;
    }
    public function setAgencia($sAgencia){
        $this->sAgencia = $sAgencia;
    }
    public function getAgencia(){
        return $this->sAgencia;
    }
    public function setConta($sConta){
        $this->sConta = $sConta;
    }
    public function getConta(){
        return $this->sConta;
    }

    public function setTipo($nTipo){
        $this->nTipo = $nTipo;
    }
    public function getTipo(){
        return $this->nTipo;
    }
    public function getTipoFormatado(){
        switch($this->nTipo) {
            case 0:
                return "Conta Corrente";
            case 1:
                return "Conta Salário";
            case 2:
                return "Conta Poupança";
            case 3:
                return "Outros";
            default:
                return false;
        }
    }
    public function setBanco($sBanco){
        $this->sBanco = $sBanco;
    }
    public function getBanco(){
        return $this->sBanco;
    }
    public function setIncluidoPor($sIncluidoPor){
        $this->sIncluidoPor = $sIncluidoPor;
    }
    public function getIncluidoPor(){
        return $this->sIncluidoPor;
    }
    public function setAlteradoPor($sAlteradoPor){
        $this->sAlteradoPor = $sAlteradoPor;
    }
    public function getAlteradoPor(){
        return $this->sAlteradoPor;
    }
    public function setDataEncerramento($dDataEncerramento){
        $this->dDataEncerramento = $dDataEncerramento;
    }
    public function getDataEncerramento(){
        return $this->dDataEncerramento;
    }
    public function getDataEncerramentoFormatado(){
        if($this->dDataEncerramento){
            try {
                $oData = new DateTime($this->dDataEncerramento);
                return $oData->format("d/m/Y");
            } catch (Exception $e) {
                return false;
            }
        } else
            return false;
    }
    public function setDataEncerramentoBanco($dDataEncerramento){
        if($dDataEncerramento){
            $oData = DateTime::createFromFormat('d/m/Y', $dDataEncerramento);
            $this->dDataEncerramento = $oData->format('Y-m-d') ;
        }
    }


    public function setAtivo($nAtivo){
        $this->nAtivo = $nAtivo;
    }
    public function getAtivo(){
        return $this->nAtivo;
    }
    public function setValor($nValor){
        $this->nValor = $nValor;
    }
    public function getValor(){
        return $this->nValor;
    }
    public function getValorFormatado(){
        return number_format($this->nValor , 2, ',', '.');
    }
    public function setValorBanco($nValor){
        if($nValor){
            $sOrigem = array('.',',');
            $sDestino = array('','.');
            $this->nValor = str_replace($sOrigem, $sDestino, $nValor);
        }else{
            $this->nValor = 'null';
        }
    }

    public function setIdEscritorio($nIdEscritorio){
        $this->nIdEscritorio = $nIdEscritorio;
    }
    public function getIdEscritorio(){
        return $this->nIdEscritorio;
    }


    public function setDataInicio($dDataInicio){
        $this->dDataInicio = $dDataInicio;
    }
    public function getDataInicio(){
        return $this->dDataInicio;
    }
    public function getDataInicioFormatado(){
        if($this->dDataInicio){
            try {
                $oData = new DateTime($this->dDataInicio);
                return $oData->format("d/m/Y");
            } catch (Exception $e) {
                return false;
            }
        } else
            return false;
    }
    public function setDataInicioBanco($dDataInicio){
        if($dDataInicio){
            $oData = DateTime::createFromFormat('d/m/Y', $dDataInicio);
            $this->dDataInicio = $oData->format('Y-m-d') ;
        }
    }
    public function setSaldoInicial($nSaldoInicial){
        $this->nSaldoInicial = $nSaldoInicial;
    }
    public function getSaldoInicial(){
        return $this->nSaldoInicial;
    }
    public function getSaldoInicialFormatado(){
        return number_format($this->nSaldoInicial , 2, ',', '.');
    }
    public function setSaldoInicialBanco($nSaldoInicial){
        if($nSaldoInicial){
            $sOrigem = array('.',',');
            $sDestino = array('','.');
            $this->nSaldoInicial = str_replace($sOrigem, $sDestino, $nSaldoInicial);
        }else{
            $this->nSaldoInicial = 'null';
        }
    }

    public function getEscritorio($nIdEscritorio){
        $oFachada = new FachadaPrincipalBD();
        $this->oEscritorio = $oFachada->recuperarUmEscritorio($nIdEscritorio);
        return $this->oEscritorio;
    }
    public function setColaborador($oColaborador){
        $this->oColaborador = $oColaborador;
    }
    public function getColaborador(){
        $oFachada = new FachadaPrincipalBD();
        $this->oColaborador = $oFachada->recuperarUmColaborador($this->getCodColaborador());
        return $this->oColaborador;
    }
}
