<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela colaborador_escritorio_grupo
 */
class ColaboradorEscritorioGrupo{
	/**
	 * @campo cod_colaborador
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodColaborador;
	/**
	 * @campo id_escritorio
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nIdEscritorio;
	/**
	 * @campo cod_grupo_usuario
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodGrupoUsuario;

	private $oEscritorio;
    private $oColaborador;

	public function setCodColaborador($nCodColaborador){
		$this->nCodColaborador = $nCodColaborador;
	}
	public function getCodColaborador(){
		return $this->nCodColaborador;
	}

	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}

	public function setIdEscritorio($nIdEscritorio){
		$this->nIdEscritorio = $nIdEscritorio;
	}
	public function getIdEscritorio(){
		return $this->nIdEscritorio;
	}
	public function getGrupoUsuario(){
		$oFachada = new FachadaAcessoBD();
        return $oFachada->recuperarUmAcessoGrupoUsuario($this->nCodGrupoUsuario);
	}
	public function setEscritorio($oEscritorio){
		$this->oEscritorio = $oEscritorio;
	}
	public function getEscritorio(){
		$oFachada = new FachadaPrincipalBD();
		$this->oEscritorio = $oFachada->recuperarUmEscritorio($this->getIdEscritorio());
		return $this->oEscritorio;
	}

	public function setColaborador($oColaborador){
		$this->oColaborador = $oColaborador;
	}
	public function getColaborador(){
		$oFachada = new FachadaPrincipalBD();
		$this->oColaborador = $oFachada->recuperarUmColaborador($this->getCodColaborador());
		return $this->oColaborador;
	}
}