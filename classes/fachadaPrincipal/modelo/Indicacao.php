<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela indicacao
  */
 class Indicacao{
 	/**
	* @campo cod_indicacao
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodIndicacao;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
    private $oPermissao;
    /**
    * @campo id_escritorio
    * @var number
    * @primario false
    * @nulo false
    * @auto-increment false
    */
    private $nIdEscritorio;
    private $oEscritorio;


 	public function __construct(){

 	}

 	public function setCodIndicacao($nCodIndicacao){
		$this->nCodIndicacao = $nCodIndicacao;
	}
	public function getCodIndicacao(){
		return $this->nCodIndicacao;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

  public function setIdEscritorio($nIdEscritorio){
    $this->nIdEscritorio = $nIdEscritorio;
  }
  public function getIdEscritorio(){
    return $this->nIdEscritorio;
  }
  public function getEscritorio($nIdEscritorio){
    $oFachada = new FachadaAdminBD();
    $this->oEscritorio = $oFachada->recuperarUmEscritorio($nIdEscritorio);
    return $this->oEscritorio;
  }

 }
 ?>
