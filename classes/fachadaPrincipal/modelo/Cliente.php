<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela cliente
  */
 class Cliente{
 	/**
	* @campo cod_cliente
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodCliente;

	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo cod_tipo_pessoa
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodTipoPessoa;
	/**
	* @campo identificacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sIdentificacao;
	/**
	* @campo email
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmail;
	/**
	* @campo cep
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCep;
	/**
	* @campo logradouro
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sLogradouro;
	/**
	* @campo numero
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNumero;
	/**
	* @campo complemento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sComplemento;
	/**
	* @campo bairro
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sBairro;
	/**
	* @campo cidade
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCidade;
	/**
	* @campo uf
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUf;
	/**
	* @campo telefones
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTelefones;
	/**
	* @campo data_nascimento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dDataNascimento;
	/**
	* @campo razao_social
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sRazaoSocial;
	/**
	* @campo inscricao_estadual
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sInscricaoEstadual;
	/**
	* @campo inscricao_municipal
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sInscricaoMunicipal;
	/**
	* @campo observacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sObservacao;
	/**
	* @campo indicado_por
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nIndicadoPor;
	/**
	* @campo incluido_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sIncluidoPor;
	/**
	* @campo alterado_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAlteradoPor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
  /**
	* @campo id_escritorio
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nIdEscritorio;
	private $oEscritorio;
	private $oIndicacao;
	private $oClienteColaborador;


 	public function __construct(){

 	}

 	public function setCodCliente($nCodCliente){
		$this->nCodCliente = $nCodCliente;
	}
	public function getCodCliente(){
		return $this->nCodCliente;
	}

	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setCodTipoPessoa($nCodTipoPessoa){
		$this->nCodTipoPessoa = $nCodTipoPessoa;
	}
	public function getCodTipoPessoa(){
		return $this->nCodTipoPessoa;
	}
	public function getCodTipoPessoaFormatada(){
		switch($this->nCodTipoPessoa){
			case 1: $sTipoPessoa = 'Física'; break;
			case 2: $sTipoPessoa = 'Jurídica'; break;
		}
		return $sTipoPessoa;
	}
	public function setIdentificacao($sIdentificacao){
		$this->sIdentificacao = $sIdentificacao;
	}
	public function getIdentificacao(){
		return $this->sIdentificacao;
	}

	public function setEmail($sEmail){
		$this->sEmail = $sEmail;
	}
	public function getEmail(){
		return $this->sEmail;
	}
	public function setCep($sCep){
		$this->sCep = $sCep;
	}
	public function getCep(){
		return $this->sCep;
	}
	public function setLogradouro($sLogradouro){
		$this->sLogradouro = $sLogradouro;
	}
	public function getLogradouro(){
		return $this->sLogradouro;
	}
	public function setNumero($sNumero){
		$this->sNumero = $sNumero;
	}
	public function getNumero(){
		return $this->sNumero;
	}
	public function setComplemento($sComplemento){
		$this->sComplemento = $sComplemento;
	}
	public function getComplemento(){
		return $this->sComplemento;
	}
	public function setBairro($sBairro){
		$this->sBairro = $sBairro;
	}
	public function getBairro(){
		return $this->sBairro;
	}
	public function setCidade($sCidade){
		$this->sCidade = $sCidade;
	}
	public function getCidade(){
		return $this->sCidade;
	}
	public function setUf($sUf){
		$this->sUf = $sUf;
	}
	public function getUf(){
		return $this->sUf;
	}
	public function setTelefones($sTelefones){
		$this->sTelefones = $sTelefones;
	}
	public function getTelefones(){
		return $this->sTelefones;
	}
	public function setDataNascimento($dDataNascimento){
		$this->dDataNascimento = $dDataNascimento;
	}
	public function getDataNascimento(){
		return $this->dDataNascimento;
	}
	public function getDataNascimentoFormatado(){
		if($this->dDataNascimento){
		$oData = new DateTime($this->dDataNascimento);
		 return $oData->format("d/m/Y");
		}else{
			return "";
		}
	}
	public function getDataAniversario(){
		if($this->dDataNascimento){
		$oData = new DateTime($this->dDataNascimento);
		 return $oData->format("d/m");
		}else{
			return "";
		}
	}
	public function setDataNascimentoBanco($dDataNascimento){
		if($dDataNascimento){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataNascimento);
			 $this->dDataNascimento = $oData->format('Y-m-d');
		}else{
			$this->dDataNascimento = 'NULL';
		}
	}

	public function setRazaoSocial($sRazaoSocial){
		$this->sRazaoSocial = $sRazaoSocial;
	}
	public function getRazaoSocial(){
		return $this->sRazaoSocial;
	}
	public function setInscricaoEstadual($sInscricaoEstadual){
		$this->sInscricaoEstadual = $sInscricaoEstadual;
	}
	public function getInscricaoEstadual(){
		return $this->sInscricaoEstadual;
	}
	public function setInscricaoMunicipal($sInscricaoMunicipal){
		$this->sInscricaoMunicipal = $sInscricaoMunicipal;
	}
	public function getInscricaoMunicipal(){
		return $this->sInscricaoMunicipal;
	}
	public function setObservacao($sObservacao){
		$this->sObservacao = $sObservacao;
	}
	public function getObservacao(){
		return $this->sObservacao;
	}
	public function setIndicadoPor($nIndicadoPor){
		$this->nIndicadoPor = $nIndicadoPor;
	}
	public function getIndicadoPor(){
		return $this->nIndicadoPor;
	}
	public function setIncluidoPor($sIncluidoPor){
		$this->sIncluidoPor = $sIncluidoPor;
	}
	public function getIncluidoPor(){
		return $this->sIncluidoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setIdEscritorio($nIdEscritorio){
		$this->nIdEscritorio = $nIdEscritorio;
	}
	public function getIdEscritorio(){
		return $this->nIdEscritorio;
	}


	public function getClienteColaborador($sIdentificacao){
		$oFachada = new FachadaPrincipalBD();
		$this->oClienteColaborador = $oFachada->recuperarUmClienteColaborador($sIdentificacao);
		return $this->oClienteColaborador;
	}
	public function getEscritorio($nIdEscritorio){
		$oFachada = new FachadaPrincipalBD();
		$this->oEscritorio = $oFachada->recuperarUmEscritorio($nIdEscritorio);
		return $this->oEscritorio;
	}
	public function getIndicacao(){
		$oFachada = new FachadaPrincipalBD();
		$this->oIndicacao = $oFachada->recuperarUmIndicacao($this->nIndicadoPor);
		return $this->oIndicacao;
	}

 }
 ?>
