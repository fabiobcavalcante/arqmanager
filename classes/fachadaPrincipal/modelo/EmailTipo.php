<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela email_tipo
  */
 class EmailTipo{
 	/**
	* @campo cod_email_tipo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodEmailTipo;
	/**
	* @campo titulo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTitulo;
	/**
	* @campo template
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTemplate;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;


 	public function __construct(){

 	}

 	public function setCodEmailTipo($nCodEmailTipo){
		$this->nCodEmailTipo = $nCodEmailTipo;
	}
	public function getCodEmailTipo(){
		return $this->nCodEmailTipo;
	}

	public function setTitulo($sTitulo){
		$this->sTitulo = $sTitulo;
	}
	public function getTitulo(){
		return $this->sTitulo;
	}
	public function setTemplate($sTemplate){
		$this->sTemplate = $sTemplate;
	}
	public function getTemplate(){
		return $this->sTemplate;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

 }
 ?>
