<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela colaborador_producao
  */
 class ColaboradorProducao{
 	/**
	* @campo cod_producao
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodProducao;
	/**
	* @campo cod_colaborador
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodColaborador;
	/**
	* @campo cod_projeto
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodProjeto;
	/**
	 * @campo percentual
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nPercentual;
	/**
	 * @campo valor
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nValor;

	private $oProjeto;
	private $oColaborador;


 	public function __construct(){

 	}

 	public function setCodProducao($nCodProducao){
		$this->nCodProducao = $nCodProducao;
	}
	public function getCodProducao(){
		return $this->nCodProducao;
	}
 	public function setCodColaborador($nCodColaborador){
		$this->nCodColaborador = $nCodColaborador;
	}
	public function getCodColaborador(){
		return $this->nCodColaborador;
	}

	public function setCodProjeto($nCodProjeto){
		$this->nCodProjeto = $nCodProjeto;
	}
	public function getCodProjeto(){
		return $this->nCodProjeto;
	}

	public function setValor($nValor){
		$this->nValor = $nValor;
	}
	public function getValor(){
		return $this->nValor;
	}
	public function getValorFormatado(){
		$vRetorno = number_format($this->nValor , 2, ',', '.');
		return $vRetorno;
	}
	public function setValorBanco($nValor){
		if($nValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValor = str_replace($sOrigem, $sDestino, $nValor);

		}else{
			$this->nValor = 'null';
		}
	}

	public function setPercentual($nPercentual){
		$this->nPercentual = $nPercentual;
	}
	public function getPercentual(){
		return $this->nPercentual;
	}
	public function getPercentualFormatado(){
		$vRetorno = number_format($this->nPercentual , 2, ',', '.');
		return $vRetorno;
	}
	public function setPercentualBanco($nPercentual){
		if($nPercentual){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nPercentual = str_replace($sOrigem, $sDestino, $nPercentual);

		}else{
			$this->nPercentual = 'null';
		}
	}


	public function setColaborador($oColaborador){
		$this->oColaborador = $oColaborador;
	}
	public function getColaborador(){
		$oFachada = new FachadaPrincipalBD();
		$this->oColaborador = $oFachada->recuperarUmColaborador($this->getCodColaborador());
		return $this->oColaborador;
	}

	public function setProjeto($oProjeto){
		$this->oProjeto = $oProjeto;
	}
	public function getProjeto(){
		$oFachada = new FachadaPrincipalBD();
		$this->oProjeto = $oFachada->recuperarUmProjeto($this->getCodProjeto());
		return $this->oProjeto;
	}
 }
 ?>
