<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela despesa_receita
  */
 class DespesaReceita{
 	/**
	* @campo cod_despesa_receita
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodDespesaReceita;
	/**
	* @campo cod_despesa_receita_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodDespesaReceitaTipo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo incluido_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sIncluidoPor;
	/**
	* @campo alterado_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAlteradoPor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
  /**
	* @campo id_escritorio
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nIdEscritorio;
	private $oEscritorio;


 	public function __construct(){

 	}

 	public function setCodDespesaReceita($nCodDespesaReceita){
		$this->nCodDespesaReceita = $nCodDespesaReceita;
	}
	public function getCodDespesaReceita(){
		return $this->nCodDespesaReceita;
	}
	public function setCodDespesaReceitaTipo($nCodDespesaReceitaTipo){
		$this->nCodDespesaReceitaTipo = $nCodDespesaReceitaTipo;
	}
	public function getCodDespesaReceitaTipo(){
		return $this->nCodDespesaReceitaTipo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setIncluidoPor($sIncluidoPor){
		$this->sIncluidoPor = $sIncluidoPor;
	}
	public function getIncluidoPor(){
		return $this->sIncluidoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
  public function setIdEscritorio($nIdEscritorio){
		$this->nIdEscritorio = $nIdEscritorio;
	}
	public function getIdEscritorio(){
		return $this->nIdEscritorio;
	}
  public function getEscritorio($nIdEscritorio){
    $oFachada = new FachadaAdminBD();
    $this->oEscritorio = $oFachada->recuperarUmEscritorio($nIdEscritorio);
    return $this->oEscritorio;
  }
 }
 ?>
