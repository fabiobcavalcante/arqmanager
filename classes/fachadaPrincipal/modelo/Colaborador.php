<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela colaborador
 */
class Colaborador{
	/**
	 * @campo cod_colaborador
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodColaborador;
	/**
	 * @campo nome
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sNome;
	/**
	 * @campo data_nascimento
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $dDataNascimento;
	/**
	 * @campo banco
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sBanco;
	/**
	 * @campo agencia
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sAgencia;
	/**
	 * @campo tipo_conta
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sTipoConta;
	/**
	 * @campo conta
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sConta;
	/**
	 * @campo pix_tipo
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sPixTipo;
	/**
	 * @campo pix_chave
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sPixChave;
	/**
	 * @campo cpf
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sCpf;

	/**
	 * @campo login
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sLogin;
	/**
	 * @campo senha
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sSenha;
	/**
	 * @campo foto
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $sFoto;
	/**
	 * @campo foto_tipo
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $sFotoTipo;
	/**
	 * @campo email
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sEmail;
	/**
	 * @campo inserido_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sInseridoPor;
	/**
	 * @campo alterado_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sAlteradoPor;
	/**
	 * @campo ativo
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nAtivo;
	/**
	 * @campo tipo
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nTipo;


	public function __construct(){

	}

	public function setCodColaborador($nCodColaborador){
		$this->nCodColaborador = $nCodColaborador;
	}
	public function getCodColaborador(){
		return $this->nCodColaborador;
	}
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setPixTipo($sPixTipo){
		$this->sPixTipo = $sPixTipo;
	}
	public function getPixTipo(){
		return $this->sPixTipo;
	}
	public function getPixChave(){
		return $this->sPixChave;
	}
	public function setPixChave($sPixChave){
		$this->sPixChave = $sPixChave;
	}

	public function setDataNascimento($dDataNascimento){
		$this->dDataNascimento = $dDataNascimento;
	}
	public function getDataNascimento(){
		return $this->dDataNascimento;
	}
	public function getDataNascimentoFormatado(){
		$oData = new DateTime($this->dDataNascimento);
		return $oData->format("d/m/Y");
	}
	public function setDataNascimentoBanco($dDataNascimento){
		if($dDataNascimento){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataNascimento);
			$this->dDataNascimento = $oData->format('Y-m-d') ;
		}
	}

	public function setBanco($sBanco){
		$this->sBanco = $sBanco;
	}
	public function getBanco(){
		return $this->sBanco;
	}
	public function setAgencia($sAgencia){
		$this->sAgencia = $sAgencia;
	}
	public function getAgencia(){
		return $this->sAgencia;
	}
	public function setTipoConta($sTipoConta){
		$this->sTipoConta = $sTipoConta;
	}
	public function getTipoConta(){
		return $this->sTipoConta;
	}
	public function setConta($sConta){
		$this->sConta = $sConta;
	}
	public function getConta(){
		return $this->sConta;
	}
	public function setCpf($sCpf){
		$this->sCpf = $sCpf;
	}
	public function getCpf(){
		return $this->sCpf;
	}

	public function setLogin($sLogin){
		$this->sLogin = $sLogin;
	}
	public function getLogin(){
		return $this->sLogin;
	}

	public function setFoto($sFoto){
		$this->sFoto = $sFoto;
	}
	public function getFoto(){
		return $this->sFoto;
	}
	public function setFotoTipo($sFotoTipo){
		$this->sFotoTipo = $sFotoTipo;
	}
	public function getFotoTipo(){
		return $this->sFotoTipo;
	}
	public function setSenha($sSenha){
		$this->sSenha = $sSenha;
	}
	public function getSenha(){
		return $this->sSenha;
	}
	public function setEmail($sEmail){
		$this->sEmail = $sEmail;
	}
	public function getEmail(){
		return $this->sEmail;
	}
	public function setInseridoPor($sInseridoPor){
		$this->sInseridoPor = $sInseridoPor;
	}
	public function getInseridoPor(){
		return $this->sInseridoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setTipo($nTipo){
		$this->nTipo = $nTipo;
	}
	public function getTipo(){
		return $this->nTipo;
	}
}