<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela conta_movimentacao
 */
class ContaMovimentacao{
	/**
	 * @campo cod_conta_movimentacao
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodContaMovimentacao;
	/**
	 * @campo cod_conta_bancaria
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodContaBancaria;

	/**
	 * @campo tipo
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sTipo;
	/**
	 * @campo valor_inicial
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nValorInicial;
	/**
	 * @campo valor_despesa
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nValorDespesa;
	/**
	 * @campo valor_receita
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nValorReceita;
	/**
	 * @campo data_movimentacao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $dDataMovimentacao;
	/**
	 * @campo ativo
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nAtivo;
	/**
	 * @campo cod_movimento
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodMovimento;
	/**
	 * @campo cod_projeto_pagamento
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodProjetoPagamento;
	private $oContaBancaria;

	public function setCodContaMovimentacao($nCodContaMovimentacao){
		$this->nCodContaMovimentacao = $nCodContaMovimentacao;
	}
	public function getCodContaMovimentacao(){
		return $this->nCodContaMovimentacao;
	}
	public function setCodContaBancaria($nCodContaBancaria){
		$this->nCodContaBancaria = $nCodContaBancaria;
	}
	public function getCodContaBancaria(){
		return $this->nCodContaBancaria;
	}
	public function setTipo($sTipo){
		$this->sTipo = $sTipo;
	}
	public function getTipo(){
		return $this->sTipo;
	}
	public function getTipoFormatado(){
		switch($this->sTipo){
			case 'S': return "SAÍDA";   break;
			case 'A': return "APORTE"; break;
			case 'E': return "ENTRADA"; break;
			default:  return "VAZIO";   break;
		}

	}
	public function setValorInicial($nValorInicial){
		$this->nValorInicial = $nValorInicial;
	}
	public function getValorInicial(){
		return $this->nValorInicial;
	}
	public function getValorInicialFormatado(){
		$vRetorno = number_format($this->nValorInicial , 2, ',', '.');
		return $vRetorno;
	}
	public function setValorInicialBanco($nValorInicial){
		if($nValorInicial){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorInicial = str_replace($sOrigem, $sDestino, $nValorInicial);

		}else{
			$this->nValorInicial = 'null';
		}
	}
	public function setValorDespesa($nValorDespesa){
		$this->nValorDespesa = $nValorDespesa;
	}
	public function getValorDespesa(){
		return $this->nValorDespesa;
	}
	public function getValorDespesaFormatado(){
		$vRetorno = number_format($this->nValorDespesa , 2, ',', '.');
		return $vRetorno;
	}
	public function setValorDespesaBanco($nValorDespesa){
		if($nValorDespesa){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorDespesa = str_replace($sOrigem, $sDestino, $nValorDespesa);

		}else{
			$this->nValorDespesa = 'null';
		}
	}
	public function setValorReceita($nValorReceita){
		$this->nValorReceita = $nValorReceita;
	}
	public function getValorReceita(){
		return $this->nValorReceita;
	}
	public function getValorReceitaFormatado(){
		$vRetorno = number_format($this->nValorReceita , 2, ',', '.');
		return $vRetorno;
	}
	public function setValorReceitaBanco($nValorReceita){
		if($nValorReceita){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorReceita = str_replace($sOrigem, $sDestino, $nValorReceita);

		}else{
			$this->nValorReceita = 'null';
		}
	}
	public function setDataMovimentacao($dDataMovimentacao){
		$this->dDataMovimentacao = $dDataMovimentacao;
	}
	public function getDataMovimentacao(){
		return $this->dDataMovimentacao;
	}
	public function getDataMovimentacaoFormatado(){
		if($this->dDataMovimentacao){
			$oData = new DateTime($this->dDataMovimentacao);
			return $oData->format("d/m/Y");
		}
	}
	public function setDataMovimentacaoBanco($dDataMovimentacao){
		if($dDataMovimentacao){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataMovimentacao);
			$this->dDataMovimentacao = $oData->format('Y-m-d') ;
		}
	}

	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
    public function setCodMovimento($nCodMovimento){
        $this->nCodMovimento = $nCodMovimento;
    }
    public function getCodMovimento(){
        return $this->nCodMovimento;
    }
    public function setCodProjetoPagamento($nCodProjetoPagamento){
        $this->nCodProjetoPagamento = $nCodProjetoPagamento;
    }
    public function getCodProjetoPagamento(){
        return $this->nCodProjetoPagamento;
    }
	public function setContaBancaria($oContaBancaria){
		$this->oContaBancaria = $oContaBancaria;
	}
	public function getContaBancaria(){
		$oFachada = new FachadaPrincipalBD();
		$this->oContaBancaria = $oFachada->recuperarUmContaBancaria($this->getCodContaBancaria());
		return $this->oContaBancaria;
	}

}