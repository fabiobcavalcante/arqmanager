<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela email_tipo_escritorio
  */
 class EmailTipoEscritorio{
 	/**
	* @campo cod_email_tipo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodEmailTipo;

  /**
  * @campo id_escritorio
  * @var number
  * @primario true
  * @nulo false
  * @auto-increment false
  */
  private $nIdEscritorio;
/**
	* @campo template
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTemplate;
  /**
  * @campo ativo
  * @var number
  * @primario false
  * @nulo false
  * @auto-increment false
  */
  private $nAtivo;  
  private $oEscritorio;


 	public function __construct(){

 	}

 	public function setCodEmailTipo($nCodEmailTipo){
		$this->nCodEmailTipo = $nCodEmailTipo;
	}
	public function getCodEmailTipo(){
		return $this->nCodEmailTipo;
	}


  public function setIdEscritorio($nIdEscritorio){
    $this->nIdEscritorio = $nIdEscritorio;
  }
  public function getIdEscritorio(){
    return $this->nIdEscritorio;
  }

	public function setTemplate($sTemplate){
		$this->sTemplate = $sTemplate;
	}
	public function getTemplate(){
		return $this->sTemplate;
	}
  
  public function setAtivo($nAtivo){
    $this->nAtivo = $nAtivo;
  }
  public function getAtivo(){
    return $this->nAtivo;
  }  

  public function getEscritorio($nIdEscritorio){
    $oFachada = new FachadaAdminBD();
    $this->oEscritorio = $oFachada->recuperarUmEscritorio($nIdEscritorio);
    return $this->oEscritorio;
  }

 }
 ?>
