<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela documento_escritorio
  */
 class DocumentoEscritorio{
 	/**
	* @campo cod_documento
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodDocumento;
	/**
	* @campo id_escritorio
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nIdEscritorio;
	/**
	* @campo template
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTemplate;

  private $oDocumento;
  private $oEscritorio;

 	public function __construct(){

 	}

 	public function setCodDocumento($nCodDocumento){
		$this->nCodDocumento = $nCodDocumento;
	}
	public function getCodDocumento(){
		return $this->nCodDocumento;
	}
	public function setTemplate($sTemplate){
		$this->sTemplate = $sTemplate;
	}
	public function getTemplate(){
		return $this->sTemplate;
	}
  public function setIdEscritorio($nIdEscritorio){
    $this->nIdEscritorio = $nIdEscritorio;
  }
  public function getIdEscritorio(){
    return $this->nIdEscritorio;
  }
  public function getEscritorio($nIdEscritorio){
    $oFachada = new FachadaPrincipalBD();
    $this->oEscritorio = $oFachada->recuperarUmEscritorio($nIdEscritorio);
    return $this->oEscritorio;
  }
  public function getDocumento($nCodDocumento){
    $oFachada = new FachadaPrincipalBD();
    $this->oDocumento = $oFachada->recuperarUmDocumento($nCodDocumento);
    return $this->oDocumento;
  }

 }
 ?>
