<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela etapa
 */
class Etapa{
    /**
     * @campo cod_etapa
     * @var number
     * @primario true
     * @nulo false
     * @auto-increment true
     */
    private $nCodEtapa;
    /**
     * @campo descricao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sDescricao;
    /**
     * @campo ordem
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nOrdem;
    /**
     * @campo exibir
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nExibir;
    /**
     * @campo id_escritorio
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nIdEscritorio;

    private $oEscritorio;

    public function setCodEtapa($nCodEtapa){
        $this->nCodEtapa = $nCodEtapa;
    }
    public function getCodEtapa(){
        return $this->nCodEtapa;
    }
    public function setDescricao($sDescricao){
        $this->sDescricao = $sDescricao;
    }
    public function getDescricao(){
        return $this->sDescricao;
    }
    public function setOrdem($nOrdem){
        $this->nOrdem = $nOrdem;
    }
    public function getOrdem(){
        return $this->nOrdem;
    }
    public function setExibir($nExibir){
        $this->nExibir = $nExibir;
    }
    public function getExibir(){
        return $this->nExibir;
    }
    public function setIdEscritorio($nIdEscritorio){
        $this->nIdEscritorio = $nIdEscritorio;
    }
    public function getIdEscritorio(){
        return $this->nIdEscritorio;
    }
    public function getEscritorio(){
        $oFachada = new FachadaPrincipalBD();
        $this->oEscritorio = $oFachada->recuperarUmEscritorio($this->getIdEscritorio());
        return $this->oEscritorio;
    }

}
