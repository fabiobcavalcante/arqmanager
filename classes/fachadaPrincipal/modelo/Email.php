<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela email
  */
 class Email{
 	/**
	* @campo cod_email
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodEmail;
 	/**
	* @campo cod_tipo_email
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodTipoEmail;
	/**
	* @campo cod_cliente
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodCliente;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo data_hora
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dDataHora;
	private $oCliente;
	private $oEmailTipo;


 	public function __construct(){

 	}

 	public function setCodEmail($nCodEmail){
		$this->nCodEmail = $nCodEmail;
	}
	public function getCodEmail(){
		return $this->nCodEmail;
	}
 	public function setCodTipoEmail($nCodTipoEmail){
		$this->nCodTipoEmail = $nCodTipoEmail;
	}
	public function getCodTipoEmail(){
		return $this->nCodTipoEmail;
	}

	public function setCodCliente($nCodCliente){
		$this->nCodCliente = $nCodCliente;
	}
	public function getCodCliente(){
		return $this->nCodCliente;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setDataHora($dDataHora){
		$this->dDataHora = $dDataHora;
	}
	public function getDataHora(){
		return $this->dDataHora;
	}

	public function getDataHoraFormatado(){
		$oData = new DateTime($this->dDataHora);
		return $oData->format("d/m/Y H:i:s");
	}
	public function setDataHoraBanco($dDataHora){
		if($dDataHora){
			$oData = DateTime::createFromFormat('d/m/Y H:i:s', $dDataHora);
			$this->dDataHora = $oData->format('Y-m-d H:i:s') ;
		}
	}

	public function setCliente($oCliente){
		$this->oCliente = $oCliente;
	}
	public function getCliente(){
		$oFachada = new FachadaPrincipalBD();
		$this->oCliente = $oFachada->recuperarUmCliente($this->getCodCliente());
		return $this->oCliente;
	}

	public function setTipoEmail($oTipoEmail){
		$this->oTipoEmail = $oTipoEmail;
	}
	public function getTipoEmail(){
		$oFachada = new FachadaPrincipalBD();
		$this->oTipoEmail = $oFachada->recuperarUmEmailTipo($this->getCodTipoEmail());
		return $this->oTipoEmail;
	}

 }
 ?>
