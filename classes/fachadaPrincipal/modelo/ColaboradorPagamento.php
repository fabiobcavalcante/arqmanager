<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela colaborador_pagamento
  */
 class ColaboradorPagamento{
 	/**
	* @campo cod_colaborador_pagamento
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodColaboradorPagamento;
 	/**
	* @campo cod_producao
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodProducao;
	/**
	* @campo valor
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodColaborador;
	/**
	* @campo data_pagamento
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodProjeto;


	private $oProducao;



 	public function __construct(){

 	}

 	public function setCodColaboradorPagamento($nCodColaboradorPagamento){
		$this->nCodColaboradorPagamento = $nCodColaboradorPagamento;
	}
	public function getCodColaboradorPagamento(){
		return $this->nCodColaboradorPagamento;
	}
 	public function setCodProducao($nCodProducao){
		$this->nCodProducao = $nCodProducao;
	}
	public function getCodProducao(){
		return $this->nCodProducao;
	}

	public function setValor($nValor){
		$this->nValor = $nValor;
	}
	public function getValor(){
		return $this->nValor;
	}
	public function getValorFormatado(){
		$vRetorno = number_format($this->nValor , 2, ',', '.');
		return $vRetorno;
	}
	public function setValorBanco($nValor){
		if($nValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValor = str_replace($sOrigem, $sDestino, $nValor);

		}else{
			$this->nValor = 'null';
		}
	}

	public function setDataPagamento($dDataPagamento){
		$this->dDataPagamento = $dDataPagamento;
	}
	public function getDataPagamento(){
		return $this->dDataPagamento;
	}
	public function getDataPagamentoFormatado(){
		if($this->dDataPagamento){
			$oData = new DateTime($this->dDataPagamento);
			return $oData->format("d/m/Y");
		}
	}
	public function setDataPagamentoBanco($dDataPagamento){
		if($dDataPagamento){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataPagamento);
			$this->dDataPagamento = $oData->format('Y-m-d') ;
		}
	}

	public function setProducao($oProducao){
		$this->oProducao = $oProducao;
	}
	public function getColaborador(){
		$oFachada = new FachadaPrincipalBD();
		$this->oProducao = $oFachada->recuperarUmColaboradorProducao($this->getCodProducao());
		return $this->oProducao;
	}


 }
 ?>
