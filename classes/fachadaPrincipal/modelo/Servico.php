<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela servico
 */
class Servico{
	/**
	 * @campo cod_servico
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodServico;
	/**
	 * @campo desc_servico
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sDescServico;
	/**
	 * @campo desc_documento
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sDescDocumento;
	/**
	 * @campo percentual_empresa
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nPercentualEmpresa;
	/**
	 * @campo inserido_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sInseridoPor;
	/**
	 * @campo alterado_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sAlteradoPor;
	/**
	 * @campo ativo
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nAtivo;
	/**
	 * @campo id_escritorio
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nIdEscritorio;
	private $oEscritorio;

	public function __construct(){

	}

	public function setCodServico($nCodServico){
		$this->nCodServico = $nCodServico;
	}
	public function getCodServico(){
		return $this->nCodServico;
	}
	public function setDescServico($sDescServico){
		$this->sDescServico = $sDescServico;
	}
	public function getDescServico(){
		return $this->sDescServico;
	}
	public function setDescDocumento($sDescDocumento){
		$this->sDescDocumento = $sDescDocumento;
	}
	public function getDescDocumento(){
		return $this->sDescDocumento;
	}
	public function setPercentualEmpresa($nPercentualEmpresa){
		$this->nPercentualEmpresa = $nPercentualEmpresa;
	}
	public function getPercentualEmpresa(){
		return $this->nPercentualEmpresa;
	}

	public function setPercentualEmpresaBanco($nPercentualEmpresa){
		if($nPercentualEmpresa){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nPercentualEmpresa = str_replace($sOrigem, $sDestino, $nPercentualEmpresa);

		}else{
			$this->nPercentualEmpresa = 0;
		}
	}
	public function getPercentualEmpresaFormatado(){
		$fmt = new NumberFormatter( 'pt_BR', NumberFormatter::DECIMAL );
		$fmt->format($this->nPercentualEmpresa);
		if(intl_is_failure($fmt->getErrorCode())) {
			return  report_error("Formatter error");
		}
		return $fmt->format($this->nPercentualEmpresa);
	}
	public function setInseridoPor($sInseridoPor){
		$this->sInseridoPor = $sInseridoPor;
	}
	public function getInseridoPor(){
		return $this->sInseridoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setIdEscritorio($nIdEscritorio){
		$this->nIdEscritorio = $nIdEscritorio;
	}
	public function getIdEscritorio(){
		return $this->nIdEscritorio;
	}
}