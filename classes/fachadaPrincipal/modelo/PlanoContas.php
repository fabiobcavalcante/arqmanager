<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela plano_contas
  */
 class PlanoContas{
 	/**
	* @campo cod_plano_contas
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodPlanoContas;
	/**
	* @campo codigo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCodigo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo visivel
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nVisivel;
	/**
	* @campo usu_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUsuInc;
	/**
	* @campo usu_alt
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUsuAlt;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
  /**
  * @campo id_escritorio
  * @var number
  * @primario false
  * @nulo false
  * @auto-increment false
  */
  private $nIdEscritorio;
  /**
  * @campo id_pai
  * @var number
  * @primario false
  * @nulo false
  * @auto-increment false
  */
  private $nIdPai;
  private $oPai;
  private $oEscritorio;

 	public function __construct(){

 	}

 	public function setCodPlanoContas($nCodPlanoContas){
		$this->nCodPlanoContas = $nCodPlanoContas;
	}
	public function getCodPlanoContas(){
		return $this->nCodPlanoContas;
	}
	public function setCodigo($sCodigo){
		$this->sCodigo = $sCodigo;
	}
	public function getCodigo(){
		return $this->sCodigo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setVisivel($nVisivel){
		$this->nVisivel = $nVisivel;
	}
	public function getVisivel(){
		return $this->nVisivel;
	}
	public function getVisivelFormatado(){
			if($this->nVisivel == 1)
				return "Sim";
			else {
				return "Não";
			}

	}
	public function setUsuInc($sUsuInc){
		$this->sUsuInc = $sUsuInc;
	}
	public function getUsuInc(){
		return $this->sUsuInc;
	}
	public function setUsuAlt($sUsuAlt){
		$this->sUsuAlt = $sUsuAlt;
	}
	public function getUsuAlt(){
		return $this->sUsuAlt;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

    public function setIdPai($nIdPai){
      $this->nIdPai = $nIdPai;
    }
    public function getIdPai(){
      return $this->nIdPai;
    }

    public function getPai(){
			$oFachada = new FachadaAdminBD();
			$this->oPai = $oFachada->recuperarUmPlanoContas($nIdPai);
			return $this->oPai;
    }
    public function setIdEscritorio($nIdEscritorio){
      $this->nIdEscritorio = $nIdEscritorio;
    }
    public function getIdEscritorio(){
      return $this->nIdEscritorio;
    }
    public function getEscritorio($nIdEscritorio){
      $oFachada = new FachadaAdminBD();
      $this->oEscritorio = $oFachada->recuperarUmEscritorio($nIdEscritorio);
      return $this->oEscritorio;
    }
 }
 ?>
