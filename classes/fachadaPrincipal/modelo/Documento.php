<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela documento
 */
class Documento{
	/**
	 * @campo cod_documento
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodDocumento;
	/**
	 * @campo titulo
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sTitulo;
	/**
	 * @campo conteudo
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sConteudo;
	/**
	 * @campo ativo
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sAtivo;

	public function setCodDocumento($nCodDocumento){
		$this->nCodDocumento = $nCodDocumento;
	}
	public function getCodDocumento(){
		return $this->nCodDocumento;
	}
	public function setTitulo($sTitulo){
		$this->sTitulo = $sTitulo;
	}
	public function getTitulo(){
		return $this->sTitulo;
	}
	public function setConteudo($sConteudo){
		$this->sConteudo = $sConteudo;
	}
	public function getConteudo(){
		return $this->sConteudo;
	}
	public function setAtivo($sAtivo){
		$this->sAtivo = $sAtivo;
	}
	public function getAtivo(){
		return $this->sAtivo;
	}
}