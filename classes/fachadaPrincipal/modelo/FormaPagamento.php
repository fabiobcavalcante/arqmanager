<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela forma_pagamento
 */
class FormaPagamento{
	/**
	 * @campo cod_forma_pagamento
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodFormaPagamento;
	/**
	 * @campo descricao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sDescricao;
	/**
	 * @campo incluido_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sIncluidoPor;
	/**
	 * @campo alterado_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sAlteradoPor;
	/**
	 * @campo ativo
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nAtivo;

	public function setCodFormaPagamento($nCodFormaPagamento){
		$this->nCodFormaPagamento = $nCodFormaPagamento;
	}
	public function getCodFormaPagamento(){
		return $this->nCodFormaPagamento;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setIncluidoPor($sIncluidoPor){
		$this->sIncluidoPor = $sIncluidoPor;
	}
	public function getIncluidoPor(){
		return $this->sIncluidoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

}