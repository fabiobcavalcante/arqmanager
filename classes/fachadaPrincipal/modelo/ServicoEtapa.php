<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela servico_etapa
 */
class ServicoEtapa{
    /**
     * @campo cod_servico_etapa
     * @var number
     * @primario true
     * @nulo false
     * @auto-increment true
     */
    private $nCodServicoEtapa;
    /**
     * @campo cod_servico
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodServico;
    /**
     * @campo descricao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sDescricao;
    /**
     * @campo descricao_contrato
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sDescricaoContrato;
    /**
     * @campo ordem
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nOrdem;
    /**
     * @campo cod_etapa
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodEtapa;
    private $oServico;
    private $oEtapa;

    public function setCodServicoEtapa($nCodServicoEtapa){
        $this->nCodServicoEtapa = $nCodServicoEtapa;
    }
    public function getCodServicoEtapa(){
        return $this->nCodServicoEtapa;
    }
    public function setCodServico($nCodServico){
        $this->nCodServico = $nCodServico;
    }
    public function getCodServico(){
        return $this->nCodServico;
    }
    public function setDescricao($sDescricao){
        $this->sDescricao = $sDescricao;
    }
    public function getDescricao(){
        return $this->sDescricao;
    }
    public function setDescricaoContrato($sDescricaoContrato){
        $this->sDescricaoContrato = $sDescricaoContrato;
    }
    public function getDescricaoContrato(){
        return $this->sDescricaoContrato;
    }
    public function setOrdem($nOrdem){
        $this->nOrdem = $nOrdem;
    }
    public function getOrdem(){
        return $this->nOrdem;
    }
    public function setCodEtapa($nCodEtapa){
        $this->nCodEtapa = $nCodEtapa;
    }
    public function getCodEtapa(){
        return $this->nCodEtapa;
    }
    public function setServico($oServico){
        $this->oServico = $oServico;
    }
    public function getServico(){
        $oFachada = new FachadaPrincipalBD();
        $this->oServico = $oFachada->recuperarUmServico($this->getCodServico());
        return $this->oServico;
    }
    public function setEtapa($oEtapa){
        $this->oEtapa = $oEtapa;
    }
    public function getEtapa(){
        $oFachada = new FachadaPrincipalBD();
        $this->oEtapa = $oFachada->recuperarUmEtapa($this->getCodEtapa());
        return $this->oEtapa;
    }

}
