<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela proposta_servico_etapa
 */
class PropostaServicoEtapa{
	/**
	 * @campo cod_etapa
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodEtapa;
	/**
	 * @campo cod_proposta
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodProposta;
	/**
	 * @campo cod_projeto
	 * @var number
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $nCodProjeto;
	/**
	 * @campo descricao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sDescricao;
	/**
	 * @campo prazo
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nPrazo;
	/**
	 * @campo data_prevista
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $dDataPrevista;
	/**
	 * @campo data_conclusao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $dDataConclusao;
	/**
	 * @campo observacao
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sObservacao;
	private $oProjeto;
	private $oServicoEtapa;
	private $oProposta;


	public function __construct(){

	}

	public function setCodEtapa($nCodEtapa){
		$this->nCodEtapa = $nCodEtapa;
	}
	public function getCodEtapa(){
		return $this->nCodEtapa;
	}
	public function setCodProposta($nCodProposta){
		$this->nCodProposta = $nCodProposta;
	}
	public function getCodProposta(){
		return $this->nCodProposta;
	}
	public function setCodProjeto($nCodProjeto){
		$this->nCodProjeto = $nCodProjeto;
	}
	public function getCodProjeto(){
		return $this->nCodProjeto;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setPrazo($nPrazo){
		$this->nPrazo = $nPrazo;
	}
	public function getPrazo(){
		return $this->nPrazo;
	}

	public function setObservacao($sObservacao){
		$this->sObservacao = $sObservacao;
	}
	public function getObservacao(){
		return $this->sObservacao;
	}

	public function setDataConclusao($dDataConclusao){
		$this->dDataConclusao = $dDataConclusao;
	}
	public function getDataConclusao(){
		return $this->dDataConclusao;
	}
	public function getDataConclusaoFormatado(){
		$oData = new DateTime($this->dDataConclusao);
		return $oData->format("d/m/Y");
	}
	public function setDataConclusaoBanco($dDataConclusao){
		if($dDataConclusao){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataConclusao);
			$this->dDataConclusao = $oData->format('Y-m-d') ;
		}else{
			$this->dDataConclusao = null;
		}
	}
	public function setDataPrevista($dDataPrevista){
		$this->dDataPrevista = $dDataPrevista;
	}
	public function getDataPrevista(){
		return $this->dDataPrevista;
	}
	public function getDataPrevistaFormatado(){
		$oData = new DateTime($this->dDataPrevista);
		return $oData->format("d/m/Y");
	}
	public function setDataPrevistaBanco($dDataPrevista){
		if($dDataPrevista){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataPrevista);
			$this->dDataPrevista = $oData->format('Y-m-d') ;
		}else{
			$this->dDataPrevista = NULL;
		}
	}

	public function setProjeto($oProjeto){
		$this->oProjeto = $oProjeto;
	}
	public function getProjeto(){
		$oFachada = new FachadaPrincipalBD();
		$this->oProjeto = $oFachada->recuperarUmProjeto($this->getCodProjeto());
		return $this->oProjeto;
	}
	public function setServicoEtapa($oServicoEtapa){
		$this->oServicoEtapa = $oServicoEtapa;
	}
	public function getServicoEtapa(){
		$oFachada = new FachadaPrincipalBD();
		$this->oServicoEtapa = $oFachada->recuperarUmServicoEtapa($this->getCodEtapa());
		return $this->oServicoEtapa;
	}
	public function setProposta($oProposta){
		$this->oProposta = $oProposta;
	}
	public function getProposta(){
		$oFachada = new FachadaPrincipalBD();
		$this->oProposta = $oFachada->recuperarUmProposta($this->getCodProposta());
		return $this->oProposta;
	}

}