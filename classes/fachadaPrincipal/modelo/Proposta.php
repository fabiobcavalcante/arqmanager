<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela proposta
 */
class Proposta{
    /**
     * @campo cod_proposta
     * @var number
     * @primario true
     * @nulo false
     * @auto-increment true
     */
    private $nCodProposta;
    /**
     * @campo cod_cliente
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodCliente;
    /**
     * @campo cod_servico
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodServico;
    /**
     * @campo cod_status
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodStatus;
    /**
     * @campo nome
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sNome;
    /**
     * @campo descricao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sDescricao;
    /**
     * @campo identificacao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sIdentificacao;
    /**
     * @campo valor_proposta
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */

    private $nValorProposta;
    /**
     * @campo data_proposta
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $dDataProposta;

    /**
     * @campo valor_avista
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nValorAvista;
    /**
     * @campo valor_parcela_aprazo
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sValorParcelaAprazo;
    /**
     * @campo visitas_incluidas
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nVisitasIncluidas;


    /**
     * @campo inserido_por
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sInseridoPor;
    /**
     * @campo alterado_por
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sAlteradoPor;
    /**
     * @campo id_escritorio
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nIdEscritorio;
    /**
     * @campo numero_proposta
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nNumeroProposta;
    /**
     * @campo ano_proposta
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nAnoProposta;
    /**
     * @campo entrega_parcial
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sEntregaParcial;
    /**
     * @campo forma_pagamento
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sFormaPagamento;
    /**
     * @campo prazo
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sPrazo;

    private $oEscritorio;
    private $oServico;
    private $oStatus;
    private $oCliente;


    public function __construct(){

    }

    public function setCodProposta($nCodProposta){
        $this->nCodProposta = $nCodProposta;
    }
    public function getCodProposta(){
        return $this->nCodProposta;
    }
    public function setCodCliente($nCodCliente){
        $this->nCodCliente = $nCodCliente;
    }
    public function getCodCliente(){
        return $this->nCodCliente;
    }
    public function setCodServico($nCodServico){
        $this->nCodServico = $nCodServico;
    }
    public function getCodServico(){
        return $this->nCodServico;
    }
    public function setCodStatus($nCodStatus){
        $this->nCodStatus = $nCodStatus;
    }
    public function getCodStatus(){
        return $this->nCodStatus;
    }
    public function setNome($sNome){
        $this->sNome = $sNome;
    }
    public function getNome(){
        return $this->sNome;
    }
    public function setDescricao($sDescricao){
        $this->sDescricao = $sDescricao;
    }
    public function getDescricao(){
        return $this->sDescricao;
    }
    public function setIdentificacao($sIdentificacao){
        $this->sIdentificacao = $sIdentificacao;
    }
    public function getIdentificacao(){
        return $this->sIdentificacao;
    }
    public function setValorProposta($nValorProposta){
        $this->nValorProposta = $nValorProposta;
    }
    public function getValorProposta(){
        return $this->nValorProposta;
    }
    public function getValorPropostaFormatado(){
        $vRetorno = number_format($this->nValorProposta , 2, ',', '.');
        return $vRetorno;
    }
    public function getValorPropostaFormatadoExtenso(){
        $vRetorno = number_format($this->nValorProposta , 2, ',', '.');
        return Extenso::valorPorExtenso($vRetorno);
    }
    public function setValorPropostaBanco($nValorProposta){
        if($nValorProposta){
            $sOrigem = array('.',',');
            $sDestino = array('','.');
            $this->nValorProposta = str_replace($sOrigem, $sDestino, $nValorProposta);

        }else{
            $this->nValorProposta = 'null';
        }
    }
    public function setValorAvista($nValorAvista){
        $this->nValorAvista = $nValorAvista;
    }
    public function getValorAvista(){
        return $this->nValorAvista;
    }
    public function getValorAvistaFormatado(){
        $vRetorno = number_format($this->nValorAvista , 2, ',', '.');
        return $vRetorno;
    }
    public function setValorAvistaBanco($nValorAvista){
        if($nValorAvista){
            $sOrigem = array('.',',');
            $sDestino = array('','.');
            $this->nValorAvista = str_replace($sOrigem, $sDestino, $nValorAvista);

        }else{
            $this->nValorAvista = 'null';
        }
    }
    public function setValorParcelaAprazo($sValorParcelaAprazo){
        $this->sValorParcelaAprazo = $sValorParcelaAprazo;
    }
    public function getValorParcelaAprazo(){
        return $this->sValorParcelaAprazo;
    }
    public function setFormaPagamento($sFormaPagamento){
        $this->sFormaPagamento = $sFormaPagamento;
    }
    public function getFormaPagamento(){
        return $this->sFormaPagamento;
    }
    public function setInseridoPor($sInseridoPor){
        $this->sInseridoPor = $sInseridoPor;
    }
    public function getInseridoPor(){
        return $this->sInseridoPor;
    }
    public function setAlteradoPor($sAlteradoPor){
        $this->sAlteradoPor = $sAlteradoPor;
    }
    public function getAlteradoPor(){
        return $this->sAlteradoPor;
    }

    public function setNumeroProposta($nNumeroProposta){
        $this->nNumeroProposta = $nNumeroProposta;
    }
    public function getNumeroProposta(){
        return $this->nNumeroProposta;
    }
    public function setAnoProposta($nAnoProposta){
        $this->nAnoProposta = $nAnoProposta;
    }
    public function getAnoProposta(){
        return $this->nAnoProposta;
    }

    public function getNumeroPropostaFormatado(){
        return $this->nAnoProposta ."/". str_pad($this->nNumeroProposta , 4 , '0' , STR_PAD_LEFT);
    }

    public function setDataProposta($dDataProposta){
        $this->dDataProposta = $dDataProposta;
    }
    public function getDataProposta(){
        return $this->dDataProposta;
    }
    public function getDataPropostaFormatado(){
        $oData = new DateTime($this->dDataProposta);
        return $oData->format("d/m/Y");
    }
    public function setDataPropostaBanco($dDataProposta){
        if($dDataProposta){
            $oData = DateTime::createFromFormat('d/m/Y', $dDataProposta);
            $this->dDataProposta = $oData->format('Y-m-d') ;
        }
    }
    public function setVisitasIncluidas($nVisitasIncluidas){
        $this->nVisitasIncluidas = $nVisitasIncluidas;
    }
    public function getVisitasIncluidas(){
        return $this->nVisitasIncluidas;
    }
    
    public function setEntregaParcial($sEntregaParcial){
        $this->sEntregaParcial = $sEntregaParcial;
    }
    public function getEntregaParcial(){
        return $this->sEntregaParcial;
    }

    public function setServico($oServico){
        $this->oServico = $oServico;
    }
    public function getServico(){
        $oFachada = new FachadaPrincipalBD();
        $this->oServico = $oFachada->recuperarUmServico($this->getCodServico());
        return $this->oServico;
    }
    public function setStatus($oStatus){
        $this->oStatus = $oStatus;
    }
    public function getStatus(){
        $oFachada = new FachadaPrincipalBD();
        $this->oStatus = $oFachada->recuperarUmStatus($this->getCodStatus());
        return $this->oStatus;
    }
    public function setPrazo($sPrazo){
        $this->sPrazo = $sPrazo;
    }
    public function getPrazo(){
        return $this->sPrazo;
    }
    public function setCliente($oCliente){
        $this->oCliente = $oCliente;
    }
    public function getCliente(){
        $oFachada = new FachadaPrincipalBD();
        $this->oCliente = $oFachada->recuperarUmCliente($this->getCodCliente());
        return $this->oCliente;
    }
    public function setIdEscritorio($nIdEscritorio){
        $this->nIdEscritorio = $nIdEscritorio;
    }
    public function getIdEscritorio(){
        return $this->nIdEscritorio;
    }

}
