<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela projeto_servico_etapa_colaborador
 */
class ProjetoServicoEtapaColaborador{
	/**
	 * @campo cod_projeto_servico_etapa_colaborador
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment true
	 */
	private $nCodProjetoServicoEtapaColaborador;
	/**
	 * @campo cod_projeto
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodProjeto;
	/**
	 * @campo cod_colaborador
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodColaborador;
	/**
	 * @campo cod_etapa
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodEtapa;
	/**
	 * @campo percentual
	 * @var number
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $nPercentual;
	private $oColaborador;


	public function __construct(){

	}

	public function setCodProjetoServicoEtapaColaborador($nCodProjetoServicoEtapaColaborador){
		$this->nCodProjetoServicoEtapaColaborador = $nCodProjetoServicoEtapaColaborador;
	}
	public function getCodProjetoServicoEtapaColaborador(){
		return $this->nCodProjetoServicoEtapaColaborador;
	}
	public function setCodProjeto($nCodProjeto){
		$this->nCodProjeto = $nCodProjeto;
	}
	public function getCodProjeto(){
		return $this->nCodProjeto;
	}
	public function setCodColaborador($nCodColaborador){
		$this->nCodColaborador = $nCodColaborador;
	}
	public function getCodColaborador(){
		return $this->nCodColaborador;
	}
	public function setCodEtapa($nCodEtapa){
		$this->nCodEtapa = $nCodEtapa;
	}
	public function getCodEtapa(){
		return $this->nCodEtapa;
	}
	public function setPercentual($nPercentual){
		$this->nPercentual = $nPercentual;
	}
	public function getPercentual(){
		return $this->nPercentual;
	}
	public function getPercentualFormatado(){
		$vRetorno = number_format($this->nPercentual , 2, ',', '.');
		return $vRetorno;
	}
	public function setPercentualBanco($nPercentual){
		if($nPercentual){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nPercentual = str_replace($sOrigem, $sDestino, $nPercentual);

		}else{
			$this->nPercentual = 'null';
		}
	}
    public function setColaborador($oColaborador){
        $this->oColaborador = $oColaborador;
    }
    public function getColaborador(){
        $oFachada = new FachadaPrincipalBD();
        $this->oColaborador = $oFachada->recuperarUmColaborador($this->getCodColaborador());
        return $this->oColaborador;
    }

}
