<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela projeto_documento
 */
class ProjetoDocumento{
    /**
     * @campo cod_projeto_documento
     * @var number
     * @primario true
     * @nulo false
     * @auto-increment true
     */
    private $nCodProjetoDocumento;
    /**
     * @campo descricao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sDescricao;
    /**
     * @campo url
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sUrl;
    /**
     * @campo cod_projeto
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodProjeto;
    private $oProjeto;

    public function setCodProjetoDocumento($nCodProjetoDocumento){
        $this->nCodProjetoDocumento = $nCodProjetoDocumento;
    }
    public function getCodProjetoDocumento(){
        return $this->nCodProjetoDocumento;
    }
    public function setDescricao($sDescricao){
        $this->sDescricao = $sDescricao;
    }
    public function getDescricao(){
        return $this->sDescricao;
    }
    public function setUrl($sUrl){
        $this->sUrl = $sUrl;
    }
    public function getUrl(){
        return $this->sUrl;
    }
    public function setCodProjeto($nCodProjeto){
        $this->nCodProjeto = $nCodProjeto;
    }
    public function getCodProjeto(){
        return $this->nCodProjeto;
    }
    public function getProjeto($nCodProjeto){
        $oFachada = new FachadaPrincipalBD();
        $this->oProjeto = $oFachada->recuperarUmProjeto($nCodProjeto);
        return $this->oProjeto;
    }

}
