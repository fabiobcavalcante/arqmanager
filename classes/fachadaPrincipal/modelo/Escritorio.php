<?php
 /**
  * @author Auto-Generated
  * @package fachadaPrincipal
  * @SGBD mysql
  * @tabela escritorio
  */
 class Escritorio{
 	/**
	* @campo id_escritorio
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nIdEscritorio;
	/**
	* @campo nome_fantasia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNomeFantasia;
	/**
	* @campo razao_social
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sRazaoSocial;
	/**
	* @campo documento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDocumento;
	/**
	* @campo endereco_reduzido
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEnderecoReduzido;
	/**
	* @campo fone
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFone;
	/**
	* @campo endereco_completo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEnderecoCompleto;
	/**
	* @campo site
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sSite;
	/**
	* @campo email
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmail;
	/**
	* @campo instagram
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sInstagram;
	/**
	* @campo email_administrador
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmailAdministrador;
	/**
	* @campo logomarca
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sLogomarca;
	/**
	* @campo logomarca_mini
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sLogomarcaMini;
	/**
	* @campo cabecalho_email
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
		private $sCabecalhoEmail;

		/**
		* @campo rodape_email
		* @var String
		* @primario false
		* @nulo false
		* @auto-increment false
		*/
		private $sRodapeEmail;
	/**
	* @campo envio_email
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEnvioEmail;
	/**
	* @campo dias_uteis
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDiasUteis;
	/**
	* @campo hora_tecnica
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sHoraTecnica;
	/**
	* @campo cidade_uf
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCidadeUf;
	/**
	* @campo assinatura
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAssinatura;


 	public function __construct(){

 	}

 	public function setIdEscritorio($nIdEscritorio){
		$this->nIdEscritorio = $nIdEscritorio;
	}
	public function getIdEscritorio(){
		return $this->nIdEscritorio;
	}
	public function setNomeFantasia($sNomeFantasia){
		$this->sNomeFantasia = $sNomeFantasia;
	}
	public function getNomeFantasia(){
		return $this->sNomeFantasia;
	}
	public function setRazaoSocial($sRazaoSocial){
		$this->sRazaoSocial = $sRazaoSocial;
	}
	public function getRazaoSocial(){
		return $this->sRazaoSocial;
	}
	public function setDocumento($sDocumento){
		$this->sDocumento = $sDocumento;
	}
	public function getDocumento(){
		return $this->sDocumento;
	}
	public function setEnderecoReduzido($sEnderecoReduzido){
		$this->sEnderecoReduzido = $sEnderecoReduzido;
	}
	public function getEnderecoReduzido(){
		return $this->sEnderecoReduzido;
	}
	public function setFone($sFone){
		$this->sFone = $sFone;
	}
	public function getFone(){
		return $this->sFone;
	}
	public function setEnderecoCompleto($sEnderecoCompleto){
		$this->sEnderecoCompleto = $sEnderecoCompleto;
	}
	public function getEnderecoCompleto(){
		return $this->sEnderecoCompleto;
	}
	public function setSite($sSite){
		$this->sSite = $sSite;
	}
	public function getSite(){
		return $this->sSite;
	}
	public function setEmail($sEmail){
		$this->sEmail = $sEmail;
	}
	public function getEmail(){
		return $this->sEmail;
	}
	public function setEmailAdministrador($sEmailAdministrador){
		$this->sEmailAdministrador = $sEmailAdministrador;
	}
	public function getEmailAdministrador(){
		return $this->sEmailAdministrador;
	}
  public function setInstagram($sInstagram){
		$this->sInstagram = $sInstagram;
	}
	public function getInstagram(){
		return $this->sInstagram;
	}
	public function setLogomarca($sLogomarca){
		$this->sLogomarca = $sLogomarca;
	}
	public function getLogomarca(){
		return $this->sLogomarca;
	}
	public function setLogomarcaMini($sLogomarcaMini){
		$this->sLogomarcaMini = $sLogomarcaMini;
	}
	public function getLogomarcaMini(){
		return $this->sLogomarcaMini;
	}
	public function setCabecalhoEmail($sCabecalhoEmail){
		$this->sCabecalhoEmail = $sCabecalhoEmail;
	}
	public function getCabecalhoEmail(){
		return $this->sCabecalhoEmail;
	}

	public function setRodapeEmail($sRodapeEmail){
		$this->sRodapeEmail = $sRodapeEmail;
	}
	public function getRodapeEmail(){
		return $this->sRodapeEmail;
	}


	public function setDiasUteis($sDiasUteis){
		$this->sDiasUteis = $sDiasUteis;
	}
	public function getDiasUteis(){
		return $this->sDiasUteis;
	}
	public function setEnvioEmail($sEnvioEmail){
		$this->sEnvioEmail = $sEnvioEmail;
	}
	public function getEnvioEmail(){
		return $this->sEnvioEmail;
	}

	public function setHoraTecnica($sHoraTecnica){
		$this->sHoraTecnica = $sHoraTecnica;
	}
	public function getHoraTecnica(){
		return $this->sHoraTecnica;
	}
	public function setCidadeUf($sCidadeUf){
		$this->sCidadeUf = $sCidadeUf;
	}
	public function getCidadeUf(){
		return $this->sCidadeUf;
	}

  public function setAssinatura($sAssinatura){
		$this->sAssinatura = $sAssinatura;
	}
	public function getAssinatura(){
		return $this->sAssinatura;
	}
	 public function getAssinaturaFormatada(){
		 return "<img src='$this->sAssinatura' style='width: 200px' alt='Assinatura'>";
	 }

 }
 ?>
