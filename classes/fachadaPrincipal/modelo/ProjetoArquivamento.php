<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela projeto_arquivamento
 */
class ProjetoArquivamento{
    /**
     * @campo id_arquivamento
     * @var number
     * @primario true
     * @nulo false
     * @auto-increment true
     */
    private $nIdArquivamento;
    /**
     * @campo cod_projeto
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodProjeto;
    /**
     * @campo cod_status
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodStatus;
    /**
     * @campo cod_colaborador
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nCodColaborador;
    /**
     * @campo data_acao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $dDataAcao;
    /**
     * @campo motivo_acao
     * @var String
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $sMotivoAcao;
    /**
     * @campo flag_calendario
     * @var number
     * @primario false
     * @nulo false
     * @auto-increment false
     */
    private $nFlagCalendario;

    private $oStatus;
    private $oColaborador;
    private $oProjeto;

    public function setIdArquivamento($nIdArquivamento){
        $this->nIdArquivamento = $nIdArquivamento;
    }
    public function getIdArquivamento(){
        return $this->nIdArquivamento;
    }
    public function setCodColaborador($nCodColaborador){
        $this->nCodColaborador = $nCodColaborador;
    }
    public function getCodColaborador(){
        return $this->nCodColaborador;
    }
    public function setCodStatus($nCodStatus){
        $this->nCodStatus = $nCodStatus;
    }
    public function getCodStatus(){
        return $this->nCodStatus;
    }
    public function setCodProjeto($nCodProjeto){
        $this->nCodProjeto = $nCodProjeto;
    }
    public function getCodProjeto(){
        return $this->nCodProjeto;
    }

    public function setMotivoAcao($sMotivoAcao){
        $this->sMotivoAcao = $sMotivoAcao;
    }
    public function getMotivoAcao(){
        return $this->sMotivoAcao;
    }

    public function setDataAcao($dDataAcao){
        $this->dDataAcao = $dDataAcao;
    }
    public function getDataAcao(){
        return $this->dDataAcao;
    }
    public function getDataAcaoFormatado(){
        if($this->dDataAcao){
            try {
                $oData = new DateTime($this->dDataAcao);
                return $oData->format("d/m/Y");
            } catch (Exception $e) {
                return false;
            }
        }else {
            return false;
        }
    }
    public function setDataAcaoBanco($dDataAcao){
        if($dDataAcao){
            $oData = DateTime::createFromFormat('d/m/Y', $dDataAcao);
            $this->dDataAcao = $oData->format('Y-m-d') ;
        }
    }
    public function setFlagCalendario($nFlagCalendario){
        $this->nFlagCalendario = $nFlagCalendario;
    }
    public function getFlagCalendario(){
        return $this->nFlagCalendario;
    }

    public function getProjeto($nCodProjeto){
        $oFachada = new FachadaPrincipalBD();
        $this->oProjeto = $oFachada->recuperarUmProjeto($nCodProjeto);
        return $this->oProjeto;
    }
    public function getStatus($nCodStatus){
        $oFachada = new FachadaPrincipalBD();
        $this->oStatus = $oFachada->recuperarUmStatus($nCodStatus);
        return $this->oStatus;
    }
    public function getColaborador($nCodColaborador){
        $oFachada = new FachadaPrincipalBD();
        $this->oColaborador = $oFachada->recuperarUmStatus($nCodColaborador);
        return $this->oColaborador;
    }

}