<?php
/**
 * @author Auto-Generated
 * @package fachadaPrincipal
 * @SGBD mysql
 * @tabela proposta_microservico
 */
class PropostaMicroServico{
	/**
	 * @campo cod_microservico
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodMicroservico;
	/**
	 * @campo cod_proposta
	 * @var number
	 * @primario true
	 * @nulo false
	 * @auto-increment false
	 */
	private $nCodProposta;
	/**
	 * @campo cod_projeto
	 * @var number
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $nCodProjeto;

	/**
	 * @campo quantidade
	 * @var number
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $nQuantidade;
	/**
	 * @campo descricao_quantidade
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $sDescricaoQuantidade;
	/**
	 * @campo dias
	 * @var number
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $nDias;

	/**
	 * @campo data_previsao_inicio
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $dDataPrevisaoInicio;
	/**
	 * @campo data_efetivacao_inicio
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $dDataEfetivacaoInicio;
	/**
	 * @campo data_previsao_fim
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $dDataPrevisaoFim;
	/**
	 * @campo data_efetivacao_fim
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $dDataEfetivacaoFim;
	/**
	 * @campo observacao
	 * @var String
	 * @primario false
	 * @nulo true
	 * @auto-increment false
	 */
	private $sObservacao;

	/**
	 * @campo inserido_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sInseridoPor;
	/**
	 * @campo alterado_por
	 * @var String
	 * @primario false
	 * @nulo false
	 * @auto-increment false
	 */
	private $sAlteradoPor;
	private $oProjeto;
	private $oServicoMicroservico;

	public function setCodMicroservico($nCodMicroservico){
		$this->nCodMicroservico = $nCodMicroservico;
	}
	public function getCodMicroservico(){
		return $this->nCodMicroservico;
	}
	public function setCodProposta($nCodProposta){
		$this->nCodProposta = $nCodProposta;
	}
	public function getCodProposta(){
		return $this->nCodProposta;
	}
	public function setCodProjeto($nCodProjeto){
		$this->nCodProjeto = $nCodProjeto;
	}
	public function getCodProjeto(){
		return $this->nCodProjeto;
	}
	public function setQuantidade($nQuantidade){
		$this->nQuantidade = $nQuantidade;
	}
	public function getQuantidade(){
		return $this->nQuantidade;
	}
	public function setDescricaoQuantidade($sDescricaoQuantidade){
		$this->sDescricaoQuantidade = $sDescricaoQuantidade;
	}
	public function getDescricaoQuantidade(){
		return $this->sDescricaoQuantidade;
	}
	public function setDias($nDias){
		$this->nDias = $nDias;
	}
	public function getDias(){
		return $this->nDias;
	}
	public function setDataPrevisaoInicio($dDataPrevisaoInicio){
		$this->dDataPrevisaoInicio = $dDataPrevisaoInicio;
	}
	public function getDataPrevisaoInicio(){
		return $this->dDataPrevisaoInicio;
	}
	public function getDataPrevisaoInicioFormatado(){
		$oData = new DateTime($this->dDataPrevisaoInicio);
		return $oData->format("d/m/Y");
	}
	public function setDataPrevisaoInicioBanco($dDataPrevisaoInicio){
		if($dDataPrevisaoInicio){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataPrevisaoInicio);
			$this->dDataPrevisaoInicio = $oData->format('Y-m-d') ;
		}else{
			$this->dDataPrevisaoInicio = NULL;
		}
	}
	public function setDataEfetivacaoInicio($dDataEfetivacaoInicio){
		$this->dDataEfetivacaoInicio = $dDataEfetivacaoInicio;
	}
	public function getDataEfetivacaoInicio(){
		return $this->dDataEfetivacaoInicio;
	}
	public function getDataEfetivacaoInicioFormatado(){
		$oData = new DateTime($this->dDataEfetivacaoInicio);
		return $oData->format("d/m/Y");
	}
	public function setDataEfetivacaoInicioBanco($dDataEfetivacaoInicio){
		if($dDataEfetivacaoInicio){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataEfetivacaoInicio);
			$this->dDataEfetivacaoInicio = $oData->format('Y-m-d') ;
		}else{
			$this->dDataEfetivacaoInicio = NULL;
		}
	}
	public function setDataPrevisaoFim($dDataPrevisaoFim){
		$this->dDataPrevisaoFim = $dDataPrevisaoFim;
	}
	public function getDataPrevisaoFim(){
		return $this->dDataPrevisaoFim;
	}
	public function getDataPrevisaoFimFormatado(){
		$oData = new DateTime($this->dDataPrevisaoFim);
		return $oData->format("d/m/Y");
	}
	public function setDataPrevisaoFimBanco($dDataPrevisaoFim){
		if($dDataPrevisaoFim){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataPrevisaoFim);
			$this->dDataPrevisaoFim = $oData->format('Y-m-d') ;
		}else{
			$this->dDataPrevisaoFim = NULL;
		}
	}
	public function setDataEfetivacaoFim($dDataEfetivacaoFim){
		$this->dDataEfetivacaoFim = $dDataEfetivacaoFim;
	}
	public function getDataEfetivacaoFim(){
		return $this->dDataEfetivacaoFim;
	}
	public function getDataEfetivacaoFimFormatado(){
		$oData = new DateTime($this->dDataEfetivacaoFim);
		return $oData->format("d/m/Y");
	}
	public function setDataEfetivacaoFimBanco($dDataEfetivacaoFim){
		if($dDataEfetivacaoFim){
			$oData = DateTime::createFromFormat('d/m/Y', $dDataEfetivacaoFim);
			$this->dDataEfetivacaoFim = $oData->format('Y-m-d') ;
		}else{
			$this->dDataEfetivacaoFim = NULL;
		}
	}
	public function setObservacao($sObservacao){
		$this->sObservacao = $sObservacao;
	}
	public function getObservacao(){
		return $this->sObservacao;
	}
	public function setCor($sCor){
		$this->sCor = $sCor;
	}
	public function getCor(){
		return $this->sCor;
	}

	public function setInseridoPor($sInseridoPor){
		$this->sInseridoPor = $sInseridoPor;
	}
	public function getInseridoPor(){
		return $this->sInseridoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setProjeto($oProjeto){
		$this->oProjeto = $oProjeto;
	}
	public function getProjeto(){
		$oFachada = new FachadaPrincipalBD();
		$this->oProjeto = $oFachada->recuperarUmProjeto($this->getCodProjeto());
		return $this->oProjeto;
	}
	public function setServicoMicroservico($oServicoMicroservico){
		$this->oServicoMicroservico = $oServicoMicroservico;
	}
	public function getServicoMicroservico(){
		$oFachada = new FachadaPrincipalBD();
		$this->oServicoMicroservico = $oFachada->recuperarUmServicoMicroservico($this->getCodMicroservico());
		return $this->oServicoMicroservico;
	}
}
