<?php
class Persistencia implements IPersistencia{

    private $oInterpretador;
    private $sSGBD;
    private $sBanco;
    private $sServidor;
    private $sUsuario;
    private $sSenha;


    public function __construct($oObjeto=""){
        if($oObjeto!=""){
            $this->oInterpretador = new Interpretador($oObjeto);
            $this->sSGBD = $this->oInterpretador->recuperarSGBD();
        }else{
            $this->sSGBD = "mysql";
        }

        $sUrl = $_SERVER['HTTP_HOST'];

        if ($sUrl == 'sistema.arqmanager.com.br'){

            error_reporting(0);
            // servidor hostgator
            $this->sBanco    = "luanap45_dbarqmanager2";
            $this->sServidor = "50.116.87.225";
            $this->sUsuario = "luanap45_uarqman";
            $this->sSenha = "@rquitetoGerente10.000";

            // $this->sBanco    = "luanap45_sistema2";
            // $this->sServidor = "50.116.87.225";
            // $this->sUsuario = "luanap45_user";
            // $this->sSenha = "Lu@n4CPC0812";

        } else{
            // local
            $this->sServidor = "localhost";
            $this->sBanco    = "dbarqmanager";
            $this->sUsuario = "root";
            $this->sSenha = "";
        }

    }

    public function inserir(){

        $sTabela = $this->oInterpretador->recuperarTabela();
        $sCampos = $this->oInterpretador->recuperarCamposInserir();
        $sValores = $this->oInterpretador->recuperarCamposInserirValores();

        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $nId = $oBancoDeDados->insereRegistroNoBanco($sTabela,$sCampos,$sValores);

        if($nId){
            $this->logPersistencia($this->oInterpretador->getObjeto(), "naoexiste",$this->oInterpretador->recuperarTabela(), "Inserir");
        }
        return $nId;
    }

    public function inserirLog($sDescricao,$sSql,$sTabelaPrincipal,$sOperacao){
        $sTabela = "acesso_log";
        $sCampos = "usuario,tabela,operacao,data_log,descricao,sql_descricao,ativo";
        if(isset($_SESSION['oUsuarioAM']))
            $sValores = $_SESSION['oUsuarioAM']->getCodColaborador().",'". $sTabelaPrincipal . "','". $sOperacao . "','"     .date('Y-m-d H:i:s')."','" . addslashes($sDescricao)."','".addslashes($sSql)."',1";
        else
            $sValores = "''".",'". $sTabelaPrincipal . "','". $sOperacao . "','"     .date('Y-m-d H:i:s')."','" . addslashes($sDescricao)."','".addslashes($sSql)."',1";

        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $nId = $oBancoDeDados->insereRegistroNoBanco($sTabela,$sCampos,$sValores);

        //if($nId)
        //	$this->logPersistencia($this->oInterpretador->getObjeto(), "naoexiste",$sTabela, "Inserir");

        return $nId;
    }

    public function alterar(){
        $sTabela = $this->oInterpretador->recuperarTabela();
        $sCampos = $this->oInterpretador->recuperarCamposAlterar();
        $sComplemento = $this->oInterpretador->recuperarWhereCamposPrimario();
        $oObjetoAntigo = $this->recuperarObjetoAntesAlteracao();

        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($bResultado = $oBancoDeDados->alteraRegistrosDoBanco($sTabela,$sCampos,$sComplemento)){
            $this->logPersistencia($this->oInterpretador->getObjeto(), $oObjetoAntigo, $sTabela,"Alterar");
            return true;
        }
        return false;
    }

    public function alterarSemChavePrimaria($sComplemento){

        $sTabela = $this->oInterpretador->recuperarTabela();
        $sCampos = $this->oInterpretador->recuperarCamposAlterar();
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($oBancoDeDados->alteraRegistrosDoBanco($sTabela,$sCampos,$sComplemento)){
            return true;
        }
        return false;
    }


    public function AlteraSomenteUmaColuna($sTabela,$sCampos,$sComplemento){
        //die("UPDATE $sTabela SET $sCampos $sComplemento");
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($bResultado = $oBancoDeDados->alteraRegistrosDoBanco($sTabela,$sCampos,$sComplemento)){
            // $this->logPersistencia($this->oInterpretador->getObjeto(), $oObjetoAntigo, $sTabela,"Alterar");
            return true;
        }
        return false;
    }
    public function executar($sTabela,$sCampos,$sComplemento){
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($bResultado = $oBancoDeDados->alteraRegistrosDoBanco($sTabela,$sCampos,$sComplemento)){
            $this->logPersistencia($this->oInterpretador->getObjeto(), $oObjetoAntigo, "Alterar");
            return true;
        }
        return false;
    }

    public function presente(){
        $sTabelas = $this->oInterpretador->recuperarTabela();
        $sCampos = "*";
        $sComplemento = $this->oInterpretador->recuperarWhereCamposPrimario();
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $vObjeto = $oBancoDeDados->recuperaRegistrosDoBanco($sCampos,$sTabelas,$sComplemento);
        if(is_array($vObjeto))
            return true;
        return false;
    }

    public function consultar($sTabelas, $sCampos, $sComplemento){
        $voObjeto = array();
        //echo "<br>SELECT $sCampos FROM $sTabelas $sComplemento;";
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $rsBanco = $oBancoDeDados->recuperaRegistrosDoBanco($sCampos,$sTabelas,$sComplemento);
        $voObjeto = $this->oInterpretador->recuperarVetorObjetos($rsBanco);

        return $voObjeto;
    }

    public function consultarLista($sTabelas, $sCampos, $sComplemento){
        $vaResultado = array();
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $vaResultado = $oBancoDeDados->recuperaRegistrosDoBanco($sCampos,$sTabelas,$sComplemento);
        return $vaResultado;
    }

    public function consultarProcedure($sTabelas, $sComplemento){
        $voObjeto = array();
        //die("call $sTabelas ($sComplemento)");
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $rsBanco = $oBancoDeDados->recuperaRegistrosProcedureDoBanco($sTabelas,$sComplemento);

        $voObjeto = $this->oInterpretador->recuperarVetorObjetos($rsBanco);
        return $voObjeto;
    }

    public function consultarProcedureLista($sTabelas, $sComplemento){
        $vaResultado = array();
        // echo "CALL " . $sTabelas ."(" . $sComplemento .")";
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $vaResultado = $oBancoDeDados->recuperaRegistrosProcedureDoBanco($sTabelas,$sComplemento);

        return $vaResultado;
    }


    public function atualizarViaProcedure($sTabelas, $sComplemento){
        $voObjeto = array();
        // die("Exec $sTabelas $sComplemento");
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        return $oBancoDeDados->alteraRegistrosDoBancoProcedure($sTabelas,$sComplemento);
    }

    public function consultarTabelaTemporaria($sTabelas, $sCampos, $sComplemento){
        $voObjeto = array();
        //die("SELECT $sCampos FROM $sTabelas $sComplemento");
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $rsBanco = $oBancoDeDados->recuperaRegistrosTabelaTemporariaDoBanco($sCampos,$sTabelas,$sComplemento);
        $voObjeto = $this->oInterpretador->recuperarVetorObjetos($rsBanco);
        return $voObjeto;
    }

    public function consultarFuncao($sNomeFuncao,$sCampos, $sComplemento){
        $voObjeto = array();
        // die("SELECT $sCampos FROM $sNomeFuncao $sComplemento");
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        $rsBanco = $oBancoDeDados->recuperaRegistrosFuncaoDoBanco($sNomeFuncao,$sCampos,$sComplemento);
        $voObjeto = $this->oInterpretador->recuperarVetorObjetos($rsBanco);
        return $voObjeto;
    }

    public function excluir(){
        $sTabela = $this->oInterpretador->recuperarTabela();
        $sComplemento = $this->oInterpretador->recuperarWhereCamposPrimario();
        $oObjetoAntigo = $this->recuperarObjetoAntesAlteracao();
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($bResultado = $oBancoDeDados->excluiRegistrosDoBanco($sTabela,$sComplemento)){
            $this->logPersistencia($this->oInterpretador->getObjeto(), $oObjetoAntigo,$this->oInterpretador->recuperarTabela(), "Excluir");
            return true;
        }
        return false;
    }

    public function excluirSemChavePrimaria($sComplemento){
        $sTabela = $this->oInterpretador->recuperarTabela();
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($bResultado = $oBancoDeDados->excluiRegistrosDoBanco($sTabela,$sComplemento)){
            $this->logPersistencia($this->oInterpretador->getObjeto(), $oObjetoAntigo,$this->oInterpretador->recuperarTabela(), "Excluir");
            return true;
        }
        return false;
    }

    public function excluirFisicamente(){
        $sTabela = $this->oInterpretador->recuperarTabela();
        $sComplemento = $this->oInterpretador->recuperarWhereCamposPrimario();
        $oObjetoAntigo = $this->recuperarObjetoAntesAlteracao();

        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($bResultado = $oBancoDeDados->excluiFisicamenteRegistrosDoBanco($sTabela,$sComplemento)){
            $this->logPersistencia($this->oInterpretador->getObjeto(), $oObjetoAntigo,$sTabela, "ExcluirFisicamente");
            return true;
        }
        return false;
    }


    public function excluirFisicamenteProcedure($sProcedure,$sParametros){
        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($bResultado = $oBancoDeDados->excluiFisicamenteRegistrosDoBancoProcedure($sProcedure,$sParametros)){
            return $bResultado;
        }
        return false;
    }

    public function excluirFisicamenteSemChavePrimaria($sComplemento){
        $sTabela = $this->oInterpretador->recuperarTabela();

        $oBancoDeDados = new BancoDeDados($this->sSGBD,$this->sBanco,$this->sServidor,$this->sUsuario,$this->sSenha);
        if($bResultado = $oBancoDeDados->excluiFisicamenteRegistrosDoBanco($sTabela,$sComplemento)){
            $this->logPersistencia($this->oInterpretador->getObjeto(), null,$sTabela, "Excluir");
            return true;
        }
        return false;
    }

    public function logPersistencia($oObjPersistido, $oObjetoAntigo,$sTabela, $sOP){

        $vMaiuscula = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        $vMinuscula_ = array('_a','_b','_c','_d','_e','_f','_g','_h','_i','_j','_k','_l','_m','_n','_o','_p','_q','_r','_d','_t','_u','_v','_w','_x','_y','_z');

        $sAtributosModificados = $sNomeCampo = $sValores = '';

        $sTabela = $this->oInterpretador->getDescritor()->getClasse();


        switch($sOP){
            case "Inserir":
                $sLogPersistencia = "Inserir ".$this->oInterpretador->getDescritor()->getClasse()."\r\n";

                foreach($this->oInterpretador->getDescritor()->getAtributoClasse() as $nIndice => $oAtributoClasse){
                    $voCampoTabela = $this->oInterpretador->getDescritor()->getCampoTabela();
                    $sNomeMetodo = "get".substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1);

                    $sAtributosModificados .= $oAtributoClasse->getNome().": ".$oObjPersistido->$sNomeMetodo()."; ";
                    $sNomeCampo .= substr($oAtributoClasse->getNome(), 1). ",";


                    $sValores .= $oObjPersistido->$sNomeMetodo() . ",";
                }
                $sValores = str_replace("'","",$sValores);
                $sNomeCampo = substr(str_replace(",_", ",",str_replace($vMaiuscula, $vMinuscula_, $sNomeCampo)), 1);
                $sNomeCampo = substr($sNomeCampo,0,-1);
                $sValores = substr($sValores,0,-1);

                $sLogPersistencia .= "[Campos Inseridos] ".$sAtributosModificados . "<br>";
                $sSql = "INSERT INTO ". $sTabela ."(".$sNomeCampo.") VALUES (".$sValores.")";

                $this->inserirLog($sLogPersistencia,$sSql,$sTabela,$sOP);

                break;
            case "Alterar":
                $sLogPersistencia = "Alterar ".$sTabela." Id (";
                $sVirgula = $sWHERE = $sSET = "";

                foreach($this->oInterpretador->getDescritor()->getAtributoClasse() as $nIndice => $oAtributoClasse){
                    $voCampoTabela = $this->oInterpretador->getDescritor()->getCampoTabela();

                    $sNomeMetodo = "get".substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1);
                    $sNomeCampo = $voCampoTabela[$nIndice]->getNome();

                    if($voCampoTabela[$nIndice]->getCampoPrimario()){
                        $sLogPersistencia .= $sVirgula.$oObjPersistido->$sNomeMetodo();
                        $sVirgula = ",";
                        if($voCampoTabela[$nIndice]->getTipo() == 'number'){
                            $sValor = $oObjPersistido->$sNomeMetodo();
                        }else{
                            //$sValor = "'". $oObjPersistido->$sNomeMetodo() ."'";
                            $sNomeMetodo = str_replace("'","",$oObjPersistido->$sNomeMetodo());
                            $sValor = "'". $sNomeMetodo ."'";

                        }
                        $sWHERE .= $sNomeCampo . " = " . $sValor . " AND ";
                    } elseif($oObjPersistido->$sNomeMetodo() != $oObjetoAntigo->$sNomeMetodo()){
                        $sAtributosModificados .= $sNomeCampo.": De ".$oObjetoAntigo->$sNomeMetodo()." para ".$oObjPersistido->$sNomeMetodo()."; ";
                        $sNomeMetodo = str_replace("'","",$oObjPersistido->$sNomeMetodo());
                        // $sAtributosModificados .= $sNomeCampo.": De ".$sNomeMetodo." para ".$sNomeMetodo."; ";
                        //$sSET .= $sNomeCampo ."= ||" . $oObjPersistido->$sNomeMetodo() ."|| ,";
                        $sSET .= $sNomeCampo ."= ||" . $sNomeMetodo ."|| ,";
                    }
                }

                $sSET = substr($sSET,0,-1);
                $sWHERE = substr($sWHERE, 0, -5);
                $sLogPersistencia .= ")\r\n";
                $sLogPersistencia .= "[Campos Alterados] ".$sAtributosModificados;

                $sSql = "UPDATE ". $this->oInterpretador->getDescritor()->getClasse() . " SET ".$sSET. " WHERE ".$sWHERE;
                /*
                echo "LOG Persistencia". $sLogPersistencia;
                echo "<br>SQL:" . $sSql;
                echo "<br> Tabela: " .$sTabela;
                echo "<br>Operacao:" .$sOP;
                die();*/

                $this->inserirLog($sLogPersistencia,$sSql,$sTabela,$sOP);

                break;
            case "Excluir":
                $sLogPersistencia = "Excluir ".$sTabela." Id (";
                $valores = $sWHERE = '';

                foreach($this->oInterpretador->getDescritor()->getAtributoClasse() as $nIndice => $oAtributoClasse){
                    $voCampoTabela = $this->oInterpretador->getDescritor()->getCampoTabela();
                    $sNomeMetodo = "get".substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1);

                    $sNomeCampo = $voCampoTabela[$nIndice]->getNome();

                    $valores .= $sNomeCampo . " =, " . substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1) . ", ";

                    if($voCampoTabela[$nIndice]->getCampoPrimario()){
                        $sVirgula = ",";
                        if($voCampoTabela[$nIndice]->getTipo() == 'number'){
                            $sValor = $oObjPersistido->$sNomeMetodo();
                        }else{
                            $sValor = "'". $oObjPersistido->$sNomeMetodo() ."'";
                        }
                        $sWHERE .= $sNomeCampo . "= ". $sValor . " AND ";

                    }
                    //$sLogPersistencia .= $sNomeCampo."= ".$oObjetoAntigo->$sNomeMetodo()."; ";

                }

                $sWHERE = substr($sWHERE, 0, -5);
                $sLogPersistencia = "[Campos Alterados] ativo: De 1 para 0;";
                $sSql = "UPDATE  ". $sTabela ." SET ativo = 0 WHERE " . $sWHERE;

                $this->inserirLog($sLogPersistencia,$sSql,$sTabela,$sOP);
                //LogPersistencia::escreverPersistencia($sLogPersistencia);

                break;
            case "ExcluirFisicamente":

                $sCampos = $sValues = $sVirgula = $sWHERE = "";

                $sLogPersistencia = "ExcluirFisicamente ".$sTabela." Id (";

                foreach($this->oInterpretador->getDescritor()->getAtributoClasse() as $nIndice => $oAtributoClasse){

                    $voCampoTabela = $this->oInterpretador->getDescritor()->getCampoTabela();

                    $sNomeMetodo = "get".substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1);
                    $sNomeCampo = $voCampoTabela[$nIndice]->getNome();

                    //usado para recuperar o registro exlcuido...
                    $sCampos .= $sNomeCampo .",";
                    $sValues .= ( (is_numeric($oObjetoAntigo->$sNomeMetodo())) ?  $oObjetoAntigo->$sNomeMetodo() : (($oObjetoAntigo) ? '"'.$oObjetoAntigo->$sNomeMetodo().'"' : null)) .", ";

                    if($voCampoTabela[$nIndice]->getCampoPrimario()){
                        $sLogPersistencia .= $sVirgula.$oObjPersistido->$sNomeMetodo();
                        $sVirgula = ",";
                        if($voCampoTabela[$nIndice]->getTipo() == 'number'){
                            $sValor = $oObjPersistido->$sNomeMetodo();
                        }else{
                            $sValor = "||". $oObjPersistido->$sNomeMetodo() ."||";
                        }
                        $sWHERE .= $sNomeCampo . " = " . $sValor . " AND ";
                    }


                }

                $sWHERE = substr($sWHERE, 0, -5);
                $sLogPersistencia .= ")\r\n";

                //usado para recuperar o registro excluido...
                $sCampos = substr($sCampos, 0, -1);
                $sValues = substr($sValues, 0, -2);
                $sInsert = "Insert Into " . $sTabela ." (" .$sCampos.") Values (" .$sValues.")";

                $sSql = "DELETE FROM  ". $this->oInterpretador->getDescritor()->getClasse() . " WHERE ".$sWHERE . " || Obj Excluido: " . $sInsert ;

                $this->inserirLog($sLogPersistencia,$sSql,$sTabela,$sOP);
                //die();
                //LogPersistencia::escreverPersistencia($sLogPersistencia);
                break;
            case 'ExcluirSemChavePrimaria':
                $sLogPersistencia = "ExcluirSemChavePrimaria ".$sTabela." Id (";
                $sWHERE = $oObjetoAntigo;
                $sLogPersistencia = "[Campos Alterados] ativo: De 1 para 0;";
                $sSql = "UPDATE  ". $sTabela ." SET ativo = 0 " . $sWHERE;
                $this->inserirLog($sLogPersistencia,$sSql,$sTabela,$sOP);
                //LogPersistencia::escreverPersistencia($sLogPersistencia);
                break;
        }
    }

    public function recuperarObjetoAntesAlteracao(){
        $sTabelas = $this->oInterpretador->recuperarTabela();
        $sCampos = "*";
        $sComplemento = $this->oInterpretador->recuperarWhereCamposPrimario();

        $voObjetoAntigo = $this->consultar($sTabelas, $sCampos, $sComplemento);
        return $voObjetoAntigo[0];
    }

}