<?php
class FrontController{

    public function __construct(){

        $action = (isset($_GET['action'])) ? $_GET['action'] : "";
        $vAction = explode(".",$action);

        if(isset($_SESSION['oUsuarioAM']) || $action == "Login.processaFormulario"){

            if(count($vAction) != 2){
                include_once('view/main/index.php');
            } else {
                if($vAction[0] == "Login")
                    $bPemissao = true;
                else
                    $bPemissao = $this->verificarPermissao();

                if($bPemissao){
                    $sClasse = $vAction[0];
                    $sMetodo = $vAction[1];
                    $oFactoryControle = new FactoryControle();
                    $oControle = $oFactoryControle->getObject($sClasse);
                    $oControle->$sMetodo();
                } else {
                    $_SESSION['sMsg2'] = "Você não tem permissão para acessar essa área!";
                    include_once('view/main/index.php');
                }
            }
        }else {
            $oFachada = new FachadaAcessoBD();
            $sOP = (!isset($_REQUEST['sOP'])) ? "Visualizar" : $_REQUEST['sOP'];
            if(isset($_REQUEST['action']) && $oFachada->recuperarUmPermissao($_REQUEST['action'], $sOP,14)){
                $sClasse = $vAction[0];
                $sMetodo = $vAction[1];
                $oFactoryControle = new FactoryControle();
                $oControle = $oFactoryControle->getObject($sClasse);
                $oControle->$sMetodo();
            }else {
                include_once('view/login/index.php');
            }
        }
    }

    public function verificarPermissao(){
        $sOP = (!isset($_REQUEST['sOP'])) ? "Visualizar" : $_REQUEST['sOP'];
        $oFachada = new FachadaAcessoBD();

        $oTransacaoModulo = ($oFachada->recuperarUmPermissao($_REQUEST['action'], $sOP,$_SESSION['oGrupoUsuario']->getCodGrupoUsuario())) ?: $oFachada->recuperarUmPermissao($_REQUEST['action'], $sOP,14);
        if($oTransacaoModulo){
            return true;
        }else{
        //    var_dump($oTransacaoModulo);
            // var_dump($_REQUEST['action'], $sOP,$_SESSION['oGrupoUsuario']->getCodGrupoUsuario());
        //    die();
            return false;
        }


    }
}
