<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Versão</b> 1.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y')?> <a href="mailto:contato@arqmanager.com.br">ARQ MANAGER</a>.</strong> Todos os direitos reservados.
</footer>
