<?php
function maskData($sData, $bTime = false)
{
    $oData = DateTime::createFromFormat("d/m/Y",$sData);

    if ($oData)
        return ($bTime) ? $oData->format('Y-m-d H:i:s') : $oData->format('Y-m-d');
    else
        return null;
}

function maskInvData($sDate,$bTime = false)
{
    try {
        if ($sDate){
            $oDate = new DateTime($sDate);
            return ($bTime) ? $oDate->format('d/m/Y H:i:s') : $oDate->format('d/m/Y');
        } else
            return false;

    } catch (Exception $e) {
        return $e->getMessage();
    }
}

function removeEspacos($string)
{
    $string = str_replace(" ", "_", $string);

    return $string;
}

function removeAcentos($string)
{
    $string = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );

    return $string;
}

function By2M($size){
    $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
    return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
}

function getUserPhoto(){

    $nCodColaborador = $_SESSION['oUsuarioAM']->getCodColaborador();

    if ($_SESSION['oUsuarioAM']->getFoto() == null){
        $sUrl = "dist/img/no_avatar.jpg";
    } else {
        $sUrl = "?action=Colaborador.colaboradorFoto&CodColaborador=$nCodColaborador";
    }

    return $sUrl;

}