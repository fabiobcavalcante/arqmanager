<?php
require_once 'vendor/autoload.php';

use Mpdf\Mpdf;
use Mpdf\MpdfException;

$html = ob_get_contents();
ob_clean();

$charset = 'utf-8';
$fonteSize = 9;

try {
    $mpdf = new mPDF();

    $mpdf->SetDisplayMode('fullpage');

    if(isset($_REQUEST['nPagina']) And $_REQUEST['nPagina'] == 1)
        $rodape = "<div style='text-align: right'>Página {PAGENO} de {nb}</div>";
    else
        $rodape="";

    $mpdf->SetHTMLFooter($rodape);
    $mpdf->showImageErrors = true;
    $mpdf->curlAllowUnsafeSslRequests = true;

    $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
    $mpdf->WriteHTML($html);

//    $mpdf->Output($sNomeArquivo,'D');

    $mpdf->Output();

} catch (MpdfException $e) {
    var_dump($e);
}
