<?php if (isset($_SESSION['sMsg'])){
    if(!isset($_GET['bErro']) || $_GET['bErro'] == 0){
        $sClasse = "alert alert-success alert-dismissible";
        $sIcone="icon fa fa-check";
    }else{
        $sClasse = "alert alert-danger alert-dismissible";
        $sIcone="icon fa fa-ban";
    } ?>

    <div id="mensagem" title="Mensagem" class="<?php echo $sClasse?>">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p><i class="<?php echo $sIcone?>"></i> <?php echo $_SESSION['sMsg']?></p>
    </div>

    <?php
    unset($_SESSION['sMsg']);
} ?>

