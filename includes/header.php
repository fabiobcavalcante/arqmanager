<header class="main-header">
    <!-- Logo -->

    <a href="?" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
        <span class="logo-lg">
          <img color="white" src="https://arqmanager.com.br/wp-content/uploads/2021/06/logo_arq_azul_min.svg">
        </span>
      </span>

        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img color="white" src="https://arqmanager.com.br/wp-content/uploads/2021/06/logo_arq_azul.png" class="imagemLogo"></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Notificações -->
                <li class="dropdown notifications-menu">
                    <a href="?action=Notificacao.preparaLista">
                        <i class="fa fa-bell-o"></i>
                    </a>
                </li>
            </ul>
        </div>

    </nav>
</header>
