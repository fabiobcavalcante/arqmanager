<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <?php if(isset($_SESSION['oUsuarioAM'])){?>
            <!-- Sidebar user panel -->
            <div class="user-panel">

                <div class="pull-left image">
                    <img class="img-circle" id="img-avatar" onclick="window.open('?action=Colaborador.preparaFormulario&sOP=Detalhar&nCodColaborador=<?php echo $_SESSION['oUsuarioAM']->getCodColaborador()?>')" src="<?php echo getUserPhoto(); ?>" alt="User Image">
                </div>

                <div class="pull-left info">
                    <p><?php echo ($_SESSION['oUsuarioAM']) ? $_SESSION['oUsuarioAM']->getLogin() : "Não conectado"?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo ($_SESSION['oUsuarioAM']) ? $_SESSION['oGrupoUsuario']->getGrupoUsuario()->getDescricao() : "Não conectado"?></a> </div>
            </div>
        <?php } ?>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">PRINCIPAL</li>
            <li class="treeview"> <a href="#"> <i class="fa fa-user"></i> <span>Cliente</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="?action=Cliente.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Cliente</a></li>
                    <li><a href="?action=Cliente.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Cliente</a></li>
                </ul>
            </li>
            <?php if (($_SESSION['oEscritorio']->getIdEscritorio() !=2) && ($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2)){?>
                <li class="treeview"> <a href="#"> <i class="fa fa-bomb"></i> <span>Orçamento</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=Proposta.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Orçamentos</a></li>
                        <li><a href="?action=Proposta.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Orçamento</a></li>
                    </ul>
                </li>
            <?php }//if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2){?>
            <li class="treeview"> <a href="#"> <i class="fa fa-building"></i> <span>Projeto</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="?action=Projeto.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Projeto</a></li>
                    <?php if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2){?>
                        <li><a href="?action=Projeto.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Projeto</a></li>
                    <?php }?>
                </ul>
            </li>
						<li class="treeview"> <a href="#"> <i class="fa fa-file-text-o"></i> <span>Atas de Reunião</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="?action=Reuniao.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Atas</a></li>
                    <?php if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2){?>
                        <li><a href="?action=Reuniao.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Ata</a></li>
                    <?php }?>
                </ul>
            </li>
            <?php if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2){?>
                <li><a href="?action=Relatorio.preparaExtratoBancario&sOP=Visualizar"><i class="fa fa-dollar"></i><span>Financeiro</span></a></li>
            <?php } ?>


            <li class="header">CONSULTAS</li>
            <li class="treeview"> <a href="#"> <i class="fa fa-search"></i> <span>Consultas / Relatórios</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="?action=Relatorio.preparaLista&sOP=Entrega"><i class="fa fa fa-calendar"></i>Próximas Entregas</a></li>
                    <li><a href="?action=Relatorio.preparaLista&sOP=Concluido"><i class="fa fa-check" aria-hidden="true"></i>Entregas Realizadas</a></li>
                    <li><a href="?action=Indicacao.preparaLista&sOP=Ranking"><i class="fa fa-trophy" aria-hidden="true"></i>Ranking de Indicações</a></li>
                    <?php if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2){?>
                        <li><a href="?action=Relatorio.preparaFinanceiroMensal&sOP=Visualizar"><i class="fa fa fa-calendar"></i>Receitas previstas/Efetivadas</a></li>
                        <!-- <li><a href="?action=Relatorio.preparaFinanceiroColaborador&sOP=Visualizar"><i class="fa fa-user-plus"></i> Produção por Funcionário</a></li> -->
                    <?php } ?>
                    <li><a href="?action=Cliente.listaAniversariantes&sOP=Visualizar"><i class="fa fa-birthday-cake"></i> Aniversariantes</a></li>
                </ul>
            </li>
            <?php if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2){?>
                <li class="header">AUXILIARES</li>

                <li class="treeview"> <a href="#"> <i class="fa fa-user-plus"></i> <span>Colaborador</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=Colaborador.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Colaborador</a></li>
                        <li><a href="?action=Colaborador.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Colaborador</a></li>
                    </ul>
                </li>
                <li class="treeview"> <a href="#"> <i class="fa fa-tasks"></i> <span>Plano de Contas</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=PlanoContas.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Plano de Contas</a></li>
                        <li><a href="?action=PlanoContas.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Plano de Contas</a></li>
                    </ul>
                </li>
                <?php if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2){?>
                    <li class="treeview"> <a href="#"> <i class="fa fa-bomb"></i> <span>Serviços</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="?action=Servico.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Serviços</a></li>
                            <li><a href="?action=Servico.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Serviço</a></li>
                        </ul>
                    </li>
                <?php }//if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1 || $_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2){?>
                <li class="treeview"> <a href="#"> <i class="fa fa-bank"></i> <span>Contas Bancárias</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=ContaBancaria.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Contas Bancárias</a></li>
                        <li><a href="?action=ContaBancaria.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Conta Bancária</a></li>
                    </ul>
                </li>
                <li class="treeview"> <a href="#"> <i class="fa fa-share-alt "></i> <span>Indicações</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=Indicacao.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Indicações</a></li>
                    </ul>
                </li>
            <?php } ?>

                <?php if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1){?>
                <li class="treeview"> <a href="#"> <i class="fa fa-file-o"></i> <span>Documento</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=Documento.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Documentos</a></li>
                        <li><a href="?action=Documento.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Documento</a></li>
                    </ul>
                </li>
                <?php }?>
                <?php if(($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1) || ($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==2)){?>
                  <li class="treeview"> <a href="#"> <i class="fa fa-cogs"></i> <span>Configurações</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=Escritorio.preparaFormulario&sOP=Alterar"><i class="fa fa-circle-o"></i> Alterar Configurações</a></li>
                    </ul>
                </li>
              <?php }?>


                <li class="header">SEGURANÇA</li>
                <?php if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1){?>
                <li class="treeview"> <a href="#"> <i class="fa fa-edit"></i> <span>Módulo</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=AcessoModulo.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Módulo</a></li>
                        <li><a href="?action=AcessoModulo.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Módulo</a></li>
                    </ul>
                </li>
                <li class="treeview"> <a href="#"> <i class="fa fa-group"></i> <span>Grupo</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span> </a>
                    <ul class="treeview-menu">
                        <li><a href="?action=AcessoGrupoUsuario.preparaLista"><i class="fa fa-circle-o"></i> Gerenciar Grupo</a></li>
                        <li><a href="?action=AcessoGrupoUsuario.preparaFormulario&sOP=Cadastrar"><i class="fa fa-circle-o"></i> Cadastrar Grupo</a></li>
                    </ul>
                </li>


            </li>
            <li> <a href="?action=Colaborador.preparaFormulario&sOP=Detalhar&nCodColaborador=<?php echo $_SESSION['oUsuarioAM']->getCodColaborador()?>"> <i class="fa fa-picture-o" ></i> <span>Alterar Foto</span> </a> </li>
          <?php } // if($_SESSION['oGrupoUsuario']->getCodGrupoUsuario()==1){?>
            <li> <a href="?action=Colaborador.preparaFormulario&sOP=AlterarSenha"> <i class="fa fa-key" ></i> <span>Alterar Senha</span> </a> </li>
            <li  style="background-color:#d2d6de"> <a href="?action=Login.processaFormulario&sOP=Logoff"> <i class="fa fa-power-off" ></i> <span>Sair do Sistema</span> </a> </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
