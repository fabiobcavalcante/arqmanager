/*
 Navicat Premium Data Transfer

 Source Server         : LOCAL
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : dbarqmanager

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 08/10/2021 11:46:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for projeto_arquivamento
-- ----------------------------
DROP TABLE IF EXISTS `projeto_arquivamento`;
CREATE TABLE `projeto_arquivamento`  (
  `id_arquivamento` int(11) NOT NULL AUTO_INCREMENT,
  `cod_projeto` int(11) NULL DEFAULT NULL,
  `cod_status` int(11) NULL DEFAULT NULL,
  `cod_colaborador` int(11) NULL DEFAULT NULL,
  `data_acao` date NULL DEFAULT NULL,
  `motivo_acao` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_arquivamento`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of projeto_arquivamento
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
