<p>
    <strong>A Sr(a). <?php echo $oPropostaImpressao->getNome()?>.</strong>
</p>
<p>
    <strong></strong>
</p>
<p>
    Conforme solicitado, apresentamos proposta orçamentária para a realização de projeto <?php echo $oPropostaImpressao->getServico()->getDescricao()?> para <?php echo $oPropostaImpressao->getDescricao()?>, situado à <?php echo $oPropostaImpressao->getEndereco()?>.
</p>
<p>
    Esta proposta contempla:
</p>
<ul>
   <?php foreach($voProjetoServicoMicroServico as $oMicroServico){?>
    <li>
        <em>
           <?php
               echo $oMicroServico->getDescricao();
               if($oMicroServico->getQuantidade()){
                  echo "QTDE. " . $oMicroServico->getQuantidade() . " (".$oMicroServico->getQuantidade().") ";
               }
?>
           ;</em>
    </li>
<?php } //foreach($voProjetoServicoMicroServico as $oMicroServico){ ?>
</ul>
<p>
    <em></em>
</p>
<p>
    <strong>VALOR DA PROPOSTA:</strong>
</p>
<p>
    O valor proposto para a execução do projeto acima mencionado é:
</p>
<p>
    * Projeto..................... R$ <?php echo $oProposta->getValorPropostaFormatado()?> (<?php echo strtoupper($oProposta->getValorPropostaExtenso())?>)
</p>
<p>
    <strong></strong>
</p>
<p>
    <strong></strong>
</p>
<p>
    <strong>FORMA DE PAGAMENTO</strong>
    :
</p>
<div align="center">
    <table border="1" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td width="726">
                    <p>
                        <strong>10% À VISTA: R$ <?php echo $oProposta->getValorAvistaFormatado()?></strong>
                    </p>
                </td>
            </tr>
            <tr>
                <td width="726">
                    <p>
                        <strong> À PRAZO: R$ <?php echo $oProposta->getValorPropostaFormatado()?> </strong>
							(<?php echo $oProposta->getValorParcelaAprazo()?>)
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<p>
    <strong></strong>
</p>
<p>
    <strong>PRAZOS DE ENTREGA:</strong>
</p>
<p>
    &nbsp;
</p>
<?php
$nEtapa=1;
foreach($voProjetoServicoEtapa as $oEtapa){?>
<p>
    <?php echo $nEtapa++ ."ª etapa:". $oEtapa->getDescricao() . " - ". $oEtapa->getPrazo(). " dias, a contar da ". ($nEtapa ==1) ? "data da assinatura do contrato." : "aprovação da etapa anterior." ?>;
</p>
<?php } ?>
<p>
    Atenciosamente,
</p>
<p>
    _____________________________ __________________________
    <br/>
    <strong> <\?php echo  \$_SESSION\['oEscritorio'\]\->getNomeFantasia\(\)\?></strong>
    Cliente
</p>
<p>
    Aprovado em: ___/___/2020.
</p>
<p>
    <strong>OBS1:</strong>
    Está incluso no presente orçamento cinco visitas técnicas ao local com agendamento prévio. Para mais visitas poderá ser cobrada hora técnica no valor de R$ 200,00 (DUZENTOS REAIS). No caso de acompanhamento programado da obra será cobrado o valor de um salário mínimo por mês com direito a duas visitas semanais.
</p>
<p>
    <strong>OBS2: </strong>
    Para gerenciamento de obra poderá ser feito um    <strong><em>orçamento da execução do projeto</em></strong>. Esse trabalho constitui na administração dos custos, que inclui as cotações de preços em  três fornecedores, a compra dos materiais, a indicação da mão de obra (pedreiro, encanador, eletricista, gesseiro, pintor, marceneiro, etc.) e a coordenação da execução de todos os serviços.
</p>
<p>
    <strong></strong>
</p>
<p>
    <strong>OBS3:</strong>
    O presente orçamento não contempla projetos de estrutura, de instalações elétricas e hidrossanitárias, assim como, instalação de som, automação e ar condicionado. <strong></strong>
</p>
<p>
    <strong>OBS4:</strong>
O prazo para assessoria na execução do projeto é de <strong>12 meses</strong> a partir da entrega do mesmo. <strong></strong>
</p>
<p>
    <strong></strong>
</p>
